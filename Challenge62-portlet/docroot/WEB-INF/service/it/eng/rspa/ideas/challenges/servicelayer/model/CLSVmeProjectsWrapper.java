/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CLSVmeProjects}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSVmeProjects
 * @generated
 */
public class CLSVmeProjectsWrapper implements CLSVmeProjects,
	ModelWrapper<CLSVmeProjects> {
	public CLSVmeProjectsWrapper(CLSVmeProjects clsVmeProjects) {
		_clsVmeProjects = clsVmeProjects;
	}

	@Override
	public Class<?> getModelClass() {
		return CLSVmeProjects.class;
	}

	@Override
	public String getModelClassName() {
		return CLSVmeProjects.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("recordId", getRecordId());
		attributes.put("vmeProjectId", getVmeProjectId());
		attributes.put("VmeProjectName", getVmeProjectName());
		attributes.put("isMockup", getIsMockup());
		attributes.put("ideaId", getIdeaId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long recordId = (Long)attributes.get("recordId");

		if (recordId != null) {
			setRecordId(recordId);
		}

		Long vmeProjectId = (Long)attributes.get("vmeProjectId");

		if (vmeProjectId != null) {
			setVmeProjectId(vmeProjectId);
		}

		String VmeProjectName = (String)attributes.get("VmeProjectName");

		if (VmeProjectName != null) {
			setVmeProjectName(VmeProjectName);
		}

		Boolean isMockup = (Boolean)attributes.get("isMockup");

		if (isMockup != null) {
			setIsMockup(isMockup);
		}

		Long ideaId = (Long)attributes.get("ideaId");

		if (ideaId != null) {
			setIdeaId(ideaId);
		}
	}

	/**
	* Returns the primary key of this c l s vme projects.
	*
	* @return the primary key of this c l s vme projects
	*/
	@Override
	public long getPrimaryKey() {
		return _clsVmeProjects.getPrimaryKey();
	}

	/**
	* Sets the primary key of this c l s vme projects.
	*
	* @param primaryKey the primary key of this c l s vme projects
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_clsVmeProjects.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the record ID of this c l s vme projects.
	*
	* @return the record ID of this c l s vme projects
	*/
	@Override
	public long getRecordId() {
		return _clsVmeProjects.getRecordId();
	}

	/**
	* Sets the record ID of this c l s vme projects.
	*
	* @param recordId the record ID of this c l s vme projects
	*/
	@Override
	public void setRecordId(long recordId) {
		_clsVmeProjects.setRecordId(recordId);
	}

	/**
	* Returns the vme project ID of this c l s vme projects.
	*
	* @return the vme project ID of this c l s vme projects
	*/
	@Override
	public long getVmeProjectId() {
		return _clsVmeProjects.getVmeProjectId();
	}

	/**
	* Sets the vme project ID of this c l s vme projects.
	*
	* @param vmeProjectId the vme project ID of this c l s vme projects
	*/
	@Override
	public void setVmeProjectId(long vmeProjectId) {
		_clsVmeProjects.setVmeProjectId(vmeProjectId);
	}

	/**
	* Returns the vme project name of this c l s vme projects.
	*
	* @return the vme project name of this c l s vme projects
	*/
	@Override
	public java.lang.String getVmeProjectName() {
		return _clsVmeProjects.getVmeProjectName();
	}

	/**
	* Sets the vme project name of this c l s vme projects.
	*
	* @param VmeProjectName the vme project name of this c l s vme projects
	*/
	@Override
	public void setVmeProjectName(java.lang.String VmeProjectName) {
		_clsVmeProjects.setVmeProjectName(VmeProjectName);
	}

	/**
	* Returns the is mockup of this c l s vme projects.
	*
	* @return the is mockup of this c l s vme projects
	*/
	@Override
	public boolean getIsMockup() {
		return _clsVmeProjects.getIsMockup();
	}

	/**
	* Returns <code>true</code> if this c l s vme projects is is mockup.
	*
	* @return <code>true</code> if this c l s vme projects is is mockup; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsMockup() {
		return _clsVmeProjects.isIsMockup();
	}

	/**
	* Sets whether this c l s vme projects is is mockup.
	*
	* @param isMockup the is mockup of this c l s vme projects
	*/
	@Override
	public void setIsMockup(boolean isMockup) {
		_clsVmeProjects.setIsMockup(isMockup);
	}

	/**
	* Returns the idea ID of this c l s vme projects.
	*
	* @return the idea ID of this c l s vme projects
	*/
	@Override
	public long getIdeaId() {
		return _clsVmeProjects.getIdeaId();
	}

	/**
	* Sets the idea ID of this c l s vme projects.
	*
	* @param ideaId the idea ID of this c l s vme projects
	*/
	@Override
	public void setIdeaId(long ideaId) {
		_clsVmeProjects.setIdeaId(ideaId);
	}

	@Override
	public boolean isNew() {
		return _clsVmeProjects.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_clsVmeProjects.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _clsVmeProjects.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_clsVmeProjects.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _clsVmeProjects.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _clsVmeProjects.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_clsVmeProjects.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _clsVmeProjects.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_clsVmeProjects.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_clsVmeProjects.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_clsVmeProjects.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CLSVmeProjectsWrapper((CLSVmeProjects)_clsVmeProjects.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects clsVmeProjects) {
		return _clsVmeProjects.compareTo(clsVmeProjects);
	}

	@Override
	public int hashCode() {
		return _clsVmeProjects.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects> toCacheModel() {
		return _clsVmeProjects.toCacheModel();
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects toEscapedModel() {
		return new CLSVmeProjectsWrapper(_clsVmeProjects.toEscapedModel());
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects toUnescapedModel() {
		return new CLSVmeProjectsWrapper(_clsVmeProjects.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _clsVmeProjects.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _clsVmeProjects.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_clsVmeProjects.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSVmeProjectsWrapper)) {
			return false;
		}

		CLSVmeProjectsWrapper clsVmeProjectsWrapper = (CLSVmeProjectsWrapper)obj;

		if (Validator.equals(_clsVmeProjects,
					clsVmeProjectsWrapper._clsVmeProjects)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public CLSVmeProjects getWrappedCLSVmeProjects() {
		return _clsVmeProjects;
	}

	@Override
	public CLSVmeProjects getWrappedModel() {
		return _clsVmeProjects;
	}

	@Override
	public void resetOriginalValues() {
		_clsVmeProjects.resetOriginalValues();
	}

	private CLSVmeProjects _clsVmeProjects;
}