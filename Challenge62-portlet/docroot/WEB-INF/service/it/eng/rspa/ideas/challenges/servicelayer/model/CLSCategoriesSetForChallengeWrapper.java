/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CLSCategoriesSetForChallenge}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSCategoriesSetForChallenge
 * @generated
 */
public class CLSCategoriesSetForChallengeWrapper
	implements CLSCategoriesSetForChallenge,
		ModelWrapper<CLSCategoriesSetForChallenge> {
	public CLSCategoriesSetForChallengeWrapper(
		CLSCategoriesSetForChallenge clsCategoriesSetForChallenge) {
		_clsCategoriesSetForChallenge = clsCategoriesSetForChallenge;
	}

	@Override
	public Class<?> getModelClass() {
		return CLSCategoriesSetForChallenge.class;
	}

	@Override
	public String getModelClassName() {
		return CLSCategoriesSetForChallenge.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("categoriesSetID", getCategoriesSetID());
		attributes.put("challengeId", getChallengeId());
		attributes.put("type", getType());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long categoriesSetID = (Long)attributes.get("categoriesSetID");

		if (categoriesSetID != null) {
			setCategoriesSetID(categoriesSetID);
		}

		Long challengeId = (Long)attributes.get("challengeId");

		if (challengeId != null) {
			setChallengeId(challengeId);
		}

		Integer type = (Integer)attributes.get("type");

		if (type != null) {
			setType(type);
		}
	}

	/**
	* Returns the primary key of this c l s categories set for challenge.
	*
	* @return the primary key of this c l s categories set for challenge
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSCategoriesSetForChallengePK getPrimaryKey() {
		return _clsCategoriesSetForChallenge.getPrimaryKey();
	}

	/**
	* Sets the primary key of this c l s categories set for challenge.
	*
	* @param primaryKey the primary key of this c l s categories set for challenge
	*/
	@Override
	public void setPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSCategoriesSetForChallengePK primaryKey) {
		_clsCategoriesSetForChallenge.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the categories set i d of this c l s categories set for challenge.
	*
	* @return the categories set i d of this c l s categories set for challenge
	*/
	@Override
	public long getCategoriesSetID() {
		return _clsCategoriesSetForChallenge.getCategoriesSetID();
	}

	/**
	* Sets the categories set i d of this c l s categories set for challenge.
	*
	* @param categoriesSetID the categories set i d of this c l s categories set for challenge
	*/
	@Override
	public void setCategoriesSetID(long categoriesSetID) {
		_clsCategoriesSetForChallenge.setCategoriesSetID(categoriesSetID);
	}

	/**
	* Returns the challenge ID of this c l s categories set for challenge.
	*
	* @return the challenge ID of this c l s categories set for challenge
	*/
	@Override
	public long getChallengeId() {
		return _clsCategoriesSetForChallenge.getChallengeId();
	}

	/**
	* Sets the challenge ID of this c l s categories set for challenge.
	*
	* @param challengeId the challenge ID of this c l s categories set for challenge
	*/
	@Override
	public void setChallengeId(long challengeId) {
		_clsCategoriesSetForChallenge.setChallengeId(challengeId);
	}

	/**
	* Returns the type of this c l s categories set for challenge.
	*
	* @return the type of this c l s categories set for challenge
	*/
	@Override
	public int getType() {
		return _clsCategoriesSetForChallenge.getType();
	}

	/**
	* Sets the type of this c l s categories set for challenge.
	*
	* @param type the type of this c l s categories set for challenge
	*/
	@Override
	public void setType(int type) {
		_clsCategoriesSetForChallenge.setType(type);
	}

	@Override
	public boolean isNew() {
		return _clsCategoriesSetForChallenge.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_clsCategoriesSetForChallenge.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _clsCategoriesSetForChallenge.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_clsCategoriesSetForChallenge.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _clsCategoriesSetForChallenge.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _clsCategoriesSetForChallenge.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_clsCategoriesSetForChallenge.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _clsCategoriesSetForChallenge.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_clsCategoriesSetForChallenge.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_clsCategoriesSetForChallenge.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_clsCategoriesSetForChallenge.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CLSCategoriesSetForChallengeWrapper((CLSCategoriesSetForChallenge)_clsCategoriesSetForChallenge.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge clsCategoriesSetForChallenge) {
		return _clsCategoriesSetForChallenge.compareTo(clsCategoriesSetForChallenge);
	}

	@Override
	public int hashCode() {
		return _clsCategoriesSetForChallenge.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge> toCacheModel() {
		return _clsCategoriesSetForChallenge.toCacheModel();
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge toEscapedModel() {
		return new CLSCategoriesSetForChallengeWrapper(_clsCategoriesSetForChallenge.toEscapedModel());
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge toUnescapedModel() {
		return new CLSCategoriesSetForChallengeWrapper(_clsCategoriesSetForChallenge.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _clsCategoriesSetForChallenge.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _clsCategoriesSetForChallenge.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_clsCategoriesSetForChallenge.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSCategoriesSetForChallengeWrapper)) {
			return false;
		}

		CLSCategoriesSetForChallengeWrapper clsCategoriesSetForChallengeWrapper = (CLSCategoriesSetForChallengeWrapper)obj;

		if (Validator.equals(_clsCategoriesSetForChallenge,
					clsCategoriesSetForChallengeWrapper._clsCategoriesSetForChallenge)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public CLSCategoriesSetForChallenge getWrappedCLSCategoriesSetForChallenge() {
		return _clsCategoriesSetForChallenge;
	}

	@Override
	public CLSCategoriesSetForChallenge getWrappedModel() {
		return _clsCategoriesSetForChallenge;
	}

	@Override
	public void resetOriginalValues() {
		_clsCategoriesSetForChallenge.resetOriginalValues();
	}

	private CLSCategoriesSetForChallenge _clsCategoriesSetForChallenge;
}