/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge;

/**
 * The persistence interface for the need linked challenge service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see NeedLinkedChallengePersistenceImpl
 * @see NeedLinkedChallengeUtil
 * @generated
 */
public interface NeedLinkedChallengePersistence extends BasePersistence<NeedLinkedChallenge> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link NeedLinkedChallengeUtil} to access the need linked challenge persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the need linked challenges where needId = &#63;.
	*
	* @param needId the need ID
	* @return the matching need linked challenges
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge> findBychallengeByNeed(
		long needId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the need linked challenges where needId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.NeedLinkedChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param needId the need ID
	* @param start the lower bound of the range of need linked challenges
	* @param end the upper bound of the range of need linked challenges (not inclusive)
	* @return the range of matching need linked challenges
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge> findBychallengeByNeed(
		long needId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the need linked challenges where needId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.NeedLinkedChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param needId the need ID
	* @param start the lower bound of the range of need linked challenges
	* @param end the upper bound of the range of need linked challenges (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching need linked challenges
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge> findBychallengeByNeed(
		long needId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first need linked challenge in the ordered set where needId = &#63;.
	*
	* @param needId the need ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching need linked challenge
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException if a matching need linked challenge could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge findBychallengeByNeed_First(
		long needId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException;

	/**
	* Returns the first need linked challenge in the ordered set where needId = &#63;.
	*
	* @param needId the need ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching need linked challenge, or <code>null</code> if a matching need linked challenge could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge fetchBychallengeByNeed_First(
		long needId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last need linked challenge in the ordered set where needId = &#63;.
	*
	* @param needId the need ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching need linked challenge
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException if a matching need linked challenge could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge findBychallengeByNeed_Last(
		long needId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException;

	/**
	* Returns the last need linked challenge in the ordered set where needId = &#63;.
	*
	* @param needId the need ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching need linked challenge, or <code>null</code> if a matching need linked challenge could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge fetchBychallengeByNeed_Last(
		long needId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the need linked challenges before and after the current need linked challenge in the ordered set where needId = &#63;.
	*
	* @param needLinkedChallengePK the primary key of the current need linked challenge
	* @param needId the need ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next need linked challenge
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException if a need linked challenge with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge[] findBychallengeByNeed_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.NeedLinkedChallengePK needLinkedChallengePK,
		long needId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException;

	/**
	* Removes all the need linked challenges where needId = &#63; from the database.
	*
	* @param needId the need ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeBychallengeByNeed(long needId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of need linked challenges where needId = &#63;.
	*
	* @param needId the need ID
	* @return the number of matching need linked challenges
	* @throws SystemException if a system exception occurred
	*/
	public int countBychallengeByNeed(long needId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the need linked challenges where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @return the matching need linked challenges
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge> findByneedByChallenge(
		long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the need linked challenges where challengeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.NeedLinkedChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param start the lower bound of the range of need linked challenges
	* @param end the upper bound of the range of need linked challenges (not inclusive)
	* @return the range of matching need linked challenges
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge> findByneedByChallenge(
		long challengeId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the need linked challenges where challengeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.NeedLinkedChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param start the lower bound of the range of need linked challenges
	* @param end the upper bound of the range of need linked challenges (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching need linked challenges
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge> findByneedByChallenge(
		long challengeId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first need linked challenge in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching need linked challenge
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException if a matching need linked challenge could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge findByneedByChallenge_First(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException;

	/**
	* Returns the first need linked challenge in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching need linked challenge, or <code>null</code> if a matching need linked challenge could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge fetchByneedByChallenge_First(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last need linked challenge in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching need linked challenge
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException if a matching need linked challenge could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge findByneedByChallenge_Last(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException;

	/**
	* Returns the last need linked challenge in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching need linked challenge, or <code>null</code> if a matching need linked challenge could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge fetchByneedByChallenge_Last(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the need linked challenges before and after the current need linked challenge in the ordered set where challengeId = &#63;.
	*
	* @param needLinkedChallengePK the primary key of the current need linked challenge
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next need linked challenge
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException if a need linked challenge with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge[] findByneedByChallenge_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.NeedLinkedChallengePK needLinkedChallengePK,
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException;

	/**
	* Removes all the need linked challenges where challengeId = &#63; from the database.
	*
	* @param challengeId the challenge ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByneedByChallenge(long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of need linked challenges where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @return the number of matching need linked challenges
	* @throws SystemException if a system exception occurred
	*/
	public int countByneedByChallenge(long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the need linked challenge in the entity cache if it is enabled.
	*
	* @param needLinkedChallenge the need linked challenge
	*/
	public void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge needLinkedChallenge);

	/**
	* Caches the need linked challenges in the entity cache if it is enabled.
	*
	* @param needLinkedChallenges the need linked challenges
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge> needLinkedChallenges);

	/**
	* Creates a new need linked challenge with the primary key. Does not add the need linked challenge to the database.
	*
	* @param needLinkedChallengePK the primary key for the new need linked challenge
	* @return the new need linked challenge
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge create(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.NeedLinkedChallengePK needLinkedChallengePK);

	/**
	* Removes the need linked challenge with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param needLinkedChallengePK the primary key of the need linked challenge
	* @return the need linked challenge that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException if a need linked challenge with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge remove(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.NeedLinkedChallengePK needLinkedChallengePK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException;

	public it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge needLinkedChallenge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the need linked challenge with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException} if it could not be found.
	*
	* @param needLinkedChallengePK the primary key of the need linked challenge
	* @return the need linked challenge
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException if a need linked challenge with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge findByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.NeedLinkedChallengePK needLinkedChallengePK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException;

	/**
	* Returns the need linked challenge with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param needLinkedChallengePK the primary key of the need linked challenge
	* @return the need linked challenge, or <code>null</code> if a need linked challenge with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge fetchByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.NeedLinkedChallengePK needLinkedChallengePK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the need linked challenges.
	*
	* @return the need linked challenges
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the need linked challenges.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.NeedLinkedChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of need linked challenges
	* @param end the upper bound of the range of need linked challenges (not inclusive)
	* @return the range of need linked challenges
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the need linked challenges.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.NeedLinkedChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of need linked challenges
	* @param end the upper bound of the range of need linked challenges (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of need linked challenges
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the need linked challenges from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of need linked challenges.
	*
	* @return the number of need linked challenges
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}