/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSIdeaServiceSoap}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSIdeaServiceSoap
 * @generated
 */
public class CLSIdeaSoap implements Serializable {
	public static CLSIdeaSoap toSoapModel(CLSIdea model) {
		CLSIdeaSoap soapModel = new CLSIdeaSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setIdeaID(model.getIdeaID());
		soapModel.setIdeaTitle(model.getIdeaTitle());
		soapModel.setIdeaDescription(model.getIdeaDescription());
		soapModel.setDateAdded(model.getDateAdded());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setUserId(model.getUserId());
		soapModel.setMunicipalityId(model.getMunicipalityId());
		soapModel.setMunicipalityOrganizationId(model.getMunicipalityOrganizationId());
		soapModel.setChallengeId(model.getChallengeId());
		soapModel.setTakenUp(model.getTakenUp());
		soapModel.setNeedId(model.getNeedId());
		soapModel.setDmFolderName(model.getDmFolderName());
		soapModel.setIdFolder(model.getIdFolder());
		soapModel.setRepresentativeImgUrl(model.getRepresentativeImgUrl());
		soapModel.setIsNeed(model.getIsNeed());
		soapModel.setStatus(model.getStatus());
		soapModel.setStatusByUserId(model.getStatusByUserId());
		soapModel.setStatusByUserName(model.getStatusByUserName());
		soapModel.setStatusDate(model.getStatusDate());
		soapModel.setChatGroup(model.getChatGroup());
		soapModel.setIdeaHashTag(model.getIdeaHashTag());
		soapModel.setIdeaStatus(model.getIdeaStatus());
		soapModel.setLanguage(model.getLanguage());
		soapModel.setFinalMotivation(model.getFinalMotivation());
		soapModel.setEvaluationPassed(model.getEvaluationPassed());

		return soapModel;
	}

	public static CLSIdeaSoap[] toSoapModels(CLSIdea[] models) {
		CLSIdeaSoap[] soapModels = new CLSIdeaSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CLSIdeaSoap[][] toSoapModels(CLSIdea[][] models) {
		CLSIdeaSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CLSIdeaSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CLSIdeaSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CLSIdeaSoap[] toSoapModels(List<CLSIdea> models) {
		List<CLSIdeaSoap> soapModels = new ArrayList<CLSIdeaSoap>(models.size());

		for (CLSIdea model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CLSIdeaSoap[soapModels.size()]);
	}

	public CLSIdeaSoap() {
	}

	public long getPrimaryKey() {
		return _ideaID;
	}

	public void setPrimaryKey(long pk) {
		setIdeaID(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getIdeaID() {
		return _ideaID;
	}

	public void setIdeaID(long ideaID) {
		_ideaID = ideaID;
	}

	public String getIdeaTitle() {
		return _ideaTitle;
	}

	public void setIdeaTitle(String ideaTitle) {
		_ideaTitle = ideaTitle;
	}

	public String getIdeaDescription() {
		return _ideaDescription;
	}

	public void setIdeaDescription(String ideaDescription) {
		_ideaDescription = ideaDescription;
	}

	public Date getDateAdded() {
		return _dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		_dateAdded = dateAdded;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public long getMunicipalityId() {
		return _municipalityId;
	}

	public void setMunicipalityId(long municipalityId) {
		_municipalityId = municipalityId;
	}

	public long getMunicipalityOrganizationId() {
		return _municipalityOrganizationId;
	}

	public void setMunicipalityOrganizationId(long municipalityOrganizationId) {
		_municipalityOrganizationId = municipalityOrganizationId;
	}

	public long getChallengeId() {
		return _challengeId;
	}

	public void setChallengeId(long challengeId) {
		_challengeId = challengeId;
	}

	public boolean getTakenUp() {
		return _takenUp;
	}

	public boolean isTakenUp() {
		return _takenUp;
	}

	public void setTakenUp(boolean takenUp) {
		_takenUp = takenUp;
	}

	public long getNeedId() {
		return _needId;
	}

	public void setNeedId(long needId) {
		_needId = needId;
	}

	public String getDmFolderName() {
		return _dmFolderName;
	}

	public void setDmFolderName(String dmFolderName) {
		_dmFolderName = dmFolderName;
	}

	public long getIdFolder() {
		return _idFolder;
	}

	public void setIdFolder(long idFolder) {
		_idFolder = idFolder;
	}

	public String getRepresentativeImgUrl() {
		return _representativeImgUrl;
	}

	public void setRepresentativeImgUrl(String representativeImgUrl) {
		_representativeImgUrl = representativeImgUrl;
	}

	public boolean getIsNeed() {
		return _isNeed;
	}

	public boolean isIsNeed() {
		return _isNeed;
	}

	public void setIsNeed(boolean isNeed) {
		_isNeed = isNeed;
	}

	public int getStatus() {
		return _status;
	}

	public void setStatus(int status) {
		_status = status;
	}

	public long getStatusByUserId() {
		return _statusByUserId;
	}

	public void setStatusByUserId(long statusByUserId) {
		_statusByUserId = statusByUserId;
	}

	public String getStatusByUserName() {
		return _statusByUserName;
	}

	public void setStatusByUserName(String statusByUserName) {
		_statusByUserName = statusByUserName;
	}

	public Date getStatusDate() {
		return _statusDate;
	}

	public void setStatusDate(Date statusDate) {
		_statusDate = statusDate;
	}

	public long getChatGroup() {
		return _chatGroup;
	}

	public void setChatGroup(long chatGroup) {
		_chatGroup = chatGroup;
	}

	public String getIdeaHashTag() {
		return _ideaHashTag;
	}

	public void setIdeaHashTag(String ideaHashTag) {
		_ideaHashTag = ideaHashTag;
	}

	public String getIdeaStatus() {
		return _ideaStatus;
	}

	public void setIdeaStatus(String ideaStatus) {
		_ideaStatus = ideaStatus;
	}

	public String getLanguage() {
		return _language;
	}

	public void setLanguage(String language) {
		_language = language;
	}

	public String getFinalMotivation() {
		return _finalMotivation;
	}

	public void setFinalMotivation(String finalMotivation) {
		_finalMotivation = finalMotivation;
	}

	public boolean getEvaluationPassed() {
		return _evaluationPassed;
	}

	public boolean isEvaluationPassed() {
		return _evaluationPassed;
	}

	public void setEvaluationPassed(boolean evaluationPassed) {
		_evaluationPassed = evaluationPassed;
	}

	private String _uuid;
	private long _ideaID;
	private String _ideaTitle;
	private String _ideaDescription;
	private Date _dateAdded;
	private long _companyId;
	private long _groupId;
	private long _userId;
	private long _municipalityId;
	private long _municipalityOrganizationId;
	private long _challengeId;
	private boolean _takenUp;
	private long _needId;
	private String _dmFolderName;
	private long _idFolder;
	private String _representativeImgUrl;
	private boolean _isNeed;
	private int _status;
	private long _statusByUserId;
	private String _statusByUserName;
	private Date _statusDate;
	private long _chatGroup;
	private String _ideaHashTag;
	private String _ideaStatus;
	private String _language;
	private String _finalMotivation;
	private boolean _evaluationPassed;
}