/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer;
import it.eng.rspa.ideas.challenges.servicelayer.service.NeedLinkedChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.NeedLinkedChallengePK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class NeedLinkedChallengeClp extends BaseModelImpl<NeedLinkedChallenge>
	implements NeedLinkedChallenge {
	public NeedLinkedChallengeClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return NeedLinkedChallenge.class;
	}

	@Override
	public String getModelClassName() {
		return NeedLinkedChallenge.class.getName();
	}

	@Override
	public NeedLinkedChallengePK getPrimaryKey() {
		return new NeedLinkedChallengePK(_needId, _challengeId);
	}

	@Override
	public void setPrimaryKey(NeedLinkedChallengePK primaryKey) {
		setNeedId(primaryKey.needId);
		setChallengeId(primaryKey.challengeId);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new NeedLinkedChallengePK(_needId, _challengeId);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((NeedLinkedChallengePK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("needId", getNeedId());
		attributes.put("challengeId", getChallengeId());
		attributes.put("date", getDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long needId = (Long)attributes.get("needId");

		if (needId != null) {
			setNeedId(needId);
		}

		Long challengeId = (Long)attributes.get("challengeId");

		if (challengeId != null) {
			setChallengeId(challengeId);
		}

		Date date = (Date)attributes.get("date");

		if (date != null) {
			setDate(date);
		}
	}

	@Override
	public long getNeedId() {
		return _needId;
	}

	@Override
	public void setNeedId(long needId) {
		_needId = needId;

		if (_needLinkedChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _needLinkedChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setNeedId", long.class);

				method.invoke(_needLinkedChallengeRemoteModel, needId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getChallengeId() {
		return _challengeId;
	}

	@Override
	public void setChallengeId(long challengeId) {
		_challengeId = challengeId;

		if (_needLinkedChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _needLinkedChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setChallengeId", long.class);

				method.invoke(_needLinkedChallengeRemoteModel, challengeId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDate() {
		return _date;
	}

	@Override
	public void setDate(Date date) {
		_date = date;

		if (_needLinkedChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _needLinkedChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setDate", Date.class);

				method.invoke(_needLinkedChallengeRemoteModel, date);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getNeedLinkedChallengeRemoteModel() {
		return _needLinkedChallengeRemoteModel;
	}

	public void setNeedLinkedChallengeRemoteModel(
		BaseModel<?> needLinkedChallengeRemoteModel) {
		_needLinkedChallengeRemoteModel = needLinkedChallengeRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _needLinkedChallengeRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_needLinkedChallengeRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			NeedLinkedChallengeLocalServiceUtil.addNeedLinkedChallenge(this);
		}
		else {
			NeedLinkedChallengeLocalServiceUtil.updateNeedLinkedChallenge(this);
		}
	}

	@Override
	public NeedLinkedChallenge toEscapedModel() {
		return (NeedLinkedChallenge)ProxyUtil.newProxyInstance(NeedLinkedChallenge.class.getClassLoader(),
			new Class[] { NeedLinkedChallenge.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		NeedLinkedChallengeClp clone = new NeedLinkedChallengeClp();

		clone.setNeedId(getNeedId());
		clone.setChallengeId(getChallengeId());
		clone.setDate(getDate());

		return clone;
	}

	@Override
	public int compareTo(NeedLinkedChallenge needLinkedChallenge) {
		NeedLinkedChallengePK primaryKey = needLinkedChallenge.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof NeedLinkedChallengeClp)) {
			return false;
		}

		NeedLinkedChallengeClp needLinkedChallenge = (NeedLinkedChallengeClp)obj;

		NeedLinkedChallengePK primaryKey = needLinkedChallenge.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{needId=");
		sb.append(getNeedId());
		sb.append(", challengeId=");
		sb.append(getChallengeId());
		sb.append(", date=");
		sb.append(getDate());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append(
			"it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>needId</column-name><column-value><![CDATA[");
		sb.append(getNeedId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>challengeId</column-name><column-value><![CDATA[");
		sb.append(getChallengeId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>date</column-name><column-value><![CDATA[");
		sb.append(getDate());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _needId;
	private long _challengeId;
	private Date _date;
	private BaseModel<?> _needLinkedChallengeRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.class;
}