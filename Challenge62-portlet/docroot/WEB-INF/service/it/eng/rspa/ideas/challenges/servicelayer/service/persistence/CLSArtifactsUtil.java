/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts;

import java.util.List;

/**
 * The persistence utility for the c l s artifacts service. This utility wraps {@link CLSArtifactsPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSArtifactsPersistence
 * @see CLSArtifactsPersistenceImpl
 * @generated
 */
public class CLSArtifactsUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(CLSArtifacts clsArtifacts) {
		getPersistence().clearCache(clsArtifacts);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CLSArtifacts> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CLSArtifacts> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CLSArtifacts> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static CLSArtifacts update(CLSArtifacts clsArtifacts)
		throws SystemException {
		return getPersistence().update(clsArtifacts);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static CLSArtifacts update(CLSArtifacts clsArtifacts,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(clsArtifacts, serviceContext);
	}

	/**
	* Returns all the c l s artifactses where ideaId = &#63; and artifactId = &#63;.
	*
	* @param ideaId the idea ID
	* @param artifactId the artifact ID
	* @return the matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findByartifactsByIdeaIdEArtifactId(
		long ideaId, long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByartifactsByIdeaIdEArtifactId(ideaId, artifactId);
	}

	/**
	* Returns a range of all the c l s artifactses where ideaId = &#63; and artifactId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param artifactId the artifact ID
	* @param start the lower bound of the range of c l s artifactses
	* @param end the upper bound of the range of c l s artifactses (not inclusive)
	* @return the range of matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findByartifactsByIdeaIdEArtifactId(
		long ideaId, long artifactId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByartifactsByIdeaIdEArtifactId(ideaId, artifactId,
			start, end);
	}

	/**
	* Returns an ordered range of all the c l s artifactses where ideaId = &#63; and artifactId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param artifactId the artifact ID
	* @param start the lower bound of the range of c l s artifactses
	* @param end the upper bound of the range of c l s artifactses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findByartifactsByIdeaIdEArtifactId(
		long ideaId, long artifactId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByartifactsByIdeaIdEArtifactId(ideaId, artifactId,
			start, end, orderByComparator);
	}

	/**
	* Returns the first c l s artifacts in the ordered set where ideaId = &#63; and artifactId = &#63;.
	*
	* @param ideaId the idea ID
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s artifacts
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts findByartifactsByIdeaIdEArtifactId_First(
		long ideaId, long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException {
		return getPersistence()
				   .findByartifactsByIdeaIdEArtifactId_First(ideaId,
			artifactId, orderByComparator);
	}

	/**
	* Returns the first c l s artifacts in the ordered set where ideaId = &#63; and artifactId = &#63;.
	*
	* @param ideaId the idea ID
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s artifacts, or <code>null</code> if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts fetchByartifactsByIdeaIdEArtifactId_First(
		long ideaId, long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByartifactsByIdeaIdEArtifactId_First(ideaId,
			artifactId, orderByComparator);
	}

	/**
	* Returns the last c l s artifacts in the ordered set where ideaId = &#63; and artifactId = &#63;.
	*
	* @param ideaId the idea ID
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s artifacts
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts findByartifactsByIdeaIdEArtifactId_Last(
		long ideaId, long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException {
		return getPersistence()
				   .findByartifactsByIdeaIdEArtifactId_Last(ideaId, artifactId,
			orderByComparator);
	}

	/**
	* Returns the last c l s artifacts in the ordered set where ideaId = &#63; and artifactId = &#63;.
	*
	* @param ideaId the idea ID
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s artifacts, or <code>null</code> if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts fetchByartifactsByIdeaIdEArtifactId_Last(
		long ideaId, long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByartifactsByIdeaIdEArtifactId_Last(ideaId,
			artifactId, orderByComparator);
	}

	/**
	* Returns the c l s artifactses before and after the current c l s artifacts in the ordered set where ideaId = &#63; and artifactId = &#63;.
	*
	* @param clsArtifactsPK the primary key of the current c l s artifacts
	* @param ideaId the idea ID
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s artifacts
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a c l s artifacts with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts[] findByartifactsByIdeaIdEArtifactId_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK clsArtifactsPK,
		long ideaId, long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException {
		return getPersistence()
				   .findByartifactsByIdeaIdEArtifactId_PrevAndNext(clsArtifactsPK,
			ideaId, artifactId, orderByComparator);
	}

	/**
	* Removes all the c l s artifactses where ideaId = &#63; and artifactId = &#63; from the database.
	*
	* @param ideaId the idea ID
	* @param artifactId the artifact ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByartifactsByIdeaIdEArtifactId(long ideaId,
		long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByartifactsByIdeaIdEArtifactId(ideaId, artifactId);
	}

	/**
	* Returns the number of c l s artifactses where ideaId = &#63; and artifactId = &#63;.
	*
	* @param ideaId the idea ID
	* @param artifactId the artifact ID
	* @return the number of matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByartifactsByIdeaIdEArtifactId(long ideaId,
		long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByartifactsByIdeaIdEArtifactId(ideaId, artifactId);
	}

	/**
	* Returns all the c l s artifactses where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findByartifactsByIdeaId(
		long ideaId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByartifactsByIdeaId(ideaId);
	}

	/**
	* Returns a range of all the c l s artifactses where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of c l s artifactses
	* @param end the upper bound of the range of c l s artifactses (not inclusive)
	* @return the range of matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findByartifactsByIdeaId(
		long ideaId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByartifactsByIdeaId(ideaId, start, end);
	}

	/**
	* Returns an ordered range of all the c l s artifactses where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of c l s artifactses
	* @param end the upper bound of the range of c l s artifactses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findByartifactsByIdeaId(
		long ideaId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByartifactsByIdeaId(ideaId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first c l s artifacts in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s artifacts
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts findByartifactsByIdeaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException {
		return getPersistence()
				   .findByartifactsByIdeaId_First(ideaId, orderByComparator);
	}

	/**
	* Returns the first c l s artifacts in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s artifacts, or <code>null</code> if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts fetchByartifactsByIdeaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByartifactsByIdeaId_First(ideaId, orderByComparator);
	}

	/**
	* Returns the last c l s artifacts in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s artifacts
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts findByartifactsByIdeaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException {
		return getPersistence()
				   .findByartifactsByIdeaId_Last(ideaId, orderByComparator);
	}

	/**
	* Returns the last c l s artifacts in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s artifacts, or <code>null</code> if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts fetchByartifactsByIdeaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByartifactsByIdeaId_Last(ideaId, orderByComparator);
	}

	/**
	* Returns the c l s artifactses before and after the current c l s artifacts in the ordered set where ideaId = &#63;.
	*
	* @param clsArtifactsPK the primary key of the current c l s artifacts
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s artifacts
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a c l s artifacts with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts[] findByartifactsByIdeaId_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK clsArtifactsPK,
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException {
		return getPersistence()
				   .findByartifactsByIdeaId_PrevAndNext(clsArtifactsPK, ideaId,
			orderByComparator);
	}

	/**
	* Removes all the c l s artifactses where ideaId = &#63; from the database.
	*
	* @param ideaId the idea ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByartifactsByIdeaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByartifactsByIdeaId(ideaId);
	}

	/**
	* Returns the number of c l s artifactses where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the number of matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByartifactsByIdeaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByartifactsByIdeaId(ideaId);
	}

	/**
	* Returns all the c l s artifactses where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @return the matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findByartifactsByArtifactId(
		long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByartifactsByArtifactId(artifactId);
	}

	/**
	* Returns a range of all the c l s artifactses where artifactId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param artifactId the artifact ID
	* @param start the lower bound of the range of c l s artifactses
	* @param end the upper bound of the range of c l s artifactses (not inclusive)
	* @return the range of matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findByartifactsByArtifactId(
		long artifactId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByartifactsByArtifactId(artifactId, start, end);
	}

	/**
	* Returns an ordered range of all the c l s artifactses where artifactId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param artifactId the artifact ID
	* @param start the lower bound of the range of c l s artifactses
	* @param end the upper bound of the range of c l s artifactses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findByartifactsByArtifactId(
		long artifactId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByartifactsByArtifactId(artifactId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first c l s artifacts in the ordered set where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s artifacts
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts findByartifactsByArtifactId_First(
		long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException {
		return getPersistence()
				   .findByartifactsByArtifactId_First(artifactId,
			orderByComparator);
	}

	/**
	* Returns the first c l s artifacts in the ordered set where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s artifacts, or <code>null</code> if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts fetchByartifactsByArtifactId_First(
		long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByartifactsByArtifactId_First(artifactId,
			orderByComparator);
	}

	/**
	* Returns the last c l s artifacts in the ordered set where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s artifacts
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts findByartifactsByArtifactId_Last(
		long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException {
		return getPersistence()
				   .findByartifactsByArtifactId_Last(artifactId,
			orderByComparator);
	}

	/**
	* Returns the last c l s artifacts in the ordered set where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s artifacts, or <code>null</code> if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts fetchByartifactsByArtifactId_Last(
		long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByartifactsByArtifactId_Last(artifactId,
			orderByComparator);
	}

	/**
	* Returns the c l s artifactses before and after the current c l s artifacts in the ordered set where artifactId = &#63;.
	*
	* @param clsArtifactsPK the primary key of the current c l s artifacts
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s artifacts
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a c l s artifacts with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts[] findByartifactsByArtifactId_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK clsArtifactsPK,
		long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException {
		return getPersistence()
				   .findByartifactsByArtifactId_PrevAndNext(clsArtifactsPK,
			artifactId, orderByComparator);
	}

	/**
	* Removes all the c l s artifactses where artifactId = &#63; from the database.
	*
	* @param artifactId the artifact ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByartifactsByArtifactId(long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByartifactsByArtifactId(artifactId);
	}

	/**
	* Returns the number of c l s artifactses where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @return the number of matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByartifactsByArtifactId(long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByartifactsByArtifactId(artifactId);
	}

	/**
	* Caches the c l s artifacts in the entity cache if it is enabled.
	*
	* @param clsArtifacts the c l s artifacts
	*/
	public static void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts clsArtifacts) {
		getPersistence().cacheResult(clsArtifacts);
	}

	/**
	* Caches the c l s artifactses in the entity cache if it is enabled.
	*
	* @param clsArtifactses the c l s artifactses
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> clsArtifactses) {
		getPersistence().cacheResult(clsArtifactses);
	}

	/**
	* Creates a new c l s artifacts with the primary key. Does not add the c l s artifacts to the database.
	*
	* @param clsArtifactsPK the primary key for the new c l s artifacts
	* @return the new c l s artifacts
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts create(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK clsArtifactsPK) {
		return getPersistence().create(clsArtifactsPK);
	}

	/**
	* Removes the c l s artifacts with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param clsArtifactsPK the primary key of the c l s artifacts
	* @return the c l s artifacts that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a c l s artifacts with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts remove(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK clsArtifactsPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException {
		return getPersistence().remove(clsArtifactsPK);
	}

	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts clsArtifacts)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(clsArtifacts);
	}

	/**
	* Returns the c l s artifacts with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException} if it could not be found.
	*
	* @param clsArtifactsPK the primary key of the c l s artifacts
	* @return the c l s artifacts
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a c l s artifacts with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts findByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK clsArtifactsPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException {
		return getPersistence().findByPrimaryKey(clsArtifactsPK);
	}

	/**
	* Returns the c l s artifacts with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param clsArtifactsPK the primary key of the c l s artifacts
	* @return the c l s artifacts, or <code>null</code> if a c l s artifacts with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts fetchByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK clsArtifactsPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(clsArtifactsPK);
	}

	/**
	* Returns all the c l s artifactses.
	*
	* @return the c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the c l s artifactses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s artifactses
	* @param end the upper bound of the range of c l s artifactses (not inclusive)
	* @return the range of c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the c l s artifactses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s artifactses
	* @param end the upper bound of the range of c l s artifactses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the c l s artifactses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of c l s artifactses.
	*
	* @return the number of c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static CLSArtifactsPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (CLSArtifactsPersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.getServletContextName(),
					CLSArtifactsPersistence.class.getName());

			ReferenceRegistry.registerReference(CLSArtifactsUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(CLSArtifactsPersistence persistence) {
	}

	private static CLSArtifactsPersistence _persistence;
}