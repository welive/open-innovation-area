/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCategoriesSetLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class CLSCategoriesSetClp extends BaseModelImpl<CLSCategoriesSet>
	implements CLSCategoriesSet {
	public CLSCategoriesSetClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return CLSCategoriesSet.class;
	}

	@Override
	public String getModelClassName() {
		return CLSCategoriesSet.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _categoriesSetID;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setCategoriesSetID(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _categoriesSetID;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("categoriesSetID", getCategoriesSetID());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long categoriesSetID = (Long)attributes.get("categoriesSetID");

		if (categoriesSetID != null) {
			setCategoriesSetID(categoriesSetID);
		}
	}

	@Override
	public long getCategoriesSetID() {
		return _categoriesSetID;
	}

	@Override
	public void setCategoriesSetID(long categoriesSetID) {
		_categoriesSetID = categoriesSetID;

		if (_clsCategoriesSetRemoteModel != null) {
			try {
				Class<?> clazz = _clsCategoriesSetRemoteModel.getClass();

				Method method = clazz.getMethod("setCategoriesSetID", long.class);

				method.invoke(_clsCategoriesSetRemoteModel, categoriesSetID);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getCLSCategoriesSetRemoteModel() {
		return _clsCategoriesSetRemoteModel;
	}

	public void setCLSCategoriesSetRemoteModel(
		BaseModel<?> clsCategoriesSetRemoteModel) {
		_clsCategoriesSetRemoteModel = clsCategoriesSetRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _clsCategoriesSetRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_clsCategoriesSetRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CLSCategoriesSetLocalServiceUtil.addCLSCategoriesSet(this);
		}
		else {
			CLSCategoriesSetLocalServiceUtil.updateCLSCategoriesSet(this);
		}
	}

	@Override
	public CLSCategoriesSet toEscapedModel() {
		return (CLSCategoriesSet)ProxyUtil.newProxyInstance(CLSCategoriesSet.class.getClassLoader(),
			new Class[] { CLSCategoriesSet.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CLSCategoriesSetClp clone = new CLSCategoriesSetClp();

		clone.setCategoriesSetID(getCategoriesSetID());

		return clone;
	}

	@Override
	public int compareTo(CLSCategoriesSet clsCategoriesSet) {
		long primaryKey = clsCategoriesSet.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSCategoriesSetClp)) {
			return false;
		}

		CLSCategoriesSetClp clsCategoriesSet = (CLSCategoriesSetClp)obj;

		long primaryKey = clsCategoriesSet.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(3);

		sb.append("{categoriesSetID=");
		sb.append(getCategoriesSetID());

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(7);

		sb.append("<model><model-name>");
		sb.append(
			"it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>categoriesSetID</column-name><column-value><![CDATA[");
		sb.append(getCategoriesSetID());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _categoriesSetID;
	private BaseModel<?> _clsCategoriesSetRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.class;
}