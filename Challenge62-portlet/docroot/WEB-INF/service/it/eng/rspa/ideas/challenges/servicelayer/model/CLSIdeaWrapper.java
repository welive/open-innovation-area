/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CLSIdea}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSIdea
 * @generated
 */
public class CLSIdeaWrapper implements CLSIdea, ModelWrapper<CLSIdea> {
	public CLSIdeaWrapper(CLSIdea clsIdea) {
		_clsIdea = clsIdea;
	}

	@Override
	public Class<?> getModelClass() {
		return CLSIdea.class;
	}

	@Override
	public String getModelClassName() {
		return CLSIdea.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("ideaID", getIdeaID());
		attributes.put("ideaTitle", getIdeaTitle());
		attributes.put("ideaDescription", getIdeaDescription());
		attributes.put("dateAdded", getDateAdded());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("userId", getUserId());
		attributes.put("municipalityId", getMunicipalityId());
		attributes.put("municipalityOrganizationId",
			getMunicipalityOrganizationId());
		attributes.put("challengeId", getChallengeId());
		attributes.put("takenUp", getTakenUp());
		attributes.put("needId", getNeedId());
		attributes.put("dmFolderName", getDmFolderName());
		attributes.put("idFolder", getIdFolder());
		attributes.put("representativeImgUrl", getRepresentativeImgUrl());
		attributes.put("isNeed", getIsNeed());
		attributes.put("status", getStatus());
		attributes.put("statusByUserId", getStatusByUserId());
		attributes.put("statusByUserName", getStatusByUserName());
		attributes.put("statusDate", getStatusDate());
		attributes.put("chatGroup", getChatGroup());
		attributes.put("ideaHashTag", getIdeaHashTag());
		attributes.put("ideaStatus", getIdeaStatus());
		attributes.put("language", getLanguage());
		attributes.put("finalMotivation", getFinalMotivation());
		attributes.put("evaluationPassed", getEvaluationPassed());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long ideaID = (Long)attributes.get("ideaID");

		if (ideaID != null) {
			setIdeaID(ideaID);
		}

		String ideaTitle = (String)attributes.get("ideaTitle");

		if (ideaTitle != null) {
			setIdeaTitle(ideaTitle);
		}

		String ideaDescription = (String)attributes.get("ideaDescription");

		if (ideaDescription != null) {
			setIdeaDescription(ideaDescription);
		}

		Date dateAdded = (Date)attributes.get("dateAdded");

		if (dateAdded != null) {
			setDateAdded(dateAdded);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Long municipalityId = (Long)attributes.get("municipalityId");

		if (municipalityId != null) {
			setMunicipalityId(municipalityId);
		}

		Long municipalityOrganizationId = (Long)attributes.get(
				"municipalityOrganizationId");

		if (municipalityOrganizationId != null) {
			setMunicipalityOrganizationId(municipalityOrganizationId);
		}

		Long challengeId = (Long)attributes.get("challengeId");

		if (challengeId != null) {
			setChallengeId(challengeId);
		}

		Boolean takenUp = (Boolean)attributes.get("takenUp");

		if (takenUp != null) {
			setTakenUp(takenUp);
		}

		Long needId = (Long)attributes.get("needId");

		if (needId != null) {
			setNeedId(needId);
		}

		String dmFolderName = (String)attributes.get("dmFolderName");

		if (dmFolderName != null) {
			setDmFolderName(dmFolderName);
		}

		Long idFolder = (Long)attributes.get("idFolder");

		if (idFolder != null) {
			setIdFolder(idFolder);
		}

		String representativeImgUrl = (String)attributes.get(
				"representativeImgUrl");

		if (representativeImgUrl != null) {
			setRepresentativeImgUrl(representativeImgUrl);
		}

		Boolean isNeed = (Boolean)attributes.get("isNeed");

		if (isNeed != null) {
			setIsNeed(isNeed);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Long statusByUserId = (Long)attributes.get("statusByUserId");

		if (statusByUserId != null) {
			setStatusByUserId(statusByUserId);
		}

		String statusByUserName = (String)attributes.get("statusByUserName");

		if (statusByUserName != null) {
			setStatusByUserName(statusByUserName);
		}

		Date statusDate = (Date)attributes.get("statusDate");

		if (statusDate != null) {
			setStatusDate(statusDate);
		}

		Long chatGroup = (Long)attributes.get("chatGroup");

		if (chatGroup != null) {
			setChatGroup(chatGroup);
		}

		String ideaHashTag = (String)attributes.get("ideaHashTag");

		if (ideaHashTag != null) {
			setIdeaHashTag(ideaHashTag);
		}

		String ideaStatus = (String)attributes.get("ideaStatus");

		if (ideaStatus != null) {
			setIdeaStatus(ideaStatus);
		}

		String language = (String)attributes.get("language");

		if (language != null) {
			setLanguage(language);
		}

		String finalMotivation = (String)attributes.get("finalMotivation");

		if (finalMotivation != null) {
			setFinalMotivation(finalMotivation);
		}

		Boolean evaluationPassed = (Boolean)attributes.get("evaluationPassed");

		if (evaluationPassed != null) {
			setEvaluationPassed(evaluationPassed);
		}
	}

	/**
	* Returns the primary key of this c l s idea.
	*
	* @return the primary key of this c l s idea
	*/
	@Override
	public long getPrimaryKey() {
		return _clsIdea.getPrimaryKey();
	}

	/**
	* Sets the primary key of this c l s idea.
	*
	* @param primaryKey the primary key of this c l s idea
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_clsIdea.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the uuid of this c l s idea.
	*
	* @return the uuid of this c l s idea
	*/
	@Override
	public java.lang.String getUuid() {
		return _clsIdea.getUuid();
	}

	/**
	* Sets the uuid of this c l s idea.
	*
	* @param uuid the uuid of this c l s idea
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_clsIdea.setUuid(uuid);
	}

	/**
	* Returns the idea i d of this c l s idea.
	*
	* @return the idea i d of this c l s idea
	*/
	@Override
	public long getIdeaID() {
		return _clsIdea.getIdeaID();
	}

	/**
	* Sets the idea i d of this c l s idea.
	*
	* @param ideaID the idea i d of this c l s idea
	*/
	@Override
	public void setIdeaID(long ideaID) {
		_clsIdea.setIdeaID(ideaID);
	}

	/**
	* Returns the idea title of this c l s idea.
	*
	* @return the idea title of this c l s idea
	*/
	@Override
	public java.lang.String getIdeaTitle() {
		return _clsIdea.getIdeaTitle();
	}

	/**
	* Sets the idea title of this c l s idea.
	*
	* @param ideaTitle the idea title of this c l s idea
	*/
	@Override
	public void setIdeaTitle(java.lang.String ideaTitle) {
		_clsIdea.setIdeaTitle(ideaTitle);
	}

	/**
	* Returns the idea description of this c l s idea.
	*
	* @return the idea description of this c l s idea
	*/
	@Override
	public java.lang.String getIdeaDescription() {
		return _clsIdea.getIdeaDescription();
	}

	/**
	* Sets the idea description of this c l s idea.
	*
	* @param ideaDescription the idea description of this c l s idea
	*/
	@Override
	public void setIdeaDescription(java.lang.String ideaDescription) {
		_clsIdea.setIdeaDescription(ideaDescription);
	}

	/**
	* Returns the date added of this c l s idea.
	*
	* @return the date added of this c l s idea
	*/
	@Override
	public java.util.Date getDateAdded() {
		return _clsIdea.getDateAdded();
	}

	/**
	* Sets the date added of this c l s idea.
	*
	* @param dateAdded the date added of this c l s idea
	*/
	@Override
	public void setDateAdded(java.util.Date dateAdded) {
		_clsIdea.setDateAdded(dateAdded);
	}

	/**
	* Returns the company ID of this c l s idea.
	*
	* @return the company ID of this c l s idea
	*/
	@Override
	public long getCompanyId() {
		return _clsIdea.getCompanyId();
	}

	/**
	* Sets the company ID of this c l s idea.
	*
	* @param companyId the company ID of this c l s idea
	*/
	@Override
	public void setCompanyId(long companyId) {
		_clsIdea.setCompanyId(companyId);
	}

	/**
	* Returns the group ID of this c l s idea.
	*
	* @return the group ID of this c l s idea
	*/
	@Override
	public long getGroupId() {
		return _clsIdea.getGroupId();
	}

	/**
	* Sets the group ID of this c l s idea.
	*
	* @param groupId the group ID of this c l s idea
	*/
	@Override
	public void setGroupId(long groupId) {
		_clsIdea.setGroupId(groupId);
	}

	/**
	* Returns the user ID of this c l s idea.
	*
	* @return the user ID of this c l s idea
	*/
	@Override
	public long getUserId() {
		return _clsIdea.getUserId();
	}

	/**
	* Sets the user ID of this c l s idea.
	*
	* @param userId the user ID of this c l s idea
	*/
	@Override
	public void setUserId(long userId) {
		_clsIdea.setUserId(userId);
	}

	/**
	* Returns the user uuid of this c l s idea.
	*
	* @return the user uuid of this c l s idea
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdea.getUserUuid();
	}

	/**
	* Sets the user uuid of this c l s idea.
	*
	* @param userUuid the user uuid of this c l s idea
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_clsIdea.setUserUuid(userUuid);
	}

	/**
	* Returns the municipality ID of this c l s idea.
	*
	* @return the municipality ID of this c l s idea
	*/
	@Override
	public long getMunicipalityId() {
		return _clsIdea.getMunicipalityId();
	}

	/**
	* Sets the municipality ID of this c l s idea.
	*
	* @param municipalityId the municipality ID of this c l s idea
	*/
	@Override
	public void setMunicipalityId(long municipalityId) {
		_clsIdea.setMunicipalityId(municipalityId);
	}

	/**
	* Returns the municipality organization ID of this c l s idea.
	*
	* @return the municipality organization ID of this c l s idea
	*/
	@Override
	public long getMunicipalityOrganizationId() {
		return _clsIdea.getMunicipalityOrganizationId();
	}

	/**
	* Sets the municipality organization ID of this c l s idea.
	*
	* @param municipalityOrganizationId the municipality organization ID of this c l s idea
	*/
	@Override
	public void setMunicipalityOrganizationId(long municipalityOrganizationId) {
		_clsIdea.setMunicipalityOrganizationId(municipalityOrganizationId);
	}

	/**
	* Returns the challenge ID of this c l s idea.
	*
	* @return the challenge ID of this c l s idea
	*/
	@Override
	public long getChallengeId() {
		return _clsIdea.getChallengeId();
	}

	/**
	* Sets the challenge ID of this c l s idea.
	*
	* @param challengeId the challenge ID of this c l s idea
	*/
	@Override
	public void setChallengeId(long challengeId) {
		_clsIdea.setChallengeId(challengeId);
	}

	/**
	* Returns the taken up of this c l s idea.
	*
	* @return the taken up of this c l s idea
	*/
	@Override
	public boolean getTakenUp() {
		return _clsIdea.getTakenUp();
	}

	/**
	* Returns <code>true</code> if this c l s idea is taken up.
	*
	* @return <code>true</code> if this c l s idea is taken up; <code>false</code> otherwise
	*/
	@Override
	public boolean isTakenUp() {
		return _clsIdea.isTakenUp();
	}

	/**
	* Sets whether this c l s idea is taken up.
	*
	* @param takenUp the taken up of this c l s idea
	*/
	@Override
	public void setTakenUp(boolean takenUp) {
		_clsIdea.setTakenUp(takenUp);
	}

	/**
	* Returns the need ID of this c l s idea.
	*
	* @return the need ID of this c l s idea
	*/
	@Override
	public long getNeedId() {
		return _clsIdea.getNeedId();
	}

	/**
	* Sets the need ID of this c l s idea.
	*
	* @param needId the need ID of this c l s idea
	*/
	@Override
	public void setNeedId(long needId) {
		_clsIdea.setNeedId(needId);
	}

	/**
	* Returns the dm folder name of this c l s idea.
	*
	* @return the dm folder name of this c l s idea
	*/
	@Override
	public java.lang.String getDmFolderName() {
		return _clsIdea.getDmFolderName();
	}

	/**
	* Sets the dm folder name of this c l s idea.
	*
	* @param dmFolderName the dm folder name of this c l s idea
	*/
	@Override
	public void setDmFolderName(java.lang.String dmFolderName) {
		_clsIdea.setDmFolderName(dmFolderName);
	}

	/**
	* Returns the id folder of this c l s idea.
	*
	* @return the id folder of this c l s idea
	*/
	@Override
	public long getIdFolder() {
		return _clsIdea.getIdFolder();
	}

	/**
	* Sets the id folder of this c l s idea.
	*
	* @param idFolder the id folder of this c l s idea
	*/
	@Override
	public void setIdFolder(long idFolder) {
		_clsIdea.setIdFolder(idFolder);
	}

	/**
	* Returns the representative img url of this c l s idea.
	*
	* @return the representative img url of this c l s idea
	*/
	@Override
	public java.lang.String getRepresentativeImgUrl() {
		return _clsIdea.getRepresentativeImgUrl();
	}

	/**
	* Sets the representative img url of this c l s idea.
	*
	* @param representativeImgUrl the representative img url of this c l s idea
	*/
	@Override
	public void setRepresentativeImgUrl(java.lang.String representativeImgUrl) {
		_clsIdea.setRepresentativeImgUrl(representativeImgUrl);
	}

	/**
	* Returns the is need of this c l s idea.
	*
	* @return the is need of this c l s idea
	*/
	@Override
	public boolean getIsNeed() {
		return _clsIdea.getIsNeed();
	}

	/**
	* Returns <code>true</code> if this c l s idea is is need.
	*
	* @return <code>true</code> if this c l s idea is is need; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsNeed() {
		return _clsIdea.isIsNeed();
	}

	/**
	* Sets whether this c l s idea is is need.
	*
	* @param isNeed the is need of this c l s idea
	*/
	@Override
	public void setIsNeed(boolean isNeed) {
		_clsIdea.setIsNeed(isNeed);
	}

	/**
	* Returns the status of this c l s idea.
	*
	* @return the status of this c l s idea
	*/
	@Override
	public int getStatus() {
		return _clsIdea.getStatus();
	}

	/**
	* Sets the status of this c l s idea.
	*
	* @param status the status of this c l s idea
	*/
	@Override
	public void setStatus(int status) {
		_clsIdea.setStatus(status);
	}

	/**
	* Returns the status by user ID of this c l s idea.
	*
	* @return the status by user ID of this c l s idea
	*/
	@Override
	public long getStatusByUserId() {
		return _clsIdea.getStatusByUserId();
	}

	/**
	* Sets the status by user ID of this c l s idea.
	*
	* @param statusByUserId the status by user ID of this c l s idea
	*/
	@Override
	public void setStatusByUserId(long statusByUserId) {
		_clsIdea.setStatusByUserId(statusByUserId);
	}

	/**
	* Returns the status by user uuid of this c l s idea.
	*
	* @return the status by user uuid of this c l s idea
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getStatusByUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdea.getStatusByUserUuid();
	}

	/**
	* Sets the status by user uuid of this c l s idea.
	*
	* @param statusByUserUuid the status by user uuid of this c l s idea
	*/
	@Override
	public void setStatusByUserUuid(java.lang.String statusByUserUuid) {
		_clsIdea.setStatusByUserUuid(statusByUserUuid);
	}

	/**
	* Returns the status by user name of this c l s idea.
	*
	* @return the status by user name of this c l s idea
	*/
	@Override
	public java.lang.String getStatusByUserName() {
		return _clsIdea.getStatusByUserName();
	}

	/**
	* Sets the status by user name of this c l s idea.
	*
	* @param statusByUserName the status by user name of this c l s idea
	*/
	@Override
	public void setStatusByUserName(java.lang.String statusByUserName) {
		_clsIdea.setStatusByUserName(statusByUserName);
	}

	/**
	* Returns the status date of this c l s idea.
	*
	* @return the status date of this c l s idea
	*/
	@Override
	public java.util.Date getStatusDate() {
		return _clsIdea.getStatusDate();
	}

	/**
	* Sets the status date of this c l s idea.
	*
	* @param statusDate the status date of this c l s idea
	*/
	@Override
	public void setStatusDate(java.util.Date statusDate) {
		_clsIdea.setStatusDate(statusDate);
	}

	/**
	* Returns the chat group of this c l s idea.
	*
	* @return the chat group of this c l s idea
	*/
	@Override
	public long getChatGroup() {
		return _clsIdea.getChatGroup();
	}

	/**
	* Sets the chat group of this c l s idea.
	*
	* @param chatGroup the chat group of this c l s idea
	*/
	@Override
	public void setChatGroup(long chatGroup) {
		_clsIdea.setChatGroup(chatGroup);
	}

	/**
	* Returns the idea hash tag of this c l s idea.
	*
	* @return the idea hash tag of this c l s idea
	*/
	@Override
	public java.lang.String getIdeaHashTag() {
		return _clsIdea.getIdeaHashTag();
	}

	/**
	* Sets the idea hash tag of this c l s idea.
	*
	* @param ideaHashTag the idea hash tag of this c l s idea
	*/
	@Override
	public void setIdeaHashTag(java.lang.String ideaHashTag) {
		_clsIdea.setIdeaHashTag(ideaHashTag);
	}

	/**
	* Returns the idea status of this c l s idea.
	*
	* @return the idea status of this c l s idea
	*/
	@Override
	public java.lang.String getIdeaStatus() {
		return _clsIdea.getIdeaStatus();
	}

	/**
	* Sets the idea status of this c l s idea.
	*
	* @param ideaStatus the idea status of this c l s idea
	*/
	@Override
	public void setIdeaStatus(java.lang.String ideaStatus) {
		_clsIdea.setIdeaStatus(ideaStatus);
	}

	/**
	* Returns the language of this c l s idea.
	*
	* @return the language of this c l s idea
	*/
	@Override
	public java.lang.String getLanguage() {
		return _clsIdea.getLanguage();
	}

	/**
	* Sets the language of this c l s idea.
	*
	* @param language the language of this c l s idea
	*/
	@Override
	public void setLanguage(java.lang.String language) {
		_clsIdea.setLanguage(language);
	}

	/**
	* Returns the final motivation of this c l s idea.
	*
	* @return the final motivation of this c l s idea
	*/
	@Override
	public java.lang.String getFinalMotivation() {
		return _clsIdea.getFinalMotivation();
	}

	/**
	* Sets the final motivation of this c l s idea.
	*
	* @param finalMotivation the final motivation of this c l s idea
	*/
	@Override
	public void setFinalMotivation(java.lang.String finalMotivation) {
		_clsIdea.setFinalMotivation(finalMotivation);
	}

	/**
	* Returns the evaluation passed of this c l s idea.
	*
	* @return the evaluation passed of this c l s idea
	*/
	@Override
	public boolean getEvaluationPassed() {
		return _clsIdea.getEvaluationPassed();
	}

	/**
	* Returns <code>true</code> if this c l s idea is evaluation passed.
	*
	* @return <code>true</code> if this c l s idea is evaluation passed; <code>false</code> otherwise
	*/
	@Override
	public boolean isEvaluationPassed() {
		return _clsIdea.isEvaluationPassed();
	}

	/**
	* Sets whether this c l s idea is evaluation passed.
	*
	* @param evaluationPassed the evaluation passed of this c l s idea
	*/
	@Override
	public void setEvaluationPassed(boolean evaluationPassed) {
		_clsIdea.setEvaluationPassed(evaluationPassed);
	}

	/**
	* @deprecated As of 6.1.0, replaced by {@link #isApproved()}
	*/
	@Override
	public boolean getApproved() {
		return _clsIdea.getApproved();
	}

	/**
	* Returns <code>true</code> if this c l s idea is approved.
	*
	* @return <code>true</code> if this c l s idea is approved; <code>false</code> otherwise
	*/
	@Override
	public boolean isApproved() {
		return _clsIdea.isApproved();
	}

	/**
	* Returns <code>true</code> if this c l s idea is denied.
	*
	* @return <code>true</code> if this c l s idea is denied; <code>false</code> otherwise
	*/
	@Override
	public boolean isDenied() {
		return _clsIdea.isDenied();
	}

	/**
	* Returns <code>true</code> if this c l s idea is a draft.
	*
	* @return <code>true</code> if this c l s idea is a draft; <code>false</code> otherwise
	*/
	@Override
	public boolean isDraft() {
		return _clsIdea.isDraft();
	}

	/**
	* Returns <code>true</code> if this c l s idea is expired.
	*
	* @return <code>true</code> if this c l s idea is expired; <code>false</code> otherwise
	*/
	@Override
	public boolean isExpired() {
		return _clsIdea.isExpired();
	}

	/**
	* Returns <code>true</code> if this c l s idea is inactive.
	*
	* @return <code>true</code> if this c l s idea is inactive; <code>false</code> otherwise
	*/
	@Override
	public boolean isInactive() {
		return _clsIdea.isInactive();
	}

	/**
	* Returns <code>true</code> if this c l s idea is incomplete.
	*
	* @return <code>true</code> if this c l s idea is incomplete; <code>false</code> otherwise
	*/
	@Override
	public boolean isIncomplete() {
		return _clsIdea.isIncomplete();
	}

	/**
	* Returns <code>true</code> if this c l s idea is pending.
	*
	* @return <code>true</code> if this c l s idea is pending; <code>false</code> otherwise
	*/
	@Override
	public boolean isPending() {
		return _clsIdea.isPending();
	}

	/**
	* Returns <code>true</code> if this c l s idea is scheduled.
	*
	* @return <code>true</code> if this c l s idea is scheduled; <code>false</code> otherwise
	*/
	@Override
	public boolean isScheduled() {
		return _clsIdea.isScheduled();
	}

	@Override
	public boolean isNew() {
		return _clsIdea.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_clsIdea.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _clsIdea.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_clsIdea.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _clsIdea.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _clsIdea.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_clsIdea.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _clsIdea.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_clsIdea.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_clsIdea.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_clsIdea.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CLSIdeaWrapper((CLSIdea)_clsIdea.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea clsIdea) {
		return _clsIdea.compareTo(clsIdea);
	}

	@Override
	public int hashCode() {
		return _clsIdea.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> toCacheModel() {
		return _clsIdea.toCacheModel();
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea toEscapedModel() {
		return new CLSIdeaWrapper(_clsIdea.toEscapedModel());
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea toUnescapedModel() {
		return new CLSIdeaWrapper(_clsIdea.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _clsIdea.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _clsIdea.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_clsIdea.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSIdeaWrapper)) {
			return false;
		}

		CLSIdeaWrapper clsIdeaWrapper = (CLSIdeaWrapper)obj;

		if (Validator.equals(_clsIdea, clsIdeaWrapper._clsIdea)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public CLSIdea getWrappedCLSIdea() {
		return _clsIdea;
	}

	@Override
	public CLSIdea getWrappedModel() {
		return _clsIdea;
	}

	@Override
	public void resetOriginalValues() {
		_clsIdea.resetOriginalValues();
	}

	private CLSIdea _clsIdea;
}