/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 * @generated
 */
public class EvaluationCriteriaPK implements Comparable<EvaluationCriteriaPK>,
	Serializable {
	public long criteriaId;
	public long challengeId;

	public EvaluationCriteriaPK() {
	}

	public EvaluationCriteriaPK(long criteriaId, long challengeId) {
		this.criteriaId = criteriaId;
		this.challengeId = challengeId;
	}

	public long getCriteriaId() {
		return criteriaId;
	}

	public void setCriteriaId(long criteriaId) {
		this.criteriaId = criteriaId;
	}

	public long getChallengeId() {
		return challengeId;
	}

	public void setChallengeId(long challengeId) {
		this.challengeId = challengeId;
	}

	@Override
	public int compareTo(EvaluationCriteriaPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (criteriaId < pk.criteriaId) {
			value = -1;
		}
		else if (criteriaId > pk.criteriaId) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		if (challengeId < pk.challengeId) {
			value = -1;
		}
		else if (challengeId > pk.challengeId) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof EvaluationCriteriaPK)) {
			return false;
		}

		EvaluationCriteriaPK pk = (EvaluationCriteriaPK)obj;

		if ((criteriaId == pk.criteriaId) && (challengeId == pk.challengeId)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (String.valueOf(criteriaId) + String.valueOf(challengeId)).hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(10);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("criteriaId");
		sb.append(StringPool.EQUAL);
		sb.append(criteriaId);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("challengeId");
		sb.append(StringPool.EQUAL);
		sb.append(challengeId);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}