/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSVmeProjectsServiceSoap}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSVmeProjectsServiceSoap
 * @generated
 */
public class CLSVmeProjectsSoap implements Serializable {
	public static CLSVmeProjectsSoap toSoapModel(CLSVmeProjects model) {
		CLSVmeProjectsSoap soapModel = new CLSVmeProjectsSoap();

		soapModel.setRecordId(model.getRecordId());
		soapModel.setVmeProjectId(model.getVmeProjectId());
		soapModel.setVmeProjectName(model.getVmeProjectName());
		soapModel.setIsMockup(model.getIsMockup());
		soapModel.setIdeaId(model.getIdeaId());

		return soapModel;
	}

	public static CLSVmeProjectsSoap[] toSoapModels(CLSVmeProjects[] models) {
		CLSVmeProjectsSoap[] soapModels = new CLSVmeProjectsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CLSVmeProjectsSoap[][] toSoapModels(CLSVmeProjects[][] models) {
		CLSVmeProjectsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CLSVmeProjectsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CLSVmeProjectsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CLSVmeProjectsSoap[] toSoapModels(List<CLSVmeProjects> models) {
		List<CLSVmeProjectsSoap> soapModels = new ArrayList<CLSVmeProjectsSoap>(models.size());

		for (CLSVmeProjects model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CLSVmeProjectsSoap[soapModels.size()]);
	}

	public CLSVmeProjectsSoap() {
	}

	public long getPrimaryKey() {
		return _recordId;
	}

	public void setPrimaryKey(long pk) {
		setRecordId(pk);
	}

	public long getRecordId() {
		return _recordId;
	}

	public void setRecordId(long recordId) {
		_recordId = recordId;
	}

	public long getVmeProjectId() {
		return _vmeProjectId;
	}

	public void setVmeProjectId(long vmeProjectId) {
		_vmeProjectId = vmeProjectId;
	}

	public String getVmeProjectName() {
		return _VmeProjectName;
	}

	public void setVmeProjectName(String VmeProjectName) {
		_VmeProjectName = VmeProjectName;
	}

	public boolean getIsMockup() {
		return _isMockup;
	}

	public boolean isIsMockup() {
		return _isMockup;
	}

	public void setIsMockup(boolean isMockup) {
		_isMockup = isMockup;
	}

	public long getIdeaId() {
		return _ideaId;
	}

	public void setIdeaId(long ideaId) {
		_ideaId = ideaId;
	}

	private long _recordId;
	private long _vmeProjectId;
	private String _VmeProjectName;
	private boolean _isMockup;
	private long _ideaId;
}