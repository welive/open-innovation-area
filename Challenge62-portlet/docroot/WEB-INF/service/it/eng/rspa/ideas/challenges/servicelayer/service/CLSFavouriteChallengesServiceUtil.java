/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for CLSFavouriteChallenges. This utility wraps
 * {@link it.eng.rspa.ideas.challenges.servicelayer.service.impl.CLSFavouriteChallengesServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSFavouriteChallengesService
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSFavouriteChallengesServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.impl.CLSFavouriteChallengesServiceImpl
 * @generated
 */
public class CLSFavouriteChallengesServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.eng.rspa.ideas.challenges.servicelayer.service.impl.CLSFavouriteChallengesServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges> getFavouriteChallenges(
		java.lang.Long favChallengeId, java.lang.Long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getFavouriteChallenges(favChallengeId, userId);
	}

	public static boolean removeFavouriteChallenge(
		java.lang.Long favChallengeId, java.lang.Long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().removeFavouriteChallenge(favChallengeId, userId);
	}

	public static boolean addFavouriteChallenge(java.lang.Long favChallengeId,
		java.lang.Long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addFavouriteChallenge(favChallengeId, userId);
	}

	public static void clearService() {
		_service = null;
	}

	public static CLSFavouriteChallengesService getService() {
		if (_service == null) {
			InvokableService invokableService = (InvokableService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					CLSFavouriteChallengesService.class.getName());

			if (invokableService instanceof CLSFavouriteChallengesService) {
				_service = (CLSFavouriteChallengesService)invokableService;
			}
			else {
				_service = new CLSFavouriteChallengesServiceClp(invokableService);
			}

			ReferenceRegistry.registerReference(CLSFavouriteChallengesServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(CLSFavouriteChallengesService service) {
	}

	private static CLSFavouriteChallengesService _service;
}