/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CLSFavouriteIdeasService}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSFavouriteIdeasService
 * @generated
 */
public class CLSFavouriteIdeasServiceWrapper implements CLSFavouriteIdeasService,
	ServiceWrapper<CLSFavouriteIdeasService> {
	public CLSFavouriteIdeasServiceWrapper(
		CLSFavouriteIdeasService clsFavouriteIdeasService) {
		_clsFavouriteIdeasService = clsFavouriteIdeasService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _clsFavouriteIdeasService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_clsFavouriteIdeasService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _clsFavouriteIdeasService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas> getFavouriteIdeas(
		java.lang.Long ideaId, java.lang.Long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasService.getFavouriteIdeas(ideaId, userId);
	}

	@Override
	public boolean addFavouriteIdea(java.lang.Long ideaId, java.lang.Long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasService.addFavouriteIdea(ideaId, userId);
	}

	@Override
	public boolean removeFavouriteIdea(java.lang.Long favIdeaId,
		java.lang.Long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasService.removeFavouriteIdea(favIdeaId, userId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CLSFavouriteIdeasService getWrappedCLSFavouriteIdeasService() {
		return _clsFavouriteIdeasService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCLSFavouriteIdeasService(
		CLSFavouriteIdeasService clsFavouriteIdeasService) {
		_clsFavouriteIdeasService = clsFavouriteIdeasService;
	}

	@Override
	public CLSFavouriteIdeasService getWrappedService() {
		return _clsFavouriteIdeasService;
	}

	@Override
	public void setWrappedService(
		CLSFavouriteIdeasService clsFavouriteIdeasService) {
		_clsFavouriteIdeasService = clsFavouriteIdeasService;
	}

	private CLSFavouriteIdeasService _clsFavouriteIdeasService;
}