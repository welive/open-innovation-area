/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.ideas.challenges.servicelayer.service.CLSRequisitiLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class CLSRequisitiClp extends BaseModelImpl<CLSRequisiti>
	implements CLSRequisiti {
	public CLSRequisitiClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return CLSRequisiti.class;
	}

	@Override
	public String getModelClassName() {
		return CLSRequisiti.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _requisitoId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setRequisitoId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _requisitoId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("requisitoId", getRequisitoId());
		attributes.put("ideaId", getIdeaId());
		attributes.put("taskId", getTaskId());
		attributes.put("tipo", getTipo());
		attributes.put("descrizione", getDescrizione());
		attributes.put("authorUser", getAuthorUser());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long requisitoId = (Long)attributes.get("requisitoId");

		if (requisitoId != null) {
			setRequisitoId(requisitoId);
		}

		Long ideaId = (Long)attributes.get("ideaId");

		if (ideaId != null) {
			setIdeaId(ideaId);
		}

		Long taskId = (Long)attributes.get("taskId");

		if (taskId != null) {
			setTaskId(taskId);
		}

		String tipo = (String)attributes.get("tipo");

		if (tipo != null) {
			setTipo(tipo);
		}

		String descrizione = (String)attributes.get("descrizione");

		if (descrizione != null) {
			setDescrizione(descrizione);
		}

		Long authorUser = (Long)attributes.get("authorUser");

		if (authorUser != null) {
			setAuthorUser(authorUser);
		}
	}

	@Override
	public long getRequisitoId() {
		return _requisitoId;
	}

	@Override
	public void setRequisitoId(long requisitoId) {
		_requisitoId = requisitoId;

		if (_clsRequisitiRemoteModel != null) {
			try {
				Class<?> clazz = _clsRequisitiRemoteModel.getClass();

				Method method = clazz.getMethod("setRequisitoId", long.class);

				method.invoke(_clsRequisitiRemoteModel, requisitoId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdeaId() {
		return _ideaId;
	}

	@Override
	public void setIdeaId(long ideaId) {
		_ideaId = ideaId;

		if (_clsRequisitiRemoteModel != null) {
			try {
				Class<?> clazz = _clsRequisitiRemoteModel.getClass();

				Method method = clazz.getMethod("setIdeaId", long.class);

				method.invoke(_clsRequisitiRemoteModel, ideaId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getTaskId() {
		return _taskId;
	}

	@Override
	public void setTaskId(long taskId) {
		_taskId = taskId;

		if (_clsRequisitiRemoteModel != null) {
			try {
				Class<?> clazz = _clsRequisitiRemoteModel.getClass();

				Method method = clazz.getMethod("setTaskId", long.class);

				method.invoke(_clsRequisitiRemoteModel, taskId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTipo() {
		return _tipo;
	}

	@Override
	public void setTipo(String tipo) {
		_tipo = tipo;

		if (_clsRequisitiRemoteModel != null) {
			try {
				Class<?> clazz = _clsRequisitiRemoteModel.getClass();

				Method method = clazz.getMethod("setTipo", String.class);

				method.invoke(_clsRequisitiRemoteModel, tipo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescrizione() {
		return _descrizione;
	}

	@Override
	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;

		if (_clsRequisitiRemoteModel != null) {
			try {
				Class<?> clazz = _clsRequisitiRemoteModel.getClass();

				Method method = clazz.getMethod("setDescrizione", String.class);

				method.invoke(_clsRequisitiRemoteModel, descrizione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getAuthorUser() {
		return _authorUser;
	}

	@Override
	public void setAuthorUser(long authorUser) {
		_authorUser = authorUser;

		if (_clsRequisitiRemoteModel != null) {
			try {
				Class<?> clazz = _clsRequisitiRemoteModel.getClass();

				Method method = clazz.getMethod("setAuthorUser", long.class);

				method.invoke(_clsRequisitiRemoteModel, authorUser);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getCLSRequisitiRemoteModel() {
		return _clsRequisitiRemoteModel;
	}

	public void setCLSRequisitiRemoteModel(BaseModel<?> clsRequisitiRemoteModel) {
		_clsRequisitiRemoteModel = clsRequisitiRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _clsRequisitiRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_clsRequisitiRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CLSRequisitiLocalServiceUtil.addCLSRequisiti(this);
		}
		else {
			CLSRequisitiLocalServiceUtil.updateCLSRequisiti(this);
		}
	}

	@Override
	public CLSRequisiti toEscapedModel() {
		return (CLSRequisiti)ProxyUtil.newProxyInstance(CLSRequisiti.class.getClassLoader(),
			new Class[] { CLSRequisiti.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CLSRequisitiClp clone = new CLSRequisitiClp();

		clone.setRequisitoId(getRequisitoId());
		clone.setIdeaId(getIdeaId());
		clone.setTaskId(getTaskId());
		clone.setTipo(getTipo());
		clone.setDescrizione(getDescrizione());
		clone.setAuthorUser(getAuthorUser());

		return clone;
	}

	@Override
	public int compareTo(CLSRequisiti clsRequisiti) {
		long primaryKey = clsRequisiti.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSRequisitiClp)) {
			return false;
		}

		CLSRequisitiClp clsRequisiti = (CLSRequisitiClp)obj;

		long primaryKey = clsRequisiti.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{requisitoId=");
		sb.append(getRequisitoId());
		sb.append(", ideaId=");
		sb.append(getIdeaId());
		sb.append(", taskId=");
		sb.append(getTaskId());
		sb.append(", tipo=");
		sb.append(getTipo());
		sb.append(", descrizione=");
		sb.append(getDescrizione());
		sb.append(", authorUser=");
		sb.append(getAuthorUser());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(22);

		sb.append("<model><model-name>");
		sb.append(
			"it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>requisitoId</column-name><column-value><![CDATA[");
		sb.append(getRequisitoId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ideaId</column-name><column-value><![CDATA[");
		sb.append(getIdeaId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>taskId</column-name><column-value><![CDATA[");
		sb.append(getTaskId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipo</column-name><column-value><![CDATA[");
		sb.append(getTipo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descrizione</column-name><column-value><![CDATA[");
		sb.append(getDescrizione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>authorUser</column-name><column-value><![CDATA[");
		sb.append(getAuthorUser());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _requisitoId;
	private long _ideaId;
	private long _taskId;
	private String _tipo;
	private String _descrizione;
	private long _authorUser;
	private BaseModel<?> _clsRequisitiRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.class;
}