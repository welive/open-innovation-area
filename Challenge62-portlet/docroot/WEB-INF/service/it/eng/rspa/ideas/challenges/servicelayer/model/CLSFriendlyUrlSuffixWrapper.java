/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CLSFriendlyUrlSuffix}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSFriendlyUrlSuffix
 * @generated
 */
public class CLSFriendlyUrlSuffixWrapper implements CLSFriendlyUrlSuffix,
	ModelWrapper<CLSFriendlyUrlSuffix> {
	public CLSFriendlyUrlSuffixWrapper(
		CLSFriendlyUrlSuffix clsFriendlyUrlSuffix) {
		_clsFriendlyUrlSuffix = clsFriendlyUrlSuffix;
	}

	@Override
	public Class<?> getModelClass() {
		return CLSFriendlyUrlSuffix.class;
	}

	@Override
	public String getModelClassName() {
		return CLSFriendlyUrlSuffix.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("friendlyUrlSuffixID", getFriendlyUrlSuffixID());
		attributes.put("UrlSuffixImsHome", getUrlSuffixImsHome());
		attributes.put("UrlSuffixIdea", getUrlSuffixIdea());
		attributes.put("UrlSuffixNeed", getUrlSuffixNeed());
		attributes.put("UrlSuffixChallenge", getUrlSuffixChallenge());
		attributes.put("senderNotificheMailIdeario",
			getSenderNotificheMailIdeario());
		attributes.put("oggettoNotificheMailIdeario",
			getOggettoNotificheMailIdeario());
		attributes.put("firmaNotificheMailIdeario",
			getFirmaNotificheMailIdeario());
		attributes.put("utenzaMail", getUtenzaMail());
		attributes.put("cdvEnabled", getCdvEnabled());
		attributes.put("cdvAddress", getCdvAddress());
		attributes.put("vcEnabled", getVcEnabled());
		attributes.put("vcAddress", getVcAddress());
		attributes.put("vcWSAddress", getVcWSAddress());
		attributes.put("deEnabled", getDeEnabled());
		attributes.put("deAddress", getDeAddress());
		attributes.put("lbbEnabled", getLbbEnabled());
		attributes.put("lbbAddress", getLbbAddress());
		attributes.put("oiaAppId4lbb", getOiaAppId4lbb());
		attributes.put("tweetingEnabled", getTweetingEnabled());
		attributes.put("basicAuthUser", getBasicAuthUser());
		attributes.put("basicAuthPwd", getBasicAuthPwd());
		attributes.put("verboseEnabled", getVerboseEnabled());
		attributes.put("mktEnabled", getMktEnabled());
		attributes.put("emailNotificationsEnabled",
			getEmailNotificationsEnabled());
		attributes.put("dockbarNotificationsEnabled",
			getDockbarNotificationsEnabled());
		attributes.put("jmsEnabled", getJmsEnabled());
		attributes.put("brokerJMSusername", getBrokerJMSusername());
		attributes.put("brokerJMSpassword", getBrokerJMSpassword());
		attributes.put("brokerJMSurl", getBrokerJMSurl());
		attributes.put("jmsTopic", getJmsTopic());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long friendlyUrlSuffixID = (Long)attributes.get("friendlyUrlSuffixID");

		if (friendlyUrlSuffixID != null) {
			setFriendlyUrlSuffixID(friendlyUrlSuffixID);
		}

		String UrlSuffixImsHome = (String)attributes.get("UrlSuffixImsHome");

		if (UrlSuffixImsHome != null) {
			setUrlSuffixImsHome(UrlSuffixImsHome);
		}

		String UrlSuffixIdea = (String)attributes.get("UrlSuffixIdea");

		if (UrlSuffixIdea != null) {
			setUrlSuffixIdea(UrlSuffixIdea);
		}

		String UrlSuffixNeed = (String)attributes.get("UrlSuffixNeed");

		if (UrlSuffixNeed != null) {
			setUrlSuffixNeed(UrlSuffixNeed);
		}

		String UrlSuffixChallenge = (String)attributes.get("UrlSuffixChallenge");

		if (UrlSuffixChallenge != null) {
			setUrlSuffixChallenge(UrlSuffixChallenge);
		}

		String senderNotificheMailIdeario = (String)attributes.get(
				"senderNotificheMailIdeario");

		if (senderNotificheMailIdeario != null) {
			setSenderNotificheMailIdeario(senderNotificheMailIdeario);
		}

		String oggettoNotificheMailIdeario = (String)attributes.get(
				"oggettoNotificheMailIdeario");

		if (oggettoNotificheMailIdeario != null) {
			setOggettoNotificheMailIdeario(oggettoNotificheMailIdeario);
		}

		String firmaNotificheMailIdeario = (String)attributes.get(
				"firmaNotificheMailIdeario");

		if (firmaNotificheMailIdeario != null) {
			setFirmaNotificheMailIdeario(firmaNotificheMailIdeario);
		}

		String utenzaMail = (String)attributes.get("utenzaMail");

		if (utenzaMail != null) {
			setUtenzaMail(utenzaMail);
		}

		Boolean cdvEnabled = (Boolean)attributes.get("cdvEnabled");

		if (cdvEnabled != null) {
			setCdvEnabled(cdvEnabled);
		}

		String cdvAddress = (String)attributes.get("cdvAddress");

		if (cdvAddress != null) {
			setCdvAddress(cdvAddress);
		}

		Boolean vcEnabled = (Boolean)attributes.get("vcEnabled");

		if (vcEnabled != null) {
			setVcEnabled(vcEnabled);
		}

		String vcAddress = (String)attributes.get("vcAddress");

		if (vcAddress != null) {
			setVcAddress(vcAddress);
		}

		String vcWSAddress = (String)attributes.get("vcWSAddress");

		if (vcWSAddress != null) {
			setVcWSAddress(vcWSAddress);
		}

		Boolean deEnabled = (Boolean)attributes.get("deEnabled");

		if (deEnabled != null) {
			setDeEnabled(deEnabled);
		}

		String deAddress = (String)attributes.get("deAddress");

		if (deAddress != null) {
			setDeAddress(deAddress);
		}

		Boolean lbbEnabled = (Boolean)attributes.get("lbbEnabled");

		if (lbbEnabled != null) {
			setLbbEnabled(lbbEnabled);
		}

		String lbbAddress = (String)attributes.get("lbbAddress");

		if (lbbAddress != null) {
			setLbbAddress(lbbAddress);
		}

		String oiaAppId4lbb = (String)attributes.get("oiaAppId4lbb");

		if (oiaAppId4lbb != null) {
			setOiaAppId4lbb(oiaAppId4lbb);
		}

		Boolean tweetingEnabled = (Boolean)attributes.get("tweetingEnabled");

		if (tweetingEnabled != null) {
			setTweetingEnabled(tweetingEnabled);
		}

		String basicAuthUser = (String)attributes.get("basicAuthUser");

		if (basicAuthUser != null) {
			setBasicAuthUser(basicAuthUser);
		}

		String basicAuthPwd = (String)attributes.get("basicAuthPwd");

		if (basicAuthPwd != null) {
			setBasicAuthPwd(basicAuthPwd);
		}

		Boolean verboseEnabled = (Boolean)attributes.get("verboseEnabled");

		if (verboseEnabled != null) {
			setVerboseEnabled(verboseEnabled);
		}

		Boolean mktEnabled = (Boolean)attributes.get("mktEnabled");

		if (mktEnabled != null) {
			setMktEnabled(mktEnabled);
		}

		Boolean emailNotificationsEnabled = (Boolean)attributes.get(
				"emailNotificationsEnabled");

		if (emailNotificationsEnabled != null) {
			setEmailNotificationsEnabled(emailNotificationsEnabled);
		}

		Boolean dockbarNotificationsEnabled = (Boolean)attributes.get(
				"dockbarNotificationsEnabled");

		if (dockbarNotificationsEnabled != null) {
			setDockbarNotificationsEnabled(dockbarNotificationsEnabled);
		}

		Boolean jmsEnabled = (Boolean)attributes.get("jmsEnabled");

		if (jmsEnabled != null) {
			setJmsEnabled(jmsEnabled);
		}

		String brokerJMSusername = (String)attributes.get("brokerJMSusername");

		if (brokerJMSusername != null) {
			setBrokerJMSusername(brokerJMSusername);
		}

		String brokerJMSpassword = (String)attributes.get("brokerJMSpassword");

		if (brokerJMSpassword != null) {
			setBrokerJMSpassword(brokerJMSpassword);
		}

		String brokerJMSurl = (String)attributes.get("brokerJMSurl");

		if (brokerJMSurl != null) {
			setBrokerJMSurl(brokerJMSurl);
		}

		String jmsTopic = (String)attributes.get("jmsTopic");

		if (jmsTopic != null) {
			setJmsTopic(jmsTopic);
		}
	}

	/**
	* Returns the primary key of this c l s friendly url suffix.
	*
	* @return the primary key of this c l s friendly url suffix
	*/
	@Override
	public long getPrimaryKey() {
		return _clsFriendlyUrlSuffix.getPrimaryKey();
	}

	/**
	* Sets the primary key of this c l s friendly url suffix.
	*
	* @param primaryKey the primary key of this c l s friendly url suffix
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_clsFriendlyUrlSuffix.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the friendly url suffix i d of this c l s friendly url suffix.
	*
	* @return the friendly url suffix i d of this c l s friendly url suffix
	*/
	@Override
	public long getFriendlyUrlSuffixID() {
		return _clsFriendlyUrlSuffix.getFriendlyUrlSuffixID();
	}

	/**
	* Sets the friendly url suffix i d of this c l s friendly url suffix.
	*
	* @param friendlyUrlSuffixID the friendly url suffix i d of this c l s friendly url suffix
	*/
	@Override
	public void setFriendlyUrlSuffixID(long friendlyUrlSuffixID) {
		_clsFriendlyUrlSuffix.setFriendlyUrlSuffixID(friendlyUrlSuffixID);
	}

	/**
	* Returns the url suffix ims home of this c l s friendly url suffix.
	*
	* @return the url suffix ims home of this c l s friendly url suffix
	*/
	@Override
	public java.lang.String getUrlSuffixImsHome() {
		return _clsFriendlyUrlSuffix.getUrlSuffixImsHome();
	}

	/**
	* Sets the url suffix ims home of this c l s friendly url suffix.
	*
	* @param UrlSuffixImsHome the url suffix ims home of this c l s friendly url suffix
	*/
	@Override
	public void setUrlSuffixImsHome(java.lang.String UrlSuffixImsHome) {
		_clsFriendlyUrlSuffix.setUrlSuffixImsHome(UrlSuffixImsHome);
	}

	/**
	* Returns the url suffix idea of this c l s friendly url suffix.
	*
	* @return the url suffix idea of this c l s friendly url suffix
	*/
	@Override
	public java.lang.String getUrlSuffixIdea() {
		return _clsFriendlyUrlSuffix.getUrlSuffixIdea();
	}

	/**
	* Sets the url suffix idea of this c l s friendly url suffix.
	*
	* @param UrlSuffixIdea the url suffix idea of this c l s friendly url suffix
	*/
	@Override
	public void setUrlSuffixIdea(java.lang.String UrlSuffixIdea) {
		_clsFriendlyUrlSuffix.setUrlSuffixIdea(UrlSuffixIdea);
	}

	/**
	* Returns the url suffix need of this c l s friendly url suffix.
	*
	* @return the url suffix need of this c l s friendly url suffix
	*/
	@Override
	public java.lang.String getUrlSuffixNeed() {
		return _clsFriendlyUrlSuffix.getUrlSuffixNeed();
	}

	/**
	* Sets the url suffix need of this c l s friendly url suffix.
	*
	* @param UrlSuffixNeed the url suffix need of this c l s friendly url suffix
	*/
	@Override
	public void setUrlSuffixNeed(java.lang.String UrlSuffixNeed) {
		_clsFriendlyUrlSuffix.setUrlSuffixNeed(UrlSuffixNeed);
	}

	/**
	* Returns the url suffix challenge of this c l s friendly url suffix.
	*
	* @return the url suffix challenge of this c l s friendly url suffix
	*/
	@Override
	public java.lang.String getUrlSuffixChallenge() {
		return _clsFriendlyUrlSuffix.getUrlSuffixChallenge();
	}

	/**
	* Sets the url suffix challenge of this c l s friendly url suffix.
	*
	* @param UrlSuffixChallenge the url suffix challenge of this c l s friendly url suffix
	*/
	@Override
	public void setUrlSuffixChallenge(java.lang.String UrlSuffixChallenge) {
		_clsFriendlyUrlSuffix.setUrlSuffixChallenge(UrlSuffixChallenge);
	}

	/**
	* Returns the sender notifiche mail ideario of this c l s friendly url suffix.
	*
	* @return the sender notifiche mail ideario of this c l s friendly url suffix
	*/
	@Override
	public java.lang.String getSenderNotificheMailIdeario() {
		return _clsFriendlyUrlSuffix.getSenderNotificheMailIdeario();
	}

	/**
	* Sets the sender notifiche mail ideario of this c l s friendly url suffix.
	*
	* @param senderNotificheMailIdeario the sender notifiche mail ideario of this c l s friendly url suffix
	*/
	@Override
	public void setSenderNotificheMailIdeario(
		java.lang.String senderNotificheMailIdeario) {
		_clsFriendlyUrlSuffix.setSenderNotificheMailIdeario(senderNotificheMailIdeario);
	}

	/**
	* Returns the oggetto notifiche mail ideario of this c l s friendly url suffix.
	*
	* @return the oggetto notifiche mail ideario of this c l s friendly url suffix
	*/
	@Override
	public java.lang.String getOggettoNotificheMailIdeario() {
		return _clsFriendlyUrlSuffix.getOggettoNotificheMailIdeario();
	}

	/**
	* Sets the oggetto notifiche mail ideario of this c l s friendly url suffix.
	*
	* @param oggettoNotificheMailIdeario the oggetto notifiche mail ideario of this c l s friendly url suffix
	*/
	@Override
	public void setOggettoNotificheMailIdeario(
		java.lang.String oggettoNotificheMailIdeario) {
		_clsFriendlyUrlSuffix.setOggettoNotificheMailIdeario(oggettoNotificheMailIdeario);
	}

	/**
	* Returns the firma notifiche mail ideario of this c l s friendly url suffix.
	*
	* @return the firma notifiche mail ideario of this c l s friendly url suffix
	*/
	@Override
	public java.lang.String getFirmaNotificheMailIdeario() {
		return _clsFriendlyUrlSuffix.getFirmaNotificheMailIdeario();
	}

	/**
	* Sets the firma notifiche mail ideario of this c l s friendly url suffix.
	*
	* @param firmaNotificheMailIdeario the firma notifiche mail ideario of this c l s friendly url suffix
	*/
	@Override
	public void setFirmaNotificheMailIdeario(
		java.lang.String firmaNotificheMailIdeario) {
		_clsFriendlyUrlSuffix.setFirmaNotificheMailIdeario(firmaNotificheMailIdeario);
	}

	/**
	* Returns the utenza mail of this c l s friendly url suffix.
	*
	* @return the utenza mail of this c l s friendly url suffix
	*/
	@Override
	public java.lang.String getUtenzaMail() {
		return _clsFriendlyUrlSuffix.getUtenzaMail();
	}

	/**
	* Sets the utenza mail of this c l s friendly url suffix.
	*
	* @param utenzaMail the utenza mail of this c l s friendly url suffix
	*/
	@Override
	public void setUtenzaMail(java.lang.String utenzaMail) {
		_clsFriendlyUrlSuffix.setUtenzaMail(utenzaMail);
	}

	/**
	* Returns the cdv enabled of this c l s friendly url suffix.
	*
	* @return the cdv enabled of this c l s friendly url suffix
	*/
	@Override
	public boolean getCdvEnabled() {
		return _clsFriendlyUrlSuffix.getCdvEnabled();
	}

	/**
	* Returns <code>true</code> if this c l s friendly url suffix is cdv enabled.
	*
	* @return <code>true</code> if this c l s friendly url suffix is cdv enabled; <code>false</code> otherwise
	*/
	@Override
	public boolean isCdvEnabled() {
		return _clsFriendlyUrlSuffix.isCdvEnabled();
	}

	/**
	* Sets whether this c l s friendly url suffix is cdv enabled.
	*
	* @param cdvEnabled the cdv enabled of this c l s friendly url suffix
	*/
	@Override
	public void setCdvEnabled(boolean cdvEnabled) {
		_clsFriendlyUrlSuffix.setCdvEnabled(cdvEnabled);
	}

	/**
	* Returns the cdv address of this c l s friendly url suffix.
	*
	* @return the cdv address of this c l s friendly url suffix
	*/
	@Override
	public java.lang.String getCdvAddress() {
		return _clsFriendlyUrlSuffix.getCdvAddress();
	}

	/**
	* Sets the cdv address of this c l s friendly url suffix.
	*
	* @param cdvAddress the cdv address of this c l s friendly url suffix
	*/
	@Override
	public void setCdvAddress(java.lang.String cdvAddress) {
		_clsFriendlyUrlSuffix.setCdvAddress(cdvAddress);
	}

	/**
	* Returns the vc enabled of this c l s friendly url suffix.
	*
	* @return the vc enabled of this c l s friendly url suffix
	*/
	@Override
	public boolean getVcEnabled() {
		return _clsFriendlyUrlSuffix.getVcEnabled();
	}

	/**
	* Returns <code>true</code> if this c l s friendly url suffix is vc enabled.
	*
	* @return <code>true</code> if this c l s friendly url suffix is vc enabled; <code>false</code> otherwise
	*/
	@Override
	public boolean isVcEnabled() {
		return _clsFriendlyUrlSuffix.isVcEnabled();
	}

	/**
	* Sets whether this c l s friendly url suffix is vc enabled.
	*
	* @param vcEnabled the vc enabled of this c l s friendly url suffix
	*/
	@Override
	public void setVcEnabled(boolean vcEnabled) {
		_clsFriendlyUrlSuffix.setVcEnabled(vcEnabled);
	}

	/**
	* Returns the vc address of this c l s friendly url suffix.
	*
	* @return the vc address of this c l s friendly url suffix
	*/
	@Override
	public java.lang.String getVcAddress() {
		return _clsFriendlyUrlSuffix.getVcAddress();
	}

	/**
	* Sets the vc address of this c l s friendly url suffix.
	*
	* @param vcAddress the vc address of this c l s friendly url suffix
	*/
	@Override
	public void setVcAddress(java.lang.String vcAddress) {
		_clsFriendlyUrlSuffix.setVcAddress(vcAddress);
	}

	/**
	* Returns the vc w s address of this c l s friendly url suffix.
	*
	* @return the vc w s address of this c l s friendly url suffix
	*/
	@Override
	public java.lang.String getVcWSAddress() {
		return _clsFriendlyUrlSuffix.getVcWSAddress();
	}

	/**
	* Sets the vc w s address of this c l s friendly url suffix.
	*
	* @param vcWSAddress the vc w s address of this c l s friendly url suffix
	*/
	@Override
	public void setVcWSAddress(java.lang.String vcWSAddress) {
		_clsFriendlyUrlSuffix.setVcWSAddress(vcWSAddress);
	}

	/**
	* Returns the de enabled of this c l s friendly url suffix.
	*
	* @return the de enabled of this c l s friendly url suffix
	*/
	@Override
	public boolean getDeEnabled() {
		return _clsFriendlyUrlSuffix.getDeEnabled();
	}

	/**
	* Returns <code>true</code> if this c l s friendly url suffix is de enabled.
	*
	* @return <code>true</code> if this c l s friendly url suffix is de enabled; <code>false</code> otherwise
	*/
	@Override
	public boolean isDeEnabled() {
		return _clsFriendlyUrlSuffix.isDeEnabled();
	}

	/**
	* Sets whether this c l s friendly url suffix is de enabled.
	*
	* @param deEnabled the de enabled of this c l s friendly url suffix
	*/
	@Override
	public void setDeEnabled(boolean deEnabled) {
		_clsFriendlyUrlSuffix.setDeEnabled(deEnabled);
	}

	/**
	* Returns the de address of this c l s friendly url suffix.
	*
	* @return the de address of this c l s friendly url suffix
	*/
	@Override
	public java.lang.String getDeAddress() {
		return _clsFriendlyUrlSuffix.getDeAddress();
	}

	/**
	* Sets the de address of this c l s friendly url suffix.
	*
	* @param deAddress the de address of this c l s friendly url suffix
	*/
	@Override
	public void setDeAddress(java.lang.String deAddress) {
		_clsFriendlyUrlSuffix.setDeAddress(deAddress);
	}

	/**
	* Returns the lbb enabled of this c l s friendly url suffix.
	*
	* @return the lbb enabled of this c l s friendly url suffix
	*/
	@Override
	public boolean getLbbEnabled() {
		return _clsFriendlyUrlSuffix.getLbbEnabled();
	}

	/**
	* Returns <code>true</code> if this c l s friendly url suffix is lbb enabled.
	*
	* @return <code>true</code> if this c l s friendly url suffix is lbb enabled; <code>false</code> otherwise
	*/
	@Override
	public boolean isLbbEnabled() {
		return _clsFriendlyUrlSuffix.isLbbEnabled();
	}

	/**
	* Sets whether this c l s friendly url suffix is lbb enabled.
	*
	* @param lbbEnabled the lbb enabled of this c l s friendly url suffix
	*/
	@Override
	public void setLbbEnabled(boolean lbbEnabled) {
		_clsFriendlyUrlSuffix.setLbbEnabled(lbbEnabled);
	}

	/**
	* Returns the lbb address of this c l s friendly url suffix.
	*
	* @return the lbb address of this c l s friendly url suffix
	*/
	@Override
	public java.lang.String getLbbAddress() {
		return _clsFriendlyUrlSuffix.getLbbAddress();
	}

	/**
	* Sets the lbb address of this c l s friendly url suffix.
	*
	* @param lbbAddress the lbb address of this c l s friendly url suffix
	*/
	@Override
	public void setLbbAddress(java.lang.String lbbAddress) {
		_clsFriendlyUrlSuffix.setLbbAddress(lbbAddress);
	}

	/**
	* Returns the oia app id4lbb of this c l s friendly url suffix.
	*
	* @return the oia app id4lbb of this c l s friendly url suffix
	*/
	@Override
	public java.lang.String getOiaAppId4lbb() {
		return _clsFriendlyUrlSuffix.getOiaAppId4lbb();
	}

	/**
	* Sets the oia app id4lbb of this c l s friendly url suffix.
	*
	* @param oiaAppId4lbb the oia app id4lbb of this c l s friendly url suffix
	*/
	@Override
	public void setOiaAppId4lbb(java.lang.String oiaAppId4lbb) {
		_clsFriendlyUrlSuffix.setOiaAppId4lbb(oiaAppId4lbb);
	}

	/**
	* Returns the tweeting enabled of this c l s friendly url suffix.
	*
	* @return the tweeting enabled of this c l s friendly url suffix
	*/
	@Override
	public boolean getTweetingEnabled() {
		return _clsFriendlyUrlSuffix.getTweetingEnabled();
	}

	/**
	* Returns <code>true</code> if this c l s friendly url suffix is tweeting enabled.
	*
	* @return <code>true</code> if this c l s friendly url suffix is tweeting enabled; <code>false</code> otherwise
	*/
	@Override
	public boolean isTweetingEnabled() {
		return _clsFriendlyUrlSuffix.isTweetingEnabled();
	}

	/**
	* Sets whether this c l s friendly url suffix is tweeting enabled.
	*
	* @param tweetingEnabled the tweeting enabled of this c l s friendly url suffix
	*/
	@Override
	public void setTweetingEnabled(boolean tweetingEnabled) {
		_clsFriendlyUrlSuffix.setTweetingEnabled(tweetingEnabled);
	}

	/**
	* Returns the basic auth user of this c l s friendly url suffix.
	*
	* @return the basic auth user of this c l s friendly url suffix
	*/
	@Override
	public java.lang.String getBasicAuthUser() {
		return _clsFriendlyUrlSuffix.getBasicAuthUser();
	}

	/**
	* Sets the basic auth user of this c l s friendly url suffix.
	*
	* @param basicAuthUser the basic auth user of this c l s friendly url suffix
	*/
	@Override
	public void setBasicAuthUser(java.lang.String basicAuthUser) {
		_clsFriendlyUrlSuffix.setBasicAuthUser(basicAuthUser);
	}

	/**
	* Returns the basic auth pwd of this c l s friendly url suffix.
	*
	* @return the basic auth pwd of this c l s friendly url suffix
	*/
	@Override
	public java.lang.String getBasicAuthPwd() {
		return _clsFriendlyUrlSuffix.getBasicAuthPwd();
	}

	/**
	* Sets the basic auth pwd of this c l s friendly url suffix.
	*
	* @param basicAuthPwd the basic auth pwd of this c l s friendly url suffix
	*/
	@Override
	public void setBasicAuthPwd(java.lang.String basicAuthPwd) {
		_clsFriendlyUrlSuffix.setBasicAuthPwd(basicAuthPwd);
	}

	/**
	* Returns the verbose enabled of this c l s friendly url suffix.
	*
	* @return the verbose enabled of this c l s friendly url suffix
	*/
	@Override
	public boolean getVerboseEnabled() {
		return _clsFriendlyUrlSuffix.getVerboseEnabled();
	}

	/**
	* Returns <code>true</code> if this c l s friendly url suffix is verbose enabled.
	*
	* @return <code>true</code> if this c l s friendly url suffix is verbose enabled; <code>false</code> otherwise
	*/
	@Override
	public boolean isVerboseEnabled() {
		return _clsFriendlyUrlSuffix.isVerboseEnabled();
	}

	/**
	* Sets whether this c l s friendly url suffix is verbose enabled.
	*
	* @param verboseEnabled the verbose enabled of this c l s friendly url suffix
	*/
	@Override
	public void setVerboseEnabled(boolean verboseEnabled) {
		_clsFriendlyUrlSuffix.setVerboseEnabled(verboseEnabled);
	}

	/**
	* Returns the mkt enabled of this c l s friendly url suffix.
	*
	* @return the mkt enabled of this c l s friendly url suffix
	*/
	@Override
	public boolean getMktEnabled() {
		return _clsFriendlyUrlSuffix.getMktEnabled();
	}

	/**
	* Returns <code>true</code> if this c l s friendly url suffix is mkt enabled.
	*
	* @return <code>true</code> if this c l s friendly url suffix is mkt enabled; <code>false</code> otherwise
	*/
	@Override
	public boolean isMktEnabled() {
		return _clsFriendlyUrlSuffix.isMktEnabled();
	}

	/**
	* Sets whether this c l s friendly url suffix is mkt enabled.
	*
	* @param mktEnabled the mkt enabled of this c l s friendly url suffix
	*/
	@Override
	public void setMktEnabled(boolean mktEnabled) {
		_clsFriendlyUrlSuffix.setMktEnabled(mktEnabled);
	}

	/**
	* Returns the email notifications enabled of this c l s friendly url suffix.
	*
	* @return the email notifications enabled of this c l s friendly url suffix
	*/
	@Override
	public boolean getEmailNotificationsEnabled() {
		return _clsFriendlyUrlSuffix.getEmailNotificationsEnabled();
	}

	/**
	* Returns <code>true</code> if this c l s friendly url suffix is email notifications enabled.
	*
	* @return <code>true</code> if this c l s friendly url suffix is email notifications enabled; <code>false</code> otherwise
	*/
	@Override
	public boolean isEmailNotificationsEnabled() {
		return _clsFriendlyUrlSuffix.isEmailNotificationsEnabled();
	}

	/**
	* Sets whether this c l s friendly url suffix is email notifications enabled.
	*
	* @param emailNotificationsEnabled the email notifications enabled of this c l s friendly url suffix
	*/
	@Override
	public void setEmailNotificationsEnabled(boolean emailNotificationsEnabled) {
		_clsFriendlyUrlSuffix.setEmailNotificationsEnabled(emailNotificationsEnabled);
	}

	/**
	* Returns the dockbar notifications enabled of this c l s friendly url suffix.
	*
	* @return the dockbar notifications enabled of this c l s friendly url suffix
	*/
	@Override
	public boolean getDockbarNotificationsEnabled() {
		return _clsFriendlyUrlSuffix.getDockbarNotificationsEnabled();
	}

	/**
	* Returns <code>true</code> if this c l s friendly url suffix is dockbar notifications enabled.
	*
	* @return <code>true</code> if this c l s friendly url suffix is dockbar notifications enabled; <code>false</code> otherwise
	*/
	@Override
	public boolean isDockbarNotificationsEnabled() {
		return _clsFriendlyUrlSuffix.isDockbarNotificationsEnabled();
	}

	/**
	* Sets whether this c l s friendly url suffix is dockbar notifications enabled.
	*
	* @param dockbarNotificationsEnabled the dockbar notifications enabled of this c l s friendly url suffix
	*/
	@Override
	public void setDockbarNotificationsEnabled(
		boolean dockbarNotificationsEnabled) {
		_clsFriendlyUrlSuffix.setDockbarNotificationsEnabled(dockbarNotificationsEnabled);
	}

	/**
	* Returns the jms enabled of this c l s friendly url suffix.
	*
	* @return the jms enabled of this c l s friendly url suffix
	*/
	@Override
	public boolean getJmsEnabled() {
		return _clsFriendlyUrlSuffix.getJmsEnabled();
	}

	/**
	* Returns <code>true</code> if this c l s friendly url suffix is jms enabled.
	*
	* @return <code>true</code> if this c l s friendly url suffix is jms enabled; <code>false</code> otherwise
	*/
	@Override
	public boolean isJmsEnabled() {
		return _clsFriendlyUrlSuffix.isJmsEnabled();
	}

	/**
	* Sets whether this c l s friendly url suffix is jms enabled.
	*
	* @param jmsEnabled the jms enabled of this c l s friendly url suffix
	*/
	@Override
	public void setJmsEnabled(boolean jmsEnabled) {
		_clsFriendlyUrlSuffix.setJmsEnabled(jmsEnabled);
	}

	/**
	* Returns the broker j m susername of this c l s friendly url suffix.
	*
	* @return the broker j m susername of this c l s friendly url suffix
	*/
	@Override
	public java.lang.String getBrokerJMSusername() {
		return _clsFriendlyUrlSuffix.getBrokerJMSusername();
	}

	/**
	* Sets the broker j m susername of this c l s friendly url suffix.
	*
	* @param brokerJMSusername the broker j m susername of this c l s friendly url suffix
	*/
	@Override
	public void setBrokerJMSusername(java.lang.String brokerJMSusername) {
		_clsFriendlyUrlSuffix.setBrokerJMSusername(brokerJMSusername);
	}

	/**
	* Returns the broker j m spassword of this c l s friendly url suffix.
	*
	* @return the broker j m spassword of this c l s friendly url suffix
	*/
	@Override
	public java.lang.String getBrokerJMSpassword() {
		return _clsFriendlyUrlSuffix.getBrokerJMSpassword();
	}

	/**
	* Sets the broker j m spassword of this c l s friendly url suffix.
	*
	* @param brokerJMSpassword the broker j m spassword of this c l s friendly url suffix
	*/
	@Override
	public void setBrokerJMSpassword(java.lang.String brokerJMSpassword) {
		_clsFriendlyUrlSuffix.setBrokerJMSpassword(brokerJMSpassword);
	}

	/**
	* Returns the broker j m surl of this c l s friendly url suffix.
	*
	* @return the broker j m surl of this c l s friendly url suffix
	*/
	@Override
	public java.lang.String getBrokerJMSurl() {
		return _clsFriendlyUrlSuffix.getBrokerJMSurl();
	}

	/**
	* Sets the broker j m surl of this c l s friendly url suffix.
	*
	* @param brokerJMSurl the broker j m surl of this c l s friendly url suffix
	*/
	@Override
	public void setBrokerJMSurl(java.lang.String brokerJMSurl) {
		_clsFriendlyUrlSuffix.setBrokerJMSurl(brokerJMSurl);
	}

	/**
	* Returns the jms topic of this c l s friendly url suffix.
	*
	* @return the jms topic of this c l s friendly url suffix
	*/
	@Override
	public java.lang.String getJmsTopic() {
		return _clsFriendlyUrlSuffix.getJmsTopic();
	}

	/**
	* Sets the jms topic of this c l s friendly url suffix.
	*
	* @param jmsTopic the jms topic of this c l s friendly url suffix
	*/
	@Override
	public void setJmsTopic(java.lang.String jmsTopic) {
		_clsFriendlyUrlSuffix.setJmsTopic(jmsTopic);
	}

	@Override
	public boolean isNew() {
		return _clsFriendlyUrlSuffix.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_clsFriendlyUrlSuffix.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _clsFriendlyUrlSuffix.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_clsFriendlyUrlSuffix.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _clsFriendlyUrlSuffix.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _clsFriendlyUrlSuffix.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_clsFriendlyUrlSuffix.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _clsFriendlyUrlSuffix.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_clsFriendlyUrlSuffix.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_clsFriendlyUrlSuffix.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_clsFriendlyUrlSuffix.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CLSFriendlyUrlSuffixWrapper((CLSFriendlyUrlSuffix)_clsFriendlyUrlSuffix.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSFriendlyUrlSuffix clsFriendlyUrlSuffix) {
		return _clsFriendlyUrlSuffix.compareTo(clsFriendlyUrlSuffix);
	}

	@Override
	public int hashCode() {
		return _clsFriendlyUrlSuffix.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFriendlyUrlSuffix> toCacheModel() {
		return _clsFriendlyUrlSuffix.toCacheModel();
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFriendlyUrlSuffix toEscapedModel() {
		return new CLSFriendlyUrlSuffixWrapper(_clsFriendlyUrlSuffix.toEscapedModel());
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFriendlyUrlSuffix toUnescapedModel() {
		return new CLSFriendlyUrlSuffixWrapper(_clsFriendlyUrlSuffix.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _clsFriendlyUrlSuffix.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _clsFriendlyUrlSuffix.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_clsFriendlyUrlSuffix.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSFriendlyUrlSuffixWrapper)) {
			return false;
		}

		CLSFriendlyUrlSuffixWrapper clsFriendlyUrlSuffixWrapper = (CLSFriendlyUrlSuffixWrapper)obj;

		if (Validator.equals(_clsFriendlyUrlSuffix,
					clsFriendlyUrlSuffixWrapper._clsFriendlyUrlSuffix)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public CLSFriendlyUrlSuffix getWrappedCLSFriendlyUrlSuffix() {
		return _clsFriendlyUrlSuffix;
	}

	@Override
	public CLSFriendlyUrlSuffix getWrappedModel() {
		return _clsFriendlyUrlSuffix;
	}

	@Override
	public void resetOriginalValues() {
		_clsFriendlyUrlSuffix.resetOriginalValues();
	}

	private CLSFriendlyUrlSuffix _clsFriendlyUrlSuffix;
}