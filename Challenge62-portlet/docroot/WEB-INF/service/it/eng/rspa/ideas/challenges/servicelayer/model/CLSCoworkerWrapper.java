/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CLSCoworker}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSCoworker
 * @generated
 */
public class CLSCoworkerWrapper implements CLSCoworker,
	ModelWrapper<CLSCoworker> {
	public CLSCoworkerWrapper(CLSCoworker clsCoworker) {
		_clsCoworker = clsCoworker;
	}

	@Override
	public Class<?> getModelClass() {
		return CLSCoworker.class;
	}

	@Override
	public String getModelClassName() {
		return CLSCoworker.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ideaID", getIdeaID());
		attributes.put("coworkerId", getCoworkerId());
		attributes.put("userId", getUserId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ideaID = (Long)attributes.get("ideaID");

		if (ideaID != null) {
			setIdeaID(ideaID);
		}

		Long coworkerId = (Long)attributes.get("coworkerId");

		if (coworkerId != null) {
			setCoworkerId(coworkerId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}
	}

	/**
	* Returns the primary key of this c l s coworker.
	*
	* @return the primary key of this c l s coworker
	*/
	@Override
	public long getPrimaryKey() {
		return _clsCoworker.getPrimaryKey();
	}

	/**
	* Sets the primary key of this c l s coworker.
	*
	* @param primaryKey the primary key of this c l s coworker
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_clsCoworker.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the idea i d of this c l s coworker.
	*
	* @return the idea i d of this c l s coworker
	*/
	@Override
	public long getIdeaID() {
		return _clsCoworker.getIdeaID();
	}

	/**
	* Sets the idea i d of this c l s coworker.
	*
	* @param ideaID the idea i d of this c l s coworker
	*/
	@Override
	public void setIdeaID(long ideaID) {
		_clsCoworker.setIdeaID(ideaID);
	}

	/**
	* Returns the coworker ID of this c l s coworker.
	*
	* @return the coworker ID of this c l s coworker
	*/
	@Override
	public long getCoworkerId() {
		return _clsCoworker.getCoworkerId();
	}

	/**
	* Sets the coworker ID of this c l s coworker.
	*
	* @param coworkerId the coworker ID of this c l s coworker
	*/
	@Override
	public void setCoworkerId(long coworkerId) {
		_clsCoworker.setCoworkerId(coworkerId);
	}

	/**
	* Returns the user ID of this c l s coworker.
	*
	* @return the user ID of this c l s coworker
	*/
	@Override
	public long getUserId() {
		return _clsCoworker.getUserId();
	}

	/**
	* Sets the user ID of this c l s coworker.
	*
	* @param userId the user ID of this c l s coworker
	*/
	@Override
	public void setUserId(long userId) {
		_clsCoworker.setUserId(userId);
	}

	/**
	* Returns the user uuid of this c l s coworker.
	*
	* @return the user uuid of this c l s coworker
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsCoworker.getUserUuid();
	}

	/**
	* Sets the user uuid of this c l s coworker.
	*
	* @param userUuid the user uuid of this c l s coworker
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_clsCoworker.setUserUuid(userUuid);
	}

	@Override
	public boolean isNew() {
		return _clsCoworker.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_clsCoworker.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _clsCoworker.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_clsCoworker.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _clsCoworker.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _clsCoworker.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_clsCoworker.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _clsCoworker.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_clsCoworker.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_clsCoworker.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_clsCoworker.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CLSCoworkerWrapper((CLSCoworker)_clsCoworker.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker clsCoworker) {
		return _clsCoworker.compareTo(clsCoworker);
	}

	@Override
	public int hashCode() {
		return _clsCoworker.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker> toCacheModel() {
		return _clsCoworker.toCacheModel();
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker toEscapedModel() {
		return new CLSCoworkerWrapper(_clsCoworker.toEscapedModel());
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker toUnescapedModel() {
		return new CLSCoworkerWrapper(_clsCoworker.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _clsCoworker.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _clsCoworker.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_clsCoworker.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSCoworkerWrapper)) {
			return false;
		}

		CLSCoworkerWrapper clsCoworkerWrapper = (CLSCoworkerWrapper)obj;

		if (Validator.equals(_clsCoworker, clsCoworkerWrapper._clsCoworker)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public CLSCoworker getWrappedCLSCoworker() {
		return _clsCoworker;
	}

	@Override
	public CLSCoworker getWrappedModel() {
		return _clsCoworker;
	}

	@Override
	public void resetOriginalValues() {
		_clsCoworker.resetOriginalValues();
	}

	private CLSCoworker _clsCoworker;
}