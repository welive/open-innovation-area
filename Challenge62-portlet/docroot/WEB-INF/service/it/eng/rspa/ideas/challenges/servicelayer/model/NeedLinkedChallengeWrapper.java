/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link NeedLinkedChallenge}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see NeedLinkedChallenge
 * @generated
 */
public class NeedLinkedChallengeWrapper implements NeedLinkedChallenge,
	ModelWrapper<NeedLinkedChallenge> {
	public NeedLinkedChallengeWrapper(NeedLinkedChallenge needLinkedChallenge) {
		_needLinkedChallenge = needLinkedChallenge;
	}

	@Override
	public Class<?> getModelClass() {
		return NeedLinkedChallenge.class;
	}

	@Override
	public String getModelClassName() {
		return NeedLinkedChallenge.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("needId", getNeedId());
		attributes.put("challengeId", getChallengeId());
		attributes.put("date", getDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long needId = (Long)attributes.get("needId");

		if (needId != null) {
			setNeedId(needId);
		}

		Long challengeId = (Long)attributes.get("challengeId");

		if (challengeId != null) {
			setChallengeId(challengeId);
		}

		Date date = (Date)attributes.get("date");

		if (date != null) {
			setDate(date);
		}
	}

	/**
	* Returns the primary key of this need linked challenge.
	*
	* @return the primary key of this need linked challenge
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.service.persistence.NeedLinkedChallengePK getPrimaryKey() {
		return _needLinkedChallenge.getPrimaryKey();
	}

	/**
	* Sets the primary key of this need linked challenge.
	*
	* @param primaryKey the primary key of this need linked challenge
	*/
	@Override
	public void setPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.NeedLinkedChallengePK primaryKey) {
		_needLinkedChallenge.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the need ID of this need linked challenge.
	*
	* @return the need ID of this need linked challenge
	*/
	@Override
	public long getNeedId() {
		return _needLinkedChallenge.getNeedId();
	}

	/**
	* Sets the need ID of this need linked challenge.
	*
	* @param needId the need ID of this need linked challenge
	*/
	@Override
	public void setNeedId(long needId) {
		_needLinkedChallenge.setNeedId(needId);
	}

	/**
	* Returns the challenge ID of this need linked challenge.
	*
	* @return the challenge ID of this need linked challenge
	*/
	@Override
	public long getChallengeId() {
		return _needLinkedChallenge.getChallengeId();
	}

	/**
	* Sets the challenge ID of this need linked challenge.
	*
	* @param challengeId the challenge ID of this need linked challenge
	*/
	@Override
	public void setChallengeId(long challengeId) {
		_needLinkedChallenge.setChallengeId(challengeId);
	}

	/**
	* Returns the date of this need linked challenge.
	*
	* @return the date of this need linked challenge
	*/
	@Override
	public java.util.Date getDate() {
		return _needLinkedChallenge.getDate();
	}

	/**
	* Sets the date of this need linked challenge.
	*
	* @param date the date of this need linked challenge
	*/
	@Override
	public void setDate(java.util.Date date) {
		_needLinkedChallenge.setDate(date);
	}

	@Override
	public boolean isNew() {
		return _needLinkedChallenge.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_needLinkedChallenge.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _needLinkedChallenge.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_needLinkedChallenge.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _needLinkedChallenge.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _needLinkedChallenge.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_needLinkedChallenge.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _needLinkedChallenge.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_needLinkedChallenge.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_needLinkedChallenge.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_needLinkedChallenge.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new NeedLinkedChallengeWrapper((NeedLinkedChallenge)_needLinkedChallenge.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge needLinkedChallenge) {
		return _needLinkedChallenge.compareTo(needLinkedChallenge);
	}

	@Override
	public int hashCode() {
		return _needLinkedChallenge.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge> toCacheModel() {
		return _needLinkedChallenge.toCacheModel();
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge toEscapedModel() {
		return new NeedLinkedChallengeWrapper(_needLinkedChallenge.toEscapedModel());
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge toUnescapedModel() {
		return new NeedLinkedChallengeWrapper(_needLinkedChallenge.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _needLinkedChallenge.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _needLinkedChallenge.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_needLinkedChallenge.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof NeedLinkedChallengeWrapper)) {
			return false;
		}

		NeedLinkedChallengeWrapper needLinkedChallengeWrapper = (NeedLinkedChallengeWrapper)obj;

		if (Validator.equals(_needLinkedChallenge,
					needLinkedChallengeWrapper._needLinkedChallenge)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public NeedLinkedChallenge getWrappedNeedLinkedChallenge() {
		return _needLinkedChallenge;
	}

	@Override
	public NeedLinkedChallenge getWrappedModel() {
		return _needLinkedChallenge;
	}

	@Override
	public void resetOriginalValues() {
		_needLinkedChallenge.resetOriginalValues();
	}

	private NeedLinkedChallenge _needLinkedChallenge;
}