/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CLSFavouriteChallengesService}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSFavouriteChallengesService
 * @generated
 */
public class CLSFavouriteChallengesServiceWrapper
	implements CLSFavouriteChallengesService,
		ServiceWrapper<CLSFavouriteChallengesService> {
	public CLSFavouriteChallengesServiceWrapper(
		CLSFavouriteChallengesService clsFavouriteChallengesService) {
		_clsFavouriteChallengesService = clsFavouriteChallengesService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _clsFavouriteChallengesService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_clsFavouriteChallengesService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _clsFavouriteChallengesService.invokeMethod(name,
			parameterTypes, arguments);
	}

	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges> getFavouriteChallenges(
		java.lang.Long favChallengeId, java.lang.Long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesService.getFavouriteChallenges(favChallengeId,
			userId);
	}

	@Override
	public boolean removeFavouriteChallenge(java.lang.Long favChallengeId,
		java.lang.Long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesService.removeFavouriteChallenge(favChallengeId,
			userId);
	}

	@Override
	public boolean addFavouriteChallenge(java.lang.Long favChallengeId,
		java.lang.Long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesService.addFavouriteChallenge(favChallengeId,
			userId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CLSFavouriteChallengesService getWrappedCLSFavouriteChallengesService() {
		return _clsFavouriteChallengesService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCLSFavouriteChallengesService(
		CLSFavouriteChallengesService clsFavouriteChallengesService) {
		_clsFavouriteChallengesService = clsFavouriteChallengesService;
	}

	@Override
	public CLSFavouriteChallengesService getWrappedService() {
		return _clsFavouriteChallengesService;
	}

	@Override
	public void setWrappedService(
		CLSFavouriteChallengesService clsFavouriteChallengesService) {
		_clsFavouriteChallengesService = clsFavouriteChallengesService;
	}

	private CLSFavouriteChallengesService _clsFavouriteChallengesService;
}