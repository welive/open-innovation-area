/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.NeedLinkedChallengePK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.eng.rspa.ideas.challenges.servicelayer.service.http.NeedLinkedChallengeServiceSoap}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.http.NeedLinkedChallengeServiceSoap
 * @generated
 */
public class NeedLinkedChallengeSoap implements Serializable {
	public static NeedLinkedChallengeSoap toSoapModel(NeedLinkedChallenge model) {
		NeedLinkedChallengeSoap soapModel = new NeedLinkedChallengeSoap();

		soapModel.setNeedId(model.getNeedId());
		soapModel.setChallengeId(model.getChallengeId());
		soapModel.setDate(model.getDate());

		return soapModel;
	}

	public static NeedLinkedChallengeSoap[] toSoapModels(
		NeedLinkedChallenge[] models) {
		NeedLinkedChallengeSoap[] soapModels = new NeedLinkedChallengeSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static NeedLinkedChallengeSoap[][] toSoapModels(
		NeedLinkedChallenge[][] models) {
		NeedLinkedChallengeSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new NeedLinkedChallengeSoap[models.length][models[0].length];
		}
		else {
			soapModels = new NeedLinkedChallengeSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static NeedLinkedChallengeSoap[] toSoapModels(
		List<NeedLinkedChallenge> models) {
		List<NeedLinkedChallengeSoap> soapModels = new ArrayList<NeedLinkedChallengeSoap>(models.size());

		for (NeedLinkedChallenge model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new NeedLinkedChallengeSoap[soapModels.size()]);
	}

	public NeedLinkedChallengeSoap() {
	}

	public NeedLinkedChallengePK getPrimaryKey() {
		return new NeedLinkedChallengePK(_needId, _challengeId);
	}

	public void setPrimaryKey(NeedLinkedChallengePK pk) {
		setNeedId(pk.needId);
		setChallengeId(pk.challengeId);
	}

	public long getNeedId() {
		return _needId;
	}

	public void setNeedId(long needId) {
		_needId = needId;
	}

	public long getChallengeId() {
		return _challengeId;
	}

	public void setChallengeId(long challengeId) {
		_challengeId = challengeId;
	}

	public Date getDate() {
		return _date;
	}

	public void setDate(Date date) {
		_date = date;
	}

	private long _needId;
	private long _challengeId;
	private Date _date;
}