/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSFavouriteChallengesServiceSoap}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSFavouriteChallengesServiceSoap
 * @generated
 */
public class CLSFavouriteChallengesSoap implements Serializable {
	public static CLSFavouriteChallengesSoap toSoapModel(
		CLSFavouriteChallenges model) {
		CLSFavouriteChallengesSoap soapModel = new CLSFavouriteChallengesSoap();

		soapModel.setChallengeId(model.getChallengeId());
		soapModel.setUserId(model.getUserId());

		return soapModel;
	}

	public static CLSFavouriteChallengesSoap[] toSoapModels(
		CLSFavouriteChallenges[] models) {
		CLSFavouriteChallengesSoap[] soapModels = new CLSFavouriteChallengesSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CLSFavouriteChallengesSoap[][] toSoapModels(
		CLSFavouriteChallenges[][] models) {
		CLSFavouriteChallengesSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CLSFavouriteChallengesSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CLSFavouriteChallengesSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CLSFavouriteChallengesSoap[] toSoapModels(
		List<CLSFavouriteChallenges> models) {
		List<CLSFavouriteChallengesSoap> soapModels = new ArrayList<CLSFavouriteChallengesSoap>(models.size());

		for (CLSFavouriteChallenges model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CLSFavouriteChallengesSoap[soapModels.size()]);
	}

	public CLSFavouriteChallengesSoap() {
	}

	public CLSFavouriteChallengesPK getPrimaryKey() {
		return new CLSFavouriteChallengesPK(_challengeId, _userId);
	}

	public void setPrimaryKey(CLSFavouriteChallengesPK pk) {
		setChallengeId(pk.challengeId);
		setUserId(pk.userId);
	}

	public long getChallengeId() {
		return _challengeId;
	}

	public void setChallengeId(long challengeId) {
		_challengeId = challengeId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	private long _challengeId;
	private long _userId;
}