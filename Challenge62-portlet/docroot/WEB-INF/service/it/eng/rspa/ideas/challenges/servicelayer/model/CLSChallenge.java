/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Accessor;
import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the CLSChallenge service. Represents a row in the &quot;CSL_CLSChallenge&quot; database table, with each column mapped to a property of this class.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSChallengeModel
 * @see it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengeImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengeModelImpl
 * @generated
 */
public interface CLSChallenge extends CLSChallengeModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengeImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<CLSChallenge, String> UUID_ACCESSOR = new Accessor<CLSChallenge, String>() {
			@Override
			public String get(CLSChallenge clsChallenge) {
				return clsChallenge.getUuid();
			}
		};
}