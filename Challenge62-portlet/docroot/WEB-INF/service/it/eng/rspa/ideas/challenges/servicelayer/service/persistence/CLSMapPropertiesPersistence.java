/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties;

/**
 * The persistence interface for the c l s map properties service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSMapPropertiesPersistenceImpl
 * @see CLSMapPropertiesUtil
 * @generated
 */
public interface CLSMapPropertiesPersistence extends BasePersistence<CLSMapProperties> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CLSMapPropertiesUtil} to access the c l s map properties persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the c l s map properties in the entity cache if it is enabled.
	*
	* @param clsMapProperties the c l s map properties
	*/
	public void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties clsMapProperties);

	/**
	* Caches the c l s map propertieses in the entity cache if it is enabled.
	*
	* @param clsMapPropertieses the c l s map propertieses
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties> clsMapPropertieses);

	/**
	* Creates a new c l s map properties with the primary key. Does not add the c l s map properties to the database.
	*
	* @param mapPropertiesId the primary key for the new c l s map properties
	* @return the new c l s map properties
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties create(
		long mapPropertiesId);

	/**
	* Removes the c l s map properties with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param mapPropertiesId the primary key of the c l s map properties
	* @return the c l s map properties that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSMapPropertiesException if a c l s map properties with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties remove(
		long mapPropertiesId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSMapPropertiesException;

	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties clsMapProperties)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the c l s map properties with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSMapPropertiesException} if it could not be found.
	*
	* @param mapPropertiesId the primary key of the c l s map properties
	* @return the c l s map properties
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSMapPropertiesException if a c l s map properties with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties findByPrimaryKey(
		long mapPropertiesId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSMapPropertiesException;

	/**
	* Returns the c l s map properties with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param mapPropertiesId the primary key of the c l s map properties
	* @return the c l s map properties, or <code>null</code> if a c l s map properties with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties fetchByPrimaryKey(
		long mapPropertiesId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the c l s map propertieses.
	*
	* @return the c l s map propertieses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the c l s map propertieses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSMapPropertiesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s map propertieses
	* @param end the upper bound of the range of c l s map propertieses (not inclusive)
	* @return the range of c l s map propertieses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the c l s map propertieses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSMapPropertiesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s map propertieses
	* @param end the upper bound of the range of c l s map propertieses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of c l s map propertieses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the c l s map propertieses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of c l s map propertieses.
	*
	* @return the number of c l s map propertieses
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}