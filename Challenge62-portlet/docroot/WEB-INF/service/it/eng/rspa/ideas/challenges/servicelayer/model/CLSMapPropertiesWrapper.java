/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CLSMapProperties}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSMapProperties
 * @generated
 */
public class CLSMapPropertiesWrapper implements CLSMapProperties,
	ModelWrapper<CLSMapProperties> {
	public CLSMapPropertiesWrapper(CLSMapProperties clsMapProperties) {
		_clsMapProperties = clsMapProperties;
	}

	@Override
	public Class<?> getModelClass() {
		return CLSMapProperties.class;
	}

	@Override
	public String getModelClassName() {
		return CLSMapProperties.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("mapPropertiesId", getMapPropertiesId());
		attributes.put("mapCenterLatitude", getMapCenterLatitude());
		attributes.put("mapCenterLongitude", getMapCenterLongitude());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long mapPropertiesId = (Long)attributes.get("mapPropertiesId");

		if (mapPropertiesId != null) {
			setMapPropertiesId(mapPropertiesId);
		}

		String mapCenterLatitude = (String)attributes.get("mapCenterLatitude");

		if (mapCenterLatitude != null) {
			setMapCenterLatitude(mapCenterLatitude);
		}

		String mapCenterLongitude = (String)attributes.get("mapCenterLongitude");

		if (mapCenterLongitude != null) {
			setMapCenterLongitude(mapCenterLongitude);
		}
	}

	/**
	* Returns the primary key of this c l s map properties.
	*
	* @return the primary key of this c l s map properties
	*/
	@Override
	public long getPrimaryKey() {
		return _clsMapProperties.getPrimaryKey();
	}

	/**
	* Sets the primary key of this c l s map properties.
	*
	* @param primaryKey the primary key of this c l s map properties
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_clsMapProperties.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the map properties ID of this c l s map properties.
	*
	* @return the map properties ID of this c l s map properties
	*/
	@Override
	public long getMapPropertiesId() {
		return _clsMapProperties.getMapPropertiesId();
	}

	/**
	* Sets the map properties ID of this c l s map properties.
	*
	* @param mapPropertiesId the map properties ID of this c l s map properties
	*/
	@Override
	public void setMapPropertiesId(long mapPropertiesId) {
		_clsMapProperties.setMapPropertiesId(mapPropertiesId);
	}

	/**
	* Returns the map center latitude of this c l s map properties.
	*
	* @return the map center latitude of this c l s map properties
	*/
	@Override
	public java.lang.String getMapCenterLatitude() {
		return _clsMapProperties.getMapCenterLatitude();
	}

	/**
	* Sets the map center latitude of this c l s map properties.
	*
	* @param mapCenterLatitude the map center latitude of this c l s map properties
	*/
	@Override
	public void setMapCenterLatitude(java.lang.String mapCenterLatitude) {
		_clsMapProperties.setMapCenterLatitude(mapCenterLatitude);
	}

	/**
	* Returns the map center longitude of this c l s map properties.
	*
	* @return the map center longitude of this c l s map properties
	*/
	@Override
	public java.lang.String getMapCenterLongitude() {
		return _clsMapProperties.getMapCenterLongitude();
	}

	/**
	* Sets the map center longitude of this c l s map properties.
	*
	* @param mapCenterLongitude the map center longitude of this c l s map properties
	*/
	@Override
	public void setMapCenterLongitude(java.lang.String mapCenterLongitude) {
		_clsMapProperties.setMapCenterLongitude(mapCenterLongitude);
	}

	@Override
	public boolean isNew() {
		return _clsMapProperties.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_clsMapProperties.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _clsMapProperties.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_clsMapProperties.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _clsMapProperties.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _clsMapProperties.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_clsMapProperties.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _clsMapProperties.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_clsMapProperties.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_clsMapProperties.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_clsMapProperties.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CLSMapPropertiesWrapper((CLSMapProperties)_clsMapProperties.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties clsMapProperties) {
		return _clsMapProperties.compareTo(clsMapProperties);
	}

	@Override
	public int hashCode() {
		return _clsMapProperties.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties> toCacheModel() {
		return _clsMapProperties.toCacheModel();
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties toEscapedModel() {
		return new CLSMapPropertiesWrapper(_clsMapProperties.toEscapedModel());
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties toUnescapedModel() {
		return new CLSMapPropertiesWrapper(_clsMapProperties.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _clsMapProperties.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _clsMapProperties.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_clsMapProperties.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSMapPropertiesWrapper)) {
			return false;
		}

		CLSMapPropertiesWrapper clsMapPropertiesWrapper = (CLSMapPropertiesWrapper)obj;

		if (Validator.equals(_clsMapProperties,
					clsMapPropertiesWrapper._clsMapProperties)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public CLSMapProperties getWrappedCLSMapProperties() {
		return _clsMapProperties;
	}

	@Override
	public CLSMapProperties getWrappedModel() {
		return _clsMapProperties;
	}

	@Override
	public void resetOriginalValues() {
		_clsMapProperties.resetOriginalValues();
	}

	private CLSMapProperties _clsMapProperties;
}