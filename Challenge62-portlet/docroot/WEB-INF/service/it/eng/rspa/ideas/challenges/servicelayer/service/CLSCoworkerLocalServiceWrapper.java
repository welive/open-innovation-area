/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CLSCoworkerLocalService}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSCoworkerLocalService
 * @generated
 */
public class CLSCoworkerLocalServiceWrapper implements CLSCoworkerLocalService,
	ServiceWrapper<CLSCoworkerLocalService> {
	public CLSCoworkerLocalServiceWrapper(
		CLSCoworkerLocalService clsCoworkerLocalService) {
		_clsCoworkerLocalService = clsCoworkerLocalService;
	}

	/**
	* Adds the c l s coworker to the database. Also notifies the appropriate model listeners.
	*
	* @param clsCoworker the c l s coworker
	* @return the c l s coworker that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker addCLSCoworker(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker clsCoworker)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsCoworkerLocalService.addCLSCoworker(clsCoworker);
	}

	/**
	* Creates a new c l s coworker with the primary key. Does not add the c l s coworker to the database.
	*
	* @param coworkerId the primary key for the new c l s coworker
	* @return the new c l s coworker
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker createCLSCoworker(
		long coworkerId) {
		return _clsCoworkerLocalService.createCLSCoworker(coworkerId);
	}

	/**
	* Deletes the c l s coworker with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param coworkerId the primary key of the c l s coworker
	* @return the c l s coworker that was removed
	* @throws PortalException if a c l s coworker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker deleteCLSCoworker(
		long coworkerId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _clsCoworkerLocalService.deleteCLSCoworker(coworkerId);
	}

	/**
	* Deletes the c l s coworker from the database. Also notifies the appropriate model listeners.
	*
	* @param clsCoworker the c l s coworker
	* @return the c l s coworker that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker deleteCLSCoworker(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker clsCoworker)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsCoworkerLocalService.deleteCLSCoworker(clsCoworker);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _clsCoworkerLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsCoworkerLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCoworkerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsCoworkerLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCoworkerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsCoworkerLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsCoworkerLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsCoworkerLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker fetchCLSCoworker(
		long coworkerId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsCoworkerLocalService.fetchCLSCoworker(coworkerId);
	}

	/**
	* Returns the c l s coworker with the primary key.
	*
	* @param coworkerId the primary key of the c l s coworker
	* @return the c l s coworker
	* @throws PortalException if a c l s coworker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker getCLSCoworker(
		long coworkerId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _clsCoworkerLocalService.getCLSCoworker(coworkerId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _clsCoworkerLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the c l s coworkers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCoworkerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s coworkers
	* @param end the upper bound of the range of c l s coworkers (not inclusive)
	* @return the range of c l s coworkers
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker> getCLSCoworkers(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsCoworkerLocalService.getCLSCoworkers(start, end);
	}

	/**
	* Returns the number of c l s coworkers.
	*
	* @return the number of c l s coworkers
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getCLSCoworkersCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsCoworkerLocalService.getCLSCoworkersCount();
	}

	/**
	* Updates the c l s coworker in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param clsCoworker the c l s coworker
	* @return the c l s coworker that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker updateCLSCoworker(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker clsCoworker)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsCoworkerLocalService.updateCLSCoworker(clsCoworker);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _clsCoworkerLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_clsCoworkerLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _clsCoworkerLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public void removeCoworkerOnIdeaDelete(long ideaId) {
		_clsCoworkerLocalService.removeCoworkerOnIdeaDelete(ideaId);
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker getCoworkerByIdeaIdAndUserId(
		long ideaId, long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsCoworkerLocalService.getCoworkerByIdeaIdAndUserId(ideaId,
			userId);
	}

	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker> getCoworkersByIdeaId(
		long ideaId) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsCoworkerLocalService.getCoworkersByIdeaId(ideaId);
	}

	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker> getCoworkersByUserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsCoworkerLocalService.getCoworkersByUserId(userId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CLSCoworkerLocalService getWrappedCLSCoworkerLocalService() {
		return _clsCoworkerLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCLSCoworkerLocalService(
		CLSCoworkerLocalService clsCoworkerLocalService) {
		_clsCoworkerLocalService = clsCoworkerLocalService;
	}

	@Override
	public CLSCoworkerLocalService getWrappedService() {
		return _clsCoworkerLocalService;
	}

	@Override
	public void setWrappedService(
		CLSCoworkerLocalService clsCoworkerLocalService) {
		_clsCoworkerLocalService = clsCoworkerLocalService;
	}

	private CLSCoworkerLocalService _clsCoworkerLocalService;
}