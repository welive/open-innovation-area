/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CLSIdeaLocalService}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSIdeaLocalService
 * @generated
 */
public class CLSIdeaLocalServiceWrapper implements CLSIdeaLocalService,
	ServiceWrapper<CLSIdeaLocalService> {
	public CLSIdeaLocalServiceWrapper(CLSIdeaLocalService clsIdeaLocalService) {
		_clsIdeaLocalService = clsIdeaLocalService;
	}

	/**
	* Adds the c l s idea to the database. Also notifies the appropriate model listeners.
	*
	* @param clsIdea the c l s idea
	* @return the c l s idea that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea addCLSIdea(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea clsIdea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.addCLSIdea(clsIdea);
	}

	/**
	* Creates a new c l s idea with the primary key. Does not add the c l s idea to the database.
	*
	* @param ideaID the primary key for the new c l s idea
	* @return the new c l s idea
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea createCLSIdea(
		long ideaID) {
		return _clsIdeaLocalService.createCLSIdea(ideaID);
	}

	/**
	* Deletes the c l s idea with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ideaID the primary key of the c l s idea
	* @return the c l s idea that was removed
	* @throws PortalException if a c l s idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea deleteCLSIdea(
		long ideaID)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.deleteCLSIdea(ideaID);
	}

	/**
	* Deletes the c l s idea from the database. Also notifies the appropriate model listeners.
	*
	* @param clsIdea the c l s idea
	* @return the c l s idea that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea deleteCLSIdea(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea clsIdea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.deleteCLSIdea(clsIdea);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _clsIdeaLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchCLSIdea(
		long ideaID) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.fetchCLSIdea(ideaID);
	}

	/**
	* Returns the c l s idea with the matching UUID and company.
	*
	* @param uuid the c l s idea's UUID
	* @param companyId the primary key of the company
	* @return the matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchCLSIdeaByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.fetchCLSIdeaByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Returns the c l s idea matching the UUID and group.
	*
	* @param uuid the c l s idea's UUID
	* @param groupId the primary key of the group
	* @return the matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchCLSIdeaByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.fetchCLSIdeaByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns the c l s idea with the primary key.
	*
	* @param ideaID the primary key of the c l s idea
	* @return the c l s idea
	* @throws PortalException if a c l s idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea getCLSIdea(
		long ideaID)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.getCLSIdea(ideaID);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the c l s idea with the matching UUID and company.
	*
	* @param uuid the c l s idea's UUID
	* @param companyId the primary key of the company
	* @return the matching c l s idea
	* @throws PortalException if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea getCLSIdeaByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.getCLSIdeaByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns the c l s idea matching the UUID and group.
	*
	* @param uuid the c l s idea's UUID
	* @param groupId the primary key of the group
	* @return the matching c l s idea
	* @throws PortalException if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea getCLSIdeaByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.getCLSIdeaByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns a range of all the c l s ideas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s ideas
	* @param end the upper bound of the range of c l s ideas (not inclusive)
	* @return the range of c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getCLSIdeas(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.getCLSIdeas(start, end);
	}

	/**
	* Returns the number of c l s ideas.
	*
	* @return the number of c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getCLSIdeasCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.getCLSIdeasCount();
	}

	/**
	* Updates the c l s idea in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param clsIdea the c l s idea
	* @return the c l s idea that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea updateCLSIdea(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea clsIdea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.updateCLSIdea(clsIdea);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _clsIdeaLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_clsIdeaLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _clsIdeaLocalService.invokeMethod(name, parameterTypes, arguments);
	}

	@Override
	public void sendNotificationForTransition(java.util.ResourceBundle res,
		long ideaId, java.lang.String transictionName,
		javax.portlet.ActionRequest actionRequest) {
		_clsIdeaLocalService.sendNotificationForTransition(res, ideaId,
			transictionName, actionRequest);
	}

	/**
	* Send notifications in dockbar and emails
	*
	* @param azione Fase in cui avviene la notifica (puo' assumere: "nuovaIdea", "delete", "passaggioDiStato", "aggiuntaCollaboratore", "modificata" )
	*/
	@Override
	public void sendNotification(java.util.ResourceBundle res,
		java.lang.String textMessage, long destinationUderId,
		java.lang.String senderUserName, long ideaId,
		javax.portlet.ActionRequest actionRequest, java.lang.String azione)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		_clsIdeaLocalService.sendNotification(res, textMessage,
			destinationUderId, senderUserName, ideaId, actionRequest, azione);
	}

	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getIdeasByUserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.getIdeasByUserId(userId);
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea getIdeasByIdeaId(
		long ideaId) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.getIdeasByIdeaId(ideaId);
	}

	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getIdeasByChallengeId(
		long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.getIdeasByChallengeId(challengeId);
	}

	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getIdeasByMunicipalityId(
		long municipalityId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.getIdeasByMunicipalityId(municipalityId);
	}

	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getIdeasByMunicipalityOrganizationId(
		long municipalityOrganizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.getIdeasByMunicipalityOrganizationId(municipalityOrganizationId);
	}

	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getIdeasByNeedId(
		long needId) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.getIdeasByNeedId(needId);
	}

	/**
	* Return a list of all Needs
	*
	* @return
	* @throws SystemException
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getNeeds()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.getNeeds();
	}

	/**
	* Return a list of all Needs of the Municipality Organization
	*
	* @param userId
	* @return
	* @throws SystemException
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getNeedsByMunicipalityOrganizationId(
		long municipalityOrganizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.getNeedsByMunicipalityOrganizationId(municipalityOrganizationId);
	}

	/**
	* Return a list of all Ideas of the Municipality Organization
	*
	* @param userId
	* @return
	* @throws SystemException
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getOnlyIdeasByMunicipalityOrganizationId(
		long municipalityOrganizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.getOnlyIdeasByMunicipalityOrganizationId(municipalityOrganizationId);
	}

	/**
	* Return a list of all Needs of the Municipality
	*
	* @param userId
	* @return
	* @throws SystemException
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getNeedsByMunicipalityId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.getNeedsByMunicipalityId(userId);
	}

	/**
	* Return a list of all Ideas of the Municipality
	*
	* @param userId
	* @return
	* @throws SystemException
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getOnlyIdeasByMunicipalityId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.getOnlyIdeasByMunicipalityId(userId);
	}

	/**
	* Return a list of all Needs of the user
	*
	* @param userId
	* @return
	* @throws SystemException
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getNeedsByUserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.getNeedsByUserId(userId);
	}

	/**
	* Return a list of all Ideas, without need
	*
	* @return
	* @throws SystemException
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getOnlyIdeas()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.getOnlyIdeas();
	}

	/**
	* Return a list of all Ideas (without need) of the user
	*
	* @return
	* @throws SystemException
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getOnlyIdeasByUserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.getOnlyIdeasByUserId(userId);
	}

	@Override
	public void addCoworker(long indeaId, com.liferay.portal.model.User coworker) {
		_clsIdeaLocalService.addCoworker(indeaId, coworker);
	}

	/**
	* @return
	*/
	@Override
	public com.liferay.portal.kernel.json.JSONObject doRightToBeForgottenIMS(
		long liferayUserId, boolean deleteAllContent,
		javax.portlet.ActionRequest actionRequest) {
		return _clsIdeaLocalService.doRightToBeForgottenIMS(liferayUserId,
			deleteAllContent, actionRequest);
	}

	/**
	* @param pilot
	* @param isNeed
	* @param maxNumber
	* @return
	*/
	@Override
	public com.liferay.portal.kernel.json.JSONArray getTopRatedIdeasOrNeedsByPilot(
		java.lang.String pilot, boolean isNeed, int maxNumber,
		java.util.Locale locale) {
		return _clsIdeaLocalService.getTopRatedIdeasOrNeedsByPilot(pilot,
			isNeed, maxNumber, locale);
	}

	/**
	* @param language_acronim (en,es, it, sr, fi)
	* @param isNeed
	* @param maxNumber
	* @return
	*/
	@Override
	public com.liferay.portal.kernel.json.JSONArray getTopRatedIdeasOrNeedsByLanguage(
		java.lang.String language, boolean isNeed, int maxNumber,
		java.util.Locale locale) {
		return _clsIdeaLocalService.getTopRatedIdeasOrNeedsByLanguage(language,
			isNeed, maxNumber, locale);
	}

	/**
	* @param liferayUserId
	* @param isNeed
	* @param maxNumber
	* @return
	*/
	@Override
	public com.liferay.portal.kernel.json.JSONArray getIdeasOrNeedsByUserId(
		long liferayUserId, boolean isNeed, int maxNumber,
		java.util.Locale locale) {
		return _clsIdeaLocalService.getIdeasOrNeedsByUserId(liferayUserId,
			isNeed, maxNumber, locale);
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea updateStatus(
		long userId, long resourcePrimKey, int wStatus,
		com.liferay.portal.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaLocalService.updateStatus(userId, resourcePrimKey,
			wStatus, serviceContext);
	}

	/**
	* Access to other plugin
	*
	* @param properties
	* @return
	*/
	@Override
	public java.lang.String getIMSProperties(java.lang.String properties) {
		return _clsIdeaLocalService.getIMSProperties(properties);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CLSIdeaLocalService getWrappedCLSIdeaLocalService() {
		return _clsIdeaLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCLSIdeaLocalService(
		CLSIdeaLocalService clsIdeaLocalService) {
		_clsIdeaLocalService = clsIdeaLocalService;
	}

	@Override
	public CLSIdeaLocalService getWrappedService() {
		return _clsIdeaLocalService;
	}

	@Override
	public void setWrappedService(CLSIdeaLocalService clsIdeaLocalService) {
		_clsIdeaLocalService = clsIdeaLocalService;
	}

	private CLSIdeaLocalService _clsIdeaLocalService;
}