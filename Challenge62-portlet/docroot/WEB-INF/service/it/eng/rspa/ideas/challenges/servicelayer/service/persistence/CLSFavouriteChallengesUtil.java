/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges;

import java.util.List;

/**
 * The persistence utility for the c l s favourite challenges service. This utility wraps {@link CLSFavouriteChallengesPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSFavouriteChallengesPersistence
 * @see CLSFavouriteChallengesPersistenceImpl
 * @generated
 */
public class CLSFavouriteChallengesUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(CLSFavouriteChallenges clsFavouriteChallenges) {
		getPersistence().clearCache(clsFavouriteChallenges);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CLSFavouriteChallenges> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CLSFavouriteChallenges> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CLSFavouriteChallenges> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static CLSFavouriteChallenges update(
		CLSFavouriteChallenges clsFavouriteChallenges)
		throws SystemException {
		return getPersistence().update(clsFavouriteChallenges);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static CLSFavouriteChallenges update(
		CLSFavouriteChallenges clsFavouriteChallenges,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(clsFavouriteChallenges, serviceContext);
	}

	/**
	* Returns all the c l s favourite challengeses where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @return the matching c l s favourite challengeses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges> findByChallengeId(
		long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByChallengeId(challengeId);
	}

	/**
	* Returns a range of all the c l s favourite challengeses where challengeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteChallengesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param start the lower bound of the range of c l s favourite challengeses
	* @param end the upper bound of the range of c l s favourite challengeses (not inclusive)
	* @return the range of matching c l s favourite challengeses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges> findByChallengeId(
		long challengeId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByChallengeId(challengeId, start, end);
	}

	/**
	* Returns an ordered range of all the c l s favourite challengeses where challengeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteChallengesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param start the lower bound of the range of c l s favourite challengeses
	* @param end the upper bound of the range of c l s favourite challengeses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s favourite challengeses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges> findByChallengeId(
		long challengeId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByChallengeId(challengeId, start, end, orderByComparator);
	}

	/**
	* Returns the first c l s favourite challenges in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s favourite challenges
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException if a matching c l s favourite challenges could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges findByChallengeId_First(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException {
		return getPersistence()
				   .findByChallengeId_First(challengeId, orderByComparator);
	}

	/**
	* Returns the first c l s favourite challenges in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s favourite challenges, or <code>null</code> if a matching c l s favourite challenges could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges fetchByChallengeId_First(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByChallengeId_First(challengeId, orderByComparator);
	}

	/**
	* Returns the last c l s favourite challenges in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s favourite challenges
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException if a matching c l s favourite challenges could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges findByChallengeId_Last(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException {
		return getPersistence()
				   .findByChallengeId_Last(challengeId, orderByComparator);
	}

	/**
	* Returns the last c l s favourite challenges in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s favourite challenges, or <code>null</code> if a matching c l s favourite challenges could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges fetchByChallengeId_Last(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByChallengeId_Last(challengeId, orderByComparator);
	}

	/**
	* Returns the c l s favourite challengeses before and after the current c l s favourite challenges in the ordered set where challengeId = &#63;.
	*
	* @param clsFavouriteChallengesPK the primary key of the current c l s favourite challenges
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s favourite challenges
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException if a c l s favourite challenges with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges[] findByChallengeId_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK clsFavouriteChallengesPK,
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException {
		return getPersistence()
				   .findByChallengeId_PrevAndNext(clsFavouriteChallengesPK,
			challengeId, orderByComparator);
	}

	/**
	* Removes all the c l s favourite challengeses where challengeId = &#63; from the database.
	*
	* @param challengeId the challenge ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByChallengeId(long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByChallengeId(challengeId);
	}

	/**
	* Returns the number of c l s favourite challengeses where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @return the number of matching c l s favourite challengeses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByChallengeId(long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByChallengeId(challengeId);
	}

	/**
	* Returns all the c l s favourite challengeses where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching c l s favourite challengeses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges> findByUserID(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUserID(userId);
	}

	/**
	* Returns a range of all the c l s favourite challengeses where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteChallengesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of c l s favourite challengeses
	* @param end the upper bound of the range of c l s favourite challengeses (not inclusive)
	* @return the range of matching c l s favourite challengeses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges> findByUserID(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUserID(userId, start, end);
	}

	/**
	* Returns an ordered range of all the c l s favourite challengeses where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteChallengesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of c l s favourite challengeses
	* @param end the upper bound of the range of c l s favourite challengeses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s favourite challengeses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges> findByUserID(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUserID(userId, start, end, orderByComparator);
	}

	/**
	* Returns the first c l s favourite challenges in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s favourite challenges
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException if a matching c l s favourite challenges could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges findByUserID_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException {
		return getPersistence().findByUserID_First(userId, orderByComparator);
	}

	/**
	* Returns the first c l s favourite challenges in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s favourite challenges, or <code>null</code> if a matching c l s favourite challenges could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges fetchByUserID_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUserID_First(userId, orderByComparator);
	}

	/**
	* Returns the last c l s favourite challenges in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s favourite challenges
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException if a matching c l s favourite challenges could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges findByUserID_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException {
		return getPersistence().findByUserID_Last(userId, orderByComparator);
	}

	/**
	* Returns the last c l s favourite challenges in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s favourite challenges, or <code>null</code> if a matching c l s favourite challenges could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges fetchByUserID_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUserID_Last(userId, orderByComparator);
	}

	/**
	* Returns the c l s favourite challengeses before and after the current c l s favourite challenges in the ordered set where userId = &#63;.
	*
	* @param clsFavouriteChallengesPK the primary key of the current c l s favourite challenges
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s favourite challenges
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException if a c l s favourite challenges with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges[] findByUserID_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK clsFavouriteChallengesPK,
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException {
		return getPersistence()
				   .findByUserID_PrevAndNext(clsFavouriteChallengesPK, userId,
			orderByComparator);
	}

	/**
	* Removes all the c l s favourite challengeses where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByUserID(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByUserID(userId);
	}

	/**
	* Returns the number of c l s favourite challengeses where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching c l s favourite challengeses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUserID(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByUserID(userId);
	}

	/**
	* Returns the c l s favourite challenges where challengeId = &#63; and userId = &#63; or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException} if it could not be found.
	*
	* @param challengeId the challenge ID
	* @param userId the user ID
	* @return the matching c l s favourite challenges
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException if a matching c l s favourite challenges could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges findByChallengeIDAndUserID(
		long challengeId, long userId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException {
		return getPersistence().findByChallengeIDAndUserID(challengeId, userId);
	}

	/**
	* Returns the c l s favourite challenges where challengeId = &#63; and userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param challengeId the challenge ID
	* @param userId the user ID
	* @return the matching c l s favourite challenges, or <code>null</code> if a matching c l s favourite challenges could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges fetchByChallengeIDAndUserID(
		long challengeId, long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByChallengeIDAndUserID(challengeId, userId);
	}

	/**
	* Returns the c l s favourite challenges where challengeId = &#63; and userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param challengeId the challenge ID
	* @param userId the user ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching c l s favourite challenges, or <code>null</code> if a matching c l s favourite challenges could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges fetchByChallengeIDAndUserID(
		long challengeId, long userId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByChallengeIDAndUserID(challengeId, userId,
			retrieveFromCache);
	}

	/**
	* Removes the c l s favourite challenges where challengeId = &#63; and userId = &#63; from the database.
	*
	* @param challengeId the challenge ID
	* @param userId the user ID
	* @return the c l s favourite challenges that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges removeByChallengeIDAndUserID(
		long challengeId, long userId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException {
		return getPersistence().removeByChallengeIDAndUserID(challengeId, userId);
	}

	/**
	* Returns the number of c l s favourite challengeses where challengeId = &#63; and userId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param userId the user ID
	* @return the number of matching c l s favourite challengeses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByChallengeIDAndUserID(long challengeId, long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByChallengeIDAndUserID(challengeId, userId);
	}

	/**
	* Caches the c l s favourite challenges in the entity cache if it is enabled.
	*
	* @param clsFavouriteChallenges the c l s favourite challenges
	*/
	public static void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges clsFavouriteChallenges) {
		getPersistence().cacheResult(clsFavouriteChallenges);
	}

	/**
	* Caches the c l s favourite challengeses in the entity cache if it is enabled.
	*
	* @param clsFavouriteChallengeses the c l s favourite challengeses
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges> clsFavouriteChallengeses) {
		getPersistence().cacheResult(clsFavouriteChallengeses);
	}

	/**
	* Creates a new c l s favourite challenges with the primary key. Does not add the c l s favourite challenges to the database.
	*
	* @param clsFavouriteChallengesPK the primary key for the new c l s favourite challenges
	* @return the new c l s favourite challenges
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges create(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK clsFavouriteChallengesPK) {
		return getPersistence().create(clsFavouriteChallengesPK);
	}

	/**
	* Removes the c l s favourite challenges with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param clsFavouriteChallengesPK the primary key of the c l s favourite challenges
	* @return the c l s favourite challenges that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException if a c l s favourite challenges with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges remove(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK clsFavouriteChallengesPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException {
		return getPersistence().remove(clsFavouriteChallengesPK);
	}

	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges clsFavouriteChallenges)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(clsFavouriteChallenges);
	}

	/**
	* Returns the c l s favourite challenges with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException} if it could not be found.
	*
	* @param clsFavouriteChallengesPK the primary key of the c l s favourite challenges
	* @return the c l s favourite challenges
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException if a c l s favourite challenges with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges findByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK clsFavouriteChallengesPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException {
		return getPersistence().findByPrimaryKey(clsFavouriteChallengesPK);
	}

	/**
	* Returns the c l s favourite challenges with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param clsFavouriteChallengesPK the primary key of the c l s favourite challenges
	* @return the c l s favourite challenges, or <code>null</code> if a c l s favourite challenges with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges fetchByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK clsFavouriteChallengesPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(clsFavouriteChallengesPK);
	}

	/**
	* Returns all the c l s favourite challengeses.
	*
	* @return the c l s favourite challengeses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the c l s favourite challengeses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteChallengesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s favourite challengeses
	* @param end the upper bound of the range of c l s favourite challengeses (not inclusive)
	* @return the range of c l s favourite challengeses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the c l s favourite challengeses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteChallengesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s favourite challengeses
	* @param end the upper bound of the range of c l s favourite challengeses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of c l s favourite challengeses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the c l s favourite challengeses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of c l s favourite challengeses.
	*
	* @return the number of c l s favourite challengeses
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static CLSFavouriteChallengesPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (CLSFavouriteChallengesPersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.getServletContextName(),
					CLSFavouriteChallengesPersistence.class.getName());

			ReferenceRegistry.registerReference(CLSFavouriteChallengesUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(CLSFavouriteChallengesPersistence persistence) {
	}

	private static CLSFavouriteChallengesPersistence _persistence;
}