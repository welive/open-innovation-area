/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSChallengePoiServiceSoap}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSChallengePoiServiceSoap
 * @generated
 */
public class CLSChallengePoiSoap implements Serializable {
	public static CLSChallengePoiSoap toSoapModel(CLSChallengePoi model) {
		CLSChallengePoiSoap soapModel = new CLSChallengePoiSoap();

		soapModel.setChallengeId(model.getChallengeId());
		soapModel.setLatitude(model.getLatitude());
		soapModel.setLongitude(model.getLongitude());
		soapModel.setDescription(model.getDescription());
		soapModel.setPoiId(model.getPoiId());
		soapModel.setTitle(model.getTitle());

		return soapModel;
	}

	public static CLSChallengePoiSoap[] toSoapModels(CLSChallengePoi[] models) {
		CLSChallengePoiSoap[] soapModels = new CLSChallengePoiSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CLSChallengePoiSoap[][] toSoapModels(
		CLSChallengePoi[][] models) {
		CLSChallengePoiSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CLSChallengePoiSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CLSChallengePoiSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CLSChallengePoiSoap[] toSoapModels(
		List<CLSChallengePoi> models) {
		List<CLSChallengePoiSoap> soapModels = new ArrayList<CLSChallengePoiSoap>(models.size());

		for (CLSChallengePoi model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CLSChallengePoiSoap[soapModels.size()]);
	}

	public CLSChallengePoiSoap() {
	}

	public long getPrimaryKey() {
		return _poiId;
	}

	public void setPrimaryKey(long pk) {
		setPoiId(pk);
	}

	public long getChallengeId() {
		return _challengeId;
	}

	public void setChallengeId(long challengeId) {
		_challengeId = challengeId;
	}

	public String getLatitude() {
		return _latitude;
	}

	public void setLatitude(String latitude) {
		_latitude = latitude;
	}

	public String getLongitude() {
		return _longitude;
	}

	public void setLongitude(String longitude) {
		_longitude = longitude;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public long getPoiId() {
		return _poiId;
	}

	public void setPoiId(long poiId) {
		_poiId = poiId;
	}

	public String getTitle() {
		return _title;
	}

	public void setTitle(String title) {
		_title = title;
	}

	private long _challengeId;
	private String _latitude;
	private String _longitude;
	private String _description;
	private long _poiId;
	private String _title;
}