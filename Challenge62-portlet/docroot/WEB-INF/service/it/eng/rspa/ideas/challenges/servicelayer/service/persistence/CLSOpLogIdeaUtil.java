/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea;

import java.util.List;

/**
 * The persistence utility for the c l s op log idea service. This utility wraps {@link CLSOpLogIdeaPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSOpLogIdeaPersistence
 * @see CLSOpLogIdeaPersistenceImpl
 * @generated
 */
public class CLSOpLogIdeaUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(CLSOpLogIdea clsOpLogIdea) {
		getPersistence().clearCache(clsOpLogIdea);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CLSOpLogIdea> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CLSOpLogIdea> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CLSOpLogIdea> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static CLSOpLogIdea update(CLSOpLogIdea clsOpLogIdea)
		throws SystemException {
		return getPersistence().update(clsOpLogIdea);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static CLSOpLogIdea update(CLSOpLogIdea clsOpLogIdea,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(clsOpLogIdea, serviceContext);
	}

	/**
	* Returns all the c l s op log ideas where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the matching c l s op log ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea> findByideaId(
		long ideaId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByideaId(ideaId);
	}

	/**
	* Returns a range of all the c l s op log ideas where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSOpLogIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of c l s op log ideas
	* @param end the upper bound of the range of c l s op log ideas (not inclusive)
	* @return the range of matching c l s op log ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea> findByideaId(
		long ideaId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByideaId(ideaId, start, end);
	}

	/**
	* Returns an ordered range of all the c l s op log ideas where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSOpLogIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of c l s op log ideas
	* @param end the upper bound of the range of c l s op log ideas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s op log ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea> findByideaId(
		long ideaId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByideaId(ideaId, start, end, orderByComparator);
	}

	/**
	* Returns the first c l s op log idea in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s op log idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSOpLogIdeaException if a matching c l s op log idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea findByideaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSOpLogIdeaException {
		return getPersistence().findByideaId_First(ideaId, orderByComparator);
	}

	/**
	* Returns the first c l s op log idea in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s op log idea, or <code>null</code> if a matching c l s op log idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea fetchByideaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByideaId_First(ideaId, orderByComparator);
	}

	/**
	* Returns the last c l s op log idea in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s op log idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSOpLogIdeaException if a matching c l s op log idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea findByideaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSOpLogIdeaException {
		return getPersistence().findByideaId_Last(ideaId, orderByComparator);
	}

	/**
	* Returns the last c l s op log idea in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s op log idea, or <code>null</code> if a matching c l s op log idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea fetchByideaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByideaId_Last(ideaId, orderByComparator);
	}

	/**
	* Returns the c l s op log ideas before and after the current c l s op log idea in the ordered set where ideaId = &#63;.
	*
	* @param opLogId the primary key of the current c l s op log idea
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s op log idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSOpLogIdeaException if a c l s op log idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea[] findByideaId_PrevAndNext(
		long opLogId, long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSOpLogIdeaException {
		return getPersistence()
				   .findByideaId_PrevAndNext(opLogId, ideaId, orderByComparator);
	}

	/**
	* Removes all the c l s op log ideas where ideaId = &#63; from the database.
	*
	* @param ideaId the idea ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByideaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByideaId(ideaId);
	}

	/**
	* Returns the number of c l s op log ideas where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the number of matching c l s op log ideas
	* @throws SystemException if a system exception occurred
	*/
	public static int countByideaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByideaId(ideaId);
	}

	/**
	* Caches the c l s op log idea in the entity cache if it is enabled.
	*
	* @param clsOpLogIdea the c l s op log idea
	*/
	public static void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea clsOpLogIdea) {
		getPersistence().cacheResult(clsOpLogIdea);
	}

	/**
	* Caches the c l s op log ideas in the entity cache if it is enabled.
	*
	* @param clsOpLogIdeas the c l s op log ideas
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea> clsOpLogIdeas) {
		getPersistence().cacheResult(clsOpLogIdeas);
	}

	/**
	* Creates a new c l s op log idea with the primary key. Does not add the c l s op log idea to the database.
	*
	* @param opLogId the primary key for the new c l s op log idea
	* @return the new c l s op log idea
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea create(
		long opLogId) {
		return getPersistence().create(opLogId);
	}

	/**
	* Removes the c l s op log idea with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param opLogId the primary key of the c l s op log idea
	* @return the c l s op log idea that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSOpLogIdeaException if a c l s op log idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea remove(
		long opLogId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSOpLogIdeaException {
		return getPersistence().remove(opLogId);
	}

	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea clsOpLogIdea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(clsOpLogIdea);
	}

	/**
	* Returns the c l s op log idea with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSOpLogIdeaException} if it could not be found.
	*
	* @param opLogId the primary key of the c l s op log idea
	* @return the c l s op log idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSOpLogIdeaException if a c l s op log idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea findByPrimaryKey(
		long opLogId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSOpLogIdeaException {
		return getPersistence().findByPrimaryKey(opLogId);
	}

	/**
	* Returns the c l s op log idea with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param opLogId the primary key of the c l s op log idea
	* @return the c l s op log idea, or <code>null</code> if a c l s op log idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea fetchByPrimaryKey(
		long opLogId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(opLogId);
	}

	/**
	* Returns all the c l s op log ideas.
	*
	* @return the c l s op log ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the c l s op log ideas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSOpLogIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s op log ideas
	* @param end the upper bound of the range of c l s op log ideas (not inclusive)
	* @return the range of c l s op log ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the c l s op log ideas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSOpLogIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s op log ideas
	* @param end the upper bound of the range of c l s op log ideas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of c l s op log ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the c l s op log ideas from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of c l s op log ideas.
	*
	* @return the number of c l s op log ideas
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static CLSOpLogIdeaPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (CLSOpLogIdeaPersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.getServletContextName(),
					CLSOpLogIdeaPersistence.class.getName());

			ReferenceRegistry.registerReference(CLSOpLogIdeaUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(CLSOpLogIdeaPersistence persistence) {
	}

	private static CLSOpLogIdeaPersistence _persistence;
}