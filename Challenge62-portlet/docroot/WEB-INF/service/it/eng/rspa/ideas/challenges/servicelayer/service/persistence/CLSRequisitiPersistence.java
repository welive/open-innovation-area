/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti;

/**
 * The persistence interface for the c l s requisiti service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSRequisitiPersistenceImpl
 * @see CLSRequisitiUtil
 * @generated
 */
public interface CLSRequisitiPersistence extends BasePersistence<CLSRequisiti> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CLSRequisitiUtil} to access the c l s requisiti persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the c l s requisitis where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the matching c l s requisitis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti> findByrequisitoByIdeaId(
		long ideaId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the c l s requisitis where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of c l s requisitis
	* @param end the upper bound of the range of c l s requisitis (not inclusive)
	* @return the range of matching c l s requisitis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti> findByrequisitoByIdeaId(
		long ideaId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the c l s requisitis where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of c l s requisitis
	* @param end the upper bound of the range of c l s requisitis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s requisitis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti> findByrequisitoByIdeaId(
		long ideaId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first c l s requisiti in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s requisiti
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a matching c l s requisiti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti findByrequisitoByIdeaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException;

	/**
	* Returns the first c l s requisiti in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s requisiti, or <code>null</code> if a matching c l s requisiti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti fetchByrequisitoByIdeaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last c l s requisiti in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s requisiti
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a matching c l s requisiti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti findByrequisitoByIdeaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException;

	/**
	* Returns the last c l s requisiti in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s requisiti, or <code>null</code> if a matching c l s requisiti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti fetchByrequisitoByIdeaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the c l s requisitis before and after the current c l s requisiti in the ordered set where ideaId = &#63;.
	*
	* @param requisitoId the primary key of the current c l s requisiti
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s requisiti
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a c l s requisiti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti[] findByrequisitoByIdeaId_PrevAndNext(
		long requisitoId, long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException;

	/**
	* Removes all the c l s requisitis where ideaId = &#63; from the database.
	*
	* @param ideaId the idea ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByrequisitoByIdeaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of c l s requisitis where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the number of matching c l s requisitis
	* @throws SystemException if a system exception occurred
	*/
	public int countByrequisitoByIdeaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the c l s requisitis where authorUser = &#63;.
	*
	* @param authorUser the author user
	* @return the matching c l s requisitis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti> findByrequisitoByAuthorid(
		long authorUser)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the c l s requisitis where authorUser = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param authorUser the author user
	* @param start the lower bound of the range of c l s requisitis
	* @param end the upper bound of the range of c l s requisitis (not inclusive)
	* @return the range of matching c l s requisitis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti> findByrequisitoByAuthorid(
		long authorUser, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the c l s requisitis where authorUser = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param authorUser the author user
	* @param start the lower bound of the range of c l s requisitis
	* @param end the upper bound of the range of c l s requisitis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s requisitis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti> findByrequisitoByAuthorid(
		long authorUser, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first c l s requisiti in the ordered set where authorUser = &#63;.
	*
	* @param authorUser the author user
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s requisiti
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a matching c l s requisiti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti findByrequisitoByAuthorid_First(
		long authorUser,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException;

	/**
	* Returns the first c l s requisiti in the ordered set where authorUser = &#63;.
	*
	* @param authorUser the author user
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s requisiti, or <code>null</code> if a matching c l s requisiti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti fetchByrequisitoByAuthorid_First(
		long authorUser,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last c l s requisiti in the ordered set where authorUser = &#63;.
	*
	* @param authorUser the author user
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s requisiti
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a matching c l s requisiti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti findByrequisitoByAuthorid_Last(
		long authorUser,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException;

	/**
	* Returns the last c l s requisiti in the ordered set where authorUser = &#63;.
	*
	* @param authorUser the author user
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s requisiti, or <code>null</code> if a matching c l s requisiti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti fetchByrequisitoByAuthorid_Last(
		long authorUser,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the c l s requisitis before and after the current c l s requisiti in the ordered set where authorUser = &#63;.
	*
	* @param requisitoId the primary key of the current c l s requisiti
	* @param authorUser the author user
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s requisiti
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a c l s requisiti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti[] findByrequisitoByAuthorid_PrevAndNext(
		long requisitoId, long authorUser,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException;

	/**
	* Removes all the c l s requisitis where authorUser = &#63; from the database.
	*
	* @param authorUser the author user
	* @throws SystemException if a system exception occurred
	*/
	public void removeByrequisitoByAuthorid(long authorUser)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of c l s requisitis where authorUser = &#63;.
	*
	* @param authorUser the author user
	* @return the number of matching c l s requisitis
	* @throws SystemException if a system exception occurred
	*/
	public int countByrequisitoByAuthorid(long authorUser)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the c l s requisitis where ideaId = &#63; and authorUser = &#63;.
	*
	* @param ideaId the idea ID
	* @param authorUser the author user
	* @return the matching c l s requisitis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti> findByrequisitoByIdeaIdandAuthorid(
		long ideaId, long authorUser)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the c l s requisitis where ideaId = &#63; and authorUser = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param authorUser the author user
	* @param start the lower bound of the range of c l s requisitis
	* @param end the upper bound of the range of c l s requisitis (not inclusive)
	* @return the range of matching c l s requisitis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti> findByrequisitoByIdeaIdandAuthorid(
		long ideaId, long authorUser, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the c l s requisitis where ideaId = &#63; and authorUser = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param authorUser the author user
	* @param start the lower bound of the range of c l s requisitis
	* @param end the upper bound of the range of c l s requisitis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s requisitis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti> findByrequisitoByIdeaIdandAuthorid(
		long ideaId, long authorUser, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first c l s requisiti in the ordered set where ideaId = &#63; and authorUser = &#63;.
	*
	* @param ideaId the idea ID
	* @param authorUser the author user
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s requisiti
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a matching c l s requisiti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti findByrequisitoByIdeaIdandAuthorid_First(
		long ideaId, long authorUser,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException;

	/**
	* Returns the first c l s requisiti in the ordered set where ideaId = &#63; and authorUser = &#63;.
	*
	* @param ideaId the idea ID
	* @param authorUser the author user
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s requisiti, or <code>null</code> if a matching c l s requisiti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti fetchByrequisitoByIdeaIdandAuthorid_First(
		long ideaId, long authorUser,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last c l s requisiti in the ordered set where ideaId = &#63; and authorUser = &#63;.
	*
	* @param ideaId the idea ID
	* @param authorUser the author user
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s requisiti
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a matching c l s requisiti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti findByrequisitoByIdeaIdandAuthorid_Last(
		long ideaId, long authorUser,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException;

	/**
	* Returns the last c l s requisiti in the ordered set where ideaId = &#63; and authorUser = &#63;.
	*
	* @param ideaId the idea ID
	* @param authorUser the author user
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s requisiti, or <code>null</code> if a matching c l s requisiti could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti fetchByrequisitoByIdeaIdandAuthorid_Last(
		long ideaId, long authorUser,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the c l s requisitis before and after the current c l s requisiti in the ordered set where ideaId = &#63; and authorUser = &#63;.
	*
	* @param requisitoId the primary key of the current c l s requisiti
	* @param ideaId the idea ID
	* @param authorUser the author user
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s requisiti
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a c l s requisiti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti[] findByrequisitoByIdeaIdandAuthorid_PrevAndNext(
		long requisitoId, long ideaId, long authorUser,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException;

	/**
	* Removes all the c l s requisitis where ideaId = &#63; and authorUser = &#63; from the database.
	*
	* @param ideaId the idea ID
	* @param authorUser the author user
	* @throws SystemException if a system exception occurred
	*/
	public void removeByrequisitoByIdeaIdandAuthorid(long ideaId,
		long authorUser)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of c l s requisitis where ideaId = &#63; and authorUser = &#63;.
	*
	* @param ideaId the idea ID
	* @param authorUser the author user
	* @return the number of matching c l s requisitis
	* @throws SystemException if a system exception occurred
	*/
	public int countByrequisitoByIdeaIdandAuthorid(long ideaId, long authorUser)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the c l s requisiti in the entity cache if it is enabled.
	*
	* @param clsRequisiti the c l s requisiti
	*/
	public void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti clsRequisiti);

	/**
	* Caches the c l s requisitis in the entity cache if it is enabled.
	*
	* @param clsRequisitis the c l s requisitis
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti> clsRequisitis);

	/**
	* Creates a new c l s requisiti with the primary key. Does not add the c l s requisiti to the database.
	*
	* @param requisitoId the primary key for the new c l s requisiti
	* @return the new c l s requisiti
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti create(
		long requisitoId);

	/**
	* Removes the c l s requisiti with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param requisitoId the primary key of the c l s requisiti
	* @return the c l s requisiti that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a c l s requisiti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti remove(
		long requisitoId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException;

	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti clsRequisiti)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the c l s requisiti with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException} if it could not be found.
	*
	* @param requisitoId the primary key of the c l s requisiti
	* @return the c l s requisiti
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a c l s requisiti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti findByPrimaryKey(
		long requisitoId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException;

	/**
	* Returns the c l s requisiti with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param requisitoId the primary key of the c l s requisiti
	* @return the c l s requisiti, or <code>null</code> if a c l s requisiti with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti fetchByPrimaryKey(
		long requisitoId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the c l s requisitis.
	*
	* @return the c l s requisitis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the c l s requisitis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s requisitis
	* @param end the upper bound of the range of c l s requisitis (not inclusive)
	* @return the range of c l s requisitis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the c l s requisitis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s requisitis
	* @param end the upper bound of the range of c l s requisitis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of c l s requisitis
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the c l s requisitis from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of c l s requisitis.
	*
	* @return the number of c l s requisitis
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}