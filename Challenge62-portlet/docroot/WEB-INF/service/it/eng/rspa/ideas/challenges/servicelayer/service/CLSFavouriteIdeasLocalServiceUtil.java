/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for CLSFavouriteIdeas. This utility wraps
 * {@link it.eng.rspa.ideas.challenges.servicelayer.service.impl.CLSFavouriteIdeasLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSFavouriteIdeasLocalService
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSFavouriteIdeasLocalServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.impl.CLSFavouriteIdeasLocalServiceImpl
 * @generated
 */
public class CLSFavouriteIdeasLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.eng.rspa.ideas.challenges.servicelayer.service.impl.CLSFavouriteIdeasLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the c l s favourite ideas to the database. Also notifies the appropriate model listeners.
	*
	* @param clsFavouriteIdeas the c l s favourite ideas
	* @return the c l s favourite ideas that was added
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas addCLSFavouriteIdeas(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas clsFavouriteIdeas)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addCLSFavouriteIdeas(clsFavouriteIdeas);
	}

	/**
	* Creates a new c l s favourite ideas with the primary key. Does not add the c l s favourite ideas to the database.
	*
	* @param clsFavouriteIdeasPK the primary key for the new c l s favourite ideas
	* @return the new c l s favourite ideas
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas createCLSFavouriteIdeas(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteIdeasPK clsFavouriteIdeasPK) {
		return getService().createCLSFavouriteIdeas(clsFavouriteIdeasPK);
	}

	/**
	* Deletes the c l s favourite ideas with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param clsFavouriteIdeasPK the primary key of the c l s favourite ideas
	* @return the c l s favourite ideas that was removed
	* @throws PortalException if a c l s favourite ideas with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas deleteCLSFavouriteIdeas(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteIdeasPK clsFavouriteIdeasPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteCLSFavouriteIdeas(clsFavouriteIdeasPK);
	}

	/**
	* Deletes the c l s favourite ideas from the database. Also notifies the appropriate model listeners.
	*
	* @param clsFavouriteIdeas the c l s favourite ideas
	* @return the c l s favourite ideas that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas deleteCLSFavouriteIdeas(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas clsFavouriteIdeas)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteCLSFavouriteIdeas(clsFavouriteIdeas);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteIdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteIdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas fetchCLSFavouriteIdeas(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteIdeasPK clsFavouriteIdeasPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchCLSFavouriteIdeas(clsFavouriteIdeasPK);
	}

	/**
	* Returns the c l s favourite ideas with the primary key.
	*
	* @param clsFavouriteIdeasPK the primary key of the c l s favourite ideas
	* @return the c l s favourite ideas
	* @throws PortalException if a c l s favourite ideas with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas getCLSFavouriteIdeas(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteIdeasPK clsFavouriteIdeasPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getCLSFavouriteIdeas(clsFavouriteIdeasPK);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the c l s favourite ideases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteIdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s favourite ideases
	* @param end the upper bound of the range of c l s favourite ideases (not inclusive)
	* @return the range of c l s favourite ideases
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas> getCLSFavouriteIdeases(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getCLSFavouriteIdeases(start, end);
	}

	/**
	* Returns the number of c l s favourite ideases.
	*
	* @return the number of c l s favourite ideases
	* @throws SystemException if a system exception occurred
	*/
	public static int getCLSFavouriteIdeasesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getCLSFavouriteIdeasesCount();
	}

	/**
	* Updates the c l s favourite ideas in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param clsFavouriteIdeas the c l s favourite ideas
	* @return the c l s favourite ideas that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas updateCLSFavouriteIdeas(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas clsFavouriteIdeas)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateCLSFavouriteIdeas(clsFavouriteIdeas);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static void removeFavouriteIdeasOnIdeaDelete(long ideaId) {
		getService().removeFavouriteIdeasOnIdeaDelete(ideaId);
	}

	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas> getFavouriteIdeasEntriesByIdeaId(
		long ideaId) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getFavouriteIdeasEntriesByIdeaId(ideaId);
	}

	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getFavouriteIdeasByUserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getFavouriteIdeasByUserId(userId);
	}

	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas> getFavouriteIdeasEntriesByUserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getFavouriteIdeasEntriesByUserId(userId);
	}

	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas> getFavouriteIdeas(
		java.lang.Long ideaId, java.lang.Long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getFavouriteIdeas(ideaId, userId);
	}

	public static boolean addFavouriteIdea(java.lang.Long favIdeaId,
		java.lang.Long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addFavouriteIdea(favIdeaId, userId);
	}

	public static boolean removeFavouriteIdea(java.lang.Long favIdeaId,
		java.lang.Long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().removeFavouriteIdea(favIdeaId, userId);
	}

	public static boolean removeFavouriteIdea(java.lang.Long favideaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().removeFavouriteIdea(favideaId);
	}

	public static void clearService() {
		_service = null;
	}

	public static CLSFavouriteIdeasLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					CLSFavouriteIdeasLocalService.class.getName());

			if (invokableLocalService instanceof CLSFavouriteIdeasLocalService) {
				_service = (CLSFavouriteIdeasLocalService)invokableLocalService;
			}
			else {
				_service = new CLSFavouriteIdeasLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(CLSFavouriteIdeasLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(CLSFavouriteIdeasLocalService service) {
	}

	private static CLSFavouriteIdeasLocalService _service;
}