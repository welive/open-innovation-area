/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.eng.rspa.ideas.challenges.servicelayer.service.http.EvaluationCriteriaServiceSoap}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.http.EvaluationCriteriaServiceSoap
 * @generated
 */
public class EvaluationCriteriaSoap implements Serializable {
	public static EvaluationCriteriaSoap toSoapModel(EvaluationCriteria model) {
		EvaluationCriteriaSoap soapModel = new EvaluationCriteriaSoap();

		soapModel.setCriteriaId(model.getCriteriaId());
		soapModel.setChallengeId(model.getChallengeId());
		soapModel.setEnabled(model.getEnabled());
		soapModel.setDescription(model.getDescription());
		soapModel.setIsBarriera(model.getIsBarriera());
		soapModel.setIsCustom(model.getIsCustom());
		soapModel.setLanguage(model.getLanguage());
		soapModel.setInputId(model.getInputId());
		soapModel.setWeight(model.getWeight());
		soapModel.setThreshold(model.getThreshold());
		soapModel.setDate(model.getDate());
		soapModel.setAuthorId(model.getAuthorId());

		return soapModel;
	}

	public static EvaluationCriteriaSoap[] toSoapModels(
		EvaluationCriteria[] models) {
		EvaluationCriteriaSoap[] soapModels = new EvaluationCriteriaSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static EvaluationCriteriaSoap[][] toSoapModels(
		EvaluationCriteria[][] models) {
		EvaluationCriteriaSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new EvaluationCriteriaSoap[models.length][models[0].length];
		}
		else {
			soapModels = new EvaluationCriteriaSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static EvaluationCriteriaSoap[] toSoapModels(
		List<EvaluationCriteria> models) {
		List<EvaluationCriteriaSoap> soapModels = new ArrayList<EvaluationCriteriaSoap>(models.size());

		for (EvaluationCriteria model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new EvaluationCriteriaSoap[soapModels.size()]);
	}

	public EvaluationCriteriaSoap() {
	}

	public EvaluationCriteriaPK getPrimaryKey() {
		return new EvaluationCriteriaPK(_criteriaId, _challengeId);
	}

	public void setPrimaryKey(EvaluationCriteriaPK pk) {
		setCriteriaId(pk.criteriaId);
		setChallengeId(pk.challengeId);
	}

	public long getCriteriaId() {
		return _criteriaId;
	}

	public void setCriteriaId(long criteriaId) {
		_criteriaId = criteriaId;
	}

	public long getChallengeId() {
		return _challengeId;
	}

	public void setChallengeId(long challengeId) {
		_challengeId = challengeId;
	}

	public boolean getEnabled() {
		return _enabled;
	}

	public boolean isEnabled() {
		return _enabled;
	}

	public void setEnabled(boolean enabled) {
		_enabled = enabled;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public boolean getIsBarriera() {
		return _isBarriera;
	}

	public boolean isIsBarriera() {
		return _isBarriera;
	}

	public void setIsBarriera(boolean isBarriera) {
		_isBarriera = isBarriera;
	}

	public boolean getIsCustom() {
		return _isCustom;
	}

	public boolean isIsCustom() {
		return _isCustom;
	}

	public void setIsCustom(boolean isCustom) {
		_isCustom = isCustom;
	}

	public String getLanguage() {
		return _language;
	}

	public void setLanguage(String language) {
		_language = language;
	}

	public String getInputId() {
		return _inputId;
	}

	public void setInputId(String inputId) {
		_inputId = inputId;
	}

	public double getWeight() {
		return _weight;
	}

	public void setWeight(double weight) {
		_weight = weight;
	}

	public int getThreshold() {
		return _threshold;
	}

	public void setThreshold(int threshold) {
		_threshold = threshold;
	}

	public Date getDate() {
		return _date;
	}

	public void setDate(Date date) {
		_date = date;
	}

	public long getAuthorId() {
		return _authorId;
	}

	public void setAuthorId(long authorId) {
		_authorId = authorId;
	}

	private long _criteriaId;
	private long _challengeId;
	private boolean _enabled;
	private String _description;
	private boolean _isBarriera;
	private boolean _isCustom;
	private String _language;
	private String _inputId;
	private double _weight;
	private int _threshold;
	private Date _date;
	private long _authorId;
}