/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCoworkerLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class CLSCoworkerClp extends BaseModelImpl<CLSCoworker>
	implements CLSCoworker {
	public CLSCoworkerClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return CLSCoworker.class;
	}

	@Override
	public String getModelClassName() {
		return CLSCoworker.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _coworkerId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setCoworkerId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _coworkerId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ideaID", getIdeaID());
		attributes.put("coworkerId", getCoworkerId());
		attributes.put("userId", getUserId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ideaID = (Long)attributes.get("ideaID");

		if (ideaID != null) {
			setIdeaID(ideaID);
		}

		Long coworkerId = (Long)attributes.get("coworkerId");

		if (coworkerId != null) {
			setCoworkerId(coworkerId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}
	}

	@Override
	public long getIdeaID() {
		return _ideaID;
	}

	@Override
	public void setIdeaID(long ideaID) {
		_ideaID = ideaID;

		if (_clsCoworkerRemoteModel != null) {
			try {
				Class<?> clazz = _clsCoworkerRemoteModel.getClass();

				Method method = clazz.getMethod("setIdeaID", long.class);

				method.invoke(_clsCoworkerRemoteModel, ideaID);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getCoworkerId() {
		return _coworkerId;
	}

	@Override
	public void setCoworkerId(long coworkerId) {
		_coworkerId = coworkerId;

		if (_clsCoworkerRemoteModel != null) {
			try {
				Class<?> clazz = _clsCoworkerRemoteModel.getClass();

				Method method = clazz.getMethod("setCoworkerId", long.class);

				method.invoke(_clsCoworkerRemoteModel, coworkerId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;

		if (_clsCoworkerRemoteModel != null) {
			try {
				Class<?> clazz = _clsCoworkerRemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", long.class);

				method.invoke(_clsCoworkerRemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public BaseModel<?> getCLSCoworkerRemoteModel() {
		return _clsCoworkerRemoteModel;
	}

	public void setCLSCoworkerRemoteModel(BaseModel<?> clsCoworkerRemoteModel) {
		_clsCoworkerRemoteModel = clsCoworkerRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _clsCoworkerRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_clsCoworkerRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CLSCoworkerLocalServiceUtil.addCLSCoworker(this);
		}
		else {
			CLSCoworkerLocalServiceUtil.updateCLSCoworker(this);
		}
	}

	@Override
	public CLSCoworker toEscapedModel() {
		return (CLSCoworker)ProxyUtil.newProxyInstance(CLSCoworker.class.getClassLoader(),
			new Class[] { CLSCoworker.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CLSCoworkerClp clone = new CLSCoworkerClp();

		clone.setIdeaID(getIdeaID());
		clone.setCoworkerId(getCoworkerId());
		clone.setUserId(getUserId());

		return clone;
	}

	@Override
	public int compareTo(CLSCoworker clsCoworker) {
		int value = 0;

		if (getCoworkerId() < clsCoworker.getCoworkerId()) {
			value = -1;
		}
		else if (getCoworkerId() > clsCoworker.getCoworkerId()) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSCoworkerClp)) {
			return false;
		}

		CLSCoworkerClp clsCoworker = (CLSCoworkerClp)obj;

		long primaryKey = clsCoworker.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{ideaID=");
		sb.append(getIdeaID());
		sb.append(", coworkerId=");
		sb.append(getCoworkerId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>ideaID</column-name><column-value><![CDATA[");
		sb.append(getIdeaID());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>coworkerId</column-name><column-value><![CDATA[");
		sb.append(getCoworkerId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _ideaID;
	private long _coworkerId;
	private long _userId;
	private String _userUuid;
	private BaseModel<?> _clsCoworkerRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.class;
}