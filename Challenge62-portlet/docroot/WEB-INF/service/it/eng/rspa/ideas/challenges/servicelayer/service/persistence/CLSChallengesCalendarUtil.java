/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengesCalendar;

import java.util.List;

/**
 * The persistence utility for the c l s challenges calendar service. This utility wraps {@link CLSChallengesCalendarPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSChallengesCalendarPersistence
 * @see CLSChallengesCalendarPersistenceImpl
 * @generated
 */
public class CLSChallengesCalendarUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(CLSChallengesCalendar clsChallengesCalendar) {
		getPersistence().clearCache(clsChallengesCalendar);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CLSChallengesCalendar> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CLSChallengesCalendar> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CLSChallengesCalendar> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static CLSChallengesCalendar update(
		CLSChallengesCalendar clsChallengesCalendar) throws SystemException {
		return getPersistence().update(clsChallengesCalendar);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static CLSChallengesCalendar update(
		CLSChallengesCalendar clsChallengesCalendar,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(clsChallengesCalendar, serviceContext);
	}

	/**
	* Caches the c l s challenges calendar in the entity cache if it is enabled.
	*
	* @param clsChallengesCalendar the c l s challenges calendar
	*/
	public static void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengesCalendar clsChallengesCalendar) {
		getPersistence().cacheResult(clsChallengesCalendar);
	}

	/**
	* Caches the c l s challenges calendars in the entity cache if it is enabled.
	*
	* @param clsChallengesCalendars the c l s challenges calendars
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengesCalendar> clsChallengesCalendars) {
		getPersistence().cacheResult(clsChallengesCalendars);
	}

	/**
	* Creates a new c l s challenges calendar with the primary key. Does not add the c l s challenges calendar to the database.
	*
	* @param challengesCalendarId the primary key for the new c l s challenges calendar
	* @return the new c l s challenges calendar
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengesCalendar create(
		long challengesCalendarId) {
		return getPersistence().create(challengesCalendarId);
	}

	/**
	* Removes the c l s challenges calendar with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param challengesCalendarId the primary key of the c l s challenges calendar
	* @return the c l s challenges calendar that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengesCalendarException if a c l s challenges calendar with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengesCalendar remove(
		long challengesCalendarId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengesCalendarException {
		return getPersistence().remove(challengesCalendarId);
	}

	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengesCalendar updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengesCalendar clsChallengesCalendar)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(clsChallengesCalendar);
	}

	/**
	* Returns the c l s challenges calendar with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengesCalendarException} if it could not be found.
	*
	* @param challengesCalendarId the primary key of the c l s challenges calendar
	* @return the c l s challenges calendar
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengesCalendarException if a c l s challenges calendar with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengesCalendar findByPrimaryKey(
		long challengesCalendarId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengesCalendarException {
		return getPersistence().findByPrimaryKey(challengesCalendarId);
	}

	/**
	* Returns the c l s challenges calendar with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param challengesCalendarId the primary key of the c l s challenges calendar
	* @return the c l s challenges calendar, or <code>null</code> if a c l s challenges calendar with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengesCalendar fetchByPrimaryKey(
		long challengesCalendarId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(challengesCalendarId);
	}

	/**
	* Returns all the c l s challenges calendars.
	*
	* @return the c l s challenges calendars
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengesCalendar> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the c l s challenges calendars.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengesCalendarModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s challenges calendars
	* @param end the upper bound of the range of c l s challenges calendars (not inclusive)
	* @return the range of c l s challenges calendars
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengesCalendar> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the c l s challenges calendars.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengesCalendarModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s challenges calendars
	* @param end the upper bound of the range of c l s challenges calendars (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of c l s challenges calendars
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengesCalendar> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the c l s challenges calendars from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of c l s challenges calendars.
	*
	* @return the number of c l s challenges calendars
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static CLSChallengesCalendarPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (CLSChallengesCalendarPersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.getServletContextName(),
					CLSChallengesCalendarPersistence.class.getName());

			ReferenceRegistry.registerReference(CLSChallengesCalendarUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(CLSChallengesCalendarPersistence persistence) {
	}

	private static CLSChallengesCalendarPersistence _persistence;
}