/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.eng.rspa.ideas.challenges.servicelayer.service.http.IdeaEvaluationServiceSoap}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.http.IdeaEvaluationServiceSoap
 * @generated
 */
public class IdeaEvaluationSoap implements Serializable {
	public static IdeaEvaluationSoap toSoapModel(IdeaEvaluation model) {
		IdeaEvaluationSoap soapModel = new IdeaEvaluationSoap();

		soapModel.setCriteriaId(model.getCriteriaId());
		soapModel.setIdeaId(model.getIdeaId());
		soapModel.setMotivation(model.getMotivation());
		soapModel.setPassed(model.getPassed());
		soapModel.setScore(model.getScore());
		soapModel.setDate(model.getDate());
		soapModel.setAuthorId(model.getAuthorId());

		return soapModel;
	}

	public static IdeaEvaluationSoap[] toSoapModels(IdeaEvaluation[] models) {
		IdeaEvaluationSoap[] soapModels = new IdeaEvaluationSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static IdeaEvaluationSoap[][] toSoapModels(IdeaEvaluation[][] models) {
		IdeaEvaluationSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new IdeaEvaluationSoap[models.length][models[0].length];
		}
		else {
			soapModels = new IdeaEvaluationSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static IdeaEvaluationSoap[] toSoapModels(List<IdeaEvaluation> models) {
		List<IdeaEvaluationSoap> soapModels = new ArrayList<IdeaEvaluationSoap>(models.size());

		for (IdeaEvaluation model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new IdeaEvaluationSoap[soapModels.size()]);
	}

	public IdeaEvaluationSoap() {
	}

	public IdeaEvaluationPK getPrimaryKey() {
		return new IdeaEvaluationPK(_criteriaId, _ideaId);
	}

	public void setPrimaryKey(IdeaEvaluationPK pk) {
		setCriteriaId(pk.criteriaId);
		setIdeaId(pk.ideaId);
	}

	public long getCriteriaId() {
		return _criteriaId;
	}

	public void setCriteriaId(long criteriaId) {
		_criteriaId = criteriaId;
	}

	public long getIdeaId() {
		return _ideaId;
	}

	public void setIdeaId(long ideaId) {
		_ideaId = ideaId;
	}

	public String getMotivation() {
		return _motivation;
	}

	public void setMotivation(String motivation) {
		_motivation = motivation;
	}

	public boolean getPassed() {
		return _passed;
	}

	public boolean isPassed() {
		return _passed;
	}

	public void setPassed(boolean passed) {
		_passed = passed;
	}

	public double getScore() {
		return _score;
	}

	public void setScore(double score) {
		_score = score;
	}

	public Date getDate() {
		return _date;
	}

	public void setDate(Date date) {
		_date = date;
	}

	public long getAuthorId() {
		return _authorId;
	}

	public void setAuthorId(long authorId) {
		_authorId = authorId;
	}

	private long _criteriaId;
	private long _ideaId;
	private String _motivation;
	private boolean _passed;
	private double _score;
	private Date _date;
	private long _authorId;
}