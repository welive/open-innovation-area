/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CLSChallengePoi}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSChallengePoi
 * @generated
 */
public class CLSChallengePoiWrapper implements CLSChallengePoi,
	ModelWrapper<CLSChallengePoi> {
	public CLSChallengePoiWrapper(CLSChallengePoi clsChallengePoi) {
		_clsChallengePoi = clsChallengePoi;
	}

	@Override
	public Class<?> getModelClass() {
		return CLSChallengePoi.class;
	}

	@Override
	public String getModelClassName() {
		return CLSChallengePoi.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("challengeId", getChallengeId());
		attributes.put("latitude", getLatitude());
		attributes.put("longitude", getLongitude());
		attributes.put("description", getDescription());
		attributes.put("poiId", getPoiId());
		attributes.put("title", getTitle());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long challengeId = (Long)attributes.get("challengeId");

		if (challengeId != null) {
			setChallengeId(challengeId);
		}

		String latitude = (String)attributes.get("latitude");

		if (latitude != null) {
			setLatitude(latitude);
		}

		String longitude = (String)attributes.get("longitude");

		if (longitude != null) {
			setLongitude(longitude);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		Long poiId = (Long)attributes.get("poiId");

		if (poiId != null) {
			setPoiId(poiId);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}
	}

	/**
	* Returns the primary key of this c l s challenge poi.
	*
	* @return the primary key of this c l s challenge poi
	*/
	@Override
	public long getPrimaryKey() {
		return _clsChallengePoi.getPrimaryKey();
	}

	/**
	* Sets the primary key of this c l s challenge poi.
	*
	* @param primaryKey the primary key of this c l s challenge poi
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_clsChallengePoi.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the challenge ID of this c l s challenge poi.
	*
	* @return the challenge ID of this c l s challenge poi
	*/
	@Override
	public long getChallengeId() {
		return _clsChallengePoi.getChallengeId();
	}

	/**
	* Sets the challenge ID of this c l s challenge poi.
	*
	* @param challengeId the challenge ID of this c l s challenge poi
	*/
	@Override
	public void setChallengeId(long challengeId) {
		_clsChallengePoi.setChallengeId(challengeId);
	}

	/**
	* Returns the latitude of this c l s challenge poi.
	*
	* @return the latitude of this c l s challenge poi
	*/
	@Override
	public java.lang.String getLatitude() {
		return _clsChallengePoi.getLatitude();
	}

	/**
	* Sets the latitude of this c l s challenge poi.
	*
	* @param latitude the latitude of this c l s challenge poi
	*/
	@Override
	public void setLatitude(java.lang.String latitude) {
		_clsChallengePoi.setLatitude(latitude);
	}

	/**
	* Returns the longitude of this c l s challenge poi.
	*
	* @return the longitude of this c l s challenge poi
	*/
	@Override
	public java.lang.String getLongitude() {
		return _clsChallengePoi.getLongitude();
	}

	/**
	* Sets the longitude of this c l s challenge poi.
	*
	* @param longitude the longitude of this c l s challenge poi
	*/
	@Override
	public void setLongitude(java.lang.String longitude) {
		_clsChallengePoi.setLongitude(longitude);
	}

	/**
	* Returns the description of this c l s challenge poi.
	*
	* @return the description of this c l s challenge poi
	*/
	@Override
	public java.lang.String getDescription() {
		return _clsChallengePoi.getDescription();
	}

	/**
	* Sets the description of this c l s challenge poi.
	*
	* @param description the description of this c l s challenge poi
	*/
	@Override
	public void setDescription(java.lang.String description) {
		_clsChallengePoi.setDescription(description);
	}

	/**
	* Returns the poi ID of this c l s challenge poi.
	*
	* @return the poi ID of this c l s challenge poi
	*/
	@Override
	public long getPoiId() {
		return _clsChallengePoi.getPoiId();
	}

	/**
	* Sets the poi ID of this c l s challenge poi.
	*
	* @param poiId the poi ID of this c l s challenge poi
	*/
	@Override
	public void setPoiId(long poiId) {
		_clsChallengePoi.setPoiId(poiId);
	}

	/**
	* Returns the title of this c l s challenge poi.
	*
	* @return the title of this c l s challenge poi
	*/
	@Override
	public java.lang.String getTitle() {
		return _clsChallengePoi.getTitle();
	}

	/**
	* Sets the title of this c l s challenge poi.
	*
	* @param title the title of this c l s challenge poi
	*/
	@Override
	public void setTitle(java.lang.String title) {
		_clsChallengePoi.setTitle(title);
	}

	@Override
	public boolean isNew() {
		return _clsChallengePoi.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_clsChallengePoi.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _clsChallengePoi.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_clsChallengePoi.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _clsChallengePoi.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _clsChallengePoi.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_clsChallengePoi.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _clsChallengePoi.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_clsChallengePoi.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_clsChallengePoi.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_clsChallengePoi.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CLSChallengePoiWrapper((CLSChallengePoi)_clsChallengePoi.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi clsChallengePoi) {
		return _clsChallengePoi.compareTo(clsChallengePoi);
	}

	@Override
	public int hashCode() {
		return _clsChallengePoi.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi> toCacheModel() {
		return _clsChallengePoi.toCacheModel();
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi toEscapedModel() {
		return new CLSChallengePoiWrapper(_clsChallengePoi.toEscapedModel());
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi toUnescapedModel() {
		return new CLSChallengePoiWrapper(_clsChallengePoi.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _clsChallengePoi.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _clsChallengePoi.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_clsChallengePoi.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSChallengePoiWrapper)) {
			return false;
		}

		CLSChallengePoiWrapper clsChallengePoiWrapper = (CLSChallengePoiWrapper)obj;

		if (Validator.equals(_clsChallengePoi,
					clsChallengePoiWrapper._clsChallengePoi)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public CLSChallengePoi getWrappedCLSChallengePoi() {
		return _clsChallengePoi;
	}

	@Override
	public CLSChallengePoi getWrappedModel() {
		return _clsChallengePoi;
	}

	@Override
	public void resetOriginalValues() {
		_clsChallengePoi.resetOriginalValues();
	}

	private CLSChallengePoi _clsChallengePoi;
}