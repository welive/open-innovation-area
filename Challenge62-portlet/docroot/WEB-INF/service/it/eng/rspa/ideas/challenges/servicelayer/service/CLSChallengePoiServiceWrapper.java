/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CLSChallengePoiService}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSChallengePoiService
 * @generated
 */
public class CLSChallengePoiServiceWrapper implements CLSChallengePoiService,
	ServiceWrapper<CLSChallengePoiService> {
	public CLSChallengePoiServiceWrapper(
		CLSChallengePoiService clsChallengePoiService) {
		_clsChallengePoiService = clsChallengePoiService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _clsChallengePoiService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_clsChallengePoiService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _clsChallengePoiService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CLSChallengePoiService getWrappedCLSChallengePoiService() {
		return _clsChallengePoiService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCLSChallengePoiService(
		CLSChallengePoiService clsChallengePoiService) {
		_clsChallengePoiService = clsChallengePoiService;
	}

	@Override
	public CLSChallengePoiService getWrappedService() {
		return _clsChallengePoiService;
	}

	@Override
	public void setWrappedService(CLSChallengePoiService clsChallengePoiService) {
		_clsChallengePoiService = clsChallengePoiService;
	}

	private CLSChallengePoiService _clsChallengePoiService;
}