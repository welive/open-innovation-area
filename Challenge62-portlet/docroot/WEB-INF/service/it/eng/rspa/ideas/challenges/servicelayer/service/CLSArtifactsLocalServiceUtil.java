/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for CLSArtifacts. This utility wraps
 * {@link it.eng.rspa.ideas.challenges.servicelayer.service.impl.CLSArtifactsLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSArtifactsLocalService
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSArtifactsLocalServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.impl.CLSArtifactsLocalServiceImpl
 * @generated
 */
public class CLSArtifactsLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.eng.rspa.ideas.challenges.servicelayer.service.impl.CLSArtifactsLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the c l s artifacts to the database. Also notifies the appropriate model listeners.
	*
	* @param clsArtifacts the c l s artifacts
	* @return the c l s artifacts that was added
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts addCLSArtifacts(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts clsArtifacts)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addCLSArtifacts(clsArtifacts);
	}

	/**
	* Creates a new c l s artifacts with the primary key. Does not add the c l s artifacts to the database.
	*
	* @param clsArtifactsPK the primary key for the new c l s artifacts
	* @return the new c l s artifacts
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts createCLSArtifacts(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK clsArtifactsPK) {
		return getService().createCLSArtifacts(clsArtifactsPK);
	}

	/**
	* Deletes the c l s artifacts with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param clsArtifactsPK the primary key of the c l s artifacts
	* @return the c l s artifacts that was removed
	* @throws PortalException if a c l s artifacts with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts deleteCLSArtifacts(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK clsArtifactsPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteCLSArtifacts(clsArtifactsPK);
	}

	/**
	* Deletes the c l s artifacts from the database. Also notifies the appropriate model listeners.
	*
	* @param clsArtifacts the c l s artifacts
	* @return the c l s artifacts that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts deleteCLSArtifacts(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts clsArtifacts)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteCLSArtifacts(clsArtifacts);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts fetchCLSArtifacts(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK clsArtifactsPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchCLSArtifacts(clsArtifactsPK);
	}

	/**
	* Returns the c l s artifacts with the primary key.
	*
	* @param clsArtifactsPK the primary key of the c l s artifacts
	* @return the c l s artifacts
	* @throws PortalException if a c l s artifacts with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts getCLSArtifacts(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK clsArtifactsPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getCLSArtifacts(clsArtifactsPK);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the c l s artifactses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s artifactses
	* @param end the upper bound of the range of c l s artifactses (not inclusive)
	* @return the range of c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> getCLSArtifactses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getCLSArtifactses(start, end);
	}

	/**
	* Returns the number of c l s artifactses.
	*
	* @return the number of c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public static int getCLSArtifactsesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getCLSArtifactsesCount();
	}

	/**
	* Updates the c l s artifacts in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param clsArtifacts the c l s artifacts
	* @return the c l s artifacts that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts updateCLSArtifacts(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts clsArtifacts)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateCLSArtifacts(clsArtifacts);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> getArtifactsByIdeaId(
		java.lang.Long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getArtifactsByIdeaId(ideaId);
	}

	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> getArtifactsByArtifactId(
		java.lang.Long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getArtifactsByArtifactId(artifactId);
	}

	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> getArtifactsByIdeaIdEArtifactId(
		java.lang.Long ideaId, java.lang.Long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getArtifactsByIdeaIdEArtifactId(ideaId, artifactId);
	}

	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> getFavouriteIdeas(
		java.lang.Long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getFavouriteIdeas(ideaId);
	}

	/**
	* @param artifactId
	*/
	public static void unlinkArtifactAndIdeas(long artifactId) {
		getService().unlinkArtifactAndIdeas(artifactId);
	}

	public static void clearService() {
		_service = null;
	}

	public static CLSArtifactsLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					CLSArtifactsLocalService.class.getName());

			if (invokableLocalService instanceof CLSArtifactsLocalService) {
				_service = (CLSArtifactsLocalService)invokableLocalService;
			}
			else {
				_service = new CLSArtifactsLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(CLSArtifactsLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(CLSArtifactsLocalService service) {
	}

	private static CLSArtifactsLocalService _service;
}