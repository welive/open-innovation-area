/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for IdeaEvaluation. This utility wraps
 * {@link it.eng.rspa.ideas.challenges.servicelayer.service.impl.IdeaEvaluationLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see IdeaEvaluationLocalService
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.IdeaEvaluationLocalServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.impl.IdeaEvaluationLocalServiceImpl
 * @generated
 */
public class IdeaEvaluationLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.eng.rspa.ideas.challenges.servicelayer.service.impl.IdeaEvaluationLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the idea evaluation to the database. Also notifies the appropriate model listeners.
	*
	* @param ideaEvaluation the idea evaluation
	* @return the idea evaluation that was added
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation addIdeaEvaluation(
		it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation ideaEvaluation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addIdeaEvaluation(ideaEvaluation);
	}

	/**
	* Creates a new idea evaluation with the primary key. Does not add the idea evaluation to the database.
	*
	* @param ideaEvaluationPK the primary key for the new idea evaluation
	* @return the new idea evaluation
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation createIdeaEvaluation(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK ideaEvaluationPK) {
		return getService().createIdeaEvaluation(ideaEvaluationPK);
	}

	/**
	* Deletes the idea evaluation with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ideaEvaluationPK the primary key of the idea evaluation
	* @return the idea evaluation that was removed
	* @throws PortalException if a idea evaluation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation deleteIdeaEvaluation(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK ideaEvaluationPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteIdeaEvaluation(ideaEvaluationPK);
	}

	/**
	* Deletes the idea evaluation from the database. Also notifies the appropriate model listeners.
	*
	* @param ideaEvaluation the idea evaluation
	* @return the idea evaluation that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation deleteIdeaEvaluation(
		it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation ideaEvaluation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteIdeaEvaluation(ideaEvaluation);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation fetchIdeaEvaluation(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK ideaEvaluationPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchIdeaEvaluation(ideaEvaluationPK);
	}

	/**
	* Returns the idea evaluation with the primary key.
	*
	* @param ideaEvaluationPK the primary key of the idea evaluation
	* @return the idea evaluation
	* @throws PortalException if a idea evaluation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation getIdeaEvaluation(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK ideaEvaluationPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getIdeaEvaluation(ideaEvaluationPK);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the idea evaluations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of idea evaluations
	* @param end the upper bound of the range of idea evaluations (not inclusive)
	* @return the range of idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> getIdeaEvaluations(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getIdeaEvaluations(start, end);
	}

	/**
	* Returns the number of idea evaluations.
	*
	* @return the number of idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public static int getIdeaEvaluationsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getIdeaEvaluationsCount();
	}

	/**
	* Updates the idea evaluation in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param ideaEvaluation the idea evaluation
	* @return the idea evaluation that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation updateIdeaEvaluation(
		it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation ideaEvaluation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateIdeaEvaluation(ideaEvaluation);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	/**
	* @param ideaId
	* @return
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> getIdeaEvaluationsByIdeaId(
		long ideaId) {
		return getService().getIdeaEvaluationsByIdeaId(ideaId);
	}

	/**
	* @param criteriaId
	* @param ideaId
	* @return
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation getIdeaEvaluationByCriteriaIdAndIdeaId(
		long criteriaId, long ideaId) {
		return getService()
				   .getIdeaEvaluationByCriteriaIdAndIdeaId(criteriaId, ideaId);
	}

	/**
	* @param criteriaId
	* @return
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> getIdeaEvaluationsByCriteriaId(
		long criteriaId) {
		return getService().getIdeaEvaluationsByCriteriaId(criteriaId);
	}

	public static void clearService() {
		_service = null;
	}

	public static IdeaEvaluationLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					IdeaEvaluationLocalService.class.getName());

			if (invokableLocalService instanceof IdeaEvaluationLocalService) {
				_service = (IdeaEvaluationLocalService)invokableLocalService;
			}
			else {
				_service = new IdeaEvaluationLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(IdeaEvaluationLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(IdeaEvaluationLocalService service) {
	}

	private static IdeaEvaluationLocalService _service;
}