/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link NeedLinkedChallengeService}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see NeedLinkedChallengeService
 * @generated
 */
public class NeedLinkedChallengeServiceWrapper
	implements NeedLinkedChallengeService,
		ServiceWrapper<NeedLinkedChallengeService> {
	public NeedLinkedChallengeServiceWrapper(
		NeedLinkedChallengeService needLinkedChallengeService) {
		_needLinkedChallengeService = needLinkedChallengeService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _needLinkedChallengeService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_needLinkedChallengeService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _needLinkedChallengeService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public NeedLinkedChallengeService getWrappedNeedLinkedChallengeService() {
		return _needLinkedChallengeService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedNeedLinkedChallengeService(
		NeedLinkedChallengeService needLinkedChallengeService) {
		_needLinkedChallengeService = needLinkedChallengeService;
	}

	@Override
	public NeedLinkedChallengeService getWrappedService() {
		return _needLinkedChallengeService;
	}

	@Override
	public void setWrappedService(
		NeedLinkedChallengeService needLinkedChallengeService) {
		_needLinkedChallengeService = needLinkedChallengeService;
	}

	private NeedLinkedChallengeService _needLinkedChallengeService;
}