/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CLSVmeProjectsLocalService}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSVmeProjectsLocalService
 * @generated
 */
public class CLSVmeProjectsLocalServiceWrapper
	implements CLSVmeProjectsLocalService,
		ServiceWrapper<CLSVmeProjectsLocalService> {
	public CLSVmeProjectsLocalServiceWrapper(
		CLSVmeProjectsLocalService clsVmeProjectsLocalService) {
		_clsVmeProjectsLocalService = clsVmeProjectsLocalService;
	}

	/**
	* Adds the c l s vme projects to the database. Also notifies the appropriate model listeners.
	*
	* @param clsVmeProjects the c l s vme projects
	* @return the c l s vme projects that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects addCLSVmeProjects(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects clsVmeProjects)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsVmeProjectsLocalService.addCLSVmeProjects(clsVmeProjects);
	}

	/**
	* Creates a new c l s vme projects with the primary key. Does not add the c l s vme projects to the database.
	*
	* @param recordId the primary key for the new c l s vme projects
	* @return the new c l s vme projects
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects createCLSVmeProjects(
		long recordId) {
		return _clsVmeProjectsLocalService.createCLSVmeProjects(recordId);
	}

	/**
	* Deletes the c l s vme projects with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param recordId the primary key of the c l s vme projects
	* @return the c l s vme projects that was removed
	* @throws PortalException if a c l s vme projects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects deleteCLSVmeProjects(
		long recordId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _clsVmeProjectsLocalService.deleteCLSVmeProjects(recordId);
	}

	/**
	* Deletes the c l s vme projects from the database. Also notifies the appropriate model listeners.
	*
	* @param clsVmeProjects the c l s vme projects
	* @return the c l s vme projects that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects deleteCLSVmeProjects(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects clsVmeProjects)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsVmeProjectsLocalService.deleteCLSVmeProjects(clsVmeProjects);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _clsVmeProjectsLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsVmeProjectsLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSVmeProjectsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsVmeProjectsLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSVmeProjectsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsVmeProjectsLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsVmeProjectsLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsVmeProjectsLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects fetchCLSVmeProjects(
		long recordId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsVmeProjectsLocalService.fetchCLSVmeProjects(recordId);
	}

	/**
	* Returns the c l s vme projects with the primary key.
	*
	* @param recordId the primary key of the c l s vme projects
	* @return the c l s vme projects
	* @throws PortalException if a c l s vme projects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects getCLSVmeProjects(
		long recordId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _clsVmeProjectsLocalService.getCLSVmeProjects(recordId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _clsVmeProjectsLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the c l s vme projectses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSVmeProjectsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s vme projectses
	* @param end the upper bound of the range of c l s vme projectses (not inclusive)
	* @return the range of c l s vme projectses
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects> getCLSVmeProjectses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsVmeProjectsLocalService.getCLSVmeProjectses(start, end);
	}

	/**
	* Returns the number of c l s vme projectses.
	*
	* @return the number of c l s vme projectses
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getCLSVmeProjectsesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsVmeProjectsLocalService.getCLSVmeProjectsesCount();
	}

	/**
	* Updates the c l s vme projects in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param clsVmeProjects the c l s vme projects
	* @return the c l s vme projects that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects updateCLSVmeProjects(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects clsVmeProjects)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsVmeProjectsLocalService.updateCLSVmeProjects(clsVmeProjects);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _clsVmeProjectsLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_clsVmeProjectsLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _clsVmeProjectsLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects> getMokcupsByIdeaId(
		long ideaId) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsVmeProjectsLocalService.getMokcupsByIdeaId(ideaId);
	}

	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects> getMashupsByIdeaId(
		long ideaId) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsVmeProjectsLocalService.getMashupsByIdeaId(ideaId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CLSVmeProjectsLocalService getWrappedCLSVmeProjectsLocalService() {
		return _clsVmeProjectsLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCLSVmeProjectsLocalService(
		CLSVmeProjectsLocalService clsVmeProjectsLocalService) {
		_clsVmeProjectsLocalService = clsVmeProjectsLocalService;
	}

	@Override
	public CLSVmeProjectsLocalService getWrappedService() {
		return _clsVmeProjectsLocalService;
	}

	@Override
	public void setWrappedService(
		CLSVmeProjectsLocalService clsVmeProjectsLocalService) {
		_clsVmeProjectsLocalService = clsVmeProjectsLocalService;
	}

	private CLSVmeProjectsLocalService _clsVmeProjectsLocalService;
}