/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK;

import java.io.Serializable;

/**
 * The base model interface for the CLSArtifacts service. Represents a row in the &quot;CSL_CLSArtifacts&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsImpl}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSArtifacts
 * @see it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl
 * @generated
 */
public interface CLSArtifactsModel extends BaseModel<CLSArtifacts> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a c l s artifacts model instance should use the {@link CLSArtifacts} interface instead.
	 */

	/**
	 * Returns the primary key of this c l s artifacts.
	 *
	 * @return the primary key of this c l s artifacts
	 */
	public CLSArtifactsPK getPrimaryKey();

	/**
	 * Sets the primary key of this c l s artifacts.
	 *
	 * @param primaryKey the primary key of this c l s artifacts
	 */
	public void setPrimaryKey(CLSArtifactsPK primaryKey);

	/**
	 * Returns the idea ID of this c l s artifacts.
	 *
	 * @return the idea ID of this c l s artifacts
	 */
	public long getIdeaId();

	/**
	 * Sets the idea ID of this c l s artifacts.
	 *
	 * @param ideaId the idea ID of this c l s artifacts
	 */
	public void setIdeaId(long ideaId);

	/**
	 * Returns the artifact ID of this c l s artifacts.
	 *
	 * @return the artifact ID of this c l s artifacts
	 */
	public long getArtifactId();

	/**
	 * Sets the artifact ID of this c l s artifacts.
	 *
	 * @param artifactId the artifact ID of this c l s artifacts
	 */
	public void setArtifactId(long artifactId);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts clsArtifacts);

	@Override
	public int hashCode();

	@Override
	public CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> toCacheModel();

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts toEscapedModel();

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}