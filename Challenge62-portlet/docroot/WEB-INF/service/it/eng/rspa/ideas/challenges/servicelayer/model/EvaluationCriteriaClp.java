/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer;
import it.eng.rspa.ideas.challenges.servicelayer.service.EvaluationCriteriaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class EvaluationCriteriaClp extends BaseModelImpl<EvaluationCriteria>
	implements EvaluationCriteria {
	public EvaluationCriteriaClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return EvaluationCriteria.class;
	}

	@Override
	public String getModelClassName() {
		return EvaluationCriteria.class.getName();
	}

	@Override
	public EvaluationCriteriaPK getPrimaryKey() {
		return new EvaluationCriteriaPK(_criteriaId, _challengeId);
	}

	@Override
	public void setPrimaryKey(EvaluationCriteriaPK primaryKey) {
		setCriteriaId(primaryKey.criteriaId);
		setChallengeId(primaryKey.challengeId);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new EvaluationCriteriaPK(_criteriaId, _challengeId);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((EvaluationCriteriaPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("criteriaId", getCriteriaId());
		attributes.put("challengeId", getChallengeId());
		attributes.put("enabled", getEnabled());
		attributes.put("description", getDescription());
		attributes.put("isBarriera", getIsBarriera());
		attributes.put("isCustom", getIsCustom());
		attributes.put("language", getLanguage());
		attributes.put("inputId", getInputId());
		attributes.put("weight", getWeight());
		attributes.put("threshold", getThreshold());
		attributes.put("date", getDate());
		attributes.put("authorId", getAuthorId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long criteriaId = (Long)attributes.get("criteriaId");

		if (criteriaId != null) {
			setCriteriaId(criteriaId);
		}

		Long challengeId = (Long)attributes.get("challengeId");

		if (challengeId != null) {
			setChallengeId(challengeId);
		}

		Boolean enabled = (Boolean)attributes.get("enabled");

		if (enabled != null) {
			setEnabled(enabled);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		Boolean isBarriera = (Boolean)attributes.get("isBarriera");

		if (isBarriera != null) {
			setIsBarriera(isBarriera);
		}

		Boolean isCustom = (Boolean)attributes.get("isCustom");

		if (isCustom != null) {
			setIsCustom(isCustom);
		}

		String language = (String)attributes.get("language");

		if (language != null) {
			setLanguage(language);
		}

		String inputId = (String)attributes.get("inputId");

		if (inputId != null) {
			setInputId(inputId);
		}

		Double weight = (Double)attributes.get("weight");

		if (weight != null) {
			setWeight(weight);
		}

		Integer threshold = (Integer)attributes.get("threshold");

		if (threshold != null) {
			setThreshold(threshold);
		}

		Date date = (Date)attributes.get("date");

		if (date != null) {
			setDate(date);
		}

		Long authorId = (Long)attributes.get("authorId");

		if (authorId != null) {
			setAuthorId(authorId);
		}
	}

	@Override
	public long getCriteriaId() {
		return _criteriaId;
	}

	@Override
	public void setCriteriaId(long criteriaId) {
		_criteriaId = criteriaId;

		if (_evaluationCriteriaRemoteModel != null) {
			try {
				Class<?> clazz = _evaluationCriteriaRemoteModel.getClass();

				Method method = clazz.getMethod("setCriteriaId", long.class);

				method.invoke(_evaluationCriteriaRemoteModel, criteriaId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getChallengeId() {
		return _challengeId;
	}

	@Override
	public void setChallengeId(long challengeId) {
		_challengeId = challengeId;

		if (_evaluationCriteriaRemoteModel != null) {
			try {
				Class<?> clazz = _evaluationCriteriaRemoteModel.getClass();

				Method method = clazz.getMethod("setChallengeId", long.class);

				method.invoke(_evaluationCriteriaRemoteModel, challengeId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getEnabled() {
		return _enabled;
	}

	@Override
	public boolean isEnabled() {
		return _enabled;
	}

	@Override
	public void setEnabled(boolean enabled) {
		_enabled = enabled;

		if (_evaluationCriteriaRemoteModel != null) {
			try {
				Class<?> clazz = _evaluationCriteriaRemoteModel.getClass();

				Method method = clazz.getMethod("setEnabled", boolean.class);

				method.invoke(_evaluationCriteriaRemoteModel, enabled);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescription() {
		return _description;
	}

	@Override
	public void setDescription(String description) {
		_description = description;

		if (_evaluationCriteriaRemoteModel != null) {
			try {
				Class<?> clazz = _evaluationCriteriaRemoteModel.getClass();

				Method method = clazz.getMethod("setDescription", String.class);

				method.invoke(_evaluationCriteriaRemoteModel, description);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getIsBarriera() {
		return _isBarriera;
	}

	@Override
	public boolean isIsBarriera() {
		return _isBarriera;
	}

	@Override
	public void setIsBarriera(boolean isBarriera) {
		_isBarriera = isBarriera;

		if (_evaluationCriteriaRemoteModel != null) {
			try {
				Class<?> clazz = _evaluationCriteriaRemoteModel.getClass();

				Method method = clazz.getMethod("setIsBarriera", boolean.class);

				method.invoke(_evaluationCriteriaRemoteModel, isBarriera);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getIsCustom() {
		return _isCustom;
	}

	@Override
	public boolean isIsCustom() {
		return _isCustom;
	}

	@Override
	public void setIsCustom(boolean isCustom) {
		_isCustom = isCustom;

		if (_evaluationCriteriaRemoteModel != null) {
			try {
				Class<?> clazz = _evaluationCriteriaRemoteModel.getClass();

				Method method = clazz.getMethod("setIsCustom", boolean.class);

				method.invoke(_evaluationCriteriaRemoteModel, isCustom);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLanguage() {
		return _language;
	}

	@Override
	public void setLanguage(String language) {
		_language = language;

		if (_evaluationCriteriaRemoteModel != null) {
			try {
				Class<?> clazz = _evaluationCriteriaRemoteModel.getClass();

				Method method = clazz.getMethod("setLanguage", String.class);

				method.invoke(_evaluationCriteriaRemoteModel, language);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getInputId() {
		return _inputId;
	}

	@Override
	public void setInputId(String inputId) {
		_inputId = inputId;

		if (_evaluationCriteriaRemoteModel != null) {
			try {
				Class<?> clazz = _evaluationCriteriaRemoteModel.getClass();

				Method method = clazz.getMethod("setInputId", String.class);

				method.invoke(_evaluationCriteriaRemoteModel, inputId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getWeight() {
		return _weight;
	}

	@Override
	public void setWeight(double weight) {
		_weight = weight;

		if (_evaluationCriteriaRemoteModel != null) {
			try {
				Class<?> clazz = _evaluationCriteriaRemoteModel.getClass();

				Method method = clazz.getMethod("setWeight", double.class);

				method.invoke(_evaluationCriteriaRemoteModel, weight);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getThreshold() {
		return _threshold;
	}

	@Override
	public void setThreshold(int threshold) {
		_threshold = threshold;

		if (_evaluationCriteriaRemoteModel != null) {
			try {
				Class<?> clazz = _evaluationCriteriaRemoteModel.getClass();

				Method method = clazz.getMethod("setThreshold", int.class);

				method.invoke(_evaluationCriteriaRemoteModel, threshold);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDate() {
		return _date;
	}

	@Override
	public void setDate(Date date) {
		_date = date;

		if (_evaluationCriteriaRemoteModel != null) {
			try {
				Class<?> clazz = _evaluationCriteriaRemoteModel.getClass();

				Method method = clazz.getMethod("setDate", Date.class);

				method.invoke(_evaluationCriteriaRemoteModel, date);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getAuthorId() {
		return _authorId;
	}

	@Override
	public void setAuthorId(long authorId) {
		_authorId = authorId;

		if (_evaluationCriteriaRemoteModel != null) {
			try {
				Class<?> clazz = _evaluationCriteriaRemoteModel.getClass();

				Method method = clazz.getMethod("setAuthorId", long.class);

				method.invoke(_evaluationCriteriaRemoteModel, authorId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getEvaluationCriteriaRemoteModel() {
		return _evaluationCriteriaRemoteModel;
	}

	public void setEvaluationCriteriaRemoteModel(
		BaseModel<?> evaluationCriteriaRemoteModel) {
		_evaluationCriteriaRemoteModel = evaluationCriteriaRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _evaluationCriteriaRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_evaluationCriteriaRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			EvaluationCriteriaLocalServiceUtil.addEvaluationCriteria(this);
		}
		else {
			EvaluationCriteriaLocalServiceUtil.updateEvaluationCriteria(this);
		}
	}

	@Override
	public EvaluationCriteria toEscapedModel() {
		return (EvaluationCriteria)ProxyUtil.newProxyInstance(EvaluationCriteria.class.getClassLoader(),
			new Class[] { EvaluationCriteria.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		EvaluationCriteriaClp clone = new EvaluationCriteriaClp();

		clone.setCriteriaId(getCriteriaId());
		clone.setChallengeId(getChallengeId());
		clone.setEnabled(getEnabled());
		clone.setDescription(getDescription());
		clone.setIsBarriera(getIsBarriera());
		clone.setIsCustom(getIsCustom());
		clone.setLanguage(getLanguage());
		clone.setInputId(getInputId());
		clone.setWeight(getWeight());
		clone.setThreshold(getThreshold());
		clone.setDate(getDate());
		clone.setAuthorId(getAuthorId());

		return clone;
	}

	@Override
	public int compareTo(EvaluationCriteria evaluationCriteria) {
		int value = 0;

		if (getCriteriaId() < evaluationCriteria.getCriteriaId()) {
			value = -1;
		}
		else if (getCriteriaId() > evaluationCriteria.getCriteriaId()) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof EvaluationCriteriaClp)) {
			return false;
		}

		EvaluationCriteriaClp evaluationCriteria = (EvaluationCriteriaClp)obj;

		EvaluationCriteriaPK primaryKey = evaluationCriteria.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{criteriaId=");
		sb.append(getCriteriaId());
		sb.append(", challengeId=");
		sb.append(getChallengeId());
		sb.append(", enabled=");
		sb.append(getEnabled());
		sb.append(", description=");
		sb.append(getDescription());
		sb.append(", isBarriera=");
		sb.append(getIsBarriera());
		sb.append(", isCustom=");
		sb.append(getIsCustom());
		sb.append(", language=");
		sb.append(getLanguage());
		sb.append(", inputId=");
		sb.append(getInputId());
		sb.append(", weight=");
		sb.append(getWeight());
		sb.append(", threshold=");
		sb.append(getThreshold());
		sb.append(", date=");
		sb.append(getDate());
		sb.append(", authorId=");
		sb.append(getAuthorId());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(40);

		sb.append("<model><model-name>");
		sb.append(
			"it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>criteriaId</column-name><column-value><![CDATA[");
		sb.append(getCriteriaId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>challengeId</column-name><column-value><![CDATA[");
		sb.append(getChallengeId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>enabled</column-name><column-value><![CDATA[");
		sb.append(getEnabled());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>description</column-name><column-value><![CDATA[");
		sb.append(getDescription());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>isBarriera</column-name><column-value><![CDATA[");
		sb.append(getIsBarriera());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>isCustom</column-name><column-value><![CDATA[");
		sb.append(getIsCustom());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>language</column-name><column-value><![CDATA[");
		sb.append(getLanguage());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>inputId</column-name><column-value><![CDATA[");
		sb.append(getInputId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>weight</column-name><column-value><![CDATA[");
		sb.append(getWeight());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>threshold</column-name><column-value><![CDATA[");
		sb.append(getThreshold());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>date</column-name><column-value><![CDATA[");
		sb.append(getDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>authorId</column-name><column-value><![CDATA[");
		sb.append(getAuthorId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _criteriaId;
	private long _challengeId;
	private boolean _enabled;
	private String _description;
	private boolean _isBarriera;
	private boolean _isCustom;
	private String _language;
	private String _inputId;
	private double _weight;
	private int _threshold;
	private Date _date;
	private long _authorId;
	private BaseModel<?> _evaluationCriteriaRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.class;
}