/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteChallengesLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class CLSFavouriteChallengesClp extends BaseModelImpl<CLSFavouriteChallenges>
	implements CLSFavouriteChallenges {
	public CLSFavouriteChallengesClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return CLSFavouriteChallenges.class;
	}

	@Override
	public String getModelClassName() {
		return CLSFavouriteChallenges.class.getName();
	}

	@Override
	public CLSFavouriteChallengesPK getPrimaryKey() {
		return new CLSFavouriteChallengesPK(_challengeId, _userId);
	}

	@Override
	public void setPrimaryKey(CLSFavouriteChallengesPK primaryKey) {
		setChallengeId(primaryKey.challengeId);
		setUserId(primaryKey.userId);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new CLSFavouriteChallengesPK(_challengeId, _userId);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((CLSFavouriteChallengesPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("challengeId", getChallengeId());
		attributes.put("userId", getUserId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long challengeId = (Long)attributes.get("challengeId");

		if (challengeId != null) {
			setChallengeId(challengeId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}
	}

	@Override
	public long getChallengeId() {
		return _challengeId;
	}

	@Override
	public void setChallengeId(long challengeId) {
		_challengeId = challengeId;

		if (_clsFavouriteChallengesRemoteModel != null) {
			try {
				Class<?> clazz = _clsFavouriteChallengesRemoteModel.getClass();

				Method method = clazz.getMethod("setChallengeId", long.class);

				method.invoke(_clsFavouriteChallengesRemoteModel, challengeId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;

		if (_clsFavouriteChallengesRemoteModel != null) {
			try {
				Class<?> clazz = _clsFavouriteChallengesRemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", long.class);

				method.invoke(_clsFavouriteChallengesRemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public BaseModel<?> getCLSFavouriteChallengesRemoteModel() {
		return _clsFavouriteChallengesRemoteModel;
	}

	public void setCLSFavouriteChallengesRemoteModel(
		BaseModel<?> clsFavouriteChallengesRemoteModel) {
		_clsFavouriteChallengesRemoteModel = clsFavouriteChallengesRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _clsFavouriteChallengesRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_clsFavouriteChallengesRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CLSFavouriteChallengesLocalServiceUtil.addCLSFavouriteChallenges(this);
		}
		else {
			CLSFavouriteChallengesLocalServiceUtil.updateCLSFavouriteChallenges(this);
		}
	}

	@Override
	public CLSFavouriteChallenges toEscapedModel() {
		return (CLSFavouriteChallenges)ProxyUtil.newProxyInstance(CLSFavouriteChallenges.class.getClassLoader(),
			new Class[] { CLSFavouriteChallenges.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CLSFavouriteChallengesClp clone = new CLSFavouriteChallengesClp();

		clone.setChallengeId(getChallengeId());
		clone.setUserId(getUserId());

		return clone;
	}

	@Override
	public int compareTo(CLSFavouriteChallenges clsFavouriteChallenges) {
		CLSFavouriteChallengesPK primaryKey = clsFavouriteChallenges.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSFavouriteChallengesClp)) {
			return false;
		}

		CLSFavouriteChallengesClp clsFavouriteChallenges = (CLSFavouriteChallengesClp)obj;

		CLSFavouriteChallengesPK primaryKey = clsFavouriteChallenges.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{challengeId=");
		sb.append(getChallengeId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(10);

		sb.append("<model><model-name>");
		sb.append(
			"it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>challengeId</column-name><column-value><![CDATA[");
		sb.append(getChallengeId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _challengeId;
	private long _userId;
	private String _userUuid;
	private BaseModel<?> _clsFavouriteChallengesRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.class;
}