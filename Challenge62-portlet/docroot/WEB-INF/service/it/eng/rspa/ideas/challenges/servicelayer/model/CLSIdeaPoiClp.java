/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaPoiLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class CLSIdeaPoiClp extends BaseModelImpl<CLSIdeaPoi>
	implements CLSIdeaPoi {
	public CLSIdeaPoiClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return CLSIdeaPoi.class;
	}

	@Override
	public String getModelClassName() {
		return CLSIdeaPoi.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _poiId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setPoiId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _poiId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ideaId", getIdeaId());
		attributes.put("latitude", getLatitude());
		attributes.put("longitude", getLongitude());
		attributes.put("description", getDescription());
		attributes.put("poiId", getPoiId());
		attributes.put("title", getTitle());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ideaId = (Long)attributes.get("ideaId");

		if (ideaId != null) {
			setIdeaId(ideaId);
		}

		String latitude = (String)attributes.get("latitude");

		if (latitude != null) {
			setLatitude(latitude);
		}

		String longitude = (String)attributes.get("longitude");

		if (longitude != null) {
			setLongitude(longitude);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		Long poiId = (Long)attributes.get("poiId");

		if (poiId != null) {
			setPoiId(poiId);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}
	}

	@Override
	public long getIdeaId() {
		return _ideaId;
	}

	@Override
	public void setIdeaId(long ideaId) {
		_ideaId = ideaId;

		if (_clsIdeaPoiRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaPoiRemoteModel.getClass();

				Method method = clazz.getMethod("setIdeaId", long.class);

				method.invoke(_clsIdeaPoiRemoteModel, ideaId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLatitude() {
		return _latitude;
	}

	@Override
	public void setLatitude(String latitude) {
		_latitude = latitude;

		if (_clsIdeaPoiRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaPoiRemoteModel.getClass();

				Method method = clazz.getMethod("setLatitude", String.class);

				method.invoke(_clsIdeaPoiRemoteModel, latitude);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLongitude() {
		return _longitude;
	}

	@Override
	public void setLongitude(String longitude) {
		_longitude = longitude;

		if (_clsIdeaPoiRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaPoiRemoteModel.getClass();

				Method method = clazz.getMethod("setLongitude", String.class);

				method.invoke(_clsIdeaPoiRemoteModel, longitude);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescription() {
		return _description;
	}

	@Override
	public void setDescription(String description) {
		_description = description;

		if (_clsIdeaPoiRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaPoiRemoteModel.getClass();

				Method method = clazz.getMethod("setDescription", String.class);

				method.invoke(_clsIdeaPoiRemoteModel, description);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getPoiId() {
		return _poiId;
	}

	@Override
	public void setPoiId(long poiId) {
		_poiId = poiId;

		if (_clsIdeaPoiRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaPoiRemoteModel.getClass();

				Method method = clazz.getMethod("setPoiId", long.class);

				method.invoke(_clsIdeaPoiRemoteModel, poiId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTitle() {
		return _title;
	}

	@Override
	public void setTitle(String title) {
		_title = title;

		if (_clsIdeaPoiRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaPoiRemoteModel.getClass();

				Method method = clazz.getMethod("setTitle", String.class);

				method.invoke(_clsIdeaPoiRemoteModel, title);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getCLSIdeaPoiRemoteModel() {
		return _clsIdeaPoiRemoteModel;
	}

	public void setCLSIdeaPoiRemoteModel(BaseModel<?> clsIdeaPoiRemoteModel) {
		_clsIdeaPoiRemoteModel = clsIdeaPoiRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _clsIdeaPoiRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_clsIdeaPoiRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CLSIdeaPoiLocalServiceUtil.addCLSIdeaPoi(this);
		}
		else {
			CLSIdeaPoiLocalServiceUtil.updateCLSIdeaPoi(this);
		}
	}

	@Override
	public CLSIdeaPoi toEscapedModel() {
		return (CLSIdeaPoi)ProxyUtil.newProxyInstance(CLSIdeaPoi.class.getClassLoader(),
			new Class[] { CLSIdeaPoi.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CLSIdeaPoiClp clone = new CLSIdeaPoiClp();

		clone.setIdeaId(getIdeaId());
		clone.setLatitude(getLatitude());
		clone.setLongitude(getLongitude());
		clone.setDescription(getDescription());
		clone.setPoiId(getPoiId());
		clone.setTitle(getTitle());

		return clone;
	}

	@Override
	public int compareTo(CLSIdeaPoi clsIdeaPoi) {
		long primaryKey = clsIdeaPoi.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSIdeaPoiClp)) {
			return false;
		}

		CLSIdeaPoiClp clsIdeaPoi = (CLSIdeaPoiClp)obj;

		long primaryKey = clsIdeaPoi.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{ideaId=");
		sb.append(getIdeaId());
		sb.append(", latitude=");
		sb.append(getLatitude());
		sb.append(", longitude=");
		sb.append(getLongitude());
		sb.append(", description=");
		sb.append(getDescription());
		sb.append(", poiId=");
		sb.append(getPoiId());
		sb.append(", title=");
		sb.append(getTitle());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(22);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>ideaId</column-name><column-value><![CDATA[");
		sb.append(getIdeaId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>latitude</column-name><column-value><![CDATA[");
		sb.append(getLatitude());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>longitude</column-name><column-value><![CDATA[");
		sb.append(getLongitude());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>description</column-name><column-value><![CDATA[");
		sb.append(getDescription());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>poiId</column-name><column-value><![CDATA[");
		sb.append(getPoiId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>title</column-name><column-value><![CDATA[");
		sb.append(getTitle());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _ideaId;
	private String _latitude;
	private String _longitude;
	private String _description;
	private long _poiId;
	private String _title;
	private BaseModel<?> _clsIdeaPoiRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.class;
}