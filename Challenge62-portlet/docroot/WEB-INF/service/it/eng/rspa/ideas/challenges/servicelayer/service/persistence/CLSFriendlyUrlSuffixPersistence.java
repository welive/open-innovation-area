/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFriendlyUrlSuffix;

/**
 * The persistence interface for the c l s friendly url suffix service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSFriendlyUrlSuffixPersistenceImpl
 * @see CLSFriendlyUrlSuffixUtil
 * @generated
 */
public interface CLSFriendlyUrlSuffixPersistence extends BasePersistence<CLSFriendlyUrlSuffix> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CLSFriendlyUrlSuffixUtil} to access the c l s friendly url suffix persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the c l s friendly url suffix in the entity cache if it is enabled.
	*
	* @param clsFriendlyUrlSuffix the c l s friendly url suffix
	*/
	public void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSFriendlyUrlSuffix clsFriendlyUrlSuffix);

	/**
	* Caches the c l s friendly url suffixs in the entity cache if it is enabled.
	*
	* @param clsFriendlyUrlSuffixs the c l s friendly url suffixs
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFriendlyUrlSuffix> clsFriendlyUrlSuffixs);

	/**
	* Creates a new c l s friendly url suffix with the primary key. Does not add the c l s friendly url suffix to the database.
	*
	* @param friendlyUrlSuffixID the primary key for the new c l s friendly url suffix
	* @return the new c l s friendly url suffix
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFriendlyUrlSuffix create(
		long friendlyUrlSuffixID);

	/**
	* Removes the c l s friendly url suffix with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param friendlyUrlSuffixID the primary key of the c l s friendly url suffix
	* @return the c l s friendly url suffix that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFriendlyUrlSuffixException if a c l s friendly url suffix with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFriendlyUrlSuffix remove(
		long friendlyUrlSuffixID)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFriendlyUrlSuffixException;

	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFriendlyUrlSuffix updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSFriendlyUrlSuffix clsFriendlyUrlSuffix)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the c l s friendly url suffix with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFriendlyUrlSuffixException} if it could not be found.
	*
	* @param friendlyUrlSuffixID the primary key of the c l s friendly url suffix
	* @return the c l s friendly url suffix
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFriendlyUrlSuffixException if a c l s friendly url suffix with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFriendlyUrlSuffix findByPrimaryKey(
		long friendlyUrlSuffixID)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFriendlyUrlSuffixException;

	/**
	* Returns the c l s friendly url suffix with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param friendlyUrlSuffixID the primary key of the c l s friendly url suffix
	* @return the c l s friendly url suffix, or <code>null</code> if a c l s friendly url suffix with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFriendlyUrlSuffix fetchByPrimaryKey(
		long friendlyUrlSuffixID)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the c l s friendly url suffixs.
	*
	* @return the c l s friendly url suffixs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFriendlyUrlSuffix> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the c l s friendly url suffixs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFriendlyUrlSuffixModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s friendly url suffixs
	* @param end the upper bound of the range of c l s friendly url suffixs (not inclusive)
	* @return the range of c l s friendly url suffixs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFriendlyUrlSuffix> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the c l s friendly url suffixs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFriendlyUrlSuffixModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s friendly url suffixs
	* @param end the upper bound of the range of c l s friendly url suffixs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of c l s friendly url suffixs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFriendlyUrlSuffix> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the c l s friendly url suffixs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of c l s friendly url suffixs.
	*
	* @return the number of c l s friendly url suffixs
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}