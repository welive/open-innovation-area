/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.ideas.challenges.servicelayer.service.CLSFriendlyUrlSuffixLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class CLSFriendlyUrlSuffixClp extends BaseModelImpl<CLSFriendlyUrlSuffix>
	implements CLSFriendlyUrlSuffix {
	public CLSFriendlyUrlSuffixClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return CLSFriendlyUrlSuffix.class;
	}

	@Override
	public String getModelClassName() {
		return CLSFriendlyUrlSuffix.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _friendlyUrlSuffixID;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setFriendlyUrlSuffixID(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _friendlyUrlSuffixID;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("friendlyUrlSuffixID", getFriendlyUrlSuffixID());
		attributes.put("UrlSuffixImsHome", getUrlSuffixImsHome());
		attributes.put("UrlSuffixIdea", getUrlSuffixIdea());
		attributes.put("UrlSuffixNeed", getUrlSuffixNeed());
		attributes.put("UrlSuffixChallenge", getUrlSuffixChallenge());
		attributes.put("senderNotificheMailIdeario",
			getSenderNotificheMailIdeario());
		attributes.put("oggettoNotificheMailIdeario",
			getOggettoNotificheMailIdeario());
		attributes.put("firmaNotificheMailIdeario",
			getFirmaNotificheMailIdeario());
		attributes.put("utenzaMail", getUtenzaMail());
		attributes.put("cdvEnabled", getCdvEnabled());
		attributes.put("cdvAddress", getCdvAddress());
		attributes.put("vcEnabled", getVcEnabled());
		attributes.put("vcAddress", getVcAddress());
		attributes.put("vcWSAddress", getVcWSAddress());
		attributes.put("deEnabled", getDeEnabled());
		attributes.put("deAddress", getDeAddress());
		attributes.put("lbbEnabled", getLbbEnabled());
		attributes.put("lbbAddress", getLbbAddress());
		attributes.put("oiaAppId4lbb", getOiaAppId4lbb());
		attributes.put("tweetingEnabled", getTweetingEnabled());
		attributes.put("basicAuthUser", getBasicAuthUser());
		attributes.put("basicAuthPwd", getBasicAuthPwd());
		attributes.put("verboseEnabled", getVerboseEnabled());
		attributes.put("mktEnabled", getMktEnabled());
		attributes.put("emailNotificationsEnabled",
			getEmailNotificationsEnabled());
		attributes.put("dockbarNotificationsEnabled",
			getDockbarNotificationsEnabled());
		attributes.put("jmsEnabled", getJmsEnabled());
		attributes.put("brokerJMSusername", getBrokerJMSusername());
		attributes.put("brokerJMSpassword", getBrokerJMSpassword());
		attributes.put("brokerJMSurl", getBrokerJMSurl());
		attributes.put("jmsTopic", getJmsTopic());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long friendlyUrlSuffixID = (Long)attributes.get("friendlyUrlSuffixID");

		if (friendlyUrlSuffixID != null) {
			setFriendlyUrlSuffixID(friendlyUrlSuffixID);
		}

		String UrlSuffixImsHome = (String)attributes.get("UrlSuffixImsHome");

		if (UrlSuffixImsHome != null) {
			setUrlSuffixImsHome(UrlSuffixImsHome);
		}

		String UrlSuffixIdea = (String)attributes.get("UrlSuffixIdea");

		if (UrlSuffixIdea != null) {
			setUrlSuffixIdea(UrlSuffixIdea);
		}

		String UrlSuffixNeed = (String)attributes.get("UrlSuffixNeed");

		if (UrlSuffixNeed != null) {
			setUrlSuffixNeed(UrlSuffixNeed);
		}

		String UrlSuffixChallenge = (String)attributes.get("UrlSuffixChallenge");

		if (UrlSuffixChallenge != null) {
			setUrlSuffixChallenge(UrlSuffixChallenge);
		}

		String senderNotificheMailIdeario = (String)attributes.get(
				"senderNotificheMailIdeario");

		if (senderNotificheMailIdeario != null) {
			setSenderNotificheMailIdeario(senderNotificheMailIdeario);
		}

		String oggettoNotificheMailIdeario = (String)attributes.get(
				"oggettoNotificheMailIdeario");

		if (oggettoNotificheMailIdeario != null) {
			setOggettoNotificheMailIdeario(oggettoNotificheMailIdeario);
		}

		String firmaNotificheMailIdeario = (String)attributes.get(
				"firmaNotificheMailIdeario");

		if (firmaNotificheMailIdeario != null) {
			setFirmaNotificheMailIdeario(firmaNotificheMailIdeario);
		}

		String utenzaMail = (String)attributes.get("utenzaMail");

		if (utenzaMail != null) {
			setUtenzaMail(utenzaMail);
		}

		Boolean cdvEnabled = (Boolean)attributes.get("cdvEnabled");

		if (cdvEnabled != null) {
			setCdvEnabled(cdvEnabled);
		}

		String cdvAddress = (String)attributes.get("cdvAddress");

		if (cdvAddress != null) {
			setCdvAddress(cdvAddress);
		}

		Boolean vcEnabled = (Boolean)attributes.get("vcEnabled");

		if (vcEnabled != null) {
			setVcEnabled(vcEnabled);
		}

		String vcAddress = (String)attributes.get("vcAddress");

		if (vcAddress != null) {
			setVcAddress(vcAddress);
		}

		String vcWSAddress = (String)attributes.get("vcWSAddress");

		if (vcWSAddress != null) {
			setVcWSAddress(vcWSAddress);
		}

		Boolean deEnabled = (Boolean)attributes.get("deEnabled");

		if (deEnabled != null) {
			setDeEnabled(deEnabled);
		}

		String deAddress = (String)attributes.get("deAddress");

		if (deAddress != null) {
			setDeAddress(deAddress);
		}

		Boolean lbbEnabled = (Boolean)attributes.get("lbbEnabled");

		if (lbbEnabled != null) {
			setLbbEnabled(lbbEnabled);
		}

		String lbbAddress = (String)attributes.get("lbbAddress");

		if (lbbAddress != null) {
			setLbbAddress(lbbAddress);
		}

		String oiaAppId4lbb = (String)attributes.get("oiaAppId4lbb");

		if (oiaAppId4lbb != null) {
			setOiaAppId4lbb(oiaAppId4lbb);
		}

		Boolean tweetingEnabled = (Boolean)attributes.get("tweetingEnabled");

		if (tweetingEnabled != null) {
			setTweetingEnabled(tweetingEnabled);
		}

		String basicAuthUser = (String)attributes.get("basicAuthUser");

		if (basicAuthUser != null) {
			setBasicAuthUser(basicAuthUser);
		}

		String basicAuthPwd = (String)attributes.get("basicAuthPwd");

		if (basicAuthPwd != null) {
			setBasicAuthPwd(basicAuthPwd);
		}

		Boolean verboseEnabled = (Boolean)attributes.get("verboseEnabled");

		if (verboseEnabled != null) {
			setVerboseEnabled(verboseEnabled);
		}

		Boolean mktEnabled = (Boolean)attributes.get("mktEnabled");

		if (mktEnabled != null) {
			setMktEnabled(mktEnabled);
		}

		Boolean emailNotificationsEnabled = (Boolean)attributes.get(
				"emailNotificationsEnabled");

		if (emailNotificationsEnabled != null) {
			setEmailNotificationsEnabled(emailNotificationsEnabled);
		}

		Boolean dockbarNotificationsEnabled = (Boolean)attributes.get(
				"dockbarNotificationsEnabled");

		if (dockbarNotificationsEnabled != null) {
			setDockbarNotificationsEnabled(dockbarNotificationsEnabled);
		}

		Boolean jmsEnabled = (Boolean)attributes.get("jmsEnabled");

		if (jmsEnabled != null) {
			setJmsEnabled(jmsEnabled);
		}

		String brokerJMSusername = (String)attributes.get("brokerJMSusername");

		if (brokerJMSusername != null) {
			setBrokerJMSusername(brokerJMSusername);
		}

		String brokerJMSpassword = (String)attributes.get("brokerJMSpassword");

		if (brokerJMSpassword != null) {
			setBrokerJMSpassword(brokerJMSpassword);
		}

		String brokerJMSurl = (String)attributes.get("brokerJMSurl");

		if (brokerJMSurl != null) {
			setBrokerJMSurl(brokerJMSurl);
		}

		String jmsTopic = (String)attributes.get("jmsTopic");

		if (jmsTopic != null) {
			setJmsTopic(jmsTopic);
		}
	}

	@Override
	public long getFriendlyUrlSuffixID() {
		return _friendlyUrlSuffixID;
	}

	@Override
	public void setFriendlyUrlSuffixID(long friendlyUrlSuffixID) {
		_friendlyUrlSuffixID = friendlyUrlSuffixID;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setFriendlyUrlSuffixID",
						long.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel,
					friendlyUrlSuffixID);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUrlSuffixImsHome() {
		return _UrlSuffixImsHome;
	}

	@Override
	public void setUrlSuffixImsHome(String UrlSuffixImsHome) {
		_UrlSuffixImsHome = UrlSuffixImsHome;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setUrlSuffixImsHome",
						String.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel, UrlSuffixImsHome);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUrlSuffixIdea() {
		return _UrlSuffixIdea;
	}

	@Override
	public void setUrlSuffixIdea(String UrlSuffixIdea) {
		_UrlSuffixIdea = UrlSuffixIdea;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setUrlSuffixIdea", String.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel, UrlSuffixIdea);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUrlSuffixNeed() {
		return _UrlSuffixNeed;
	}

	@Override
	public void setUrlSuffixNeed(String UrlSuffixNeed) {
		_UrlSuffixNeed = UrlSuffixNeed;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setUrlSuffixNeed", String.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel, UrlSuffixNeed);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUrlSuffixChallenge() {
		return _UrlSuffixChallenge;
	}

	@Override
	public void setUrlSuffixChallenge(String UrlSuffixChallenge) {
		_UrlSuffixChallenge = UrlSuffixChallenge;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setUrlSuffixChallenge",
						String.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel,
					UrlSuffixChallenge);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getSenderNotificheMailIdeario() {
		return _senderNotificheMailIdeario;
	}

	@Override
	public void setSenderNotificheMailIdeario(String senderNotificheMailIdeario) {
		_senderNotificheMailIdeario = senderNotificheMailIdeario;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setSenderNotificheMailIdeario",
						String.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel,
					senderNotificheMailIdeario);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getOggettoNotificheMailIdeario() {
		return _oggettoNotificheMailIdeario;
	}

	@Override
	public void setOggettoNotificheMailIdeario(
		String oggettoNotificheMailIdeario) {
		_oggettoNotificheMailIdeario = oggettoNotificheMailIdeario;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setOggettoNotificheMailIdeario",
						String.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel,
					oggettoNotificheMailIdeario);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFirmaNotificheMailIdeario() {
		return _firmaNotificheMailIdeario;
	}

	@Override
	public void setFirmaNotificheMailIdeario(String firmaNotificheMailIdeario) {
		_firmaNotificheMailIdeario = firmaNotificheMailIdeario;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setFirmaNotificheMailIdeario",
						String.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel,
					firmaNotificheMailIdeario);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUtenzaMail() {
		return _utenzaMail;
	}

	@Override
	public void setUtenzaMail(String utenzaMail) {
		_utenzaMail = utenzaMail;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setUtenzaMail", String.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel, utenzaMail);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getCdvEnabled() {
		return _cdvEnabled;
	}

	@Override
	public boolean isCdvEnabled() {
		return _cdvEnabled;
	}

	@Override
	public void setCdvEnabled(boolean cdvEnabled) {
		_cdvEnabled = cdvEnabled;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setCdvEnabled", boolean.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel, cdvEnabled);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCdvAddress() {
		return _cdvAddress;
	}

	@Override
	public void setCdvAddress(String cdvAddress) {
		_cdvAddress = cdvAddress;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setCdvAddress", String.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel, cdvAddress);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getVcEnabled() {
		return _vcEnabled;
	}

	@Override
	public boolean isVcEnabled() {
		return _vcEnabled;
	}

	@Override
	public void setVcEnabled(boolean vcEnabled) {
		_vcEnabled = vcEnabled;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setVcEnabled", boolean.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel, vcEnabled);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getVcAddress() {
		return _vcAddress;
	}

	@Override
	public void setVcAddress(String vcAddress) {
		_vcAddress = vcAddress;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setVcAddress", String.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel, vcAddress);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getVcWSAddress() {
		return _vcWSAddress;
	}

	@Override
	public void setVcWSAddress(String vcWSAddress) {
		_vcWSAddress = vcWSAddress;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setVcWSAddress", String.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel, vcWSAddress);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getDeEnabled() {
		return _deEnabled;
	}

	@Override
	public boolean isDeEnabled() {
		return _deEnabled;
	}

	@Override
	public void setDeEnabled(boolean deEnabled) {
		_deEnabled = deEnabled;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setDeEnabled", boolean.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel, deEnabled);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDeAddress() {
		return _deAddress;
	}

	@Override
	public void setDeAddress(String deAddress) {
		_deAddress = deAddress;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setDeAddress", String.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel, deAddress);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getLbbEnabled() {
		return _lbbEnabled;
	}

	@Override
	public boolean isLbbEnabled() {
		return _lbbEnabled;
	}

	@Override
	public void setLbbEnabled(boolean lbbEnabled) {
		_lbbEnabled = lbbEnabled;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setLbbEnabled", boolean.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel, lbbEnabled);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLbbAddress() {
		return _lbbAddress;
	}

	@Override
	public void setLbbAddress(String lbbAddress) {
		_lbbAddress = lbbAddress;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setLbbAddress", String.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel, lbbAddress);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getOiaAppId4lbb() {
		return _oiaAppId4lbb;
	}

	@Override
	public void setOiaAppId4lbb(String oiaAppId4lbb) {
		_oiaAppId4lbb = oiaAppId4lbb;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setOiaAppId4lbb", String.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel, oiaAppId4lbb);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getTweetingEnabled() {
		return _tweetingEnabled;
	}

	@Override
	public boolean isTweetingEnabled() {
		return _tweetingEnabled;
	}

	@Override
	public void setTweetingEnabled(boolean tweetingEnabled) {
		_tweetingEnabled = tweetingEnabled;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setTweetingEnabled",
						boolean.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel, tweetingEnabled);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getBasicAuthUser() {
		return _basicAuthUser;
	}

	@Override
	public void setBasicAuthUser(String basicAuthUser) {
		_basicAuthUser = basicAuthUser;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setBasicAuthUser", String.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel, basicAuthUser);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getBasicAuthPwd() {
		return _basicAuthPwd;
	}

	@Override
	public void setBasicAuthPwd(String basicAuthPwd) {
		_basicAuthPwd = basicAuthPwd;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setBasicAuthPwd", String.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel, basicAuthPwd);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getVerboseEnabled() {
		return _verboseEnabled;
	}

	@Override
	public boolean isVerboseEnabled() {
		return _verboseEnabled;
	}

	@Override
	public void setVerboseEnabled(boolean verboseEnabled) {
		_verboseEnabled = verboseEnabled;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setVerboseEnabled",
						boolean.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel, verboseEnabled);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getMktEnabled() {
		return _mktEnabled;
	}

	@Override
	public boolean isMktEnabled() {
		return _mktEnabled;
	}

	@Override
	public void setMktEnabled(boolean mktEnabled) {
		_mktEnabled = mktEnabled;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setMktEnabled", boolean.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel, mktEnabled);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getEmailNotificationsEnabled() {
		return _emailNotificationsEnabled;
	}

	@Override
	public boolean isEmailNotificationsEnabled() {
		return _emailNotificationsEnabled;
	}

	@Override
	public void setEmailNotificationsEnabled(boolean emailNotificationsEnabled) {
		_emailNotificationsEnabled = emailNotificationsEnabled;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setEmailNotificationsEnabled",
						boolean.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel,
					emailNotificationsEnabled);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getDockbarNotificationsEnabled() {
		return _dockbarNotificationsEnabled;
	}

	@Override
	public boolean isDockbarNotificationsEnabled() {
		return _dockbarNotificationsEnabled;
	}

	@Override
	public void setDockbarNotificationsEnabled(
		boolean dockbarNotificationsEnabled) {
		_dockbarNotificationsEnabled = dockbarNotificationsEnabled;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setDockbarNotificationsEnabled",
						boolean.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel,
					dockbarNotificationsEnabled);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getJmsEnabled() {
		return _jmsEnabled;
	}

	@Override
	public boolean isJmsEnabled() {
		return _jmsEnabled;
	}

	@Override
	public void setJmsEnabled(boolean jmsEnabled) {
		_jmsEnabled = jmsEnabled;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setJmsEnabled", boolean.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel, jmsEnabled);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getBrokerJMSusername() {
		return _brokerJMSusername;
	}

	@Override
	public void setBrokerJMSusername(String brokerJMSusername) {
		_brokerJMSusername = brokerJMSusername;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setBrokerJMSusername",
						String.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel,
					brokerJMSusername);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getBrokerJMSpassword() {
		return _brokerJMSpassword;
	}

	@Override
	public void setBrokerJMSpassword(String brokerJMSpassword) {
		_brokerJMSpassword = brokerJMSpassword;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setBrokerJMSpassword",
						String.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel,
					brokerJMSpassword);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getBrokerJMSurl() {
		return _brokerJMSurl;
	}

	@Override
	public void setBrokerJMSurl(String brokerJMSurl) {
		_brokerJMSurl = brokerJMSurl;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setBrokerJMSurl", String.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel, brokerJMSurl);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getJmsTopic() {
		return _jmsTopic;
	}

	@Override
	public void setJmsTopic(String jmsTopic) {
		_jmsTopic = jmsTopic;

		if (_clsFriendlyUrlSuffixRemoteModel != null) {
			try {
				Class<?> clazz = _clsFriendlyUrlSuffixRemoteModel.getClass();

				Method method = clazz.getMethod("setJmsTopic", String.class);

				method.invoke(_clsFriendlyUrlSuffixRemoteModel, jmsTopic);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getCLSFriendlyUrlSuffixRemoteModel() {
		return _clsFriendlyUrlSuffixRemoteModel;
	}

	public void setCLSFriendlyUrlSuffixRemoteModel(
		BaseModel<?> clsFriendlyUrlSuffixRemoteModel) {
		_clsFriendlyUrlSuffixRemoteModel = clsFriendlyUrlSuffixRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _clsFriendlyUrlSuffixRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_clsFriendlyUrlSuffixRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CLSFriendlyUrlSuffixLocalServiceUtil.addCLSFriendlyUrlSuffix(this);
		}
		else {
			CLSFriendlyUrlSuffixLocalServiceUtil.updateCLSFriendlyUrlSuffix(this);
		}
	}

	@Override
	public CLSFriendlyUrlSuffix toEscapedModel() {
		return (CLSFriendlyUrlSuffix)ProxyUtil.newProxyInstance(CLSFriendlyUrlSuffix.class.getClassLoader(),
			new Class[] { CLSFriendlyUrlSuffix.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CLSFriendlyUrlSuffixClp clone = new CLSFriendlyUrlSuffixClp();

		clone.setFriendlyUrlSuffixID(getFriendlyUrlSuffixID());
		clone.setUrlSuffixImsHome(getUrlSuffixImsHome());
		clone.setUrlSuffixIdea(getUrlSuffixIdea());
		clone.setUrlSuffixNeed(getUrlSuffixNeed());
		clone.setUrlSuffixChallenge(getUrlSuffixChallenge());
		clone.setSenderNotificheMailIdeario(getSenderNotificheMailIdeario());
		clone.setOggettoNotificheMailIdeario(getOggettoNotificheMailIdeario());
		clone.setFirmaNotificheMailIdeario(getFirmaNotificheMailIdeario());
		clone.setUtenzaMail(getUtenzaMail());
		clone.setCdvEnabled(getCdvEnabled());
		clone.setCdvAddress(getCdvAddress());
		clone.setVcEnabled(getVcEnabled());
		clone.setVcAddress(getVcAddress());
		clone.setVcWSAddress(getVcWSAddress());
		clone.setDeEnabled(getDeEnabled());
		clone.setDeAddress(getDeAddress());
		clone.setLbbEnabled(getLbbEnabled());
		clone.setLbbAddress(getLbbAddress());
		clone.setOiaAppId4lbb(getOiaAppId4lbb());
		clone.setTweetingEnabled(getTweetingEnabled());
		clone.setBasicAuthUser(getBasicAuthUser());
		clone.setBasicAuthPwd(getBasicAuthPwd());
		clone.setVerboseEnabled(getVerboseEnabled());
		clone.setMktEnabled(getMktEnabled());
		clone.setEmailNotificationsEnabled(getEmailNotificationsEnabled());
		clone.setDockbarNotificationsEnabled(getDockbarNotificationsEnabled());
		clone.setJmsEnabled(getJmsEnabled());
		clone.setBrokerJMSusername(getBrokerJMSusername());
		clone.setBrokerJMSpassword(getBrokerJMSpassword());
		clone.setBrokerJMSurl(getBrokerJMSurl());
		clone.setJmsTopic(getJmsTopic());

		return clone;
	}

	@Override
	public int compareTo(CLSFriendlyUrlSuffix clsFriendlyUrlSuffix) {
		long primaryKey = clsFriendlyUrlSuffix.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSFriendlyUrlSuffixClp)) {
			return false;
		}

		CLSFriendlyUrlSuffixClp clsFriendlyUrlSuffix = (CLSFriendlyUrlSuffixClp)obj;

		long primaryKey = clsFriendlyUrlSuffix.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(63);

		sb.append("{friendlyUrlSuffixID=");
		sb.append(getFriendlyUrlSuffixID());
		sb.append(", UrlSuffixImsHome=");
		sb.append(getUrlSuffixImsHome());
		sb.append(", UrlSuffixIdea=");
		sb.append(getUrlSuffixIdea());
		sb.append(", UrlSuffixNeed=");
		sb.append(getUrlSuffixNeed());
		sb.append(", UrlSuffixChallenge=");
		sb.append(getUrlSuffixChallenge());
		sb.append(", senderNotificheMailIdeario=");
		sb.append(getSenderNotificheMailIdeario());
		sb.append(", oggettoNotificheMailIdeario=");
		sb.append(getOggettoNotificheMailIdeario());
		sb.append(", firmaNotificheMailIdeario=");
		sb.append(getFirmaNotificheMailIdeario());
		sb.append(", utenzaMail=");
		sb.append(getUtenzaMail());
		sb.append(", cdvEnabled=");
		sb.append(getCdvEnabled());
		sb.append(", cdvAddress=");
		sb.append(getCdvAddress());
		sb.append(", vcEnabled=");
		sb.append(getVcEnabled());
		sb.append(", vcAddress=");
		sb.append(getVcAddress());
		sb.append(", vcWSAddress=");
		sb.append(getVcWSAddress());
		sb.append(", deEnabled=");
		sb.append(getDeEnabled());
		sb.append(", deAddress=");
		sb.append(getDeAddress());
		sb.append(", lbbEnabled=");
		sb.append(getLbbEnabled());
		sb.append(", lbbAddress=");
		sb.append(getLbbAddress());
		sb.append(", oiaAppId4lbb=");
		sb.append(getOiaAppId4lbb());
		sb.append(", tweetingEnabled=");
		sb.append(getTweetingEnabled());
		sb.append(", basicAuthUser=");
		sb.append(getBasicAuthUser());
		sb.append(", basicAuthPwd=");
		sb.append(getBasicAuthPwd());
		sb.append(", verboseEnabled=");
		sb.append(getVerboseEnabled());
		sb.append(", mktEnabled=");
		sb.append(getMktEnabled());
		sb.append(", emailNotificationsEnabled=");
		sb.append(getEmailNotificationsEnabled());
		sb.append(", dockbarNotificationsEnabled=");
		sb.append(getDockbarNotificationsEnabled());
		sb.append(", jmsEnabled=");
		sb.append(getJmsEnabled());
		sb.append(", brokerJMSusername=");
		sb.append(getBrokerJMSusername());
		sb.append(", brokerJMSpassword=");
		sb.append(getBrokerJMSpassword());
		sb.append(", brokerJMSurl=");
		sb.append(getBrokerJMSurl());
		sb.append(", jmsTopic=");
		sb.append(getJmsTopic());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(97);

		sb.append("<model><model-name>");
		sb.append(
			"it.eng.rspa.ideas.challenges.servicelayer.model.CLSFriendlyUrlSuffix");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>friendlyUrlSuffixID</column-name><column-value><![CDATA[");
		sb.append(getFriendlyUrlSuffixID());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>UrlSuffixImsHome</column-name><column-value><![CDATA[");
		sb.append(getUrlSuffixImsHome());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>UrlSuffixIdea</column-name><column-value><![CDATA[");
		sb.append(getUrlSuffixIdea());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>UrlSuffixNeed</column-name><column-value><![CDATA[");
		sb.append(getUrlSuffixNeed());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>UrlSuffixChallenge</column-name><column-value><![CDATA[");
		sb.append(getUrlSuffixChallenge());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>senderNotificheMailIdeario</column-name><column-value><![CDATA[");
		sb.append(getSenderNotificheMailIdeario());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>oggettoNotificheMailIdeario</column-name><column-value><![CDATA[");
		sb.append(getOggettoNotificheMailIdeario());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>firmaNotificheMailIdeario</column-name><column-value><![CDATA[");
		sb.append(getFirmaNotificheMailIdeario());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>utenzaMail</column-name><column-value><![CDATA[");
		sb.append(getUtenzaMail());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cdvEnabled</column-name><column-value><![CDATA[");
		sb.append(getCdvEnabled());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cdvAddress</column-name><column-value><![CDATA[");
		sb.append(getCdvAddress());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>vcEnabled</column-name><column-value><![CDATA[");
		sb.append(getVcEnabled());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>vcAddress</column-name><column-value><![CDATA[");
		sb.append(getVcAddress());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>vcWSAddress</column-name><column-value><![CDATA[");
		sb.append(getVcWSAddress());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>deEnabled</column-name><column-value><![CDATA[");
		sb.append(getDeEnabled());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>deAddress</column-name><column-value><![CDATA[");
		sb.append(getDeAddress());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>lbbEnabled</column-name><column-value><![CDATA[");
		sb.append(getLbbEnabled());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>lbbAddress</column-name><column-value><![CDATA[");
		sb.append(getLbbAddress());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>oiaAppId4lbb</column-name><column-value><![CDATA[");
		sb.append(getOiaAppId4lbb());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tweetingEnabled</column-name><column-value><![CDATA[");
		sb.append(getTweetingEnabled());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>basicAuthUser</column-name><column-value><![CDATA[");
		sb.append(getBasicAuthUser());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>basicAuthPwd</column-name><column-value><![CDATA[");
		sb.append(getBasicAuthPwd());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>verboseEnabled</column-name><column-value><![CDATA[");
		sb.append(getVerboseEnabled());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>mktEnabled</column-name><column-value><![CDATA[");
		sb.append(getMktEnabled());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>emailNotificationsEnabled</column-name><column-value><![CDATA[");
		sb.append(getEmailNotificationsEnabled());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dockbarNotificationsEnabled</column-name><column-value><![CDATA[");
		sb.append(getDockbarNotificationsEnabled());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>jmsEnabled</column-name><column-value><![CDATA[");
		sb.append(getJmsEnabled());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>brokerJMSusername</column-name><column-value><![CDATA[");
		sb.append(getBrokerJMSusername());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>brokerJMSpassword</column-name><column-value><![CDATA[");
		sb.append(getBrokerJMSpassword());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>brokerJMSurl</column-name><column-value><![CDATA[");
		sb.append(getBrokerJMSurl());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>jmsTopic</column-name><column-value><![CDATA[");
		sb.append(getJmsTopic());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _friendlyUrlSuffixID;
	private String _UrlSuffixImsHome;
	private String _UrlSuffixIdea;
	private String _UrlSuffixNeed;
	private String _UrlSuffixChallenge;
	private String _senderNotificheMailIdeario;
	private String _oggettoNotificheMailIdeario;
	private String _firmaNotificheMailIdeario;
	private String _utenzaMail;
	private boolean _cdvEnabled;
	private String _cdvAddress;
	private boolean _vcEnabled;
	private String _vcAddress;
	private String _vcWSAddress;
	private boolean _deEnabled;
	private String _deAddress;
	private boolean _lbbEnabled;
	private String _lbbAddress;
	private String _oiaAppId4lbb;
	private boolean _tweetingEnabled;
	private String _basicAuthUser;
	private String _basicAuthPwd;
	private boolean _verboseEnabled;
	private boolean _mktEnabled;
	private boolean _emailNotificationsEnabled;
	private boolean _dockbarNotificationsEnabled;
	private boolean _jmsEnabled;
	private String _brokerJMSusername;
	private String _brokerJMSpassword;
	private String _brokerJMSurl;
	private String _jmsTopic;
	private BaseModel<?> _clsFriendlyUrlSuffixRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.class;
}