/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSMapPropertiesServiceSoap}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSMapPropertiesServiceSoap
 * @generated
 */
public class CLSMapPropertiesSoap implements Serializable {
	public static CLSMapPropertiesSoap toSoapModel(CLSMapProperties model) {
		CLSMapPropertiesSoap soapModel = new CLSMapPropertiesSoap();

		soapModel.setMapPropertiesId(model.getMapPropertiesId());
		soapModel.setMapCenterLatitude(model.getMapCenterLatitude());
		soapModel.setMapCenterLongitude(model.getMapCenterLongitude());

		return soapModel;
	}

	public static CLSMapPropertiesSoap[] toSoapModels(CLSMapProperties[] models) {
		CLSMapPropertiesSoap[] soapModels = new CLSMapPropertiesSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CLSMapPropertiesSoap[][] toSoapModels(
		CLSMapProperties[][] models) {
		CLSMapPropertiesSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CLSMapPropertiesSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CLSMapPropertiesSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CLSMapPropertiesSoap[] toSoapModels(
		List<CLSMapProperties> models) {
		List<CLSMapPropertiesSoap> soapModels = new ArrayList<CLSMapPropertiesSoap>(models.size());

		for (CLSMapProperties model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CLSMapPropertiesSoap[soapModels.size()]);
	}

	public CLSMapPropertiesSoap() {
	}

	public long getPrimaryKey() {
		return _mapPropertiesId;
	}

	public void setPrimaryKey(long pk) {
		setMapPropertiesId(pk);
	}

	public long getMapPropertiesId() {
		return _mapPropertiesId;
	}

	public void setMapPropertiesId(long mapPropertiesId) {
		_mapPropertiesId = mapPropertiesId;
	}

	public String getMapCenterLatitude() {
		return _mapCenterLatitude;
	}

	public void setMapCenterLatitude(String mapCenterLatitude) {
		_mapCenterLatitude = mapCenterLatitude;
	}

	public String getMapCenterLongitude() {
		return _mapCenterLongitude;
	}

	public void setMapCenterLongitude(String mapCenterLongitude) {
		_mapCenterLongitude = mapCenterLongitude;
	}

	private long _mapPropertiesId;
	private String _mapCenterLatitude;
	private String _mapCenterLongitude;
}