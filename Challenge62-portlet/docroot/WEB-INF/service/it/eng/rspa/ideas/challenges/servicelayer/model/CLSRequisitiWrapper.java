/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CLSRequisiti}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSRequisiti
 * @generated
 */
public class CLSRequisitiWrapper implements CLSRequisiti,
	ModelWrapper<CLSRequisiti> {
	public CLSRequisitiWrapper(CLSRequisiti clsRequisiti) {
		_clsRequisiti = clsRequisiti;
	}

	@Override
	public Class<?> getModelClass() {
		return CLSRequisiti.class;
	}

	@Override
	public String getModelClassName() {
		return CLSRequisiti.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("requisitoId", getRequisitoId());
		attributes.put("ideaId", getIdeaId());
		attributes.put("taskId", getTaskId());
		attributes.put("tipo", getTipo());
		attributes.put("descrizione", getDescrizione());
		attributes.put("authorUser", getAuthorUser());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long requisitoId = (Long)attributes.get("requisitoId");

		if (requisitoId != null) {
			setRequisitoId(requisitoId);
		}

		Long ideaId = (Long)attributes.get("ideaId");

		if (ideaId != null) {
			setIdeaId(ideaId);
		}

		Long taskId = (Long)attributes.get("taskId");

		if (taskId != null) {
			setTaskId(taskId);
		}

		String tipo = (String)attributes.get("tipo");

		if (tipo != null) {
			setTipo(tipo);
		}

		String descrizione = (String)attributes.get("descrizione");

		if (descrizione != null) {
			setDescrizione(descrizione);
		}

		Long authorUser = (Long)attributes.get("authorUser");

		if (authorUser != null) {
			setAuthorUser(authorUser);
		}
	}

	/**
	* Returns the primary key of this c l s requisiti.
	*
	* @return the primary key of this c l s requisiti
	*/
	@Override
	public long getPrimaryKey() {
		return _clsRequisiti.getPrimaryKey();
	}

	/**
	* Sets the primary key of this c l s requisiti.
	*
	* @param primaryKey the primary key of this c l s requisiti
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_clsRequisiti.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the requisito ID of this c l s requisiti.
	*
	* @return the requisito ID of this c l s requisiti
	*/
	@Override
	public long getRequisitoId() {
		return _clsRequisiti.getRequisitoId();
	}

	/**
	* Sets the requisito ID of this c l s requisiti.
	*
	* @param requisitoId the requisito ID of this c l s requisiti
	*/
	@Override
	public void setRequisitoId(long requisitoId) {
		_clsRequisiti.setRequisitoId(requisitoId);
	}

	/**
	* Returns the idea ID of this c l s requisiti.
	*
	* @return the idea ID of this c l s requisiti
	*/
	@Override
	public long getIdeaId() {
		return _clsRequisiti.getIdeaId();
	}

	/**
	* Sets the idea ID of this c l s requisiti.
	*
	* @param ideaId the idea ID of this c l s requisiti
	*/
	@Override
	public void setIdeaId(long ideaId) {
		_clsRequisiti.setIdeaId(ideaId);
	}

	/**
	* Returns the task ID of this c l s requisiti.
	*
	* @return the task ID of this c l s requisiti
	*/
	@Override
	public long getTaskId() {
		return _clsRequisiti.getTaskId();
	}

	/**
	* Sets the task ID of this c l s requisiti.
	*
	* @param taskId the task ID of this c l s requisiti
	*/
	@Override
	public void setTaskId(long taskId) {
		_clsRequisiti.setTaskId(taskId);
	}

	/**
	* Returns the tipo of this c l s requisiti.
	*
	* @return the tipo of this c l s requisiti
	*/
	@Override
	public java.lang.String getTipo() {
		return _clsRequisiti.getTipo();
	}

	/**
	* Sets the tipo of this c l s requisiti.
	*
	* @param tipo the tipo of this c l s requisiti
	*/
	@Override
	public void setTipo(java.lang.String tipo) {
		_clsRequisiti.setTipo(tipo);
	}

	/**
	* Returns the descrizione of this c l s requisiti.
	*
	* @return the descrizione of this c l s requisiti
	*/
	@Override
	public java.lang.String getDescrizione() {
		return _clsRequisiti.getDescrizione();
	}

	/**
	* Sets the descrizione of this c l s requisiti.
	*
	* @param descrizione the descrizione of this c l s requisiti
	*/
	@Override
	public void setDescrizione(java.lang.String descrizione) {
		_clsRequisiti.setDescrizione(descrizione);
	}

	/**
	* Returns the author user of this c l s requisiti.
	*
	* @return the author user of this c l s requisiti
	*/
	@Override
	public long getAuthorUser() {
		return _clsRequisiti.getAuthorUser();
	}

	/**
	* Sets the author user of this c l s requisiti.
	*
	* @param authorUser the author user of this c l s requisiti
	*/
	@Override
	public void setAuthorUser(long authorUser) {
		_clsRequisiti.setAuthorUser(authorUser);
	}

	@Override
	public boolean isNew() {
		return _clsRequisiti.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_clsRequisiti.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _clsRequisiti.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_clsRequisiti.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _clsRequisiti.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _clsRequisiti.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_clsRequisiti.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _clsRequisiti.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_clsRequisiti.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_clsRequisiti.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_clsRequisiti.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CLSRequisitiWrapper((CLSRequisiti)_clsRequisiti.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti clsRequisiti) {
		return _clsRequisiti.compareTo(clsRequisiti);
	}

	@Override
	public int hashCode() {
		return _clsRequisiti.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti> toCacheModel() {
		return _clsRequisiti.toCacheModel();
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti toEscapedModel() {
		return new CLSRequisitiWrapper(_clsRequisiti.toEscapedModel());
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti toUnescapedModel() {
		return new CLSRequisitiWrapper(_clsRequisiti.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _clsRequisiti.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _clsRequisiti.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_clsRequisiti.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSRequisitiWrapper)) {
			return false;
		}

		CLSRequisitiWrapper clsRequisitiWrapper = (CLSRequisitiWrapper)obj;

		if (Validator.equals(_clsRequisiti, clsRequisitiWrapper._clsRequisiti)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public CLSRequisiti getWrappedCLSRequisiti() {
		return _clsRequisiti;
	}

	@Override
	public CLSRequisiti getWrappedModel() {
		return _clsRequisiti;
	}

	@Override
	public void resetOriginalValues() {
		_clsRequisiti.resetOriginalValues();
	}

	private CLSRequisiti _clsRequisiti;
}