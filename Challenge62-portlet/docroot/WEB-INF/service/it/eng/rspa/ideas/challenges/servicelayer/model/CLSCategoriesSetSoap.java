/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSCategoriesSetServiceSoap}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSCategoriesSetServiceSoap
 * @generated
 */
public class CLSCategoriesSetSoap implements Serializable {
	public static CLSCategoriesSetSoap toSoapModel(CLSCategoriesSet model) {
		CLSCategoriesSetSoap soapModel = new CLSCategoriesSetSoap();

		soapModel.setCategoriesSetID(model.getCategoriesSetID());

		return soapModel;
	}

	public static CLSCategoriesSetSoap[] toSoapModels(CLSCategoriesSet[] models) {
		CLSCategoriesSetSoap[] soapModels = new CLSCategoriesSetSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CLSCategoriesSetSoap[][] toSoapModels(
		CLSCategoriesSet[][] models) {
		CLSCategoriesSetSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CLSCategoriesSetSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CLSCategoriesSetSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CLSCategoriesSetSoap[] toSoapModels(
		List<CLSCategoriesSet> models) {
		List<CLSCategoriesSetSoap> soapModels = new ArrayList<CLSCategoriesSetSoap>(models.size());

		for (CLSCategoriesSet model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CLSCategoriesSetSoap[soapModels.size()]);
	}

	public CLSCategoriesSetSoap() {
	}

	public long getPrimaryKey() {
		return _categoriesSetID;
	}

	public void setPrimaryKey(long pk) {
		setCategoriesSetID(pk);
	}

	public long getCategoriesSetID() {
		return _categoriesSetID;
	}

	public void setCategoriesSetID(long categoriesSetID) {
		_categoriesSetID = categoriesSetID;
	}

	private long _categoriesSetID;
}