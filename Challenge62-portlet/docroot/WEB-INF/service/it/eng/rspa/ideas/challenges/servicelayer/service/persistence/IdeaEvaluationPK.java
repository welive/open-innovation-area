/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 * @generated
 */
public class IdeaEvaluationPK implements Comparable<IdeaEvaluationPK>,
	Serializable {
	public long criteriaId;
	public long ideaId;

	public IdeaEvaluationPK() {
	}

	public IdeaEvaluationPK(long criteriaId, long ideaId) {
		this.criteriaId = criteriaId;
		this.ideaId = ideaId;
	}

	public long getCriteriaId() {
		return criteriaId;
	}

	public void setCriteriaId(long criteriaId) {
		this.criteriaId = criteriaId;
	}

	public long getIdeaId() {
		return ideaId;
	}

	public void setIdeaId(long ideaId) {
		this.ideaId = ideaId;
	}

	@Override
	public int compareTo(IdeaEvaluationPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (criteriaId < pk.criteriaId) {
			value = -1;
		}
		else if (criteriaId > pk.criteriaId) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		if (ideaId < pk.ideaId) {
			value = -1;
		}
		else if (ideaId > pk.ideaId) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof IdeaEvaluationPK)) {
			return false;
		}

		IdeaEvaluationPK pk = (IdeaEvaluationPK)obj;

		if ((criteriaId == pk.criteriaId) && (ideaId == pk.ideaId)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (String.valueOf(criteriaId) + String.valueOf(ideaId)).hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(10);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("criteriaId");
		sb.append(StringPool.EQUAL);
		sb.append(criteriaId);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("ideaId");
		sb.append(StringPool.EQUAL);
		sb.append(ideaId);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}