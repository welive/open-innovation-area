/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CLSIdeaPoi}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSIdeaPoi
 * @generated
 */
public class CLSIdeaPoiWrapper implements CLSIdeaPoi, ModelWrapper<CLSIdeaPoi> {
	public CLSIdeaPoiWrapper(CLSIdeaPoi clsIdeaPoi) {
		_clsIdeaPoi = clsIdeaPoi;
	}

	@Override
	public Class<?> getModelClass() {
		return CLSIdeaPoi.class;
	}

	@Override
	public String getModelClassName() {
		return CLSIdeaPoi.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ideaId", getIdeaId());
		attributes.put("latitude", getLatitude());
		attributes.put("longitude", getLongitude());
		attributes.put("description", getDescription());
		attributes.put("poiId", getPoiId());
		attributes.put("title", getTitle());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ideaId = (Long)attributes.get("ideaId");

		if (ideaId != null) {
			setIdeaId(ideaId);
		}

		String latitude = (String)attributes.get("latitude");

		if (latitude != null) {
			setLatitude(latitude);
		}

		String longitude = (String)attributes.get("longitude");

		if (longitude != null) {
			setLongitude(longitude);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		Long poiId = (Long)attributes.get("poiId");

		if (poiId != null) {
			setPoiId(poiId);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}
	}

	/**
	* Returns the primary key of this c l s idea poi.
	*
	* @return the primary key of this c l s idea poi
	*/
	@Override
	public long getPrimaryKey() {
		return _clsIdeaPoi.getPrimaryKey();
	}

	/**
	* Sets the primary key of this c l s idea poi.
	*
	* @param primaryKey the primary key of this c l s idea poi
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_clsIdeaPoi.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the idea ID of this c l s idea poi.
	*
	* @return the idea ID of this c l s idea poi
	*/
	@Override
	public long getIdeaId() {
		return _clsIdeaPoi.getIdeaId();
	}

	/**
	* Sets the idea ID of this c l s idea poi.
	*
	* @param ideaId the idea ID of this c l s idea poi
	*/
	@Override
	public void setIdeaId(long ideaId) {
		_clsIdeaPoi.setIdeaId(ideaId);
	}

	/**
	* Returns the latitude of this c l s idea poi.
	*
	* @return the latitude of this c l s idea poi
	*/
	@Override
	public java.lang.String getLatitude() {
		return _clsIdeaPoi.getLatitude();
	}

	/**
	* Sets the latitude of this c l s idea poi.
	*
	* @param latitude the latitude of this c l s idea poi
	*/
	@Override
	public void setLatitude(java.lang.String latitude) {
		_clsIdeaPoi.setLatitude(latitude);
	}

	/**
	* Returns the longitude of this c l s idea poi.
	*
	* @return the longitude of this c l s idea poi
	*/
	@Override
	public java.lang.String getLongitude() {
		return _clsIdeaPoi.getLongitude();
	}

	/**
	* Sets the longitude of this c l s idea poi.
	*
	* @param longitude the longitude of this c l s idea poi
	*/
	@Override
	public void setLongitude(java.lang.String longitude) {
		_clsIdeaPoi.setLongitude(longitude);
	}

	/**
	* Returns the description of this c l s idea poi.
	*
	* @return the description of this c l s idea poi
	*/
	@Override
	public java.lang.String getDescription() {
		return _clsIdeaPoi.getDescription();
	}

	/**
	* Sets the description of this c l s idea poi.
	*
	* @param description the description of this c l s idea poi
	*/
	@Override
	public void setDescription(java.lang.String description) {
		_clsIdeaPoi.setDescription(description);
	}

	/**
	* Returns the poi ID of this c l s idea poi.
	*
	* @return the poi ID of this c l s idea poi
	*/
	@Override
	public long getPoiId() {
		return _clsIdeaPoi.getPoiId();
	}

	/**
	* Sets the poi ID of this c l s idea poi.
	*
	* @param poiId the poi ID of this c l s idea poi
	*/
	@Override
	public void setPoiId(long poiId) {
		_clsIdeaPoi.setPoiId(poiId);
	}

	/**
	* Returns the title of this c l s idea poi.
	*
	* @return the title of this c l s idea poi
	*/
	@Override
	public java.lang.String getTitle() {
		return _clsIdeaPoi.getTitle();
	}

	/**
	* Sets the title of this c l s idea poi.
	*
	* @param title the title of this c l s idea poi
	*/
	@Override
	public void setTitle(java.lang.String title) {
		_clsIdeaPoi.setTitle(title);
	}

	@Override
	public boolean isNew() {
		return _clsIdeaPoi.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_clsIdeaPoi.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _clsIdeaPoi.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_clsIdeaPoi.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _clsIdeaPoi.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _clsIdeaPoi.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_clsIdeaPoi.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _clsIdeaPoi.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_clsIdeaPoi.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_clsIdeaPoi.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_clsIdeaPoi.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CLSIdeaPoiWrapper((CLSIdeaPoi)_clsIdeaPoi.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi clsIdeaPoi) {
		return _clsIdeaPoi.compareTo(clsIdeaPoi);
	}

	@Override
	public int hashCode() {
		return _clsIdeaPoi.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi> toCacheModel() {
		return _clsIdeaPoi.toCacheModel();
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi toEscapedModel() {
		return new CLSIdeaPoiWrapper(_clsIdeaPoi.toEscapedModel());
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi toUnescapedModel() {
		return new CLSIdeaPoiWrapper(_clsIdeaPoi.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _clsIdeaPoi.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _clsIdeaPoi.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_clsIdeaPoi.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSIdeaPoiWrapper)) {
			return false;
		}

		CLSIdeaPoiWrapper clsIdeaPoiWrapper = (CLSIdeaPoiWrapper)obj;

		if (Validator.equals(_clsIdeaPoi, clsIdeaPoiWrapper._clsIdeaPoi)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public CLSIdeaPoi getWrappedCLSIdeaPoi() {
		return _clsIdeaPoi;
	}

	@Override
	public CLSIdeaPoi getWrappedModel() {
		return _clsIdeaPoi;
	}

	@Override
	public void resetOriginalValues() {
		_clsIdeaPoi.resetOriginalValues();
	}

	private CLSIdeaPoi _clsIdeaPoi;
}