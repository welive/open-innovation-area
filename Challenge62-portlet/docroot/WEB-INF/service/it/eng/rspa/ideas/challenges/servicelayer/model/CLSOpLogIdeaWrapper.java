/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CLSOpLogIdea}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSOpLogIdea
 * @generated
 */
public class CLSOpLogIdeaWrapper implements CLSOpLogIdea,
	ModelWrapper<CLSOpLogIdea> {
	public CLSOpLogIdeaWrapper(CLSOpLogIdea clsOpLogIdea) {
		_clsOpLogIdea = clsOpLogIdea;
	}

	@Override
	public Class<?> getModelClass() {
		return CLSOpLogIdea.class;
	}

	@Override
	public String getModelClassName() {
		return CLSOpLogIdea.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("opLogId", getOpLogId());
		attributes.put("ideaId", getIdeaId());
		attributes.put("userId", getUserId());
		attributes.put("date", getDate());
		attributes.put("extraData", getExtraData());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long opLogId = (Long)attributes.get("opLogId");

		if (opLogId != null) {
			setOpLogId(opLogId);
		}

		Long ideaId = (Long)attributes.get("ideaId");

		if (ideaId != null) {
			setIdeaId(ideaId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date date = (Date)attributes.get("date");

		if (date != null) {
			setDate(date);
		}

		String extraData = (String)attributes.get("extraData");

		if (extraData != null) {
			setExtraData(extraData);
		}
	}

	/**
	* Returns the primary key of this c l s op log idea.
	*
	* @return the primary key of this c l s op log idea
	*/
	@Override
	public long getPrimaryKey() {
		return _clsOpLogIdea.getPrimaryKey();
	}

	/**
	* Sets the primary key of this c l s op log idea.
	*
	* @param primaryKey the primary key of this c l s op log idea
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_clsOpLogIdea.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the op log ID of this c l s op log idea.
	*
	* @return the op log ID of this c l s op log idea
	*/
	@Override
	public long getOpLogId() {
		return _clsOpLogIdea.getOpLogId();
	}

	/**
	* Sets the op log ID of this c l s op log idea.
	*
	* @param opLogId the op log ID of this c l s op log idea
	*/
	@Override
	public void setOpLogId(long opLogId) {
		_clsOpLogIdea.setOpLogId(opLogId);
	}

	/**
	* Returns the idea ID of this c l s op log idea.
	*
	* @return the idea ID of this c l s op log idea
	*/
	@Override
	public long getIdeaId() {
		return _clsOpLogIdea.getIdeaId();
	}

	/**
	* Sets the idea ID of this c l s op log idea.
	*
	* @param ideaId the idea ID of this c l s op log idea
	*/
	@Override
	public void setIdeaId(long ideaId) {
		_clsOpLogIdea.setIdeaId(ideaId);
	}

	/**
	* Returns the user ID of this c l s op log idea.
	*
	* @return the user ID of this c l s op log idea
	*/
	@Override
	public long getUserId() {
		return _clsOpLogIdea.getUserId();
	}

	/**
	* Sets the user ID of this c l s op log idea.
	*
	* @param userId the user ID of this c l s op log idea
	*/
	@Override
	public void setUserId(long userId) {
		_clsOpLogIdea.setUserId(userId);
	}

	/**
	* Returns the user uuid of this c l s op log idea.
	*
	* @return the user uuid of this c l s op log idea
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsOpLogIdea.getUserUuid();
	}

	/**
	* Sets the user uuid of this c l s op log idea.
	*
	* @param userUuid the user uuid of this c l s op log idea
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_clsOpLogIdea.setUserUuid(userUuid);
	}

	/**
	* Returns the date of this c l s op log idea.
	*
	* @return the date of this c l s op log idea
	*/
	@Override
	public java.util.Date getDate() {
		return _clsOpLogIdea.getDate();
	}

	/**
	* Sets the date of this c l s op log idea.
	*
	* @param date the date of this c l s op log idea
	*/
	@Override
	public void setDate(java.util.Date date) {
		_clsOpLogIdea.setDate(date);
	}

	/**
	* Returns the extra data of this c l s op log idea.
	*
	* @return the extra data of this c l s op log idea
	*/
	@Override
	public java.lang.String getExtraData() {
		return _clsOpLogIdea.getExtraData();
	}

	/**
	* Sets the extra data of this c l s op log idea.
	*
	* @param extraData the extra data of this c l s op log idea
	*/
	@Override
	public void setExtraData(java.lang.String extraData) {
		_clsOpLogIdea.setExtraData(extraData);
	}

	@Override
	public boolean isNew() {
		return _clsOpLogIdea.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_clsOpLogIdea.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _clsOpLogIdea.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_clsOpLogIdea.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _clsOpLogIdea.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _clsOpLogIdea.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_clsOpLogIdea.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _clsOpLogIdea.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_clsOpLogIdea.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_clsOpLogIdea.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_clsOpLogIdea.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CLSOpLogIdeaWrapper((CLSOpLogIdea)_clsOpLogIdea.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea clsOpLogIdea) {
		return _clsOpLogIdea.compareTo(clsOpLogIdea);
	}

	@Override
	public int hashCode() {
		return _clsOpLogIdea.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea> toCacheModel() {
		return _clsOpLogIdea.toCacheModel();
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea toEscapedModel() {
		return new CLSOpLogIdeaWrapper(_clsOpLogIdea.toEscapedModel());
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea toUnescapedModel() {
		return new CLSOpLogIdeaWrapper(_clsOpLogIdea.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _clsOpLogIdea.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _clsOpLogIdea.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_clsOpLogIdea.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSOpLogIdeaWrapper)) {
			return false;
		}

		CLSOpLogIdeaWrapper clsOpLogIdeaWrapper = (CLSOpLogIdeaWrapper)obj;

		if (Validator.equals(_clsOpLogIdea, clsOpLogIdeaWrapper._clsOpLogIdea)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public CLSOpLogIdea getWrappedCLSOpLogIdea() {
		return _clsOpLogIdea;
	}

	@Override
	public CLSOpLogIdea getWrappedModel() {
		return _clsOpLogIdea;
	}

	@Override
	public void resetOriginalValues() {
		_clsOpLogIdea.resetOriginalValues();
	}

	private CLSOpLogIdea _clsOpLogIdea;
}