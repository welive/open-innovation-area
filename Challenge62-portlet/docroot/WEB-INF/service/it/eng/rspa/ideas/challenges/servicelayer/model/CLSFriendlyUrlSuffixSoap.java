/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSFriendlyUrlSuffixServiceSoap}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSFriendlyUrlSuffixServiceSoap
 * @generated
 */
public class CLSFriendlyUrlSuffixSoap implements Serializable {
	public static CLSFriendlyUrlSuffixSoap toSoapModel(
		CLSFriendlyUrlSuffix model) {
		CLSFriendlyUrlSuffixSoap soapModel = new CLSFriendlyUrlSuffixSoap();

		soapModel.setFriendlyUrlSuffixID(model.getFriendlyUrlSuffixID());
		soapModel.setUrlSuffixImsHome(model.getUrlSuffixImsHome());
		soapModel.setUrlSuffixIdea(model.getUrlSuffixIdea());
		soapModel.setUrlSuffixNeed(model.getUrlSuffixNeed());
		soapModel.setUrlSuffixChallenge(model.getUrlSuffixChallenge());
		soapModel.setSenderNotificheMailIdeario(model.getSenderNotificheMailIdeario());
		soapModel.setOggettoNotificheMailIdeario(model.getOggettoNotificheMailIdeario());
		soapModel.setFirmaNotificheMailIdeario(model.getFirmaNotificheMailIdeario());
		soapModel.setUtenzaMail(model.getUtenzaMail());
		soapModel.setCdvEnabled(model.getCdvEnabled());
		soapModel.setCdvAddress(model.getCdvAddress());
		soapModel.setVcEnabled(model.getVcEnabled());
		soapModel.setVcAddress(model.getVcAddress());
		soapModel.setVcWSAddress(model.getVcWSAddress());
		soapModel.setDeEnabled(model.getDeEnabled());
		soapModel.setDeAddress(model.getDeAddress());
		soapModel.setLbbEnabled(model.getLbbEnabled());
		soapModel.setLbbAddress(model.getLbbAddress());
		soapModel.setOiaAppId4lbb(model.getOiaAppId4lbb());
		soapModel.setTweetingEnabled(model.getTweetingEnabled());
		soapModel.setBasicAuthUser(model.getBasicAuthUser());
		soapModel.setBasicAuthPwd(model.getBasicAuthPwd());
		soapModel.setVerboseEnabled(model.getVerboseEnabled());
		soapModel.setMktEnabled(model.getMktEnabled());
		soapModel.setEmailNotificationsEnabled(model.getEmailNotificationsEnabled());
		soapModel.setDockbarNotificationsEnabled(model.getDockbarNotificationsEnabled());
		soapModel.setJmsEnabled(model.getJmsEnabled());
		soapModel.setBrokerJMSusername(model.getBrokerJMSusername());
		soapModel.setBrokerJMSpassword(model.getBrokerJMSpassword());
		soapModel.setBrokerJMSurl(model.getBrokerJMSurl());
		soapModel.setJmsTopic(model.getJmsTopic());

		return soapModel;
	}

	public static CLSFriendlyUrlSuffixSoap[] toSoapModels(
		CLSFriendlyUrlSuffix[] models) {
		CLSFriendlyUrlSuffixSoap[] soapModels = new CLSFriendlyUrlSuffixSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CLSFriendlyUrlSuffixSoap[][] toSoapModels(
		CLSFriendlyUrlSuffix[][] models) {
		CLSFriendlyUrlSuffixSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CLSFriendlyUrlSuffixSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CLSFriendlyUrlSuffixSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CLSFriendlyUrlSuffixSoap[] toSoapModels(
		List<CLSFriendlyUrlSuffix> models) {
		List<CLSFriendlyUrlSuffixSoap> soapModels = new ArrayList<CLSFriendlyUrlSuffixSoap>(models.size());

		for (CLSFriendlyUrlSuffix model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CLSFriendlyUrlSuffixSoap[soapModels.size()]);
	}

	public CLSFriendlyUrlSuffixSoap() {
	}

	public long getPrimaryKey() {
		return _friendlyUrlSuffixID;
	}

	public void setPrimaryKey(long pk) {
		setFriendlyUrlSuffixID(pk);
	}

	public long getFriendlyUrlSuffixID() {
		return _friendlyUrlSuffixID;
	}

	public void setFriendlyUrlSuffixID(long friendlyUrlSuffixID) {
		_friendlyUrlSuffixID = friendlyUrlSuffixID;
	}

	public String getUrlSuffixImsHome() {
		return _UrlSuffixImsHome;
	}

	public void setUrlSuffixImsHome(String UrlSuffixImsHome) {
		_UrlSuffixImsHome = UrlSuffixImsHome;
	}

	public String getUrlSuffixIdea() {
		return _UrlSuffixIdea;
	}

	public void setUrlSuffixIdea(String UrlSuffixIdea) {
		_UrlSuffixIdea = UrlSuffixIdea;
	}

	public String getUrlSuffixNeed() {
		return _UrlSuffixNeed;
	}

	public void setUrlSuffixNeed(String UrlSuffixNeed) {
		_UrlSuffixNeed = UrlSuffixNeed;
	}

	public String getUrlSuffixChallenge() {
		return _UrlSuffixChallenge;
	}

	public void setUrlSuffixChallenge(String UrlSuffixChallenge) {
		_UrlSuffixChallenge = UrlSuffixChallenge;
	}

	public String getSenderNotificheMailIdeario() {
		return _senderNotificheMailIdeario;
	}

	public void setSenderNotificheMailIdeario(String senderNotificheMailIdeario) {
		_senderNotificheMailIdeario = senderNotificheMailIdeario;
	}

	public String getOggettoNotificheMailIdeario() {
		return _oggettoNotificheMailIdeario;
	}

	public void setOggettoNotificheMailIdeario(
		String oggettoNotificheMailIdeario) {
		_oggettoNotificheMailIdeario = oggettoNotificheMailIdeario;
	}

	public String getFirmaNotificheMailIdeario() {
		return _firmaNotificheMailIdeario;
	}

	public void setFirmaNotificheMailIdeario(String firmaNotificheMailIdeario) {
		_firmaNotificheMailIdeario = firmaNotificheMailIdeario;
	}

	public String getUtenzaMail() {
		return _utenzaMail;
	}

	public void setUtenzaMail(String utenzaMail) {
		_utenzaMail = utenzaMail;
	}

	public boolean getCdvEnabled() {
		return _cdvEnabled;
	}

	public boolean isCdvEnabled() {
		return _cdvEnabled;
	}

	public void setCdvEnabled(boolean cdvEnabled) {
		_cdvEnabled = cdvEnabled;
	}

	public String getCdvAddress() {
		return _cdvAddress;
	}

	public void setCdvAddress(String cdvAddress) {
		_cdvAddress = cdvAddress;
	}

	public boolean getVcEnabled() {
		return _vcEnabled;
	}

	public boolean isVcEnabled() {
		return _vcEnabled;
	}

	public void setVcEnabled(boolean vcEnabled) {
		_vcEnabled = vcEnabled;
	}

	public String getVcAddress() {
		return _vcAddress;
	}

	public void setVcAddress(String vcAddress) {
		_vcAddress = vcAddress;
	}

	public String getVcWSAddress() {
		return _vcWSAddress;
	}

	public void setVcWSAddress(String vcWSAddress) {
		_vcWSAddress = vcWSAddress;
	}

	public boolean getDeEnabled() {
		return _deEnabled;
	}

	public boolean isDeEnabled() {
		return _deEnabled;
	}

	public void setDeEnabled(boolean deEnabled) {
		_deEnabled = deEnabled;
	}

	public String getDeAddress() {
		return _deAddress;
	}

	public void setDeAddress(String deAddress) {
		_deAddress = deAddress;
	}

	public boolean getLbbEnabled() {
		return _lbbEnabled;
	}

	public boolean isLbbEnabled() {
		return _lbbEnabled;
	}

	public void setLbbEnabled(boolean lbbEnabled) {
		_lbbEnabled = lbbEnabled;
	}

	public String getLbbAddress() {
		return _lbbAddress;
	}

	public void setLbbAddress(String lbbAddress) {
		_lbbAddress = lbbAddress;
	}

	public String getOiaAppId4lbb() {
		return _oiaAppId4lbb;
	}

	public void setOiaAppId4lbb(String oiaAppId4lbb) {
		_oiaAppId4lbb = oiaAppId4lbb;
	}

	public boolean getTweetingEnabled() {
		return _tweetingEnabled;
	}

	public boolean isTweetingEnabled() {
		return _tweetingEnabled;
	}

	public void setTweetingEnabled(boolean tweetingEnabled) {
		_tweetingEnabled = tweetingEnabled;
	}

	public String getBasicAuthUser() {
		return _basicAuthUser;
	}

	public void setBasicAuthUser(String basicAuthUser) {
		_basicAuthUser = basicAuthUser;
	}

	public String getBasicAuthPwd() {
		return _basicAuthPwd;
	}

	public void setBasicAuthPwd(String basicAuthPwd) {
		_basicAuthPwd = basicAuthPwd;
	}

	public boolean getVerboseEnabled() {
		return _verboseEnabled;
	}

	public boolean isVerboseEnabled() {
		return _verboseEnabled;
	}

	public void setVerboseEnabled(boolean verboseEnabled) {
		_verboseEnabled = verboseEnabled;
	}

	public boolean getMktEnabled() {
		return _mktEnabled;
	}

	public boolean isMktEnabled() {
		return _mktEnabled;
	}

	public void setMktEnabled(boolean mktEnabled) {
		_mktEnabled = mktEnabled;
	}

	public boolean getEmailNotificationsEnabled() {
		return _emailNotificationsEnabled;
	}

	public boolean isEmailNotificationsEnabled() {
		return _emailNotificationsEnabled;
	}

	public void setEmailNotificationsEnabled(boolean emailNotificationsEnabled) {
		_emailNotificationsEnabled = emailNotificationsEnabled;
	}

	public boolean getDockbarNotificationsEnabled() {
		return _dockbarNotificationsEnabled;
	}

	public boolean isDockbarNotificationsEnabled() {
		return _dockbarNotificationsEnabled;
	}

	public void setDockbarNotificationsEnabled(
		boolean dockbarNotificationsEnabled) {
		_dockbarNotificationsEnabled = dockbarNotificationsEnabled;
	}

	public boolean getJmsEnabled() {
		return _jmsEnabled;
	}

	public boolean isJmsEnabled() {
		return _jmsEnabled;
	}

	public void setJmsEnabled(boolean jmsEnabled) {
		_jmsEnabled = jmsEnabled;
	}

	public String getBrokerJMSusername() {
		return _brokerJMSusername;
	}

	public void setBrokerJMSusername(String brokerJMSusername) {
		_brokerJMSusername = brokerJMSusername;
	}

	public String getBrokerJMSpassword() {
		return _brokerJMSpassword;
	}

	public void setBrokerJMSpassword(String brokerJMSpassword) {
		_brokerJMSpassword = brokerJMSpassword;
	}

	public String getBrokerJMSurl() {
		return _brokerJMSurl;
	}

	public void setBrokerJMSurl(String brokerJMSurl) {
		_brokerJMSurl = brokerJMSurl;
	}

	public String getJmsTopic() {
		return _jmsTopic;
	}

	public void setJmsTopic(String jmsTopic) {
		_jmsTopic = jmsTopic;
	}

	private long _friendlyUrlSuffixID;
	private String _UrlSuffixImsHome;
	private String _UrlSuffixIdea;
	private String _UrlSuffixNeed;
	private String _UrlSuffixChallenge;
	private String _senderNotificheMailIdeario;
	private String _oggettoNotificheMailIdeario;
	private String _firmaNotificheMailIdeario;
	private String _utenzaMail;
	private boolean _cdvEnabled;
	private String _cdvAddress;
	private boolean _vcEnabled;
	private String _vcAddress;
	private String _vcWSAddress;
	private boolean _deEnabled;
	private String _deAddress;
	private boolean _lbbEnabled;
	private String _lbbAddress;
	private String _oiaAppId4lbb;
	private boolean _tweetingEnabled;
	private String _basicAuthUser;
	private String _basicAuthPwd;
	private boolean _verboseEnabled;
	private boolean _mktEnabled;
	private boolean _emailNotificationsEnabled;
	private boolean _dockbarNotificationsEnabled;
	private boolean _jmsEnabled;
	private String _brokerJMSusername;
	private String _brokerJMSpassword;
	private String _brokerJMSurl;
	private String _jmsTopic;
}