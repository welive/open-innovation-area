package it.eng.rspa.jms;

import it.eng.reputation.model.Reputation;
import it.eng.reputation.service.ReputationLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi;
import it.eng.rspa.jms.model.IdeaCompetitionDataWrapper;
import it.eng.rspa.jms.model.IdeaDataWrapper;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang3.StringEscapeUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class MessageGenerator {
	
	private MessageGenerator(){
		//Do Nothing
	}
	
	private static String getCurrentTimeStamp(){
		SimpleDateFormat sdf = new SimpleDateFormat(Messages.getString("MessageGenerator.dateFormat")); //$NON-NLS-1$
	   	sdf.setTimeZone(TimeZone.getTimeZone(Messages.getString("MessageGenerator.timeZone"))); //$NON-NLS-1$
	   	
	   	return sdf.format(Calendar.getInstance().getTime());
	}
	
	private static String fixHTMLString(String s) {

		String clean = StringEscapeUtils.unescapeHtml4(s)
				.replaceAll("</(p|li|ul|ol)>","\\\\n")
				.replaceAll("\\<.*?>"," ")
				.replaceAll(" +"," ")
				.replaceAll("(\\\\n)+", "\\\\n");
				//.replaceAll("&amp;", "&");
		
		return clean;
	}
	
	private static String domToString(Document doc){
		//Convert XML DOM to String

		DOMSource source = new DOMSource(doc);
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		
		try { TransformerFactory.newInstance().newTransformer().transform(source, result); } 
		catch (Exception e) {
			return ""; //$NON-NLS-1$
		} 
		return StringEscapeUtils.unescapeHtml4(writer.toString());
		
	}
	
	public static String createIdeaCompetitionMessage(IdeaCompetitionDataWrapper req){
		
		/*
		 * <?xml version="1.0" encoding="UTF-8"?>
		 * <challenge>
		 *		<challengeId>2404</challengeId>
		 *   		<challengeTitle>Nuova gara 28 Luglio - test creazione</challengeTitle>
		 *   		<challengeDescription>inizio testo   punto 1  punto 2   fine testo</challengeDescription>
		 *   		<timestamp>2015-07-28T10:07:57</timestamp>
		 *   		<openingDate>2015-08-28T10:07:57</openingDate>
		 *   		<authorId>60012</authorId>
		 *   		<categories />
		 *   		<tags>
		 *      		<tag>taggara2</tag>
		 *   		</tags>
		 *   		<pois />
		 *	</challenge>
		 */

		
		try {
			Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
			
			// Root element - <challenge>
			Element rootElement = doc.createElement(Messages.getString("MessageGenerator.challenge.root")); //$NON-NLS-1$
			doc.appendChild(rootElement);

			//<challengeId>
			Element tag = doc.createElement(Messages.getString("MessageGenerator.challenge.challengeid")); //$NON-NLS-1$
			tag.appendChild(doc.createTextNode(String.valueOf(req.getChallengeId())));
			rootElement.appendChild(tag);
			
			//<challengeTitle>
			tag = doc.createElement(Messages.getString("MessageGenerator.challenge.title")); //$NON-NLS-1$
			tag.appendChild(doc.createTextNode(req.getChallengeTitle()));
			rootElement.appendChild(tag);
			
			//<challengeDescription>
			tag = doc.createElement(Messages.getString("MessageGenerator.challenge.description")); //$NON-NLS-1$
			String noHTMLString = fixHTMLString(req.getChallengeDescription());
			tag.appendChild(doc.createTextNode(noHTMLString));
			rootElement.appendChild(tag);
			
			//<timestamp>
			SimpleDateFormat sdf = new SimpleDateFormat(Messages.getString("MessageGenerator.dateFormat")); //$NON-NLS-1$
		   	sdf.setTimeZone(TimeZone.getTimeZone(Messages.getString("MessageGenerator.timeZone"))); //$NON-NLS-1$
			
			tag = doc.createElement(Messages.getString("MessageGenerator.challenge.timestamp")); //$NON-NLS-1$
			tag.appendChild(doc.createTextNode(sdf.format(req.getOpeningTimestamp())));
			rootElement.appendChild(tag);
			
			//<closingDate>
			tag = doc.createElement(Messages.getString("MessageGenerator.challenge.closingtimestamp")); //$NON-NLS-1$
			tag.appendChild(doc.createTextNode(sdf.format(req.getClosingTimestamp())));
			rootElement.appendChild(tag);
			
			//<authorId>
			tag = doc.createElement(Messages.getString("MessageGenerator.challenge.authorid")); //$NON-NLS-1$
			tag.appendChild(doc.createTextNode(String.valueOf(req.getAuthorId())));
			rootElement.appendChild(tag);

			//<authorReputation>
			List<Reputation> myRep = null;
			int reputation = 0;
			try{ 
				myRep = ReputationLocalServiceUtil.getReputationByUserId(req.getAuthorId()); 
				if(myRep!=null && !myRep.isEmpty()){
					int maxReputation = ReputationLocalServiceUtil.getOrderedReputation().get(0).getReputationScore();
					reputation = 100*(myRep.get(0).getReputationScore())/maxReputation;
				}
			}
			catch(Exception e){ reputation = 0; }
			
			tag = doc.createElement(Messages.getString("MessageGenerator.challenge.authorreputation")); //$NON-NLS-1$
			tag.appendChild(doc.createTextNode(String.valueOf(reputation)));
			rootElement.appendChild(tag);

			//<categories>
			tag = doc.createElement(Messages.getString("MessageGenerator.challenge.categories")); //$NON-NLS-1$
			Element subTagElement = null;
			Element subSubTagElement = null;
			List<String> catGroups = req.getCategoryGroups();
			Iterator<String> catGroupsIt = catGroups.iterator();
			
			while(catGroupsIt.hasNext()){
				
				String catGroup = catGroupsIt.next();
				subTagElement = doc.createElement(Messages.getString("MessageGenerator.challenge.categories.group")); //$NON-NLS-1$
				subTagElement.setAttribute(Messages.getString("MessageGenerator.challenge.categories.group.name"), catGroup); //$NON-NLS-1$
				
				tag.appendChild(subTagElement);
				
			}
			rootElement.appendChild(tag);

			
			//<tags>
			tag = doc.createElement(Messages.getString("MessageGenerator.challenge.tags")); //$NON-NLS-1$
			List<String> tags = req.getTags();		
			Iterator<String> tagsIt = tags.iterator();
			while(tagsIt.hasNext()){
				String item = tagsIt.next();
				
				//<tag>
				subTagElement = doc.createElement(Messages.getString("MessageGenerator.challenge.tags.tag")); //$NON-NLS-1$
				subTagElement.appendChild(doc.createTextNode(item));
				tag.appendChild(subTagElement);
				
			}
			rootElement.appendChild(tag);
			
			//<hashtags>
			tag = doc.createElement(Messages.getString("MessageGenerator.challenge.hashtags")); //$NON-NLS-1$
			List<String> hashtags = req.getHashtags();
			Iterator<String> hashtagsIt = hashtags.iterator();
			while(hashtagsIt.hasNext()){
				String item = "#"+hashtagsIt.next();
				
				//<hashtag>
				subTagElement = doc.createElement(Messages.getString("MessageGenerator.challenge.hashtags.hashtag")); //$NON-NLS-1$
				subTagElement.appendChild(doc.createTextNode(item));
				tag.appendChild(subTagElement);
				
			}
			rootElement.appendChild(tag);
			
			//<pois>
			tag = doc.createElement(Messages.getString("MessageGenerator.challenge.pois")); //$NON-NLS-1$
			List<CLSChallengePoi> pois = req.getPois();
			Iterator<CLSChallengePoi> poisIt = pois.iterator();
			while(poisIt.hasNext()){
				
				//<poi>
				CLSChallengePoi poi = poisIt.next();
				subTagElement = doc.createElement(Messages.getString("MessageGenerator.challenge.pois.poi")); //$NON-NLS-1$
				
				//<latitude>
				subSubTagElement = doc.createElement(Messages.getString("MessageGenerator.challenge.pois.poi.latitude")); //$NON-NLS-1$
				subSubTagElement.appendChild(doc.createTextNode(String.valueOf(poi.getLatitude())));
				subTagElement.appendChild(subSubTagElement);
				
				//<longitude>
				subSubTagElement = doc.createElement(Messages.getString("MessageGenerator.challenge.pois.poi.longitude")); //$NON-NLS-1$
				subSubTagElement.appendChild(doc.createTextNode(String.valueOf(poi.getLongitude())));
				subTagElement.appendChild(subSubTagElement);
				
				//<descriptionPoi>
				subSubTagElement = doc.createElement(Messages.getString("MessageGenerator.challenge.pois.poi.description")); //$NON-NLS-1$
				subSubTagElement.appendChild(doc.createTextNode(poi.getDescription()));
				subTagElement.appendChild(subSubTagElement);
				
				//<poiId>
				subSubTagElement = doc.createElement(Messages.getString("MessageGenerator.challenge.pois.poi.id")); //$NON-NLS-1$
				subSubTagElement.appendChild(doc.createTextNode(String.valueOf(poi.getPoiId())));
				subTagElement.appendChild(subSubTagElement);
				
				//<title>
				subSubTagElement = doc.createElement(Messages.getString("MessageGenerator.challenges.pois.poi.title")); //$NON-NLS-1$
				subSubTagElement.appendChild(doc.createTextNode(poi.getTitle()));
				subTagElement.appendChild(subSubTagElement);
				
				tag.appendChild(subTagElement);
				
			}
			
			rootElement.appendChild(tag);
			
			
			doc.setXmlVersion(Messages.getString("MessageGenerator.challenge.xmlVersion")); //$NON-NLS-1$
			doc.setXmlStandalone(true);

			return domToString(doc);
	 
		} 
		catch (Exception e) { 
			return ""; //$NON-NLS-1$
		}
	}
	
	public static String createIdeaMessage(IdeaDataWrapper req){
		
		try {
			Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
			
			// Root element
			Element rootElement = doc.createElement(Messages.getString("MessageGenerator.idea.root")); //$NON-NLS-1$
			doc.appendChild(rootElement);

			Element tag = doc.createElement(Messages.getString("MessageGenerator.idea.ideaid")); //$NON-NLS-1$
					
			tag.appendChild(doc.createTextNode(String.valueOf(req.getIdeaId())));
			rootElement.appendChild(tag);
			
			tag = doc.createElement(Messages.getString("MessageGenerator.idea.challangeid")); //$NON-NLS-1$
			tag.appendChild(doc.createTextNode(req.getChallengeId()));
			rootElement.appendChild(tag);
			
			tag = doc.createElement(Messages.getString("MessageGenerator.idea.title")); //$NON-NLS-1$
			tag.appendChild(doc.createTextNode(req.getIdeaTitle()));
			rootElement.appendChild(tag);
			
			tag = doc.createElement(Messages.getString("MessageGenerator.idea.description")); //$NON-NLS-1$
			String noHTMLString = fixHTMLString(req.getIdeaDescription());
			tag.appendChild(doc.createTextNode(noHTMLString));
			rootElement.appendChild(tag);
			
			tag = doc.createElement(Messages.getString("MessageGenerator.idea.timestamp")); //$NON-NLS-1$
			tag.appendChild(doc.createTextNode(getCurrentTimeStamp()));
			rootElement.appendChild(tag);
			
			tag = doc.createElement(Messages.getString("MessageGenerator.idea.authorid")); //$NON-NLS-1$
			tag.appendChild(doc.createTextNode(String.valueOf(req.getAuthorId())));
			rootElement.appendChild(tag);

			int reputation = 0;
			List<Reputation> myRep;
			try {
				myRep = ReputationLocalServiceUtil.getReputationByUserId(Long.parseLong(req.getAuthorId()));
				if(myRep!=null && !myRep.isEmpty()){
					int maxReputation = ReputationLocalServiceUtil.getOrderedReputation().get(0).getReputationScore();
					reputation = 100*(myRep.get(0).getReputationScore())/maxReputation;
				}
			}
			catch(Exception e) { reputation = 0; }
			
			tag = doc.createElement(Messages.getString("MessageGenerator.idea.authorreputation")); //$NON-NLS-1$
			tag.appendChild(doc.createTextNode(String.valueOf(reputation)));
			rootElement.appendChild(tag);

			tag = doc.createElement(Messages.getString("MessageGenerator.idea.categories")); //$NON-NLS-1$
			Element subTagElement = null;
			Element subSubTagElement = null;
			Iterator <String> catIt = null;
			HashMap<String, List<String>> catGroups = req.getCategoryGroups();
			
			Set<String> vocab = catGroups.keySet();
			for(String key: vocab) {

				subTagElement = doc.createElement(Messages.getString("MessageGenerator.idea.categories.group")); //$NON-NLS-1$
				subTagElement.setAttribute(Messages.getString("MessageGenerator.idea.categories.group.name"), key); //$NON-NLS-1$

				List<String> ls = catGroups.get(key);
				catIt = ls.iterator();
				while(catIt.hasNext()){
					String currCat = catIt.next();
					subSubTagElement = doc.createElement(Messages.getString("MessageGenerator.idea.categories.category")); //$NON-NLS-1$
					subSubTagElement.appendChild(doc.createTextNode(currCat));
					subTagElement.appendChild(subSubTagElement);
				}
				
				tag.appendChild(subTagElement);
				
			}
			rootElement.appendChild(tag);

			tag = doc.createElement(Messages.getString("MessageGenerator.idea.tags")); //$NON-NLS-1$
			List<String> tags = req.getTags();		
			Iterator<String> tagsIt = tags.iterator();
			while(tagsIt.hasNext()){
				String item = tagsIt.next();
				
				subTagElement = doc.createElement(Messages.getString("MessageGenerator.idea.tags.tag")); //$NON-NLS-1$
				subTagElement.appendChild(doc.createTextNode(item));
				tag.appendChild(subTagElement);
				
			}
			rootElement.appendChild(tag);
			
			tag = doc.createElement(Messages.getString("MessageGenerator.idea.hashtags")); //$NON-NLS-1$
			List<String> hashtags = req.getHashtags();
			Iterator<String> hashtagsIt = hashtags.iterator();
			while(hashtagsIt.hasNext()){
				String item = "#"+hashtagsIt.next();
				
				subTagElement = doc.createElement(Messages.getString("MessageGenerator.idea.hashtags.hashtag")); //$NON-NLS-1$
				subTagElement.appendChild(doc.createTextNode(item));
				tag.appendChild(subTagElement);
				
			}
			rootElement.appendChild(tag);
			
			tag = doc.createElement(Messages.getString("MessageGenerator.idea.pois")); //$NON-NLS-1$
			List<CLSIdeaPoi> pois = req.getPois();
			Iterator<CLSIdeaPoi> poisIt = pois.iterator();
			while(poisIt.hasNext()){
				CLSIdeaPoi poi = poisIt.next();
				subTagElement = doc.createElement(Messages.getString("MessageGenerator.idea.pois.poi")); //$NON-NLS-1$
				
				subSubTagElement = doc.createElement(Messages.getString("MessageGenerator.idea.pois.poi.latitude")); //$NON-NLS-1$
				subSubTagElement.appendChild(doc.createTextNode(String.valueOf(poi.getLatitude())));
				subTagElement.appendChild(subSubTagElement);
				
				subSubTagElement = doc.createElement(Messages.getString("MessageGenerator.idea.pois.poi.longitude")); //$NON-NLS-1$
				subSubTagElement.appendChild(doc.createTextNode(String.valueOf(poi.getLongitude())));
				subTagElement.appendChild(subSubTagElement);
				
				subSubTagElement = doc.createElement(Messages.getString("MessageGenerator.idea.pois.poi.description")); //$NON-NLS-1$
				subSubTagElement.appendChild(doc.createTextNode(poi.getDescription()));
				subTagElement.appendChild(subSubTagElement);
				
				subSubTagElement = doc.createElement(Messages.getString("MessageGenerator.idea.pois.poi.id")); //$NON-NLS-1$
				subSubTagElement.appendChild(doc.createTextNode(String.valueOf(poi.getPoiId())));
				subTagElement.appendChild(subSubTagElement);
				
				subSubTagElement = doc.createElement(Messages.getString("MessageGenerator.idea.pois.poi.title")); //$NON-NLS-1$
				subSubTagElement.appendChild(doc.createTextNode(poi.getTitle()));
				subTagElement.appendChild(subSubTagElement);
				
				tag.appendChild(subTagElement);
				
			}
			rootElement.appendChild(tag);
			
			tag = doc.createElement(Messages.getString("MessageGenerator.idea.coworkers")); //$NON-NLS-1$
			List<CLSCoworker> coworkers = req.getCoworkers();
			Iterator<CLSCoworker> coworkersIt = coworkers.iterator();
			while(coworkersIt.hasNext()){
				CLSCoworker cw = coworkersIt.next();
				subTagElement = doc.createElement(Messages.getString("MessageGenerator.idea.coworkers.coworker")); //$NON-NLS-1$
				
				subSubTagElement = doc.createElement(Messages.getString("MessageGenerator.idea.coworkers.coworker.id")); //$NON-NLS-1$
				subSubTagElement.appendChild(doc.createTextNode(String.valueOf(cw.getUserId())));
				subTagElement.appendChild(subSubTagElement);
				
				reputation = 0;
				try {
					myRep = ReputationLocalServiceUtil.getReputationByUserId(cw.getUserId());
					if(myRep!=null && !myRep.isEmpty()){
						int maxReputation = ReputationLocalServiceUtil.getOrderedReputation().get(0).getReputationScore();
						reputation = 100*(myRep.get(0).getReputationScore())/maxReputation;
					}
				}
				catch(Exception e) { reputation = 0; }
				
				subSubTagElement = doc.createElement(Messages.getString("MessageGenerator.idea.coworkers.coworker.reputation")); //$NON-NLS-1$
				subSubTagElement.appendChild(doc.createTextNode(String.valueOf(reputation)));
				subTagElement.appendChild(subSubTagElement);
				tag.appendChild(subTagElement);
				
			}
			rootElement.appendChild(tag);
			
			doc.setXmlVersion(Messages.getString("MessageGenerator.idea.xmlVersion")); //$NON-NLS-1$
			doc.setXmlStandalone(true);
			
			return domToString(doc);
	 
		} 
		catch (Exception e) { 
			return ""; //$NON-NLS-1$
		}
		
	}
	
	public static String createIdeaRemovedMessage(long ideaId){
		
		try {
			Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
			
			// Root element
			Element rootElement = doc.createElement(Messages.getString("MessageGenerator.ideadelete.root")); //$NON-NLS-1$
			doc.appendChild(rootElement);

			Element tag = doc.createElement(Messages.getString("MessageGenerator.ideadelete.id")); //$NON-NLS-1$
			tag.appendChild(doc.createTextNode(String.valueOf(ideaId)));
			rootElement.appendChild(tag);
			
			tag = doc.createElement(Messages.getString("MessageGenerator.ideadelete.timestamp")); //$NON-NLS-1$
			tag.appendChild(doc.createTextNode(getCurrentTimeStamp()));
			rootElement.appendChild(tag);
			
			doc.setXmlVersion(Messages.getString("MessageGenerator.ideadelete.xmlVersion")); //$NON-NLS-1$
			doc.setXmlStandalone(true);
			
			return domToString(doc);
	 
		} 
		catch (Exception e) { 
			return "";//$NON-NLS-1$
		}
	}
	
	public static String createIdeaCompetitionRemovedMessage(long challengeId){
		
		try {
			Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
			
			// Root element
			Element rootElement = doc.createElement(Messages.getString("MessageGenerator.challengedelete.root")); //$NON-NLS-1$
			doc.appendChild(rootElement);

			Element tag = doc.createElement(Messages.getString("MessageGenerator.challengedelete.id")); //$NON-NLS-1$
			tag.appendChild(doc.createTextNode(String.valueOf(challengeId)));
			rootElement.appendChild(tag);
			
			tag = doc.createElement(Messages.getString("MessageGenerator.challengedelete.timestamp")); //$NON-NLS-1$
			tag.appendChild(doc.createTextNode(getCurrentTimeStamp()));
			rootElement.appendChild(tag);
			
			doc.setXmlVersion(Messages.getString("MessageGenerator.challengedelete.xmlVersion")); //$NON-NLS-1$
			doc.setXmlStandalone(true);
			
			return domToString(doc);
	 
		} 
		catch (Exception e) { 
			return ""; //$NON-NLS-1$
		}
	}
	
}
