package it.eng.rspa.jms;

import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;
import it.eng.rspa.jms.model.IdeaCompetitionDataWrapper;
import it.eng.rspa.jms.model.IdeaDataWrapper;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.liferay.util.portlet.PortletProps;

public class Producer {
	
	private static String brokerurl = IdeaManagementSystemProperties.getProperty("brokerJMSurl"); //$NON-NLS-1$
	private static String username = IdeaManagementSystemProperties.getProperty("brokerJMSusername"); //$NON-NLS-1$ 
	private static String password = IdeaManagementSystemProperties.getProperty("brokerJMSpassword"); //$NON-NLS-1$
	private String topicName;
	
	public static String CHALLENGE = Messages.getString("Producer.challenge");  //$NON-NLS-1$
	public static String CHALLENGE_COMMENT = Messages.getString("Producer.challengecomment"); //$NON-NLS-1$
	public static String DEL_CHALLANGE = Messages.getString("Producer.challengeremoved");  //$NON-NLS-1$
	public static String DEL_CHALLENGE_COMMENT = Messages.getString("Producer.challengecommentremoved");  //$NON-NLS-1$
	public static String IDEA = Messages.getString("Producer.idea");  //$NON-NLS-1$
	public static String IDEA_COMMENT = Messages.getString("Producer.ideacomment");  //$NON-NLS-1$
	public static String DEL_IDEA = Messages.getString("Producer.idearemoved");  //$NON-NLS-1$
	public static String DEL_IDEA_COMMENT = Messages.getString("Producer.ideacommentremoved"); //$NON-NLS-1$
	public static String QOS_USER_FEEDBACK = Messages.getString("Producer.qosfeedback"); //$NON-NLS-1$


	public Producer(String topicName) throws Exception {
		if(!topicName.trim().equals("")) //$NON-NLS-1$
			this.topicName = topicName;
		else
			throw new Exception("Empty topic name is not allowed"); //$NON-NLS-1$
	}

	public static String getBrokerUrl() {
		return Producer.brokerurl;
	}

	public static void setBrokerUrl(String url) {
		Producer.brokerurl = url;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	private static String getUsername() {
		return username;
	}

	public static void setUsername(String username) {
		Producer.username = username;
	}

	private static String getPassword() {
		return password;
	}

	public static void setPassword(String password) {
		Producer.password = password;
	}

	private void sendJMSMessage(String msg, String msgType){
		
		Connection connection = null;
		try{
			
            connection = (new ActiveMQConnectionFactory(getBrokerUrl())).createConnection(getUsername(), getPassword());
            
			connection.start();

            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Topic topic = session.createTopic(this.topicName);
	        
            TextMessage txtMsg = session.createTextMessage();
	        			txtMsg.setText(msg);
	        			txtMsg.setJMSType(msgType);
	        
	        MessageProducer producer = session.createProducer(topic);
	        				producer.send(txtMsg);
	        
    	}
    	catch(Exception e){
    		System.out.println("errore sendJMSMessage: " + e.getMessage()); //$NON-NLS-1$
    	}
		finally{
			try{
				connection.close();
			}
			catch(JMSException je){
				System.out.println("Impossibile chiudere la connessione: "+je.getMessage()); //$NON-NLS-1$
			}
		}
	}
	
	public void sendChallenge(IdeaCompetitionDataWrapper data){
		String msg = MessageGenerator.createIdeaCompetitionMessage(data);
		sendJMSMessage(msg, CHALLENGE);
	}
	
	public void sendIdea(IdeaDataWrapper data){
		String msg = MessageGenerator.createIdeaMessage(data);
		sendJMSMessage(msg, IDEA);
	}
	
	public void sendChallengeRemoved(long challengeId){
		String msg = MessageGenerator.createIdeaCompetitionRemovedMessage(challengeId);
		sendJMSMessage(msg, DEL_CHALLANGE);		
	}
	
	public void sendIdeaRemoved(long ideaId){
		String msg = MessageGenerator.createIdeaRemovedMessage(ideaId);
		sendJMSMessage(msg, DEL_IDEA);		
	}
	
}
