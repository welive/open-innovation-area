package it.eng.rspa.jms.model;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCategoriesSetForChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCoworkerLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaPoiLocalServiceUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.portlet.PortletException;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portlet.asset.model.AssetCategory;
import com.liferay.portlet.asset.model.AssetEntry;
import com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetTagLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil;

public class IdeaDataWrapper{

	
	private String ideaId;
	private String challengeId;
	private String ideaTitle;
	private String ideaDescription;
	private String authorId;
	private int authorReputation;
	private HashMap<String, List<String>> categoryGroups;
	private List<String> tags;
	private List<String> hashtags;
	private List<CLSIdeaPoi> pois;
	private List<CLSCoworker> coworkers;
	
	public IdeaDataWrapper(long ideaId) throws Exception{
		
		CLSIdea idea = CLSIdeaLocalServiceUtil.getCLSIdea(ideaId);
		
		setIdeaId(String.valueOf(ideaId));		
		setChallengeId(String.valueOf(idea.getChallengeId()));
		setIdeaTitle(idea.getIdeaTitle());
		setIdeaDescription(idea.getIdeaDescription());
		setAuthorId(String.valueOf(idea.getUserId()));
		setAuthorReputation(0);
		
		try{setCategoryGroups(ideaId);}
		catch(Exception e) {this.categoryGroups=new HashMap<String, List<String>>();}
		setTags(AssetTagLocalServiceUtil.getTagNames(CLSIdea.class.getName(), ideaId));
		setHashtags(ideaId);
		setPois(CLSIdeaPoiLocalServiceUtil.getIdeaPOIByIdeaId(ideaId));
		setCoworkers(CLSCoworkerLocalServiceUtil.getCoworkersByIdeaId(ideaId));
		
	}
	
	public IdeaDataWrapper(String ideaId, String challengeId,
			String ideaTitle, String ideaDescription, String authorId,
			int authorReputation, List<String> tags, List<CLSIdeaPoi> pois, List<CLSCoworker> coworkers) {

		setIdeaId(ideaId);
		setChallengeId(challengeId);
		setIdeaTitle(ideaTitle);
		setIdeaDescription(ideaDescription);
		setAuthorId(authorId);
		try { setAuthorReputation(authorReputation); } 
		catch (Exception e) { this.authorReputation = 0; }
		try{setCategoryGroups(Long.parseLong(ideaId));}
		catch(Exception e) {this.categoryGroups = new HashMap<String, List<String>>();}
		setTags(tags);
		setHashtags(Long.parseLong(ideaId));
		setPois(pois);
		setCoworkers(coworkers);
	}
	
	public String getIdeaId() {
		return ideaId;
	}
	public void setIdeaId(String ideaId) {
		this.ideaId = ideaId;
	}
	public String getChallengeId() {
		return challengeId;
	}
	public void setChallengeId(String challengeId) {
		this.challengeId = challengeId;
	}
	public String getIdeaTitle() {
		return ideaTitle;
	}
	public void setIdeaTitle(String ideaTitle) {
		this.ideaTitle = ideaTitle;
	}
	public String getIdeaDescription() {
		return ideaDescription;
	}
	public void setIdeaDescription(String ideaDescription) {
		this.ideaDescription = ideaDescription;
	}
	public String getAuthorId() {
		return authorId;
	}
	public void setAuthorId(String authorId) {
		this.authorId = authorId;
	}
	public int getAuthorReputation() {
		return authorReputation;
	}
	public void setAuthorReputation(int authorReputation) throws Exception {
		if(authorReputation>=0 && authorReputation<=100)
			this.authorReputation = authorReputation;
		else
			throw new Exception("Invalid author reputation value: "+authorReputation);
	}
	
	public HashMap<String, List<String>> getCategoryGroups() {
		return categoryGroups;
	}
	public void setCategoryGroups(long ideaId) 
			throws PortalException, SystemException, PortletException {
		
		this.categoryGroups = new HashMap<String, List<String>>();
		
		CLSIdea idea = CLSIdeaLocalServiceUtil.getCLSIdea(ideaId);
		CLSChallenge challenge = CLSChallengeLocalServiceUtil.getCLSChallenge(idea.getChallengeId());
		
		//AssetEntry challengeEntry = AssetEntryLocalServiceUtil.getEntry(CLSChallenge.class.getName(), challenge.getChallengeId());
		List<CLSCategoriesSetForChallenge> categoriesList =CLSCategoriesSetForChallengeLocalServiceUtil.getCategoriesSetForChallenge(challenge.getChallengeId());
		Iterator<CLSCategoriesSetForChallenge> cListIt = categoriesList.iterator();
		while(cListIt.hasNext()) {
			long categoriesSetID = cListIt.next().getCategoriesSetID();
			String challangeCategory = AssetVocabularyLocalServiceUtil.fetchAssetVocabulary(categoriesSetID).getName();
			this.categoryGroups.put(challangeCategory, new ArrayList<String>());
		}
		
		AssetEntry ideaEntry = AssetEntryLocalServiceUtil.getEntry(CLSIdea.class.getName(), ideaId);
		List<AssetCategory> ideasList = ideaEntry.getCategories();
		Iterator<AssetCategory> iListIt = ideasList.iterator();
		while(iListIt.hasNext()) {
			try{
				AssetCategory ac = iListIt.next();
				this.categoryGroups.get(AssetVocabularyLocalServiceUtil.getAssetVocabulary(ac.getVocabularyId()).getName()).add(ac.getName());
			}
			catch(Exception e) {
				//Skip this category
				System.out.println(e.getMessage());
			}
		}

	}
	
	public List<String> getTags() {
		return tags;
	}
	public void setTags(List<String> tags) {
		if(tags!=null)
			this.tags = new ArrayList<String>(tags);
		else
			this.tags = new ArrayList<String>();
	}
	public void setTags(String[] tagStrings){
		this.tags = Arrays.asList(tagStrings);
	}
	public void addTag(String newTag){
		this.tags.add(newTag);
	}
	
	public List<String> getHashtags() {
		return hashtags;
	}

	public void setHashtags(long challengeId) {
		this.hashtags = new ArrayList<String>();
		this.hashtags.add("IMS_I"+challengeId);
	}
	
	public List<CLSIdeaPoi> getPois() {
		return pois;
	}
	public void setPois(List<CLSIdeaPoi> pois) {
		if(pois!=null)
			this.pois = new ArrayList<CLSIdeaPoi>(pois);
		else
			this.pois = new ArrayList<CLSIdeaPoi>();
	}
	public void addPoi(CLSIdeaPoi newPoi){
		this.pois.add(newPoi);
	}
	
	public List<CLSCoworker> getCoworkers() {
		return coworkers;
	}
	public void setCoworkers(List<CLSCoworker> coworkers) {
		if(coworkers!=null)
			this.coworkers = new ArrayList<CLSCoworker>(coworkers);
		else
			this.coworkers = new ArrayList<CLSCoworker>();
		
	}
	public void addCoworker(CLSCoworker newCoworker){
		this.coworkers.add(newCoworker);
	}

}
