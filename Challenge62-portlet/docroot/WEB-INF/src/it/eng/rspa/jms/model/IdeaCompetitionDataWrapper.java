package it.eng.rspa.jms.model;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCategoriesSetForChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengePoiLocalServiceUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.portlet.PortletException;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portlet.asset.service.AssetTagLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil;

public class IdeaCompetitionDataWrapper{

	private long challengeId;
	private String challengeTitle;
	private String challengeDescription;
	private long openingTimestamp;
	private long closingTimestamp;
	private long authorId;
	private long authorReputation;
	private List<String> categoryGroups;
	private List<String> tags;
	private List<String> hashtags;
	private List<CLSChallengePoi> pois;

	public IdeaCompetitionDataWrapper(long challengeId) throws Exception{
		
		CLSChallenge challenge = CLSChallengeLocalServiceUtil.getCLSChallenge(challengeId);
		setChallengeId(challengeId);
		setChallengeTitle(challenge.getChallengeTitle());
		setChallengeDescription(challenge.getChallengeDescription());
		setOpeningTimestamp(challenge.getDateStart().getTime());
		setClosingTimestamp(challenge.getDateEnd().getTime());
		setAuthorId(challenge.getUserId());
		setAuthorReputation(0);
		setTags(AssetTagLocalServiceUtil.getTagNames(CLSChallenge.class.getName(), challengeId));
		setHashtags(challengeId);
		setPois(CLSChallengePoiLocalServiceUtil.getChallengePOIByChallengeId(challengeId));
		setCategoryGroups(challengeId);
	}
	
	public IdeaCompetitionDataWrapper(long challengeId,
			String challengeTitle, String challengeDescription, Date open, Date close, long authorId,
			long authorReputation, List<String> tags, List<CLSChallengePoi> pois) {

		setChallengeId(challengeId);
		setChallengeTitle(challengeTitle);
		setChallengeDescription(challengeDescription);
		setOpeningTimestamp(open.getTime());
		setClosingTimestamp(close.getTime());
		setAuthorId(authorId);
		try { setAuthorReputation(authorReputation); } 
		catch (Exception e) { this.authorReputation = 0; }
		try { setCategoryGroups(challengeId); } 
		catch (Exception e) { e.printStackTrace(); }
		setTags(tags);
		setHashtags(challengeId);
		setPois(pois);
	}
	
	public long getOpeningTimestamp() {
		return this.openingTimestamp;
	}
	
	public long getClosingTimestamp() {
		return this.closingTimestamp;
	}
	
	public void setClosingTimestamp(long time) {
		this.closingTimestamp = time;
		
	}

	public void setOpeningTimestamp(long time) {
		this.openingTimestamp = time;
	}

	public long getChallengeId() {
		return challengeId;
	}

	public void setChallengeId(long challengeId) {
		this.challengeId = challengeId;
	}

	public String getChallengeTitle() {
		return challengeTitle;
	}

	public void setChallengeTitle(String challengeTitle) {
		this.challengeTitle = challengeTitle;
	}

	public String getChallengeDescription() {
		return challengeDescription;
	}

	public void setChallengeDescription(String challengeDescription) {
		this.challengeDescription = challengeDescription;
	}

	public long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(long authorId) {
		this.authorId = authorId;
	}

	public long getAuthorReputation() {
		return authorReputation;
	}

	public void setAuthorReputation(long authorReputation) throws Exception {
		if(authorReputation>=0 && authorReputation<=100)
			this.authorReputation = authorReputation;
		else
			throw new Exception("Invalid author reputation value: "+authorReputation);
	}

	public List<String> getCategoryGroups() {
		return categoryGroups;
	}

	public void setCategoryGroups(long challengeId) 
			throws PortalException, SystemException, PortletException {
		this.categoryGroups = new ArrayList<String>();
		
		CLSChallenge challenge = CLSChallengeLocalServiceUtil.getCLSChallenge(challengeId);
		List<CLSCategoriesSetForChallenge> categoriesList =CLSCategoriesSetForChallengeLocalServiceUtil.getCategoriesSetForChallenge(challenge.getChallengeId());
		Iterator<CLSCategoriesSetForChallenge> cListIt = categoriesList.iterator();
		while(cListIt.hasNext()) {
			long categoriesSetID = cListIt.next().getCategoriesSetID();
			String challangeCategory = AssetVocabularyLocalServiceUtil.fetchAssetVocabulary(categoriesSetID).getName();
			this.categoryGroups.add(challangeCategory);
		}
	}
	
	public void addCategoryGroup(String newCG){
		this.categoryGroups.add(newCG);
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		if(tags!=null)
			this.tags = new ArrayList<String>(tags);
		else
			this.tags = new ArrayList<String>();
	}
	
	public void setTags(String[] tagStrings){
		this.tags = Arrays.asList(tagStrings);
	}
	
	public List<String> getHashtags() {
		return hashtags;
	}

	public void setHashtags(long challengeId) {
		this.hashtags = new ArrayList<String>();
		this.hashtags.add("IMS_G"+challengeId);
	}
	
	public void addTag(String newTag){
		this.tags.add(newTag);
	}

	public List<CLSChallengePoi> getPois() {
		return pois;
	}

	public void setPois(List<CLSChallengePoi> pois) {
		if(pois!=null)
			this.pois = new ArrayList<CLSChallengePoi>(pois);
		else
			this.pois = new ArrayList<CLSChallengePoi>();
	}
	
	public void addPoi(CLSChallengePoi newPoi){
		this.pois.add(newPoi);
	}

}
