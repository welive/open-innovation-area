package it.eng.rspa.jms.model;

public class IdeaCommentDataWrapper{

	private long ideaId;
	private long commentId;
	private long authorId;
	private int authorReputation;
	private String textComment;
	private int like;
	private int dislike;
	private int problemRelevant;
	private int solveProblem;
	private int solutionRealistic;
	
	public IdeaCommentDataWrapper(long ideaId, long commentId,
			long authorId, int authorReputation, String textComment, int like,
			int dislike, int problemRelevant, int solveProblem,
			int solutionRealistic) {
		
		setIdeaId(ideaId);
		setCommentId(commentId);
		setAuthorId(authorId);
		
		try { setAuthorReputation(authorReputation); } 
		catch (Exception e) { this.authorReputation = 0; };
		
		setTextComment(textComment);
		setLike(like);
		setDislike(dislike);
		
		try { setProblemRelevant(problemRelevant); } 
		catch (Exception e) { this.problemRelevant = 0; }
		
		try { setSolveProblem(solveProblem); } 
		catch (Exception e) { this.solveProblem = 0; }
		
		try { setSolutionRealistic(solutionRealistic); } 
		catch (Exception e) { this.solutionRealistic = 0; }
	}
	
	public long getIdeaId() {
		return ideaId;
	}
	public void setIdeaId(long ideaId) {
		this.ideaId = ideaId;
	}
	public long getCommentId() {
		return commentId;
	}
	public void setCommentId(long commentId) {
		this.commentId = commentId;
	}
	public long getAuthorId() {
		return authorId;
	}
	public void setAuthorId(long authorId) {
		this.authorId = authorId;
	}
	public int getAuthorReputation() {
		return authorReputation;
	}
	public void setAuthorReputation(int authorReputation) throws Exception {
		if(authorReputation>=0 && authorReputation<=100)
			this.authorReputation = authorReputation;
		else
			throw new Exception("Invalid author reputation value: "+authorReputation);
	}
	public String getTextComment() {
		return textComment;
	}
	public void setTextComment(String textComment) {
		this.textComment = textComment;
	}
	public int getLike() {
		return like;
	}
	public void setLike(int like) {
		this.like = like;
	}
	public int getDislike() {
		return dislike;
	}
	public void setDislike(int dislike) {
		this.dislike = dislike;
	}
	public int getProblemRelevant() {
		return problemRelevant;
	}
	public void setProblemRelevant(int problemRelevant) throws Exception{
		if(problemRelevant>=0 && problemRelevant<=5)	
			this.problemRelevant = problemRelevant;
		else
			throw new Exception("Invalid Problem Relevant value "+solutionRealistic);
	}
	public int getSolveProblem() {
		return solveProblem;
	}
	public void setSolveProblem(int solveProblem) throws Exception{
		if(solveProblem>=0 && solveProblem<=5)	
			this.solveProblem = solveProblem;
		else
			throw new Exception("Invalid Solve Problem value "+solutionRealistic);
	}
	public int getSolutionRealistic() {
		return solutionRealistic;
	}
	public void setSolutionRealistic(int solutionRealistic) throws Exception{
		if(solutionRealistic>=0 && solutionRealistic<=5)
			this.solutionRealistic = solutionRealistic;
		else
			throw new Exception("Invalid Solution Realistic value "+solutionRealistic);
	}
	
}
