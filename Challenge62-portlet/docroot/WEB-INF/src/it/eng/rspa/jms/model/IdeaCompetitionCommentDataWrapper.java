package it.eng.rspa.jms.model;

public class IdeaCompetitionCommentDataWrapper{
	
	private long challengeId;
	private String commentId;
	private long authorId;
	private long authorReputation;
	private String textComment;
	private int like;
	private int dislike;
	
	public IdeaCompetitionCommentDataWrapper(long challengeId,
			String commentId, long authorId, long authorReputation,
			String textComment, int like, int dislike) {
		
		setChallengeId(challengeId);
		
		setCommentId(commentId);
		
		setAuthorId(authorId);
		
		try { setAuthorReputation(authorReputation); } 
		catch (Exception e) { this.authorReputation = 0; }
		
		setTextComment(textComment);
		
		try { setLike(like); } 
		catch (Exception e) { this.like = 0; }
		
		try { setDislike(dislike); } 
		catch (Exception e) { this.dislike = 0; }
	}
	public long getChallengeId() {
		return challengeId;
	}
	public void setChallengeId(long challengeId) {
		this.challengeId = challengeId;
	}
	public String getCommentId() {
		return commentId;
	}
	public void setCommentId(String commentId) {
		this.commentId = commentId;
	}
	public long getAuthorId() {
		return authorId;
	}
	public void setAuthorId(long authorId) {
		this.authorId = authorId;
	}
	public long getAuthorReputation() {
		return authorReputation;
	}
	public void setAuthorReputation(long authorReputation) throws Exception {
		if(authorReputation>=0 && authorReputation<=100)
			this.authorReputation = authorReputation;
		else
			throw new Exception("Invalid author reputation value: "+authorReputation);
	}
	public String getTextComment() {
		return textComment;
	}
	public void setTextComment(String textComment) {
		this.textComment = textComment;
	}
	public int getLike() {
		return like;
	}
	public void setLike(int like) throws Exception{
		if(like==1)
			this.like = like;
		else
			throw new Exception("Invalid like value");
	}
	public int getDislike() {
		return dislike;
	}
	public void setDislike(int dislike) throws Exception{
		if(like==-1)
			this.dislike = dislike;
		else
			throw new Exception("Invalid dislike value");
	}
}
