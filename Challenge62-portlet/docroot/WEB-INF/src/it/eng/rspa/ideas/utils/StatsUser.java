package it.eng.rspa.ideas.utils;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;

public class StatsUser {

	
	public static int getNeedsNumberByUser(User user){
	
	int needNumber = 0;
	
	List<CLSIdea> ideasStats;
	try {
		ideasStats = CLSIdeaLocalServiceUtil.getIdeasByUserId(user.getUserId());
		
		for (CLSIdea idea:ideasStats ){
			
			if (idea.getIsNeed())
				needNumber++;
		}
		
		
	} catch (SystemException e) {
		
		e.printStackTrace();
	}
	

	return needNumber;
	
	}
	
	
}
