/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException;
import it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the idea evaluation service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see IdeaEvaluationPersistence
 * @see IdeaEvaluationUtil
 * @generated
 */
public class IdeaEvaluationPersistenceImpl extends BasePersistenceImpl<IdeaEvaluation>
	implements IdeaEvaluationPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link IdeaEvaluationUtil} to access the idea evaluation persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = IdeaEvaluationImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(IdeaEvaluationModelImpl.ENTITY_CACHE_ENABLED,
			IdeaEvaluationModelImpl.FINDER_CACHE_ENABLED,
			IdeaEvaluationImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(IdeaEvaluationModelImpl.ENTITY_CACHE_ENABLED,
			IdeaEvaluationModelImpl.FINDER_CACHE_ENABLED,
			IdeaEvaluationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(IdeaEvaluationModelImpl.ENTITY_CACHE_ENABLED,
			IdeaEvaluationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_CRITERIAANDIDEAID = new FinderPath(IdeaEvaluationModelImpl.ENTITY_CACHE_ENABLED,
			IdeaEvaluationModelImpl.FINDER_CACHE_ENABLED,
			IdeaEvaluationImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchBycriteriaAndIdeaId",
			new String[] { Long.class.getName(), Long.class.getName() },
			IdeaEvaluationModelImpl.CRITERIAID_COLUMN_BITMASK |
			IdeaEvaluationModelImpl.IDEAID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CRITERIAANDIDEAID = new FinderPath(IdeaEvaluationModelImpl.ENTITY_CACHE_ENABLED,
			IdeaEvaluationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countBycriteriaAndIdeaId",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns the idea evaluation where criteriaId = &#63; and ideaId = &#63; or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException} if it could not be found.
	 *
	 * @param criteriaId the criteria ID
	 * @param ideaId the idea ID
	 * @return the matching idea evaluation
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a matching idea evaluation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaEvaluation findBycriteriaAndIdeaId(long criteriaId, long ideaId)
		throws NoSuchIdeaEvaluationException, SystemException {
		IdeaEvaluation ideaEvaluation = fetchBycriteriaAndIdeaId(criteriaId,
				ideaId);

		if (ideaEvaluation == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("criteriaId=");
			msg.append(criteriaId);

			msg.append(", ideaId=");
			msg.append(ideaId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchIdeaEvaluationException(msg.toString());
		}

		return ideaEvaluation;
	}

	/**
	 * Returns the idea evaluation where criteriaId = &#63; and ideaId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param criteriaId the criteria ID
	 * @param ideaId the idea ID
	 * @return the matching idea evaluation, or <code>null</code> if a matching idea evaluation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaEvaluation fetchBycriteriaAndIdeaId(long criteriaId, long ideaId)
		throws SystemException {
		return fetchBycriteriaAndIdeaId(criteriaId, ideaId, true);
	}

	/**
	 * Returns the idea evaluation where criteriaId = &#63; and ideaId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param criteriaId the criteria ID
	 * @param ideaId the idea ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching idea evaluation, or <code>null</code> if a matching idea evaluation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaEvaluation fetchBycriteriaAndIdeaId(long criteriaId,
		long ideaId, boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { criteriaId, ideaId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_CRITERIAANDIDEAID,
					finderArgs, this);
		}

		if (result instanceof IdeaEvaluation) {
			IdeaEvaluation ideaEvaluation = (IdeaEvaluation)result;

			if ((criteriaId != ideaEvaluation.getCriteriaId()) ||
					(ideaId != ideaEvaluation.getIdeaId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_IDEAEVALUATION_WHERE);

			query.append(_FINDER_COLUMN_CRITERIAANDIDEAID_CRITERIAID_2);

			query.append(_FINDER_COLUMN_CRITERIAANDIDEAID_IDEAID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(criteriaId);

				qPos.add(ideaId);

				List<IdeaEvaluation> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CRITERIAANDIDEAID,
						finderArgs, list);
				}
				else {
					IdeaEvaluation ideaEvaluation = list.get(0);

					result = ideaEvaluation;

					cacheResult(ideaEvaluation);

					if ((ideaEvaluation.getCriteriaId() != criteriaId) ||
							(ideaEvaluation.getIdeaId() != ideaId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CRITERIAANDIDEAID,
							finderArgs, ideaEvaluation);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CRITERIAANDIDEAID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (IdeaEvaluation)result;
		}
	}

	/**
	 * Removes the idea evaluation where criteriaId = &#63; and ideaId = &#63; from the database.
	 *
	 * @param criteriaId the criteria ID
	 * @param ideaId the idea ID
	 * @return the idea evaluation that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaEvaluation removeBycriteriaAndIdeaId(long criteriaId, long ideaId)
		throws NoSuchIdeaEvaluationException, SystemException {
		IdeaEvaluation ideaEvaluation = findBycriteriaAndIdeaId(criteriaId,
				ideaId);

		return remove(ideaEvaluation);
	}

	/**
	 * Returns the number of idea evaluations where criteriaId = &#63; and ideaId = &#63;.
	 *
	 * @param criteriaId the criteria ID
	 * @param ideaId the idea ID
	 * @return the number of matching idea evaluations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBycriteriaAndIdeaId(long criteriaId, long ideaId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CRITERIAANDIDEAID;

		Object[] finderArgs = new Object[] { criteriaId, ideaId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_IDEAEVALUATION_WHERE);

			query.append(_FINDER_COLUMN_CRITERIAANDIDEAID_CRITERIAID_2);

			query.append(_FINDER_COLUMN_CRITERIAANDIDEAID_IDEAID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(criteriaId);

				qPos.add(ideaId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CRITERIAANDIDEAID_CRITERIAID_2 = "ideaEvaluation.id.criteriaId = ? AND ";
	private static final String _FINDER_COLUMN_CRITERIAANDIDEAID_IDEAID_2 = "ideaEvaluation.id.ideaId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CRITERIAID =
		new FinderPath(IdeaEvaluationModelImpl.ENTITY_CACHE_ENABLED,
			IdeaEvaluationModelImpl.FINDER_CACHE_ENABLED,
			IdeaEvaluationImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findBycriteriaId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CRITERIAID =
		new FinderPath(IdeaEvaluationModelImpl.ENTITY_CACHE_ENABLED,
			IdeaEvaluationModelImpl.FINDER_CACHE_ENABLED,
			IdeaEvaluationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBycriteriaId",
			new String[] { Long.class.getName() },
			IdeaEvaluationModelImpl.CRITERIAID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CRITERIAID = new FinderPath(IdeaEvaluationModelImpl.ENTITY_CACHE_ENABLED,
			IdeaEvaluationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBycriteriaId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the idea evaluations where criteriaId = &#63;.
	 *
	 * @param criteriaId the criteria ID
	 * @return the matching idea evaluations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<IdeaEvaluation> findBycriteriaId(long criteriaId)
		throws SystemException {
		return findBycriteriaId(criteriaId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the idea evaluations where criteriaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param criteriaId the criteria ID
	 * @param start the lower bound of the range of idea evaluations
	 * @param end the upper bound of the range of idea evaluations (not inclusive)
	 * @return the range of matching idea evaluations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<IdeaEvaluation> findBycriteriaId(long criteriaId, int start,
		int end) throws SystemException {
		return findBycriteriaId(criteriaId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the idea evaluations where criteriaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param criteriaId the criteria ID
	 * @param start the lower bound of the range of idea evaluations
	 * @param end the upper bound of the range of idea evaluations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching idea evaluations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<IdeaEvaluation> findBycriteriaId(long criteriaId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CRITERIAID;
			finderArgs = new Object[] { criteriaId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CRITERIAID;
			finderArgs = new Object[] { criteriaId, start, end, orderByComparator };
		}

		List<IdeaEvaluation> list = (List<IdeaEvaluation>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (IdeaEvaluation ideaEvaluation : list) {
				if ((criteriaId != ideaEvaluation.getCriteriaId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_IDEAEVALUATION_WHERE);

			query.append(_FINDER_COLUMN_CRITERIAID_CRITERIAID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(IdeaEvaluationModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(criteriaId);

				if (!pagination) {
					list = (List<IdeaEvaluation>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<IdeaEvaluation>(list);
				}
				else {
					list = (List<IdeaEvaluation>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first idea evaluation in the ordered set where criteriaId = &#63;.
	 *
	 * @param criteriaId the criteria ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching idea evaluation
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a matching idea evaluation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaEvaluation findBycriteriaId_First(long criteriaId,
		OrderByComparator orderByComparator)
		throws NoSuchIdeaEvaluationException, SystemException {
		IdeaEvaluation ideaEvaluation = fetchBycriteriaId_First(criteriaId,
				orderByComparator);

		if (ideaEvaluation != null) {
			return ideaEvaluation;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("criteriaId=");
		msg.append(criteriaId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchIdeaEvaluationException(msg.toString());
	}

	/**
	 * Returns the first idea evaluation in the ordered set where criteriaId = &#63;.
	 *
	 * @param criteriaId the criteria ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching idea evaluation, or <code>null</code> if a matching idea evaluation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaEvaluation fetchBycriteriaId_First(long criteriaId,
		OrderByComparator orderByComparator) throws SystemException {
		List<IdeaEvaluation> list = findBycriteriaId(criteriaId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last idea evaluation in the ordered set where criteriaId = &#63;.
	 *
	 * @param criteriaId the criteria ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching idea evaluation
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a matching idea evaluation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaEvaluation findBycriteriaId_Last(long criteriaId,
		OrderByComparator orderByComparator)
		throws NoSuchIdeaEvaluationException, SystemException {
		IdeaEvaluation ideaEvaluation = fetchBycriteriaId_Last(criteriaId,
				orderByComparator);

		if (ideaEvaluation != null) {
			return ideaEvaluation;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("criteriaId=");
		msg.append(criteriaId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchIdeaEvaluationException(msg.toString());
	}

	/**
	 * Returns the last idea evaluation in the ordered set where criteriaId = &#63;.
	 *
	 * @param criteriaId the criteria ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching idea evaluation, or <code>null</code> if a matching idea evaluation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaEvaluation fetchBycriteriaId_Last(long criteriaId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBycriteriaId(criteriaId);

		if (count == 0) {
			return null;
		}

		List<IdeaEvaluation> list = findBycriteriaId(criteriaId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the idea evaluations before and after the current idea evaluation in the ordered set where criteriaId = &#63;.
	 *
	 * @param ideaEvaluationPK the primary key of the current idea evaluation
	 * @param criteriaId the criteria ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next idea evaluation
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a idea evaluation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaEvaluation[] findBycriteriaId_PrevAndNext(
		IdeaEvaluationPK ideaEvaluationPK, long criteriaId,
		OrderByComparator orderByComparator)
		throws NoSuchIdeaEvaluationException, SystemException {
		IdeaEvaluation ideaEvaluation = findByPrimaryKey(ideaEvaluationPK);

		Session session = null;

		try {
			session = openSession();

			IdeaEvaluation[] array = new IdeaEvaluationImpl[3];

			array[0] = getBycriteriaId_PrevAndNext(session, ideaEvaluation,
					criteriaId, orderByComparator, true);

			array[1] = ideaEvaluation;

			array[2] = getBycriteriaId_PrevAndNext(session, ideaEvaluation,
					criteriaId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected IdeaEvaluation getBycriteriaId_PrevAndNext(Session session,
		IdeaEvaluation ideaEvaluation, long criteriaId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_IDEAEVALUATION_WHERE);

		query.append(_FINDER_COLUMN_CRITERIAID_CRITERIAID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(IdeaEvaluationModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(criteriaId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(ideaEvaluation);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<IdeaEvaluation> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the idea evaluations where criteriaId = &#63; from the database.
	 *
	 * @param criteriaId the criteria ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBycriteriaId(long criteriaId) throws SystemException {
		for (IdeaEvaluation ideaEvaluation : findBycriteriaId(criteriaId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(ideaEvaluation);
		}
	}

	/**
	 * Returns the number of idea evaluations where criteriaId = &#63;.
	 *
	 * @param criteriaId the criteria ID
	 * @return the number of matching idea evaluations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBycriteriaId(long criteriaId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CRITERIAID;

		Object[] finderArgs = new Object[] { criteriaId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_IDEAEVALUATION_WHERE);

			query.append(_FINDER_COLUMN_CRITERIAID_CRITERIAID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(criteriaId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CRITERIAID_CRITERIAID_2 = "ideaEvaluation.id.criteriaId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEAID = new FinderPath(IdeaEvaluationModelImpl.ENTITY_CACHE_ENABLED,
			IdeaEvaluationModelImpl.FINDER_CACHE_ENABLED,
			IdeaEvaluationImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByideaId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID =
		new FinderPath(IdeaEvaluationModelImpl.ENTITY_CACHE_ENABLED,
			IdeaEvaluationModelImpl.FINDER_CACHE_ENABLED,
			IdeaEvaluationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByideaId",
			new String[] { Long.class.getName() },
			IdeaEvaluationModelImpl.IDEAID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IDEAID = new FinderPath(IdeaEvaluationModelImpl.ENTITY_CACHE_ENABLED,
			IdeaEvaluationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByideaId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the idea evaluations where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @return the matching idea evaluations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<IdeaEvaluation> findByideaId(long ideaId)
		throws SystemException {
		return findByideaId(ideaId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the idea evaluations where ideaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param start the lower bound of the range of idea evaluations
	 * @param end the upper bound of the range of idea evaluations (not inclusive)
	 * @return the range of matching idea evaluations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<IdeaEvaluation> findByideaId(long ideaId, int start, int end)
		throws SystemException {
		return findByideaId(ideaId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the idea evaluations where ideaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param start the lower bound of the range of idea evaluations
	 * @param end the upper bound of the range of idea evaluations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching idea evaluations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<IdeaEvaluation> findByideaId(long ideaId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID;
			finderArgs = new Object[] { ideaId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEAID;
			finderArgs = new Object[] { ideaId, start, end, orderByComparator };
		}

		List<IdeaEvaluation> list = (List<IdeaEvaluation>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (IdeaEvaluation ideaEvaluation : list) {
				if ((ideaId != ideaEvaluation.getIdeaId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_IDEAEVALUATION_WHERE);

			query.append(_FINDER_COLUMN_IDEAID_IDEAID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(IdeaEvaluationModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				if (!pagination) {
					list = (List<IdeaEvaluation>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<IdeaEvaluation>(list);
				}
				else {
					list = (List<IdeaEvaluation>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first idea evaluation in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching idea evaluation
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a matching idea evaluation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaEvaluation findByideaId_First(long ideaId,
		OrderByComparator orderByComparator)
		throws NoSuchIdeaEvaluationException, SystemException {
		IdeaEvaluation ideaEvaluation = fetchByideaId_First(ideaId,
				orderByComparator);

		if (ideaEvaluation != null) {
			return ideaEvaluation;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchIdeaEvaluationException(msg.toString());
	}

	/**
	 * Returns the first idea evaluation in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching idea evaluation, or <code>null</code> if a matching idea evaluation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaEvaluation fetchByideaId_First(long ideaId,
		OrderByComparator orderByComparator) throws SystemException {
		List<IdeaEvaluation> list = findByideaId(ideaId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last idea evaluation in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching idea evaluation
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a matching idea evaluation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaEvaluation findByideaId_Last(long ideaId,
		OrderByComparator orderByComparator)
		throws NoSuchIdeaEvaluationException, SystemException {
		IdeaEvaluation ideaEvaluation = fetchByideaId_Last(ideaId,
				orderByComparator);

		if (ideaEvaluation != null) {
			return ideaEvaluation;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchIdeaEvaluationException(msg.toString());
	}

	/**
	 * Returns the last idea evaluation in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching idea evaluation, or <code>null</code> if a matching idea evaluation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaEvaluation fetchByideaId_Last(long ideaId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByideaId(ideaId);

		if (count == 0) {
			return null;
		}

		List<IdeaEvaluation> list = findByideaId(ideaId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the idea evaluations before and after the current idea evaluation in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaEvaluationPK the primary key of the current idea evaluation
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next idea evaluation
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a idea evaluation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaEvaluation[] findByideaId_PrevAndNext(
		IdeaEvaluationPK ideaEvaluationPK, long ideaId,
		OrderByComparator orderByComparator)
		throws NoSuchIdeaEvaluationException, SystemException {
		IdeaEvaluation ideaEvaluation = findByPrimaryKey(ideaEvaluationPK);

		Session session = null;

		try {
			session = openSession();

			IdeaEvaluation[] array = new IdeaEvaluationImpl[3];

			array[0] = getByideaId_PrevAndNext(session, ideaEvaluation, ideaId,
					orderByComparator, true);

			array[1] = ideaEvaluation;

			array[2] = getByideaId_PrevAndNext(session, ideaEvaluation, ideaId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected IdeaEvaluation getByideaId_PrevAndNext(Session session,
		IdeaEvaluation ideaEvaluation, long ideaId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_IDEAEVALUATION_WHERE);

		query.append(_FINDER_COLUMN_IDEAID_IDEAID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(IdeaEvaluationModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(ideaId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(ideaEvaluation);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<IdeaEvaluation> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the idea evaluations where ideaId = &#63; from the database.
	 *
	 * @param ideaId the idea ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByideaId(long ideaId) throws SystemException {
		for (IdeaEvaluation ideaEvaluation : findByideaId(ideaId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(ideaEvaluation);
		}
	}

	/**
	 * Returns the number of idea evaluations where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @return the number of matching idea evaluations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByideaId(long ideaId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IDEAID;

		Object[] finderArgs = new Object[] { ideaId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_IDEAEVALUATION_WHERE);

			query.append(_FINDER_COLUMN_IDEAID_IDEAID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IDEAID_IDEAID_2 = "ideaEvaluation.id.ideaId = ?";

	public IdeaEvaluationPersistenceImpl() {
		setModelClass(IdeaEvaluation.class);
	}

	/**
	 * Caches the idea evaluation in the entity cache if it is enabled.
	 *
	 * @param ideaEvaluation the idea evaluation
	 */
	@Override
	public void cacheResult(IdeaEvaluation ideaEvaluation) {
		EntityCacheUtil.putResult(IdeaEvaluationModelImpl.ENTITY_CACHE_ENABLED,
			IdeaEvaluationImpl.class, ideaEvaluation.getPrimaryKey(),
			ideaEvaluation);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CRITERIAANDIDEAID,
			new Object[] {
				ideaEvaluation.getCriteriaId(), ideaEvaluation.getIdeaId()
			}, ideaEvaluation);

		ideaEvaluation.resetOriginalValues();
	}

	/**
	 * Caches the idea evaluations in the entity cache if it is enabled.
	 *
	 * @param ideaEvaluations the idea evaluations
	 */
	@Override
	public void cacheResult(List<IdeaEvaluation> ideaEvaluations) {
		for (IdeaEvaluation ideaEvaluation : ideaEvaluations) {
			if (EntityCacheUtil.getResult(
						IdeaEvaluationModelImpl.ENTITY_CACHE_ENABLED,
						IdeaEvaluationImpl.class, ideaEvaluation.getPrimaryKey()) == null) {
				cacheResult(ideaEvaluation);
			}
			else {
				ideaEvaluation.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all idea evaluations.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(IdeaEvaluationImpl.class.getName());
		}

		EntityCacheUtil.clearCache(IdeaEvaluationImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the idea evaluation.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(IdeaEvaluation ideaEvaluation) {
		EntityCacheUtil.removeResult(IdeaEvaluationModelImpl.ENTITY_CACHE_ENABLED,
			IdeaEvaluationImpl.class, ideaEvaluation.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(ideaEvaluation);
	}

	@Override
	public void clearCache(List<IdeaEvaluation> ideaEvaluations) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (IdeaEvaluation ideaEvaluation : ideaEvaluations) {
			EntityCacheUtil.removeResult(IdeaEvaluationModelImpl.ENTITY_CACHE_ENABLED,
				IdeaEvaluationImpl.class, ideaEvaluation.getPrimaryKey());

			clearUniqueFindersCache(ideaEvaluation);
		}
	}

	protected void cacheUniqueFindersCache(IdeaEvaluation ideaEvaluation) {
		if (ideaEvaluation.isNew()) {
			Object[] args = new Object[] {
					ideaEvaluation.getCriteriaId(), ideaEvaluation.getIdeaId()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CRITERIAANDIDEAID,
				args, Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CRITERIAANDIDEAID,
				args, ideaEvaluation);
		}
		else {
			IdeaEvaluationModelImpl ideaEvaluationModelImpl = (IdeaEvaluationModelImpl)ideaEvaluation;

			if ((ideaEvaluationModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_CRITERIAANDIDEAID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						ideaEvaluation.getCriteriaId(),
						ideaEvaluation.getIdeaId()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CRITERIAANDIDEAID,
					args, Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CRITERIAANDIDEAID,
					args, ideaEvaluation);
			}
		}
	}

	protected void clearUniqueFindersCache(IdeaEvaluation ideaEvaluation) {
		IdeaEvaluationModelImpl ideaEvaluationModelImpl = (IdeaEvaluationModelImpl)ideaEvaluation;

		Object[] args = new Object[] {
				ideaEvaluation.getCriteriaId(), ideaEvaluation.getIdeaId()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CRITERIAANDIDEAID,
			args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CRITERIAANDIDEAID,
			args);

		if ((ideaEvaluationModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_CRITERIAANDIDEAID.getColumnBitmask()) != 0) {
			args = new Object[] {
					ideaEvaluationModelImpl.getOriginalCriteriaId(),
					ideaEvaluationModelImpl.getOriginalIdeaId()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CRITERIAANDIDEAID,
				args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CRITERIAANDIDEAID,
				args);
		}
	}

	/**
	 * Creates a new idea evaluation with the primary key. Does not add the idea evaluation to the database.
	 *
	 * @param ideaEvaluationPK the primary key for the new idea evaluation
	 * @return the new idea evaluation
	 */
	@Override
	public IdeaEvaluation create(IdeaEvaluationPK ideaEvaluationPK) {
		IdeaEvaluation ideaEvaluation = new IdeaEvaluationImpl();

		ideaEvaluation.setNew(true);
		ideaEvaluation.setPrimaryKey(ideaEvaluationPK);

		return ideaEvaluation;
	}

	/**
	 * Removes the idea evaluation with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param ideaEvaluationPK the primary key of the idea evaluation
	 * @return the idea evaluation that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a idea evaluation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaEvaluation remove(IdeaEvaluationPK ideaEvaluationPK)
		throws NoSuchIdeaEvaluationException, SystemException {
		return remove((Serializable)ideaEvaluationPK);
	}

	/**
	 * Removes the idea evaluation with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the idea evaluation
	 * @return the idea evaluation that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a idea evaluation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaEvaluation remove(Serializable primaryKey)
		throws NoSuchIdeaEvaluationException, SystemException {
		Session session = null;

		try {
			session = openSession();

			IdeaEvaluation ideaEvaluation = (IdeaEvaluation)session.get(IdeaEvaluationImpl.class,
					primaryKey);

			if (ideaEvaluation == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchIdeaEvaluationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(ideaEvaluation);
		}
		catch (NoSuchIdeaEvaluationException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected IdeaEvaluation removeImpl(IdeaEvaluation ideaEvaluation)
		throws SystemException {
		ideaEvaluation = toUnwrappedModel(ideaEvaluation);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(ideaEvaluation)) {
				ideaEvaluation = (IdeaEvaluation)session.get(IdeaEvaluationImpl.class,
						ideaEvaluation.getPrimaryKeyObj());
			}

			if (ideaEvaluation != null) {
				session.delete(ideaEvaluation);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (ideaEvaluation != null) {
			clearCache(ideaEvaluation);
		}

		return ideaEvaluation;
	}

	@Override
	public IdeaEvaluation updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation ideaEvaluation)
		throws SystemException {
		ideaEvaluation = toUnwrappedModel(ideaEvaluation);

		boolean isNew = ideaEvaluation.isNew();

		IdeaEvaluationModelImpl ideaEvaluationModelImpl = (IdeaEvaluationModelImpl)ideaEvaluation;

		Session session = null;

		try {
			session = openSession();

			if (ideaEvaluation.isNew()) {
				session.save(ideaEvaluation);

				ideaEvaluation.setNew(false);
			}
			else {
				session.merge(ideaEvaluation);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !IdeaEvaluationModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((ideaEvaluationModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CRITERIAID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						ideaEvaluationModelImpl.getOriginalCriteriaId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CRITERIAID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CRITERIAID,
					args);

				args = new Object[] { ideaEvaluationModelImpl.getCriteriaId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CRITERIAID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CRITERIAID,
					args);
			}

			if ((ideaEvaluationModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						ideaEvaluationModelImpl.getOriginalIdeaId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID,
					args);

				args = new Object[] { ideaEvaluationModelImpl.getIdeaId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID,
					args);
			}
		}

		EntityCacheUtil.putResult(IdeaEvaluationModelImpl.ENTITY_CACHE_ENABLED,
			IdeaEvaluationImpl.class, ideaEvaluation.getPrimaryKey(),
			ideaEvaluation);

		clearUniqueFindersCache(ideaEvaluation);
		cacheUniqueFindersCache(ideaEvaluation);

		return ideaEvaluation;
	}

	protected IdeaEvaluation toUnwrappedModel(IdeaEvaluation ideaEvaluation) {
		if (ideaEvaluation instanceof IdeaEvaluationImpl) {
			return ideaEvaluation;
		}

		IdeaEvaluationImpl ideaEvaluationImpl = new IdeaEvaluationImpl();

		ideaEvaluationImpl.setNew(ideaEvaluation.isNew());
		ideaEvaluationImpl.setPrimaryKey(ideaEvaluation.getPrimaryKey());

		ideaEvaluationImpl.setCriteriaId(ideaEvaluation.getCriteriaId());
		ideaEvaluationImpl.setIdeaId(ideaEvaluation.getIdeaId());
		ideaEvaluationImpl.setMotivation(ideaEvaluation.getMotivation());
		ideaEvaluationImpl.setPassed(ideaEvaluation.isPassed());
		ideaEvaluationImpl.setScore(ideaEvaluation.getScore());
		ideaEvaluationImpl.setDate(ideaEvaluation.getDate());
		ideaEvaluationImpl.setAuthorId(ideaEvaluation.getAuthorId());

		return ideaEvaluationImpl;
	}

	/**
	 * Returns the idea evaluation with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the idea evaluation
	 * @return the idea evaluation
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a idea evaluation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaEvaluation findByPrimaryKey(Serializable primaryKey)
		throws NoSuchIdeaEvaluationException, SystemException {
		IdeaEvaluation ideaEvaluation = fetchByPrimaryKey(primaryKey);

		if (ideaEvaluation == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchIdeaEvaluationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return ideaEvaluation;
	}

	/**
	 * Returns the idea evaluation with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException} if it could not be found.
	 *
	 * @param ideaEvaluationPK the primary key of the idea evaluation
	 * @return the idea evaluation
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a idea evaluation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaEvaluation findByPrimaryKey(IdeaEvaluationPK ideaEvaluationPK)
		throws NoSuchIdeaEvaluationException, SystemException {
		return findByPrimaryKey((Serializable)ideaEvaluationPK);
	}

	/**
	 * Returns the idea evaluation with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the idea evaluation
	 * @return the idea evaluation, or <code>null</code> if a idea evaluation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaEvaluation fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		IdeaEvaluation ideaEvaluation = (IdeaEvaluation)EntityCacheUtil.getResult(IdeaEvaluationModelImpl.ENTITY_CACHE_ENABLED,
				IdeaEvaluationImpl.class, primaryKey);

		if (ideaEvaluation == _nullIdeaEvaluation) {
			return null;
		}

		if (ideaEvaluation == null) {
			Session session = null;

			try {
				session = openSession();

				ideaEvaluation = (IdeaEvaluation)session.get(IdeaEvaluationImpl.class,
						primaryKey);

				if (ideaEvaluation != null) {
					cacheResult(ideaEvaluation);
				}
				else {
					EntityCacheUtil.putResult(IdeaEvaluationModelImpl.ENTITY_CACHE_ENABLED,
						IdeaEvaluationImpl.class, primaryKey,
						_nullIdeaEvaluation);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(IdeaEvaluationModelImpl.ENTITY_CACHE_ENABLED,
					IdeaEvaluationImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return ideaEvaluation;
	}

	/**
	 * Returns the idea evaluation with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param ideaEvaluationPK the primary key of the idea evaluation
	 * @return the idea evaluation, or <code>null</code> if a idea evaluation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public IdeaEvaluation fetchByPrimaryKey(IdeaEvaluationPK ideaEvaluationPK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)ideaEvaluationPK);
	}

	/**
	 * Returns all the idea evaluations.
	 *
	 * @return the idea evaluations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<IdeaEvaluation> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the idea evaluations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of idea evaluations
	 * @param end the upper bound of the range of idea evaluations (not inclusive)
	 * @return the range of idea evaluations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<IdeaEvaluation> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the idea evaluations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of idea evaluations
	 * @param end the upper bound of the range of idea evaluations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of idea evaluations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<IdeaEvaluation> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<IdeaEvaluation> list = (List<IdeaEvaluation>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_IDEAEVALUATION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_IDEAEVALUATION;

				if (pagination) {
					sql = sql.concat(IdeaEvaluationModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<IdeaEvaluation>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<IdeaEvaluation>(list);
				}
				else {
					list = (List<IdeaEvaluation>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the idea evaluations from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (IdeaEvaluation ideaEvaluation : findAll()) {
			remove(ideaEvaluation);
		}
	}

	/**
	 * Returns the number of idea evaluations.
	 *
	 * @return the number of idea evaluations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_IDEAEVALUATION);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the idea evaluation persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<IdeaEvaluation>> listenersList = new ArrayList<ModelListener<IdeaEvaluation>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<IdeaEvaluation>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(IdeaEvaluationImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_IDEAEVALUATION = "SELECT ideaEvaluation FROM IdeaEvaluation ideaEvaluation";
	private static final String _SQL_SELECT_IDEAEVALUATION_WHERE = "SELECT ideaEvaluation FROM IdeaEvaluation ideaEvaluation WHERE ";
	private static final String _SQL_COUNT_IDEAEVALUATION = "SELECT COUNT(ideaEvaluation) FROM IdeaEvaluation ideaEvaluation";
	private static final String _SQL_COUNT_IDEAEVALUATION_WHERE = "SELECT COUNT(ideaEvaluation) FROM IdeaEvaluation ideaEvaluation WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "ideaEvaluation.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No IdeaEvaluation exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No IdeaEvaluation exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(IdeaEvaluationPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"date"
			});
	private static IdeaEvaluation _nullIdeaEvaluation = new IdeaEvaluationImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<IdeaEvaluation> toCacheModel() {
				return _nullIdeaEvaluationCacheModel;
			}
		};

	private static CacheModel<IdeaEvaluation> _nullIdeaEvaluationCacheModel = new CacheModel<IdeaEvaluation>() {
			@Override
			public IdeaEvaluation toEntityModel() {
				return _nullIdeaEvaluation;
			}
		};
}