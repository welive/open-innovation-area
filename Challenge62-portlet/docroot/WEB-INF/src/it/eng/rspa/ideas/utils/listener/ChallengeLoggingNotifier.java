package it.eng.rspa.ideas.utils.listener;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;
import it.eng.rspa.ideas.utils.MyUtils;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;

public class ChallengeLoggingNotifier extends
		LoggingBuildingBlockNotificationService implements iChallengeListener {

	private enum Actions { publication, modification, deletion };
	
	private JSONObject prepareObject(Actions action, CLSChallenge c){
		
		JSONObject json = JSONFactoryUtil.createJSONObject();
		
		/** ADD COMMON FIELDS **/
		//Add the ChallengeID
			long nChallengeID = c.getChallengeId();
			json.put("challengeid", nChallengeID);
		
		
		//Add the PilotId
			long enteId = c.getUserId();
			User enteUser = null;
			try { enteUser = UserLocalServiceUtil.getUser(enteId); } 
			catch (Exception e) {
				e.printStackTrace();
				_log.error("No Municipality exists with id "+enteId);
				return null;
			}
			String pilotId = MyUtils.getPilotByUser(enteUser);
			
						
			if(pilotId==null || pilotId.equals("")){
				_log.error("No pilotId associated with the challenge "+nChallengeID);
				return null;
			}
			json.put("pilot", pilotId);
		
		switch(action){
			case publication:
			case deletion:
				//Add the AuthorityName
				String authName =MyUtils.getOrganizationNameByMunicipalityId(enteUser.getUserId());
				
					//String authName = enteUser.getFullName();
					json.put("authorityname", authName);
				break;
				
			default:
				return null;
		}
		
		return json;
		
	}
	
	@Override
	public void onChallengePublish(CLSChallenge c) {
		if(c==null){
			_log.error("No challenge to be notified");
			return;
		}
		
		JSONObject json = prepareObject(Actions.publication, c);
		if(json==null){ return; }
		
	 	sendNotification("ChallengePublished",json);
	 	return;
	}

	@Override
	public void onChallengeDelete(CLSChallenge c) {
		if(c==null){
			_log.error("No challenge to be notified");
			return;
		}
		
		JSONObject json = prepareObject(Actions.deletion, c);
		if(json==null){ return; }
		
	 	sendNotification("ChallengeRemoved",json);
	 	return;
	}

	@Override
	public void onChallengeUpdate(CLSChallenge c) {
		// Do nothing

	}

}
