/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengesCalendar;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing CLSChallengesCalendar in entity cache.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSChallengesCalendar
 * @generated
 */
public class CLSChallengesCalendarCacheModel implements CacheModel<CLSChallengesCalendar>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{challengesCalendarId=");
		sb.append(challengesCalendarId);
		sb.append(", referenceCalendarId=");
		sb.append(referenceCalendarId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CLSChallengesCalendar toEntityModel() {
		CLSChallengesCalendarImpl clsChallengesCalendarImpl = new CLSChallengesCalendarImpl();

		clsChallengesCalendarImpl.setChallengesCalendarId(challengesCalendarId);
		clsChallengesCalendarImpl.setReferenceCalendarId(referenceCalendarId);

		clsChallengesCalendarImpl.resetOriginalValues();

		return clsChallengesCalendarImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		challengesCalendarId = objectInput.readLong();
		referenceCalendarId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(challengesCalendarId);
		objectOutput.writeLong(referenceCalendarId);
	}

	public long challengesCalendarId;
	public long referenceCalendarId;
}