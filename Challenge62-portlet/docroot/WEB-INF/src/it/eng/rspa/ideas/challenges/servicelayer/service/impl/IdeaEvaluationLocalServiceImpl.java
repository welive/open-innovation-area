/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException;
import it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria;
import it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation;
import it.eng.rspa.ideas.challenges.servicelayer.service.base.IdeaEvaluationLocalServiceBaseImpl;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationUtil;

/**
 * The implementation of the idea evaluation local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.ideas.challenges.servicelayer.service.IdeaEvaluationLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.IdeaEvaluationLocalServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.IdeaEvaluationLocalServiceUtil
 */
public class IdeaEvaluationLocalServiceImpl
	extends IdeaEvaluationLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.ideas.challenges.servicelayer.service.IdeaEvaluationLocalServiceUtil} to access the idea evaluation local service.
	 */
	
	
	/**
	 * @param ideaId
	 * @return
	 */
	public List<IdeaEvaluation> getIdeaEvaluationsByIdeaId(long ideaId){		
		
		List<IdeaEvaluation> ret = new ArrayList<IdeaEvaluation>();
		
		try {
			return IdeaEvaluationUtil.findByideaId(ideaId);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return ret;
		}
		
	}
	
	/**
	 * @param criteriaId
	 * @param ideaId
	 * @return
	 */
	public IdeaEvaluation getIdeaEvaluationByCriteriaIdAndIdeaId(long criteriaId, long ideaId){		
		
		IdeaEvaluation ret = null;
		
			try {
				return IdeaEvaluationUtil.findBycriteriaAndIdeaId(criteriaId, ideaId);
			} catch (NoSuchIdeaEvaluationException | SystemException e) {
				e.printStackTrace();
				return ret;
			}
		
	}
	
	/**
	 * @param criteriaId
	 * @return
	 */
	public List<IdeaEvaluation> getIdeaEvaluationsByCriteriaId(long criteriaId){		
		
		List<IdeaEvaluation> ret = new ArrayList<IdeaEvaluation>();
		
		try {
			return IdeaEvaluationUtil.findBycriteriaId(criteriaId);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return ret;
		}
		
	}
	
}