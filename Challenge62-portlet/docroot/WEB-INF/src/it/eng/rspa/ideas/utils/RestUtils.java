package it.eng.rspa.ideas.utils;

import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;

import java.awt.Container;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.net.Authenticator;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.imageio.ImageIO;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.core.MediaType;

import org.fusesource.hawtbuf.ByteArrayOutputStream;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.util.portlet.PortletProps;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.client.urlconnection.HTTPSProperties;



public abstract class RestUtils {
	
	
	//Formato JSON di Liferay
	public static String consumePostWs (JSONObject json, String endpoint, String user , String password) throws Exception{
		
		if(endpoint==null || endpoint.equals(""))
			throw new Exception("A valid endpoint must be specified. \""+endpoint+"\" has been provided");
		
	
		Client client= Client.create();
		
		//Client client= Client.create(configureClient());
		if (!user.equals(""))
			client.addFilter(new HTTPBasicAuthFilter(user, password));//Basic Authentication
		
		WebResource webResource = client.resource(endpoint);
		
		if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
			System.out.println("Inviato: "+json.toString());
		}
		ClientResponse resp = webResource.post(ClientResponse.class, json.toString());
		String risposta = resp.getEntity(String.class);
		if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
			
			System.out.println("Response ["+resp.getStatus()+"] "+risposta);
		}
		
		
		return risposta;
	}
	
	//Formato JSON di Liferay
	public static String consumePostWs (JSONObject json, String endpoint) throws Exception{
		
		//Basic Authentication
		String user = IdeaManagementSystemProperties.getProperty("basicAuthUser");
		String password = IdeaManagementSystemProperties.getProperty("basicAuthPwd");
		
		
		if(endpoint==null || endpoint.equals(""))
			throw new Exception("A valid endpoint must be specified. \""+endpoint+"\" has been provided");
		
	
		Client client= Client.create();
		
		//Client client= Client.create(configureClient());
		if (!user.equals(""))
			client.addFilter(new HTTPBasicAuthFilter(user, password));//Basic Authentication
		
		WebResource webResource = client.resource(endpoint);
		
		if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
			System.out.println("Inviato: "+json.toString());
		}
		ClientResponse resp = webResource.post(ClientResponse.class, json.toString());
		String risposta = resp.getEntity(String.class);
		if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
			
			System.out.println("Response ["+resp.getStatus()+"] "+risposta);
		}
		
		
		return risposta;
	}
	
	
	//Formato JSON di Liferay
		public static String consumePostWsJSONFormat (JSONObject json, String endpoint) throws Exception{
			
			//Basic Authentication
			String user = IdeaManagementSystemProperties.getProperty("basicAuthUser");
			String password = IdeaManagementSystemProperties.getProperty("basicAuthPwd");
			
			
			if(endpoint==null || endpoint.equals(""))
				throw new Exception("A valid endpoint must be specified. \""+endpoint+"\" has been provided");
			
		
			Client client= Client.create();
			
			//Client client= Client.create(configureClient());
			if (!user.equals(""))
				client.addFilter(new HTTPBasicAuthFilter(user, password));//Basic Authentication
			
			WebResource webResource = client.resource(endpoint);
			
			if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
				System.out.println("Inviato: "+json.toString());
			}
			ClientResponse resp = webResource.accept("application/json").type("application/json").post(ClientResponse.class, json.toString());
			
			
			String risposta = resp.getEntity(String.class);
			
			if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
			
				System.out.println("Response ["+resp.getStatus()+"] "+risposta);
			}
			
			return risposta;
		}
	
	
	
	//Formato JSON di Liferay
	public static String consumePostWs (JSONArray json, String endpoint) throws Exception{
		
		//Basic Authentication
		String user = IdeaManagementSystemProperties.getProperty("basicAuthUser");
		String password = IdeaManagementSystemProperties.getProperty("basicAuthPwd");
		
		
		if(endpoint==null || endpoint.equals(""))
			throw new Exception("A valid endpoint must be specified. \""+endpoint+"\" has been provided");
		
		
		
		Client client= Client.create();
		
		
		client.addFilter(new HTTPBasicAuthFilter(user, password));//Basic Authentication
		
		WebResource webResource = client.resource(endpoint);
	
		if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
			System.out.println("Inviato: "+json.toString());
		}
		
		ClientResponse resp = webResource.accept("application/json").type("application/json").post(ClientResponse.class, json.toString());
		
		
		String risposta = resp.getEntity(String.class);
		if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
			
			System.out.println("Response ["+resp.getStatus()+"] "+risposta);
		}
		
		
		return risposta;
	}
	
	
	/**
	 * @param endpoint
	 * @return
	 * @throws Exception
	 */
	public static String consumeGetWsAuthJsonFormat (String endpoint) throws Exception{
			
		//Basic Authentication
		String user = IdeaManagementSystemProperties.getProperty("basicAuthUser");
		String password = IdeaManagementSystemProperties.getProperty("basicAuthPwd");
		
		
			if(endpoint==null || endpoint.equals(""))
				throw new Exception("A valid endpoint must be specified. \""+endpoint+"\" has been provided");
			
			Client client = Client.create();
			client.addFilter(new HTTPBasicAuthFilter(user, password));//Basic Authentication
			
			WebResource webResource = client.resource(endpoint);
			
			
			ClientResponse respGet = webResource.accept("application/json").get(ClientResponse.class);
			
			
			String risposta = respGet.getEntity(String.class);
			if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
				System.out.println("risposta: "+risposta);
			}
			
			return risposta;
		}
	
	
	
	
	/**
	 * @param endpoint
	 * @return
	 * @throws Exception
	 */
	public static String consumeGetWs (String endpoint) throws Exception{
			
			if(endpoint==null || endpoint.equals(""))
				throw new Exception("A valid endpoint must be specified. \""+endpoint+"\" has been provided");
			
			Client client = Client.create();
			WebResource webResource = client.resource(endpoint);
			
			ClientResponse respGet = webResource.get(ClientResponse.class);
			String risposta = respGet.getEntity(String.class);
			if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
				System.out.println("risposta: "+risposta);
			}
			
			return risposta;
		}
	
	
	//Formato JSON di Liferay
	public static String consumePutWs (JSONObject json, String endpoint) throws Exception{
				
				//Basic Authentication
				String user = IdeaManagementSystemProperties.getProperty("basicAuthUser");
				String password = IdeaManagementSystemProperties.getProperty("basicAuthPwd");
				
				
				if(endpoint==null || endpoint.equals(""))
					throw new Exception("A valid endpoint must be specified. \""+endpoint+"\" has been provided");
				
			
				Client client= Client.create();
				
				//Client client= Client.create(configureClient());
				if (!user.equals(""))
					client.addFilter(new HTTPBasicAuthFilter(user, password));//Basic Authentication
				
				
				WebResource webResource = client.resource(endpoint);
				if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
					System.out.println("Inviato: "+json.toString());
				}
				
				//ClientResponse resp =webResource.accept("text/plain").type("application/json").put(ClientResponse.class, json.toString());
				ClientResponse resp = webResource.accept("application/json;charset=UTF-8").type("application/json;charset=UTF-8").put(ClientResponse.class, json.toString());
				
				String risposta = resp.getEntity(String.class);
				if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
					System.out.println("Response ["+resp.getStatus()+"] "+risposta);
				}
				
				return risposta;
			}
	
	
	//Formato JSON di Liferay
	public static String consumeDeleteWs (String endpoint) throws Exception{
					
					//Basic Authentication
					String user = IdeaManagementSystemProperties.getProperty("basicAuthUser");
					String password = IdeaManagementSystemProperties.getProperty("basicAuthPwd");
					
					
					if(endpoint==null || endpoint.equals(""))
						throw new Exception("A valid endpoint must be specified. \""+endpoint+"\" has been provided");
					
				
					Client client= Client.create();
					
					//Client client= Client.create(configureClient());
					if (!user.equals(""))
						client.addFilter(new HTTPBasicAuthFilter(user, password));//Basic Authentication
					
					WebResource webResource = client.resource(endpoint);
					
					ClientResponse resp = webResource.delete(ClientResponse.class);
					
					String risposta = resp.getEntity(String.class);
					if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
						System.out.println("Response ["+resp.getStatus()+"] "+risposta);
					
					}
					return risposta;
				}
	
	
	
	/**
	 * @param url
	 * @param method
	 * @param data
	 * @return
	 */
	public static ClientResponse invokeRestAPI(String url, String method, String data) {
		
		ClientResponse clientResponse =null;
		
		boolean proxyEnabled = Boolean.parseBoolean(PortletProps.get("proxyEnabled"));
		
		System.out.println("proxyEnabled "+proxyEnabled);
		
		String proxyHost = "";
		String proxyPort = "";
		String proxyUser = "";
		String proxyPass = "";
		
		if(proxyEnabled){
			proxyHost = PortletProps.get("proxyHost");
			proxyPort = PortletProps.get("proxyPort");
			proxyUser = PortletProps.get("proxyUser");
			proxyPass = PortletProps.get("proxyPass");
			
			
			System.out.println("proxyHost "+proxyHost);
			System.out.println("proxyPort "+proxyPort);
			System.out.println("proxyUser "+proxyUser);
			System.out.println("proxyPass "+proxyPass);
		}
		
		try {
			if(proxyEnabled){
        		System.setProperty("http.proxyHost", proxyHost);  
        		System.setProperty("http.proxyPort", proxyPort);
        		Authenticator.setDefault(new ProxyAuthenticator(proxyUser, proxyPass));
        		
        		System.out.println("system.getProperty "+System.getProperty("http.proxyHost"));
        	}
			Client client = Client.create();
			WebResource webResource = client.resource(url);
			//WebResource.Builder wsb = webResource.type(MediaType.APPLICATION_JSON).accept(MediaType.WILDCARD);
			//ClientResponse clientResponse = webResource.accept(MediaType.WILDCARD).get(ClientResponse.class);
			
			switch(method.toLowerCase()) {
				case "post":	
					//clientResponse = wsb.post(ClientResponse.class, data);
					break;
				case "get":
					//clientResponse = wsb.get(ClientResponse.class);
					 clientResponse = webResource.accept(MediaType.WILDCARD).get(ClientResponse.class);
					break;
				default:
					System.out.println("Method "+method+" not allowed");
					break;
			}
		}
		catch(Exception e) {
			System.out.println("An error occurred while invoking the service "+url+" with data "+data+". Due to: "+e.getMessage());
			e.printStackTrace();
		}
//		finally {
//			if(proxyEnabled){
//        		System.setProperty("http.proxyHost", "");
//		       	System.setProperty("http.proxyPort", "");
//        	}
//		}
		
		return clientResponse;
	}
	
	

	
	public static ClientConfig configureClient()    {
		
        System.setProperty("jsse.enableSNIExtension", "false");
        
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] { 
          new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() { 
              return new X509Certificate[0]; 
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {}
            public void checkServerTrusted(X509Certificate[] certs, String authType) {}
        }};

     // Ignore differences between given hostname and certificate hostname
        HostnameVerifier hv = new HostnameVerifier() {
			
			@Override
			public boolean verify(String hostname, SSLSession session) {
				// TODO Auto-generated method stub
				System.out.println("veriify!");
				return true;
			}
		};

        // Install the all-trusting trust manager
		SSLContext sc=null;
        try {
          sc = SSLContext.getInstance("SSL");
          sc.init(null, trustAllCerts, new SecureRandom());
          HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
          HttpsURLConnection.setDefaultHostnameVerifier(hv);
          System.out.println("context");
          
        } catch (Exception ex) { System.out.println("context exception");}
      
	
     	
        ClientConfig config = new DefaultClientConfig();
        try
        {
            config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(
               hv,sc));
            System.out.println("config");
        }
        catch (Exception ex2)
        {System.out.println("config exception");
        }
        
        
        return config;
    }
}
