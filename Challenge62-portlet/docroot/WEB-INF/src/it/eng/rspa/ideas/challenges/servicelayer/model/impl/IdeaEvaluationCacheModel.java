/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing IdeaEvaluation in entity cache.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see IdeaEvaluation
 * @generated
 */
public class IdeaEvaluationCacheModel implements CacheModel<IdeaEvaluation>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{criteriaId=");
		sb.append(criteriaId);
		sb.append(", ideaId=");
		sb.append(ideaId);
		sb.append(", motivation=");
		sb.append(motivation);
		sb.append(", passed=");
		sb.append(passed);
		sb.append(", score=");
		sb.append(score);
		sb.append(", date=");
		sb.append(date);
		sb.append(", authorId=");
		sb.append(authorId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public IdeaEvaluation toEntityModel() {
		IdeaEvaluationImpl ideaEvaluationImpl = new IdeaEvaluationImpl();

		ideaEvaluationImpl.setCriteriaId(criteriaId);
		ideaEvaluationImpl.setIdeaId(ideaId);

		if (motivation == null) {
			ideaEvaluationImpl.setMotivation(StringPool.BLANK);
		}
		else {
			ideaEvaluationImpl.setMotivation(motivation);
		}

		ideaEvaluationImpl.setPassed(passed);
		ideaEvaluationImpl.setScore(score);

		if (date == Long.MIN_VALUE) {
			ideaEvaluationImpl.setDate(null);
		}
		else {
			ideaEvaluationImpl.setDate(new Date(date));
		}

		ideaEvaluationImpl.setAuthorId(authorId);

		ideaEvaluationImpl.resetOriginalValues();

		return ideaEvaluationImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		criteriaId = objectInput.readLong();
		ideaId = objectInput.readLong();
		motivation = objectInput.readUTF();
		passed = objectInput.readBoolean();
		score = objectInput.readDouble();
		date = objectInput.readLong();
		authorId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(criteriaId);
		objectOutput.writeLong(ideaId);

		if (motivation == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(motivation);
		}

		objectOutput.writeBoolean(passed);
		objectOutput.writeDouble(score);
		objectOutput.writeLong(date);
		objectOutput.writeLong(authorId);
	}

	public long criteriaId;
	public long ideaId;
	public String motivation;
	public boolean passed;
	public double score;
	public long date;
	public long authorId;
}