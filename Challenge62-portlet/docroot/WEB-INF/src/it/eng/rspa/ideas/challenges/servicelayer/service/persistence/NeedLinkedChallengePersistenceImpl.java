/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException;
import it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.NeedLinkedChallengeImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.NeedLinkedChallengeModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the need linked challenge service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see NeedLinkedChallengePersistence
 * @see NeedLinkedChallengeUtil
 * @generated
 */
public class NeedLinkedChallengePersistenceImpl extends BasePersistenceImpl<NeedLinkedChallenge>
	implements NeedLinkedChallengePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link NeedLinkedChallengeUtil} to access the need linked challenge persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = NeedLinkedChallengeImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(NeedLinkedChallengeModelImpl.ENTITY_CACHE_ENABLED,
			NeedLinkedChallengeModelImpl.FINDER_CACHE_ENABLED,
			NeedLinkedChallengeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(NeedLinkedChallengeModelImpl.ENTITY_CACHE_ENABLED,
			NeedLinkedChallengeModelImpl.FINDER_CACHE_ENABLED,
			NeedLinkedChallengeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(NeedLinkedChallengeModelImpl.ENTITY_CACHE_ENABLED,
			NeedLinkedChallengeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CHALLENGEBYNEED =
		new FinderPath(NeedLinkedChallengeModelImpl.ENTITY_CACHE_ENABLED,
			NeedLinkedChallengeModelImpl.FINDER_CACHE_ENABLED,
			NeedLinkedChallengeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBychallengeByNeed",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEBYNEED =
		new FinderPath(NeedLinkedChallengeModelImpl.ENTITY_CACHE_ENABLED,
			NeedLinkedChallengeModelImpl.FINDER_CACHE_ENABLED,
			NeedLinkedChallengeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBychallengeByNeed",
			new String[] { Long.class.getName() },
			NeedLinkedChallengeModelImpl.NEEDID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CHALLENGEBYNEED = new FinderPath(NeedLinkedChallengeModelImpl.ENTITY_CACHE_ENABLED,
			NeedLinkedChallengeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countBychallengeByNeed", new String[] { Long.class.getName() });

	/**
	 * Returns all the need linked challenges where needId = &#63;.
	 *
	 * @param needId the need ID
	 * @return the matching need linked challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<NeedLinkedChallenge> findBychallengeByNeed(long needId)
		throws SystemException {
		return findBychallengeByNeed(needId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the need linked challenges where needId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.NeedLinkedChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param needId the need ID
	 * @param start the lower bound of the range of need linked challenges
	 * @param end the upper bound of the range of need linked challenges (not inclusive)
	 * @return the range of matching need linked challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<NeedLinkedChallenge> findBychallengeByNeed(long needId,
		int start, int end) throws SystemException {
		return findBychallengeByNeed(needId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the need linked challenges where needId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.NeedLinkedChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param needId the need ID
	 * @param start the lower bound of the range of need linked challenges
	 * @param end the upper bound of the range of need linked challenges (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching need linked challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<NeedLinkedChallenge> findBychallengeByNeed(long needId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEBYNEED;
			finderArgs = new Object[] { needId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CHALLENGEBYNEED;
			finderArgs = new Object[] { needId, start, end, orderByComparator };
		}

		List<NeedLinkedChallenge> list = (List<NeedLinkedChallenge>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (NeedLinkedChallenge needLinkedChallenge : list) {
				if ((needId != needLinkedChallenge.getNeedId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_NEEDLINKEDCHALLENGE_WHERE);

			query.append(_FINDER_COLUMN_CHALLENGEBYNEED_NEEDID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(NeedLinkedChallengeModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(needId);

				if (!pagination) {
					list = (List<NeedLinkedChallenge>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<NeedLinkedChallenge>(list);
				}
				else {
					list = (List<NeedLinkedChallenge>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first need linked challenge in the ordered set where needId = &#63;.
	 *
	 * @param needId the need ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching need linked challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException if a matching need linked challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public NeedLinkedChallenge findBychallengeByNeed_First(long needId,
		OrderByComparator orderByComparator)
		throws NoSuchNeedLinkedChallengeException, SystemException {
		NeedLinkedChallenge needLinkedChallenge = fetchBychallengeByNeed_First(needId,
				orderByComparator);

		if (needLinkedChallenge != null) {
			return needLinkedChallenge;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("needId=");
		msg.append(needId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchNeedLinkedChallengeException(msg.toString());
	}

	/**
	 * Returns the first need linked challenge in the ordered set where needId = &#63;.
	 *
	 * @param needId the need ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching need linked challenge, or <code>null</code> if a matching need linked challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public NeedLinkedChallenge fetchBychallengeByNeed_First(long needId,
		OrderByComparator orderByComparator) throws SystemException {
		List<NeedLinkedChallenge> list = findBychallengeByNeed(needId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last need linked challenge in the ordered set where needId = &#63;.
	 *
	 * @param needId the need ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching need linked challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException if a matching need linked challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public NeedLinkedChallenge findBychallengeByNeed_Last(long needId,
		OrderByComparator orderByComparator)
		throws NoSuchNeedLinkedChallengeException, SystemException {
		NeedLinkedChallenge needLinkedChallenge = fetchBychallengeByNeed_Last(needId,
				orderByComparator);

		if (needLinkedChallenge != null) {
			return needLinkedChallenge;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("needId=");
		msg.append(needId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchNeedLinkedChallengeException(msg.toString());
	}

	/**
	 * Returns the last need linked challenge in the ordered set where needId = &#63;.
	 *
	 * @param needId the need ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching need linked challenge, or <code>null</code> if a matching need linked challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public NeedLinkedChallenge fetchBychallengeByNeed_Last(long needId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBychallengeByNeed(needId);

		if (count == 0) {
			return null;
		}

		List<NeedLinkedChallenge> list = findBychallengeByNeed(needId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the need linked challenges before and after the current need linked challenge in the ordered set where needId = &#63;.
	 *
	 * @param needLinkedChallengePK the primary key of the current need linked challenge
	 * @param needId the need ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next need linked challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException if a need linked challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public NeedLinkedChallenge[] findBychallengeByNeed_PrevAndNext(
		NeedLinkedChallengePK needLinkedChallengePK, long needId,
		OrderByComparator orderByComparator)
		throws NoSuchNeedLinkedChallengeException, SystemException {
		NeedLinkedChallenge needLinkedChallenge = findByPrimaryKey(needLinkedChallengePK);

		Session session = null;

		try {
			session = openSession();

			NeedLinkedChallenge[] array = new NeedLinkedChallengeImpl[3];

			array[0] = getBychallengeByNeed_PrevAndNext(session,
					needLinkedChallenge, needId, orderByComparator, true);

			array[1] = needLinkedChallenge;

			array[2] = getBychallengeByNeed_PrevAndNext(session,
					needLinkedChallenge, needId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected NeedLinkedChallenge getBychallengeByNeed_PrevAndNext(
		Session session, NeedLinkedChallenge needLinkedChallenge, long needId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_NEEDLINKEDCHALLENGE_WHERE);

		query.append(_FINDER_COLUMN_CHALLENGEBYNEED_NEEDID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(NeedLinkedChallengeModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(needId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(needLinkedChallenge);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<NeedLinkedChallenge> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the need linked challenges where needId = &#63; from the database.
	 *
	 * @param needId the need ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBychallengeByNeed(long needId) throws SystemException {
		for (NeedLinkedChallenge needLinkedChallenge : findBychallengeByNeed(
				needId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(needLinkedChallenge);
		}
	}

	/**
	 * Returns the number of need linked challenges where needId = &#63;.
	 *
	 * @param needId the need ID
	 * @return the number of matching need linked challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBychallengeByNeed(long needId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CHALLENGEBYNEED;

		Object[] finderArgs = new Object[] { needId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_NEEDLINKEDCHALLENGE_WHERE);

			query.append(_FINDER_COLUMN_CHALLENGEBYNEED_NEEDID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(needId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CHALLENGEBYNEED_NEEDID_2 = "needLinkedChallenge.id.needId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_NEEDBYCHALLENGE =
		new FinderPath(NeedLinkedChallengeModelImpl.ENTITY_CACHE_ENABLED,
			NeedLinkedChallengeModelImpl.FINDER_CACHE_ENABLED,
			NeedLinkedChallengeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByneedByChallenge",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NEEDBYCHALLENGE =
		new FinderPath(NeedLinkedChallengeModelImpl.ENTITY_CACHE_ENABLED,
			NeedLinkedChallengeModelImpl.FINDER_CACHE_ENABLED,
			NeedLinkedChallengeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByneedByChallenge",
			new String[] { Long.class.getName() },
			NeedLinkedChallengeModelImpl.CHALLENGEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_NEEDBYCHALLENGE = new FinderPath(NeedLinkedChallengeModelImpl.ENTITY_CACHE_ENABLED,
			NeedLinkedChallengeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByneedByChallenge", new String[] { Long.class.getName() });

	/**
	 * Returns all the need linked challenges where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @return the matching need linked challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<NeedLinkedChallenge> findByneedByChallenge(long challengeId)
		throws SystemException {
		return findByneedByChallenge(challengeId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the need linked challenges where challengeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.NeedLinkedChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param start the lower bound of the range of need linked challenges
	 * @param end the upper bound of the range of need linked challenges (not inclusive)
	 * @return the range of matching need linked challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<NeedLinkedChallenge> findByneedByChallenge(long challengeId,
		int start, int end) throws SystemException {
		return findByneedByChallenge(challengeId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the need linked challenges where challengeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.NeedLinkedChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param start the lower bound of the range of need linked challenges
	 * @param end the upper bound of the range of need linked challenges (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching need linked challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<NeedLinkedChallenge> findByneedByChallenge(long challengeId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NEEDBYCHALLENGE;
			finderArgs = new Object[] { challengeId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_NEEDBYCHALLENGE;
			finderArgs = new Object[] { challengeId, start, end, orderByComparator };
		}

		List<NeedLinkedChallenge> list = (List<NeedLinkedChallenge>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (NeedLinkedChallenge needLinkedChallenge : list) {
				if ((challengeId != needLinkedChallenge.getChallengeId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_NEEDLINKEDCHALLENGE_WHERE);

			query.append(_FINDER_COLUMN_NEEDBYCHALLENGE_CHALLENGEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(NeedLinkedChallengeModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				if (!pagination) {
					list = (List<NeedLinkedChallenge>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<NeedLinkedChallenge>(list);
				}
				else {
					list = (List<NeedLinkedChallenge>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first need linked challenge in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching need linked challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException if a matching need linked challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public NeedLinkedChallenge findByneedByChallenge_First(long challengeId,
		OrderByComparator orderByComparator)
		throws NoSuchNeedLinkedChallengeException, SystemException {
		NeedLinkedChallenge needLinkedChallenge = fetchByneedByChallenge_First(challengeId,
				orderByComparator);

		if (needLinkedChallenge != null) {
			return needLinkedChallenge;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchNeedLinkedChallengeException(msg.toString());
	}

	/**
	 * Returns the first need linked challenge in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching need linked challenge, or <code>null</code> if a matching need linked challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public NeedLinkedChallenge fetchByneedByChallenge_First(long challengeId,
		OrderByComparator orderByComparator) throws SystemException {
		List<NeedLinkedChallenge> list = findByneedByChallenge(challengeId, 0,
				1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last need linked challenge in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching need linked challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException if a matching need linked challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public NeedLinkedChallenge findByneedByChallenge_Last(long challengeId,
		OrderByComparator orderByComparator)
		throws NoSuchNeedLinkedChallengeException, SystemException {
		NeedLinkedChallenge needLinkedChallenge = fetchByneedByChallenge_Last(challengeId,
				orderByComparator);

		if (needLinkedChallenge != null) {
			return needLinkedChallenge;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchNeedLinkedChallengeException(msg.toString());
	}

	/**
	 * Returns the last need linked challenge in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching need linked challenge, or <code>null</code> if a matching need linked challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public NeedLinkedChallenge fetchByneedByChallenge_Last(long challengeId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByneedByChallenge(challengeId);

		if (count == 0) {
			return null;
		}

		List<NeedLinkedChallenge> list = findByneedByChallenge(challengeId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the need linked challenges before and after the current need linked challenge in the ordered set where challengeId = &#63;.
	 *
	 * @param needLinkedChallengePK the primary key of the current need linked challenge
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next need linked challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException if a need linked challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public NeedLinkedChallenge[] findByneedByChallenge_PrevAndNext(
		NeedLinkedChallengePK needLinkedChallengePK, long challengeId,
		OrderByComparator orderByComparator)
		throws NoSuchNeedLinkedChallengeException, SystemException {
		NeedLinkedChallenge needLinkedChallenge = findByPrimaryKey(needLinkedChallengePK);

		Session session = null;

		try {
			session = openSession();

			NeedLinkedChallenge[] array = new NeedLinkedChallengeImpl[3];

			array[0] = getByneedByChallenge_PrevAndNext(session,
					needLinkedChallenge, challengeId, orderByComparator, true);

			array[1] = needLinkedChallenge;

			array[2] = getByneedByChallenge_PrevAndNext(session,
					needLinkedChallenge, challengeId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected NeedLinkedChallenge getByneedByChallenge_PrevAndNext(
		Session session, NeedLinkedChallenge needLinkedChallenge,
		long challengeId, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_NEEDLINKEDCHALLENGE_WHERE);

		query.append(_FINDER_COLUMN_NEEDBYCHALLENGE_CHALLENGEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(NeedLinkedChallengeModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(challengeId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(needLinkedChallenge);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<NeedLinkedChallenge> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the need linked challenges where challengeId = &#63; from the database.
	 *
	 * @param challengeId the challenge ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByneedByChallenge(long challengeId)
		throws SystemException {
		for (NeedLinkedChallenge needLinkedChallenge : findByneedByChallenge(
				challengeId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(needLinkedChallenge);
		}
	}

	/**
	 * Returns the number of need linked challenges where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @return the number of matching need linked challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByneedByChallenge(long challengeId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_NEEDBYCHALLENGE;

		Object[] finderArgs = new Object[] { challengeId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_NEEDLINKEDCHALLENGE_WHERE);

			query.append(_FINDER_COLUMN_NEEDBYCHALLENGE_CHALLENGEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_NEEDBYCHALLENGE_CHALLENGEID_2 = "needLinkedChallenge.id.challengeId = ?";

	public NeedLinkedChallengePersistenceImpl() {
		setModelClass(NeedLinkedChallenge.class);
	}

	/**
	 * Caches the need linked challenge in the entity cache if it is enabled.
	 *
	 * @param needLinkedChallenge the need linked challenge
	 */
	@Override
	public void cacheResult(NeedLinkedChallenge needLinkedChallenge) {
		EntityCacheUtil.putResult(NeedLinkedChallengeModelImpl.ENTITY_CACHE_ENABLED,
			NeedLinkedChallengeImpl.class, needLinkedChallenge.getPrimaryKey(),
			needLinkedChallenge);

		needLinkedChallenge.resetOriginalValues();
	}

	/**
	 * Caches the need linked challenges in the entity cache if it is enabled.
	 *
	 * @param needLinkedChallenges the need linked challenges
	 */
	@Override
	public void cacheResult(List<NeedLinkedChallenge> needLinkedChallenges) {
		for (NeedLinkedChallenge needLinkedChallenge : needLinkedChallenges) {
			if (EntityCacheUtil.getResult(
						NeedLinkedChallengeModelImpl.ENTITY_CACHE_ENABLED,
						NeedLinkedChallengeImpl.class,
						needLinkedChallenge.getPrimaryKey()) == null) {
				cacheResult(needLinkedChallenge);
			}
			else {
				needLinkedChallenge.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all need linked challenges.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(NeedLinkedChallengeImpl.class.getName());
		}

		EntityCacheUtil.clearCache(NeedLinkedChallengeImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the need linked challenge.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(NeedLinkedChallenge needLinkedChallenge) {
		EntityCacheUtil.removeResult(NeedLinkedChallengeModelImpl.ENTITY_CACHE_ENABLED,
			NeedLinkedChallengeImpl.class, needLinkedChallenge.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<NeedLinkedChallenge> needLinkedChallenges) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (NeedLinkedChallenge needLinkedChallenge : needLinkedChallenges) {
			EntityCacheUtil.removeResult(NeedLinkedChallengeModelImpl.ENTITY_CACHE_ENABLED,
				NeedLinkedChallengeImpl.class,
				needLinkedChallenge.getPrimaryKey());
		}
	}

	/**
	 * Creates a new need linked challenge with the primary key. Does not add the need linked challenge to the database.
	 *
	 * @param needLinkedChallengePK the primary key for the new need linked challenge
	 * @return the new need linked challenge
	 */
	@Override
	public NeedLinkedChallenge create(
		NeedLinkedChallengePK needLinkedChallengePK) {
		NeedLinkedChallenge needLinkedChallenge = new NeedLinkedChallengeImpl();

		needLinkedChallenge.setNew(true);
		needLinkedChallenge.setPrimaryKey(needLinkedChallengePK);

		return needLinkedChallenge;
	}

	/**
	 * Removes the need linked challenge with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param needLinkedChallengePK the primary key of the need linked challenge
	 * @return the need linked challenge that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException if a need linked challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public NeedLinkedChallenge remove(
		NeedLinkedChallengePK needLinkedChallengePK)
		throws NoSuchNeedLinkedChallengeException, SystemException {
		return remove((Serializable)needLinkedChallengePK);
	}

	/**
	 * Removes the need linked challenge with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the need linked challenge
	 * @return the need linked challenge that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException if a need linked challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public NeedLinkedChallenge remove(Serializable primaryKey)
		throws NoSuchNeedLinkedChallengeException, SystemException {
		Session session = null;

		try {
			session = openSession();

			NeedLinkedChallenge needLinkedChallenge = (NeedLinkedChallenge)session.get(NeedLinkedChallengeImpl.class,
					primaryKey);

			if (needLinkedChallenge == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchNeedLinkedChallengeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(needLinkedChallenge);
		}
		catch (NoSuchNeedLinkedChallengeException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected NeedLinkedChallenge removeImpl(
		NeedLinkedChallenge needLinkedChallenge) throws SystemException {
		needLinkedChallenge = toUnwrappedModel(needLinkedChallenge);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(needLinkedChallenge)) {
				needLinkedChallenge = (NeedLinkedChallenge)session.get(NeedLinkedChallengeImpl.class,
						needLinkedChallenge.getPrimaryKeyObj());
			}

			if (needLinkedChallenge != null) {
				session.delete(needLinkedChallenge);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (needLinkedChallenge != null) {
			clearCache(needLinkedChallenge);
		}

		return needLinkedChallenge;
	}

	@Override
	public NeedLinkedChallenge updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge needLinkedChallenge)
		throws SystemException {
		needLinkedChallenge = toUnwrappedModel(needLinkedChallenge);

		boolean isNew = needLinkedChallenge.isNew();

		NeedLinkedChallengeModelImpl needLinkedChallengeModelImpl = (NeedLinkedChallengeModelImpl)needLinkedChallenge;

		Session session = null;

		try {
			session = openSession();

			if (needLinkedChallenge.isNew()) {
				session.save(needLinkedChallenge);

				needLinkedChallenge.setNew(false);
			}
			else {
				session.merge(needLinkedChallenge);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !NeedLinkedChallengeModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((needLinkedChallengeModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEBYNEED.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						needLinkedChallengeModelImpl.getOriginalNeedId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CHALLENGEBYNEED,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEBYNEED,
					args);

				args = new Object[] { needLinkedChallengeModelImpl.getNeedId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CHALLENGEBYNEED,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEBYNEED,
					args);
			}

			if ((needLinkedChallengeModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NEEDBYCHALLENGE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						needLinkedChallengeModelImpl.getOriginalChallengeId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NEEDBYCHALLENGE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NEEDBYCHALLENGE,
					args);

				args = new Object[] {
						needLinkedChallengeModelImpl.getChallengeId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NEEDBYCHALLENGE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NEEDBYCHALLENGE,
					args);
			}
		}

		EntityCacheUtil.putResult(NeedLinkedChallengeModelImpl.ENTITY_CACHE_ENABLED,
			NeedLinkedChallengeImpl.class, needLinkedChallenge.getPrimaryKey(),
			needLinkedChallenge);

		return needLinkedChallenge;
	}

	protected NeedLinkedChallenge toUnwrappedModel(
		NeedLinkedChallenge needLinkedChallenge) {
		if (needLinkedChallenge instanceof NeedLinkedChallengeImpl) {
			return needLinkedChallenge;
		}

		NeedLinkedChallengeImpl needLinkedChallengeImpl = new NeedLinkedChallengeImpl();

		needLinkedChallengeImpl.setNew(needLinkedChallenge.isNew());
		needLinkedChallengeImpl.setPrimaryKey(needLinkedChallenge.getPrimaryKey());

		needLinkedChallengeImpl.setNeedId(needLinkedChallenge.getNeedId());
		needLinkedChallengeImpl.setChallengeId(needLinkedChallenge.getChallengeId());
		needLinkedChallengeImpl.setDate(needLinkedChallenge.getDate());

		return needLinkedChallengeImpl;
	}

	/**
	 * Returns the need linked challenge with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the need linked challenge
	 * @return the need linked challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException if a need linked challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public NeedLinkedChallenge findByPrimaryKey(Serializable primaryKey)
		throws NoSuchNeedLinkedChallengeException, SystemException {
		NeedLinkedChallenge needLinkedChallenge = fetchByPrimaryKey(primaryKey);

		if (needLinkedChallenge == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchNeedLinkedChallengeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return needLinkedChallenge;
	}

	/**
	 * Returns the need linked challenge with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException} if it could not be found.
	 *
	 * @param needLinkedChallengePK the primary key of the need linked challenge
	 * @return the need linked challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException if a need linked challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public NeedLinkedChallenge findByPrimaryKey(
		NeedLinkedChallengePK needLinkedChallengePK)
		throws NoSuchNeedLinkedChallengeException, SystemException {
		return findByPrimaryKey((Serializable)needLinkedChallengePK);
	}

	/**
	 * Returns the need linked challenge with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the need linked challenge
	 * @return the need linked challenge, or <code>null</code> if a need linked challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public NeedLinkedChallenge fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		NeedLinkedChallenge needLinkedChallenge = (NeedLinkedChallenge)EntityCacheUtil.getResult(NeedLinkedChallengeModelImpl.ENTITY_CACHE_ENABLED,
				NeedLinkedChallengeImpl.class, primaryKey);

		if (needLinkedChallenge == _nullNeedLinkedChallenge) {
			return null;
		}

		if (needLinkedChallenge == null) {
			Session session = null;

			try {
				session = openSession();

				needLinkedChallenge = (NeedLinkedChallenge)session.get(NeedLinkedChallengeImpl.class,
						primaryKey);

				if (needLinkedChallenge != null) {
					cacheResult(needLinkedChallenge);
				}
				else {
					EntityCacheUtil.putResult(NeedLinkedChallengeModelImpl.ENTITY_CACHE_ENABLED,
						NeedLinkedChallengeImpl.class, primaryKey,
						_nullNeedLinkedChallenge);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(NeedLinkedChallengeModelImpl.ENTITY_CACHE_ENABLED,
					NeedLinkedChallengeImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return needLinkedChallenge;
	}

	/**
	 * Returns the need linked challenge with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param needLinkedChallengePK the primary key of the need linked challenge
	 * @return the need linked challenge, or <code>null</code> if a need linked challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public NeedLinkedChallenge fetchByPrimaryKey(
		NeedLinkedChallengePK needLinkedChallengePK) throws SystemException {
		return fetchByPrimaryKey((Serializable)needLinkedChallengePK);
	}

	/**
	 * Returns all the need linked challenges.
	 *
	 * @return the need linked challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<NeedLinkedChallenge> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the need linked challenges.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.NeedLinkedChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of need linked challenges
	 * @param end the upper bound of the range of need linked challenges (not inclusive)
	 * @return the range of need linked challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<NeedLinkedChallenge> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the need linked challenges.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.NeedLinkedChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of need linked challenges
	 * @param end the upper bound of the range of need linked challenges (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of need linked challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<NeedLinkedChallenge> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<NeedLinkedChallenge> list = (List<NeedLinkedChallenge>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_NEEDLINKEDCHALLENGE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_NEEDLINKEDCHALLENGE;

				if (pagination) {
					sql = sql.concat(NeedLinkedChallengeModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<NeedLinkedChallenge>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<NeedLinkedChallenge>(list);
				}
				else {
					list = (List<NeedLinkedChallenge>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the need linked challenges from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (NeedLinkedChallenge needLinkedChallenge : findAll()) {
			remove(needLinkedChallenge);
		}
	}

	/**
	 * Returns the number of need linked challenges.
	 *
	 * @return the number of need linked challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_NEEDLINKEDCHALLENGE);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the need linked challenge persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<NeedLinkedChallenge>> listenersList = new ArrayList<ModelListener<NeedLinkedChallenge>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<NeedLinkedChallenge>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(NeedLinkedChallengeImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_NEEDLINKEDCHALLENGE = "SELECT needLinkedChallenge FROM NeedLinkedChallenge needLinkedChallenge";
	private static final String _SQL_SELECT_NEEDLINKEDCHALLENGE_WHERE = "SELECT needLinkedChallenge FROM NeedLinkedChallenge needLinkedChallenge WHERE ";
	private static final String _SQL_COUNT_NEEDLINKEDCHALLENGE = "SELECT COUNT(needLinkedChallenge) FROM NeedLinkedChallenge needLinkedChallenge";
	private static final String _SQL_COUNT_NEEDLINKEDCHALLENGE_WHERE = "SELECT COUNT(needLinkedChallenge) FROM NeedLinkedChallenge needLinkedChallenge WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "needLinkedChallenge.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No NeedLinkedChallenge exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No NeedLinkedChallenge exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(NeedLinkedChallengePersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"date"
			});
	private static NeedLinkedChallenge _nullNeedLinkedChallenge = new NeedLinkedChallengeImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<NeedLinkedChallenge> toCacheModel() {
				return _nullNeedLinkedChallengeCacheModel;
			}
		};

	private static CacheModel<NeedLinkedChallenge> _nullNeedLinkedChallengeCacheModel =
		new CacheModel<NeedLinkedChallenge>() {
			@Override
			public NeedLinkedChallenge toEntityModel() {
				return _nullNeedLinkedChallenge;
			}
		};
}