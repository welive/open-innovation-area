package it.eng.rspa.ideas.utils;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCoworkerImpl;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCoworkerLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import javax.portlet.ActionRequest;
import javax.portlet.RenderRequest;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserGroup;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.UserGroupLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;



public class UpdateIdeaUtils {
	
		
	
	/**
	 * @param ideaID
	 * @return
	 */
	public static boolean isIdeaWithDeveloper(long ideaID){
		
		CLSIdea	 idea = null;
		try {
			 idea = CLSIdeaLocalServiceUtil.getCLSIdea(ideaID);
			 
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return false;
		}
		
		//controllo innanzitutto se l'autore e' un developer
		boolean autoreDeveloper = isDeveloperByUserId(idea.getUserId());
		
		
		if (autoreDeveloper){
			return autoreDeveloper;
		}else{//se non lo e' l'autore allora controllo i collaboratori
			
			List<CLSCoworker> coworkersList = new ArrayList<CLSCoworker>();
			try {
				coworkersList  =	CLSCoworkerLocalServiceUtil.getCoworkersByIdeaId(ideaID);
			} catch (SystemException e) {
				
				e.printStackTrace();
				return false;
			}
			
			boolean coworkerDeveloper = false;
			for (int i =0; i<coworkersList.size(); i++){
				CLSCoworker cw = coworkersList.get(i);
				
				coworkerDeveloper = isDeveloperByUserId(cw.getUserId());
				if (coworkerDeveloper)
					return coworkerDeveloper;
				
			}			
		}
		
		
		return false;
	}
	
	
	/**
	 * @param userId
	 * @return
	 */
	public static boolean isDeveloperByUserId (long userId){
		
		
		//controllo innanzitutto se l'autore e' un developer
		User autore = null;
				
		
			try {
				autore = UserLocalServiceUtil.getUserById(userId);
			} catch (PortalException | SystemException e) {
				
				e.printStackTrace();
				return false;
			}
		
		
		boolean autoreDeveloper = MyUtils.isDeveloper(autore);
		
		
		return autoreDeveloper;
	}
	
	
	/**
	 * @return
	 */
	public static List<User> getAllMunicipalities(){
		
		long roleIdEnte = MyUtils.getRoleIdbyRoleName(MyConstants.ROLE_AUTHORITY);
		
		List<User> municipalities = new ArrayList<User>();
		
		try {
			municipalities = UserLocalServiceUtil.getRoleUsers(roleIdEnte);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return municipalities;
			
		}
		return municipalities;
		
	}
	
	/**
	 * @return
	 */
	public static List<Organization> getAllOrganizationMunicipalities(){
		
				
		List<Organization> municipalities = new ArrayList<Organization>();
		
		try {
			List<Organization> municipalitiesAll = OrganizationLocalServiceUtil.getOrganizations(0, OrganizationLocalServiceUtil.getOrganizationsCount());
			
			for (Organization org:municipalitiesAll){
			
				if (org.getExpandoBridge().getAttribute(MyConstants.CUSTOMFIELD_ISAUTHORITY).toString().equalsIgnoreCase("true"))
					municipalities.add(org);
			
			}
			
			
		} catch (SystemException e) {
			
			e.printStackTrace();
		}
		return municipalities;
		
	}

	
	/**
	 * Some idea fields aren't editable after the elaboration state
	 * 
	 * @param idea
	 * @return
	 */
	public static boolean areIdeaFieldsEditable (CLSIdea idea){
		
		if (idea.getIsNeed()){
			
			boolean isAssignedNeed = NeedUtils.isNeedAssigned(idea.getIdeaID());
			
			if (isAssignedNeed)
				return false;
		}
		
		String stato = idea.getIdeaStatus();
		
		
		
		if (stato.equalsIgnoreCase(MyConstants.IDEA_STATE_ELABORATION) )
			return true;
		
		
		return false;
	}
	
	
	/**
	 * In some state the idea is NOT editable
	 * 
	 * @param idea
	 * @return
	 */
	public static boolean isIdeaEditable (CLSIdea idea){
		
		
		String stato = idea.getIdeaStatus();
		
		//I exclude these states
		if (stato.equalsIgnoreCase(MyConstants.IDEA_STATE_EVALUATION) || 
			stato.equalsIgnoreCase(MyConstants.IDEA_STATE_SELECTED) || 
			stato.equalsIgnoreCase(MyConstants.IDEA_STATE_MONITORING)||
			stato.equalsIgnoreCase(MyConstants.IDEA_STATE_REJECT)
		)
			return false;
		
		
		return true;
	}
	
	
	
	/**
	 * The idea description is editable only in some states
	 * 
	 * @param idea
	 * @return
	 */
	public static boolean isDescriptionIdeaEditable (CLSIdea idea){
		
		
		if (idea.getIsNeed()){
			
			boolean isAssignedNeed = NeedUtils.isNeedAssigned(idea.getIdeaID());
			
			if (isAssignedNeed)
				return false;
		}
			
		
		String stato = idea.getIdeaStatus();
		
		if (stato.equalsIgnoreCase(MyConstants.IDEA_STATE_ELABORATION) ||  stato.equalsIgnoreCase(MyConstants.IDEA_STATE_REFINEMENT) )
			return true;
		
		
		return false;
	}
	
	

	/**
	 * Check if the user has open challenge, so he/she can create ideas.
	 * 
	 * @param user
	 * @return
	 */
	public static boolean areThereOpenChallengeByUser(RenderRequest renderRequest){
		
		String pilota = IdeasListUtils.getPilot(renderRequest);
		
		List<CLSChallenge> challengesI = new ArrayList<CLSChallenge>();
		try {
			challengesI = CLSChallengeLocalServiceUtil.getActiveChallenges();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		List<CLSChallenge> challenges = ChallengesUtils.getListaGareFiltrataPerPilot(challengesI, pilota);//filtro
		
		if (challenges.size() > 0)
			return true;
		
		
		return false;
	}
	
	
	
	
	/**
	 * @param idea
	 * @param isAuthor
	 * @param isNew
	 * @param coworkersUserIdList
	 * @param coworkersListOld
	 * @param actionRequest
	 * @param res
	 * @throws SystemException
	 */
	public static void manageCoworkers(CLSIdea idea, boolean isAuthor, boolean isNew, ArrayList<Long> coworkersUserIdList, List<CLSCoworker> coworkersListOld,
			ActionRequest actionRequest, ResourceBundle res, User user) throws SystemException{
		
		boolean isTakenUp = idea.getTakenUp();
		boolean isEntityReferencebyIdea = IdeasListUtils.isEntityReferencebyIdea(idea, user);
		
		
		// update coworker if the logged user is the author
		if (isAuthor || isNew || (isTakenUp && isEntityReferencebyIdea)) {
			// I eliminate even old as being part of the group for the chat
			UserGroup chatGroup = UserGroupLocalServiceUtil.fetchUserGroup(idea.getChatGroup());
							

			for (CLSCoworker coworkerOld:coworkersListOld){	
				
				//retrieve the user and remove from chatGroup
				UserGroupLocalServiceUtil.deleteUserUserGroup(coworkerOld.getUserId(), chatGroup.getUserGroupId());

				try {
					CLSCoworkerLocalServiceUtil.deleteCLSCoworker(coworkerOld.getCoworkerId());
				} catch (PortalException e) {
					
					e.printStackTrace();
				}
			}

			// I add also new as part of the group for the chat
			Iterator<Long> coworkersUserIdListIt = coworkersUserIdList.iterator();
			
			while (coworkersUserIdListIt.hasNext()) {
				long coworkerUserId = coworkersUserIdListIt.next();
				long coworkerId = CounterLocalServiceUtil.increment(CLSCoworker.class.getName());
				CLSCoworker coworker = new CLSCoworkerImpl();
				coworker.setCoworkerId(coworkerId);
				coworker.setIdeaID(idea.getIdeaID());
				coworker.setUserId(coworkerUserId);
				CLSCoworkerLocalServiceUtil.addCLSCoworker(coworker);
				
				//retrieve the user and add to chatGroup
				UserGroupLocalServiceUtil.addUserUserGroup(coworker.getUserId(), chatGroup.getUserGroupId());
				
				String senderName = user.getFullName();
										
				
				if (!isNew) {//if is not a new idea, I see whether it is an old co-worker
				
					boolean coworkerOld = false;
					for (int y=0; y<coworkersListOld.size(); y++){
					
						
						if (coworkersListOld.get(y).getUserId() ==  coworkerUserId )
							coworkerOld = true;							
					}//for
						
					if (isNew || !coworkerOld) {	
					
						String collaboratorAddedd = "collaborator added";
						try {
							collaboratorAddedd = new String(res.getString("ims.collaborator-added") .getBytes("ISO-8859-1"), "UTF-8");
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
						}
						
						//send notification
						String textMessage = senderName+" "+ collaboratorAddedd+" "+ idea.getIdeaTitle();
						try {
							CLSIdeaLocalServiceUtil.sendNotification(res,textMessage, coworkerUserId, senderName, idea.getIdeaID(), actionRequest, "aggiuntaCollaboratore");
						} catch (PortalException e) {
							e.printStackTrace();
						}
						
						//sent a notication to Author of the idea (the action is done  by the CompanyLeader)
						if (isTakenUp && isEntityReferencebyIdea){
							
							String hasAdded = "has added";
							String toTheCollaboratorsOfTheIdea = "to the collaborators of the idea";
							try {
								hasAdded = new String(res.getString("ims.has-added") .getBytes("ISO-8859-1"), "UTF-8");
								toTheCollaboratorsOfTheIdea = new String(res.getString("ims.to-the-collaborators-of-the-idea") .getBytes("ISO-8859-1"), "UTF-8");
							} catch (UnsupportedEncodingException e) {
								e.printStackTrace();
							}
							
							String fullname = MyUtils.getFulltNameByUserId(coworkerUserId);
							
							//send notification
							String textMessage2 = senderName+" "+ hasAdded+" "+ fullname+" "+toTheCollaboratorsOfTheIdea+" "+idea.getIdeaTitle();
							
							try {
								CLSIdeaLocalServiceUtil.sendNotification(res,textMessage2, idea.getUserId(), senderName, idea.getIdeaID(), actionRequest, "aggiuntaCollaboratore");
							} catch (PortalException e) {
								e.printStackTrace();
							}
						}
						
						IdeasListUtils.subscribeUserToIdeaComment(idea, coworkerUserId);
						
					}//new Idea o new cowoker
															
				}//if !isNew
				
			}//while (coworkersUserIdListIt.hasNext()) 
							
		}//if (isAuthor || isNew) {
		
		
	}
	
	
	/**
	 * Notify to all coworkers
	 * 
	 * @param coworkersUserIdList
	 * @param idea
	 * @param actionRequest
	 * @param res
	 */
	public static  void notifyCoworkers (ArrayList<Long> coworkersUserIdList, CLSIdea idea, ActionRequest actionRequest, ResourceBundle res, User user){

		
		
		for(int i=0; i<coworkersUserIdList.size();i++){
			
			
			String collaboratorAdded="collaborator added";
			try {
				collaboratorAdded = new String(res.getString("ims.collaborator-added") .getBytes("ISO-8859-1"), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				
				e.printStackTrace();
			}
			
			
			String textMessage = user.getFullName()  +" "+ collaboratorAdded +" "+ idea.getIdeaTitle();
			long selectedUserId = coworkersUserIdList.get(i);
								
			String senderName = user.getFullName();
		
			try {
				CLSIdeaLocalServiceUtil.sendNotification(res,textMessage, selectedUserId, senderName, idea.getIdeaID(), actionRequest,  "aggiuntaCollaboratore");
			} catch (PortalException | SystemException e) {
				
				e.printStackTrace();
			}
		}

	}


	/**
	 * @param coworkersJsonString
	 * @return
	 */
	public static ArrayList<Long>  coworkerStringToList (String coworkersJsonString ){

		ArrayList<Long> coworkersUserIdList = new ArrayList<Long>();
		
		JSONObject coworkersJson = JSONFactoryUtil.createJSONObject();
		if(coworkersJsonString!=null && !coworkersJsonString.equals("")){
			try{ coworkersJson = JSONFactoryUtil.createJSONObject(coworkersJsonString); }
			catch(Exception e){
				e.printStackTrace();
				
			}
		}
			
		Iterator<String> jsonKeys = coworkersJson.keys();
		while(jsonKeys.hasNext()){
			String sUid = jsonKeys.next();
			JSONObject coworkerMeta = coworkersJson.getJSONObject(sUid);
			
			if(coworkerMeta!=null && coworkerMeta.has("selected") && coworkerMeta.getBoolean("selected") && coworkerMeta.has("userid")){
				coworkersUserIdList.add(coworkerMeta.getLong("userid"));
			}
			
	}
	 return coworkersUserIdList;
	}


/**
 * Visual Composer notifications for group
 * 
 * @param idea
 * @param idea
 * @param coworkersUserIdList
 */
public static void notifyVC (CLSIdea idea, ArrayList<Long> coworkersUserIdList){
	
	if (IdeaManagementSystemProperties.getEnabledProperty("vcEnabled")
			&& (idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_IMPLEMENTATION) || 
					idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_MONITORING)) //da raffinamento a impl lo fa in completeTask 
			&& UpdateIdeaUtils.isIdeaWithDeveloper(idea.getIdeaID()) ){
	
		//notifico info sull'dea al VME in modo da generare il gruppo di utenti anche da quel lato
		Vc.vcNotification(idea, coworkersUserIdList);
	}
}


/**
 * CDV notifications 
 * 
 * @param idea
 * @param coworkersUserIdList
 * @param user
 * @param isNew
 * @param coworkersListOld
 */
public static void notifyCDV (CLSIdea idea, ArrayList<Long> coworkersUserIdList, User user, boolean isNew, List<CLSCoworker> coworkersListOld){
	
	if (IdeaManagementSystemProperties.getEnabledProperty("cdvEnabled") && !idea.isIsNeed()  ){
		
		String eventoSuIdea="modifica";	
		if(isNew) eventoSuIdea="nuova";	

		Cdv.cdvEventNotification(idea, user.getUserId(), coworkersUserIdList, eventoSuIdea ,coworkersListOld );
	}
	
}


/**
 * @param currentUser
 * @param idea
 * @return
 */
public static List<User> getListUsersOfMyCompanyNoAuthorIdea (User currentUser, CLSIdea idea){
	
	List<User> employees = new ArrayList<User>();
	
	employees = MyUtils.getListUsersOfMyCompany(currentUser);
	
	
	try {
		User author = UserLocalServiceUtil.getUser(idea.getUserId());
		
		employees.remove(author);
		
	} catch (PortalException | SystemException e) {
		
		e.printStackTrace();
	}
	
	
	
	return employees;
}


/**
 * @param companyLeader
 * @param userId
 * @return
 */
public static boolean isUserOfCompanyLeader(User companyLeader, long userId){
	
	List<User> employees =  MyUtils.getListUsersOfMyCompany(companyLeader);
	
	for (User emp:employees){
		
		if (emp.getUserId() == userId )
			return true;
	}
	
	return false;
}


/**
 * @param companyLeader
 * @param userId
 * @return
 */
public static boolean isUserOfCompanyLeaderByUserId(long companyLeaderId, long userId){
	
	
	if (companyLeaderId <=0)
		return false;
	
	
	User companyLeader = null ;
	try {
		companyLeader = UserLocalServiceUtil.getUser(companyLeaderId);
	} catch (PortalException | SystemException e) {
		e.printStackTrace();
	}
	
		
	return isUserOfCompanyLeader(companyLeader,  userId);
}


	
}
