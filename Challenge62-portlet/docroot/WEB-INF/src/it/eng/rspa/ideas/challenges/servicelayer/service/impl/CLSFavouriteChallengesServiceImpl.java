/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.impl;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteChallengesLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSFavouriteChallengesServiceBaseImpl;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteIdeasUtil;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the c l s favourite challenges remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteChallengesService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSFavouriteChallengesServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteChallengesServiceUtil
 */
public class CLSFavouriteChallengesServiceImpl
	extends CLSFavouriteChallengesServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteChallengesServiceUtil} to access the c l s favourite challenges remote service.
	 */
	
	public List<CLSFavouriteChallenges> getFavouriteChallenges(Long favChallengeId, Long userId) throws SystemException {
		return CLSFavouriteChallengesLocalServiceUtil.getFavouriteChallenges(favChallengeId, userId);
	}
	
	public boolean removeFavouriteChallenge(Long favChallengeId, Long userId) throws  SystemException {
		return CLSFavouriteChallengesLocalServiceUtil.removeFavouriteChallenge(favChallengeId, userId);
	}
	
	public boolean addFavouriteChallenge(Long favChallengeId, Long userId) throws  SystemException{
		return CLSFavouriteChallengesLocalServiceUtil.addFavouriteChallenge(favChallengeId, userId);
	}
	

	
	
}