/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSMapPropertiesException;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSMapPropertiesImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSMapPropertiesModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the c l s map properties service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSMapPropertiesPersistence
 * @see CLSMapPropertiesUtil
 * @generated
 */
public class CLSMapPropertiesPersistenceImpl extends BasePersistenceImpl<CLSMapProperties>
	implements CLSMapPropertiesPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CLSMapPropertiesUtil} to access the c l s map properties persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CLSMapPropertiesImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CLSMapPropertiesModelImpl.ENTITY_CACHE_ENABLED,
			CLSMapPropertiesModelImpl.FINDER_CACHE_ENABLED,
			CLSMapPropertiesImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CLSMapPropertiesModelImpl.ENTITY_CACHE_ENABLED,
			CLSMapPropertiesModelImpl.FINDER_CACHE_ENABLED,
			CLSMapPropertiesImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CLSMapPropertiesModelImpl.ENTITY_CACHE_ENABLED,
			CLSMapPropertiesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public CLSMapPropertiesPersistenceImpl() {
		setModelClass(CLSMapProperties.class);
	}

	/**
	 * Caches the c l s map properties in the entity cache if it is enabled.
	 *
	 * @param clsMapProperties the c l s map properties
	 */
	@Override
	public void cacheResult(CLSMapProperties clsMapProperties) {
		EntityCacheUtil.putResult(CLSMapPropertiesModelImpl.ENTITY_CACHE_ENABLED,
			CLSMapPropertiesImpl.class, clsMapProperties.getPrimaryKey(),
			clsMapProperties);

		clsMapProperties.resetOriginalValues();
	}

	/**
	 * Caches the c l s map propertieses in the entity cache if it is enabled.
	 *
	 * @param clsMapPropertieses the c l s map propertieses
	 */
	@Override
	public void cacheResult(List<CLSMapProperties> clsMapPropertieses) {
		for (CLSMapProperties clsMapProperties : clsMapPropertieses) {
			if (EntityCacheUtil.getResult(
						CLSMapPropertiesModelImpl.ENTITY_CACHE_ENABLED,
						CLSMapPropertiesImpl.class,
						clsMapProperties.getPrimaryKey()) == null) {
				cacheResult(clsMapProperties);
			}
			else {
				clsMapProperties.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all c l s map propertieses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CLSMapPropertiesImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CLSMapPropertiesImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the c l s map properties.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CLSMapProperties clsMapProperties) {
		EntityCacheUtil.removeResult(CLSMapPropertiesModelImpl.ENTITY_CACHE_ENABLED,
			CLSMapPropertiesImpl.class, clsMapProperties.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<CLSMapProperties> clsMapPropertieses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CLSMapProperties clsMapProperties : clsMapPropertieses) {
			EntityCacheUtil.removeResult(CLSMapPropertiesModelImpl.ENTITY_CACHE_ENABLED,
				CLSMapPropertiesImpl.class, clsMapProperties.getPrimaryKey());
		}
	}

	/**
	 * Creates a new c l s map properties with the primary key. Does not add the c l s map properties to the database.
	 *
	 * @param mapPropertiesId the primary key for the new c l s map properties
	 * @return the new c l s map properties
	 */
	@Override
	public CLSMapProperties create(long mapPropertiesId) {
		CLSMapProperties clsMapProperties = new CLSMapPropertiesImpl();

		clsMapProperties.setNew(true);
		clsMapProperties.setPrimaryKey(mapPropertiesId);

		return clsMapProperties;
	}

	/**
	 * Removes the c l s map properties with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param mapPropertiesId the primary key of the c l s map properties
	 * @return the c l s map properties that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSMapPropertiesException if a c l s map properties with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSMapProperties remove(long mapPropertiesId)
		throws NoSuchCLSMapPropertiesException, SystemException {
		return remove((Serializable)mapPropertiesId);
	}

	/**
	 * Removes the c l s map properties with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the c l s map properties
	 * @return the c l s map properties that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSMapPropertiesException if a c l s map properties with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSMapProperties remove(Serializable primaryKey)
		throws NoSuchCLSMapPropertiesException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CLSMapProperties clsMapProperties = (CLSMapProperties)session.get(CLSMapPropertiesImpl.class,
					primaryKey);

			if (clsMapProperties == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCLSMapPropertiesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(clsMapProperties);
		}
		catch (NoSuchCLSMapPropertiesException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CLSMapProperties removeImpl(CLSMapProperties clsMapProperties)
		throws SystemException {
		clsMapProperties = toUnwrappedModel(clsMapProperties);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(clsMapProperties)) {
				clsMapProperties = (CLSMapProperties)session.get(CLSMapPropertiesImpl.class,
						clsMapProperties.getPrimaryKeyObj());
			}

			if (clsMapProperties != null) {
				session.delete(clsMapProperties);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (clsMapProperties != null) {
			clearCache(clsMapProperties);
		}

		return clsMapProperties;
	}

	@Override
	public CLSMapProperties updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties clsMapProperties)
		throws SystemException {
		clsMapProperties = toUnwrappedModel(clsMapProperties);

		boolean isNew = clsMapProperties.isNew();

		Session session = null;

		try {
			session = openSession();

			if (clsMapProperties.isNew()) {
				session.save(clsMapProperties);

				clsMapProperties.setNew(false);
			}
			else {
				session.merge(clsMapProperties);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(CLSMapPropertiesModelImpl.ENTITY_CACHE_ENABLED,
			CLSMapPropertiesImpl.class, clsMapProperties.getPrimaryKey(),
			clsMapProperties);

		return clsMapProperties;
	}

	protected CLSMapProperties toUnwrappedModel(
		CLSMapProperties clsMapProperties) {
		if (clsMapProperties instanceof CLSMapPropertiesImpl) {
			return clsMapProperties;
		}

		CLSMapPropertiesImpl clsMapPropertiesImpl = new CLSMapPropertiesImpl();

		clsMapPropertiesImpl.setNew(clsMapProperties.isNew());
		clsMapPropertiesImpl.setPrimaryKey(clsMapProperties.getPrimaryKey());

		clsMapPropertiesImpl.setMapPropertiesId(clsMapProperties.getMapPropertiesId());
		clsMapPropertiesImpl.setMapCenterLatitude(clsMapProperties.getMapCenterLatitude());
		clsMapPropertiesImpl.setMapCenterLongitude(clsMapProperties.getMapCenterLongitude());

		return clsMapPropertiesImpl;
	}

	/**
	 * Returns the c l s map properties with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s map properties
	 * @return the c l s map properties
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSMapPropertiesException if a c l s map properties with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSMapProperties findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCLSMapPropertiesException, SystemException {
		CLSMapProperties clsMapProperties = fetchByPrimaryKey(primaryKey);

		if (clsMapProperties == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCLSMapPropertiesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return clsMapProperties;
	}

	/**
	 * Returns the c l s map properties with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSMapPropertiesException} if it could not be found.
	 *
	 * @param mapPropertiesId the primary key of the c l s map properties
	 * @return the c l s map properties
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSMapPropertiesException if a c l s map properties with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSMapProperties findByPrimaryKey(long mapPropertiesId)
		throws NoSuchCLSMapPropertiesException, SystemException {
		return findByPrimaryKey((Serializable)mapPropertiesId);
	}

	/**
	 * Returns the c l s map properties with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s map properties
	 * @return the c l s map properties, or <code>null</code> if a c l s map properties with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSMapProperties fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		CLSMapProperties clsMapProperties = (CLSMapProperties)EntityCacheUtil.getResult(CLSMapPropertiesModelImpl.ENTITY_CACHE_ENABLED,
				CLSMapPropertiesImpl.class, primaryKey);

		if (clsMapProperties == _nullCLSMapProperties) {
			return null;
		}

		if (clsMapProperties == null) {
			Session session = null;

			try {
				session = openSession();

				clsMapProperties = (CLSMapProperties)session.get(CLSMapPropertiesImpl.class,
						primaryKey);

				if (clsMapProperties != null) {
					cacheResult(clsMapProperties);
				}
				else {
					EntityCacheUtil.putResult(CLSMapPropertiesModelImpl.ENTITY_CACHE_ENABLED,
						CLSMapPropertiesImpl.class, primaryKey,
						_nullCLSMapProperties);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CLSMapPropertiesModelImpl.ENTITY_CACHE_ENABLED,
					CLSMapPropertiesImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return clsMapProperties;
	}

	/**
	 * Returns the c l s map properties with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param mapPropertiesId the primary key of the c l s map properties
	 * @return the c l s map properties, or <code>null</code> if a c l s map properties with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSMapProperties fetchByPrimaryKey(long mapPropertiesId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)mapPropertiesId);
	}

	/**
	 * Returns all the c l s map propertieses.
	 *
	 * @return the c l s map propertieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSMapProperties> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s map propertieses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSMapPropertiesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s map propertieses
	 * @param end the upper bound of the range of c l s map propertieses (not inclusive)
	 * @return the range of c l s map propertieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSMapProperties> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s map propertieses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSMapPropertiesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s map propertieses
	 * @param end the upper bound of the range of c l s map propertieses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of c l s map propertieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSMapProperties> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CLSMapProperties> list = (List<CLSMapProperties>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CLSMAPPROPERTIES);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CLSMAPPROPERTIES;

				if (pagination) {
					sql = sql.concat(CLSMapPropertiesModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CLSMapProperties>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSMapProperties>(list);
				}
				else {
					list = (List<CLSMapProperties>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the c l s map propertieses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (CLSMapProperties clsMapProperties : findAll()) {
			remove(clsMapProperties);
		}
	}

	/**
	 * Returns the number of c l s map propertieses.
	 *
	 * @return the number of c l s map propertieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CLSMAPPROPERTIES);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the c l s map properties persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CLSMapProperties>> listenersList = new ArrayList<ModelListener<CLSMapProperties>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CLSMapProperties>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CLSMapPropertiesImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CLSMAPPROPERTIES = "SELECT clsMapProperties FROM CLSMapProperties clsMapProperties";
	private static final String _SQL_COUNT_CLSMAPPROPERTIES = "SELECT COUNT(clsMapProperties) FROM CLSMapProperties clsMapProperties";
	private static final String _ORDER_BY_ENTITY_ALIAS = "clsMapProperties.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CLSMapProperties exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CLSMapPropertiesPersistenceImpl.class);
	private static CLSMapProperties _nullCLSMapProperties = new CLSMapPropertiesImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CLSMapProperties> toCacheModel() {
				return _nullCLSMapPropertiesCacheModel;
			}
		};

	private static CacheModel<CLSMapProperties> _nullCLSMapPropertiesCacheModel = new CacheModel<CLSMapProperties>() {
			@Override
			public CLSMapProperties toEntityModel() {
				return _nullCLSMapProperties;
			}
		};
}