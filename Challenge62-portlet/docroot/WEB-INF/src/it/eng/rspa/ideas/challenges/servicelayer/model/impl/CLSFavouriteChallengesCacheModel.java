/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing CLSFavouriteChallenges in entity cache.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSFavouriteChallenges
 * @generated
 */
public class CLSFavouriteChallengesCacheModel implements CacheModel<CLSFavouriteChallenges>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{challengeId=");
		sb.append(challengeId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CLSFavouriteChallenges toEntityModel() {
		CLSFavouriteChallengesImpl clsFavouriteChallengesImpl = new CLSFavouriteChallengesImpl();

		clsFavouriteChallengesImpl.setChallengeId(challengeId);
		clsFavouriteChallengesImpl.setUserId(userId);

		clsFavouriteChallengesImpl.resetOriginalValues();

		return clsFavouriteChallengesImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		challengeId = objectInput.readLong();
		userId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(challengeId);
		objectOutput.writeLong(userId);
	}

	public long challengeId;
	public long userId;
}