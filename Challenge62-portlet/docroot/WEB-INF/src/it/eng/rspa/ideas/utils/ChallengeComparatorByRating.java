package it.eng.rspa.ideas.utils;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;

import java.util.Comparator;

public class ChallengeComparatorByRating implements Comparator<CLSChallenge> {

	@Override
	public int compare(CLSChallenge gara1, CLSChallenge gara2) {
		
		double rating1 = ChallengesUtils.getAverageDoubleRatingsByChallenge(gara1);
		double rating2 = ChallengesUtils.getAverageDoubleRatingsByChallenge(gara2);
		
		if (rating2 > rating1)
			return 1;
		
		if (rating1 > rating2)
			return -1;
		
		return 0;
		
	}

}
