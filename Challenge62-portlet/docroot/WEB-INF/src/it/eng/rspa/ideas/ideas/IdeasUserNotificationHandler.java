package it.eng.rspa.ideas.ideas;

import java.util.Locale;
import java.util.ResourceBundle;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.notifications.BaseUserNotificationHandler;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.UserNotificationEvent;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.theme.ThemeDisplay;

public class IdeasUserNotificationHandler extends BaseUserNotificationHandler{

	
	public IdeasUserNotificationHandler() {
		setPortletId(PortletKeys.IDEAS);
	}
	
	@Override
	protected String getBody(UserNotificationEvent userNotificationEvent, ServiceContext serviceContext) throws Exception {
		
		ThemeDisplay themeD = serviceContext.getThemeDisplay();
		/*Gestione localizzazione*/
		ResourceBundle rB =  ResourceBundle.getBundle("Language", new Locale(themeD.getLocale().getLanguage(), themeD.getLocale().getCountry()));		
		String labelUtente=new String(rB.getString("ims.user") .getBytes("ISO-8859-1"), "UTF-8");
		
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject(userNotificationEvent.getPayload());
		
		StringBundler sb = new StringBundler(5);
		sb.append("<div class=\"title\">"+labelUtente+": ");
		sb.append(HtmlUtil.escape(jsonObject.getString("sender")));
		sb.append("</div><div class=\"body\">");
		sb.append(HtmlUtil.escape(jsonObject.getString("text-message")));
		sb.append("</div>");
		
		return sb.toString();
	}
	
	@Override
	protected String getLink(UserNotificationEvent userNotificationEvent, ServiceContext serviceContext) throws Exception {
		
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject(userNotificationEvent.getPayload());
		 String currentURL = jsonObject.getString("currentURL");
		 
		
		return currentURL;
	}
}
