/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.http;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaServiceUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaSoap}.
 * If the method in the service utility returns a
 * {@link it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea}, that is translated to a
 * {@link it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSIdeaServiceHttp
 * @see it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaSoap
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaServiceUtil
 * @generated
 */
public class CLSIdeaServiceSoap {
	public static java.lang.String publishScreenShot(java.lang.String body)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CLSIdeaServiceUtil.publishScreenShot(body);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String vcProjectUpdate(
		java.lang.String vmeprojectid, java.lang.String vmeprojectname,
		boolean ismockup, java.lang.String ideaid) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CLSIdeaServiceUtil.vcProjectUpdate(vmeprojectid,
					vmeprojectname, ismockup, ideaid);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	/**
	* @param fileEntryId
	
	questo metodo viene utilizzato per eliminare gli allegati con Ajax
	*/
	public static void deleteIdeaFile(long fileEntryId)
		throws RemoteException {
		try {
			CLSIdeaServiceUtil.deleteIdeaFile(fileEntryId);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(CLSIdeaServiceSoap.class);
}