package it.eng.rspa.ideas.utils;

import java.io.UnsupportedEncodingException;

import javax.mail.internet.InternetAddress;

import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.portal.kernel.messaging.DestinationNames;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageBusUtil;

public class MailUtil {

	public static void sendMail(String fromMail, String fromName,
								String toMail, String toName,
								String subject, String mailBody, boolean htmlFormat) throws UnsupportedEncodingException{
		
		InternetAddress from = new InternetAddress(fromMail, fromName);
		InternetAddress to = new InternetAddress(toMail, toName);
		
		MailMessage message = new MailMessage(from, to, subject, mailBody, htmlFormat);
		
		Message myMessage = new Message();
		myMessage.setDestinationName(DestinationNames.MAIL);
		myMessage.setPayload(message);
		MessageBusUtil.sendMessage(myMessage.getDestinationName(), myMessage);
	}
}
