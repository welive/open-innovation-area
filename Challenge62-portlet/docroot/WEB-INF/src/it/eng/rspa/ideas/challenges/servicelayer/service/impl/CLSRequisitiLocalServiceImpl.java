/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.impl;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti;
import it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSRequisitiLocalServiceBaseImpl;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSRequisitiUtil;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the c l s requisiti local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.ideas.challenges.servicelayer.service.CLSRequisitiLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSRequisitiLocalServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.CLSRequisitiLocalServiceUtil
 */
public class CLSRequisitiLocalServiceImpl
	extends CLSRequisitiLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.ideas.challenges.servicelayer.service.CLSRequisitiLocalServiceUtil} to access the c l s requisiti local service.
	 */
	
	public List<CLSRequisiti> getRequisitiByIdeaId(long ideaId) throws SystemException{
		return CLSRequisitiUtil.findByrequisitoByIdeaId(ideaId);
	}

	public List<CLSRequisiti> getRequirementByUserId(long userId) throws SystemException{
		return CLSRequisitiUtil.findByrequisitoByAuthorid(userId);
	}
	
	public List<CLSRequisiti> getRequirementByUserIdAndIdeaId(long userId, long ideaId) throws SystemException{
		return CLSRequisitiUtil.findByrequisitoByIdeaIdandAuthorid(ideaId, userId);
	}

}



