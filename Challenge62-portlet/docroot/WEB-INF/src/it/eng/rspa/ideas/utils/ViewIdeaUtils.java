package it.eng.rspa.ideas.utils;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCoworkerLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSRequisitiLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSVmeProjectsLocalServiceUtil;
import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.portlet.ActionRequest;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.User;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil;
import com.liferay.tasks.model.TasksDetails;
import com.liferay.tasks.model.TasksEntry;
import com.liferay.tasks.service.TasksEntryLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;

public class ViewIdeaUtils {
	
	/**
	 * @param groupId
	 * @param folderId
	 * @return
	 */
	public static List<DLFileEntry> getScreenshotsOrderedByRating(long groupId, long folderId){
		try {
			
			//Questo metodo ritorna gli screenshot ordinati per rating
			
			List<DLFileEntry> entries = DLFileEntryLocalServiceUtil.getFileEntries(groupId, folderId);
			
			SSComparatorByRating ssComparatorBR = new SSComparatorByRating();
			entries=ListUtil.sort(entries, ssComparatorBR);
			
			
			return entries;
		} catch (SystemException e) {
			e.printStackTrace();
			return new ArrayList<DLFileEntry>();
		}
	}
	
	/**
	 * @param groupId
	 * @param screenshotFolderId
	 * @return
	 */
	public static String getMostRatedScreenshot(long groupId, long screenshotFolderId){
		
		try {
			
			//controllo se la cartella utente esiste
			 DynamicQuery queryUtente = DynamicQueryFactoryUtil.forClass(DLFolder.class)
					 .add(PropertyFactoryUtil.forName("groupId").eq(new Long(groupId)))
					 .add(PropertyFactoryUtil.forName("parentFolderId").eq(screenshotFolderId))
					 .add(PropertyFactoryUtil.forName("name").eq(PortletProps.get("screenshot.thumbnail.foldername")));
			
			 List<DLFolder> dLFolderSS = new ArrayList<DLFolder>();
			
			 dLFolderSS = DLFolderLocalServiceUtil.dynamicQuery(queryUtente);
			
			 
			 if (dLFolderSS.size() > 0){		
				 DLFolder  thumbsFolder = DLFolderLocalServiceUtil.getFolder(groupId, screenshotFolderId,	PortletProps.get("screenshot.thumbnail.foldername"));
			
				List<DLFileEntry> screenshots = getScreenshotsOrderedByRating(groupId, thumbsFolder.getFolderId());
				if(screenshots.size()>0){
					DLFileEntry dlf_t = screenshots.get(0);
					String uuidSS = DLFileEntryLocalServiceUtil.getFileEntry(groupId, screenshotFolderId, dlf_t.getTitle()).getUuid();
					return uuidSS;
				}
				else
					return null;
			
			 }else
				 return null;
			 
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static List<CLSVmeProjects> getMockups(long ideaId){
		
		List<CLSVmeProjects> list = new ArrayList<CLSVmeProjects>();
		try { list = CLSVmeProjectsLocalServiceUtil.getMokcupsByIdeaId(ideaId);}
		catch (Exception e) {
			System.out.println("[Exception "+e.getMessage()+"] Something went wrong... returning empty mockups list");
			list = new ArrayList<CLSVmeProjects>(); 
		}
		
		return list;
	}
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static List<CLSVmeProjects> getMashups(long ideaId){
		
		List<CLSVmeProjects> list = new ArrayList<CLSVmeProjects>();
		try { list = CLSVmeProjectsLocalServiceUtil.getMashupsByIdeaId(ideaId);}
		catch (Exception e) {
			System.out.println("[Exception "+e.getMessage()+"] Something went wrong... returning empty mashups list");
			list = new ArrayList<CLSVmeProjects>(); 
		}
		
		return list;
	}
	
	/**
	 * @param u
	 * @param idea
	 * @return
	 * @throws SystemException
	 */
	public static boolean isCoworker(User u, CLSIdea idea) throws SystemException{
		List<CLSCoworker> list = CLSCoworkerLocalServiceUtil.getCoworkersByIdeaId(idea.getIdeaID());
		Iterator<CLSCoworker> it = list.iterator();
		while(it.hasNext()){
			if(it.next().getUserId()==u.getUserId())
				return true;
		}
		return false;
	}
	
	/**
	 * @param u
	 * @param idea
	 * @return
	 */
	public static boolean isAuthor(User u, CLSIdea idea){
		if(u.getUserId() == idea.getUserId())
			return true;
		else
			return false;
	}
	
	
	
	/**
	 * Ritorna la percentuale di raffinamento sulla base di requisiti e task
	 * @param ideaId
	 * @return
	 */
	public static int getPercentualeRaffinamento(long ideaId){
		int result=0;
		
		List<CLSRequisiti> requisiti= new ArrayList<CLSRequisiti>();
		
		try {
			 requisiti = CLSRequisitiLocalServiceUtil.getRequisitiByIdeaId(ideaId);
		} catch (SystemException e) {
			e.printStackTrace();
			return result;
		}
		
		if (requisiti.size() == 0)//se non ho requisito aspetto solo la comunicazione della fine
			return 100;
		
		double pesoPerReq = (double) 100/ requisiti.size();
		
		double completamentoReq = 0.00;
		double completamentoRef = 0;
		for(int i=0; i<requisiti.size();i++){
			List<TasksEntry> tasksForRequisito= new ArrayList<TasksEntry>();
			
			try {
				 tasksForRequisito=TasksEntryLocalServiceUtil.getByRequisitiId(requisiti.get(i).getRequisitoId());
			} catch (SystemException e) {
				e.printStackTrace();
				return result;
			}
			
			
			int tasksaperti = 0;
			for(int j=0;j<tasksForRequisito.size(); j++){
				
				if(tasksForRequisito.get(j).getStatoRequisito().equals("assegnato")){
					tasksaperti++;
				}
			}
			
			
			if (tasksForRequisito.size() > 0)
				completamentoReq = pesoPerReq - (tasksaperti*pesoPerReq/tasksForRequisito.size());
			else	
				completamentoReq = 0;
			
			completamentoRef += completamentoReq;
		}
		
			result=(int) Math.round(completamentoRef);
			
		return result;
	}
	
	
	
	

	/**
	 * @param ideaId
	 * @return
	 */
	public static boolean refinementNoPendingActivities(CLSIdea idea){
		
		boolean result=false;
		int tasks=0;
		int tasksaperti=0;
		
		
		List<CLSRequisiti> requisiti=new ArrayList<CLSRequisiti>();
		
		try {
			 requisiti = CLSRequisitiLocalServiceUtil.getRequisitiByIdeaId(idea.getIdeaID());
		} catch (SystemException e) {
			e.printStackTrace();
			return result;
		}
		
		int numRequisiti =requisiti.size();
		
		for(int i=0; i<requisiti.size();i++){
			List<TasksEntry> tasksForRequisito= new ArrayList<TasksEntry>();
			
			try {
				 tasksForRequisito=TasksEntryLocalServiceUtil.getByRequisitiId(requisiti.get(i).getRequisitoId());
			} catch (SystemException e) {
				e.printStackTrace();
				return result;
			}
			
			if (tasksForRequisito.size() == 0)//se c'e il requisito allora ci deve essere almeno un task
				return false;
			
			
			for(int j=0;j<tasksForRequisito.size(); j++){
				
				tasks++;
				
				if(tasksForRequisito.get(j).getStatoRequisito().equals("assegnato")){
					tasksaperti++;
				}
				
			}
		}
		
		//si puo procedere se i requisiti sono =0 oppure se ci sono requisiti ci deve essere almeno un task e i task devono essere tutti chiusi
		
		if (numRequisiti ==0){
			return true;
		}else{
			if( tasks>0 && tasksaperti==0 )
				return true;
			
		}
		
		
		return false;
	}
	
	
	/**
	 * @param ideaId
	 * @param userId
	 * @param isEnte
	 * @return
	 */
	public static boolean isShowedStateButtons(CLSIdea idea, User currentUser){
		
		boolean isEnteRiferimento = IdeasListUtils.isEntityReferencebyIdea(idea, currentUser);	
		
		if (!isEnteRiferimento)
			return false;
		
	
		if(idea.getIdeaStatus().equals(MyConstants.IDEA_STATE_SELECTED) || 
				idea.getIdeaStatus().equals(MyConstants.IDEA_STATE_IMPLEMENTATION)
			||	idea.getIdeaStatus().equals(MyConstants.IDEA_STATE_REFINEMENT)	) {
			return true;
		}
				

		
		return false;
	}
	
	
	/**
	 * @param idea
	 * @param currentUser
	 * @return
	 */
	public static boolean isShowedCloseRefinementButton(CLSIdea idea,User currentUser ){
		
		boolean isAuthor = IdeasListUtils.isAuthorByIdeaId(idea.getIdeaID(), currentUser);
		
		if (!isAuthor || !idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_REFINEMENT))
			return false;
		
		boolean noHavePendingActivities =  refinementNoPendingActivities(idea);
		
		int closed = idea.getStatus();
		
		
		if (noHavePendingActivities && closed !=1)
			return true;

		
		return false;
	}
	
	/**
	 * @param idea
	 * @param currentUser
	 * @return
	 */
	public static boolean isShowedReopenRefinementButton(CLSIdea idea,User currentUser ){
		
		boolean isEnteRiferimento = IdeasListUtils.isEntityReferencebyIdea(idea, currentUser);
		
		if (!isEnteRiferimento || !idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_REFINEMENT))
			return false;
		
		
		if (idea.getStatus() == 1)
			return true;

		
		return false;
	}
	
	
	
	
	
	/**
	 * @param actionRequest
	 * @param ideaId
	 * @return
	 */
	public String getUrlIdeaById(ActionRequest actionRequest,	long ideaId){

		  String	urlCompleto = IdeaManagementSystemProperties.getFriendlyUrlIdeas(ideaId);
			
		return urlCompleto;
	}
	
		
	
	
	/**
	 * Ritorna il link al file legato all'outcomes del task (TasksDetails)
	 * @param tk
	 * @return
	 */
	public static String getFileOutcomeLinkByTaskDetails (TasksDetails tk){
		
		TasksEntry te = null;
		try {
			 te = TasksEntryLocalServiceUtil.getTasksEntry(tk.getTasksEntryId());
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
		}
		
		CLSIdea idea = null;
		
		try {
			idea = CLSIdeaLocalServiceUtil.getCLSIdea(te.getIdeaId());
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
		}
		
		long groupId = idea.getGroupId();
				
		List<DLFileEntry> dlFiles = IdeasListUtils.getFilesByIdeaId(te.getIdeaId(), groupId);
		
		//scorro tutti i file legati all'idea
		for (DLFileEntry dlFile :dlFiles ){
			
			//trovero' il file dell'outcomes 
			if (dlFile.getName().equals(tk.getFilename())){
				  String fileURL = "/documents/" + groupId + StringPool.SLASH +  dlFile.getUuid();
				  return fileURL;
			}
			
		}
		
		return "";
	}
	
	
	/**
	 * @param idea
	 * @return
	 */
	public static boolean isVisibleRequirementSectionByIdea (CLSIdea idea){
		
		
		List<CLSRequisiti> requisiti = new ArrayList<CLSRequisiti>();
		try {
			requisiti = CLSRequisitiLocalServiceUtil.getRequisitiByIdeaId(idea.getIdeaID());
		} catch (SystemException e) {
			
			e.printStackTrace();
		}
		
		
		if (
			idea.getIdeaStatus().equals(MyConstants.IDEA_STATE_REFINEMENT) ||
			idea.getIdeaStatus().equals(MyConstants.IDEA_STATE_IMPLEMENTATION) ||
			idea.getIdeaStatus().equals(MyConstants.IDEA_STATE_MONITORING) ||
			(idea.getIdeaStatus().equals(MyConstants.IDEA_STATE_REJECT) && requisiti.size() > 0)
			){
			
			return true;
		}
		
		
		return false;
		
	}
	
	/**
	 * @param idea
	 * @return
	 */
	public static boolean isRefinementActivitiesClosed(CLSIdea idea){
		
		if (idea.getStatus() == 1)
			return true;
		else
			return false;
		
	}
	
	/**
	 * @param idea
	 * @return
	 */
	public static boolean isRefinementActivitiesEditable(CLSIdea idea){
		
		if (idea.getStatus() == 1 || !idea.getIdeaStatus().equals(MyConstants.IDEA_STATE_REFINEMENT))
			return false;
		
			return true;
		
	}
	
	

	
	
	/**
	 * @param idea
	 * @return
	 */
	public static boolean isIdeaInChargeOfCompany(CLSIdea idea){
		
		long organizationId =  idea.getMunicipalityOrganizationId();
		return MyUtils.isCompanyOrganization(organizationId);
		
	}
	
	
}
