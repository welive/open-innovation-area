package it.eng.rspa.ideas.utils;


import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;

import java.util.Comparator;

public class IdeaComparator implements Comparator<CLSIdea>{

	

	@Override
	public int compare(CLSIdea idea1, CLSIdea idea2) {
		
		int ret=idea2.getDateAdded().compareTo(idea1.getDateAdded());
		return ret;
	}

}
