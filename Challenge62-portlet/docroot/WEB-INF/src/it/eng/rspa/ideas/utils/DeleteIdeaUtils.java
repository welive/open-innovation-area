package it.eng.rspa.ideas.utils;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects;
import it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation;
import it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSArtifactsLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaPoiLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSOpLogIdeaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSRequisitiLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSVmeProjectsLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.IdeaEvaluationLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.NeedLinkedChallengeLocalServiceUtil;
import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;
import it.eng.rspa.jms.Producer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.ResourceConstants;
import com.liferay.portal.model.UserGroup;
import com.liferay.portal.service.ResourceLocalServiceUtil;
import com.liferay.portal.service.SubscriptionLocalServiceUtil;
import com.liferay.portal.service.UserGroupLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil;
import com.liferay.portlet.social.service.SocialActivityLocalServiceUtil;
import com.liferay.tasks.model.TasksEntry;
import com.liferay.tasks.service.TasksEntryLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;

public class DeleteIdeaUtils {
	
	
	/**
	 * @param ideaId
	 */
	public static void deletePoiByIdeaId(long ideaId){

		/////////////////////////  rimuovo i POI dell'idea
		try{
		List<CLSIdeaPoi> ideaPois = CLSIdeaPoiLocalServiceUtil.getIdeaPOIByIdeaId(ideaId);
		Iterator<CLSIdeaPoi> ideaPoisIt = ideaPois.iterator();
		
		while(ideaPoisIt.hasNext()){
			CLSIdeaPoi ideaPoi = ideaPoisIt.next();
			CLSIdeaPoiLocalServiceUtil.deleteCLSIdeaPoi(ideaPoi);
		}
		}catch(Exception e){ System.out.println(e.getMessage()); }

	}
	

	/**
	 * @param ideaId
	 */
	public static void deleteRequirementsAndTasksByIdeaId(long ideaId){
		
			/////////////////////////  cancello i Requisiti e i TASK
			try{
			List<CLSRequisiti> requisiti = CLSRequisitiLocalServiceUtil.getRequisitiByIdeaId(ideaId);
			
			for (int i = 0; i < requisiti.size(); i++) {
			
			List<TasksEntry> tasksForRequisito = TasksEntryLocalServiceUtil.getByRequisitiId(requisiti.get(i).getRequisitoId());
			
			/////////////////////////  cancello i TASK
			for (int y = 0; y < tasksForRequisito.size(); y++) {
			TasksEntryLocalServiceUtil.deleteTasksEntry(tasksForRequisito.get(y));
			}
			
			CLSRequisitiLocalServiceUtil.deleteCLSRequisiti(requisiti.get(i));
			}
			}catch(Exception e){ System.out.println(e.getMessage()); }
		
	}
	

	/**
	 * @param ideaId
	 */
	public static void deleteArtifactsAssociationByIdeaId(long ideaId){
		
		/////////////////////////  cancello l'associazione con gli artefatti
		try{
		
		List<CLSArtifacts> artefatti = CLSArtifactsLocalServiceUtil.getArtifactsByIdeaId(ideaId);
		for (int i = 0; i < artefatti.size(); i++) {
		
		CLSArtifactsLocalServiceUtil.deleteCLSArtifacts(artefatti.get(i));
		
		}
		
		}catch(Exception e){ System.out.println(e.getMessage()); }
		
	}
	
	/**
	 * @param artifactId
	 */
	public static void deletIdeasAssociationByArtifactId(long artifactId){
		
		/////////////////////////  Delete the associationss
		try{
		
		List<CLSArtifacts> artifacts = CLSArtifactsLocalServiceUtil.getArtifactsByArtifactId(artifactId);
		for (int i = 0; i < artifacts.size(); i++) {
		
			CLSArtifactsLocalServiceUtil.deleteCLSArtifacts(artifacts.get(i));
		
		}
		
		}catch(Exception e){ System.out.println(e.getMessage()); }
		
	}


	/**
	 * @param ideaId
	 */
	public static void deleteVMEProjectsByIdeaId(long ideaId){
		
		/////////////////////////  cancello i progetti del VME
		try{
		//prima i mashups
		List<CLSVmeProjects>  mashups = ViewIdeaUtils.getMashups(ideaId);
		
		for (int i = 0; i < mashups.size(); i++) {
		
		CLSVmeProjectsLocalServiceUtil.deleteCLSVmeProjects(mashups.get(i));
		}
		
		//prima i mockup
		List<CLSVmeProjects>  mockup = ViewIdeaUtils.getMockups(ideaId);
		
		for (int i = 0; i < mockup.size(); i++) {
		
		CLSVmeProjectsLocalServiceUtil.deleteCLSVmeProjects(mockup.get(i));
		}
		
		
		}catch(Exception e){ System.out.println(e.getMessage()); }
		
	}


	/**Delete association with challenges (only for Need)
	 * @param ideaId
	 */
	public static void deleteLinkWithChallengeByNeedId(long needId){

		List<NeedLinkedChallenge> nLCs = new ArrayList<>();
			try {
				nLCs = NeedLinkedChallengeLocalServiceUtil.getNeedLinkedChallengeByNeedId(needId);
			} catch (SystemException e) {
				
				e.printStackTrace();
			}

		for (NeedLinkedChallenge nlc : nLCs){
			try {
				NeedLinkedChallengeLocalServiceUtil.deleteNeedLinkedChallenge(nlc);
			} catch (SystemException e) {
				
				e.printStackTrace();
			}

		}
		
	}


	/**
	 * Delete all notification subscription on the idea 
	 * @param idea
	 */
	public static void deleteSubscriptionByIdea(CLSIdea idea){
		
		
		try {
		SubscriptionLocalServiceUtil.deleteSubscriptions(idea.getCompanyId(), CLSIdea.class.getName(), idea.getIdeaID());
		} catch (PortalException | SystemException e2) {

		e2.printStackTrace();
		}
		
	}


	/**
	 * @param idea
	 */
	public static void deleteIdeaAsAssetByIdea(CLSIdea idea){
	
		////////////////////////rimuovo l'IDEA come asset
		try{ AssetEntryLocalServiceUtil.deleteEntry(CLSIdea.class.getName(), idea.getPrimaryKey()); }
		catch(Exception e){ System.out.println(e.getMessage()); }

	}
	
	/**
	 * @param idea
	 */
	public static void deleteSocialActivitiesByIdea(CLSIdea idea){
	
		//rimuovo le social activities
		try { SocialActivityLocalServiceUtil.deleteActivities(CLSIdea.class.getName(), idea.getPrimaryKey()); } 
		catch (Exception e) { System.out.println(e.getMessage()); }
	}
	
	
	/**
	 * @param idea
	 */
	public static void deleteIdeaAsResourceByIdea (CLSIdea idea){
		try{
			//rimuovo l'idea dalle resource
			ResourceLocalServiceUtil.deleteResource(
					idea.getCompanyId(), 
					CLSIdea.class.getName(),
	                ResourceConstants.SCOPE_INDIVIDUAL, 
	                idea.getPrimaryKey());
		}
		catch(Exception e){ System.out.println(e.getMessage()); }
		
	}
	
	/**
	 * @param idea
	 */
	public static void deleteChatgroupByIdea (CLSIdea idea){
		
		UserGroup chatGroup = null;

		try {
			chatGroup = UserGroupLocalServiceUtil.fetchUserGroup(idea.getChatGroup());
			
		} catch (SystemException e1) {
			e1.printStackTrace();
		}
		
		
		try {
			//rimuovo l'autore dalla chatGroup
			UserGroupLocalServiceUtil.deleteUserUserGroup(idea.getUserId(), idea.getChatGroup());
		} 
		catch (Exception e) { System.out.println(e.getMessage()); }
		
		try {
			//Cancello il gruppo della chat
			UserGroupLocalServiceUtil.deleteUserGroup(chatGroup);
		} 
		catch (Exception e) { System.out.println(e.getMessage()); }
	}
	
	
	/**
	 * @param ideaId
	 */
	public static void deleteOpLogByIdeaId (long ideaId){
	
		try {
			//Rimuovo il log delle operazione dell'idea
			List<CLSOpLogIdea> opLogs = CLSOpLogIdeaLocalServiceUtil.getOpLogIdeaByIdeaId(ideaId);
			for (CLSOpLogIdea clsOpLogIdea : opLogs) {
				try { CLSOpLogIdeaLocalServiceUtil.deleteCLSOpLogIdea(clsOpLogIdea); }
				catch (Exception e) { System.out.println(e.getMessage()); }
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	
	/**
	 * @param ideaId
	 */
	public static void deleteIdeaEvaluationByIdeaId(long ideaId){
		
		List<IdeaEvaluation> iEvals = IdeaEvaluationLocalServiceUtil.getIdeaEvaluationsByIdeaId(ideaId);
		
		for (IdeaEvaluation iEval:iEvals){
			
			try {
				IdeaEvaluationLocalServiceUtil.deleteIdeaEvaluation(iEval);
			} catch (SystemException e) {
				
				e.printStackTrace();
			}
			
			
		}
		
	}
	
	
	
	
	/**
	 * @param ideaId
	 */
	public static void outNotificationOnDeleteIdeaByIdeadId(CLSIdea idea){
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////    Decision Engine NOTIFICATION - inizio
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		
		if (IdeaManagementSystemProperties.getEnabledProperty("deEnabled")  ){
		
		DecisionEngine.deDeleteIdeaNotifier(idea);
		}
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////    Decision Engine - fine
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////    JMS - inizio
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		
		boolean jmsEnabled = false;
		try{ jmsEnabled = IdeaManagementSystemProperties.getEnabledProperty("jmsEnabled"); }
		catch(Exception e){
			System.out.println(e.getMessage());
			jmsEnabled = false;
		}
		//Send JMS notitification to Sentiment Analyzer
		if(jmsEnabled){
			try {
				Producer sentimentProducer = new Producer(IdeaManagementSystemProperties.getProperty("jmsTopic"));
				sentimentProducer.sendIdeaRemoved(idea.getIdeaID());
			} 
			catch (Exception e) { System.out.println("In deleteIdea: "+e.getMessage()); }
		}
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////    JMS - fine
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		
	}
	
	
	
}
