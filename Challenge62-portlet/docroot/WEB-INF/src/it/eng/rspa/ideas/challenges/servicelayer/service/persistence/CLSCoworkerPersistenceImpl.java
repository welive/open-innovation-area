/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCoworkerImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCoworkerModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the c l s coworker service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSCoworkerPersistence
 * @see CLSCoworkerUtil
 * @generated
 */
public class CLSCoworkerPersistenceImpl extends BasePersistenceImpl<CLSCoworker>
	implements CLSCoworkerPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CLSCoworkerUtil} to access the c l s coworker persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CLSCoworkerImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CLSCoworkerModelImpl.ENTITY_CACHE_ENABLED,
			CLSCoworkerModelImpl.FINDER_CACHE_ENABLED, CLSCoworkerImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CLSCoworkerModelImpl.ENTITY_CACHE_ENABLED,
			CLSCoworkerModelImpl.FINDER_CACHE_ENABLED, CLSCoworkerImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CLSCoworkerModelImpl.ENTITY_CACHE_ENABLED,
			CLSCoworkerModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEAID = new FinderPath(CLSCoworkerModelImpl.ENTITY_CACHE_ENABLED,
			CLSCoworkerModelImpl.FINDER_CACHE_ENABLED, CLSCoworkerImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByIdeaID",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID =
		new FinderPath(CLSCoworkerModelImpl.ENTITY_CACHE_ENABLED,
			CLSCoworkerModelImpl.FINDER_CACHE_ENABLED, CLSCoworkerImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByIdeaID",
			new String[] { Long.class.getName() },
			CLSCoworkerModelImpl.IDEAID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IDEAID = new FinderPath(CLSCoworkerModelImpl.ENTITY_CACHE_ENABLED,
			CLSCoworkerModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByIdeaID",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the c l s coworkers where ideaID = &#63;.
	 *
	 * @param ideaID the idea i d
	 * @return the matching c l s coworkers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSCoworker> findByIdeaID(long ideaID)
		throws SystemException {
		return findByIdeaID(ideaID, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s coworkers where ideaID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCoworkerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaID the idea i d
	 * @param start the lower bound of the range of c l s coworkers
	 * @param end the upper bound of the range of c l s coworkers (not inclusive)
	 * @return the range of matching c l s coworkers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSCoworker> findByIdeaID(long ideaID, int start, int end)
		throws SystemException {
		return findByIdeaID(ideaID, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s coworkers where ideaID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCoworkerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaID the idea i d
	 * @param start the lower bound of the range of c l s coworkers
	 * @param end the upper bound of the range of c l s coworkers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s coworkers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSCoworker> findByIdeaID(long ideaID, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID;
			finderArgs = new Object[] { ideaID };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEAID;
			finderArgs = new Object[] { ideaID, start, end, orderByComparator };
		}

		List<CLSCoworker> list = (List<CLSCoworker>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSCoworker clsCoworker : list) {
				if ((ideaID != clsCoworker.getIdeaID())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSCOWORKER_WHERE);

			query.append(_FINDER_COLUMN_IDEAID_IDEAID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSCoworkerModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaID);

				if (!pagination) {
					list = (List<CLSCoworker>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSCoworker>(list);
				}
				else {
					list = (List<CLSCoworker>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s coworker in the ordered set where ideaID = &#63;.
	 *
	 * @param ideaID the idea i d
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s coworker
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException if a matching c l s coworker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCoworker findByIdeaID_First(long ideaID,
		OrderByComparator orderByComparator)
		throws NoSuchCLSCoworkerException, SystemException {
		CLSCoworker clsCoworker = fetchByIdeaID_First(ideaID, orderByComparator);

		if (clsCoworker != null) {
			return clsCoworker;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaID=");
		msg.append(ideaID);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSCoworkerException(msg.toString());
	}

	/**
	 * Returns the first c l s coworker in the ordered set where ideaID = &#63;.
	 *
	 * @param ideaID the idea i d
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s coworker, or <code>null</code> if a matching c l s coworker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCoworker fetchByIdeaID_First(long ideaID,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSCoworker> list = findByIdeaID(ideaID, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s coworker in the ordered set where ideaID = &#63;.
	 *
	 * @param ideaID the idea i d
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s coworker
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException if a matching c l s coworker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCoworker findByIdeaID_Last(long ideaID,
		OrderByComparator orderByComparator)
		throws NoSuchCLSCoworkerException, SystemException {
		CLSCoworker clsCoworker = fetchByIdeaID_Last(ideaID, orderByComparator);

		if (clsCoworker != null) {
			return clsCoworker;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaID=");
		msg.append(ideaID);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSCoworkerException(msg.toString());
	}

	/**
	 * Returns the last c l s coworker in the ordered set where ideaID = &#63;.
	 *
	 * @param ideaID the idea i d
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s coworker, or <code>null</code> if a matching c l s coworker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCoworker fetchByIdeaID_Last(long ideaID,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByIdeaID(ideaID);

		if (count == 0) {
			return null;
		}

		List<CLSCoworker> list = findByIdeaID(ideaID, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s coworkers before and after the current c l s coworker in the ordered set where ideaID = &#63;.
	 *
	 * @param coworkerId the primary key of the current c l s coworker
	 * @param ideaID the idea i d
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s coworker
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException if a c l s coworker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCoworker[] findByIdeaID_PrevAndNext(long coworkerId, long ideaID,
		OrderByComparator orderByComparator)
		throws NoSuchCLSCoworkerException, SystemException {
		CLSCoworker clsCoworker = findByPrimaryKey(coworkerId);

		Session session = null;

		try {
			session = openSession();

			CLSCoworker[] array = new CLSCoworkerImpl[3];

			array[0] = getByIdeaID_PrevAndNext(session, clsCoworker, ideaID,
					orderByComparator, true);

			array[1] = clsCoworker;

			array[2] = getByIdeaID_PrevAndNext(session, clsCoworker, ideaID,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSCoworker getByIdeaID_PrevAndNext(Session session,
		CLSCoworker clsCoworker, long ideaID,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSCOWORKER_WHERE);

		query.append(_FINDER_COLUMN_IDEAID_IDEAID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSCoworkerModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(ideaID);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsCoworker);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSCoworker> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s coworkers where ideaID = &#63; from the database.
	 *
	 * @param ideaID the idea i d
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByIdeaID(long ideaID) throws SystemException {
		for (CLSCoworker clsCoworker : findByIdeaID(ideaID, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(clsCoworker);
		}
	}

	/**
	 * Returns the number of c l s coworkers where ideaID = &#63;.
	 *
	 * @param ideaID the idea i d
	 * @return the number of matching c l s coworkers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByIdeaID(long ideaID) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IDEAID;

		Object[] finderArgs = new Object[] { ideaID };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSCOWORKER_WHERE);

			query.append(_FINDER_COLUMN_IDEAID_IDEAID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaID);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IDEAID_IDEAID_2 = "clsCoworker.ideaID = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID = new FinderPath(CLSCoworkerModelImpl.ENTITY_CACHE_ENABLED,
			CLSCoworkerModelImpl.FINDER_CACHE_ENABLED, CLSCoworkerImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserID",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID =
		new FinderPath(CLSCoworkerModelImpl.ENTITY_CACHE_ENABLED,
			CLSCoworkerModelImpl.FINDER_CACHE_ENABLED, CLSCoworkerImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUserID",
			new String[] { Long.class.getName() },
			CLSCoworkerModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(CLSCoworkerModelImpl.ENTITY_CACHE_ENABLED,
			CLSCoworkerModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserID",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the c l s coworkers where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching c l s coworkers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSCoworker> findByUserID(long userId)
		throws SystemException {
		return findByUserID(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s coworkers where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCoworkerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of c l s coworkers
	 * @param end the upper bound of the range of c l s coworkers (not inclusive)
	 * @return the range of matching c l s coworkers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSCoworker> findByUserID(long userId, int start, int end)
		throws SystemException {
		return findByUserID(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s coworkers where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCoworkerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of c l s coworkers
	 * @param end the upper bound of the range of c l s coworkers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s coworkers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSCoworker> findByUserID(long userId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<CLSCoworker> list = (List<CLSCoworker>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSCoworker clsCoworker : list) {
				if ((userId != clsCoworker.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSCOWORKER_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSCoworkerModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (!pagination) {
					list = (List<CLSCoworker>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSCoworker>(list);
				}
				else {
					list = (List<CLSCoworker>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s coworker in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s coworker
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException if a matching c l s coworker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCoworker findByUserID_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSCoworkerException, SystemException {
		CLSCoworker clsCoworker = fetchByUserID_First(userId, orderByComparator);

		if (clsCoworker != null) {
			return clsCoworker;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSCoworkerException(msg.toString());
	}

	/**
	 * Returns the first c l s coworker in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s coworker, or <code>null</code> if a matching c l s coworker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCoworker fetchByUserID_First(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSCoworker> list = findByUserID(userId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s coworker in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s coworker
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException if a matching c l s coworker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCoworker findByUserID_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSCoworkerException, SystemException {
		CLSCoworker clsCoworker = fetchByUserID_Last(userId, orderByComparator);

		if (clsCoworker != null) {
			return clsCoworker;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSCoworkerException(msg.toString());
	}

	/**
	 * Returns the last c l s coworker in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s coworker, or <code>null</code> if a matching c l s coworker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCoworker fetchByUserID_Last(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUserID(userId);

		if (count == 0) {
			return null;
		}

		List<CLSCoworker> list = findByUserID(userId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s coworkers before and after the current c l s coworker in the ordered set where userId = &#63;.
	 *
	 * @param coworkerId the primary key of the current c l s coworker
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s coworker
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException if a c l s coworker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCoworker[] findByUserID_PrevAndNext(long coworkerId, long userId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSCoworkerException, SystemException {
		CLSCoworker clsCoworker = findByPrimaryKey(coworkerId);

		Session session = null;

		try {
			session = openSession();

			CLSCoworker[] array = new CLSCoworkerImpl[3];

			array[0] = getByUserID_PrevAndNext(session, clsCoworker, userId,
					orderByComparator, true);

			array[1] = clsCoworker;

			array[2] = getByUserID_PrevAndNext(session, clsCoworker, userId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSCoworker getByUserID_PrevAndNext(Session session,
		CLSCoworker clsCoworker, long userId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSCOWORKER_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSCoworkerModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsCoworker);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSCoworker> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s coworkers where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUserID(long userId) throws SystemException {
		for (CLSCoworker clsCoworker : findByUserID(userId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(clsCoworker);
		}
	}

	/**
	 * Returns the number of c l s coworkers where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching c l s coworkers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUserID(long userId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID;

		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSCOWORKER_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_2 = "clsCoworker.userId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_IDEAIDANDUSERID = new FinderPath(CLSCoworkerModelImpl.ENTITY_CACHE_ENABLED,
			CLSCoworkerModelImpl.FINDER_CACHE_ENABLED, CLSCoworkerImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByIdeaIDAndUserID",
			new String[] { Long.class.getName(), Long.class.getName() },
			CLSCoworkerModelImpl.IDEAID_COLUMN_BITMASK |
			CLSCoworkerModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IDEAIDANDUSERID = new FinderPath(CLSCoworkerModelImpl.ENTITY_CACHE_ENABLED,
			CLSCoworkerModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByIdeaIDAndUserID",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns the c l s coworker where ideaID = &#63; and userId = &#63; or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException} if it could not be found.
	 *
	 * @param ideaID the idea i d
	 * @param userId the user ID
	 * @return the matching c l s coworker
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException if a matching c l s coworker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCoworker findByIdeaIDAndUserID(long ideaID, long userId)
		throws NoSuchCLSCoworkerException, SystemException {
		CLSCoworker clsCoworker = fetchByIdeaIDAndUserID(ideaID, userId);

		if (clsCoworker == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("ideaID=");
			msg.append(ideaID);

			msg.append(", userId=");
			msg.append(userId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchCLSCoworkerException(msg.toString());
		}

		return clsCoworker;
	}

	/**
	 * Returns the c l s coworker where ideaID = &#63; and userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param ideaID the idea i d
	 * @param userId the user ID
	 * @return the matching c l s coworker, or <code>null</code> if a matching c l s coworker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCoworker fetchByIdeaIDAndUserID(long ideaID, long userId)
		throws SystemException {
		return fetchByIdeaIDAndUserID(ideaID, userId, true);
	}

	/**
	 * Returns the c l s coworker where ideaID = &#63; and userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param ideaID the idea i d
	 * @param userId the user ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching c l s coworker, or <code>null</code> if a matching c l s coworker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCoworker fetchByIdeaIDAndUserID(long ideaID, long userId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { ideaID, userId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_IDEAIDANDUSERID,
					finderArgs, this);
		}

		if (result instanceof CLSCoworker) {
			CLSCoworker clsCoworker = (CLSCoworker)result;

			if ((ideaID != clsCoworker.getIdeaID()) ||
					(userId != clsCoworker.getUserId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_CLSCOWORKER_WHERE);

			query.append(_FINDER_COLUMN_IDEAIDANDUSERID_IDEAID_2);

			query.append(_FINDER_COLUMN_IDEAIDANDUSERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaID);

				qPos.add(userId);

				List<CLSCoworker> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDEAIDANDUSERID,
						finderArgs, list);
				}
				else {
					CLSCoworker clsCoworker = list.get(0);

					result = clsCoworker;

					cacheResult(clsCoworker);

					if ((clsCoworker.getIdeaID() != ideaID) ||
							(clsCoworker.getUserId() != userId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDEAIDANDUSERID,
							finderArgs, clsCoworker);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDEAIDANDUSERID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (CLSCoworker)result;
		}
	}

	/**
	 * Removes the c l s coworker where ideaID = &#63; and userId = &#63; from the database.
	 *
	 * @param ideaID the idea i d
	 * @param userId the user ID
	 * @return the c l s coworker that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCoworker removeByIdeaIDAndUserID(long ideaID, long userId)
		throws NoSuchCLSCoworkerException, SystemException {
		CLSCoworker clsCoworker = findByIdeaIDAndUserID(ideaID, userId);

		return remove(clsCoworker);
	}

	/**
	 * Returns the number of c l s coworkers where ideaID = &#63; and userId = &#63;.
	 *
	 * @param ideaID the idea i d
	 * @param userId the user ID
	 * @return the number of matching c l s coworkers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByIdeaIDAndUserID(long ideaID, long userId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IDEAIDANDUSERID;

		Object[] finderArgs = new Object[] { ideaID, userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CLSCOWORKER_WHERE);

			query.append(_FINDER_COLUMN_IDEAIDANDUSERID_IDEAID_2);

			query.append(_FINDER_COLUMN_IDEAIDANDUSERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaID);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IDEAIDANDUSERID_IDEAID_2 = "clsCoworker.ideaID = ? AND ";
	private static final String _FINDER_COLUMN_IDEAIDANDUSERID_USERID_2 = "clsCoworker.userId = ?";

	public CLSCoworkerPersistenceImpl() {
		setModelClass(CLSCoworker.class);
	}

	/**
	 * Caches the c l s coworker in the entity cache if it is enabled.
	 *
	 * @param clsCoworker the c l s coworker
	 */
	@Override
	public void cacheResult(CLSCoworker clsCoworker) {
		EntityCacheUtil.putResult(CLSCoworkerModelImpl.ENTITY_CACHE_ENABLED,
			CLSCoworkerImpl.class, clsCoworker.getPrimaryKey(), clsCoworker);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDEAIDANDUSERID,
			new Object[] { clsCoworker.getIdeaID(), clsCoworker.getUserId() },
			clsCoworker);

		clsCoworker.resetOriginalValues();
	}

	/**
	 * Caches the c l s coworkers in the entity cache if it is enabled.
	 *
	 * @param clsCoworkers the c l s coworkers
	 */
	@Override
	public void cacheResult(List<CLSCoworker> clsCoworkers) {
		for (CLSCoworker clsCoworker : clsCoworkers) {
			if (EntityCacheUtil.getResult(
						CLSCoworkerModelImpl.ENTITY_CACHE_ENABLED,
						CLSCoworkerImpl.class, clsCoworker.getPrimaryKey()) == null) {
				cacheResult(clsCoworker);
			}
			else {
				clsCoworker.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all c l s coworkers.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CLSCoworkerImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CLSCoworkerImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the c l s coworker.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CLSCoworker clsCoworker) {
		EntityCacheUtil.removeResult(CLSCoworkerModelImpl.ENTITY_CACHE_ENABLED,
			CLSCoworkerImpl.class, clsCoworker.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(clsCoworker);
	}

	@Override
	public void clearCache(List<CLSCoworker> clsCoworkers) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CLSCoworker clsCoworker : clsCoworkers) {
			EntityCacheUtil.removeResult(CLSCoworkerModelImpl.ENTITY_CACHE_ENABLED,
				CLSCoworkerImpl.class, clsCoworker.getPrimaryKey());

			clearUniqueFindersCache(clsCoworker);
		}
	}

	protected void cacheUniqueFindersCache(CLSCoworker clsCoworker) {
		if (clsCoworker.isNew()) {
			Object[] args = new Object[] {
					clsCoworker.getIdeaID(), clsCoworker.getUserId()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_IDEAIDANDUSERID,
				args, Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDEAIDANDUSERID,
				args, clsCoworker);
		}
		else {
			CLSCoworkerModelImpl clsCoworkerModelImpl = (CLSCoworkerModelImpl)clsCoworker;

			if ((clsCoworkerModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_IDEAIDANDUSERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsCoworker.getIdeaID(), clsCoworker.getUserId()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_IDEAIDANDUSERID,
					args, Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDEAIDANDUSERID,
					args, clsCoworker);
			}
		}
	}

	protected void clearUniqueFindersCache(CLSCoworker clsCoworker) {
		CLSCoworkerModelImpl clsCoworkerModelImpl = (CLSCoworkerModelImpl)clsCoworker;

		Object[] args = new Object[] {
				clsCoworker.getIdeaID(), clsCoworker.getUserId()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAIDANDUSERID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDEAIDANDUSERID, args);

		if ((clsCoworkerModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_IDEAIDANDUSERID.getColumnBitmask()) != 0) {
			args = new Object[] {
					clsCoworkerModelImpl.getOriginalIdeaID(),
					clsCoworkerModelImpl.getOriginalUserId()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAIDANDUSERID,
				args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDEAIDANDUSERID,
				args);
		}
	}

	/**
	 * Creates a new c l s coworker with the primary key. Does not add the c l s coworker to the database.
	 *
	 * @param coworkerId the primary key for the new c l s coworker
	 * @return the new c l s coworker
	 */
	@Override
	public CLSCoworker create(long coworkerId) {
		CLSCoworker clsCoworker = new CLSCoworkerImpl();

		clsCoworker.setNew(true);
		clsCoworker.setPrimaryKey(coworkerId);

		return clsCoworker;
	}

	/**
	 * Removes the c l s coworker with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param coworkerId the primary key of the c l s coworker
	 * @return the c l s coworker that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException if a c l s coworker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCoworker remove(long coworkerId)
		throws NoSuchCLSCoworkerException, SystemException {
		return remove((Serializable)coworkerId);
	}

	/**
	 * Removes the c l s coworker with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the c l s coworker
	 * @return the c l s coworker that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException if a c l s coworker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCoworker remove(Serializable primaryKey)
		throws NoSuchCLSCoworkerException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CLSCoworker clsCoworker = (CLSCoworker)session.get(CLSCoworkerImpl.class,
					primaryKey);

			if (clsCoworker == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCLSCoworkerException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(clsCoworker);
		}
		catch (NoSuchCLSCoworkerException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CLSCoworker removeImpl(CLSCoworker clsCoworker)
		throws SystemException {
		clsCoworker = toUnwrappedModel(clsCoworker);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(clsCoworker)) {
				clsCoworker = (CLSCoworker)session.get(CLSCoworkerImpl.class,
						clsCoworker.getPrimaryKeyObj());
			}

			if (clsCoworker != null) {
				session.delete(clsCoworker);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (clsCoworker != null) {
			clearCache(clsCoworker);
		}

		return clsCoworker;
	}

	@Override
	public CLSCoworker updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker clsCoworker)
		throws SystemException {
		clsCoworker = toUnwrappedModel(clsCoworker);

		boolean isNew = clsCoworker.isNew();

		CLSCoworkerModelImpl clsCoworkerModelImpl = (CLSCoworkerModelImpl)clsCoworker;

		Session session = null;

		try {
			session = openSession();

			if (clsCoworker.isNew()) {
				session.save(clsCoworker);

				clsCoworker.setNew(false);
			}
			else {
				session.merge(clsCoworker);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CLSCoworkerModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((clsCoworkerModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsCoworkerModelImpl.getOriginalIdeaID()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID,
					args);

				args = new Object[] { clsCoworkerModelImpl.getIdeaID() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID,
					args);
			}

			if ((clsCoworkerModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsCoworkerModelImpl.getOriginalUserId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);

				args = new Object[] { clsCoworkerModelImpl.getUserId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);
			}
		}

		EntityCacheUtil.putResult(CLSCoworkerModelImpl.ENTITY_CACHE_ENABLED,
			CLSCoworkerImpl.class, clsCoworker.getPrimaryKey(), clsCoworker);

		clearUniqueFindersCache(clsCoworker);
		cacheUniqueFindersCache(clsCoworker);

		return clsCoworker;
	}

	protected CLSCoworker toUnwrappedModel(CLSCoworker clsCoworker) {
		if (clsCoworker instanceof CLSCoworkerImpl) {
			return clsCoworker;
		}

		CLSCoworkerImpl clsCoworkerImpl = new CLSCoworkerImpl();

		clsCoworkerImpl.setNew(clsCoworker.isNew());
		clsCoworkerImpl.setPrimaryKey(clsCoworker.getPrimaryKey());

		clsCoworkerImpl.setIdeaID(clsCoworker.getIdeaID());
		clsCoworkerImpl.setCoworkerId(clsCoworker.getCoworkerId());
		clsCoworkerImpl.setUserId(clsCoworker.getUserId());

		return clsCoworkerImpl;
	}

	/**
	 * Returns the c l s coworker with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s coworker
	 * @return the c l s coworker
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException if a c l s coworker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCoworker findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCLSCoworkerException, SystemException {
		CLSCoworker clsCoworker = fetchByPrimaryKey(primaryKey);

		if (clsCoworker == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCLSCoworkerException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return clsCoworker;
	}

	/**
	 * Returns the c l s coworker with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException} if it could not be found.
	 *
	 * @param coworkerId the primary key of the c l s coworker
	 * @return the c l s coworker
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException if a c l s coworker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCoworker findByPrimaryKey(long coworkerId)
		throws NoSuchCLSCoworkerException, SystemException {
		return findByPrimaryKey((Serializable)coworkerId);
	}

	/**
	 * Returns the c l s coworker with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s coworker
	 * @return the c l s coworker, or <code>null</code> if a c l s coworker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCoworker fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		CLSCoworker clsCoworker = (CLSCoworker)EntityCacheUtil.getResult(CLSCoworkerModelImpl.ENTITY_CACHE_ENABLED,
				CLSCoworkerImpl.class, primaryKey);

		if (clsCoworker == _nullCLSCoworker) {
			return null;
		}

		if (clsCoworker == null) {
			Session session = null;

			try {
				session = openSession();

				clsCoworker = (CLSCoworker)session.get(CLSCoworkerImpl.class,
						primaryKey);

				if (clsCoworker != null) {
					cacheResult(clsCoworker);
				}
				else {
					EntityCacheUtil.putResult(CLSCoworkerModelImpl.ENTITY_CACHE_ENABLED,
						CLSCoworkerImpl.class, primaryKey, _nullCLSCoworker);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CLSCoworkerModelImpl.ENTITY_CACHE_ENABLED,
					CLSCoworkerImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return clsCoworker;
	}

	/**
	 * Returns the c l s coworker with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param coworkerId the primary key of the c l s coworker
	 * @return the c l s coworker, or <code>null</code> if a c l s coworker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCoworker fetchByPrimaryKey(long coworkerId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)coworkerId);
	}

	/**
	 * Returns all the c l s coworkers.
	 *
	 * @return the c l s coworkers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSCoworker> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s coworkers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCoworkerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s coworkers
	 * @param end the upper bound of the range of c l s coworkers (not inclusive)
	 * @return the range of c l s coworkers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSCoworker> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s coworkers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCoworkerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s coworkers
	 * @param end the upper bound of the range of c l s coworkers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of c l s coworkers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSCoworker> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CLSCoworker> list = (List<CLSCoworker>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CLSCOWORKER);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CLSCOWORKER;

				if (pagination) {
					sql = sql.concat(CLSCoworkerModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CLSCoworker>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSCoworker>(list);
				}
				else {
					list = (List<CLSCoworker>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the c l s coworkers from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (CLSCoworker clsCoworker : findAll()) {
			remove(clsCoworker);
		}
	}

	/**
	 * Returns the number of c l s coworkers.
	 *
	 * @return the number of c l s coworkers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CLSCOWORKER);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the c l s coworker persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CLSCoworker>> listenersList = new ArrayList<ModelListener<CLSCoworker>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CLSCoworker>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CLSCoworkerImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CLSCOWORKER = "SELECT clsCoworker FROM CLSCoworker clsCoworker";
	private static final String _SQL_SELECT_CLSCOWORKER_WHERE = "SELECT clsCoworker FROM CLSCoworker clsCoworker WHERE ";
	private static final String _SQL_COUNT_CLSCOWORKER = "SELECT COUNT(clsCoworker) FROM CLSCoworker clsCoworker";
	private static final String _SQL_COUNT_CLSCOWORKER_WHERE = "SELECT COUNT(clsCoworker) FROM CLSCoworker clsCoworker WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "clsCoworker.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CLSCoworker exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CLSCoworker exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CLSCoworkerPersistenceImpl.class);
	private static CLSCoworker _nullCLSCoworker = new CLSCoworkerImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CLSCoworker> toCacheModel() {
				return _nullCLSCoworkerCacheModel;
			}
		};

	private static CacheModel<CLSCoworker> _nullCLSCoworkerCacheModel = new CacheModel<CLSCoworker>() {
			@Override
			public CLSCoworker toEntityModel() {
				return _nullCLSCoworker;
			}
		};
}