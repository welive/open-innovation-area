package it.eng.rspa.ideas.utils.listener;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCoworkerLocalServiceUtil;
import it.eng.rspa.ideas.utils.IdeasListUtils;
import it.eng.rspa.ideas.utils.MyUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.Validator;

public class IdeaLoggingNotifier extends LoggingBuildingBlockNotificationService
		implements iIdeaListener {
	
	private enum Actions {
					publication, modification, deletion, 
					selection_state, refinement_state, implementation_state, monitoring_state
					};
	
	private JSONObject prepareObject(Actions action, CLSIdea i, boolean isNeed){
		
		String idFieldName = "ideaid";
		String titleFieldName = "ideatitle";
		
		if(isNeed){ 
			idFieldName = "needid"; 
			titleFieldName = "needtitle"; 
		}
		
		JSONObject json = JSONFactoryUtil.createJSONObject();
		/** ADD COMMON FIELDS **/
		//Add the IdeaID
			long sIdeaId = i.getIdeaID();
			String title = i.getIdeaTitle();
			json.put(idFieldName, sIdeaId);
			json.put(titleFieldName, title);
		
		
		//Add the PilotId
			String pilotId = MyUtils.getPilotByIdea(i);
			
			
			if(pilotId==null || pilotId.equals("")){
				_log.error("No pilotId associated with the idea "+sIdeaId);
				return null;
			}
			json.put("pilot", pilotId);
			
		Long challengeId = null;
		Long referredNeedId = null;
		Long ccuid = null;
		JSONArray collaborators_arr = JSONFactoryUtil.createJSONArray();
		
		switch(action){
			case publication:
				if(!isNeed){
					//Add the ChallengeId
						challengeId = i.getChallengeId();
						if(Validator.isNotNull(challengeId) && challengeId>0){
							json.put("challengeid", challengeId);
						}
						
					//Add the needId
						referredNeedId = i.getNeedId();
						if(Validator.isNotNull(referredNeedId) && referredNeedId> 0  ) {
							json.put("needid", referredNeedId);
						}
						
						
				}
				//Add the AuthorId
					ccuid = MyUtils.getCCUserIdbyUserId(i.getUserId());
					if(ccuid==null){
						_log.error("No ccUserId associated with the user "+i.getUserId());
						return null;
					}
					json.put("authorid", ccuid);
			
				//TODO rimuovere collaboratori per i Need	
				//Add the CollaboratorsId
					try {
						List<CLSCoworker> collaborators = CLSCoworkerLocalServiceUtil.getCoworkersByIdeaId(sIdeaId);
						for(CLSCoworker cw : collaborators){
							Long collaborator_ccUid = MyUtils.getCCUserIdbyUserId(cw.getUserId());
							if(collaborator_ccUid==null) continue;
							collaborators_arr.put(collaborator_ccUid);
						}
					} 
					catch (SystemException e) {
						e.printStackTrace();
						_log.debug("No coworker for idea "+sIdeaId);
					}
					json.put("collaboratorsid", collaborators_arr);
					
				//Add the categories
					Set<String> categories = new HashSet<String>();
					String[] subcats = IdeasListUtils.getCategoriesByIdeaId(i.getIdeaID(), Locale.ENGLISH);
					if(subcats!=null)
						categories.addAll(Arrays.asList(subcats));
					
					String macrocat = IdeasListUtils.getVocabularyNameByIdeaId(i.getIdeaID(), Locale.ENGLISH);
					if(macrocat!=null && !macrocat.equalsIgnoreCase(""))
						categories.add(macrocat);
					
					JSONArray categoriesArray = JSONFactoryUtil.createJSONArray();
					for(String s: categories)
						categoriesArray.put(s);
					
					json.put("categories", categoriesArray);
				
				break;
				
			case modification:
				if(!isNeed){
					//Add the ChallengeId
						challengeId = i.getChallengeId();
						if(challengeId!=null && challengeId>-1){
							json.put("challengeid", challengeId);
						}
				}
				//Add the AuthorId
					ccuid = MyUtils.getCCUserIdbyUserId(i.getUserId());
					if(ccuid==null){
						_log.error("No ccUserId associated with the user "+i.getUserId());
						return null;
					}
					collaborators_arr.put(ccuid);
			
				//Add the CollaboratorsId
					try {
						List<CLSCoworker> collaborators = CLSCoworkerLocalServiceUtil.getCoworkersByIdeaId(sIdeaId);
						for(CLSCoworker cw : collaborators){
							Long collaborator_ccUid = MyUtils.getCCUserIdbyUserId(cw.getUserId());
							if(collaborator_ccUid==null) continue;
							collaborators_arr.put(collaborator_ccUid);
						}
					} 
					catch (SystemException e) {
						e.printStackTrace();
						_log.debug("No coworker for idea "+sIdeaId);
					}
					json.put("usersid", collaborators_arr);
				
				break;
			case deletion:
				if(!isNeed){
					//Add the ChallengeId
						challengeId = i.getChallengeId();
						if(challengeId!=null && challengeId>-1){
							json.put("challengeid", challengeId);
						}
				}
				//Add the UsersId
					collaborators_arr = JSONFactoryUtil.createJSONArray();
					
					ccuid = MyUtils.getCCUserIdbyUserId(i.getUserId());
					if(ccuid==null){
						_log.error("No ccUserId associated with the user "+i.getUserId());
						return null;
					}
					collaborators_arr.put(ccuid);
					try {
						List<CLSCoworker> collaborators = CLSCoworkerLocalServiceUtil.getCoworkersByIdeaId(sIdeaId);
						for(CLSCoworker cw : collaborators){
							Long collaborator_ccUid = MyUtils.getCCUserIdbyUserId(cw.getUserId());
							if(collaborator_ccUid==null){
								_log.error("No ccUserId associated with the user "+cw.getUserId());
								continue;
							}
							collaborators_arr.put(collaborator_ccUid);
						}			
					} 
					catch (SystemException e) {
						e.printStackTrace();
						_log.debug("No coworker for idea "+sIdeaId);
					}
					
					json.put("usersid", collaborators_arr);
				break;
				
			case implementation_state:
				//Add the UsersId
					collaborators_arr = JSONFactoryUtil.createJSONArray();
					
					ccuid = MyUtils.getCCUserIdbyUserId(i.getUserId());
					if(ccuid==null){
						_log.error("No ccUserId associated with the user "+i.getUserId());
						return null;
					}
					
					json.put("authorid", ccuid);
					
					try {
						List<CLSCoworker> collaborators = CLSCoworkerLocalServiceUtil.getCoworkersByIdeaId(sIdeaId);
						for(CLSCoworker cw : collaborators){
							Long collaborator_ccUid = MyUtils.getCCUserIdbyUserId(cw.getUserId());
							if(collaborator_ccUid==null){
								_log.error("No ccUserId associated with the user "+cw.getUserId());
								continue;
							}
							collaborators_arr.put(collaborator_ccUid);
						}			
					} 
					catch (SystemException e) {
						e.printStackTrace();
						_log.debug("No coworkers for idea "+sIdeaId);
					}
					
					json.put("collaboratorsid", collaborators_arr);
				break;
				
			default:
				return null;
		}
		
		return json;
		
	}

	@Override
	public void onIdeaPublish(CLSIdea i) {
		
		if(i==null){
			_log.error("No idea to be notified");
			return;
		}
		
		boolean isNeed = i.isIsNeed();
		String eventName = "IdeaPublished";
		if(isNeed){ eventName = "NeedPublished"; }
		
		
		JSONObject json = prepareObject(Actions.publication, i, isNeed);
		if(json==null){ return; }
		
	 	sendNotification(eventName,json);
	 	return;
	 	
	}

	@Override
	public void onIdeaDelete(CLSIdea i) {
		if(i==null){
			_log.error("No idea to be notified");
			return;
		}
		
		boolean isNeed = i.isIsNeed();
		String eventName = "IdeaRemoved";
		if(isNeed){ eventName = "NeedRemoved"; }
		
		JSONObject json = prepareObject(Actions.deletion, i, isNeed);
		if(json==null){ return; }
		
	 	sendNotification(eventName,json);
	 	return;

	}

	@Override
	public void onIdeaUpdate(CLSIdea i) {
		if(i==null){
			_log.error("No idea to be notified");
			return;
		}
		
		boolean isNeed = i.isIsNeed();
		String eventName = "IdeaModified";
		if(isNeed){ eventName = "NeedModified"; }
		
		JSONObject json = prepareObject(Actions.modification, i, isNeed);
		if(json==null){ return; }
		
	 	sendNotification(eventName,json);
	 	return;
	}

	@Override
	public void onIdeaStateChange(CLSIdea i, String sourceState, String targetState) {
		
		if(i==null){
			_log.error("No idea to be notified");
			return;				
		}
		
		boolean isNeed = i.isIsNeed();
		
		switch(targetState.toLowerCase()){
			case "implementation":
				String eventName = "IdeaInImplementation";
				if(isNeed){ eventName = "NeedModified"; }
				
				JSONObject json = prepareObject(Actions.implementation_state, i, isNeed);
				if(json==null){ return; }
				
			 	sendNotification(eventName,json);
				break;
		}
		
		return;

	}

}
