/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing CLSArtifacts in entity cache.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSArtifacts
 * @generated
 */
public class CLSArtifactsCacheModel implements CacheModel<CLSArtifacts>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{ideaId=");
		sb.append(ideaId);
		sb.append(", artifactId=");
		sb.append(artifactId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CLSArtifacts toEntityModel() {
		CLSArtifactsImpl clsArtifactsImpl = new CLSArtifactsImpl();

		clsArtifactsImpl.setIdeaId(ideaId);
		clsArtifactsImpl.setArtifactId(artifactId);

		clsArtifactsImpl.resetOriginalValues();

		return clsArtifactsImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		ideaId = objectInput.readLong();
		artifactId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(ideaId);
		objectOutput.writeLong(artifactId);
	}

	public long ideaId;
	public long artifactId;
}