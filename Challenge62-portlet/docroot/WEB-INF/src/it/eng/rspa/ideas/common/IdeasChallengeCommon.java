package it.eng.rspa.ideas.common;

import it.eng.rspa.ideas.challenges.Challenges;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteChallengesImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteIdeasImpl;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSArtifactsLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteChallengesLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteIdeasLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSRequisitiLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteIdeasPK;
import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;
import it.eng.rspa.ideas.ideas.Ideas;
import it.eng.rspa.ideas.utils.ChallengesUtils;
import it.eng.rspa.ideas.utils.DecisionEngine;
import it.eng.rspa.ideas.utils.IdeasListUtils;
import it.eng.rspa.ideas.utils.MktUtils;
import it.eng.rspa.ideas.utils.MyConstants;
import it.eng.rspa.ideas.utils.MyUtils;
import it.eng.rspa.ideas.utils.NeedUtils;
import it.eng.rspa.ideas.utils.UpdateIdeaUtils;
import it.eng.rspa.ideas.utils.Vc;
import it.eng.rspa.ideas.utils.listener.iIdeaListener;
import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.model.Categoria;
import it.eng.rspa.marketplace.artefatto.model.ImmagineArt;
import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.CategoriaLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.service.ImmagineArtLocalServiceUtil;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.ibm.icu.text.SimpleDateFormat;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayWindowState;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.servlet.ServletResponseUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Layout;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.RoleConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.service.ResourcePermissionServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletURLFactoryUtil;
import com.liferay.portlet.asset.AssetTagException;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil;
import com.liferay.portlet.messageboards.model.MBMessage;
import com.liferay.portlet.messageboards.service.MBMessageServiceUtil;
import com.liferay.tasks.model.TasksEntry;
import com.liferay.tasks.service.TasksEntryLocalServiceUtil;
import com.liferay.tasks.service.TasksEntryServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.liferay.util.portlet.PortletProps;


/**
 * Portlet implementation class IdeeCuiCollaboro
 */
public class IdeasChallengeCommon extends MVCPortlet {
	
	
	
	/**
	 * @param actionRequest
	 * @param actionResponse
	 * @throws IOException
	 * @throws PortletException
	 */
	public void deleteIdea(ActionRequest actionRequest,	ActionResponse actionResponse)	throws IOException, PortletException {
		Ideas idea = new Ideas();
		idea.deleteIdea(actionRequest, 0);//ideaId passed by actionRequest
		
		MyUtils.redirect(actionRequest,actionResponse);
	}
	
	
	
	/**
	 * @param actionRequest
	 * @param actionResponse
	 * @throws IOException
	 * @throws PortletException
	 * @throws PortalException
	 * @throws SystemException
	 */
	public void updateIdeas(ActionRequest actionRequest,ActionResponse actionResponse)	throws IOException, PortletException, PortalException, SystemException {
		
		
		long ideaId = ParamUtil.getLong(actionRequest, "ideaId");
		String ideaTitle = ParamUtil.getString(actionRequest, "ideaTitle");
		String needTitle = ParamUtil.getString(actionRequest, "needTitle");
		boolean isNeed = ParamUtil.getBoolean(actionRequest, "needReport");
		boolean isWizard = ParamUtil.getBoolean(actionRequest, "isWizard");		
		
		
		if (isNeed && isWizard)
			ideaTitle = needTitle;
		
		String title = "";
		
		if(!ideaTitle.equals(title) || ideaId >0){
			
			title=ideaTitle;
			Ideas idea = new Ideas();
			ideaId=idea.updateIdeas(actionRequest, actionResponse);
		}
		
		if (ideaId <0){
			SessionErrors.add(actionRequest, "error");
			return;
		}
		
		
		if (!isWizard){//if isWizard the redirect is managed by a button
		
			if (!isNeed)		
				IdeasListUtils.redirectToIdea(actionRequest, actionResponse, ideaId);
			else
				NeedUtils.redirectToNeed(actionRequest, actionResponse, ideaId);
		
		}
	}
	
	/**
	 * @param actionRequest
	 * @param actionResponse
	 * @throws IOException
	 * @throws PortletException
	 * @throws PortalException
	 * @throws SystemException
	 */
	public void associateChallenge(ActionRequest actionRequest,	ActionResponse actionResponse)	throws IOException, PortletException, PortalException, SystemException {
		
		Ideas idea = new Ideas();
		idea.associateChallenge(actionRequest, actionResponse);
		
	}
	
	
	
	/**
	 * @param actionRequest
	 * @param actionResponse
	 * @throws IOException
	 * @throws PortletException
	 */
	public void deleteChallenge(ActionRequest actionRequest,ActionResponse actionResponse)	throws IOException, PortletException {
		Challenges ch = new Challenges();
		ch.deleteChallenge(actionRequest, actionResponse);
		
		MyUtils.redirect(actionRequest,actionResponse);
	}
	
	

	/**
	 * @param actionRequest
	 * @param actionResponse
	 * @throws IOException
	 * @throws PortletException
	 * @throws PortalException
	 * @throws SystemException
	 */
	public void updateChallenges(ActionRequest actionRequest,ActionResponse actionResponse)throws IOException, PortletException, PortalException, SystemException {
		long challengeId = ParamUtil.getLong(actionRequest, "challengeId");
		String challengeTitle = ParamUtil.getString(actionRequest, "challengeTitle");
		String titleC="";
		
		Challenges ch=null;
		if(!challengeTitle.equals(titleC) || challengeId >0){
			titleC=challengeTitle;
			ch = new Challenges();
			challengeId=ch.updateChallenges(actionRequest, actionResponse);
		}
		
		
		if (challengeId <0){
			SessionErrors.add(actionRequest, "error");
			return;
		}
		
		ChallengesUtils.redirectToChallenge(actionRequest,actionResponse, challengeId);
	}
	
	
	
	/**
	 * @param actionRequest
	 * @param actionResponse
	 * @throws Exception
	 */
	public void completeTask (ActionRequest actionRequest,ActionResponse actionResponse) throws Exception {
		//It is called on the state change of the idea
		
		Long ideaId = ParamUtil.getLong(actionRequest, "ideaId");
		
		User currentUser = (User) actionRequest.getAttribute(WebKeys.USER);
		boolean isEntityReference = IdeasListUtils.isEntityReferencebyIdeaId(ideaId, currentUser);
		if (!isEntityReference){//Security issue
			SessionErrors.add(actionRequest, "error");
			return;
		}
		
		String transictionName = ParamUtil.getString(actionRequest, "transictionName");

		ThemeDisplay themeD = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		ResourceBundle res = MyUtils.getResBundleViaPilotByAction(actionRequest);
		
		//update the field ideaStatus
		CLSIdea idea = CLSIdeaLocalServiceUtil.fetchCLSIdea(ideaId);
		CLSIdea ideaBak = idea;
		
		if(!transictionName.equals("refinementReopenActivities")){
			idea.setIdeaStatus(transictionName);
		
			//these 3 sets only for any utility - no current specific use
			idea.setStatusByUserId(themeD.getUserId());
			idea.setStatusByUserName(themeD.getUser().getFullName());
			idea.setStatusDate(new Date());
		}
		
		
		boolean isEnte=MyUtils.isAuthority(themeD.getUser());
		if(isEnte ){
			if (idea.getMunicipalityId() == 0){// If it is not assigned, then I assign it
				idea.setMunicipalityId(themeD.getUserId());
			}
		}
		
		
		if(transictionName.equals(MyConstants.IDEA_STATE_REFINEMENT)  || transictionName.equals("refinementReopenActivities")){
			
			
			//to keep track of the activities of refinement not concluded yet
			idea.setStatus(0);

			int numrequisitifunzionali = ParamUtil.getInteger(actionRequest,"numFunzionali");
			
			for(int i=0 ;i<numrequisitifunzionali;i++){
				
				String testoRequisito =ParamUtil.getString(actionRequest,"testorequisitifunzionali"+(i+1));
				if(!testoRequisito.isEmpty()){
					
					long reqId= CounterLocalServiceUtil.increment(CLSRequisiti.class.getName());
					CLSRequisiti requisito=CLSRequisitiLocalServiceUtil.createCLSRequisiti(reqId);
					requisito.setIdeaId(ideaId);
					requisito.setDescrizione(testoRequisito);
					requisito.setTipo("funzionale");
					requisito.setAuthorUser(themeD.getUserId());
					
					CLSRequisitiLocalServiceUtil.updateCLSRequisiti(requisito);
				}
			}
		}
		
		
		if(transictionName.equals(MyConstants.IDEA_STATE_IMPLEMENTATION)){
			
			//non ci sono attivita di refinement aperte
			idea.setStatus(1);
			
					///////////////////////////////////////////////////////////////////////////////////////
					//////////////////////////		Notifiche VME per gruppo 	  /////////////////////////
					///////////////////////////////////////////////////////////////////////////////////////
			if (IdeaManagementSystemProperties.getEnabledProperty("vcEnabled") && UpdateIdeaUtils.isIdeaWithDeveloper(idea.getIdeaID()) ){
			
				//notifico info sull'idea al VME in modo da generare il gruppo di utenti anche da quel lato
				Vc.vcNotification(idea, null);
			}
		}
		
		for(iIdeaListener listener : Ideas.ideaListeners){
			listener.onIdeaStateChange(ideaBak, ideaBak.getIdeaStatus(), transictionName);
		}
		
		
		CLSIdeaLocalServiceUtil.updateCLSIdea(idea);
		
		CLSIdeaLocalServiceUtil.sendNotificationForTransition(res,ideaId, transictionName, actionRequest);
		
		IdeasListUtils.redirectToIdea(actionRequest, actionResponse, ideaId);
	}
	
	
	/**
	 * @param actionRequest
	 * @param actionResponse
	 */
	public void refinementActivitiesFinished (ActionRequest actionRequest,ActionResponse actionResponse){
		
		Long ideaId = ParamUtil.getLong(actionRequest, "ideaId");

		User currentUser = (User) actionRequest.getAttribute(WebKeys.USER);
		boolean isAuthor = IdeasListUtils.isAuthorByIdeaId(ideaId, currentUser);
		if (!isAuthor){//Security issue
			SessionErrors.add(actionRequest, "error");
			return;
		}
		
		
		ResourceBundle res = MyUtils.getResBundleViaPilotByAction(actionRequest);
		
		
		CLSIdea idea= null;
		try {
			idea = CLSIdeaLocalServiceUtil.getCLSIdea(ideaId);
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
		}
		
		
		//per tenere traccia delle attivita' di refinement concluse
		idea.setStatus(1);
		
		
		try {
			CLSIdeaLocalServiceUtil.updateCLSIdea(idea);
		} catch (SystemException e) {
			
			e.printStackTrace();
		}
		
		CLSIdeaLocalServiceUtil.sendNotificationForTransition(res,ideaId, "refinementActivitiesFinished", actionRequest);
		
		
		IdeasListUtils.redirectToIdea(actionRequest, actionResponse, ideaId);
		
	}
	
	
	User user;
	Log logger = LogFactoryUtil.getLog(this.getClass());
	String pathImg = "";
	
	
@Override
public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)  throws IOException, PortletException {  
		
		//chiamo la funzione Ajax due volte.. la prima volta gli mando il file(ramo else)
		//la seconda volta (ramo if) gli mando il path dell'immagine che deve caricare
		
		
		ThemeDisplay themeDisplay= (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
		User user = (User) resourceRequest.getAttribute(WebKeys.USER);
		Long groupId =  themeDisplay.getPortletGroupId();
		

		

		String[] actionIdWiew =  {ActionKeys.VIEW};
		String[] actionIdRoot =  {ActionKeys.VIEW,ActionKeys.ACCESS, ActionKeys.ADD_DOCUMENT,ActionKeys.ADD_SHORTCUT,ActionKeys.ADD_SUBFOLDER,ActionKeys.UPDATE};
		Role userRols = null;
		try {
			if(user!=null)
				userRols = RoleLocalServiceUtil.getRole(user.getCompanyId(), RoleConstants.USER);
			else
				userRols = RoleLocalServiceUtil.getRole(themeDisplay.getCompanyId(), RoleConstants.GUEST);
			
		} catch (PortalException | SystemException e7) { 
			e7.printStackTrace();
		}
		
		
		List<Role> allRoles = new ArrayList<Role>();
		try {
			if(user!=null)
				allRoles = RoleLocalServiceUtil.getRoles(user.getCompanyId());
			else
				allRoles = RoleLocalServiceUtil.getRoles(themeDisplay.getCompanyId());
		} catch (SystemException e6) {
			
			e6.printStackTrace();
		}
		java.util.List<Role> roles = new ArrayList<Role>(); // se voglio tutti i ruoli --> RoleLocalServiceUtil.getRoles(user.getCompanyId());				
		roles.add(userRols);
		
		String  view=resourceRequest.getParameter("param");						
		if(view==null){
			view="caricamento";
		}
		
		//System.out.println("parametro: "+view);
		if(view.equals("showScreenShot")) {
			try {
				CLSIdea idea = CLSIdeaLocalServiceUtil.getCLSIdea(Long.parseLong(resourceRequest.getParameter("ideaId")));
				DLFolder screenshotsFolder = DLFolderLocalServiceUtil.getFolder(idea.getGroupId(), idea.getIdFolder(), PortletProps.get("screenshot.foldername"));

				DLFileEntry dlf = DLFileEntryLocalServiceUtil.getFileEntry(idea.getGroupId(), 
						screenshotsFolder.getFolderId(), 
						resourceRequest.getParameter("filename"));
				
				
				String pathDocumentLibrary = themeDisplay.getPortalURL()
											+ themeDisplay.getPathContext() + "/documents/"
											+ idea.getGroupId() + StringPool.SLASH;
				
				JSONObject json = JSONFactoryUtil.createJSONObject();
				json.put("return", "1");
				json.put("screenshotUrl", pathDocumentLibrary+dlf.getUuid());
				json.put("screenshotDescription", dlf.getDescription());
				json.put("userScreenName", UserLocalServiceUtil.getUser(dlf.getUserId()).getFullName());
				json.put("uploadDateTime", (new SimpleDateFormat("dd/MM/yyyy HH:mm")).format(dlf.getCreateDate()));
				
				resourceResponse.getWriter().write(json.toString());
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		}else if(view.equals("modifyReq")){
			String reqId=resourceRequest.getParameter("reqId");
			String descReq=resourceRequest.getParameter("descReq");
			
			CLSRequisiti requisito = null;
			try {
				requisito = CLSRequisitiLocalServiceUtil.getCLSRequisiti(Long.parseLong(reqId, 10));
				requisito.setDescrizione(descReq);
				CLSRequisitiLocalServiceUtil.updateCLSRequisiti(requisito);
				
			} catch (PortalException | SystemException e) {
				
				e.printStackTrace();
			}
			
		}
		else if(view.equals("deleteReq")){
			String reqId=resourceRequest.getParameter("reqId");
			
			
			
				try {
					CLSRequisitiLocalServiceUtil.deleteCLSRequisiti(Long.parseLong(reqId, 10));
				} catch (NumberFormatException e) {

					e.printStackTrace();
				} catch (PortalException | SystemException e) {

					e.printStackTrace();
				}
				
			
		}
		
		else if(view.equals("addReq")){
				String tipoRequisito=resourceRequest.getParameter("tipoRequisito");
				String descReq=resourceRequest.getParameter("descReq");
				String ideaId=resourceRequest.getParameter("ideaId");
				String authorUser=resourceRequest.getParameter("authorUser");
				
				
				try {
					long reqId = CounterLocalServiceUtil.increment(CLSRequisiti.class.getName());
					CLSRequisiti requisito=CLSRequisitiLocalServiceUtil.createCLSRequisiti(reqId);
//					requisito.setStato("NONassegnato");
					requisito.setIdeaId(Long.parseLong(ideaId, 10));
					requisito.setDescrizione(descReq);
					requisito.setTipo(tipoRequisito);
					requisito.setAuthorUser(Long.parseLong(authorUser));
					CLSRequisitiLocalServiceUtil.updateCLSRequisiti(requisito);
				} catch (SystemException e) {
					
					e.printStackTrace();
				}
				
				
			
		}else if (view.equalsIgnoreCase("search")  ){
			
			String keyword=resourceRequest.getParameter("key");
			String ideaId=resourceRequest.getParameter("idIdea");
			
			
			JSONArray jsonArray= JSONFactoryUtil.createJSONArray();
			JSONObject jsonObject2 = JSONFactoryUtil.createJSONObject();
			
			List<Artefatto>artefatti=ArtefattoLocalServiceUtil.getArtefattiByKeyword(keyword);
						
			Iterator<Artefatto> iteratore=artefatti.iterator();
			while(iteratore.hasNext()){
				Artefatto art= iteratore.next();
				List <CLSArtifacts> lista=new ArrayList<CLSArtifacts>();
				try {
					 lista= CLSArtifactsLocalServiceUtil.getArtifactsByIdeaIdEArtifactId(Long.parseLong(ideaId,10), art.getArtefattoId());
				} catch (SystemException e) {
					
					e.printStackTrace();
				}
				
				String associato="no";
				if(lista.size()>0){
					associato="yes";	
				}
				
				String category = "bblocks";
				String type = MyConstants.MKT_CAT_REST;
				try{ 
					Categoria c = CategoriaLocalServiceUtil.getCategoria(art.getCategoriamkpId());
					if(c!=null)
						type = c.getNomeCategoria();
					
					if(type.equalsIgnoreCase(MyConstants.MKT_CAT_REST) || type.equalsIgnoreCase(MyConstants.MKT_CAT_SOAP)){
						category = "bblock";
					}
					else if(type.equalsIgnoreCase(MyConstants.MKT_CAT_APPWEB) || type.equalsIgnoreCase(MyConstants.MKT_CAT_APPMOBILE)){
						category = "psa";
					}
					else if(type.equalsIgnoreCase(MyConstants.MKT_CAT_DATASET)){
						category = "dataset";
					}
				}
				catch(Exception e){e.printStackTrace();}
				
				String author = "";
				try{
					User u = UserLocalServiceUtil.getUser(art.getUserId());
					if(u!=null)
						author = u.getFullName();
				}
				catch(Exception e){e.printStackTrace();}
				
				JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
				jsonObject.put("titolo", art.getTitle());
				jsonObject.put("author", author);
				jsonObject.put("id",String.valueOf( art.getArtefattoId()));
				jsonObject.put("associato",associato);
				jsonObject.put("category", category);
				jsonObject.put("type", type);
				
				jsonArray.put(jsonObject);
			}
				
			
			jsonObject2.put("value",jsonArray);
			
			PrintWriter writer = resourceResponse.getWriter();
			writer.write(jsonObject2.toString());
			
		}else if (view.equalsIgnoreCase("suggestCollaborator")  ){
			
			String categAjax= resourceRequest.getParameter("categories");
			String tagAjax=resourceRequest.getParameter("tags");
			String lingua=resourceRequest.getParameter("lingua");
			
			JSONArray jsonArray = DecisionEngine.deUserReccomenderNotifier ( categAjax,  tagAjax,  lingua );
			
			JSONObject jsonObject2 = JSONFactoryUtil.createJSONObject();
			jsonObject2.put("value",jsonArray);
			
			PrintWriter writer = resourceResponse.getWriter();
			writer.write(jsonObject2.toString());
			
		
			}
		
		else if (view.equalsIgnoreCase("associa")  ){
			
			String ideaId=resourceRequest.getParameter("idIdea");
			String artefattoId=resourceRequest.getParameter("idArtefatto");
			
			
			Artefatto artefatto=null;
			try {
				artefatto = ArtefattoLocalServiceUtil.getArtefatto(Long.parseLong(artefattoId,10));
			} catch (NumberFormatException | PortalException | SystemException e1) {
								
				e1.printStackTrace();
			}
			
			CLSArtifactsPK primary= new CLSArtifactsPK(Long.parseLong(ideaId,10), Long.parseLong(artefattoId,10));
			CLSArtifacts artifacts=CLSArtifactsLocalServiceUtil.createCLSArtifacts(primary);
			try {
				CLSArtifactsLocalServiceUtil.addCLSArtifacts(artifacts);
			} catch (SystemException e) {
				
				e.printStackTrace();
			}
			
			String imgURl = "";
			String pathDocumentLibrary = themeDisplay.getPortalURL()
					+ themeDisplay.getPathContext() + "/documents/"+ themeDisplay.getScopeGroupId() + StringPool.SLASH;
			DLFileEntry dlf = null;
			ImmagineArt img = null;
			try { 
				if(artefatto.getImgId()>0){		
					img=ImmagineArtLocalServiceUtil.getImmagineArt(artefatto.getImgId());
				}else if (ImmagineArtLocalServiceUtil.findByArtefattoId(artefatto.getArtefattoId()).size() > 0) {
					img = ImmagineArtLocalServiceUtil.findByArtefattoId(artefatto.getArtefattoId()).get(0);
				}
				dlf = DLFileEntryLocalServiceUtil.getDLFileEntry(img.getDlImageId()); 
				imgURl =pathDocumentLibrary+ dlf.getUuid();
			} 
			catch (Exception e) { 
				
				imgURl = resourceRequest.getContextPath()+MktUtils.getDefaultMktIconByArtefact(artefatto);
			}
			
			JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
			jsonObject.put("titolo", artefatto.getTitle());
			jsonObject.put("providerName",artefatto.getProviderName());
			jsonObject.put("imgURl",imgURl);
			PrintWriter writer = resourceResponse.getWriter();
			writer.write(jsonObject.toString());
		
		
		}else if (view.equalsIgnoreCase("disassocia")  ){
			
			String ideaId=resourceRequest.getParameter("idIdea");
			String artefattoId=resourceRequest.getParameter("idArtefatto");
			
			CLSArtifactsPK primary= new CLSArtifactsPK(Long.parseLong(ideaId,10), Long.parseLong(artefattoId,10));
			CLSArtifacts artifacts=CLSArtifactsLocalServiceUtil.createCLSArtifacts(primary);
			try {
				CLSArtifactsLocalServiceUtil.deleteCLSArtifacts(artifacts);
			} catch (SystemException e) {
				
				e.printStackTrace();
			}
		}
		
		else if (view.equalsIgnoreCase("updateCategories")  ){
			
			//per tirare con Ajax, nelle idee, le categorie abilitate nelle gare
			
				String challengeId=resourceRequest.getParameter("challengeChoosen");
				String needId=resourceRequest.getParameter("needChoosen");
				Boolean isNeed= Boolean.valueOf(resourceRequest.getParameter("isNeed"));
				String locale=resourceRequest.getParameter("localePerAssetVoc");
				
				//in this way, it dosen't work:
					//Locale locale = (Locale)resourceRequest.getPortletSession().getAttribute("org.apache.struts.action.LOCALE");
									
				
				String tabNames = "";
				
				if (!isNeed)
					tabNames = ChallengesUtils.getVocabularyNameByChallengeId(Long.parseLong(challengeId), locale)+ ",";
				else
					tabNames = NeedUtils.getVocabularyNameByNeedId(Long.parseLong(needId), locale)+ ",";
				
				
				resourceResponse.setContentType("text/html");
		        PrintWriter writer2 = resourceResponse.getWriter();
		        JSONObject jsonObject2 =  JSONFactoryUtil.createJSONObject();
				
			    jsonObject2.put("vocabolari", tabNames);			
				
		        writer2.print(jsonObject2.toString());
		        writer2.flush();
		        writer2.close();
		}
		
		else if (view.equalsIgnoreCase("categoriesByVocabolaryId")  ){
			
				String vocId=resourceRequest.getParameter("vocId");
				String locale=resourceRequest.getParameter("localePerAssetVoc");
				
				JSONArray jsonAr = MyUtils.getCategoriesByVocabularyId(Long.parseLong(vocId), locale);
				
				resourceResponse.setContentType("text/html");
				
		        PrintWriter writer2 = resourceResponse.getWriter();
		        writer2.print(jsonAr.toString());
		        writer2.flush();
		        writer2.close();
		}
		
		
		else if (view.equalsIgnoreCase("titoloGara") || view.equalsIgnoreCase("titoloIdea") ){
			
			//controllo se il titolo  non sia gia' esistente
			
			String modifica=resourceRequest.getParameter("modifica");			
			
			boolean titleEsistente = false;
			long ideaId = 0;
			String ideaDetailsPage ="";	
			if (modifica.equals("-1") ){//se non siamo in modifica
			
				String titleChoosen=resourceRequest.getParameter("titleChoosen").trim();
							
				if (view.equalsIgnoreCase("titoloGara")  ){
					
					
					List<CLSChallenge> gares = new ArrayList<CLSChallenge>();
					try {
						int gareCount =CLSChallengeLocalServiceUtil.getCLSChallengesCount();
						
						gares = CLSChallengeLocalServiceUtil.getCLSChallenges(0, gareCount);
						
						
					} catch (SystemException e) {
						
						e.printStackTrace();
					}
								
					for (int y =0;y< gares.size(); y++){
						
						CLSChallenge gara = gares.get(y);					
						String titolo = gara.getChallengeTitle();
						
						if (titolo.equalsIgnoreCase(titleChoosen))
							titleEsistente = true;								
					}
				
				}else if (view.equalsIgnoreCase("titoloIdea")  ){
					
					List<CLSIdea> idee = new ArrayList<CLSIdea>();
					
					try {
						int ideaCount = CLSIdeaLocalServiceUtil.getCLSIdeasCount();
						idee = CLSIdeaLocalServiceUtil.getCLSIdeas(0,  ideaCount  );
					} catch (SystemException e) {
						
						e.printStackTrace();
					}
						
					
					for (int y =0;y< idee.size(); y++){
						
						CLSIdea idea = idee.get(y);					
						String titolo = idea.getIdeaTitle().trim();
						
						
						if (titolo.equalsIgnoreCase(titleChoosen)){
							titleEsistente = true;	
							
							if (idea.getIsNeed())
								ideaDetailsPage = IdeaManagementSystemProperties.getFriendlyUrlNeeds(idea.getIdeaID());
							else
								ideaDetailsPage = IdeaManagementSystemProperties.getFriendlyUrlIdeas(idea.getIdeaID());
							
							ideaId = idea.getIdeaID();
							break;
						}
														
					}
				}				
			}// se siamo in modifica titleEsistente = false
					
			
				resourceResponse.setContentType("text/html");
		        PrintWriter writer2 = resourceResponse.getWriter();
		        JSONObject jsonObject2 =  JSONFactoryUtil.createJSONObject();
				
				
				if (titleEsistente){
					
			        jsonObject2.put("titleExist", "yes");	
				}else{
					
					jsonObject2.put("titleExist", "no");
				}
				jsonObject2.put("ideaId", ideaId  );
				jsonObject2.put("ideaDetailsPage", ideaDetailsPage  );
				
				
		        writer2.print(jsonObject2.toString());
		        writer2.flush();
		        writer2.close();
		        
	//altra chiamata        
	}else if (view.equals("challenge")){
				
			
	try{
		OutputStream outStream;	   
		String jsonRet = "";
			
		
		// rimuove un'idea dai preferiti
		if (resourceRequest.getResourceID().equals("removeFavouriteChallenge")) {	
			user = (User) resourceRequest.getAttribute(WebKeys.USER);
			Long favChallengeId = ParamUtil.getLong(resourceRequest,"favChallengeId");
			// inserisco all'elenco delle idee preferite 
			jsonRet = String.valueOf( removeFavouriteChallenge(favChallengeId,user.getUserId())) ;
			
			outStream = resourceResponse.getPortletOutputStream();
            outStream.write(jsonRet.getBytes(Charset.forName("UTF-8")));
		}
		
		// aggiunge un'idea ai preferiti
		if (resourceRequest.getResourceID().equals("addFavouriteChallenge")) {	
			user = (User) resourceRequest.getAttribute(WebKeys.USER);
			Long favChallengeId = ParamUtil.getLong(resourceRequest,"favChallengeId");
			// inserisco all'elenco delle idee preferite 
			jsonRet = String.valueOf( addFavouriteChallenge(favChallengeId, user.getUserId())) ;
			
			outStream = resourceResponse.getPortletOutputStream();
            outStream.write(jsonRet.getBytes(Charset.forName("UTF-8")));
		}
		
		// ottengo l'elenco delle idee preferite
		if (resourceRequest.getResourceID().equals("getFavouriteChallenge")) {
				user = (User) resourceRequest.getAttribute(WebKeys.USER);
				Long favChallengeId = ParamUtil.getLong(resourceRequest,"favChallengeId");

				// ricavo l'elenco delle idee preferite 
				jsonRet = getFavouriteChallenges(favChallengeId, user.getUserId());
				
				outStream = resourceResponse.getPortletOutputStream();
	            outStream.write(jsonRet.getBytes(Charset.forName("UTF-8")));
		}
					
		
			
	}catch (Exception e) {
		e.printStackTrace();
		
	}
	
	
	}else if(view.equals("setPilot")){
		
		String pilotToSet = ParamUtil.getString(resourceRequest,"pilotToSet");
		
		MyUtils.setPilotByResourceReq(resourceRequest, pilotToSet);
		
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
		jsonObject.put("pilotToSet", pilotToSet);
		
		PrintWriter writer = resourceResponse.getWriter();
		writer.write(jsonObject.toString());
		
	}else if(view.equals("view")){
			JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
			jsonObject.put("value", pathImg);
			
			PrintWriter writer = resourceResponse.getWriter();
			writer.write(jsonObject.toString());
		
	}else if(view.equals("deleteTask")){
		
		long taskId = ParamUtil.getLong(resourceRequest,"taskId");
		try {
			TasksEntryLocalServiceUtil.deleteTasksEntry(taskId);
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
		}
	
}else if (view.equals("caricamento")){
		
		
			UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(resourceRequest);
			File objFile = uploadRequest.getFile("myFile");
						
			byte[] bytesImg = FileUtil.getBytes(objFile);//original image	
			String imgType = MyUtils.getFileExtension(objFile); //originale img extension
			
			byte[] thumb = new byte[0]; //resized image
			try {
				 thumb = MyUtils.createThumbnail(bytesImg, MyConstants.REPRESENTATIVEIMG_THUMB_SIZE,imgType);
				
			} catch (Exception e1) {
				
				e1.printStackTrace();
			}
			
			String filename = "img_"+new GregorianCalendar().getTimeInMillis()+"."+imgType.replace("image/", "");//new filename
			
			
			if(IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled"))
				System.out.println("File length :"+ objFile.length());


			
			DLFolder ideaManagementFolderRoot = null;
			
			ServiceContext serviceContext = null;
			try {
				serviceContext = ServiceContextFactory.getInstance(resourceRequest);
			} catch (PortalException e5) {
				
				e5.printStackTrace();
			} catch (SystemException e5) {
				
				e5.printStackTrace();
			}
			try{
				ideaManagementFolderRoot = DLFolderLocalServiceUtil.getFolder(
						groupId, 0, MyConstants.DL_FOLDER_ROOT);
			} catch (Exception e) {
				//questa folder non esiste la creo
				try {
					ideaManagementFolderRoot=DLFolderLocalServiceUtil.addFolder(user.getUserId(),// userId,
							groupId,// groupId
							groupId,// repositoryId,
							false,// mountPoint,
							0,// parentFolderId, root
							MyConstants.DL_FOLDER_ROOT,// name,
							"",// description,
							serviceContext);
				} catch (PortalException | SystemException e2) {
					
					e2.printStackTrace();

				}// serviceContext)

}
			DLFolder folder = null;
			try {
				folder = DLFolderLocalServiceUtil.addFolder(user.getUserId(),// userId,
						groupId,// groupId
						groupId,// repositoryId,
						false,// mountPoint,
						ideaManagementFolderRoot.getFolderId(),// parentFolderId, root
						filename,// name,
						filename,// description,
						serviceContext);
			} catch (PortalException | SystemException e4) {
				
				e4.printStackTrace();

			}
			
			try {
				setPermission(resourceRequest, Long.toString(folder.getFolderId()), DLFolder.class.getName(),actionIdWiew,roles);
			} catch (SystemException | PortalException e3) {
				
				e3.printStackTrace();
			}
		
			FileEntry fileEntry=null;
			try{
				
				fileEntry=DLAppLocalServiceUtil.addFileEntry(user.getUserId(), groupId, folder.getFolderId(), filename, imgType, filename, "", "", thumb, serviceContext);
			} catch (PortalException | SystemException e) {
				System.out.println("Eccezione DL - file "+e.getMessage());
				e.printStackTrace();
			}
			
		 // imposto i permessi per il gruppo user					    
		try {
			setPermission(resourceRequest, Long.toString(fileEntry.getFileEntryId()), DLFileEntry.class.getName(),actionIdWiew, allRoles);
		} catch (SystemException  |PortalException e) {
			
			e.printStackTrace();
		} //roles);
		
		pathImg="/documents/"+groupId+"/"+folder.getFolderId()+"/"+filename;
		 
		}else if (view.equals("IdeasList")){
			
			String  stateSelection=resourceRequest.getParameter("stateSelection");
			String  stateEvaluation=resourceRequest.getParameter("stateEvaluation");
			String  stateSelected=resourceRequest.getParameter("stateSelected");
			String  stateRefinement=resourceRequest.getParameter("stateRefinement");	
			String  stateImplementation=resourceRequest.getParameter("stateImplementation");	
			String  stateMonitoring=resourceRequest.getParameter("stateMonitoring");
			String  stateRejected=resourceRequest.getParameter("stateRejected");
			
			Map<String, String> statiFiltri = new HashMap<String, String>();
			statiFiltri.put(MyConstants.IDEA_STATE_ELABORATION, stateSelection);
			statiFiltri.put(MyConstants.IDEA_STATE_EVALUATION, stateEvaluation);
			statiFiltri.put(MyConstants.IDEA_STATE_SELECTED, stateSelected);
			statiFiltri.put(MyConstants.IDEA_STATE_REFINEMENT,stateRefinement);
			statiFiltri.put(MyConstants.IDEA_STATE_IMPLEMENTATION, stateImplementation);
			statiFiltri.put(MyConstants.IDEA_STATE_MONITORING, stateMonitoring);
			statiFiltri.put(MyConstants.IDEA_STATE_REJECT, stateRejected);
			
			
			String  locale=resourceRequest.getParameter("localePerAssetVoc");
			String  pilot=resourceRequest.getParameter("pilot");
			
			Map<String, String> catFiltri = MyUtils.getHMFilterCategoryParam (resourceRequest,locale );
			
			boolean  isNeed=Boolean.valueOf(resourceRequest.getParameter("isNeed"));
			
			
			String  pers1=resourceRequest.getParameter("pers1");
			String  pers2=resourceRequest.getParameter("pers2");
			String  pers3=resourceRequest.getParameter("pers3");
			String  pers4=resourceRequest.getParameter("pers4");
			String  pers5=resourceRequest.getParameter("pers5");
			String  pers6=resourceRequest.getParameter("pers6");
			
			
			Map<String, String> persFiltri = new HashMap<String, String>();
			persFiltri.put("myIdeasCitizen", pers1);
			persFiltri.put("ideasReferredToMyAuthority", pers2);
			persFiltri.put("favoriteIdeas", pers3);
			persFiltri.put("onlyWithoutChallenge", pers4);
			persFiltri.put("onlyMyPilot", pers5);
			persFiltri.put("ideasReferredToMeAuthority", pers6);
			
			String  sortByDate=resourceRequest.getParameter("sortByDate");
			String  sortDateAsc=resourceRequest.getParameter("sortDateAsc");
			String  sortByRating=resourceRequest.getParameter("sortByRating");
			String  sortRatingLower=resourceRequest.getParameter("sortRatingLower");
			
			Map<String, String> sorting = new HashMap<String, String>();
			sorting.put("sortByDate", sortByDate);
			sorting.put("sortDateAsc", sortDateAsc);
			sorting.put("sortByRating", sortByRating);
			sorting.put("sortRatingLower", sortRatingLower);
			
			JSONArray jsonArlistaIdee = IdeasListUtils.getListaIdeeAjax( resourceRequest, pilot, isNeed, statiFiltri, catFiltri, persFiltri, sorting);
			
			
			PrintWriter writer = resourceResponse.getWriter();
			writer.write(jsonArlistaIdee.toString());
			
		}else if (view.equals("ChallengesList")){
			
			String  stateOpen=resourceRequest.getParameter("stateOpen");
			String  stateEvaluation=resourceRequest.getParameter("stateEvaluation");
			String  stateEvaluated=resourceRequest.getParameter("stateEvaluated");	
			
			Map<String, String> statiFiltri = new HashMap<String, String>();
			statiFiltri.put(MyConstants.CHALLENGE_STATE_OPEN, stateOpen);
			statiFiltri.put(MyConstants.CHALLENGE_STATE_EVALUATION, stateEvaluation);
			statiFiltri.put(MyConstants.CHALLENGE_STATE_EVALUATED,stateEvaluated);
			
			String  locale=resourceRequest.getParameter("localePerAssetVoc");
			String  pilot=resourceRequest.getParameter("pilot");
			
			Map<String, String> catFiltri = MyUtils.getHMFilterCategoryParam (resourceRequest,locale );
			
			String  pers1=resourceRequest.getParameter("pers1");
			String  pers2=resourceRequest.getParameter("pers2");
			String  pers3=resourceRequest.getParameter("pers3");
			String  pers4=resourceRequest.getParameter("pers4");
			String  pers5=resourceRequest.getParameter("pers5");
			
			Map<String, String> persFiltri = new HashMap<String, String>();
			persFiltri.put("activeChallenge", pers1);
			persFiltri.put("inactiveChallenge", pers2);
			persFiltri.put("myChallenges", pers3);
			persFiltri.put("onlyMyPilot", pers4);
			persFiltri.put("favoriteChallenges", pers5);
			
			String  sortByStartDate=resourceRequest.getParameter("sortByStartDate");
			String  sortStartDateAsc=resourceRequest.getParameter("sortStartDateAsc");
			String  sortByEndDate=resourceRequest.getParameter("sortByEndDate");
			String  sortEndDateAsc=resourceRequest.getParameter("sortEndDateAsc");
			String  sortByRating=resourceRequest.getParameter("sortByRating");
			String  sortRatingLower=resourceRequest.getParameter("sortRatingLower");
			
			
			
			Map<String, String> sorting = new HashMap<String, String>();
			sorting.put("sortByStartDate", sortByStartDate);
			sorting.put("sortStartDateAsc", sortStartDateAsc);
			sorting.put("sortByEndDate", sortByEndDate);
			sorting.put("sortEndDateAsc", sortEndDateAsc);
			sorting.put("sortByRating", sortByRating);
			sorting.put("sortRatingLower", sortRatingLower);
			
			JSONArray jsonArlistaIdee = ChallengesUtils.getListaGareAjax( resourceRequest, pilot, statiFiltri, catFiltri, persFiltri, sorting);
			
			
			PrintWriter writer = resourceResponse.getWriter();
			writer.write(jsonArlistaIdee.toString());
			
		}
}//serveResource

	
	/**
	 * @param actionRequest
	 * @param actionRresponse
	 * @throws PortletException
	 * @throws IOException
	 */
	public void uploadCase(ActionRequest actionRequest,	ActionResponse actionRresponse) throws PortletException,IOException {
		
		
		HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(actionRequest);
		if (!PortalUtil.isMultipartRequest(httpRequest)) {
			throw new IllegalArgumentException(
					"Request is not multipart, please 'multipart/form-data' enctype for your form.");
		}

		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);

		
		File f =uploadRequest.getFile("myfile");
		
		String fileName = uploadRequest.getFileName("myfile");
		InputStream rfis = uploadRequest.getFileAsStream("myfile");
		
		
		String token = uploadRequest.getParameter("token");

		File dummyFile = File.createTempFile("dummy-file", ".dummy");
		String tempdir = dummyFile.getParent();
		if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
			logger.info("tempdir " + tempdir);
		}
		dummyFile.delete();
		File folder = new File(tempdir + File.separator + token);
		folder.mkdir();

		try {

			String sourceFileName = fileName;
			File file = f;

			if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
				logger.info("Nome file:" + sourceFileName);
			}
			File newFile = null;
			newFile = new File(folder.getPath() + File.separator + sourceFileName);
			
			if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
				logger.info("New file name: " + newFile.getName());
				logger.info("New file path: " + newFile.getPath());
			}

			InputStream in = new BufferedInputStream(rfis);
			FileInputStream fis = new FileInputStream(file);
			FileOutputStream fos = new FileOutputStream(newFile);

			byte[] bytes_ = FileUtil.getBytes(in);
			int i = fis.read(bytes_);

			while (i != -1) {
				fos.write(bytes_, 0, i);
				i = fis.read(bytes_);
			}
			fis.close();
			fos.close();

			if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
				logger.info("File created: " + newFile.getName());
			}
			SessionMessages.add(actionRequest, "success");

		} catch (FileNotFoundException e) {
			System.out.println("File Not Found.");
			e.printStackTrace();
			SessionMessages.add(actionRequest, "error");
		} catch (NullPointerException e) {
			System.out.println("File Not Found");
			e.printStackTrace();
			SessionMessages.add(actionRequest, "error");
		} catch (IOException e1) {
			System.out.println("Error Reading The File.");
			SessionMessages.add(actionRequest, "error");
			e1.printStackTrace();
		}
	}
	
	
	
	
	/**
	 * @param actionRequest
	 * @param actionRresponse
	 * @throws PortletException
	 * @throws IOException
	 */
	public void deleteChallengeFile(ActionRequest actionRequest,ActionResponse actionRresponse) throws PortletException,IOException {
		Challenges challenges = new Challenges();
		challenges.deleteChallengeFile(actionRequest, actionRresponse);
	}
	
		
		

	/**
	 * @param actionRequest
	 * @param resourceId
	 * @param resourceType
	 * @param actionIdWiew
	 * @param roles
	 * @throws com.liferay.portal.kernel.exception.SystemException
	 * @throws PortalException
	 */
	private void setPermission(ResourceRequest actionRequest, String resourceId, String resourceType,String[] actionIdWiew, java.util.List<Role> roles) throws com.liferay.portal.kernel.exception.SystemException, PortalException {
		//String[] actionIdsOwner =  {ActionKeys.VIEW, ActionKeys.};
		String[] actionIdAllFile =  {ActionKeys.VIEW,ActionKeys.ADD_DISCUSSION,ActionKeys.DELETE,ActionKeys.DELETE_DISCUSSION,ActionKeys.PERMISSIONS,ActionKeys.UPDATE,ActionKeys.UPDATE_DISCUSSION};
		String[] actionIdAllFolder =  {ActionKeys.VIEW,
				ActionKeys.ACCESS,
				ActionKeys.ADD_DOCUMENT,
				ActionKeys.ADD_SHORTCUT,
				ActionKeys.ADD_SUBFOLDER,
				ActionKeys.DELETE,
				ActionKeys.PERMISSIONS,
				ActionKeys.UPDATE};
		
		User user = (User) actionRequest.getAttribute(WebKeys.USER);
		//List<Role> roles = RoleLocalServiceUtil.getRoles(com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS, com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS);
		Iterator<Role> rolesIt = roles.iterator();
		while(rolesIt.hasNext()){
			Role role = rolesIt.next();
			//ResourcePermissionLocalServiceUtil.setResourcePermissions(companyId, name, scope, primKey, roleId, actionIds)
			
			if(!role.getName().equals("Owner")){
				ResourcePermissionServiceUtil.setIndividualResourcePermissions(ParamUtil.getLong(actionRequest, "groupid"), //groupId
						   user.getCompanyId(), //companyId
						   resourceType, //name
						   resourceId, //primKey
						   role.getPrimaryKey(), //roleId
						   actionIdWiew);
			}
			
			
			final Role userRolsOwner = RoleLocalServiceUtil.getRole(user.getCompanyId(), RoleConstants.OWNER);
			
			if (resourceType.equals("com.liferay.portlet.documentlibrary.model.DLFileEntry")){
				ResourcePermissionServiceUtil.setIndividualResourcePermissions(ParamUtil.getLong(actionRequest, "groupid"), //groupId
						   user.getCompanyId(), //companyId
						   resourceType, //name
						   resourceId, //primKey
						   userRolsOwner.getPrimaryKey(), //roleId
						   actionIdAllFile);
			}else if (resourceType.equals("com.liferay.portlet.documentlibrary.model.DLFolder")){
				ResourcePermissionServiceUtil.setIndividualResourcePermissions(ParamUtil.getLong(actionRequest, "groupid"), //groupId
						   user.getCompanyId(), //companyId
						   resourceType, //name
						   resourceId, //primKey
						   userRolsOwner.getPrimaryKey(), //roleId
						   actionIdAllFolder);
			}
		}
		
		
	}
	
	/**
	 * @param ideaId
	 * @param UserId
	 * @return
	 * @throws PortalException
	 */
	public boolean removeFavouriteIdea(Long ideaId, Long UserId) throws  PortalException {
		boolean retVal = true;
		try {			
			CLSFavouriteIdeasLocalServiceUtil.deleteCLSFavouriteIdeas(new CLSFavouriteIdeasPK(ideaId, UserId));//deleteCLSFavouriteIdeas(favideaId);
			
		} catch (com.liferay.portal.kernel.exception.SystemException e) {
			
			retVal = false;
			e.printStackTrace();
		}

		
		return retVal;
	}
	
	
	
	
	/**
	 * @param favChallengeId
	 * @param userId
	 * @return
	 * @throws PortalException
	 */
	private boolean removeFavouriteChallenge(Long favChallengeId, Long userId) throws  PortalException {
		boolean retVal = true;
		try {
			CLSFavouriteChallengesPK clsFavouriteChallengesPK = new CLSFavouriteChallengesPK(favChallengeId, userId);
			CLSFavouriteChallengesLocalServiceUtil.deleteCLSFavouriteChallenges(clsFavouriteChallengesPK);
			
		} catch (com.liferay.portal.kernel.exception.SystemException e) {
			
			retVal = false;
			e.printStackTrace();
		}
		
		return retVal;
	}
	
	/**
	 * @param favChallengeId
	 * @param UserId
	 * @return
	 * @throws PortletException
	 */
	private boolean addFavouriteChallenge(Long favChallengeId, Long UserId) throws PortletException {
		boolean retVal = true;
		try {
			
			CLSFavouriteChallenges favChallenge = new CLSFavouriteChallengesImpl();
					
			favChallenge.setChallengeId(favChallengeId);
			favChallenge.setUserId(user.getUserId());			
			
			CLSFavouriteChallengesLocalServiceUtil.addCLSFavouriteChallenges(favChallenge);
			
			
			
		} catch (com.liferay.portal.kernel.exception.SystemException e) {
			
			retVal = false;
			e.printStackTrace();
			throw new PortletException(e.getMessage());
		}
		
		return retVal;
	}
	
	/**
	 * @param favChallengeId
	 * @param userId
	 * @return
	 * @throws PortalException
	 */
	private String getFavouriteChallenges(Long favChallengeId, Long userId) throws PortalException {		 	   
			
			List favChallenges = new ArrayList();
			
			String retVal = "";
			try {			
				// verifico se l'idea � tra i miei preferiti
				Criterion c_favIdea1 = RestrictionsFactoryUtil.eq("primaryKey.challengeId", favChallengeId);
				Criterion c_favIdea2 = RestrictionsFactoryUtil.eq("primaryKey.userId",userId);
				Criterion c_favIdea3 = RestrictionsFactoryUtil.and(c_favIdea1,c_favIdea2);
				
				DynamicQuery dqFavChallenge = DynamicQueryFactoryUtil.forClass(CLSFavouriteChallenges.class).add(c_favIdea3);

				favChallenges = CLSFavouriteChallengesLocalServiceUtil.dynamicQuery(dqFavChallenge);
				String json = new Gson().toJson(favChallenges );
				
				retVal = json;
				
			} catch (com.liferay.portal.kernel.exception.SystemException e) {
				
				e.printStackTrace();				
			}
			
			
			return retVal;
	    }
	

	 
	
	/**
	 * @param ideaId
	 * @param UserId
	 * @return
	 * @throws PortletException
	 */
	public boolean addFavouriteIdea(Long ideaId, Long UserId) throws PortletException {
		boolean retVal = true;
		try {
			
			CLSFavouriteIdeas favIdea = new CLSFavouriteIdeasImpl();
			favIdea.setIdeaID(ideaId);
			favIdea.setUserId(user.getUserId());			
			
			CLSFavouriteIdeasLocalServiceUtil.addCLSFavouriteIdeas(favIdea);
		} catch (com.liferay.portal.kernel.exception.SystemException e) {
			
			retVal = false;
			e.printStackTrace();
			throw new PortletException(e.getMessage());
		}
		
		return retVal;
	}
	
	/**
	 * @param ideaId
	 * @param UserId
	 * @return
	 * @throws PortalException
	 */
	public String getFavouriteIdeas(Long ideaId, Long UserId) throws PortalException {		 	   
			
			List l_favIdea = new ArrayList();
			
			String retVal = "";
			try {			
				// verifico se l'idea � tra i miei preferiti
				Criterion c_favIdea1 = RestrictionsFactoryUtil.eq("ideaID", ideaId);
				Criterion c_favIdea2 = RestrictionsFactoryUtil.eq("userId",UserId);
				Criterion c_favIdea3 = RestrictionsFactoryUtil.and(c_favIdea1,c_favIdea2);

				DynamicQuery dq_favIdea = DynamicQueryFactoryUtil.forClass(CLSFavouriteIdeas.class).add(c_favIdea3);
				l_favIdea = (CLSFavouriteIdeasLocalServiceUtil.dynamicQuery(dq_favIdea));
				
				String json = new Gson().toJson(l_favIdea );			
				
				retVal = json;
				
			} catch (com.liferay.portal.kernel.exception.SystemException e) {
				
				e.printStackTrace();				
			}
			
			
			return retVal;
	    }
	

	
	
	
	/* tasks*/
	
	/**
	 * @param actionRequest
	 * @param actionResponse
	 * @throws Exception
	 */
	public void deleteTasksEntry(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		long tasksEntryId = ParamUtil.getLong(actionRequest, "tasksEntryId");

		TasksEntryLocalServiceUtil.deleteTasksEntry(tasksEntryId);

		String redirect = ParamUtil.getString(actionRequest, "redirect");

		if (Validator.isNotNull(redirect)) {
			actionResponse.sendRedirect(redirect);
		}
		else {
			JSONObject jsonObject = JSONFactoryUtil.createJSONObject();

			jsonObject.put("success", Boolean.TRUE);

			HttpServletResponse response = PortalUtil.getHttpServletResponse(
				actionResponse);

			ServletResponseUtil.write(response, jsonObject.toString());
		}
	}


	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#processAction(javax.portlet.ActionRequest, javax.portlet.ActionResponse)
	 */
	public void processAction(	ActionRequest actionRequest, ActionResponse actionResponse)	throws IOException, PortletException {

		if (!isProcessActionRequest(actionRequest)) {
			return;
		}

		if (!callActionMethod(actionRequest, actionResponse)) {
			return;
		}

//		if (SessionErrors.isEmpty(actionRequest)) { //TODO
//			SessionMessages.add(actionRequest, "requestProcessed");
//		}

		String actionName = ParamUtil.getString(
			actionRequest, ActionRequest.ACTION_NAME);

		if (actionName.equals("updateTasksEntry")) {
			return;
		}

		String redirect = ParamUtil.getString(actionRequest, "redirect");

		if (Validator.isNotNull(redirect)) {
			actionResponse.sendRedirect(redirect);
		}
	}

	/**
	 * @param actionRequest
	 * @param actionResponse
	 * @throws Exception
	 */
	public void updateMessage(ActionRequest actionRequest, ActionResponse actionResponse)throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

		String cmd = ParamUtil.getString(actionRequest, Constants.CMD);

		long groupId = PortalUtil.getScopeGroupId(actionRequest);
		String className = ParamUtil.getString(actionRequest, "className");
		long classPK = ParamUtil.getLong(actionRequest, "classPK");
		long messageId = ParamUtil.getLong(actionRequest, "messageId");
		long threadId = ParamUtil.getLong(actionRequest, "threadId");
		long parentMessageId = ParamUtil.getLong(
			actionRequest, "parentMessageId");
		String subject = ParamUtil.getString(actionRequest, "subject");
		String body = ParamUtil.getString(actionRequest, "body");

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
			MBMessage.class.getName(), actionRequest);

		if (cmd.equals(Constants.DELETE)) {
			MBMessageServiceUtil.deleteDiscussionMessage(
				groupId, className, classPK, className, classPK,
				themeDisplay.getUserId(), messageId);
		}
		else if (messageId <= 0) {
			MBMessageServiceUtil.addDiscussionMessage(
				groupId, className, classPK, className, classPK,
				themeDisplay.getUserId(), threadId, parentMessageId, subject,
				body, serviceContext);
		}
		else {
			MBMessageServiceUtil.updateDiscussionMessage(
				className, classPK, className, classPK,
				themeDisplay.getUserId(), messageId, subject, body,
				serviceContext);
		}
	}

	
	/**
	 * @param actionRequest
	 * @param actionResponse
	 * @throws Exception
	 */
	public void updateTasksEntry(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		
		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		
		long tasksEntryId = ParamUtil.getLong(actionRequest, "tasksEntryId");
		
		long ideaId = ParamUtil.getLong(actionRequest, "ideaId");
		String tipoRequisito="";
		 tipoRequisito = ParamUtil.getString(actionRequest, "tipoRequisito");
		
		 long reqId = ParamUtil.getLong(actionRequest, "reqId");
		 
		String title = ParamUtil.getString(actionRequest, "title");
		int priority = ParamUtil.getInteger(actionRequest, "priority");
		long assigneeUserId = ParamUtil.getLong(
			actionRequest, "assigneeUserId");
		long resolverUserId = ParamUtil.getLong(
			actionRequest, "resolverUserId");

		int dueDateMonth = ParamUtil.getInteger(actionRequest, "dueDateMonth");
		int dueDateDay = ParamUtil.getInteger(actionRequest, "dueDateDay");
		int dueDateYear = ParamUtil.getInteger(actionRequest, "dueDateYear");
		int dueDateHour = ParamUtil.getInteger(actionRequest, "dueDateHour");
		int dueDateMinute = ParamUtil.getInteger(
			actionRequest, "dueDateMinute");
		int dueDateAmPm = ParamUtil.getInteger(actionRequest, "dueDateAmPm");

		if (dueDateAmPm == Calendar.PM) {
			dueDateHour += 12;
		}

		boolean addDueDate = ParamUtil.getBoolean(actionRequest, "addDueDate");
		int status = ParamUtil.getInteger(actionRequest, "status");

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
			TasksEntry.class.getName(), actionRequest);

		TasksEntry taskEntry = null;

		try {
			if (tasksEntryId <= 0) {
				taskEntry = TasksEntryServiceUtil.addTasksEntry(
					title, priority, assigneeUserId, dueDateMonth, dueDateDay,
					dueDateYear, dueDateHour, dueDateMinute, addDueDate,
					serviceContext);
			}
			else {
				taskEntry = TasksEntryServiceUtil.updateTasksEntry(
					tasksEntryId, title, priority, assigneeUserId,
					resolverUserId, dueDateMonth, dueDateDay, dueDateYear,
					dueDateHour, dueDateMinute, addDueDate, status,
					serviceContext);
			}
			
			
			String paginaIdee = IdeaManagementSystemProperties.getFriendlyUrlSuffixIdeas();
			
			String totalPath= "/"+paginaIdee+"/-/ideas_explorer_contest/"+String.valueOf(ideaId)+"/view";
			taskEntry.setIdeaLink(totalPath);
			taskEntry.setIdeaId(ideaId);
			taskEntry.setTipoRequisito(tipoRequisito);
			taskEntry.setStatoRequisito("assegnato");
			taskEntry.setRequisitoId(reqId);
			TasksEntryLocalServiceUtil.updateTasksEntry(taskEntry);
			
			
			CLSRequisiti requisito=CLSRequisitiLocalServiceUtil.getCLSRequisiti(reqId);
			requisito.setTaskId(taskEntry.getTasksEntryId());
			CLSRequisitiLocalServiceUtil.updateCLSRequisiti(requisito);
			
				Layout layout = themeDisplay.getLayout();
			String portletName = (String)actionRequest.getAttribute(WebKeys.PORTLET_ID);
			PortletURL portletURL = PortletURLFactoryUtil.create(
				actionRequest,portletName , layout.getPlid(),
				PortletRequest.RENDER_PHASE);

			portletURL.setWindowState(LiferayWindowState.EXCLUSIVE);

			portletURL.setParameter("jspPage", "/html/tasks/view_task.jsp");
			portletURL.setParameter("tasksEntryId", String.valueOf(taskEntry.getTasksEntryId()));
			
			actionResponse.sendRedirect(portletURL.toString());
		}
		catch (AssetTagException ate) {
			actionResponse.setRenderParameter(
				"mvcPath", "/tasks/edit_task.jsp");

			actionResponse.setRenderParameters(actionRequest.getParameterMap());

			SessionErrors.add(actionRequest, ate.getClass(), ate);
		}
	}

	/**
	 * @param actionRequest
	 * @param actionResponse
	 * @throws Exception
	 */
	public void updateTasksEntryStatus(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		long tasksEntryId = ParamUtil.getLong(actionRequest, "tasksEntryId");

		long resolverUserId = ParamUtil.getLong(
			actionRequest, "resolverUserId");
		int status = ParamUtil.getInteger(actionRequest, "status");

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
			TasksEntry.class.getName(), actionRequest);

		TasksEntryLocalServiceUtil.updateTasksEntryStatus(
			tasksEntryId, resolverUserId, status, serviceContext);
	}
	

	
 /**
 * Creation and management of the criteria for selection of Ideas on the Challenge
 * @param actionRequest
 * @param actionResponse
 */
public void manageCriteria (ActionRequest actionRequest, ActionResponse actionResponse){
	 
		Challenges challenges = new Challenges();
		challenges.manageCriteria(actionRequest, actionResponse);
	 
 }

/**
* Evaluation of the Ideas on the Challenge
* @param actionRequest
* @param actionResponse
*/
public void evalIdea (ActionRequest actionRequest, ActionResponse actionResponse){
	 
	Challenges challenges = new Challenges();
	challenges.evalIdea(actionRequest, actionResponse);
	 
}
	
/**Open the evaluation process and send all notifications. 
 * @param actionRequest
 * @param actionResponse
 */
public void openEvaluation (ActionRequest actionRequest, ActionResponse actionResponse){
	 
	Challenges challenges = new Challenges();
	challenges.openEvaluation(actionRequest, actionResponse);
 
}

/**Close the evaluation process and send all notifications. 
 * @param actionRequest
 * @param actionResponse
 */
public void closeEvaluation (ActionRequest actionRequest, ActionResponse actionResponse){
	 
	Challenges challenges = new Challenges();
	challenges.closeEvaluation(actionRequest, actionResponse);
 
}


/** The company take charge of the rejected idea 
 * @param actionRequest
 * @param actionResponse
 */
public void takeCharge (ActionRequest actionRequest,ActionResponse actionResponse) {
	
	Ideas idea = new Ideas();
	idea.takeCharge(actionRequest, actionResponse);
	
}

/** add coworkwers to idea (outside the edit)
 * @param actionRequest
 * @param actionResponse
 * @throws SystemException 
 */
public void addCoworkers (ActionRequest actionRequest,ActionResponse actionResponse) throws SystemException {
	
	Ideas idea = new Ideas();
	idea.addCoworkers(actionRequest, actionResponse);
	
}





		
}
