package it.eng.rspa.ideas.utils;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSChallengeUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSIdeaUtil;
import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;

import java.awt.Container;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.ResourceRequest;

import org.apache.commons.lang3.StringUtils;
import org.fusesource.hawtbuf.ByteArrayOutputStream;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionList;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Company;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ClassNameLocalServiceUtil;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.asset.model.AssetCategory;
import com.liferay.portlet.asset.model.AssetTag;
import com.liferay.portlet.asset.model.AssetTagStats;
import com.liferay.portlet.asset.model.AssetVocabulary;
import com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetTagLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetTagStatsLocalServiceUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil;
import com.liferay.portlet.expando.model.ExpandoValue;
import com.liferay.portlet.expando.service.ExpandoColumnLocalServiceUtil;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;
import com.liferay.portlet.messageboards.model.MBMessage;
import com.liferay.portlet.messageboards.service.MBMessageLocalServiceUtil;

public class MyUtils {
	
	
	
	public List<CLSIdea> getIdeasByChallengeId(long challengeId) throws SystemException{
			return CLSIdeaUtil.findByIdeaChallengeId(challengeId);
	}

	public List<CLSIdea> getIdeasByUserId(long userId) throws SystemException{
		return CLSIdeaUtil.findByIdeaUserId(userId);
	}
	
	public List<CLSIdea> noauthSearchIdeaByTileAndDescription(long challengeId, String ideaTitle, String ideaDescription) throws SystemException{

		List<CLSIdea> ideas = CLSIdeaUtil.findByIdeaChallengeId(challengeId);
		ArrayList<CLSIdea> results = new ArrayList<CLSIdea>();
		for(CLSIdea idea : ideas){
			if( StringUtils.containsIgnoreCase(idea.getIdeaTitle(), ideaTitle)
					|| 
					StringUtils.containsIgnoreCase(idea.getIdeaDescription(), ideaDescription) ){
					results.add(idea);
				}
			
		}

		
		return results; 
	}

	
	

	
	public List<CLSIdea> searchIdeaByTileAndDescription(long challengeId, String ideaTitle, String ideaDescription) throws SystemException{
		List<CLSIdea> ideas = CLSIdeaUtil.findByIdeaChallengeId(challengeId);
		ArrayList<CLSIdea> results = new ArrayList<CLSIdea>();
		for(CLSIdea idea : ideas){
			if( (!ideaTitle.trim().equals("")) && (idea.getIdeaTitle().contains(ideaTitle))
				|| 
				(!ideaDescription.trim().equals("")) && idea.getIdeaDescription().contains(ideaDescription)){
				results.add(idea);
			}
		}
		return results; 
	}
	
	
	


	public void deleteIdeaFile(long fileEntryId) throws PortletException, IOException {

		try {
			DLFileEntryLocalServiceUtil.deleteDLFileEntry(fileEntryId);
		} catch (PortalException e) {
			
			e.printStackTrace();
		} catch (com.liferay.portal.kernel.exception.SystemException e) {
			
			e.printStackTrace();
		}
		
	}
	
	
	

	public ArrayList<JSONObject> getIdeaImages(long ideaId) throws SystemException, PortalException{
		ArrayList<JSONObject> myJSons = new ArrayList<JSONObject>();
		
		Set<String> mimeTypeImages = new HashSet<String>(Arrays.asList(
			     new String[] {
			    		 "image/bmp",
			    		 "image/png",
			    		 "image/cis-cod",
			    		 "image/gif",
			    		 "image/ief",
			    		 "image/jpeg",
			    		 "image/pipeg",
			    		 "image/svg+xml",
			    		 "image/tiff",
			    		 "image/tiff",
			    		 "image/x-cmu-raster",
			    		 "image/x-cmx",
			    		 "image/x-icon",
			    		 "image/x-portable-anymap",
			    		 "image/x-portable-bitmap",
			    		 "image/x-portable-graymap",
			    		 "image/x-portable-pixmap",
			    		 "image/x-rgb",
			    		 "image/x-xbitmap",
			    		 "image/x-xpixmap",
			    		 "image/x-xwindowdump"
			    }
		));
		
		if(ideaId > 0){
			CLSIdea idea = CLSIdeaLocalServiceUtil.getCLSIdea(ideaId);
			Long folderId = idea.getIdFolder();
			DLFolder folder = DLFolderLocalServiceUtil.fetchDLFolder(folderId);
			Integer count = DLFileEntryLocalServiceUtil.getFileEntriesCount(folder.getGroupId(), folderId);
			OrderByComparator obc = OrderByComparatorFactoryUtil.create("DLFileEntry", "title", true);
			List<DLFileEntry> files = DLFileEntryLocalServiceUtil.getFileEntries(folder.getGroupId(), folderId.longValue(), 0, count, obc);
			Iterator<DLFileEntry> filesIt = files.iterator();
			if (filesIt!=null){
				while (filesIt.hasNext()){	
					DLFileEntry fileCor = filesIt.next();

					long groupId = fileCor.getGroupId();
					if(mimeTypeImages.contains(fileCor.getMimeType()) ){
						JSONObject myJSon = JSONFactoryUtil.createJSONObject();
						//String fileCorURL = "http://" + hostName+ ":" + port + 
						//		"" + 
						String fileCorURL=	 "/documents/" + 
					 			groupId + 
					 			 StringPool.SLASH + 
					 			 fileCor.getUuid();
						myJSon.put("image", fileCorURL);	
						myJSons.add(myJSon);
					}
				}
			}
		}

		return myJSons;
	}
	
	
	
	
	//usato nell'indice di vitalita, si potrebbe eliminare
	@SuppressWarnings("unchecked")
	public ArrayList<JSONObject> getUsersCommentsOnIdea(long ideaId) throws SystemException, PortalException{
		ArrayList<JSONObject> myJSons = new ArrayList<JSONObject>();
		
		Random randomGenerator = new Random();
		
		
		//Date now  = new Date();
		
		//ottengo gli utenti che hanno rilasciato commenti su delle idee
		
		DynamicQuery comment_query = MBMessageLocalServiceUtil.dynamicQuery(); // DynamicQueryFactoryUtil.forClass(MBMessage.class);
		ProjectionList projectionList_comment = ProjectionFactoryUtil.projectionList();
		projectionList_comment.add(ProjectionFactoryUtil.groupProperty("userId"));
		comment_query.setProjection(projectionList_comment);
		comment_query.add(RestrictionsFactoryUtil.eq("classNameId", ClassNameLocalServiceUtil.getClassNameId(CLSIdea.class.getName())));
		comment_query.add(RestrictionsFactoryUtil.eq("classPK", ideaId));
		//comment_query.add(RestrictionsFactoryUtil.between("createDate", new Date(timeAgo), now));
		List<Long> requestList_comemnt = MBMessageLocalServiceUtil.dynamicQuery(comment_query);
		
		for (int n=0; n<requestList_comemnt.size(); n++){

			try{
				User user_comment = UserLocalServiceUtil.getUser(requestList_comemnt.get(n));
				DynamicQuery mbMessageQuery = MBMessageLocalServiceUtil.dynamicQuery(); //DynamicQueryFactoryUtil.forClass(MBMessage.class);
				mbMessageQuery.add(RestrictionsFactoryUtil.eq("classNameId", ClassNameLocalServiceUtil.getClassNameId(CLSIdea.class.getName())));
				mbMessageQuery.add(RestrictionsFactoryUtil.eq("userId", user_comment.getUserId()));		//	mbMessageQuery.add(RestrictionsFactoryUtil.between("createDate", new Date(timeAgo), now));
				List<MBMessage> comments = MBMessageLocalServiceUtil.dynamicQuery(mbMessageQuery);

				for (Iterator<MBMessage> iterator = comments.iterator(); iterator.hasNext();) {
					
					MBMessage mbMessage = (MBMessage) iterator.next();
					if (mbMessage.getParentMessageId() != 0){
					JSONObject myJSon = JSONFactoryUtil.createJSONObject();
					String commento = mbMessage.getBody();
					myJSon.put("testo", commento);	
					myJSon.put("userId", mbMessage.getUserId());
					myJSon.put("userReputazione", randomGenerator.nextInt(1000));//TODO ??
					myJSons.add(myJSon);
					}
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
			}
			
		}
				
		return myJSons;
	}
	
	
	//metodo che legge i file nel primo livello della DL, utile solo per il mock
		@Deprecated
		protected JSONObject getFileFromDL(String title) throws SystemException, PortalException{
			
			 List<DLFileEntry> listaFile = DLFileEntryLocalServiceUtil.getFileEntries(0, DLFileEntryLocalServiceUtil.getFileEntriesCount());
			 DLFileEntry fileAtom = null;
			 for (int i = 0; i <listaFile.size(); i++){
				 
				 DLFileEntry file = listaFile.get(i);
				 
				 if (file.getTitle().equals(title))	{	
					  fileAtom =  file;
				 }
			 }
			 		 
			 InputStream atom = DLFileEntryLocalServiceUtil.getFileAsStream(fileAtom.getUserId(), fileAtom.getFileEntryId(),fileAtom.getVersion() );
			 		 
			 BufferedReader streamReader;
			 StringBuilder responseStrBuilder = new StringBuilder();
			 String inputStr;
			 
			try {
				streamReader = new BufferedReader(new InputStreamReader(atom, "UTF-8"));
				 while ((inputStr = streamReader.readLine()) != null)
				     responseStrBuilder.append(inputStr);
				
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} 
			
			
			JSONObject myJSon = JSONFactoryUtil.createJSONObject();
			myJSon.put("file", responseStrBuilder.toString());
			return myJSon;
		}
		
		
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////     CHALLENGE
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		public int getChallengesCount(){
			try {
				return CLSChallengeLocalServiceUtil.getCLSChallengesCount();
			} catch (SystemException e) {
				e.printStackTrace();
				return -1;
			}
		}
		
		

		public List<CLSChallenge> getChallengesByUserId(long userId) throws SystemException{
			return CLSChallengeUtil.findByChallengeUserId(userId);
		}
		
		public void deleteChallengeFile(long fileEntryId) throws PortletException,
				IOException {
			System.out.println("fileEntryId: " + fileEntryId);
			try { DLFileEntryLocalServiceUtil.deleteDLFileEntry(fileEntryId); } 
			catch (Exception e) { e.printStackTrace(); }

		}
		
		public ArrayList<JSONObject> getChallengeImages(long challengeId) throws SystemException, PortalException{
			ArrayList<JSONObject> myJSons = new ArrayList<JSONObject>();
			
			Set<String> mimeTypeImages = new HashSet<String>(Arrays.asList(
				     new String[] {
				    		 "image/bmp",
				    		 "image/png",
				    		 "image/cis-cod",
				    		 "image/gif",
				    		 "image/ief",
				    		 "image/jpeg",
				    		 "image/pipeg",
				    		 "image/svg+xml",
				    		 "image/tiff",
				    		 "image/tiff",
				    		 "image/x-cmu-raster",
				    		 "image/x-cmx",
				    		 "image/x-icon",
				    		 "image/x-portable-anymap",
				    		 "image/x-portable-bitmap",
				    		 "image/x-portable-graymap",
				    		 "image/x-portable-pixmap",
				    		 "image/x-rgb",
				    		 "image/x-xbitmap",
				    		 "image/x-xpixmap",
				    		 "image/x-xwindowdump"
				    }
			));
			
			if(challengeId > 0){
				CLSChallenge challenge = CLSChallengeLocalServiceUtil.getCLSChallenge(challengeId);
				Long folderId = challenge.getIdFolder();
				DLFolder folder = DLFolderLocalServiceUtil.fetchDLFolder(folderId);
				Integer count = DLFileEntryLocalServiceUtil.getFileEntriesCount(folder.getGroupId(), folderId);
				OrderByComparator obc = OrderByComparatorFactoryUtil.create("DLFileEntry", "title", true);
				List<DLFileEntry> files = DLFileEntryLocalServiceUtil.getFileEntries(folder.getGroupId(), folderId.longValue(), 0, count, obc);
				Iterator<DLFileEntry> filesIt = files.iterator();
				if (filesIt!=null){
					while (filesIt.hasNext()){	
						DLFileEntry fileCor = filesIt.next();
						long groupId = fileCor.getGroupId();
						if(mimeTypeImages.contains(fileCor.getMimeType()) ){
																			
							JSONObject myJSon = JSONFactoryUtil.createJSONObject();
							//String fileCorURL = "http://" + hostName+ ":" + port + 
							//		"" + 
							String fileCorURL=		 "/documents/" + 
						 			groupId + 
						 			 StringPool.SLASH + 
						 			 fileCor.getUuid();
							myJSon.put("image", fileCorURL);	
							myJSons.add(myJSon);
						}
					}
				}
			}

			return myJSons;
		}
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////     FILTRAGGIO PER PILOT
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
public static List<CLSIdea> getListaIdeeFiltrataPerPilotLingue (List<CLSIdea> listaI, User currentUser, String pilot){
			
			List<CLSIdea> listaF = new ArrayList<CLSIdea>();
			
			
			String pilotCorrente ="";
			
			if (currentUser == null ){
				
				pilotCorrente = pilot;
				
			}else{
				try {
					String[] pilotUtente = (String[]) currentUser.getExpandoBridge().getAttribute(MyConstants.CUSTOMFIELD_PILOT);
					pilotCorrente =pilotUtente[0];
					
				} catch (Exception e) {
					
					e.printStackTrace();
					return listaF;
					
				}
			}
			
			
			
			for (int i = 0; i<listaI.size(); i++){//scorro la lista iniziale
				
				CLSIdea idea = listaI.get(i); //mi piglio l'idea corrente
			

				
				String pilotEnte = getPilotByIdea(idea);
				
				
				
				if (pilotEnte.equalsIgnoreCase(pilotCorrente)){
					listaF.add(idea);
				}else{
					
					//se l'idea e' in una lingua che l'utente (loggato) conosce
					
					if (currentUser != null ){
						//attenzione se fai una idea per un pilot che non sia il tuo in una lingua che non conosci, non te la visualizza
						String[] lingueUtente = (String[]) currentUser.getExpandoBridge().getAttribute(MyConstants.CUSTOMFIELD_LANGUAGES);
					
						for(String linguaUt: lingueUtente) {
							
							if (lingueUguali(linguaUt,idea.getLanguage())){
								listaF.add(idea);
								break;
							}		
						}
					}
				}
			}
			
			
			return listaF;
		}//metodo	
		


/**
 * @param listaI
 * @param currentUser
 * @param pilot
 * @return
 */
public static List<CLSChallenge> getListaGareFiltrataPerPilotLingue (List<CLSChallenge> listaI, User currentUser, String pilot){
			
			List<CLSChallenge> listaF = new ArrayList<CLSChallenge>();
			
			
			String pilotCorrente ="";
			
			if (currentUser == null ){
				
				pilotCorrente = pilot;
				
			}else{
				
				try {
				
				String[] pilotUtente = (String[]) currentUser.getExpandoBridge().getAttribute(MyConstants.CUSTOMFIELD_PILOT);
				pilotCorrente =pilotUtente[0];
				
				} catch (Exception e) {
					
					e.printStackTrace();
					return listaF ;
					
				}
				
			}
			
			
			for (int i = 0; i<listaI.size(); i++){
				
				CLSChallenge gara = listaI.get(i); //mi piglio la gara corrente
				
				String pilotGara = getPilotByChallenge(gara);
				
				if (pilotGara.equalsIgnoreCase(pilotCorrente)){
					listaF.add(gara);
				}else{
					
					//se la gara e' in una lingua che l'utente (loggato) conosce
					
					if (currentUser != null ){
						
						String[] lingueUtente = (String[]) currentUser.getExpandoBridge().getAttribute(MyConstants.CUSTOMFIELD_LANGUAGES);
					
						for(String linguaUt: lingueUtente) {
							
							if (lingueUguali(linguaUt,gara.getLanguage())){
								listaF.add(gara);
								break;
							}		
						}
					}
				}	
			}
			
		
			return listaF;
		}//metodo
		




 /**
 * @param currentUser
 * @param users
 * @return
 */
public static List<User> getListaUtenticonPilotUguali (User currentUser, List<User> users){
	
	List<User> listaF = new ArrayList<User>();
	
		String[] pilotUtenteCorrente = (String[]) currentUser.getExpandoBridge().getAttribute(MyConstants.CUSTOMFIELD_PILOT);
	
		for (User user:users){
			
			String[] pilotUtente = (String[]) user.getExpandoBridge().getAttribute(MyConstants.CUSTOMFIELD_PILOT);
		
			
			if ((pilotUtenteCorrente.length > 0) && (pilotUtente.length > 0)){
				
				if (pilotUtente[0].equalsIgnoreCase(pilotUtenteCorrente[0]))
					listaF.add(user);
			
			}
		}
	return listaF;
}
 
 
/**
* @param currentUser
* @param users
* @return
*/
public static List<Organization> getListaOrganizationsconPilotUgualeByUser (User currentUser, List<Organization> organizations){
	
	List<Organization> listaF = new ArrayList<Organization>();
	
	
	String pilotUtenteCorrente = getPilotByUser(currentUser);
	
		for (Organization org:organizations){

			
			String pilotOrganization = getPilotByOrganization(org);
		
			
			if (Validator.isNotNull(pilotUtenteCorrente) && !pilotUtenteCorrente.equals("")){
				
					if (pilotUtenteCorrente.equalsIgnoreCase(pilotOrganization))
					 listaF.add(org);
			
			}
		}
	return listaF;
}



 
 /**
 * @param currentUser
 * @param users
 * @return
 */
public static List<User> getListaUtentiIMSSimpleconPilotUguali (User currentUser, List<User> users){
	
	//utilizzata per mostrare i collaboratori
	
	List<User> listaF = new ArrayList<User>();
		for (User user:users){

			String[] pilotUtenteCorrente = (String[]) currentUser.getExpandoBridge().getAttribute(MyConstants.CUSTOMFIELD_PILOT);
			String[] pilotUtente = (String[]) user.getExpandoBridge().getAttribute(MyConstants.CUSTOMFIELD_PILOT);
			
			
			if ((pilotUtenteCorrente.length > 0) && (pilotUtente.length > 0)  && user.isActive()){
				
				boolean isIMSSimpleUser = isIMSSimpleUser(user);
				
				//rimuovo l'utente stesso
				if (currentUser.getUserId() == user.getUserId())
					isIMSSimpleUser = false;	
				
			
				if (pilotUtente[0].equalsIgnoreCase(pilotUtenteCorrente[0]) && isIMSSimpleUser)
					listaF.add(user);
			
			}
		}
	return listaF;
} 
		
		
static boolean lingueUguali (String linguaUtente, String linguaIdea){
			
			Map<String, String> lingue = new HashMap<String, String>();
			lingue.put("Italian", "it");
			lingue.put("Spanish", "es");
			lingue.put("Finnish", "fi");
			lingue.put("Serbian", "sr");
			lingue.put("SerbianLatin", "sr");
			lingue.put("English", "en");
			
			String val = lingue.get(linguaUtente);		
			if (val.equalsIgnoreCase(linguaIdea))
				return true;
			
			
			return false;
		}


/**
 * @param acronimo
 * @return
 */
public static String getFullLanguageNameByAcronym (String acronimo){
	
	Map<String, String> lingue = new HashMap<String, String>();
	
	lingue.put("it", "italian");
	lingue.put("es", "spanish");
	lingue.put("fi", "finnish");
	lingue.put("sr", "serbian");
	lingue.put("en", "english");
	
	String val = lingue.get(acronimo);		
	
	
	return val;
}


/**
 * @param acronimo
 * @param locale
 * @return
 */
public static String getFullLanguageNamei18nByAcronym (String acronimo, Locale locale){
	
	String languageName = "";
	
	String language = locale.getLanguage();
	String country = locale.getCountry();
	ResourceBundle res = ResourceBundle.getBundle("Language", new Locale(language, country));
	
	String italian= res.getString("ims.italian");
	String spanish= res.getString("ims.spanish");
	String finnish= res.getString("ims.finnish");
	String serbian= res.getString("ims.serbian");
	String english= res.getString("ims.english");

	try {
		italian = new String(italian.getBytes("ISO-8859-1"), "UTF-8");
		spanish = new String(spanish.getBytes("ISO-8859-1"), "UTF-8");
		finnish = new String(finnish.getBytes("ISO-8859-1"), "UTF-8");
		serbian = new String(serbian.getBytes("ISO-8859-1"), "UTF-8");
		english = new String(english.getBytes("ISO-8859-1"), "UTF-8");

	} catch (UnsupportedEncodingException e) {
		
		e.printStackTrace();
		
	}
	
	HashMap<String, String> lingue = new HashMap<String,String>();
	lingue.put("it", italian);
	lingue.put("es", spanish);
	lingue.put("fi", finnish);
	lingue.put("sr", serbian);
	lingue.put("en", english);
	
	languageName = lingue.get(acronimo);		
	
	
	return languageName;
}


public static String getIdeaStatusI18n (String ideaStatus, Locale locale) {
	
	String statoIdea = "";
	
	String language = locale.getLanguage();
	String country = locale.getCountry();
	ResourceBundle res = ResourceBundle.getBundle("Language", new Locale(language, country));
		
	HashMap<String, String> stati = new HashMap<String,String>();
	
	String elaborazione= res.getString("ims.selection");
	String valutazione= res.getString("ims.evaluation");
	String selezionata= res.getString("ims.selected");
	String raffinamento= res.getString("ims.refinement");
	String realizzazione= res.getString("ims.implementation");
	String monitoraggio= res.getString("ims.monitoring");
	String completata= res.getString("ims.completed");
	String rifiutata= res.getString("ims.reject");
	
	try {
		elaborazione = new String(elaborazione.getBytes("ISO-8859-1"), "UTF-8");
		valutazione = new String(valutazione.getBytes("ISO-8859-1"), "UTF-8");
		selezionata = new String(selezionata.getBytes("ISO-8859-1"), "UTF-8");
		raffinamento = new String(raffinamento.getBytes("ISO-8859-1"), "UTF-8");
		realizzazione = new String(realizzazione.getBytes("ISO-8859-1"), "UTF-8");
		monitoraggio = new String(monitoraggio.getBytes("ISO-8859-1"), "UTF-8");
		completata = new String(completata.getBytes("ISO-8859-1"), "UTF-8");
		rifiutata = new String(rifiutata.getBytes("ISO-8859-1"), "UTF-8");

	} catch (UnsupportedEncodingException e) {
		
		e.printStackTrace();
		return statoIdea;
		
	}	
		
		
	stati.put(MyConstants.IDEA_STATE_MONITORING, monitoraggio);
	stati.put(MyConstants.IDEA_STATE_IMPLEMENTATION, realizzazione);
	stati.put(MyConstants.IDEA_STATE_REFINEMENT, raffinamento);
	stati.put(MyConstants.IDEA_STATE_SELECTED, selezionata);
	stati.put(MyConstants.IDEA_STATE_EVALUATION, valutazione);
	stati.put(MyConstants.IDEA_STATE_ELABORATION, elaborazione);
	stati.put(MyConstants.IDEA_STATE_COMPLETED, completata);
	stati.put(MyConstants.IDEA_STATE_REJECT, rifiutata);


	 statoIdea =stati.get(ideaStatus);
	 
	 return statoIdea;
	
}


		
	/**
	 * @param userId
	 * @return
	 */
	public static Long getCCUserIdbyUserId (long userId){
		
		User user = null;
		try {
			user = UserLocalServiceUtil.getUserById(userId);
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return null;
		}
		
		long CCUserID =  Long.parseLong(user.getExpandoBridge().getAttribute(MyConstants.CUSTOMFIELD_CCUSERID).toString());	
		
		return CCUserID;
	}
		


private static final String USEREMAIL = IdeaManagementSystemProperties.getProperty("basicAuthUser");

//controllo che le API siano usate solo dall'utente abilitato, settato nel properties
public static boolean checkWsCredentials (String email){
	
	
	if (email.equalsIgnoreCase(USEREMAIL))
		return true;
	
	
	
	return false;
	
}


	/**
	 * @param u
	 * @return
	 */
	public static String getPilotByUser(User u){
		String[] pilotEnte = (String[]) u.getExpandoBridge().getAttribute(MyConstants.CUSTOMFIELD_PILOT);
		
		return pilotEnte[0];
	}

	/**
	 * @param userId
	 * @return
	 */
	public static String getPilotByUserId (long userId){
		
		User utente = null;
		try { utente = UserLocalServiceUtil.getUserById(userId); } 
		catch (PortalException | SystemException e) {
			e.printStackTrace();
			return "";
		}
		
		return getPilotByUser(utente);
	}

	/**
	 * @param idea
	 * @return
	 */
	public static String getPilotByIdea(CLSIdea idea){
		
		long userId = idea.getMunicipalityId();
		long organizationId = idea.getMunicipalityOrganizationId();
		
		if (userId > 0)
			return getPilotByUserId(userId);
		else
			return getPilotByOrganizationId(organizationId);
			
	}

	/**
	 * @param ideaId
	 * @return
	 */
	public static String getPilotByIdeaId (long ideaId){
	
		CLSIdea idea = null;
		try {
			idea = CLSIdeaLocalServiceUtil.getIdeasByIdeaId(ideaId);
		} catch (SystemException e1) {
			
			e1.printStackTrace();
			return null;
		}
		
		return getPilotByIdea(idea);
		
	}
	
	/**
	 * @param gara
	 * @return
	 */
	public static String getPilotByChallenge(CLSChallenge gara){
		
		long userId = gara.getUserId();
		return getPilotByUserId(userId);
		
	}
	
	/**
	 * @param challengeId
	 * @return
	 */
	public static String getPilotByChallengeId (long challengeId){
		
		CLSChallenge gara = null;
		try { gara = CLSChallengeLocalServiceUtil.getCLSChallenge(challengeId); } 
		catch (Exception e1) {
			e1.printStackTrace();
			return null;
		}
		
		return getPilotByChallenge(gara);
	}


	/**
	 * @param organization
	 * @return
	 */
	public static String getPilotByOrganization (Organization organization){
		
		String pilota="";
		
		try{
		
		String[] pilotOrganizationEnte = (String[]) organization.getExpandoBridge().getAttribute(MyConstants.CUSTOMFIELD_PILOT);
		 pilota = pilotOrganizationEnte[0];
		} catch (Exception e) {
			
			if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
			
				System.out.println("Error on MyUtils.getPilotByOrganization: "+ e.getMessage());
			
			}
			return "";
		}
		
		return pilota;
		
	}
	
	
	/**
	 * @param organizationId
	 * @return
	 */
	public static String getPilotByOrganizationId (long organizationId){
		
		Organization org = null;
		try {
			 org = OrganizationLocalServiceUtil.getOrganization(organizationId);
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
		}

		return getPilotByOrganization(org);
	}
	
	
	/**
	 * @param currentUser
	 * @return
	 */
	public static boolean isCitizen (User currentUser){
		
		if ( Validator.isNull(currentUser))
			return false;
		

		boolean isCitizen=false;

		try {
			for (Role role: currentUser.getRoles()){ 
				
			  	if(role.getName().equalsIgnoreCase(MyConstants.ROLE_CITIZEN))
			  		isCitizen=true;
			  			
			}
		} catch (SystemException e) {
			
			e.printStackTrace();
			return false;
		}
		
		return isCitizen;
	}
	
	/**
	 * @param currentUser
	 * @return
	 */
	public static boolean isAcademy (User currentUser){
		
		if ( Validator.isNull(currentUser))
			return false;
		
		
		boolean isAcademy=false;

		
		try {
			for (Role role: currentUser.getRoles()){ 
				
			  	if(role.getName().equalsIgnoreCase(MyConstants.ROLE_ACADEMY))
			  		isAcademy=true;
			  			
			}
		} catch (SystemException e) {
			
			e.printStackTrace();
			return false;
		}
		
		return isAcademy;
	}
	
	/**
	 * @param currentUser
	 * @return
	 */
	public static boolean isBusiness (User currentUser){
		
		if ( Validator.isNull(currentUser))
			return false;
		

		boolean isBusiness=false;

		
		try {
			for (Role role: currentUser.getRoles()){ 
				
			  	if(role.getName().equalsIgnoreCase(MyConstants.ROLE_BUSINESS))
			  		isBusiness=true;
			  			
			}
		} catch (SystemException e) {
			
			e.printStackTrace();
			return false;
		}
		
		return isBusiness;
	}
	
	/**
	 * @param currentUser
	 * @return
	 */
	public static boolean isEntrepreneur (User currentUser){
		
		if ( Validator.isNull(currentUser))
			return false;
		

		boolean isEntrepreneur=false;

		try {
			for (Role role: currentUser.getRoles()){ 
				
			  	if(role.getName().equalsIgnoreCase(MyConstants.ROLE_ENTREPRENEUR))
			  		isEntrepreneur=true;
			  			
			}
		} catch (SystemException e) {
			
			e.printStackTrace();
			return false;
		}
		
		return isEntrepreneur;
	}

	
	
	/**
	 * @param currentUser
	 * @return
	 */
	public static boolean isIMSSimpleUser (User currentUser){
		
		if (Validator.isNull(currentUser) ||  !currentUser.isActive() )
			return false;
		

		boolean isAcademy=false;
		boolean isBusiness=false;
		boolean isCitizen=false;
		boolean isEntrepreneur=false;

		
		try {
			for (Role role: currentUser.getRoles()){ 
				
			  	if(role.getName().equalsIgnoreCase(MyConstants.ROLE_ACADEMY)){
			  		isAcademy=true;
			  		break;
			  	}else if(role.getName().equalsIgnoreCase(MyConstants.ROLE_BUSINESS)){
			  		isBusiness=true;
			  		break;
			  	}else if(role.getName().equalsIgnoreCase(MyConstants.ROLE_CITIZEN)){
			  		isCitizen=true;
			  		break;
			  		
			  	}else if(role.getName().equalsIgnoreCase(MyConstants.ROLE_ENTREPRENEUR)){
			  		isEntrepreneur=true;
			  		break;
			  	}		
			}
		} catch (SystemException e) {
			
			e.printStackTrace();
			return false;
		}
		
		if (isAcademy || isBusiness || isCitizen || isEntrepreneur)
			return true;
		
		
		return false;
	}
	
	
	
	/**
	 * @param currentUser
	 * @return
	 */
	public static boolean isDeveloper (User currentUser){
		
		if (currentUser == null)
			return false;
		

		boolean isDeveloper=false;

		
		try {
			for (Role role: currentUser.getRoles()){ 
				
			  	if(role.getName().equals(MyConstants.ROLE_DEVELOPER))
			  		isDeveloper=true;
			  			
			}
		} catch (SystemException e) {
			
			e.printStackTrace();
			return false;
		}
		
		return isDeveloper;
	}
	
	/**
	 * @param userId
	 * @return
	 */
	public static boolean isDeveloperbyUserId (long  userId){
		
		User currentUser=null;
		try {
			currentUser = UserLocalServiceUtil.getUser(userId);
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
		}
		
		return isDeveloper(currentUser);
	}
	
	
	/**
	 * @param userId
	 * @return
	 */
	public static boolean isAuthorityOrCompanyLeaderByUserId (long userId){
		
		User currentUser=null;
		boolean isAuthorityOrCompanyLeader = false;
		try {
			currentUser = UserLocalServiceUtil.getUser(userId);
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
			return isAuthorityOrCompanyLeader;
		}
		
		boolean isAuthority = isAuthority(currentUser);
		boolean isCompanyLeader = isCompanyLeader(currentUser);
		isAuthorityOrCompanyLeader = isAuthority || isCompanyLeader;
		
		return isAuthorityOrCompanyLeader;
	}
	
	
	
	/**
	 * @param userId
	 * @return
	 */
	public static boolean isAuthorityByUserId (long userId){
		
		User currentUser=null;
		boolean isAuthority = false;
		try {
			currentUser = UserLocalServiceUtil.getUser(userId);
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
			return isAuthority;
		}
		
		return isAuthority(currentUser);
		
		
	}
	
	
	
	/**
	 * @param currentUser
	 * @return
	 */
	public static boolean isAuthority (User currentUser){
		
		if (currentUser == null)
			return false;
		

		boolean isEnte=false;
		
		try {
			for (Role role: currentUser.getRoles()){ 
				
			  	if(role.getName().equals(MyConstants.ROLE_AUTHORITY))
			  		isEnte=true;
			  			
			}
		} catch (SystemException e) {
			
			e.printStackTrace();
			return false;
		}
		
		return isEnte;
	}
	
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static String getDateDDMMYYYYByDate (Date date){
		
		String data = "";
		
		SimpleDateFormat t = new SimpleDateFormat("dd/MM/yyyy");
		
		 data = t.format(date);
		
		
		return data;
	}
	
	
	
	
	/**
	 * @param user
	 * @return nella forma .../user/filippo/so/tasks
	 */
	public static String getTaskPageUrlByCurrentUser (User user){
		
		String rootUrl = IdeaManagementSystemProperties.getRootUrl();
		String username = user.getScreenName();
		String url = rootUrl+"/user/"+username+"/so/tasks";
		
		return url;
	}
	
	
	/**
	 * @param ccUserId
	 * @return
	 */
	public static Long getUserIdByCcUserId(String ccUserId){
		long companyId = getGenericPortalCompanyId();
		Long userId = (long) 0;
		try{
			long columnId = ExpandoColumnLocalServiceUtil.getColumn(companyId, User.class.getName(), "CUSTOM_FIELDS", "CCUserID").getColumnId();
			List<ExpandoValue> values = ExpandoValueLocalServiceUtil.getColumnValues(columnId, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			for(ExpandoValue v:values){
				if(v.getData().equals(ccUserId)){
					userId = v.getClassPK();
					break;
				}
			}
		}
		catch(Exception e){ 
			e.printStackTrace(); 
			return userId;
		}
		
		return userId;
	}
	
	
	/**
	 * @return
	 */
	public static long getGenericPortalCompanyId() {
		
		long companyId = 0;

		//Ottengo utente default Liferay
		 String webId = new String("liferay.com");
		 Company company = null;
		try {
			company = CompanyLocalServiceUtil.getCompanyByWebId(webId);
			
			 companyId = company.getCompanyId();
			
		} catch (PortalException | SystemException e) {
            e.printStackTrace();
			 return companyId;
		}
		
		
		return companyId;
	}
	
	
	/**
	 * @param roleName
	 * @return
	 */
	public static long getRoleIdbyRoleName(String roleName) {
		
		long ruoloId = 0;
		
		List<Role> ruoli = new ArrayList<Role>();
		
		 try {
			 ruoli = RoleLocalServiceUtil.getRoles(0, RoleLocalServiceUtil.getRolesCount());
		} catch (SystemException e) {
			
			e.printStackTrace();
			return ruoloId;
		}
		
		 
		 for (Role r : ruoli){
			 
			 if (r.getName().equals(roleName)){
				 ruoloId = r.getRoleId();
				 break;
			 }
		 }
		
		return ruoloId;
	}
	
	
	
	/**
	 * @param pilot
	 * @return
	 */
	public static String getDefaultAcronimLanguageByPilot(String pilot){
		
		String val="";
		
		Map<String, String> lingue = new HashMap<String, String>();
		lingue.put(MyConstants.PILOT_CITY_BILBAO, MyConstants.ACRONIM_LANGUAGE_SPANISH);
		lingue.put(MyConstants.PILOT_CITY_NOVISAD, MyConstants.ACRONIM_LANGUAGE_SERBIAN);
		lingue.put(MyConstants.PILOT_CITY_UUSIMAA, MyConstants.ACRONIM_LANGUAGE_FINNISH);
		lingue.put(MyConstants.PILOT_CITY_TRENTO, MyConstants.ACRONIM_LANGUAGE_ITALIAN);
	
		
		val = lingue.get(pilot);		
		
		return val;
		
	}
	
	/**
	 * @param pilot
	 * @return
	 */
	public static String getDefaultAcronimCountryByPilot(String pilot){
		
		String val="";
		
		Map<String, String> lingue = new HashMap<String, String>();
		lingue.put(MyConstants.PILOT_CITY_BILBAO, MyConstants.ACRONIM_COUNTRY_SPAIN);
		lingue.put(MyConstants.PILOT_CITY_NOVISAD, MyConstants.ACRONIM_COUNTRY_SERBIA);
		lingue.put(MyConstants.PILOT_CITY_UUSIMAA, MyConstants.ACRONIM_COUNTRY_FINLAND);
		lingue.put(MyConstants.PILOT_CITY_TRENTO, MyConstants.ACRONIM_COUNTRY_ITALY);
	
		
		val = lingue.get(pilot);		
		
		return val;
		
	}
	
	/**
	 * @param acronimLanguage
	 * @return
	 */
	public static Locale getLocaleByAcronimLanguage(String acronimLanguage){
		
		
		Map<String, String> languages = new HashMap<String, String>();
		languages.put(MyConstants.ACRONIM_LANGUAGE_SPANISH, MyConstants.ACRONIM_COUNTRY_SPAIN);
		languages.put(MyConstants.ACRONIM_LANGUAGE_ITALIAN, MyConstants.ACRONIM_COUNTRY_ITALY);
		languages.put(MyConstants.ACRONIM_LANGUAGE_SERBIAN, MyConstants.ACRONIM_COUNTRY_SERBIA);
		languages.put(MyConstants.ACRONIM_LANGUAGE_FINNISH, MyConstants.ACRONIM_COUNTRY_FINLAND);
	
		
		Locale loc = new Locale(acronimLanguage, languages.get(acronimLanguage));
		
		return loc;
		
	}
	
	/**
	 * @param authorityId
	 * @return
	 */
	public static String getLanguageAcronimByAuthoridyIdPilot (long authorityId){
		
		String pilot = getPilotByUserId(authorityId);
		
		String acronimoLingua = getDefaultAcronimLanguageByPilot(pilot);
		
		return acronimoLingua;
		
	}
	
	/**
	 * @param authorityId
	 * @return
	 */
	public static String getLanguageAcronimByOrganizationIdPilot (long organizationId){
		
		String pilot = getPilotByOrganizationId(organizationId);
		
		String acronimoLingua = getDefaultAcronimLanguageByPilot(pilot);
		
		return acronimoLingua;
		
	}
	
	/**
	 * @param currentUser
	 * @return
	 */
	public static  boolean isUserWhoKnowEnglish (User currentUser){
		
		
		if (currentUser != null ){
			//attenzione se fai una idea per un pilot che non sia il tuo in una lingua che non conosci, non te la visualizza
			String[] lingueUtente = (String[]) currentUser.getExpandoBridge().getAttribute(MyConstants.CUSTOMFIELD_LANGUAGES);
		
			for(String linguaUt: lingueUtente) {
				
				if(linguaUt.equalsIgnoreCase("english"))
					return true;
				
			}
		}
		
		return false;
	}
	

	
	
	
	/**
	 * @param pilot
	 * @return
	 */
	public static Locale getLocaleByPilot (String pilot){
		
		String language = getDefaultAcronimLanguageByPilot(pilot);
		String country = getDefaultAcronimCountryByPilot(pilot);
		
		Locale loc = new Locale(language, country);
		
		return loc;
		
	}
	
	
	/**
	 * @param liferayPilot
	 * @return
	 */
	public static String getUIPilotByPilotId(String pilotId){
		
		String val="";
		
		Map<String, String> pilots = new HashMap<String, String>();
		pilots.put(MyConstants.PILOT_CITY_BILBAO, MyConstants.UI_PILOT_CITY_BILBAO);
		pilots.put(MyConstants.PILOT_CITY_NOVISAD, MyConstants.UI_PILOT_CITY_NOVISAD);
		pilots.put(MyConstants.PILOT_CITY_UUSIMAA, MyConstants.UI_PILOT_CITY_UUSIMAA);
		pilots.put(MyConstants.PILOT_CITY_TRENTO, MyConstants.UI_PILOT_CITY_TRENTO);
	
		
		val = pilots.get(pilotId);		
		
		return val;
		
	}
	
	
	
	/**
	 * @param currentUser
	 * @param organizationId
	 * @return
	 */
	public static boolean isUserOfOrganizationAuthority (User currentUser, long organizationId){
		
		long orgaAuthorityId = getOrganizationIdByMunicipalityId(currentUser.getUserId());
		
		if ( (orgaAuthorityId > 0)  &&  (organizationId == orgaAuthorityId) )
			return true;
		
		return false;
		
		
	}
	
	
	/**
	 * @param organizationId
	 * @return
	 */
	public static List<User> getUsersByOrganizationId (long organizationId){
		
		List<User> users = new ArrayList<User>();
		
		try {
			users =UserLocalServiceUtil.getOrganizationUsers(organizationId);
		} catch (SystemException e) {
			
			e.printStackTrace();
		}
		
		
		return users;
	}
	
	
	/**
	 * An user must be assigned only to one Organization with isAuthority=true
	 * @param municipalityId
	 * @return
	 */
	public static long getOrganizationIdByMunicipalityId (long municipalityId){
		
		List<Organization> orgs = new ArrayList<Organization>();
		
		try {
			orgs = OrganizationLocalServiceUtil.getUserOrganizations(municipalityId);
		} catch (SystemException e) {
			
			e.printStackTrace();
		}
		

		 
		for (Organization org:orgs){
			
			if (org.getExpandoBridge().getAttribute(MyConstants.CUSTOMFIELD_ISAUTHORITY).toString().equalsIgnoreCase("true"))
				return org.getOrganizationId();
		}
		
		return 0;
	}
	
	/**
	 * An user must be assigned only to one Organization with isAuthority=true
	 * @param municipalityId
	 * @return
	 */
	public static long getOrganizationIdByLeaderId (long leaderId){
		
		List<Organization> orgs = new ArrayList<Organization>();
		
		try {
			orgs = OrganizationLocalServiceUtil.getUserOrganizations(leaderId);
		} catch (SystemException e) {
			
			e.printStackTrace();
		}
		

		 
		for (Organization org:orgs){

			try {
			
				if (org.getExpandoBridge().getAttribute(MyConstants.CUSTOMFIELD_ISCOMPANY).toString().equalsIgnoreCase("true"))
					return org.getOrganizationId();
			
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			
		}
		
		return 0;
	}
	
	/**
	 * @param userId
	 * @return
	 */
	public static String getOrganizationNameByLeaderId (long userId){
		
		long orgId = getOrganizationIdByLeaderId(userId);
		String orgName = getOrganizationNameByOrganizationId(orgId);
		
		return orgName;
	}
	
	
	
	/**
	 * @param organizationId
	 * @return
	 */
	public static String getOrganizationNameByOrganizationId (long organizationId){
		
		
		String orgName = "";
		
		try {
			Organization org =OrganizationLocalServiceUtil.getOrganization(organizationId);
			
			orgName = org.getName();
			
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return orgName;
		}
		
		
		return orgName;
	}
	
	
	/**
	 * @param userId
	 * @return
	 */
	public static String getOrganizationNameByMunicipalityId(long userId){
		
		long orgid = getOrganizationIdByMunicipalityId(userId);
		
		String name = getOrganizationNameByOrganizationId(orgid);
		
		return name;
	}
	
	
	
	/**
	 * Date due liste di idee ritorna la loro unione senza duplicati
	 * @param l1
	 * @param l2
	 * @return
	 */
	public static List<CLSIdea> getIdeaListWithoutDuplicates (List<CLSIdea> l1, List<CLSIdea> l2){
		
		ArrayList<CLSIdea> mergeList = new ArrayList<CLSIdea>();
		mergeList.addAll(l1);
		mergeList.addAll(l2);
		Set<CLSIdea> set  = new HashSet<CLSIdea>(mergeList);
		ArrayList<CLSIdea> mergeListWithoutDuplicates = new ArrayList<CLSIdea>();
		mergeListWithoutDuplicates.addAll(set);
		return mergeListWithoutDuplicates;
		
	}
	
	
	
	/**
	 * Per eliminare i tag non usati da nessuna parte e non mostrarli tra i suggerimenti
	 */
	public static void bonificaTags (){
		
		
		//bonifico prima la tabella dei tag
		List<AssetTag> tags = new ArrayList<AssetTag>();
		try {
		 tags = AssetTagLocalServiceUtil.getAssetTags(0, AssetTagLocalServiceUtil.getAssetTagsCount());
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		for (AssetTag tag:tags){
			
			if (tag.getAssetCount() == 0)
				try {
					AssetTagLocalServiceUtil.deleteAssetTag(tag);
				} catch (SystemException e) {
					
					e.printStackTrace();
				}
			
		}
		
		//bonifico poi la tabella delle statistiche dei tag
		List<AssetTagStats> tagStats = new ArrayList<AssetTagStats>();
		
		try {
			tagStats = AssetTagStatsLocalServiceUtil.getAssetTagStatses(0, AssetTagStatsLocalServiceUtil.getAssetTagStatsesCount());
			} catch (SystemException e) {
				e.printStackTrace();
			}
		
		
		for (AssetTagStats tagStat:tagStats){
			
			if (tagStat.getAssetCount() == 0)
				try {
					AssetTagStatsLocalServiceUtil.deleteAssetTagStats(tagStat);
				} catch (SystemException e) {
					
					e.printStackTrace();
				}
			
		}
		
	}
	
	
	
	/**
	 * @param user
	 * @return
	 */
	public static String getUserProfileUrlByUser(User user){
		
		
		String userURL = "/web/"+user.getScreenName()+"/so/profile";
		
		return userURL;
	}
	
	
	/**
	 * @param resourceRequest
	 * @param pilot
	 */
	public static void setPilotByResourceReq (ResourceRequest resourceRequest, String pilot){
		
		PortletSession session = resourceRequest.getPortletSession();
		session.setAttribute(MyConstants.KEY_PILOT, pilot, PortletSession.APPLICATION_SCOPE);
		
	}
	
	/**
	 * @param resourceRequest
	 * @param pilot
	 */
	public static void setPilotByRenderReq (RenderRequest request, String pilot){
		
		PortletSession session = request.getPortletSession();
		session.setAttribute(MyConstants.KEY_PILOT, pilot, PortletSession.APPLICATION_SCOPE);
		
	}
	
	
	
	/**
	 * @param renderRequest
	 * @return
	 */
	public static String getPilot (ActionRequest actionRequest){
		
		String pilot = "";
		
		User currentUser = null;
		
		try {
			 currentUser = PortalUtil.getUser(actionRequest);
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			
		}
		
		try {
		
		
			if (Validator.isNull(currentUser)){
			
				PortletSession sessionUser = actionRequest.getPortletSession();
				pilot = (String) sessionUser.getAttribute(MyConstants.KEY_PILOT, PortletSession.APPLICATION_SCOPE); //questo valore viene settato da un'altro tool: il Controller
			}else{
				
				
				String[] pilotUtente = (String[]) currentUser.getExpandoBridge().getAttribute(MyConstants.CUSTOMFIELD_PILOT);
				pilot = pilotUtente[0];
				
			}
		
		
		}	 catch (Exception e) {
			
			e.printStackTrace();
			return pilot;
		}
		
		if (pilot == null)
			return "";
		
		
		return pilot;
		
	}
	
	/**
	 * @param actionRequest
	 * @return
	 */
	public static ResourceBundle getResBundleViaPilotByAction(ActionRequest actionRequest){
	
		String pilot = getPilot(actionRequest);
		Locale locale =getLocaleByPilot(pilot);
		ResourceBundle res = ResourceBundle.getBundle("Language", locale);
	
	
	return res;
	
	}	
	
	
	
	/**
	 * Ritorna il centro della mappa di google maps, differenziato per pilot
	 * @param pilot
	 * @return
	 */
	public static String[] getMapLatLongCenterbyPilot (String pilot){
		
		String[] latLong = {"46.0747793", "11.121748600000046"};//trento
		
		if (pilot.equalsIgnoreCase(MyConstants.PILOT_CITY_BILBAO)){
			
			latLong [0] = "43.2769911";// Bilbao
			latLong [1] = "-2.952563199999986";
			
		}else if(pilot.equalsIgnoreCase(MyConstants.PILOT_CITY_NOVISAD)){
			latLong [0] = "45.2671352";// Novi Sad
			latLong [1] = "19.83354959999997";
			
		}else if(pilot.equalsIgnoreCase(MyConstants.PILOT_CITY_UUSIMAA)){
			
			latLong [0] = "60.16985569999999";// Helsinki
			latLong [1] = "24.93837899999994";
			
		}else if(pilot.equalsIgnoreCase(MyConstants.PILOT_CITY_TRENTO)){
			latLong [0] = "46.0747793";// Trento
			latLong [1] = "11.121748600000046";
		}else{
			
			//ritorno quelli di default settati nel pannello di controllo
			try {
				latLong [0] = IdeaManagementSystemProperties.getMapCenterLatitude();
				latLong [1] = IdeaManagementSystemProperties.getMapCenterLongitude();
			} catch (SystemException e) {
				
				e.printStackTrace();
				return latLong;
			}
			
		}
					
		
		return latLong;
	}
	
	
	
	/**
	 * @param currentUser
	 * @return
	 */
	public static String getFirstNameByUser(User currentUser){
		
		if (Validator.isNull(currentUser))
			return "";
		else
			return currentUser.getFirstName();
		
	}
	
	/**
	 * @param currentUser
	 * @return
	 */
	public static String getFulltNameByUserId(long userId){
		
		User user=null;
		try {
			user = UserLocalServiceUtil.getUser(userId);
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return "";
		}
		
		return user.getFullName();
		
	}
	
	
	/**
	 * @param themeDisplay
	 * @return
	 */
	public static String getPathDLbyThemeDisplay (ThemeDisplay themeDisplay){
		
		return (themeDisplay.getPortalURL() + themeDisplay.getPathContext() + "/documents/" + themeDisplay.getScopeGroupId() + StringPool.SLASH);
		
	}
	
	
	/**
	 * redirect to the rigth page
	 * @param actionRequest
	 * @param actionResponse
	 */
	public static void redirect(ActionRequest actionRequest,ActionResponse actionResponse){
		
		 ThemeDisplay td  = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		 String  portal=td.getPortalURL();
		 String url =td.getURLCurrent();
		 StringTokenizer path= new StringTokenizer(url, "?");
		 url=path.nextToken();
		 String totalPath= portal+url;
		 
		try {
			actionResponse.sendRedirect(totalPath);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} //go to the page that you put as second parameter
	}
	
	
	/**
	 * @param resourceRequest
	 * @param locale
	 * @return
	 */
	public static Map<String, String> getHMFilterCategoryParam (ResourceRequest resourceRequest, String locale){
	
		String  cat0=resourceRequest.getParameter("cat0");
		String  cat1=resourceRequest.getParameter("cat1");
		String  cat2=resourceRequest.getParameter("cat2");
		String  cat3=resourceRequest.getParameter("cat3");
		String  cat4=resourceRequest.getParameter("cat4");
		String  cat5=resourceRequest.getParameter("cat5");
		String  cat6=resourceRequest.getParameter("cat6");
		String  cat7=resourceRequest.getParameter("cat7");
		String  cat8=resourceRequest.getParameter("cat8");
		String  cat9=resourceRequest.getParameter("cat9");
		
		
		Map<String, String> catFiltri = new HashMap<String, String>();
		catFiltri.put(IdeasListUtils.getVocabularyLocaleByEn("Residence - Housing & Development", locale) , cat0);
		catFiltri.put(IdeasListUtils.getVocabularyLocaleByEn("Transportation and Mobility", locale) , cat1);
		catFiltri.put(IdeasListUtils.getVocabularyLocaleByEn("Economy", locale) , cat2);
		catFiltri.put(IdeasListUtils.getVocabularyLocaleByEn("Environment & Energy", locale) , cat3);
		catFiltri.put(IdeasListUtils.getVocabularyLocaleByEn("Society & Social Services", locale) , cat4);
		catFiltri.put(IdeasListUtils.getVocabularyLocaleByEn("Education & Culture", locale) , cat5);
		catFiltri.put(IdeasListUtils.getVocabularyLocaleByEn("City Government & Business", locale) , cat6);
		catFiltri.put(IdeasListUtils.getVocabularyLocaleByEn("Tourism & Leisure & Recreation", locale) , cat7);
		catFiltri.put(IdeasListUtils.getVocabularyLocaleByEn("Public Safety", locale) , cat8);
		catFiltri.put(IdeasListUtils.getVocabularyLocaleByEn("Quality of Life", locale) , cat9);
		
		
		return catFiltri;
		
	}
	
	
	
	/**
	 * @return
	 */
	public static long getDefaultCompanyId (){
		
		 String webId = new String("liferay.com");
		 
		 Company company = null;
		 
		 try {
			company = CompanyLocalServiceUtil.getCompanyByWebId(webId);
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
		}
		
		return company.getCompanyId();
	}
	
	/**
	 * @param organizationId
	 * @return
	 */
	public static boolean isCompanyOrganization (long organizationId){
		
		try {
			Organization org = OrganizationLocalServiceUtil.getOrganization(organizationId);
			
			System.out.println("CUSTOMFIELD_ISCOMPANY "+MyConstants.CUSTOMFIELD_ISCOMPANY);
			
			
			if (org.getExpandoBridge().getAttribute(MyConstants.CUSTOMFIELD_ISCOMPANY).toString().equalsIgnoreCase("true"))
				return true;
			
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return false;
		}
		return false;
	}

	
	/**
	 * @param currentUser
	 * @return
	 */
	public static boolean isCompanyLeader (User currentUser){
		
		if (Validator.isNull(currentUser))
			return false;
		

		boolean isCompanyLeader=false;

		
		try {
			for (Role role: currentUser.getRoles()){ 
				
			  	if(role.getName().equals(MyConstants.ROLE_COMPANY_LEADER))
			  		isCompanyLeader=true;
			  			
			}
		} catch (SystemException e) {
			
			e.printStackTrace();
			return false;
		}
		return isCompanyLeader;
	}
	
	
	
	/**
	 * @param userId
	 * @return
	 */
	public static boolean isCompanyLeaderByUserId (long userId){
		
		User currentUser=null;
		boolean isCompanyLeader = false;
		try {
			currentUser = UserLocalServiceUtil.getUser(userId);
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
			return isCompanyLeader;
		}
		
		return isCompanyLeader(currentUser);
		
		
	}
	
	
	
	
	/**
	 * @param currentUser
	 * @return
	 */
	public static List<User> getListUsersOfMyCompany (User currentUser){
		
		List<User> employees = new ArrayList<User>();
		
		if (!isCompanyLeader(currentUser))
			return employees;
		
		long orgId = MyUtils.getOrganizationIdByLeaderId(currentUser.getUserId());
		
		employees = getUsersByOrganizationId(orgId);
		
		employees.remove(currentUser);//I remove the companyLeader
		
		
		
		return employees;
	}
	
	
	/**
	 * @param vocabularyId
	 * @param locale
	 * @return
	 */
	public static JSONArray getCategoriesByVocabularyId (long  vocabularyId, String locale){
		
		JSONArray jsonArr =  JSONFactoryUtil.createJSONArray();
		

		
		List<AssetCategory> categoryList = new ArrayList<AssetCategory>();
		 try {
			 categoryList = AssetCategoryLocalServiceUtil.getVocabularyCategories(vocabularyId, 0, AssetCategoryLocalServiceUtil.getVocabularyCategoriesCount(vocabularyId), null);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return jsonArr;
		}
		 
		 
		for (AssetCategory categ:categoryList){
			
			 JSONObject jsonObject =  JSONFactoryUtil.createJSONObject();
			 jsonObject.put("categId", categ.getCategoryId());
			 jsonObject.put("categName", categ.getTitle(locale));
			 jsonArr.put(jsonObject);
		}
		
	
		
		return jsonArr;
	} 
	
	
	/**
	 * @param userId
	 * @return
	 */
	public static long getMunicipalityOrganizationIdByUserId(long userId){
		
		User currentUser=null;
	
		try {
			currentUser = UserLocalServiceUtil.getUser(userId);
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
			return 0;
		}
		
		return NeedUtils.getMunicipalityOrganizationIdByUser(currentUser);
		
	}
	
	
	/**
	 * @param sourceImg
	 * @param size
	 * @param imgType
	 * @return
	 * @throws Exception
	 */
	public static byte[] createThumbnail(byte[] sourceImg, int size, String imgType) throws Exception {
		
		BufferedImage img = ImageIO.read(new ByteArrayInputStream(sourceImg));
		double ratio = (double)img.getWidth()/ (double)img.getHeight();
		
		Image thumb;
		if(Math.max(img.getWidth(), img.getHeight())<size) {
			return sourceImg;
		}
		else if(img.getHeight() > img.getWidth()) { //Portrait
			thumb = img.getScaledInstance((int)Math.round(size*ratio), size, Image.SCALE_SMOOTH);
		}
		else if(img.getWidth() > img.getHeight()) { //Landscape
			thumb = img.getScaledInstance(size, (int)Math.round(size/ratio), Image.SCALE_SMOOTH);
		}
		else { //Square
			thumb = img.getScaledInstance(size, size, Image.SCALE_SMOOTH);
		}
		
		MediaTracker tracker = new MediaTracker(new Container());
		tracker.addImage(thumb, 0);
		try { tracker.waitForAll(); }
		catch(InterruptedException e) { e.printStackTrace(); }
		BufferedImage bufferedImage = new BufferedImage(thumb.getWidth(null), thumb.getHeight(null), 1);
		bufferedImage.createGraphics().drawImage(thumb, 0, 0, null);
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ImageIO.write(bufferedImage, imgType.replace("image/", ""), bos);
		
		return bos.toByteArray();
	}
	
	
	/**
	 * @param file
	 * @return
	 */
	public static String getFileExtension(File file) {
        
		String fileName = file.getName();
        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
        return fileName.substring(fileName.lastIndexOf(".")+1);
        else return "";
    }
	
	
	
}//classe