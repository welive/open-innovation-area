package it.eng.rspa.ideas.utils;

import com.liferay.util.portlet.PortletProps;

public final class MyConstants {

	
	public static final String  ROLE_ACADEMY   = PortletProps.get("role.academy");
	public static final String  ROLE_AUTHORITY = PortletProps.get("role.authority");
	public static final String  ROLE_BUSINESS  = PortletProps.get("role.business");
	public static final String  ROLE_CITIZEN   = PortletProps.get("role.citizen");
	public static final String  ROLE_DEVELOPER = PortletProps.get("role.developer");
	public static final String  ROLE_ENTREPRENEUR = PortletProps.get("role.entrepreneur");
	public static final String  ROLE_DELETED_USER = PortletProps.get("role.deleteduser");
	public static final String  ROLE_COMPANY_LEADER = PortletProps.get("role.companyleader");
	
	public static final String DL_FOLDER_ROOT = "ideaManagementFolderRoot";
	
	public static final int REPRESENTATIVEIMG_THUMB_SIZE = Integer.parseInt(PortletProps.get("representativeimg.thumbnail.size"));
	public static final int SCREENSHOT_THUMB_SIZE = Integer.parseInt(PortletProps.get("screenshot.thumbnail.size"));
	
	public static final String KEY_PILOT = PortletProps.get("key.pilot");
	
	public static final String CUSTOMFIELD_CCUSERID = PortletProps.get("customfield.ccuserid");
	public static final String CUSTOMFIELD_ISAUTHORITY = PortletProps.get("customfield.isauthority");
	public static final String CUSTOMFIELD_ISCOMPANY = PortletProps.get("customfield.iscompany");
	public static final String CUSTOMFIELD_LANGUAGES = PortletProps.get("customfield.languages");
	public static final String CUSTOMFIELD_PILOT = PortletProps.get("customfield.pilot");
	
	public static final String CHALLENGE_STATE_OPEN = "open";
	public static final String CHALLENGE_STATE_EVALUATION = "evaluation";
	public static final String CHALLENGE_STATE_EVALUATED = "evaluated";
	public static final String CHALLENGE_STATE_CLOSED = "closed";
	
	public static final String IDEA_STATE_ELABORATION = "selection";
	public static final String IDEA_STATE_EVALUATION = "evaluation";
	public static final String IDEA_STATE_SELECTED = "selected";
	public static final String IDEA_STATE_REFINEMENT = "refinement";
	public static final String IDEA_STATE_IMPLEMENTATION = "implementation";
	public static final String IDEA_STATE_MONITORING = "monitoring";
	public static final String IDEA_STATE_REJECT = "reject";
	public static final String IDEA_STATE_COMPLETED = "completed";
	
	public final static String PILOT_CITY_BILBAO = "Bilbao";
	public final static String PILOT_CITY_UUSIMAA = "Uusimaa";
	public final static String PILOT_CITY_NOVISAD = "Novisad";
	public final static String PILOT_CITY_TRENTO = "Trento";
	
	public final static String UI_PILOT_CITY_BILBAO = "Bilbao";
	public final static String UI_PILOT_CITY_UUSIMAA = "Helsinki Region";
	public final static String UI_PILOT_CITY_NOVISAD = "Novi Sad";
	public final static String UI_PILOT_CITY_TRENTO = "Trento";
	
	public final static String LANGUAGE_ENGLISH = "English";
	public final static String LANGUAGE_SPANISH = "Spanish";
	public final static String LANGUAGE_ITALIAN = "Italian";
	public final static String LANGUAGE_FINNISH = "Finnish";
	public final static String LANGUAGE_SERBIAN = "Serbian";
	public final static String LANGUAGE_SERBIAN_LATIN = "SerbianLatin";
	
	public final static String ACRONIM_LANGUAGE_SPANISH = "es";
	public final static String ACRONIM_LANGUAGE_ITALIAN = "it";
	public final static String ACRONIM_LANGUAGE_FINNISH = "fi";
	public final static String ACRONIM_LANGUAGE_SERBIAN = "sr";
	
	public final static String ACRONIM_COUNTRY_SPAIN = "ES";
	public final static String ACRONIM_COUNTRY_ITALY= "IT";
	public final static String ACRONIM_COUNTRY_FINLAND = "FI";
	public final static String ACRONIM_COUNTRY_SERBIA = "RS";
	
	public final static int N_OF_STATIC_BARRIER_CRITERIA = 3;
	public final static int N_OF_STATIC_QUANTITATIVE_CRITERIA = 10;
	
	public final static String MKT_DETAIL_LINK = PortletProps.get("mkt.details-link");
	public final static String MKT_CAT_REST = PortletProps.get("mkt.category-rest");
	public final static String MKT_CAT_SOAP = PortletProps.get("mkt.category-soap");
	public final static String MKT_CAT_OSGI = PortletProps.get("mkt.category-osgi");
	public final static String MKT_CAT_DATASET = PortletProps.get("mkt.category-dataset");
	public final static String MKT_CAT_GADGET = PortletProps.get("mkt.category-gadget");
	public final static String MKT_CAT_APPMOBILE = PortletProps.get("mkt.category-appmobile");
	public final static String MKT_CAT_APPWEB =PortletProps.get("mkt.category-appweb");
	
}
