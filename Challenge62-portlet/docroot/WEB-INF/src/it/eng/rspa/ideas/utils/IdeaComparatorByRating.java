package it.eng.rspa.ideas.utils;


import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;

import java.util.Comparator;

public class IdeaComparatorByRating implements Comparator<CLSIdea>{

	

	@Override
	public int compare(CLSIdea idea1, CLSIdea idea2) {
		
		double rating1 = 0;
		double rating2 = 0;

		rating1 = IdeasListUtils.getAverageDoubleRatingsByIdeaOrNeed(idea1);
		rating2 = IdeasListUtils.getAverageDoubleRatingsByIdeaOrNeed(idea2);
			

		if (rating2 > rating1)
			return 1;
		
		if (rating1 > rating2)
			return -1;
		
		return 0;
	}

}
