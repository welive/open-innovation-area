/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSVmeProjectsImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSVmeProjectsModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the c l s vme projects service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSVmeProjectsPersistence
 * @see CLSVmeProjectsUtil
 * @generated
 */
public class CLSVmeProjectsPersistenceImpl extends BasePersistenceImpl<CLSVmeProjects>
	implements CLSVmeProjectsPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CLSVmeProjectsUtil} to access the c l s vme projects persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CLSVmeProjectsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CLSVmeProjectsModelImpl.ENTITY_CACHE_ENABLED,
			CLSVmeProjectsModelImpl.FINDER_CACHE_ENABLED,
			CLSVmeProjectsImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CLSVmeProjectsModelImpl.ENTITY_CACHE_ENABLED,
			CLSVmeProjectsModelImpl.FINDER_CACHE_ENABLED,
			CLSVmeProjectsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CLSVmeProjectsModelImpl.ENTITY_CACHE_ENABLED,
			CLSVmeProjectsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PROJECTSBYIDEAID =
		new FinderPath(CLSVmeProjectsModelImpl.ENTITY_CACHE_ENABLED,
			CLSVmeProjectsModelImpl.FINDER_CACHE_ENABLED,
			CLSVmeProjectsImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByprojectsByIdeaId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSBYIDEAID =
		new FinderPath(CLSVmeProjectsModelImpl.ENTITY_CACHE_ENABLED,
			CLSVmeProjectsModelImpl.FINDER_CACHE_ENABLED,
			CLSVmeProjectsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByprojectsByIdeaId", new String[] { Long.class.getName() },
			CLSVmeProjectsModelImpl.IDEAID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PROJECTSBYIDEAID = new FinderPath(CLSVmeProjectsModelImpl.ENTITY_CACHE_ENABLED,
			CLSVmeProjectsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByprojectsByIdeaId", new String[] { Long.class.getName() });

	/**
	 * Returns all the c l s vme projectses where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @return the matching c l s vme projectses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSVmeProjects> findByprojectsByIdeaId(long ideaId)
		throws SystemException {
		return findByprojectsByIdeaId(ideaId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s vme projectses where ideaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSVmeProjectsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param start the lower bound of the range of c l s vme projectses
	 * @param end the upper bound of the range of c l s vme projectses (not inclusive)
	 * @return the range of matching c l s vme projectses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSVmeProjects> findByprojectsByIdeaId(long ideaId, int start,
		int end) throws SystemException {
		return findByprojectsByIdeaId(ideaId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s vme projectses where ideaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSVmeProjectsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param start the lower bound of the range of c l s vme projectses
	 * @param end the upper bound of the range of c l s vme projectses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s vme projectses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSVmeProjects> findByprojectsByIdeaId(long ideaId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSBYIDEAID;
			finderArgs = new Object[] { ideaId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PROJECTSBYIDEAID;
			finderArgs = new Object[] { ideaId, start, end, orderByComparator };
		}

		List<CLSVmeProjects> list = (List<CLSVmeProjects>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSVmeProjects clsVmeProjects : list) {
				if ((ideaId != clsVmeProjects.getIdeaId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSVMEPROJECTS_WHERE);

			query.append(_FINDER_COLUMN_PROJECTSBYIDEAID_IDEAID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSVmeProjectsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				if (!pagination) {
					list = (List<CLSVmeProjects>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSVmeProjects>(list);
				}
				else {
					list = (List<CLSVmeProjects>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s vme projects in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s vme projects
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException if a matching c l s vme projects could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSVmeProjects findByprojectsByIdeaId_First(long ideaId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSVmeProjectsException, SystemException {
		CLSVmeProjects clsVmeProjects = fetchByprojectsByIdeaId_First(ideaId,
				orderByComparator);

		if (clsVmeProjects != null) {
			return clsVmeProjects;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSVmeProjectsException(msg.toString());
	}

	/**
	 * Returns the first c l s vme projects in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s vme projects, or <code>null</code> if a matching c l s vme projects could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSVmeProjects fetchByprojectsByIdeaId_First(long ideaId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSVmeProjects> list = findByprojectsByIdeaId(ideaId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s vme projects in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s vme projects
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException if a matching c l s vme projects could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSVmeProjects findByprojectsByIdeaId_Last(long ideaId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSVmeProjectsException, SystemException {
		CLSVmeProjects clsVmeProjects = fetchByprojectsByIdeaId_Last(ideaId,
				orderByComparator);

		if (clsVmeProjects != null) {
			return clsVmeProjects;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSVmeProjectsException(msg.toString());
	}

	/**
	 * Returns the last c l s vme projects in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s vme projects, or <code>null</code> if a matching c l s vme projects could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSVmeProjects fetchByprojectsByIdeaId_Last(long ideaId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByprojectsByIdeaId(ideaId);

		if (count == 0) {
			return null;
		}

		List<CLSVmeProjects> list = findByprojectsByIdeaId(ideaId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s vme projectses before and after the current c l s vme projects in the ordered set where ideaId = &#63;.
	 *
	 * @param recordId the primary key of the current c l s vme projects
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s vme projects
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException if a c l s vme projects with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSVmeProjects[] findByprojectsByIdeaId_PrevAndNext(long recordId,
		long ideaId, OrderByComparator orderByComparator)
		throws NoSuchCLSVmeProjectsException, SystemException {
		CLSVmeProjects clsVmeProjects = findByPrimaryKey(recordId);

		Session session = null;

		try {
			session = openSession();

			CLSVmeProjects[] array = new CLSVmeProjectsImpl[3];

			array[0] = getByprojectsByIdeaId_PrevAndNext(session,
					clsVmeProjects, ideaId, orderByComparator, true);

			array[1] = clsVmeProjects;

			array[2] = getByprojectsByIdeaId_PrevAndNext(session,
					clsVmeProjects, ideaId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSVmeProjects getByprojectsByIdeaId_PrevAndNext(
		Session session, CLSVmeProjects clsVmeProjects, long ideaId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSVMEPROJECTS_WHERE);

		query.append(_FINDER_COLUMN_PROJECTSBYIDEAID_IDEAID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSVmeProjectsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(ideaId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsVmeProjects);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSVmeProjects> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s vme projectses where ideaId = &#63; from the database.
	 *
	 * @param ideaId the idea ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByprojectsByIdeaId(long ideaId) throws SystemException {
		for (CLSVmeProjects clsVmeProjects : findByprojectsByIdeaId(ideaId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(clsVmeProjects);
		}
	}

	/**
	 * Returns the number of c l s vme projectses where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @return the number of matching c l s vme projectses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByprojectsByIdeaId(long ideaId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PROJECTSBYIDEAID;

		Object[] finderArgs = new Object[] { ideaId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSVMEPROJECTS_WHERE);

			query.append(_FINDER_COLUMN_PROJECTSBYIDEAID_IDEAID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PROJECTSBYIDEAID_IDEAID_2 = "clsVmeProjects.ideaId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PROJECTSBYIDEAIDANDTYPE =
		new FinderPath(CLSVmeProjectsModelImpl.ENTITY_CACHE_ENABLED,
			CLSVmeProjectsModelImpl.FINDER_CACHE_ENABLED,
			CLSVmeProjectsImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByprojectsByIdeaIdAndType",
			new String[] {
				Long.class.getName(), Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSBYIDEAIDANDTYPE =
		new FinderPath(CLSVmeProjectsModelImpl.ENTITY_CACHE_ENABLED,
			CLSVmeProjectsModelImpl.FINDER_CACHE_ENABLED,
			CLSVmeProjectsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByprojectsByIdeaIdAndType",
			new String[] { Long.class.getName(), Boolean.class.getName() },
			CLSVmeProjectsModelImpl.IDEAID_COLUMN_BITMASK |
			CLSVmeProjectsModelImpl.ISMOCKUP_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PROJECTSBYIDEAIDANDTYPE = new FinderPath(CLSVmeProjectsModelImpl.ENTITY_CACHE_ENABLED,
			CLSVmeProjectsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByprojectsByIdeaIdAndType",
			new String[] { Long.class.getName(), Boolean.class.getName() });

	/**
	 * Returns all the c l s vme projectses where ideaId = &#63; and isMockup = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param isMockup the is mockup
	 * @return the matching c l s vme projectses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSVmeProjects> findByprojectsByIdeaIdAndType(long ideaId,
		boolean isMockup) throws SystemException {
		return findByprojectsByIdeaIdAndType(ideaId, isMockup,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s vme projectses where ideaId = &#63; and isMockup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSVmeProjectsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param isMockup the is mockup
	 * @param start the lower bound of the range of c l s vme projectses
	 * @param end the upper bound of the range of c l s vme projectses (not inclusive)
	 * @return the range of matching c l s vme projectses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSVmeProjects> findByprojectsByIdeaIdAndType(long ideaId,
		boolean isMockup, int start, int end) throws SystemException {
		return findByprojectsByIdeaIdAndType(ideaId, isMockup, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s vme projectses where ideaId = &#63; and isMockup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSVmeProjectsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param isMockup the is mockup
	 * @param start the lower bound of the range of c l s vme projectses
	 * @param end the upper bound of the range of c l s vme projectses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s vme projectses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSVmeProjects> findByprojectsByIdeaIdAndType(long ideaId,
		boolean isMockup, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSBYIDEAIDANDTYPE;
			finderArgs = new Object[] { ideaId, isMockup };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PROJECTSBYIDEAIDANDTYPE;
			finderArgs = new Object[] {
					ideaId, isMockup,
					
					start, end, orderByComparator
				};
		}

		List<CLSVmeProjects> list = (List<CLSVmeProjects>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSVmeProjects clsVmeProjects : list) {
				if ((ideaId != clsVmeProjects.getIdeaId()) ||
						(isMockup != clsVmeProjects.getIsMockup())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_CLSVMEPROJECTS_WHERE);

			query.append(_FINDER_COLUMN_PROJECTSBYIDEAIDANDTYPE_IDEAID_2);

			query.append(_FINDER_COLUMN_PROJECTSBYIDEAIDANDTYPE_ISMOCKUP_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSVmeProjectsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				qPos.add(isMockup);

				if (!pagination) {
					list = (List<CLSVmeProjects>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSVmeProjects>(list);
				}
				else {
					list = (List<CLSVmeProjects>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s vme projects in the ordered set where ideaId = &#63; and isMockup = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param isMockup the is mockup
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s vme projects
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException if a matching c l s vme projects could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSVmeProjects findByprojectsByIdeaIdAndType_First(long ideaId,
		boolean isMockup, OrderByComparator orderByComparator)
		throws NoSuchCLSVmeProjectsException, SystemException {
		CLSVmeProjects clsVmeProjects = fetchByprojectsByIdeaIdAndType_First(ideaId,
				isMockup, orderByComparator);

		if (clsVmeProjects != null) {
			return clsVmeProjects;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(", isMockup=");
		msg.append(isMockup);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSVmeProjectsException(msg.toString());
	}

	/**
	 * Returns the first c l s vme projects in the ordered set where ideaId = &#63; and isMockup = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param isMockup the is mockup
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s vme projects, or <code>null</code> if a matching c l s vme projects could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSVmeProjects fetchByprojectsByIdeaIdAndType_First(long ideaId,
		boolean isMockup, OrderByComparator orderByComparator)
		throws SystemException {
		List<CLSVmeProjects> list = findByprojectsByIdeaIdAndType(ideaId,
				isMockup, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s vme projects in the ordered set where ideaId = &#63; and isMockup = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param isMockup the is mockup
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s vme projects
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException if a matching c l s vme projects could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSVmeProjects findByprojectsByIdeaIdAndType_Last(long ideaId,
		boolean isMockup, OrderByComparator orderByComparator)
		throws NoSuchCLSVmeProjectsException, SystemException {
		CLSVmeProjects clsVmeProjects = fetchByprojectsByIdeaIdAndType_Last(ideaId,
				isMockup, orderByComparator);

		if (clsVmeProjects != null) {
			return clsVmeProjects;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(", isMockup=");
		msg.append(isMockup);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSVmeProjectsException(msg.toString());
	}

	/**
	 * Returns the last c l s vme projects in the ordered set where ideaId = &#63; and isMockup = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param isMockup the is mockup
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s vme projects, or <code>null</code> if a matching c l s vme projects could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSVmeProjects fetchByprojectsByIdeaIdAndType_Last(long ideaId,
		boolean isMockup, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByprojectsByIdeaIdAndType(ideaId, isMockup);

		if (count == 0) {
			return null;
		}

		List<CLSVmeProjects> list = findByprojectsByIdeaIdAndType(ideaId,
				isMockup, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s vme projectses before and after the current c l s vme projects in the ordered set where ideaId = &#63; and isMockup = &#63;.
	 *
	 * @param recordId the primary key of the current c l s vme projects
	 * @param ideaId the idea ID
	 * @param isMockup the is mockup
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s vme projects
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException if a c l s vme projects with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSVmeProjects[] findByprojectsByIdeaIdAndType_PrevAndNext(
		long recordId, long ideaId, boolean isMockup,
		OrderByComparator orderByComparator)
		throws NoSuchCLSVmeProjectsException, SystemException {
		CLSVmeProjects clsVmeProjects = findByPrimaryKey(recordId);

		Session session = null;

		try {
			session = openSession();

			CLSVmeProjects[] array = new CLSVmeProjectsImpl[3];

			array[0] = getByprojectsByIdeaIdAndType_PrevAndNext(session,
					clsVmeProjects, ideaId, isMockup, orderByComparator, true);

			array[1] = clsVmeProjects;

			array[2] = getByprojectsByIdeaIdAndType_PrevAndNext(session,
					clsVmeProjects, ideaId, isMockup, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSVmeProjects getByprojectsByIdeaIdAndType_PrevAndNext(
		Session session, CLSVmeProjects clsVmeProjects, long ideaId,
		boolean isMockup, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSVMEPROJECTS_WHERE);

		query.append(_FINDER_COLUMN_PROJECTSBYIDEAIDANDTYPE_IDEAID_2);

		query.append(_FINDER_COLUMN_PROJECTSBYIDEAIDANDTYPE_ISMOCKUP_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSVmeProjectsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(ideaId);

		qPos.add(isMockup);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsVmeProjects);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSVmeProjects> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s vme projectses where ideaId = &#63; and isMockup = &#63; from the database.
	 *
	 * @param ideaId the idea ID
	 * @param isMockup the is mockup
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByprojectsByIdeaIdAndType(long ideaId, boolean isMockup)
		throws SystemException {
		for (CLSVmeProjects clsVmeProjects : findByprojectsByIdeaIdAndType(
				ideaId, isMockup, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(clsVmeProjects);
		}
	}

	/**
	 * Returns the number of c l s vme projectses where ideaId = &#63; and isMockup = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param isMockup the is mockup
	 * @return the number of matching c l s vme projectses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByprojectsByIdeaIdAndType(long ideaId, boolean isMockup)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PROJECTSBYIDEAIDANDTYPE;

		Object[] finderArgs = new Object[] { ideaId, isMockup };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CLSVMEPROJECTS_WHERE);

			query.append(_FINDER_COLUMN_PROJECTSBYIDEAIDANDTYPE_IDEAID_2);

			query.append(_FINDER_COLUMN_PROJECTSBYIDEAIDANDTYPE_ISMOCKUP_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				qPos.add(isMockup);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PROJECTSBYIDEAIDANDTYPE_IDEAID_2 = "clsVmeProjects.ideaId = ? AND ";
	private static final String _FINDER_COLUMN_PROJECTSBYIDEAIDANDTYPE_ISMOCKUP_2 =
		"clsVmeProjects.isMockup = ?";

	public CLSVmeProjectsPersistenceImpl() {
		setModelClass(CLSVmeProjects.class);
	}

	/**
	 * Caches the c l s vme projects in the entity cache if it is enabled.
	 *
	 * @param clsVmeProjects the c l s vme projects
	 */
	@Override
	public void cacheResult(CLSVmeProjects clsVmeProjects) {
		EntityCacheUtil.putResult(CLSVmeProjectsModelImpl.ENTITY_CACHE_ENABLED,
			CLSVmeProjectsImpl.class, clsVmeProjects.getPrimaryKey(),
			clsVmeProjects);

		clsVmeProjects.resetOriginalValues();
	}

	/**
	 * Caches the c l s vme projectses in the entity cache if it is enabled.
	 *
	 * @param clsVmeProjectses the c l s vme projectses
	 */
	@Override
	public void cacheResult(List<CLSVmeProjects> clsVmeProjectses) {
		for (CLSVmeProjects clsVmeProjects : clsVmeProjectses) {
			if (EntityCacheUtil.getResult(
						CLSVmeProjectsModelImpl.ENTITY_CACHE_ENABLED,
						CLSVmeProjectsImpl.class, clsVmeProjects.getPrimaryKey()) == null) {
				cacheResult(clsVmeProjects);
			}
			else {
				clsVmeProjects.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all c l s vme projectses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CLSVmeProjectsImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CLSVmeProjectsImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the c l s vme projects.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CLSVmeProjects clsVmeProjects) {
		EntityCacheUtil.removeResult(CLSVmeProjectsModelImpl.ENTITY_CACHE_ENABLED,
			CLSVmeProjectsImpl.class, clsVmeProjects.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<CLSVmeProjects> clsVmeProjectses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CLSVmeProjects clsVmeProjects : clsVmeProjectses) {
			EntityCacheUtil.removeResult(CLSVmeProjectsModelImpl.ENTITY_CACHE_ENABLED,
				CLSVmeProjectsImpl.class, clsVmeProjects.getPrimaryKey());
		}
	}

	/**
	 * Creates a new c l s vme projects with the primary key. Does not add the c l s vme projects to the database.
	 *
	 * @param recordId the primary key for the new c l s vme projects
	 * @return the new c l s vme projects
	 */
	@Override
	public CLSVmeProjects create(long recordId) {
		CLSVmeProjects clsVmeProjects = new CLSVmeProjectsImpl();

		clsVmeProjects.setNew(true);
		clsVmeProjects.setPrimaryKey(recordId);

		return clsVmeProjects;
	}

	/**
	 * Removes the c l s vme projects with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param recordId the primary key of the c l s vme projects
	 * @return the c l s vme projects that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException if a c l s vme projects with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSVmeProjects remove(long recordId)
		throws NoSuchCLSVmeProjectsException, SystemException {
		return remove((Serializable)recordId);
	}

	/**
	 * Removes the c l s vme projects with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the c l s vme projects
	 * @return the c l s vme projects that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException if a c l s vme projects with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSVmeProjects remove(Serializable primaryKey)
		throws NoSuchCLSVmeProjectsException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CLSVmeProjects clsVmeProjects = (CLSVmeProjects)session.get(CLSVmeProjectsImpl.class,
					primaryKey);

			if (clsVmeProjects == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCLSVmeProjectsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(clsVmeProjects);
		}
		catch (NoSuchCLSVmeProjectsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CLSVmeProjects removeImpl(CLSVmeProjects clsVmeProjects)
		throws SystemException {
		clsVmeProjects = toUnwrappedModel(clsVmeProjects);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(clsVmeProjects)) {
				clsVmeProjects = (CLSVmeProjects)session.get(CLSVmeProjectsImpl.class,
						clsVmeProjects.getPrimaryKeyObj());
			}

			if (clsVmeProjects != null) {
				session.delete(clsVmeProjects);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (clsVmeProjects != null) {
			clearCache(clsVmeProjects);
		}

		return clsVmeProjects;
	}

	@Override
	public CLSVmeProjects updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects clsVmeProjects)
		throws SystemException {
		clsVmeProjects = toUnwrappedModel(clsVmeProjects);

		boolean isNew = clsVmeProjects.isNew();

		CLSVmeProjectsModelImpl clsVmeProjectsModelImpl = (CLSVmeProjectsModelImpl)clsVmeProjects;

		Session session = null;

		try {
			session = openSession();

			if (clsVmeProjects.isNew()) {
				session.save(clsVmeProjects);

				clsVmeProjects.setNew(false);
			}
			else {
				session.merge(clsVmeProjects);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CLSVmeProjectsModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((clsVmeProjectsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSBYIDEAID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsVmeProjectsModelImpl.getOriginalIdeaId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PROJECTSBYIDEAID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSBYIDEAID,
					args);

				args = new Object[] { clsVmeProjectsModelImpl.getIdeaId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PROJECTSBYIDEAID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSBYIDEAID,
					args);
			}

			if ((clsVmeProjectsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSBYIDEAIDANDTYPE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsVmeProjectsModelImpl.getOriginalIdeaId(),
						clsVmeProjectsModelImpl.getOriginalIsMockup()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PROJECTSBYIDEAIDANDTYPE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSBYIDEAIDANDTYPE,
					args);

				args = new Object[] {
						clsVmeProjectsModelImpl.getIdeaId(),
						clsVmeProjectsModelImpl.getIsMockup()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PROJECTSBYIDEAIDANDTYPE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSBYIDEAIDANDTYPE,
					args);
			}
		}

		EntityCacheUtil.putResult(CLSVmeProjectsModelImpl.ENTITY_CACHE_ENABLED,
			CLSVmeProjectsImpl.class, clsVmeProjects.getPrimaryKey(),
			clsVmeProjects);

		return clsVmeProjects;
	}

	protected CLSVmeProjects toUnwrappedModel(CLSVmeProjects clsVmeProjects) {
		if (clsVmeProjects instanceof CLSVmeProjectsImpl) {
			return clsVmeProjects;
		}

		CLSVmeProjectsImpl clsVmeProjectsImpl = new CLSVmeProjectsImpl();

		clsVmeProjectsImpl.setNew(clsVmeProjects.isNew());
		clsVmeProjectsImpl.setPrimaryKey(clsVmeProjects.getPrimaryKey());

		clsVmeProjectsImpl.setRecordId(clsVmeProjects.getRecordId());
		clsVmeProjectsImpl.setVmeProjectId(clsVmeProjects.getVmeProjectId());
		clsVmeProjectsImpl.setVmeProjectName(clsVmeProjects.getVmeProjectName());
		clsVmeProjectsImpl.setIsMockup(clsVmeProjects.isIsMockup());
		clsVmeProjectsImpl.setIdeaId(clsVmeProjects.getIdeaId());

		return clsVmeProjectsImpl;
	}

	/**
	 * Returns the c l s vme projects with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s vme projects
	 * @return the c l s vme projects
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException if a c l s vme projects with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSVmeProjects findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCLSVmeProjectsException, SystemException {
		CLSVmeProjects clsVmeProjects = fetchByPrimaryKey(primaryKey);

		if (clsVmeProjects == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCLSVmeProjectsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return clsVmeProjects;
	}

	/**
	 * Returns the c l s vme projects with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException} if it could not be found.
	 *
	 * @param recordId the primary key of the c l s vme projects
	 * @return the c l s vme projects
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException if a c l s vme projects with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSVmeProjects findByPrimaryKey(long recordId)
		throws NoSuchCLSVmeProjectsException, SystemException {
		return findByPrimaryKey((Serializable)recordId);
	}

	/**
	 * Returns the c l s vme projects with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s vme projects
	 * @return the c l s vme projects, or <code>null</code> if a c l s vme projects with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSVmeProjects fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		CLSVmeProjects clsVmeProjects = (CLSVmeProjects)EntityCacheUtil.getResult(CLSVmeProjectsModelImpl.ENTITY_CACHE_ENABLED,
				CLSVmeProjectsImpl.class, primaryKey);

		if (clsVmeProjects == _nullCLSVmeProjects) {
			return null;
		}

		if (clsVmeProjects == null) {
			Session session = null;

			try {
				session = openSession();

				clsVmeProjects = (CLSVmeProjects)session.get(CLSVmeProjectsImpl.class,
						primaryKey);

				if (clsVmeProjects != null) {
					cacheResult(clsVmeProjects);
				}
				else {
					EntityCacheUtil.putResult(CLSVmeProjectsModelImpl.ENTITY_CACHE_ENABLED,
						CLSVmeProjectsImpl.class, primaryKey,
						_nullCLSVmeProjects);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CLSVmeProjectsModelImpl.ENTITY_CACHE_ENABLED,
					CLSVmeProjectsImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return clsVmeProjects;
	}

	/**
	 * Returns the c l s vme projects with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param recordId the primary key of the c l s vme projects
	 * @return the c l s vme projects, or <code>null</code> if a c l s vme projects with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSVmeProjects fetchByPrimaryKey(long recordId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)recordId);
	}

	/**
	 * Returns all the c l s vme projectses.
	 *
	 * @return the c l s vme projectses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSVmeProjects> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s vme projectses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSVmeProjectsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s vme projectses
	 * @param end the upper bound of the range of c l s vme projectses (not inclusive)
	 * @return the range of c l s vme projectses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSVmeProjects> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s vme projectses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSVmeProjectsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s vme projectses
	 * @param end the upper bound of the range of c l s vme projectses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of c l s vme projectses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSVmeProjects> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CLSVmeProjects> list = (List<CLSVmeProjects>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CLSVMEPROJECTS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CLSVMEPROJECTS;

				if (pagination) {
					sql = sql.concat(CLSVmeProjectsModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CLSVmeProjects>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSVmeProjects>(list);
				}
				else {
					list = (List<CLSVmeProjects>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the c l s vme projectses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (CLSVmeProjects clsVmeProjects : findAll()) {
			remove(clsVmeProjects);
		}
	}

	/**
	 * Returns the number of c l s vme projectses.
	 *
	 * @return the number of c l s vme projectses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CLSVMEPROJECTS);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the c l s vme projects persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CLSVmeProjects>> listenersList = new ArrayList<ModelListener<CLSVmeProjects>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CLSVmeProjects>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CLSVmeProjectsImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CLSVMEPROJECTS = "SELECT clsVmeProjects FROM CLSVmeProjects clsVmeProjects";
	private static final String _SQL_SELECT_CLSVMEPROJECTS_WHERE = "SELECT clsVmeProjects FROM CLSVmeProjects clsVmeProjects WHERE ";
	private static final String _SQL_COUNT_CLSVMEPROJECTS = "SELECT COUNT(clsVmeProjects) FROM CLSVmeProjects clsVmeProjects";
	private static final String _SQL_COUNT_CLSVMEPROJECTS_WHERE = "SELECT COUNT(clsVmeProjects) FROM CLSVmeProjects clsVmeProjects WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "clsVmeProjects.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CLSVmeProjects exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CLSVmeProjects exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CLSVmeProjectsPersistenceImpl.class);
	private static CLSVmeProjects _nullCLSVmeProjects = new CLSVmeProjectsImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CLSVmeProjects> toCacheModel() {
				return _nullCLSVmeProjectsCacheModel;
			}
		};

	private static CacheModel<CLSVmeProjects> _nullCLSVmeProjectsCacheModel = new CacheModel<CLSVmeProjects>() {
			@Override
			public CLSVmeProjects toEntityModel() {
				return _nullCLSVmeProjects;
			}
		};
}