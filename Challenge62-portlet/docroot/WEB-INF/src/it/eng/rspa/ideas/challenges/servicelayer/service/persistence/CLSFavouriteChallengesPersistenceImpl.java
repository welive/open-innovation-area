/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteChallengesImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteChallengesModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the c l s favourite challenges service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSFavouriteChallengesPersistence
 * @see CLSFavouriteChallengesUtil
 * @generated
 */
public class CLSFavouriteChallengesPersistenceImpl extends BasePersistenceImpl<CLSFavouriteChallenges>
	implements CLSFavouriteChallengesPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CLSFavouriteChallengesUtil} to access the c l s favourite challenges persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CLSFavouriteChallengesImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CLSFavouriteChallengesModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteChallengesModelImpl.FINDER_CACHE_ENABLED,
			CLSFavouriteChallengesImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CLSFavouriteChallengesModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteChallengesModelImpl.FINDER_CACHE_ENABLED,
			CLSFavouriteChallengesImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CLSFavouriteChallengesModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteChallengesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CHALLENGEID =
		new FinderPath(CLSFavouriteChallengesModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteChallengesModelImpl.FINDER_CACHE_ENABLED,
			CLSFavouriteChallengesImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByChallengeId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEID =
		new FinderPath(CLSFavouriteChallengesModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteChallengesModelImpl.FINDER_CACHE_ENABLED,
			CLSFavouriteChallengesImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByChallengeId",
			new String[] { Long.class.getName() },
			CLSFavouriteChallengesModelImpl.CHALLENGEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CHALLENGEID = new FinderPath(CLSFavouriteChallengesModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteChallengesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByChallengeId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the c l s favourite challengeses where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @return the matching c l s favourite challengeses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSFavouriteChallenges> findByChallengeId(long challengeId)
		throws SystemException {
		return findByChallengeId(challengeId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s favourite challengeses where challengeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteChallengesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param start the lower bound of the range of c l s favourite challengeses
	 * @param end the upper bound of the range of c l s favourite challengeses (not inclusive)
	 * @return the range of matching c l s favourite challengeses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSFavouriteChallenges> findByChallengeId(long challengeId,
		int start, int end) throws SystemException {
		return findByChallengeId(challengeId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s favourite challengeses where challengeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteChallengesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param start the lower bound of the range of c l s favourite challengeses
	 * @param end the upper bound of the range of c l s favourite challengeses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s favourite challengeses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSFavouriteChallenges> findByChallengeId(long challengeId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEID;
			finderArgs = new Object[] { challengeId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CHALLENGEID;
			finderArgs = new Object[] { challengeId, start, end, orderByComparator };
		}

		List<CLSFavouriteChallenges> list = (List<CLSFavouriteChallenges>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSFavouriteChallenges clsFavouriteChallenges : list) {
				if ((challengeId != clsFavouriteChallenges.getChallengeId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSFAVOURITECHALLENGES_WHERE);

			query.append(_FINDER_COLUMN_CHALLENGEID_CHALLENGEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSFavouriteChallengesModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				if (!pagination) {
					list = (List<CLSFavouriteChallenges>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSFavouriteChallenges>(list);
				}
				else {
					list = (List<CLSFavouriteChallenges>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s favourite challenges in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s favourite challenges
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException if a matching c l s favourite challenges could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteChallenges findByChallengeId_First(long challengeId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSFavouriteChallengesException, SystemException {
		CLSFavouriteChallenges clsFavouriteChallenges = fetchByChallengeId_First(challengeId,
				orderByComparator);

		if (clsFavouriteChallenges != null) {
			return clsFavouriteChallenges;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSFavouriteChallengesException(msg.toString());
	}

	/**
	 * Returns the first c l s favourite challenges in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s favourite challenges, or <code>null</code> if a matching c l s favourite challenges could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteChallenges fetchByChallengeId_First(long challengeId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSFavouriteChallenges> list = findByChallengeId(challengeId, 0,
				1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s favourite challenges in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s favourite challenges
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException if a matching c l s favourite challenges could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteChallenges findByChallengeId_Last(long challengeId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSFavouriteChallengesException, SystemException {
		CLSFavouriteChallenges clsFavouriteChallenges = fetchByChallengeId_Last(challengeId,
				orderByComparator);

		if (clsFavouriteChallenges != null) {
			return clsFavouriteChallenges;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSFavouriteChallengesException(msg.toString());
	}

	/**
	 * Returns the last c l s favourite challenges in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s favourite challenges, or <code>null</code> if a matching c l s favourite challenges could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteChallenges fetchByChallengeId_Last(long challengeId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByChallengeId(challengeId);

		if (count == 0) {
			return null;
		}

		List<CLSFavouriteChallenges> list = findByChallengeId(challengeId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s favourite challengeses before and after the current c l s favourite challenges in the ordered set where challengeId = &#63;.
	 *
	 * @param clsFavouriteChallengesPK the primary key of the current c l s favourite challenges
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s favourite challenges
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException if a c l s favourite challenges with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteChallenges[] findByChallengeId_PrevAndNext(
		CLSFavouriteChallengesPK clsFavouriteChallengesPK, long challengeId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSFavouriteChallengesException, SystemException {
		CLSFavouriteChallenges clsFavouriteChallenges = findByPrimaryKey(clsFavouriteChallengesPK);

		Session session = null;

		try {
			session = openSession();

			CLSFavouriteChallenges[] array = new CLSFavouriteChallengesImpl[3];

			array[0] = getByChallengeId_PrevAndNext(session,
					clsFavouriteChallenges, challengeId, orderByComparator, true);

			array[1] = clsFavouriteChallenges;

			array[2] = getByChallengeId_PrevAndNext(session,
					clsFavouriteChallenges, challengeId, orderByComparator,
					false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSFavouriteChallenges getByChallengeId_PrevAndNext(
		Session session, CLSFavouriteChallenges clsFavouriteChallenges,
		long challengeId, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSFAVOURITECHALLENGES_WHERE);

		query.append(_FINDER_COLUMN_CHALLENGEID_CHALLENGEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSFavouriteChallengesModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(challengeId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsFavouriteChallenges);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSFavouriteChallenges> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s favourite challengeses where challengeId = &#63; from the database.
	 *
	 * @param challengeId the challenge ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByChallengeId(long challengeId) throws SystemException {
		for (CLSFavouriteChallenges clsFavouriteChallenges : findByChallengeId(
				challengeId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(clsFavouriteChallenges);
		}
	}

	/**
	 * Returns the number of c l s favourite challengeses where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @return the number of matching c l s favourite challengeses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByChallengeId(long challengeId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CHALLENGEID;

		Object[] finderArgs = new Object[] { challengeId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSFAVOURITECHALLENGES_WHERE);

			query.append(_FINDER_COLUMN_CHALLENGEID_CHALLENGEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CHALLENGEID_CHALLENGEID_2 = "clsFavouriteChallenges.id.challengeId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID = new FinderPath(CLSFavouriteChallengesModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteChallengesModelImpl.FINDER_CACHE_ENABLED,
			CLSFavouriteChallengesImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserID",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID =
		new FinderPath(CLSFavouriteChallengesModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteChallengesModelImpl.FINDER_CACHE_ENABLED,
			CLSFavouriteChallengesImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUserID",
			new String[] { Long.class.getName() },
			CLSFavouriteChallengesModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(CLSFavouriteChallengesModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteChallengesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserID",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the c l s favourite challengeses where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching c l s favourite challengeses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSFavouriteChallenges> findByUserID(long userId)
		throws SystemException {
		return findByUserID(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s favourite challengeses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteChallengesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of c l s favourite challengeses
	 * @param end the upper bound of the range of c l s favourite challengeses (not inclusive)
	 * @return the range of matching c l s favourite challengeses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSFavouriteChallenges> findByUserID(long userId, int start,
		int end) throws SystemException {
		return findByUserID(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s favourite challengeses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteChallengesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of c l s favourite challengeses
	 * @param end the upper bound of the range of c l s favourite challengeses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s favourite challengeses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSFavouriteChallenges> findByUserID(long userId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<CLSFavouriteChallenges> list = (List<CLSFavouriteChallenges>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSFavouriteChallenges clsFavouriteChallenges : list) {
				if ((userId != clsFavouriteChallenges.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSFAVOURITECHALLENGES_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSFavouriteChallengesModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (!pagination) {
					list = (List<CLSFavouriteChallenges>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSFavouriteChallenges>(list);
				}
				else {
					list = (List<CLSFavouriteChallenges>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s favourite challenges in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s favourite challenges
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException if a matching c l s favourite challenges could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteChallenges findByUserID_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSFavouriteChallengesException, SystemException {
		CLSFavouriteChallenges clsFavouriteChallenges = fetchByUserID_First(userId,
				orderByComparator);

		if (clsFavouriteChallenges != null) {
			return clsFavouriteChallenges;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSFavouriteChallengesException(msg.toString());
	}

	/**
	 * Returns the first c l s favourite challenges in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s favourite challenges, or <code>null</code> if a matching c l s favourite challenges could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteChallenges fetchByUserID_First(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSFavouriteChallenges> list = findByUserID(userId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s favourite challenges in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s favourite challenges
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException if a matching c l s favourite challenges could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteChallenges findByUserID_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSFavouriteChallengesException, SystemException {
		CLSFavouriteChallenges clsFavouriteChallenges = fetchByUserID_Last(userId,
				orderByComparator);

		if (clsFavouriteChallenges != null) {
			return clsFavouriteChallenges;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSFavouriteChallengesException(msg.toString());
	}

	/**
	 * Returns the last c l s favourite challenges in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s favourite challenges, or <code>null</code> if a matching c l s favourite challenges could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteChallenges fetchByUserID_Last(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUserID(userId);

		if (count == 0) {
			return null;
		}

		List<CLSFavouriteChallenges> list = findByUserID(userId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s favourite challengeses before and after the current c l s favourite challenges in the ordered set where userId = &#63;.
	 *
	 * @param clsFavouriteChallengesPK the primary key of the current c l s favourite challenges
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s favourite challenges
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException if a c l s favourite challenges with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteChallenges[] findByUserID_PrevAndNext(
		CLSFavouriteChallengesPK clsFavouriteChallengesPK, long userId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSFavouriteChallengesException, SystemException {
		CLSFavouriteChallenges clsFavouriteChallenges = findByPrimaryKey(clsFavouriteChallengesPK);

		Session session = null;

		try {
			session = openSession();

			CLSFavouriteChallenges[] array = new CLSFavouriteChallengesImpl[3];

			array[0] = getByUserID_PrevAndNext(session, clsFavouriteChallenges,
					userId, orderByComparator, true);

			array[1] = clsFavouriteChallenges;

			array[2] = getByUserID_PrevAndNext(session, clsFavouriteChallenges,
					userId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSFavouriteChallenges getByUserID_PrevAndNext(Session session,
		CLSFavouriteChallenges clsFavouriteChallenges, long userId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSFAVOURITECHALLENGES_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSFavouriteChallengesModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsFavouriteChallenges);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSFavouriteChallenges> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s favourite challengeses where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUserID(long userId) throws SystemException {
		for (CLSFavouriteChallenges clsFavouriteChallenges : findByUserID(
				userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(clsFavouriteChallenges);
		}
	}

	/**
	 * Returns the number of c l s favourite challengeses where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching c l s favourite challengeses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUserID(long userId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID;

		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSFAVOURITECHALLENGES_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_2 = "clsFavouriteChallenges.id.userId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_CHALLENGEIDANDUSERID = new FinderPath(CLSFavouriteChallengesModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteChallengesModelImpl.FINDER_CACHE_ENABLED,
			CLSFavouriteChallengesImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByChallengeIDAndUserID",
			new String[] { Long.class.getName(), Long.class.getName() },
			CLSFavouriteChallengesModelImpl.CHALLENGEID_COLUMN_BITMASK |
			CLSFavouriteChallengesModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CHALLENGEIDANDUSERID = new FinderPath(CLSFavouriteChallengesModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteChallengesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByChallengeIDAndUserID",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns the c l s favourite challenges where challengeId = &#63; and userId = &#63; or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException} if it could not be found.
	 *
	 * @param challengeId the challenge ID
	 * @param userId the user ID
	 * @return the matching c l s favourite challenges
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException if a matching c l s favourite challenges could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteChallenges findByChallengeIDAndUserID(long challengeId,
		long userId)
		throws NoSuchCLSFavouriteChallengesException, SystemException {
		CLSFavouriteChallenges clsFavouriteChallenges = fetchByChallengeIDAndUserID(challengeId,
				userId);

		if (clsFavouriteChallenges == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("challengeId=");
			msg.append(challengeId);

			msg.append(", userId=");
			msg.append(userId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchCLSFavouriteChallengesException(msg.toString());
		}

		return clsFavouriteChallenges;
	}

	/**
	 * Returns the c l s favourite challenges where challengeId = &#63; and userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param challengeId the challenge ID
	 * @param userId the user ID
	 * @return the matching c l s favourite challenges, or <code>null</code> if a matching c l s favourite challenges could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteChallenges fetchByChallengeIDAndUserID(
		long challengeId, long userId) throws SystemException {
		return fetchByChallengeIDAndUserID(challengeId, userId, true);
	}

	/**
	 * Returns the c l s favourite challenges where challengeId = &#63; and userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param challengeId the challenge ID
	 * @param userId the user ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching c l s favourite challenges, or <code>null</code> if a matching c l s favourite challenges could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteChallenges fetchByChallengeIDAndUserID(
		long challengeId, long userId, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { challengeId, userId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_CHALLENGEIDANDUSERID,
					finderArgs, this);
		}

		if (result instanceof CLSFavouriteChallenges) {
			CLSFavouriteChallenges clsFavouriteChallenges = (CLSFavouriteChallenges)result;

			if ((challengeId != clsFavouriteChallenges.getChallengeId()) ||
					(userId != clsFavouriteChallenges.getUserId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_CLSFAVOURITECHALLENGES_WHERE);

			query.append(_FINDER_COLUMN_CHALLENGEIDANDUSERID_CHALLENGEID_2);

			query.append(_FINDER_COLUMN_CHALLENGEIDANDUSERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				qPos.add(userId);

				List<CLSFavouriteChallenges> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CHALLENGEIDANDUSERID,
						finderArgs, list);
				}
				else {
					CLSFavouriteChallenges clsFavouriteChallenges = list.get(0);

					result = clsFavouriteChallenges;

					cacheResult(clsFavouriteChallenges);

					if ((clsFavouriteChallenges.getChallengeId() != challengeId) ||
							(clsFavouriteChallenges.getUserId() != userId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CHALLENGEIDANDUSERID,
							finderArgs, clsFavouriteChallenges);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CHALLENGEIDANDUSERID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (CLSFavouriteChallenges)result;
		}
	}

	/**
	 * Removes the c l s favourite challenges where challengeId = &#63; and userId = &#63; from the database.
	 *
	 * @param challengeId the challenge ID
	 * @param userId the user ID
	 * @return the c l s favourite challenges that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteChallenges removeByChallengeIDAndUserID(
		long challengeId, long userId)
		throws NoSuchCLSFavouriteChallengesException, SystemException {
		CLSFavouriteChallenges clsFavouriteChallenges = findByChallengeIDAndUserID(challengeId,
				userId);

		return remove(clsFavouriteChallenges);
	}

	/**
	 * Returns the number of c l s favourite challengeses where challengeId = &#63; and userId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param userId the user ID
	 * @return the number of matching c l s favourite challengeses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByChallengeIDAndUserID(long challengeId, long userId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CHALLENGEIDANDUSERID;

		Object[] finderArgs = new Object[] { challengeId, userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CLSFAVOURITECHALLENGES_WHERE);

			query.append(_FINDER_COLUMN_CHALLENGEIDANDUSERID_CHALLENGEID_2);

			query.append(_FINDER_COLUMN_CHALLENGEIDANDUSERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CHALLENGEIDANDUSERID_CHALLENGEID_2 =
		"clsFavouriteChallenges.id.challengeId = ? AND ";
	private static final String _FINDER_COLUMN_CHALLENGEIDANDUSERID_USERID_2 = "clsFavouriteChallenges.id.userId = ?";

	public CLSFavouriteChallengesPersistenceImpl() {
		setModelClass(CLSFavouriteChallenges.class);
	}

	/**
	 * Caches the c l s favourite challenges in the entity cache if it is enabled.
	 *
	 * @param clsFavouriteChallenges the c l s favourite challenges
	 */
	@Override
	public void cacheResult(CLSFavouriteChallenges clsFavouriteChallenges) {
		EntityCacheUtil.putResult(CLSFavouriteChallengesModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteChallengesImpl.class,
			clsFavouriteChallenges.getPrimaryKey(), clsFavouriteChallenges);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CHALLENGEIDANDUSERID,
			new Object[] {
				clsFavouriteChallenges.getChallengeId(),
				clsFavouriteChallenges.getUserId()
			}, clsFavouriteChallenges);

		clsFavouriteChallenges.resetOriginalValues();
	}

	/**
	 * Caches the c l s favourite challengeses in the entity cache if it is enabled.
	 *
	 * @param clsFavouriteChallengeses the c l s favourite challengeses
	 */
	@Override
	public void cacheResult(
		List<CLSFavouriteChallenges> clsFavouriteChallengeses) {
		for (CLSFavouriteChallenges clsFavouriteChallenges : clsFavouriteChallengeses) {
			if (EntityCacheUtil.getResult(
						CLSFavouriteChallengesModelImpl.ENTITY_CACHE_ENABLED,
						CLSFavouriteChallengesImpl.class,
						clsFavouriteChallenges.getPrimaryKey()) == null) {
				cacheResult(clsFavouriteChallenges);
			}
			else {
				clsFavouriteChallenges.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all c l s favourite challengeses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CLSFavouriteChallengesImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CLSFavouriteChallengesImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the c l s favourite challenges.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CLSFavouriteChallenges clsFavouriteChallenges) {
		EntityCacheUtil.removeResult(CLSFavouriteChallengesModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteChallengesImpl.class,
			clsFavouriteChallenges.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(clsFavouriteChallenges);
	}

	@Override
	public void clearCache(
		List<CLSFavouriteChallenges> clsFavouriteChallengeses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CLSFavouriteChallenges clsFavouriteChallenges : clsFavouriteChallengeses) {
			EntityCacheUtil.removeResult(CLSFavouriteChallengesModelImpl.ENTITY_CACHE_ENABLED,
				CLSFavouriteChallengesImpl.class,
				clsFavouriteChallenges.getPrimaryKey());

			clearUniqueFindersCache(clsFavouriteChallenges);
		}
	}

	protected void cacheUniqueFindersCache(
		CLSFavouriteChallenges clsFavouriteChallenges) {
		if (clsFavouriteChallenges.isNew()) {
			Object[] args = new Object[] {
					clsFavouriteChallenges.getChallengeId(),
					clsFavouriteChallenges.getUserId()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CHALLENGEIDANDUSERID,
				args, Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CHALLENGEIDANDUSERID,
				args, clsFavouriteChallenges);
		}
		else {
			CLSFavouriteChallengesModelImpl clsFavouriteChallengesModelImpl = (CLSFavouriteChallengesModelImpl)clsFavouriteChallenges;

			if ((clsFavouriteChallengesModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_CHALLENGEIDANDUSERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsFavouriteChallenges.getChallengeId(),
						clsFavouriteChallenges.getUserId()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CHALLENGEIDANDUSERID,
					args, Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CHALLENGEIDANDUSERID,
					args, clsFavouriteChallenges);
			}
		}
	}

	protected void clearUniqueFindersCache(
		CLSFavouriteChallenges clsFavouriteChallenges) {
		CLSFavouriteChallengesModelImpl clsFavouriteChallengesModelImpl = (CLSFavouriteChallengesModelImpl)clsFavouriteChallenges;

		Object[] args = new Object[] {
				clsFavouriteChallenges.getChallengeId(),
				clsFavouriteChallenges.getUserId()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CHALLENGEIDANDUSERID,
			args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CHALLENGEIDANDUSERID,
			args);

		if ((clsFavouriteChallengesModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_CHALLENGEIDANDUSERID.getColumnBitmask()) != 0) {
			args = new Object[] {
					clsFavouriteChallengesModelImpl.getOriginalChallengeId(),
					clsFavouriteChallengesModelImpl.getOriginalUserId()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CHALLENGEIDANDUSERID,
				args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CHALLENGEIDANDUSERID,
				args);
		}
	}

	/**
	 * Creates a new c l s favourite challenges with the primary key. Does not add the c l s favourite challenges to the database.
	 *
	 * @param clsFavouriteChallengesPK the primary key for the new c l s favourite challenges
	 * @return the new c l s favourite challenges
	 */
	@Override
	public CLSFavouriteChallenges create(
		CLSFavouriteChallengesPK clsFavouriteChallengesPK) {
		CLSFavouriteChallenges clsFavouriteChallenges = new CLSFavouriteChallengesImpl();

		clsFavouriteChallenges.setNew(true);
		clsFavouriteChallenges.setPrimaryKey(clsFavouriteChallengesPK);

		return clsFavouriteChallenges;
	}

	/**
	 * Removes the c l s favourite challenges with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param clsFavouriteChallengesPK the primary key of the c l s favourite challenges
	 * @return the c l s favourite challenges that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException if a c l s favourite challenges with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteChallenges remove(
		CLSFavouriteChallengesPK clsFavouriteChallengesPK)
		throws NoSuchCLSFavouriteChallengesException, SystemException {
		return remove((Serializable)clsFavouriteChallengesPK);
	}

	/**
	 * Removes the c l s favourite challenges with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the c l s favourite challenges
	 * @return the c l s favourite challenges that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException if a c l s favourite challenges with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteChallenges remove(Serializable primaryKey)
		throws NoSuchCLSFavouriteChallengesException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CLSFavouriteChallenges clsFavouriteChallenges = (CLSFavouriteChallenges)session.get(CLSFavouriteChallengesImpl.class,
					primaryKey);

			if (clsFavouriteChallenges == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCLSFavouriteChallengesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(clsFavouriteChallenges);
		}
		catch (NoSuchCLSFavouriteChallengesException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CLSFavouriteChallenges removeImpl(
		CLSFavouriteChallenges clsFavouriteChallenges)
		throws SystemException {
		clsFavouriteChallenges = toUnwrappedModel(clsFavouriteChallenges);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(clsFavouriteChallenges)) {
				clsFavouriteChallenges = (CLSFavouriteChallenges)session.get(CLSFavouriteChallengesImpl.class,
						clsFavouriteChallenges.getPrimaryKeyObj());
			}

			if (clsFavouriteChallenges != null) {
				session.delete(clsFavouriteChallenges);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (clsFavouriteChallenges != null) {
			clearCache(clsFavouriteChallenges);
		}

		return clsFavouriteChallenges;
	}

	@Override
	public CLSFavouriteChallenges updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges clsFavouriteChallenges)
		throws SystemException {
		clsFavouriteChallenges = toUnwrappedModel(clsFavouriteChallenges);

		boolean isNew = clsFavouriteChallenges.isNew();

		CLSFavouriteChallengesModelImpl clsFavouriteChallengesModelImpl = (CLSFavouriteChallengesModelImpl)clsFavouriteChallenges;

		Session session = null;

		try {
			session = openSession();

			if (clsFavouriteChallenges.isNew()) {
				session.save(clsFavouriteChallenges);

				clsFavouriteChallenges.setNew(false);
			}
			else {
				session.merge(clsFavouriteChallenges);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CLSFavouriteChallengesModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((clsFavouriteChallengesModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsFavouriteChallengesModelImpl.getOriginalChallengeId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CHALLENGEID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEID,
					args);

				args = new Object[] {
						clsFavouriteChallengesModelImpl.getChallengeId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CHALLENGEID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEID,
					args);
			}

			if ((clsFavouriteChallengesModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsFavouriteChallengesModelImpl.getOriginalUserId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);

				args = new Object[] { clsFavouriteChallengesModelImpl.getUserId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);
			}
		}

		EntityCacheUtil.putResult(CLSFavouriteChallengesModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteChallengesImpl.class,
			clsFavouriteChallenges.getPrimaryKey(), clsFavouriteChallenges);

		clearUniqueFindersCache(clsFavouriteChallenges);
		cacheUniqueFindersCache(clsFavouriteChallenges);

		return clsFavouriteChallenges;
	}

	protected CLSFavouriteChallenges toUnwrappedModel(
		CLSFavouriteChallenges clsFavouriteChallenges) {
		if (clsFavouriteChallenges instanceof CLSFavouriteChallengesImpl) {
			return clsFavouriteChallenges;
		}

		CLSFavouriteChallengesImpl clsFavouriteChallengesImpl = new CLSFavouriteChallengesImpl();

		clsFavouriteChallengesImpl.setNew(clsFavouriteChallenges.isNew());
		clsFavouriteChallengesImpl.setPrimaryKey(clsFavouriteChallenges.getPrimaryKey());

		clsFavouriteChallengesImpl.setChallengeId(clsFavouriteChallenges.getChallengeId());
		clsFavouriteChallengesImpl.setUserId(clsFavouriteChallenges.getUserId());

		return clsFavouriteChallengesImpl;
	}

	/**
	 * Returns the c l s favourite challenges with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s favourite challenges
	 * @return the c l s favourite challenges
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException if a c l s favourite challenges with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteChallenges findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCLSFavouriteChallengesException, SystemException {
		CLSFavouriteChallenges clsFavouriteChallenges = fetchByPrimaryKey(primaryKey);

		if (clsFavouriteChallenges == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCLSFavouriteChallengesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return clsFavouriteChallenges;
	}

	/**
	 * Returns the c l s favourite challenges with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException} if it could not be found.
	 *
	 * @param clsFavouriteChallengesPK the primary key of the c l s favourite challenges
	 * @return the c l s favourite challenges
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException if a c l s favourite challenges with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteChallenges findByPrimaryKey(
		CLSFavouriteChallengesPK clsFavouriteChallengesPK)
		throws NoSuchCLSFavouriteChallengesException, SystemException {
		return findByPrimaryKey((Serializable)clsFavouriteChallengesPK);
	}

	/**
	 * Returns the c l s favourite challenges with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s favourite challenges
	 * @return the c l s favourite challenges, or <code>null</code> if a c l s favourite challenges with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteChallenges fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		CLSFavouriteChallenges clsFavouriteChallenges = (CLSFavouriteChallenges)EntityCacheUtil.getResult(CLSFavouriteChallengesModelImpl.ENTITY_CACHE_ENABLED,
				CLSFavouriteChallengesImpl.class, primaryKey);

		if (clsFavouriteChallenges == _nullCLSFavouriteChallenges) {
			return null;
		}

		if (clsFavouriteChallenges == null) {
			Session session = null;

			try {
				session = openSession();

				clsFavouriteChallenges = (CLSFavouriteChallenges)session.get(CLSFavouriteChallengesImpl.class,
						primaryKey);

				if (clsFavouriteChallenges != null) {
					cacheResult(clsFavouriteChallenges);
				}
				else {
					EntityCacheUtil.putResult(CLSFavouriteChallengesModelImpl.ENTITY_CACHE_ENABLED,
						CLSFavouriteChallengesImpl.class, primaryKey,
						_nullCLSFavouriteChallenges);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CLSFavouriteChallengesModelImpl.ENTITY_CACHE_ENABLED,
					CLSFavouriteChallengesImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return clsFavouriteChallenges;
	}

	/**
	 * Returns the c l s favourite challenges with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param clsFavouriteChallengesPK the primary key of the c l s favourite challenges
	 * @return the c l s favourite challenges, or <code>null</code> if a c l s favourite challenges with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteChallenges fetchByPrimaryKey(
		CLSFavouriteChallengesPK clsFavouriteChallengesPK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)clsFavouriteChallengesPK);
	}

	/**
	 * Returns all the c l s favourite challengeses.
	 *
	 * @return the c l s favourite challengeses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSFavouriteChallenges> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s favourite challengeses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteChallengesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s favourite challengeses
	 * @param end the upper bound of the range of c l s favourite challengeses (not inclusive)
	 * @return the range of c l s favourite challengeses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSFavouriteChallenges> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s favourite challengeses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteChallengesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s favourite challengeses
	 * @param end the upper bound of the range of c l s favourite challengeses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of c l s favourite challengeses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSFavouriteChallenges> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CLSFavouriteChallenges> list = (List<CLSFavouriteChallenges>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CLSFAVOURITECHALLENGES);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CLSFAVOURITECHALLENGES;

				if (pagination) {
					sql = sql.concat(CLSFavouriteChallengesModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CLSFavouriteChallenges>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSFavouriteChallenges>(list);
				}
				else {
					list = (List<CLSFavouriteChallenges>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the c l s favourite challengeses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (CLSFavouriteChallenges clsFavouriteChallenges : findAll()) {
			remove(clsFavouriteChallenges);
		}
	}

	/**
	 * Returns the number of c l s favourite challengeses.
	 *
	 * @return the number of c l s favourite challengeses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CLSFAVOURITECHALLENGES);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the c l s favourite challenges persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CLSFavouriteChallenges>> listenersList = new ArrayList<ModelListener<CLSFavouriteChallenges>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CLSFavouriteChallenges>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CLSFavouriteChallengesImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CLSFAVOURITECHALLENGES = "SELECT clsFavouriteChallenges FROM CLSFavouriteChallenges clsFavouriteChallenges";
	private static final String _SQL_SELECT_CLSFAVOURITECHALLENGES_WHERE = "SELECT clsFavouriteChallenges FROM CLSFavouriteChallenges clsFavouriteChallenges WHERE ";
	private static final String _SQL_COUNT_CLSFAVOURITECHALLENGES = "SELECT COUNT(clsFavouriteChallenges) FROM CLSFavouriteChallenges clsFavouriteChallenges";
	private static final String _SQL_COUNT_CLSFAVOURITECHALLENGES_WHERE = "SELECT COUNT(clsFavouriteChallenges) FROM CLSFavouriteChallenges clsFavouriteChallenges WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "clsFavouriteChallenges.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CLSFavouriteChallenges exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CLSFavouriteChallenges exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CLSFavouriteChallengesPersistenceImpl.class);
	private static CLSFavouriteChallenges _nullCLSFavouriteChallenges = new CLSFavouriteChallengesImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CLSFavouriteChallenges> toCacheModel() {
				return _nullCLSFavouriteChallengesCacheModel;
			}
		};

	private static CacheModel<CLSFavouriteChallenges> _nullCLSFavouriteChallengesCacheModel =
		new CacheModel<CLSFavouriteChallenges>() {
			@Override
			public CLSFavouriteChallenges toEntityModel() {
				return _nullCLSFavouriteChallenges;
			}
		};
}