package it.eng.rspa.ideas.challenges.search;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalServiceUtil;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.portlet.WindowStateException;

import org.apache.commons.lang3.StringEscapeUtils;

import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.LiferayWindowState;
import com.liferay.portal.kernel.search.BaseIndexer;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.DocumentImpl;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchEngineUtil;
import com.liferay.portal.kernel.search.Summary;
import com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetTagLocalServiceUtil;

public class ChallengeIndexer extends BaseIndexer {

	public static final String[] CLASS_NAMES = {it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge.class.getName()};
	//public static final String PORTLET_ID = "Challenges_WAR_Challengesportlet";
	public static final String PORTLET_ID = "Challenges_WAR_Challenge62portlet";
	
	@Override
	public String[] getClassNames() {
		return CLASS_NAMES;
	}

	@Override
	public String getPortletId() {
		return PORTLET_ID;
	}

	@Override
	protected void doDelete(Object obj) throws Exception {		
		//Set<String> seids = SearchEngineUtil.getSearchEngineIds();
		
		CLSChallenge challenge = (CLSChallenge)obj;
		Document document = new DocumentImpl();
		document.addUID(PORTLET_ID, challenge.getPrimaryKey());
		SearchEngineUtil.deleteDocument(SearchEngineUtil.SYSTEM_ENGINE_ID, challenge.getCompanyId(), document.get(Field.UID)); //(challenge.getCompanyId(), document.get(Field.UID));
	}

	@Override
	protected Document doGetDocument(Object obj) throws Exception {
		CLSChallenge challenge = (CLSChallenge)obj;
		long companyId = challenge.getCompanyId();
		long groupId = getParentGroupId(challenge.getGroupId());
		long scopeGroupId = challenge.getGroupId();
		long userId = challenge.getUserId();
		long resourcePrimKey = challenge.getPrimaryKey();
		
		String title = challenge.getChallengeTitle();
		
		String content = challenge.getChallengeDescription();
		String regex ="\\<[^\\>]*\\>";
		String replacement = "";
		content = content.replaceAll(regex, replacement);
		content = StringEscapeUtils.unescapeHtml4(content);
		String description = content;
		
		Date modifiedDate = challenge.getDateAdded();
		
		long[] assetCategoryIds = AssetCategoryLocalServiceUtil.getCategoryIds(CLSChallenge.class.getName(), resourcePrimKey);
		String[] assetCategoryNames = AssetCategoryLocalServiceUtil.getCategoryNames(CLSChallenge.class.getName(), resourcePrimKey);
		String[] assetTagNames = AssetTagLocalServiceUtil.getTagNames(CLSChallenge.class.getName(), resourcePrimKey);
		
		Document document = new DocumentImpl();
		
		document.addUID(PORTLET_ID, resourcePrimKey);
		document.addModifiedDate(modifiedDate);
		document.addKeyword(Field.COMPANY_ID, companyId);
		document.addKeyword(Field.PORTLET_ID, PORTLET_ID);
		document.addKeyword(Field.GROUP_ID, groupId);
		document.addKeyword(Field.SCOPE_GROUP_ID, scopeGroupId);
		document.addKeyword(Field.USER_ID, userId);
		document.addText(Field.TITLE, title);
		document.addText(Field.CONTENT, content);
		document.addText(Field.DESCRIPTION, description);
		document.addKeyword(Field.ASSET_CATEGORY_IDS, assetCategoryIds);
		document.addKeyword(Field.ASSET_CATEGORY_NAMES, assetCategoryNames);
		document.addKeyword(Field.ASSET_TAG_NAMES, assetTagNames);
		document.addKeyword(Field.ENTRY_CLASS_NAME,CLSChallenge.class.getName());
		document.addKeyword(Field.ENTRY_CLASS_PK, resourcePrimKey);
		
		return document;
	}

	@Override
	protected Summary doGetSummary(Document document, Locale locale, String snippet, PortletURL portletURL) throws Exception {

		
		LiferayPortletURL liferayPortletURL = (LiferayPortletURL)portletURL;

		liferayPortletURL.setLifecycle(PortletRequest.ACTION_PHASE);
		
		try {
			liferayPortletURL.setWindowState(LiferayWindowState.MAXIMIZED);
		}
		catch (WindowStateException wse) {
		}
		
		String resourcePrimKey = document.get(Field.ENTRY_CLASS_PK);
		
		portletURL.setParameter("jspPage", "html/challenges/view_challenge.jsp");
		portletURL.setParameter("challengeId", resourcePrimKey);
		
		System.out.println(portletURL.toString());
		
		Summary summary = createSummary(document, Field.TITLE, Field.CONTENT);

		summary.setMaxContentLength(200);
		summary.setPortletURL(portletURL);
		
		
		
		return summary;//new Summary(title, content, portletURL);
	}

	@Override
	protected void doReindex(Object obj) throws Exception {
		CLSChallenge challenge = (CLSChallenge)obj;
		SearchEngineUtil.updateDocument(SearchEngineUtil.SYSTEM_ENGINE_ID, challenge.getCompanyId(), getDocument(challenge));
	}

	@Override
	protected void doReindex(String className, long classPK) throws Exception {
		if(className.equals(CLSChallenge.class.getName())){
			CLSChallengeLocalServiceUtil.getCLSChallenge(classPK);
		}
	}

	@Override
	protected void doReindex(String[] ids) throws Exception {

		List<CLSChallenge> challenges = CLSChallengeLocalServiceUtil.getCLSChallenges(com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS, com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS);
		Iterator<CLSChallenge> challengesIt = challenges.iterator();
		while (challengesIt.hasNext()) {
			CLSChallenge clsChallenge = (CLSChallenge) challengesIt.next();
			doReindex(clsChallenge);
		}
	}


	@Override
	protected String getPortletId(SearchContext searchContext) {
		return PORTLET_ID;
	}

}
