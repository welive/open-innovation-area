package it.eng.rspa.ideas.utils;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;

import java.util.Comparator;

public class ChallengeComparatorByStartDate implements Comparator<CLSChallenge> {

	@Override
	public int compare(CLSChallenge gara1, CLSChallenge gara2) {
		
		int ret=gara2.getDateStart().compareTo(gara1.getDateStart()); //mette le piu nuove in testa
		
		return ret;
		
	}

}
