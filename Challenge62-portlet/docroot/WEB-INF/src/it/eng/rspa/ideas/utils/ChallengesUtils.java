/**
 * 
 */
package it.eng.rspa.ideas.utils;


import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria;
import it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCategoriesSetForChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengePoiLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteChallengesLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.EvaluationCriteriaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.NeedLinkedChallengeLocalServiceUtil;
import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.ResourceRequest;

import org.apache.commons.lang3.StringEscapeUtils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.SubscriptionLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.asset.model.AssetCategory;
import com.liferay.portlet.asset.model.AssetEntry;
import com.liferay.portlet.asset.model.AssetVocabulary;
import com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.liferay.portlet.ratings.service.RatingsStatsLocalServiceUtil;


/**
 * @author Engineering
 *
 */
public class ChallengesUtils {
	
	
	/**
	 * Returns a JSON array with the list of events to display populating the card with Ajax
	 * @param resourceRequest
	 * @return
	 */
	public static JSONArray getListaGareAjax ( ResourceRequest resourceRequest, String pilot, Map<String, String> stateFilters, Map<String, String> vocabFiltri,  
			Map<String, String> persFiltri, Map<String, String> sorting){
		
		User user = (User) resourceRequest.getAttribute(WebKeys.USER);
		
		JSONArray jsonArRisp = JSONFactoryUtil.createJSONArray();
		Locale locale = resourceRequest.getLocale();
		
		List<CLSChallenge> gareF = new ArrayList<CLSChallenge>();
		gareF = getListeGareFiltrate(resourceRequest, pilot, stateFilters, vocabFiltri, persFiltri, sorting);
		
		
		for (int i = 0; i<gareF.size(); i++){
			
			CLSChallenge gara = gareF.get(i); //get the current challenge
			
			JSONObject jsonGara = JSONFactoryUtil.createJSONObject();
			
			jsonGara.put("idChallenge", gara.getChallengeId());
			
			
			String vocabolario = getVocabularyNameByChallengeId(gara.getChallengeId(),locale );
			jsonGara.put("Vocabulary", vocabolario);
			
			//I throw those in English because the colors checks are made on the English names
			String nomeVocabularyEN = getVocabularyNameByChallengeId(gara.getChallengeId(), Locale.ENGLISH);
			Map<String, String > graphVoc = IdeasListUtils.getGraphicalElementForVocabulary(nomeVocabularyEN);
			String iconVocabulary = graphVoc.get("iconVocabulary");
			String colorVocabulary = graphVoc.get("colorVocabulary");
			jsonGara.put("iconVocabulary", iconVocabulary);
			jsonGara.put("colorVocabulary",colorVocabulary);
			
			String nomeAutore = getAuthorNameByChallenge(gara);
			jsonGara.put("Author", nomeAutore);
			
			String nomeOrganizationAutore = getAuthorOrganizationNameByChallenge(gara);
			jsonGara.put("AuthorOrganization", nomeOrganizationAutore);
			
			jsonGara.put("Title", gara.getChallengeTitle());
			
			String dataStart = MyUtils.getDateDDMMYYYYByDate(gara.getDateStart());
			jsonGara.put("StartDate", dataStart);
			
			String dataEnd = MyUtils.getDateDDMMYYYYByDate(gara.getDateEnd());
			jsonGara.put("EndDate", dataEnd);
			
			int ratingStar = getAverageRatingsByChallenge(gara);		
			jsonGara.put("RatingsStar", ratingStar);
			
			String imgUrl = getRepresentativeImgUrlByChallenge(gara, resourceRequest.getContextPath());
			jsonGara.put("ImageURL", imgUrl);
			
			jsonGara.put("Description", getSocialSummaryByChallengeNotLimited(gara));
			
			String fUGara=IdeaManagementSystemProperties.getFriendlyUrlChallenges(gara.getChallengeId());
			jsonGara.put("GaraFriendlyUrl", fUGara);
			
			int numIdeas= getNumIdeasByChallengeId(gara.getChallengeId());
			jsonGara.put("NumIdeas", numIdeas );
			
			String challengeStatus = gara.getChallengeStatus();
			
			//For legacy
			if (gara.getChallengeStatus().isEmpty())
				challengeStatus = MyConstants.CHALLENGE_STATE_OPEN;
			
			jsonGara.put("Status", challengeStatus);
			
			jsonGara.put("isAuthor", isAuthor(gara, user)); 
			jsonGara.put("isActive", isActiveChallenge(gara)); 
			
			
			jsonArRisp.put(jsonGara);
			
		}
		
		
		
		return jsonArRisp;
	}
	
	/**
	 * @param ideasI
	 * @param statiFiltri
	 * @return
	 */
	private static List<CLSChallenge> getListeGareFiltrate(ResourceRequest resourceRequest, String pilot,  Map<String, String> stateFilters,
			Map<String, String> vocabFiltri, Map<String, String> persFiltri, Map<String, String> sorting){
		
		Locale locale = resourceRequest.getLocale();
		
		boolean activeChallengeF = Boolean.valueOf(persFiltri.get("activeChallenge"));
		boolean inactiveChallengeF = Boolean.valueOf(persFiltri.get("inactiveChallenge"));
		boolean myChallengesF = Boolean.valueOf(persFiltri.get("myChallenges"));
		boolean onlyMyPilotF = Boolean.valueOf(persFiltri.get("onlyMyPilot"));
		boolean favoriteChallengesF = Boolean.valueOf(persFiltri.get("favoriteChallenges"));
		
		
		List<CLSChallenge> gareI = new ArrayList<CLSChallenge>();
		List<CLSChallenge> gareFav = new ArrayList<CLSChallenge>();
		List<CLSChallenge> garePers = new ArrayList<CLSChallenge>();
		
		User user = (User) resourceRequest.getAttribute(WebKeys.USER);
		
		try {
			
			if (myChallengesF  || favoriteChallengesF){
				
				long userId = user.getUserId();
				
				if (favoriteChallengesF){
					gareFav = CLSFavouriteChallengesLocalServiceUtil.getFavouriteChallengesByUserId(userId);
					gareI=gareFav;
				}
				if (myChallengesF){
					garePers = CLSChallengeLocalServiceUtil.getChallengesByUserId(userId);
					gareI=garePers;
				}	
				
				if (myChallengesF && favoriteChallengesF){
					

					gareI = new ArrayList<CLSChallenge>(gareFav);
					
					gareI.retainAll(garePers);//It is the intersection of the two lists
					
				}
				
				
			}else{//I am the guest user and I get all
				
				gareI= CLSChallengeLocalServiceUtil.getCLSChallenges(0, CLSChallengeLocalServiceUtil.getCLSChallengesCount());
				
			}
			
			
			
			
		} catch (SystemException e) {
			
			e.printStackTrace();
			return gareI;
		}
		
		
		List<CLSChallenge> gareFiltrate = new ArrayList<CLSChallenge>();
		

		
		for (int i = 0; i<gareI.size(); i++){
			
			CLSChallenge gara = gareI.get(i);
			
			String garaVocabulary = getVocabularyNameByChallengeId(gara.getChallengeId(), locale);
			String stateChallenge = gara.getChallengeStatus();
			
			//for legacy
			if (gara.getChallengeStatus().isEmpty())
					stateChallenge= MyConstants.CHALLENGE_STATE_OPEN;
			
			boolean valueFilterState = Boolean.valueOf(stateFilters.get(stateChallenge));
			boolean valoreFiltroVocab = Boolean.valueOf(vocabFiltri.get(garaVocabulary));
			
			boolean garaAttiva = isActiveChallenge(gara);
			boolean filtroAttiva = false;
			
			if ((activeChallengeF && garaAttiva) || (inactiveChallengeF && !garaAttiva))
				filtroAttiva = true;
			
			boolean filtroOnlyMyPilot = false;
			boolean isChallengeOfUserPilot = isChallengeOfCurrentPilot(gara, pilot);
			
			if (( onlyMyPilotF && isChallengeOfUserPilot) || (!onlyMyPilotF))
				filtroOnlyMyPilot = true;
			
			if (valueFilterState && valoreFiltroVocab && filtroAttiva && filtroOnlyMyPilot)
				gareFiltrate.add(gara);
			
		}
		
		//Filtering on pilot
		gareFiltrate = MyUtils.getListaGareFiltrataPerPilotLingue(gareFiltrate, user, pilot);

		
		///////////  SORTING ///////////
		boolean sortByStartDate = Boolean.valueOf(sorting.get("sortByStartDate"));
		boolean sortStartDateAsc = Boolean.valueOf(sorting.get("sortStartDateAsc"));
		boolean sortByEndDate = Boolean.valueOf(sorting.get("sortByEndDate"));
		boolean sortEndDateAsc = Boolean.valueOf(sorting.get("sortEndDateAsc"));
		boolean sortByRating = Boolean.valueOf(sorting.get("sortByRating"));
		boolean sortRatingLower = Boolean.valueOf(sorting.get("sortRatingLower"));
		
		
		if (sortByStartDate){
			ChallengeComparatorByStartDate	ccbsd = new ChallengeComparatorByStartDate();
			gareFiltrate=ListUtil.sort(gareFiltrate, ccbsd);	
			
			if (sortStartDateAsc)			
				Collections.reverse(gareFiltrate);//reverse order of the list sorted by start date
			
		}else if  (sortByEndDate){
			
			ChallengeComparatorByEndDate ccbed = new ChallengeComparatorByEndDate();
			gareFiltrate=ListUtil.sort(gareFiltrate, ccbed);	
			
			if (sortEndDateAsc)			
				Collections.reverse(gareFiltrate);//reverse order of the list sorted by end date
			
		}else if  (sortByRating){
			
			ChallengeComparatorByRating ccbr = new ChallengeComparatorByRating();
			gareFiltrate=ListUtil.sort(gareFiltrate, ccbr);	
			
			if (sortRatingLower)
				Collections.reverse(gareFiltrate);//reverse order of the list sorted by rating
			
		}
		
		
		return gareFiltrate;
	}
	


	/**
	 * @param userId
	 * @return
	 */
	public static List<CLSChallenge>  getActiveChallengesByAuthorityId (long userId){
		
		List<CLSChallenge> gare = new ArrayList<CLSChallenge>();
		
		
		try {
			gare = CLSChallengeLocalServiceUtil.getActiveChallenges();
		} catch (SystemException e) {
			
			e.printStackTrace();
			return gare;
		}
		
		
		List<CLSChallenge> gareFinal = new ArrayList<CLSChallenge>();
		
		for (CLSChallenge challenge : gare) {
			
			if(challenge.getUserId()==userId)
				gareFinal.add(challenge);
		}
			
		
		
		return gareFinal;
		
	}

	
	/**
	 * @param ideaId
	 * @param Locale locale
	 * @return
	 */
	public static String getVocabularyNameByChallengeId(long challengeId, Locale locale){
		
			String nomeVocabolario = "";
			
			long vocId = getVocabularyIdByChallengeId(challengeId);

			
			if (vocId>-1){
			
				AssetVocabulary voc = null; 
				
				try {
					 voc = AssetVocabularyLocalServiceUtil.getVocabulary(vocId);
				} catch (PortalException | SystemException e) {
					
					e.printStackTrace();
					return null;
				}
				
				nomeVocabolario = voc.getTitle(locale);
			}
			
		
		return nomeVocabolario;

	}
	
	/**
	 * @param ideaId
	 * @param String locale
	 * @return
	 */
	public static String getVocabularyNameByChallengeId(long challengeId, String locale){
		
			String nomeVocabolario = "";
			
			long vocId = getVocabularyIdByChallengeId(challengeId);

			
			if (vocId>-1){
			
				AssetVocabulary voc = null; 
				
				try {
					 voc = AssetVocabularyLocalServiceUtil.getVocabulary(vocId);
				} catch (PortalException | SystemException e) {
					
					e.printStackTrace();
					return null;
				}
				
				nomeVocabolario = voc.getTitle(locale);
			}
			
		
		return nomeVocabolario;

	}
	
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static long getVocabularyIdByChallengeId(long challengeId){
		
			long vocId = -1;
			
			List<CLSCategoriesSetForChallenge> categoriesList = new ArrayList<CLSCategoriesSetForChallenge>();
			try {
				categoriesList = CLSCategoriesSetForChallengeLocalServiceUtil.getCategoriesSetForChallenge(challengeId);
			} catch (SystemException | PortletException e1) {
				
				e1.printStackTrace();
				return vocId;
			}
			
			
			if (categoriesList.size() > 0){
				
				CLSCategoriesSetForChallenge	categorySet = categoriesList.get(0);// sempre e solo uno
				
				AssetVocabulary vocabulary;
				try {
					vocabulary = AssetVocabularyLocalServiceUtil.fetchAssetVocabulary(categorySet.getCategoriesSetID());
					vocId = vocabulary.getVocabularyId();
				} catch (SystemException e) {
					
					e.printStackTrace();
					return vocId;
				}
			
			}
			
		return vocId;

	}
	
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static String getTagsVirgoleByChallengeId(long challengeId){

		String[] tags = {""} ;
		String allTags = ""; 
		
		AssetEntry entry;
		try {
			entry = AssetEntryLocalServiceUtil.getEntry(CLSChallenge.class.getName(), challengeId);
			tags = entry.getTagNames();
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return allTags;
		}
		
		for (int i =0; i<tags.length; i++ ){
			
			if (i!=tags.length-1){
				allTags+=tags[i]+",&nbsp;";
			}else{
				allTags+=tags[i];
				
			}
		}

		 return allTags;
	}
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static String getTagsCommasByChallengeId(long challengeId){

		String[] tags = {""} ;
		String allTags = ""; 
		
		AssetEntry entry;
		try {
			entry = AssetEntryLocalServiceUtil.getEntry(CLSChallenge.class.getName(), challengeId);
			tags = entry.getTagNames();
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return allTags;
		}
		
		for (int i =0; i<tags.length; i++ ){
			
			if (i!=tags.length-1){
				allTags+=tags[i]+",";
			}else{
				allTags+=tags[i];
				
			}
		}

		 
		 return allTags;
	}
	
	
	/**
	 * @param challengeId
	 * @return
	 */
	public static String[] getTagsArrayByChallengeId(long challengeId){

		String[] tags = {""} ;
		
		
		AssetEntry entry;
		try {
			entry = AssetEntryLocalServiceUtil.getEntry(CLSChallenge.class.getName(), challengeId);
			tags = entry.getTagNames();
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			
		}
		
		 
		 return tags;
	}
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static String getTagsCommasForArrayJSByChallengeId(long challengeId){

		String[] tags = {""} ;
		String allTags = ""; 
		
		AssetEntry entry;
		try {
			entry = AssetEntryLocalServiceUtil.getEntry(CLSChallenge.class.getName(), challengeId);
			tags = entry.getTagNames();
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return allTags;
		}
		
		for (int i =0; i<tags.length; i++ ){
			
			if (i!=tags.length-1){
				allTags+="\""+tags[i]+"\",";
			}else{
				allTags+="\""+tags[i]+"\"";
				
			}
		}

		 
		 return allTags;
	}

	/**
	 * @param challenge
	 * @return
	 */
	public static String getAuthorNameByChallenge(CLSChallenge challenge){
		
		String name ="";
		
		try {
			User user = UserLocalServiceUtil.getUser(challenge.getUserId());
			name=user.getFullName();
			
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return name;
		}
		
		return name;
		
		
	}
	
	
	/**
	 * @param challenge
	 * @return
	 */
	public static String getAuthorOrganizationNameByChallenge(CLSChallenge challenge){
		
		long orgId = MyUtils.getOrganizationIdByMunicipalityId(challenge.getUserId());
		return MyUtils.getOrganizationNameByOrganizationId(orgId);
	}
	
	/**
	 * @param challenge
	 * @return
	 */
	public static String getAuthorNameByChallengeAndUser(CLSChallenge challenge, User currentUser){
		
		String name = getAuthorOrganizationNameByChallenge (challenge);
		
		boolean isEnte = MyUtils.isAuthority(currentUser);
		
		if (isEnte){
			
			name = getAuthorNameByChallenge(challenge)+" - "+name;
			
		}
			
		return name;			
			
	}
	

	/**
	 * @param challenge
	 * @return
	 */
	public static double getAverageDoubleRatingsByChallenge (CLSChallenge challenge){
		
		double ratingsStats=0;

		try {
			ratingsStats = RatingsStatsLocalServiceUtil.getStats(CLSChallenge.class.getName(), challenge.getChallengeId()).getAverageScore();
		} catch (SystemException e) {
			
			e.printStackTrace();
			return ratingsStats;
		}
		
		return ratingsStats;
	}
	
	
	
	/**
	 * @param challenge
	 * @return
	 */
	public static int getAverageRatingsByChallenge (CLSChallenge challenge){
		
		int black = 0;
		
		double average = getAverageDoubleRatingsByChallenge(challenge);
		
		black=(int) Math.rint(average); //stelle piene
		
		return black;
	}
	
	
	/**
	 * @param challenge
	 * @param resourceRequest
	 * @return
	 */
	public static String getRepresentativeImgUrlByChallenge (CLSChallenge challenge, String contextPath){
		
		String representativeImgUrl =contextPath+ "/img/challengesImg.png";
		
		if(!challenge.getRepresentativeImgUrl().trim().equals(""))			
			representativeImgUrl = challenge.getRepresentativeImgUrl();
		
		
		return representativeImgUrl;
	}
	
	/**
	 * @param challenge
	 * @return
	 */
	public static String getRepresentativeImgUrlByChallenge (CLSChallenge challenge){
		
		return getRepresentativeImgUrlByChallenge(challenge, "/Challenge62-portlet");
		
	}
	
	
	/**
	 * @param challengeId
	 * @return
	 */
	public static int getNumIdeasByChallengeId (long challengeId){
	
		
		int numIdeas = 0;
		
		List<CLSIdea> ideasforChallenge = new ArrayList<CLSIdea>();
		try {
			 ideasforChallenge = CLSIdeaLocalServiceUtil.getIdeasByChallengeId(challengeId);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return numIdeas;
		}
		
		numIdeas = ideasforChallenge.size();
		
		return numIdeas;
		
	}

	

	
	
	/**
	 * @param idea
	 * @return
	 */
	public static String getColorForVocabularyByChallenge( CLSChallenge challenge){
		
		
		String nomeVocabularyEN = getVocabularyNameByChallengeId(challenge.getChallengeId(), Locale.ENGLISH);
		
		
		Map<String, String > graphVoc = IdeasListUtils.getGraphicalElementForVocabulary(nomeVocabularyEN);
		String colorVocabulary = graphVoc.get("colorVocabulary");
		
		return colorVocabulary;
	}
	
	
	/**
	 * @param idea
	 * @return
	 */
	public static String getIconForVocabularyByChallenge( CLSChallenge challenge){
		
		String nomeVocabularyEN = getVocabularyNameByChallengeId(challenge.getChallengeId(), Locale.ENGLISH);
		
		Map<String, String > graphVoc = IdeasListUtils.getGraphicalElementForVocabulary(nomeVocabularyEN);
		String iconVocabulary = graphVoc.get("iconVocabulary");
		
		return iconVocabulary;
	}
	
	
	
	/**
	 * @param challenge
	 * @return
	 */
	public static String getSocialSummaryByChallenge(CLSChallenge challenge){
		
		
		String summary = challenge.getChallengeDescription().replaceAll("[\\r\\n]", "") ;
		summary = StringEscapeUtils.unescapeHtml4(summary);
		if(summary!=null){
			if(summary.trim()!=""){
				String regex ="\\<[^\\>]*\\>";
		   		String replacement = "";
		   		summary = summary.replaceAll(regex, replacement);
		   		summary = summary.replaceAll("\"", replacement);
				summary = StringUtil.shorten(HtmlUtil.stripHtml(summary), 200);
			}else{
				summary="";
			}
		}else{
			summary="";
		}
		
		return summary;
		
		
	}
	
	/**
	 * @param challenge
	 * @return
	 */
	public static String getSocialSummaryByChallengeNotLimited(CLSChallenge challenge){
		
		
		String summary = challenge.getChallengeDescription().replaceAll("[\\r\\n]", "") ;
		summary = StringEscapeUtils.unescapeHtml4(summary);
		if(summary!=null){
			if(summary.trim()!=""){
				String regex ="\\<[^\\>]*\\>";
		   		String replacement = "";
		   		summary = summary.replaceAll(regex, replacement);
		   		summary = summary.replaceAll("\"", replacement);
				//summary = StringUtil.shorten(HtmlUtil.stripHtml(summary), 200);
			}else{
				summary="";
			}
		}else{
			summary="";
		}
		
		return summary;
		
		
	}
	
	/**
	 * @param challenge
	 * @return
	 */
	public static boolean isActiveChallenge (CLSChallenge challenge){
		
		Date now = new Date();
		
		if (now.before(challenge.getDateEnd())  &&  now.after(challenge.getDateStart())   )
			return true;
		
		return false;
		
	}
	
	/**
	 * @param challenge
	 * @return
	 */
	public static boolean isActiveChallengeByChallengeId (long challengeId){
		
		CLSChallenge chall = null;
		try {
			chall = CLSChallengeLocalServiceUtil.getCLSChallenge(challengeId);
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return false;
		}
		
		return isActiveChallenge (chall);
		
	}
	
	/**
	 * @param challenge
	 * @param pilot
	 * @return
	 */
	private static boolean isChallengeOfCurrentPilot (CLSChallenge challenge, String pilot){
		
		String pilotGara =MyUtils.getPilotByChallenge(challenge);
		
		if (pilotGara.equalsIgnoreCase(pilot))
			return true;
		
		return false;
		
		
	}
	
	/**
	 * @param challengeId
	 * @param renderRequest
	 * @return
	 */
	public static boolean isChallengeWhichCanProposeIdeasByChallengeIdAction (long challengeId, ActionRequest actionRequest){
		
		CLSChallenge challenge = null;
		try {
			challenge = CLSChallengeLocalServiceUtil.getCLSChallenge(challengeId);
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return false;
		}
		
		User currentUser = null;
		
		try {
			 currentUser = PortalUtil.getUser(actionRequest);
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			
		}
		boolean isEnte = MyUtils.isAuthority(currentUser);
		
		boolean isActiveChallenge = isActiveChallenge(challenge);
		
		if (!isActiveChallenge || isEnte)
			return false;
		
		String pilota = MyUtils.getPilot(actionRequest);
		
		boolean isChallengeOfCurrentPilot =  isChallengeOfCurrentPilot ( challenge, pilota);
		if (isChallengeOfCurrentPilot ) 
			return true;
		
		return false;
	}
	
	
	
	/**
	 * @param challenge
	 * @param renderRequest
	 * @return
	 */
	public static boolean isChallengeWhichCanProposeIdeas (CLSChallenge challenge, RenderRequest renderRequest){
		
				
		//boolean isIMSSimpleUser = MyUtils.isIMSSimpleUser(currentUser);
		User currentUser = null;
		
		try {
			 currentUser = PortalUtil.getUser(renderRequest);
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			
		}
		boolean isEnte = MyUtils.isAuthority(currentUser);
		
		
		boolean isActiveChallenge = isActiveChallenge(challenge);
		
		if (!isActiveChallenge || isEnte)
			return false;
		
		
	//	String pilota = MyUtils.getPilotByUser(currentUser);
		
		String pilota = IdeasListUtils.getPilot(renderRequest);
		
		
		boolean isChallengeOfCurrentPilot =  isChallengeOfCurrentPilot ( challenge, pilota);
				
		
		//controllo che sia in una lingua che l'utente conosce
//		String[] lingueUtente = (String[]) currentUser.getExpandoBridge().getAttribute(MyConstants.CUSTOMFIELD_LANGUAGES);
//		
//		
//		boolean linguaUguale = false;
//		for(String linguaUt: lingueUtente) {
//			
//			if (MyUtils.lingueUguali(linguaUt,challenge.getLanguage())){
//				linguaUguale = true;
//				break;
//			}		
//		}
		
		//se dello stesso pilot oppure in una lingua che conosce
		if (isChallengeOfCurrentPilot ) 
			return true;
		
		
		return false;
	}
	
	
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static List<DLFileEntry> getFilesByChallenge (CLSChallenge challenge){
		
		List<DLFileEntry> files = new ArrayList<DLFileEntry>();
		

		
		Long folderId = challenge.getIdFolder();
		Integer count = 0;
		try {
			count = DLFileEntryLocalServiceUtil.getFileEntriesCount(challenge.getGroupId(), folderId);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return files;
		}
		OrderByComparator obc = OrderByComparatorFactoryUtil.create("DLFileEntry", "title", true);
		
		
		
		try {
			 files = DLFileEntryLocalServiceUtil.getFileEntries(challenge.getGroupId(), folderId.longValue(), 0, count,obc);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return files;
		}

		return files;
		
		
	}
	
	/**
	 * @param challenge
	 * @return
	 */
	public static boolean isGaraWithPoi (CLSChallenge challenge){

		List<CLSChallengePoi> poisList = new ArrayList<CLSChallengePoi>();
		
		try {
			
			poisList = CLSChallengePoiLocalServiceUtil.getChallengePOIByChallengeId(challenge.getChallengeId());
		} catch (SystemException e) {
			
			e.printStackTrace();
			return false;
			
		}
		
		if (poisList.size() > 0)
			return true;
		
		return false;
		
	}
	
	/**
	 * @param challenge
	 * @param currentUser
	 * @return
	 */
	public static boolean isFavoriteChallenge(CLSChallenge challenge, User currentUser){
		
		if(currentUser == null)
			return false;
		
		List<CLSFavouriteChallenges> favGare = new ArrayList<CLSFavouriteChallenges>();
		try {
			favGare = CLSFavouriteChallengesLocalServiceUtil.getFavouriteChallenges(challenge.getChallengeId(), currentUser.getUserId());
		} catch (SystemException e) {
			
			e.printStackTrace();
			return false;
		}
				
				
		if (favGare.size() >0)
			return true;
		
		return false;
	}
	
	
	/**
	 * @param challenge
	 * @param currentUser
	 * @return
	 */
	public static boolean isAuthor(CLSChallenge challenge, User currentUser){
		
		if(Validator.isNull(currentUser) )
			return false;
		
		if (challenge.getUserId() == currentUser.getUserId())
			return true;
		
		
		return false;
	}
	
	/**
	 * @param challengeId
	 * @param currentUser
	 * @return
	 */
	public static boolean isAuthorByChallengeId(long challengeId, User currentUser){
		
		CLSChallenge challenge = null;
		
		try {
			challenge =  CLSChallengeLocalServiceUtil.getCLSChallenge(challengeId);
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
			return false;
		}
				
		return isAuthor (challenge,currentUser );
		
	}
	
	/**
	 * @param vocabulary
	 * @param locale
	 * @return
	 */
	public static String getCategoriesByVocabulary (AssetVocabulary vocabulary, Locale locale){
		
		String cat = "";
		
		long  vocabularyId = vocabulary.getVocabularyId();
		
		List<AssetCategory> categoryList = new ArrayList<AssetCategory>();
		
		 try {
			 categoryList = AssetCategoryLocalServiceUtil.getVocabularyCategories(vocabularyId, 0, AssetCategoryLocalServiceUtil.getVocabularyCategoriesCount(vocabularyId), null);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return cat;
		}
		
		for (int i =0; i< categoryList.size(); i++){
			
			AssetCategory  categ= categoryList.get(i);
			
			cat += categ.getTitle(locale);
			if (i != categoryList.size()-1)
				cat +=", ";
		}
		
		return cat;
	}
	
	
	/**
	 * @param listaI
	 * @param currentUser
	 * @param pilot
	 * @return
	 */
	public static List<CLSChallenge> getListaGareFiltrataPerPilot (List<CLSChallenge> listaI, String pilotCorrente){
		
		List<CLSChallenge> listaF = new ArrayList<CLSChallenge>();
		
		
		//String pilotCorrente =MyUtils.getPilotByUser(currentUser);
		
		
		for (int i = 0; i<listaI.size(); i++){
			
			CLSChallenge gara = listaI.get(i); //mi piglio la gara corrente
			
			String pilotGara = MyUtils.getPilotByChallenge(gara);
			
			if (pilotGara.equalsIgnoreCase(pilotCorrente)){
				listaF.add(gara);
			}
		}
		
		//I show before the newest
		ChallengeComparator	cc = new ChallengeComparator();
		listaF=ListUtil.sort(listaF, cc);
		
	
		return listaF;
	}//metodo
	


	/**
	 * @param challengeId
	 * @return
	 */
	public static long getAuthorIdByChallengeId (long challengeId){
		
		long authorid = 0;
		
		CLSChallenge gara = null;
		try {
			 gara = CLSChallengeLocalServiceUtil.getCLSChallenge(challengeId);
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return authorid;
		}
		
		
		 authorid = gara.getUserId();
		
		return authorid;
		
	}
	
	/**
	 * @param challengeId
	 * @return
	 */
	public static String getChallengeNameByChallengeId (long challengeId){
		
		String name = "";
		
		CLSChallenge gara = null;
		try {
			 gara = CLSChallengeLocalServiceUtil.getCLSChallenge(challengeId);
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return name;
		}
		
		name = gara.getChallengeTitle();
		
		return name;
	}
	
	
	/**
	 * @param challengeId
	 * @return
	 */
	public static boolean isChallengeLinkedWithNeed (long challengeId){
		
		int nlc;
		try {
			nlc = NeedLinkedChallengeLocalServiceUtil.getNeedByChallengeId(challengeId).size();
		} catch (SystemException e) {
			
			e.printStackTrace();
			return false;
		}
		
		if  (nlc > 0)
			return true;
		
		return false;
	}
	
	
	/**
	 * @param challengeId
	 * @return
	 */
	public static boolean isChallengeLinkedWithOnlyOneNeed (long challengeId){
		
		int nlc;
		try {
			nlc = NeedLinkedChallengeLocalServiceUtil.getNeedByChallengeId(challengeId).size();
		} catch (SystemException e) {
			
			e.printStackTrace();
			return false;
		}
		
		if  (nlc ==1)
			return true;
		
		return false;
	}
	
	/**
	 * @param challengeId
	 * @return
	 */
	public static boolean isChallengeLinkedWithManyNeeds (long challengeId){
		
		int nlc;
		try {
			nlc = NeedLinkedChallengeLocalServiceUtil.getNeedByChallengeId(challengeId).size();
		} catch (SystemException e) {
			
			e.printStackTrace();
			return false;
		}
		
		if  (nlc > 1)
			return true;
		
		return false;
	}
	
	/**
	 * I can delete a challenge only if it isn't linked with any idea
	 * @param needId
	 * @return
	 */
	public static boolean isChallengeWhichCanDelete (long challengeId){
		
		
		List<CLSIdea> linkedIdeas = new ArrayList<CLSIdea>();
		try {
			linkedIdeas = CLSIdeaLocalServiceUtil.getIdeasByChallengeId(challengeId);
		} catch (SystemException e) {
			
			e.printStackTrace();
		}
		
		if (linkedIdeas.size() > 0)
			return false;
		
		return true;
	}
	
	/**
	 * Remove the link between Challenge and Need on Challenge Deletion
	 * 
	 * @param challengeId
	 */
	public static void deleteNeedLinkedChallengeByChallenge (long challengeId){
		
		List<NeedLinkedChallenge> ncs = new ArrayList<NeedLinkedChallenge>();
		
		//get all link fot this challenge
		 try {
			 ncs = NeedLinkedChallengeLocalServiceUtil.getNeedLinkedChallengeByChallengeId(challengeId);
		} catch (SystemException e3) {
			
			e3.printStackTrace();
		}

		 
		//loop  and delete
		for ( NeedLinkedChallenge nc:ncs){
			
			try {
				NeedLinkedChallengeLocalServiceUtil.deleteNeedLinkedChallenge(nc);
			} catch (SystemException e) {
				
				e.printStackTrace();
			}
			
		}
		 
	}
	

/**
 * @param challenge
 * @param userId
 */
public static void subscribeUserToChallengeComment (CLSChallenge challenge , long userId){
	
	
	try {
		SubscriptionLocalServiceUtil.addSubscription(userId, challenge.getGroupId(), CLSChallenge.class.getName(), challenge.getChallengeId(), "instant");
	} catch (PortalException | SystemException e) {
		
		e.printStackTrace();
	}
	
}
	

/** Check if the challenge is in the open state
 *  The field is empty in legacy cases
 * @param challenge
 * @return
 */
public static boolean isOpenChallenge(CLSChallenge challenge){
	
	if ( challenge.getChallengeStatus().equalsIgnoreCase(MyConstants.CHALLENGE_STATE_OPEN) ||
		challenge.getChallengeStatus().isEmpty()
		)
		return true;
	
	return false;
				
}

public static boolean isOpenChallengeByChallengeId(long challengeId){

	CLSChallenge chall = null;
	try {
		 chall = CLSChallengeLocalServiceUtil.getCLSChallenge(challengeId);
	} catch (PortalException | SystemException e) {
		
		e.printStackTrace();
		return false;
	}
	
	return isOpenChallenge(chall);
	
				
}


/**
 * redirect to the rigth page
 * @param actionRequest
 * @param actionResponse
 * @param challengeId
 */
public static void redirectToChallenge(ActionRequest actionRequest,	ActionResponse actionResponse, long challengeId){
	 
	String totalPath = IdeaManagementSystemProperties.getFriendlyUrlChallenges(challengeId);
	 
	try {
		actionResponse.sendRedirect(totalPath);
	} catch (IOException e) {
		e.printStackTrace();
	} //go to the page that you put as second parameter
}


/**
 * @param challengeId
 * @return
 */
public static String getLanguageByChallengeId (long challengeId){
	
	String language = "";
	
	CLSChallenge gara = null;
	try {
		 gara = CLSChallengeLocalServiceUtil.getCLSChallenge(challengeId);
	} catch (PortalException | SystemException e) {
		
		e.printStackTrace();
		return language;
	}
	
	language = gara.getLanguage();
	
	return language;
	
	
}


/**
 * @param challengeId
 * @param inputId
 * @param type
 * @return
 */
public static String getCriteriaDetailsByChallengeInputType (long challengeId, String inputId, String type){
	
	 List<EvaluationCriteria> crit = EvaluationCriteriaLocalServiceUtil.getCriteriaByChallengeIdAndInputId(challengeId, inputId);
	
	 if (crit.size() > 0){
		 
		 EvaluationCriteria ec = crit.get(0);
		 
		 if (type.equals("w"))
			 return String.valueOf(ec.getWeight());
		 else if (type.equals("t"))
			 return String.valueOf(ec.getThreshold());
		 
	 }
	 
	return "";
}


/**
 * @param challengeId
 * @param inputId
 * @return
 */
public static boolean  isEnabledCriteria (long challengeId, String inputId){ 

	List<EvaluationCriteria> critList = EvaluationCriteriaLocalServiceUtil.getCriteriaByChallengeIdAndInputId(challengeId, inputId);
	
	
	if (critList.size() == 0)
		return false;
	else{
		EvaluationCriteria cri = critList.get(0);
		return cri.getEnabled();
		
	}		

}



/**
 * Check if there are the condition to close the evaluation process
 * @param challengeId
 * @return
 */
public static boolean isEvaluationClosable (CLSChallenge challenge){
	
	//if it is on the state of evaluation
	if (!challenge.getChallengeStatus().equalsIgnoreCase(MyConstants.CHALLENGE_STATE_EVALUATION))
		return false;
	
	
	//Check if all ideas for this challenge have been evaluated
	List<CLSIdea> ideas = new ArrayList<CLSIdea>();
	try {
		 ideas = CLSIdeaLocalServiceUtil.getIdeasByChallengeId(challenge.getChallengeId());
	} catch (SystemException e) {
		
		e.printStackTrace();
	}
	
	for (CLSIdea idea:ideas ){
		
		if (idea.getFinalMotivation().isEmpty())
			return false;	
		
	}
	
	
	return true;
}

/**
 * @param challengeId
 * @return
 */
public static boolean isEvaluationClosableByChallengeId (long challengeId){
	
	CLSChallenge chall = null;
	try {
		 chall = CLSChallengeLocalServiceUtil.getCLSChallenge(challengeId);
	} catch (PortalException | SystemException e) {
		
		e.printStackTrace();
		return false;
	}
	
	return isEvaluationClosable (chall);
	
}



/**
 * Check if all ideas for this challenge have been evaluated
 * @param challengeId
 * @return
 */
public static boolean isEvaluationOpenable (long challengeId){
	
	boolean isActiveChallenge = isActiveChallengeByChallengeId(challengeId);
	
	//the endDate of the challenge must be passed
	if (isActiveChallenge)
		return false;
	
	List<CLSIdea> ideas = new ArrayList<CLSIdea>();
	try {
		 ideas = CLSIdeaLocalServiceUtil.getIdeasByChallengeId(challengeId);
	} catch (SystemException e) {
		
		e.printStackTrace();
	}
	
	//At least one idea must be presented
	if (ideas.size() == 0)
		return false;
	
	//the state of the challenge must be "open" or "" ("" is legacy)
	if (!isOpenChallengeByChallengeId(challengeId))
		return false;
	
	
	List<EvaluationCriteria> ecBarriera = EvaluationCriteriaLocalServiceUtil.getEnabledCriteriaByChallengeIdAndIsBarriera(challengeId, true);
	List<EvaluationCriteria> ecQuantitative = EvaluationCriteriaLocalServiceUtil.getEnabledCriteriaByChallengeIdAndIsBarriera(challengeId, false);
	
	//we must have at least one criteria for each type
	if ((ecBarriera.size()== 0) || (ecQuantitative.size()== 0) )
		return false;
	
	
	return true;
}


/**
 * @param challengeId
 * @return
 */
public static boolean isChallengeInEvaluation(long challengeId){
	
	CLSChallenge chall=null;
	try {
		chall = CLSChallengeLocalServiceUtil.getCLSChallenge(challengeId);
	} catch (PortalException | SystemException e) {
		
		e.printStackTrace();
		return false;
	}
	
	if (chall.getChallengeStatus().equalsIgnoreCase(MyConstants.CHALLENGE_STATE_EVALUATION))
		return true;
	
	return false;
	
}

/**
 * @param challengeId
 * @return
 */
public static boolean isChallengeEvaluated(long challengeId){
	
	CLSChallenge chall=null;
	try {
		chall = CLSChallengeLocalServiceUtil.getCLSChallenge(challengeId);
	} catch (PortalException | SystemException e) {
		
		e.printStackTrace();
		return false;
	}
	
	if (chall.getChallengeStatus().equalsIgnoreCase(MyConstants.CHALLENGE_STATE_EVALUATED))
		return true;
	
	return false;
	
}


/**
 * @param challengeStatus
 * @param locale
 * @return
 */
public static String getChallengeStatusI18n (String challengeStatus, Locale locale) {
	
	String statusChi18n = "";
	
	String language = locale.getLanguage();
	String country = locale.getCountry();
	ResourceBundle res = ResourceBundle.getBundle("Language", new Locale(language, country));
		
	HashMap<String, String> stati = new HashMap<String,String>();
	
	String open= res.getString("ims.open");
	String evaluation= res.getString("ims.in-evaluation");
	String evaluated= res.getString("ims.evaluated");
	String closed= res.getString("ims.closed");

	
	try {
		open = new String(open.getBytes("ISO-8859-1"), "UTF-8");
		evaluation = new String(evaluation.getBytes("ISO-8859-1"), "UTF-8");
		evaluated = new String(evaluated.getBytes("ISO-8859-1"), "UTF-8");
		closed = new String(closed.getBytes("ISO-8859-1"), "UTF-8");


	} catch (UnsupportedEncodingException e) {
		
		e.printStackTrace();
		return statusChi18n;
		
	}	
		
		
	stati.put(MyConstants.CHALLENGE_STATE_OPEN, open);
	stati.put(MyConstants.CHALLENGE_STATE_EVALUATION, evaluation);
	stati.put(MyConstants.CHALLENGE_STATE_EVALUATED, evaluated);
	stati.put(MyConstants.CHALLENGE_STATE_CLOSED, closed);



	statusChi18n =stati.get(challengeStatus);
	 
	 return statusChi18n;
	
}

/**
 * @param userId
 * @param maxNumber
 * @param locale
 * @return
 * @throws SystemException
 */
public static JSONArray getChallengesForMeByUserId(long userId, int maxNumber, Locale locale){
	
	
	JSONArray challengesJ =  JSONFactoryUtil.createJSONArray();
	
	List<CLSChallenge> challengesR = new ArrayList<CLSChallenge>();
	
	boolean isAuthority = MyUtils.isAuthorityByUserId(userId);
	
	if (isAuthority){
		try {
			List<CLSChallenge> challenges = CLSChallengeLocalServiceUtil.getChallengesByUserId(userId);
			challengesR = new ArrayList<CLSChallenge>(challenges);
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}else{
		
		String pilot = MyUtils.getPilotByUserId(userId);
		
		List<CLSChallenge> challengesI= new ArrayList<CLSChallenge>();
		try {
			challengesI = CLSChallengeLocalServiceUtil.getActiveChallenges();
			
			if (challengesI.size() == 0)
				challengesI = CLSChallengeLocalServiceUtil.getCLSChallenges(0, CLSChallengeLocalServiceUtil.getCLSChallengesCount());
			
		} catch (SystemException e) {
			e.printStackTrace();
		}
		challengesR = ChallengesUtils.getListaGareFiltrataPerPilot(challengesI, pilot);//filter
		
	}
	
	if (challengesR.size() >0){
		Collections.reverse (challengesR);//before the newest
	}		
	
	
	
	for (int i=0; i< challengesR.size(); i++ ){
		
		if (i >= maxNumber )
			break;
		
		CLSChallenge challenge = challengesR.get(i);
		JSONObject challengeJ = jsonifyChallenge(challenge,  locale);
		
		challengesJ.put(challengeJ);
	
	}
	return challengesJ;
}


/**
 * @param userId
 * @param maxNumber
 * @param locale
 * @return
 * @throws SystemException
 */
public static JSONArray getTopRatedChallengesByPilotOrLanguage(String pilot, String language, int maxNumber, Locale locale){
	
	
	JSONArray challengesJ =  JSONFactoryUtil.createJSONArray();
	
	List<CLSChallenge> challenges = new ArrayList<CLSChallenge>();
	
	List<CLSChallenge> challengesI= new ArrayList<CLSChallenge>();
	try {
		challengesI = CLSChallengeLocalServiceUtil.getActiveChallenges();
		
		if (challengesI.size() == 0)
			challengesI = CLSChallengeLocalServiceUtil.getCLSChallenges(0, CLSChallengeLocalServiceUtil.getCLSChallengesCount());
		
	} catch (SystemException e) {
		e.printStackTrace();
	}
	
	if (!pilot.equalsIgnoreCase("")){
		challenges = ChallengesUtils.getListaGareFiltrataPerPilot(challengesI, pilot);//filter
	}else{
		challenges = getChallengesListFilterByLanguage(challengesI, language);
	}
	
	
	
	if (challenges.size() >0){
		Collections.reverse (challenges);//before the newest
		
		//I show before the top rated
		ChallengeComparatorByRating cc = new ChallengeComparatorByRating();
		challenges=ListUtil.sort(challenges, cc);
		
	}		
	
	
	
	for (int i=0; i< challenges.size(); i++ ){
		
		if (i >= maxNumber )
			break;
		
		CLSChallenge challenge = challenges.get(i);
		JSONObject challengeJ = jsonifyChallenge(challenge,  locale);
		
		challengesJ.put(challengeJ);
	
	}
	return challengesJ;
}


/**
 * @param challenge
 * @param locale
 * @return
 */
private static JSONObject jsonifyChallenge (CLSChallenge challenge, Locale locale){
	
	JSONObject challengeJ = JSONFactoryUtil.createJSONObject();
	
	challengeJ.put("type", "Challenge");
	challengeJ.put("tit", challenge.getChallengeTitle());
	challengeJ.put("text",  getSocialSummaryByChallenge(challenge));
	challengeJ.put("url",  IdeaManagementSystemProperties.getFriendlyUrlChallenges(challenge.getChallengeId()));
	challengeJ.put("status", challenge.getChallengeStatus());
	challengeJ.put("statusi18n", getChallengeStatusI18n(challenge.getChallengeStatus(), locale));
	challengeJ.put("image", IdeaManagementSystemProperties.getRootUrl() + getRepresentativeImgUrlByChallenge(challenge));
	challengeJ.put("rating", getAverageDoubleRatingsByChallenge(challenge));
	challengeJ.put("vocabularyName", getVocabularyNameByChallengeId(challenge.getChallengeId(), locale));
	challengeJ.put("vocabularyIcon", getIconForVocabularyByChallenge(challenge));
	challengeJ.put("vocabularyColor", getColorForVocabularyByChallenge(challenge));
	challengeJ.put("authorName", getAuthorNameByChallenge(challenge));
	challengeJ.put("startDate", MyUtils.getDateDDMMYYYYByDate(challenge.getDateStart()));
	challengeJ.put("endtDate", MyUtils.getDateDDMMYYYYByDate(challenge.getDateEnd()));
	challengeJ.put("authorOrganizationName",  getAuthorOrganizationNameByChallenge(challenge));
	challengeJ.put("numIdeas",  getNumIdeasByChallengeId(challenge.getChallengeId()));
	
	String challengeStatus = challenge.getChallengeStatus();
	//For legacy
	if (challenge.getChallengeStatus().isEmpty())
		challengeStatus = MyConstants.CHALLENGE_STATE_OPEN;
	
	challengeJ.put("status", challengeStatus);
	challengeJ.put("statusi18n", getChallengeStatusI18n(challengeStatus, locale));

	return challengeJ;
}


/**
 * @param listaI
 * @param language
 * @return
 */
public static List<CLSChallenge> getChallengesListFilterByLanguage (List<CLSChallenge> listaC, String language){
	
	List<CLSChallenge> listaF = new ArrayList<CLSChallenge>();
	
	
	for (int i = 0; i<listaC.size(); i++){//slide beginnig list
		
		CLSChallenge challenge = listaC.get(i); //the current idea
		
		if (challenge.getLanguage().equalsIgnoreCase(language))
			listaF.add(challenge);

	}
	
	//I show before the newest
	ChallengeComparator ic = new ChallengeComparator();
	listaF=ListUtil.sort(listaF, ic);
	
	
	return listaF;
}//metodo

	
}
