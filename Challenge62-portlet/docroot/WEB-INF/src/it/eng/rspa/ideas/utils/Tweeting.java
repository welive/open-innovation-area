
package it.eng.rspa.ideas.utils;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.asset.model.AssetCategory;
import com.liferay.portlet.asset.model.AssetVocabulary;
import com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil;


/**
 * @author Engineering
 *
 */
public class Tweeting {

	/**
	 * @param args
	 */
	

	public static void twitta(ThemeDisplay td, String tipoIdeaNeedChallenge,String title, long id, String hashTag, String[] tagsArray, 
			long[] catsIdsLongArray, ArrayList<CLSCategoriesSetForChallenge> categoriesSet ){
		
		Locale locale = td.getLocale();
		
		String tweet = buildTweet(td, tipoIdeaNeedChallenge, title, id, hashTag, tagsArray, catsIdsLongArray, categoriesSet, locale);
		sendTweet( tweet);
				
	}
	
	//Costruisco il tweet per la nuova idea/challenge
	//la lunghezza massima dei msg di twitter e' 140 char
	//gli URL prendono 23 caratteri a prescindere se hanno lunghezza minore o superiore
	private static String buildTweet(ThemeDisplay td,String tipoIdeaNeedChallenge,String title, long id, String hashTag, String[] tagsArray, 
			long[] catsIdsLongArray, ArrayList<CLSCategoriesSetForChallenge> categoriesSet, Locale locale ){
		
		//costruisco l'URL dell'idea/challenge
		String  portal=td.getPortalURL();
		String url =td.getURLCurrent();
		StringTokenizer path= new StringTokenizer(url, "?");
		url=path.nextToken();
		
		String urlExplorerContest = "ideas";
		String tweet1 = "Nuova idea";
		String indiceTipo = "i";
		if (tipoIdeaNeedChallenge.equalsIgnoreCase("Challenge") ){
			urlExplorerContest = "challenges";
			tweet1 = "Nuova gara";
			indiceTipo = "g";
		}	
		
		//ATTENZIONE LOCALHOST non e' considerato URL da Twitter!!! e la dimensione potrebbe essere > 140 ed andare in eccezione
		String totalURL= portal+url+"/-/"+urlExplorerContest+"_explorer_contest/"+String.valueOf(id)+"/view";
		
		//totalURL="http://socialgov.eng.it/ims/-/ideas_explorer_contest/5602/view";//TEST
		
		
		
		List<String> neWparts = new ArrayList<String>();
		String newHashTag="";
		//check hashtag
		if (!hashTag.equals("")){
			
			String[] parts = hashTag.split("\\s{1,}");
			
			for(int i=0; i<parts.length; i++) {
				String p=parts[i];
			  
				p = checkHashTag(p);
			 	neWparts.add(p);
			  
			  newHashTag+=p+" ";//aggiungo spazio
			}
		}
		
		
		if (tagsArray!= null){
			for(int i=0; i<tagsArray.length; i++) {
				
				String tagHashato = checkHashTag(tagsArray[i]);
				neWparts.add(tagHashato);
				newHashTag+=tagHashato+" ";//aggiungo spazio
			}
		}
		
		if (catsIdsLongArray!= null){
			for(int i=0; i<catsIdsLongArray.length; i++) {
				
				try {
					AssetCategory ac =AssetCategoryLocalServiceUtil.getCategory(catsIdsLongArray[i]);
					String categHashato = checkHashTag(ac.getName());
					neWparts.add(categHashato);
					newHashTag+=categHashato+" ";//aggiungo spazio
				} catch (PortalException | SystemException e) {
					
					e.printStackTrace();
				}
	
			}
		}
		
		
		//per le gare le categorie sono in realta i vocabolari
		if (categoriesSet!= null){
			Iterator<CLSCategoriesSetForChallenge> categoriesListIt = categoriesSet.iterator();
			while (categoriesListIt.hasNext()){
				CLSCategoriesSetForChallenge categorySet = categoriesListIt.next();
				AssetVocabulary vocabulary;
				try {
					vocabulary = AssetVocabularyLocalServiceUtil.fetchAssetVocabulary(categorySet.getCategoriesSetID());
					
					String categoryName = vocabulary.getTitle(locale);
					String categHashato = checkHashTag(categoryName);			
					neWparts.add(categHashato);
					newHashTag+=categHashato+" ";//aggiungo spazio
					
				} catch (SystemException e) {
					
					e.printStackTrace();
				}						
			}
		}
		
		
		
		String tweet1b = " ";//spazio prima di titolo
		String tweet2a = "\"";//apici prima e dopo titolo
		String tweetOnline = " online ";
		String tweetHashTag = "";
		String tweetFine =" #ims_"+indiceTipo+Long.toString(id);//Id concatenato all'hashatg fisso

		tweetHashTag =newHashTag; //hashtag variabile

		String tweet= tweet1 + tweet1b +tweet2a +title +tweet2a + tweetOnline   + tweetHashTag  ;
		String tweetMenoURL=tweet+tweetFine;
		
		tweet= tweet1 + tweet1b +tweet2a +title +tweet2a + tweetOnline;
		tweet +=totalURL;	
		tweet +=tweetHashTag;
		tweet +=tweetFine;		
		
		//ATTENZIONE LOCALHOST non e' considerato URL da Twitter!!!
		int lunghezzaUrl=24;//23 fissi +1 spazio
		 
		int lunghezzaTotale=tweetMenoURL.length()+lunghezzaUrl;
			
		
		
		if (lunghezzaTotale > 140){
			
			
			String TweetNoTitleNoHashtagNoUrl = tweet1 + tweetOnline   + tweetFine; //campi fissi obbligatori
			
			int lunghFissaTweet = TweetNoTitleNoHashtagNoUrl.length()+lunghezzaUrl;		
			int rimanenti1 = 140 - lunghFissaTweet;
			int hashtagLung = newHashTag.length();
			
			String titleRidotto="";
			int rimanenti2 = rimanenti1 - hashtagLung;
			
			
			if (rimanenti2<0){//se l'hashtag e' troppo lungo, allora lo gestisco e poi controllo il titolo

				String hashcorrente = "";
				for( int y = 0; y< neWparts.size(); y++){
					
					int lunghNewPartsY = neWparts.get(y).length();
					int lunghHashCorrente = hashcorrente.length();
					 
					if ( (lunghNewPartsY < rimanenti1) && ((lunghNewPartsY + lunghHashCorrente)<rimanenti1))
						hashcorrente+=" "+neWparts.get(y);
				}
				
				int rimanenti1b = rimanenti1 - hashcorrente.length();

				int lunghMax1 =rimanenti1b;	
				if (lunghMax1 > title.length())
					lunghMax1 = title.length();//per non andare out of index
				
				int prendereDaTitolo = lunghMax1-6;// 1 spazio + 2 apici + 3 puntini
				
				if (prendereDaTitolo < 0)
					titleRidotto="";
				else{
					titleRidotto = title.substring(0, prendereDaTitolo);
					titleRidotto = " \""+titleRidotto+"...\"";//se tronco il titolo metto i puntini
					
				}
				
				tweet= tweet1 +titleRidotto + tweetOnline + totalURL +hashcorrente  +tweetFine;		
				
				
			}else{//se l'hashtag non e' troppo lungo, allora controllo solo il titolo
				
				int lunghMax2 =rimanenti2;	
				if (rimanenti2 > title.length())
					lunghMax2 = title.length();//per non andare out of index
				
				int prendereDaTitolo = lunghMax2-6;// 1 spazio + 2 apici + 3 puntini
				
				if (prendereDaTitolo < 0)
					titleRidotto="";
				else{
					
					titleRidotto = title.substring(0, prendereDaTitolo);
					titleRidotto = " \""+titleRidotto+"...\"";//se tronco il titolo metto i puntini
					
				}

				 tweet= tweet1 + titleRidotto  + tweetOnline +totalURL+ tweetHashTag +tweetFine ;
			}
			
		}

		return tweet;
	}
	
	
	
	
	
	//si occupa solo dell'invio
	private static void sendTweet(String args) {
		
        if (args.equals("")) {
            System.out.println("Usage: java twitter4j.examples.tweets.UpdateStatus [text]");
        }
        try {
            Twitter twitter = new TwitterFactory().getInstance();
            try {
                // get request token.
                // this will throw IllegalStateException if access token is already available
                RequestToken requestToken = twitter.getOAuthRequestToken();
                System.out.println("Got request token.");
                System.out.println("Request token: " + requestToken.getToken());
                System.out.println("Request token secret: " + requestToken.getTokenSecret());
                AccessToken accessToken = null;

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                while (null == accessToken) {
                    System.out.println("Open the following URL and grant access to your account:");
                    System.out.println(requestToken.getAuthorizationURL());
                    System.out.print("Enter the PIN(if available) and hit enter after you granted access.[PIN]:");
                    String pin = br.readLine();
                    try {
                        if (pin.length() > 0) {
                            accessToken = twitter.getOAuthAccessToken(requestToken, pin);
                        } else {
                            accessToken = twitter.getOAuthAccessToken(requestToken);
                        }
                    } catch (TwitterException te) {
                        if (401 == te.getStatusCode()) {
                            System.out.println("Unable to get the access token.");
                        } else {
                            te.printStackTrace();
                        }
                    }
                }
                System.out.println("Got access token.");
                System.out.println("Access token: " + accessToken.getToken());
                System.out.println("Access token secret: " + accessToken.getTokenSecret());
            } catch (IllegalStateException ie) {
                // access token is already available, or consumer key/secret is not set.
                if (!twitter.getAuthorization().isEnabled()) {
                    System.out.println("OAuth consumer key/secret is not set.");
                }
            }
            Status status = twitter.updateStatus(args);
            System.out.println("Successfully updated Twitter status to [" + status.getText() + "].");

        } catch (TwitterException te) {
            te.printStackTrace();
            System.out.println("Failed to get timeline: " + te.getMessage());

        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.out.println("Failed to read the system input.");


        }
    }

	
	
	private static String checkHashTag (String hash){
		
		hash = hash.replaceAll(" ","");
		hash = hash.toLowerCase();
		String a = hash.substring(0,1);
		  if (!a.equals("#"))
			  hash= "#"+hash;
		  
		return hash;
	}
	
}
