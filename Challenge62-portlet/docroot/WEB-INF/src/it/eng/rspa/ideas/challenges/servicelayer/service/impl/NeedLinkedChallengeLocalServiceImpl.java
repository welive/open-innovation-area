/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.impl;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.base.NeedLinkedChallengeLocalServiceBaseImpl;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.NeedLinkedChallengeUtil;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the need linked challenge local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.ideas.challenges.servicelayer.service.NeedLinkedChallengeLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.NeedLinkedChallengeLocalServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.NeedLinkedChallengeLocalServiceUtil
 */
public class NeedLinkedChallengeLocalServiceImpl
	extends NeedLinkedChallengeLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.ideas.challenges.servicelayer.service.NeedLinkedChallengeLocalServiceUtil} to access the need linked challenge local service.
	 */
	
	
	/**
	 * @param challengeId
	 * @return
	 * @throws SystemException
	 */
	public List<CLSIdea> getNeedByChallengeId(long challengeId) throws SystemException{
		
		List<NeedLinkedChallenge>   needIdList  =  NeedLinkedChallengeUtil.findByneedByChallenge(challengeId);
		
		 List<CLSIdea> listNeed = new ArrayList<CLSIdea>();
		 
		 for (NeedLinkedChallenge n:needIdList){
			 
			 
			 CLSIdea need;
			try {
				need = CLSIdeaLocalServiceUtil.getCLSIdea(n.getNeedId());
				 listNeed.add(need);
			} catch (PortalException e) {
				
				e.printStackTrace();
			}
			 
			
		 }
			 
		return listNeed;
	}
	
	
	/**
	 * @param needId
	 * @return
	 * @throws SystemException
	 */
	public List<CLSChallenge> getChallengeByNeedId(long needId) throws SystemException{
		
		List<NeedLinkedChallenge>   challengeIdList  =  NeedLinkedChallengeUtil.findBychallengeByNeed(needId);
		
		 List<CLSChallenge> listChalllenges = new ArrayList<CLSChallenge>();
		 
		 for (NeedLinkedChallenge n:challengeIdList){
			 
			 
			 CLSChallenge challenge;
			try {
				challenge = CLSChallengeLocalServiceUtil.getCLSChallenge(n.getChallengeId());
				listChalllenges.add(challenge);
			} catch (PortalException e) {
				
				e.printStackTrace();
			}
			 
			 
		 }
			 
		return listChalllenges;
	}
	
	
	
	/**
	 * @param needId
	 * @return
	 * @throws SystemException
	 */
	public List<NeedLinkedChallenge> getNeedLinkedChallengeByNeedId(long needId)throws SystemException{
		
		return NeedLinkedChallengeUtil.findBychallengeByNeed(needId);
	}
	
	
	/**
	 * @param challengeId
	 * @return
	 * @throws SystemException
	 */
	public List<NeedLinkedChallenge> getNeedLinkedChallengeByChallengeId(long challengeId)throws SystemException{
		
		return NeedLinkedChallengeUtil.findByneedByChallenge(challengeId);
	}
	
	
}