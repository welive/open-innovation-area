/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.base;

import it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteChallengesLocalServiceUtil;

import java.util.Arrays;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 * @generated
 */
public class CLSFavouriteChallengesLocalServiceClpInvoker {
	public CLSFavouriteChallengesLocalServiceClpInvoker() {
		_methodName0 = "addCLSFavouriteChallenges";

		_methodParameterTypes0 = new String[] {
				"it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges"
			};

		_methodName1 = "createCLSFavouriteChallenges";

		_methodParameterTypes1 = new String[] {
				"it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK"
			};

		_methodName2 = "deleteCLSFavouriteChallenges";

		_methodParameterTypes2 = new String[] {
				"it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK"
			};

		_methodName3 = "deleteCLSFavouriteChallenges";

		_methodParameterTypes3 = new String[] {
				"it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges"
			};

		_methodName4 = "dynamicQuery";

		_methodParameterTypes4 = new String[] {  };

		_methodName5 = "dynamicQuery";

		_methodParameterTypes5 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
			};

		_methodName6 = "dynamicQuery";

		_methodParameterTypes6 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
			};

		_methodName7 = "dynamicQuery";

		_methodParameterTypes7 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
				"com.liferay.portal.kernel.util.OrderByComparator"
			};

		_methodName8 = "dynamicQueryCount";

		_methodParameterTypes8 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
			};

		_methodName9 = "dynamicQueryCount";

		_methodParameterTypes9 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery",
				"com.liferay.portal.kernel.dao.orm.Projection"
			};

		_methodName10 = "fetchCLSFavouriteChallenges";

		_methodParameterTypes10 = new String[] {
				"it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK"
			};

		_methodName11 = "getCLSFavouriteChallenges";

		_methodParameterTypes11 = new String[] {
				"it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK"
			};

		_methodName12 = "getPersistedModel";

		_methodParameterTypes12 = new String[] { "java.io.Serializable" };

		_methodName13 = "getCLSFavouriteChallengeses";

		_methodParameterTypes13 = new String[] { "int", "int" };

		_methodName14 = "getCLSFavouriteChallengesesCount";

		_methodParameterTypes14 = new String[] {  };

		_methodName15 = "updateCLSFavouriteChallenges";

		_methodParameterTypes15 = new String[] {
				"it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges"
			};

		_methodName142 = "getBeanIdentifier";

		_methodParameterTypes142 = new String[] {  };

		_methodName143 = "setBeanIdentifier";

		_methodParameterTypes143 = new String[] { "java.lang.String" };

		_methodName148 = "removeFavouriteChallengesOnChallengeDelete";

		_methodParameterTypes148 = new String[] { "long" };

		_methodName149 = "getFavouriteChallengesEntriesByChallengeId";

		_methodParameterTypes149 = new String[] { "long" };

		_methodName150 = "getFavouriteChallengesByChallengeId";

		_methodParameterTypes150 = new String[] { "long" };

		_methodName151 = "getFavouriteChallengesByUserId";

		_methodParameterTypes151 = new String[] { "long" };

		_methodName152 = "addFavouriteChallenge";

		_methodParameterTypes152 = new String[] {
				"java.lang.Long", "java.lang.Long"
			};

		_methodName153 = "removeFavouriteChallenge";

		_methodParameterTypes153 = new String[] {
				"java.lang.Long", "java.lang.Long"
			};

		_methodName154 = "getFavouriteChallenges";

		_methodParameterTypes154 = new String[] {
				"java.lang.Long", "java.lang.Long"
			};

		_methodName155 = "getFavouriteChallengesEntriesByUserId";

		_methodParameterTypes155 = new String[] { "long" };
	}

	public Object invokeMethod(String name, String[] parameterTypes,
		Object[] arguments) throws Throwable {
		if (_methodName0.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.addCLSFavouriteChallenges((it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges)arguments[0]);
		}

		if (_methodName1.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.createCLSFavouriteChallenges((it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK)arguments[0]);
		}

		if (_methodName2.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.deleteCLSFavouriteChallenges((it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK)arguments[0]);
		}

		if (_methodName3.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.deleteCLSFavouriteChallenges((it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges)arguments[0]);
		}

		if (_methodName4.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.dynamicQuery();
		}

		if (_methodName5.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0]);
		}

		if (_methodName6.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue());
		}

		if (_methodName7.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue(),
				(com.liferay.portal.kernel.util.OrderByComparator)arguments[3]);
		}

		if (_methodName8.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0]);
		}

		if (_methodName9.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				(com.liferay.portal.kernel.dao.orm.Projection)arguments[1]);
		}

		if (_methodName10.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.fetchCLSFavouriteChallenges((it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK)arguments[0]);
		}

		if (_methodName11.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.getCLSFavouriteChallenges((it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK)arguments[0]);
		}

		if (_methodName12.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.getPersistedModel((java.io.Serializable)arguments[0]);
		}

		if (_methodName13.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.getCLSFavouriteChallengeses(((Integer)arguments[0]).intValue(),
				((Integer)arguments[1]).intValue());
		}

		if (_methodName14.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.getCLSFavouriteChallengesesCount();
		}

		if (_methodName15.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.updateCLSFavouriteChallenges((it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges)arguments[0]);
		}

		if (_methodName142.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes142, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.getBeanIdentifier();
		}

		if (_methodName143.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes143, parameterTypes)) {
			CLSFavouriteChallengesLocalServiceUtil.setBeanIdentifier((java.lang.String)arguments[0]);

			return null;
		}

		if (_methodName148.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes148, parameterTypes)) {
			CLSFavouriteChallengesLocalServiceUtil.removeFavouriteChallengesOnChallengeDelete(((Long)arguments[0]).longValue());

			return null;
		}

		if (_methodName149.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes149, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.getFavouriteChallengesEntriesByChallengeId(((Long)arguments[0]).longValue());
		}

		if (_methodName150.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes150, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.getFavouriteChallengesByChallengeId(((Long)arguments[0]).longValue());
		}

		if (_methodName151.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes151, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.getFavouriteChallengesByUserId(((Long)arguments[0]).longValue());
		}

		if (_methodName152.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes152, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.addFavouriteChallenge((java.lang.Long)arguments[0],
				(java.lang.Long)arguments[1]);
		}

		if (_methodName153.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes153, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.removeFavouriteChallenge((java.lang.Long)arguments[0],
				(java.lang.Long)arguments[1]);
		}

		if (_methodName154.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes154, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.getFavouriteChallenges((java.lang.Long)arguments[0],
				(java.lang.Long)arguments[1]);
		}

		if (_methodName155.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes155, parameterTypes)) {
			return CLSFavouriteChallengesLocalServiceUtil.getFavouriteChallengesEntriesByUserId(((Long)arguments[0]).longValue());
		}

		throw new UnsupportedOperationException();
	}

	private String _methodName0;
	private String[] _methodParameterTypes0;
	private String _methodName1;
	private String[] _methodParameterTypes1;
	private String _methodName2;
	private String[] _methodParameterTypes2;
	private String _methodName3;
	private String[] _methodParameterTypes3;
	private String _methodName4;
	private String[] _methodParameterTypes4;
	private String _methodName5;
	private String[] _methodParameterTypes5;
	private String _methodName6;
	private String[] _methodParameterTypes6;
	private String _methodName7;
	private String[] _methodParameterTypes7;
	private String _methodName8;
	private String[] _methodParameterTypes8;
	private String _methodName9;
	private String[] _methodParameterTypes9;
	private String _methodName10;
	private String[] _methodParameterTypes10;
	private String _methodName11;
	private String[] _methodParameterTypes11;
	private String _methodName12;
	private String[] _methodParameterTypes12;
	private String _methodName13;
	private String[] _methodParameterTypes13;
	private String _methodName14;
	private String[] _methodParameterTypes14;
	private String _methodName15;
	private String[] _methodParameterTypes15;
	private String _methodName142;
	private String[] _methodParameterTypes142;
	private String _methodName143;
	private String[] _methodParameterTypes143;
	private String _methodName148;
	private String[] _methodParameterTypes148;
	private String _methodName149;
	private String[] _methodParameterTypes149;
	private String _methodName150;
	private String[] _methodParameterTypes150;
	private String _methodName151;
	private String[] _methodParameterTypes151;
	private String _methodName152;
	private String[] _methodParameterTypes152;
	private String _methodName153;
	private String[] _methodParameterTypes153;
	private String _methodName154;
	private String[] _methodParameterTypes154;
	private String _methodName155;
	private String[] _methodParameterTypes155;
}