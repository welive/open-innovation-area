/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing CLSRequisiti in entity cache.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSRequisiti
 * @generated
 */
public class CLSRequisitiCacheModel implements CacheModel<CLSRequisiti>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{requisitoId=");
		sb.append(requisitoId);
		sb.append(", ideaId=");
		sb.append(ideaId);
		sb.append(", taskId=");
		sb.append(taskId);
		sb.append(", tipo=");
		sb.append(tipo);
		sb.append(", descrizione=");
		sb.append(descrizione);
		sb.append(", authorUser=");
		sb.append(authorUser);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CLSRequisiti toEntityModel() {
		CLSRequisitiImpl clsRequisitiImpl = new CLSRequisitiImpl();

		clsRequisitiImpl.setRequisitoId(requisitoId);
		clsRequisitiImpl.setIdeaId(ideaId);
		clsRequisitiImpl.setTaskId(taskId);

		if (tipo == null) {
			clsRequisitiImpl.setTipo(StringPool.BLANK);
		}
		else {
			clsRequisitiImpl.setTipo(tipo);
		}

		if (descrizione == null) {
			clsRequisitiImpl.setDescrizione(StringPool.BLANK);
		}
		else {
			clsRequisitiImpl.setDescrizione(descrizione);
		}

		clsRequisitiImpl.setAuthorUser(authorUser);

		clsRequisitiImpl.resetOriginalValues();

		return clsRequisitiImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		requisitoId = objectInput.readLong();
		ideaId = objectInput.readLong();
		taskId = objectInput.readLong();
		tipo = objectInput.readUTF();
		descrizione = objectInput.readUTF();
		authorUser = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(requisitoId);
		objectOutput.writeLong(ideaId);
		objectOutput.writeLong(taskId);

		if (tipo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipo);
		}

		if (descrizione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descrizione);
		}

		objectOutput.writeLong(authorUser);
	}

	public long requisitoId;
	public long ideaId;
	public long taskId;
	public String tipo;
	public String descrizione;
	public long authorUser;
}