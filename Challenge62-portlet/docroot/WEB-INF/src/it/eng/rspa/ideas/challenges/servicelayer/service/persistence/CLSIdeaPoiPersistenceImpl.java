/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaPoiException;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaPoiImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaPoiModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the c l s idea poi service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSIdeaPoiPersistence
 * @see CLSIdeaPoiUtil
 * @generated
 */
public class CLSIdeaPoiPersistenceImpl extends BasePersistenceImpl<CLSIdeaPoi>
	implements CLSIdeaPoiPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CLSIdeaPoiUtil} to access the c l s idea poi persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CLSIdeaPoiImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CLSIdeaPoiModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaPoiModelImpl.FINDER_CACHE_ENABLED, CLSIdeaPoiImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CLSIdeaPoiModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaPoiModelImpl.FINDER_CACHE_ENABLED, CLSIdeaPoiImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CLSIdeaPoiModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaPoiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEAID = new FinderPath(CLSIdeaPoiModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaPoiModelImpl.FINDER_CACHE_ENABLED, CLSIdeaPoiImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByIdeaId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID =
		new FinderPath(CLSIdeaPoiModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaPoiModelImpl.FINDER_CACHE_ENABLED, CLSIdeaPoiImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByIdeaId",
			new String[] { Long.class.getName() },
			CLSIdeaPoiModelImpl.IDEAID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IDEAID = new FinderPath(CLSIdeaPoiModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaPoiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByIdeaId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the c l s idea pois where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @return the matching c l s idea pois
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdeaPoi> findByIdeaId(long ideaId) throws SystemException {
		return findByIdeaId(ideaId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s idea pois where ideaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaPoiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param start the lower bound of the range of c l s idea pois
	 * @param end the upper bound of the range of c l s idea pois (not inclusive)
	 * @return the range of matching c l s idea pois
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdeaPoi> findByIdeaId(long ideaId, int start, int end)
		throws SystemException {
		return findByIdeaId(ideaId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s idea pois where ideaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaPoiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param start the lower bound of the range of c l s idea pois
	 * @param end the upper bound of the range of c l s idea pois (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s idea pois
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdeaPoi> findByIdeaId(long ideaId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID;
			finderArgs = new Object[] { ideaId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEAID;
			finderArgs = new Object[] { ideaId, start, end, orderByComparator };
		}

		List<CLSIdeaPoi> list = (List<CLSIdeaPoi>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSIdeaPoi clsIdeaPoi : list) {
				if ((ideaId != clsIdeaPoi.getIdeaId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSIDEAPOI_WHERE);

			query.append(_FINDER_COLUMN_IDEAID_IDEAID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSIdeaPoiModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				if (!pagination) {
					list = (List<CLSIdeaPoi>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSIdeaPoi>(list);
				}
				else {
					list = (List<CLSIdeaPoi>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s idea poi in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s idea poi
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaPoiException if a matching c l s idea poi could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdeaPoi findByIdeaId_First(long ideaId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaPoiException, SystemException {
		CLSIdeaPoi clsIdeaPoi = fetchByIdeaId_First(ideaId, orderByComparator);

		if (clsIdeaPoi != null) {
			return clsIdeaPoi;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSIdeaPoiException(msg.toString());
	}

	/**
	 * Returns the first c l s idea poi in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s idea poi, or <code>null</code> if a matching c l s idea poi could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdeaPoi fetchByIdeaId_First(long ideaId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSIdeaPoi> list = findByIdeaId(ideaId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s idea poi in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s idea poi
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaPoiException if a matching c l s idea poi could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdeaPoi findByIdeaId_Last(long ideaId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaPoiException, SystemException {
		CLSIdeaPoi clsIdeaPoi = fetchByIdeaId_Last(ideaId, orderByComparator);

		if (clsIdeaPoi != null) {
			return clsIdeaPoi;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSIdeaPoiException(msg.toString());
	}

	/**
	 * Returns the last c l s idea poi in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s idea poi, or <code>null</code> if a matching c l s idea poi could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdeaPoi fetchByIdeaId_Last(long ideaId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByIdeaId(ideaId);

		if (count == 0) {
			return null;
		}

		List<CLSIdeaPoi> list = findByIdeaId(ideaId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s idea pois before and after the current c l s idea poi in the ordered set where ideaId = &#63;.
	 *
	 * @param poiId the primary key of the current c l s idea poi
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s idea poi
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaPoiException if a c l s idea poi with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdeaPoi[] findByIdeaId_PrevAndNext(long poiId, long ideaId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaPoiException, SystemException {
		CLSIdeaPoi clsIdeaPoi = findByPrimaryKey(poiId);

		Session session = null;

		try {
			session = openSession();

			CLSIdeaPoi[] array = new CLSIdeaPoiImpl[3];

			array[0] = getByIdeaId_PrevAndNext(session, clsIdeaPoi, ideaId,
					orderByComparator, true);

			array[1] = clsIdeaPoi;

			array[2] = getByIdeaId_PrevAndNext(session, clsIdeaPoi, ideaId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSIdeaPoi getByIdeaId_PrevAndNext(Session session,
		CLSIdeaPoi clsIdeaPoi, long ideaId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSIDEAPOI_WHERE);

		query.append(_FINDER_COLUMN_IDEAID_IDEAID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSIdeaPoiModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(ideaId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsIdeaPoi);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSIdeaPoi> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s idea pois where ideaId = &#63; from the database.
	 *
	 * @param ideaId the idea ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByIdeaId(long ideaId) throws SystemException {
		for (CLSIdeaPoi clsIdeaPoi : findByIdeaId(ideaId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(clsIdeaPoi);
		}
	}

	/**
	 * Returns the number of c l s idea pois where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @return the number of matching c l s idea pois
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByIdeaId(long ideaId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IDEAID;

		Object[] finderArgs = new Object[] { ideaId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSIDEAPOI_WHERE);

			query.append(_FINDER_COLUMN_IDEAID_IDEAID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IDEAID_IDEAID_2 = "clsIdeaPoi.ideaId = ?";

	public CLSIdeaPoiPersistenceImpl() {
		setModelClass(CLSIdeaPoi.class);
	}

	/**
	 * Caches the c l s idea poi in the entity cache if it is enabled.
	 *
	 * @param clsIdeaPoi the c l s idea poi
	 */
	@Override
	public void cacheResult(CLSIdeaPoi clsIdeaPoi) {
		EntityCacheUtil.putResult(CLSIdeaPoiModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaPoiImpl.class, clsIdeaPoi.getPrimaryKey(), clsIdeaPoi);

		clsIdeaPoi.resetOriginalValues();
	}

	/**
	 * Caches the c l s idea pois in the entity cache if it is enabled.
	 *
	 * @param clsIdeaPois the c l s idea pois
	 */
	@Override
	public void cacheResult(List<CLSIdeaPoi> clsIdeaPois) {
		for (CLSIdeaPoi clsIdeaPoi : clsIdeaPois) {
			if (EntityCacheUtil.getResult(
						CLSIdeaPoiModelImpl.ENTITY_CACHE_ENABLED,
						CLSIdeaPoiImpl.class, clsIdeaPoi.getPrimaryKey()) == null) {
				cacheResult(clsIdeaPoi);
			}
			else {
				clsIdeaPoi.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all c l s idea pois.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CLSIdeaPoiImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CLSIdeaPoiImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the c l s idea poi.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CLSIdeaPoi clsIdeaPoi) {
		EntityCacheUtil.removeResult(CLSIdeaPoiModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaPoiImpl.class, clsIdeaPoi.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<CLSIdeaPoi> clsIdeaPois) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CLSIdeaPoi clsIdeaPoi : clsIdeaPois) {
			EntityCacheUtil.removeResult(CLSIdeaPoiModelImpl.ENTITY_CACHE_ENABLED,
				CLSIdeaPoiImpl.class, clsIdeaPoi.getPrimaryKey());
		}
	}

	/**
	 * Creates a new c l s idea poi with the primary key. Does not add the c l s idea poi to the database.
	 *
	 * @param poiId the primary key for the new c l s idea poi
	 * @return the new c l s idea poi
	 */
	@Override
	public CLSIdeaPoi create(long poiId) {
		CLSIdeaPoi clsIdeaPoi = new CLSIdeaPoiImpl();

		clsIdeaPoi.setNew(true);
		clsIdeaPoi.setPrimaryKey(poiId);

		return clsIdeaPoi;
	}

	/**
	 * Removes the c l s idea poi with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param poiId the primary key of the c l s idea poi
	 * @return the c l s idea poi that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaPoiException if a c l s idea poi with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdeaPoi remove(long poiId)
		throws NoSuchCLSIdeaPoiException, SystemException {
		return remove((Serializable)poiId);
	}

	/**
	 * Removes the c l s idea poi with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the c l s idea poi
	 * @return the c l s idea poi that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaPoiException if a c l s idea poi with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdeaPoi remove(Serializable primaryKey)
		throws NoSuchCLSIdeaPoiException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CLSIdeaPoi clsIdeaPoi = (CLSIdeaPoi)session.get(CLSIdeaPoiImpl.class,
					primaryKey);

			if (clsIdeaPoi == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCLSIdeaPoiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(clsIdeaPoi);
		}
		catch (NoSuchCLSIdeaPoiException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CLSIdeaPoi removeImpl(CLSIdeaPoi clsIdeaPoi)
		throws SystemException {
		clsIdeaPoi = toUnwrappedModel(clsIdeaPoi);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(clsIdeaPoi)) {
				clsIdeaPoi = (CLSIdeaPoi)session.get(CLSIdeaPoiImpl.class,
						clsIdeaPoi.getPrimaryKeyObj());
			}

			if (clsIdeaPoi != null) {
				session.delete(clsIdeaPoi);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (clsIdeaPoi != null) {
			clearCache(clsIdeaPoi);
		}

		return clsIdeaPoi;
	}

	@Override
	public CLSIdeaPoi updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi clsIdeaPoi)
		throws SystemException {
		clsIdeaPoi = toUnwrappedModel(clsIdeaPoi);

		boolean isNew = clsIdeaPoi.isNew();

		CLSIdeaPoiModelImpl clsIdeaPoiModelImpl = (CLSIdeaPoiModelImpl)clsIdeaPoi;

		Session session = null;

		try {
			session = openSession();

			if (clsIdeaPoi.isNew()) {
				session.save(clsIdeaPoi);

				clsIdeaPoi.setNew(false);
			}
			else {
				session.merge(clsIdeaPoi);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CLSIdeaPoiModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((clsIdeaPoiModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsIdeaPoiModelImpl.getOriginalIdeaId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID,
					args);

				args = new Object[] { clsIdeaPoiModelImpl.getIdeaId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID,
					args);
			}
		}

		EntityCacheUtil.putResult(CLSIdeaPoiModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaPoiImpl.class, clsIdeaPoi.getPrimaryKey(), clsIdeaPoi);

		return clsIdeaPoi;
	}

	protected CLSIdeaPoi toUnwrappedModel(CLSIdeaPoi clsIdeaPoi) {
		if (clsIdeaPoi instanceof CLSIdeaPoiImpl) {
			return clsIdeaPoi;
		}

		CLSIdeaPoiImpl clsIdeaPoiImpl = new CLSIdeaPoiImpl();

		clsIdeaPoiImpl.setNew(clsIdeaPoi.isNew());
		clsIdeaPoiImpl.setPrimaryKey(clsIdeaPoi.getPrimaryKey());

		clsIdeaPoiImpl.setIdeaId(clsIdeaPoi.getIdeaId());
		clsIdeaPoiImpl.setLatitude(clsIdeaPoi.getLatitude());
		clsIdeaPoiImpl.setLongitude(clsIdeaPoi.getLongitude());
		clsIdeaPoiImpl.setDescription(clsIdeaPoi.getDescription());
		clsIdeaPoiImpl.setPoiId(clsIdeaPoi.getPoiId());
		clsIdeaPoiImpl.setTitle(clsIdeaPoi.getTitle());

		return clsIdeaPoiImpl;
	}

	/**
	 * Returns the c l s idea poi with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s idea poi
	 * @return the c l s idea poi
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaPoiException if a c l s idea poi with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdeaPoi findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCLSIdeaPoiException, SystemException {
		CLSIdeaPoi clsIdeaPoi = fetchByPrimaryKey(primaryKey);

		if (clsIdeaPoi == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCLSIdeaPoiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return clsIdeaPoi;
	}

	/**
	 * Returns the c l s idea poi with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaPoiException} if it could not be found.
	 *
	 * @param poiId the primary key of the c l s idea poi
	 * @return the c l s idea poi
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaPoiException if a c l s idea poi with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdeaPoi findByPrimaryKey(long poiId)
		throws NoSuchCLSIdeaPoiException, SystemException {
		return findByPrimaryKey((Serializable)poiId);
	}

	/**
	 * Returns the c l s idea poi with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s idea poi
	 * @return the c l s idea poi, or <code>null</code> if a c l s idea poi with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdeaPoi fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		CLSIdeaPoi clsIdeaPoi = (CLSIdeaPoi)EntityCacheUtil.getResult(CLSIdeaPoiModelImpl.ENTITY_CACHE_ENABLED,
				CLSIdeaPoiImpl.class, primaryKey);

		if (clsIdeaPoi == _nullCLSIdeaPoi) {
			return null;
		}

		if (clsIdeaPoi == null) {
			Session session = null;

			try {
				session = openSession();

				clsIdeaPoi = (CLSIdeaPoi)session.get(CLSIdeaPoiImpl.class,
						primaryKey);

				if (clsIdeaPoi != null) {
					cacheResult(clsIdeaPoi);
				}
				else {
					EntityCacheUtil.putResult(CLSIdeaPoiModelImpl.ENTITY_CACHE_ENABLED,
						CLSIdeaPoiImpl.class, primaryKey, _nullCLSIdeaPoi);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CLSIdeaPoiModelImpl.ENTITY_CACHE_ENABLED,
					CLSIdeaPoiImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return clsIdeaPoi;
	}

	/**
	 * Returns the c l s idea poi with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param poiId the primary key of the c l s idea poi
	 * @return the c l s idea poi, or <code>null</code> if a c l s idea poi with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdeaPoi fetchByPrimaryKey(long poiId) throws SystemException {
		return fetchByPrimaryKey((Serializable)poiId);
	}

	/**
	 * Returns all the c l s idea pois.
	 *
	 * @return the c l s idea pois
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdeaPoi> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s idea pois.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaPoiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s idea pois
	 * @param end the upper bound of the range of c l s idea pois (not inclusive)
	 * @return the range of c l s idea pois
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdeaPoi> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s idea pois.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaPoiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s idea pois
	 * @param end the upper bound of the range of c l s idea pois (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of c l s idea pois
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdeaPoi> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CLSIdeaPoi> list = (List<CLSIdeaPoi>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CLSIDEAPOI);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CLSIDEAPOI;

				if (pagination) {
					sql = sql.concat(CLSIdeaPoiModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CLSIdeaPoi>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSIdeaPoi>(list);
				}
				else {
					list = (List<CLSIdeaPoi>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the c l s idea pois from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (CLSIdeaPoi clsIdeaPoi : findAll()) {
			remove(clsIdeaPoi);
		}
	}

	/**
	 * Returns the number of c l s idea pois.
	 *
	 * @return the number of c l s idea pois
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CLSIDEAPOI);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the c l s idea poi persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CLSIdeaPoi>> listenersList = new ArrayList<ModelListener<CLSIdeaPoi>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CLSIdeaPoi>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CLSIdeaPoiImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CLSIDEAPOI = "SELECT clsIdeaPoi FROM CLSIdeaPoi clsIdeaPoi";
	private static final String _SQL_SELECT_CLSIDEAPOI_WHERE = "SELECT clsIdeaPoi FROM CLSIdeaPoi clsIdeaPoi WHERE ";
	private static final String _SQL_COUNT_CLSIDEAPOI = "SELECT COUNT(clsIdeaPoi) FROM CLSIdeaPoi clsIdeaPoi";
	private static final String _SQL_COUNT_CLSIDEAPOI_WHERE = "SELECT COUNT(clsIdeaPoi) FROM CLSIdeaPoi clsIdeaPoi WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "clsIdeaPoi.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CLSIdeaPoi exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CLSIdeaPoi exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CLSIdeaPoiPersistenceImpl.class);
	private static CLSIdeaPoi _nullCLSIdeaPoi = new CLSIdeaPoiImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CLSIdeaPoi> toCacheModel() {
				return _nullCLSIdeaPoiCacheModel;
			}
		};

	private static CacheModel<CLSIdeaPoi> _nullCLSIdeaPoiCacheModel = new CacheModel<CLSIdeaPoi>() {
			@Override
			public CLSIdeaPoi toEntityModel() {
				return _nullCLSIdeaPoi;
			}
		};
}