/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing CLSChallenge in entity cache.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSChallenge
 * @generated
 */
public class CLSChallengeCacheModel implements CacheModel<CLSChallenge>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(49);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", challengeId=");
		sb.append(challengeId);
		sb.append(", challengeTitle=");
		sb.append(challengeTitle);
		sb.append(", challengeDescription=");
		sb.append(challengeDescription);
		sb.append(", challengeWithReward=");
		sb.append(challengeWithReward);
		sb.append(", challengeReward=");
		sb.append(challengeReward);
		sb.append(", numberOfSelectableIdeas=");
		sb.append(numberOfSelectableIdeas);
		sb.append(", dateAdded=");
		sb.append(dateAdded);
		sb.append(", challengeStatus=");
		sb.append(challengeStatus);
		sb.append(", status=");
		sb.append(status);
		sb.append(", statusByUserId=");
		sb.append(statusByUserId);
		sb.append(", statusByUserName=");
		sb.append(statusByUserName);
		sb.append(", statusDate=");
		sb.append(statusDate);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", dateEnd=");
		sb.append(dateEnd);
		sb.append(", dateStart=");
		sb.append(dateStart);
		sb.append(", language=");
		sb.append(language);
		sb.append(", challengeHashTag=");
		sb.append(challengeHashTag);
		sb.append(", dmFolderName=");
		sb.append(dmFolderName);
		sb.append(", idFolder=");
		sb.append(idFolder);
		sb.append(", representativeImgUrl=");
		sb.append(representativeImgUrl);
		sb.append(", calendarBooking=");
		sb.append(calendarBooking);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CLSChallenge toEntityModel() {
		CLSChallengeImpl clsChallengeImpl = new CLSChallengeImpl();

		if (uuid == null) {
			clsChallengeImpl.setUuid(StringPool.BLANK);
		}
		else {
			clsChallengeImpl.setUuid(uuid);
		}

		clsChallengeImpl.setChallengeId(challengeId);

		if (challengeTitle == null) {
			clsChallengeImpl.setChallengeTitle(StringPool.BLANK);
		}
		else {
			clsChallengeImpl.setChallengeTitle(challengeTitle);
		}

		if (challengeDescription == null) {
			clsChallengeImpl.setChallengeDescription(StringPool.BLANK);
		}
		else {
			clsChallengeImpl.setChallengeDescription(challengeDescription);
		}

		clsChallengeImpl.setChallengeWithReward(challengeWithReward);

		if (challengeReward == null) {
			clsChallengeImpl.setChallengeReward(StringPool.BLANK);
		}
		else {
			clsChallengeImpl.setChallengeReward(challengeReward);
		}

		clsChallengeImpl.setNumberOfSelectableIdeas(numberOfSelectableIdeas);

		if (dateAdded == Long.MIN_VALUE) {
			clsChallengeImpl.setDateAdded(null);
		}
		else {
			clsChallengeImpl.setDateAdded(new Date(dateAdded));
		}

		if (challengeStatus == null) {
			clsChallengeImpl.setChallengeStatus(StringPool.BLANK);
		}
		else {
			clsChallengeImpl.setChallengeStatus(challengeStatus);
		}

		clsChallengeImpl.setStatus(status);
		clsChallengeImpl.setStatusByUserId(statusByUserId);

		if (statusByUserName == null) {
			clsChallengeImpl.setStatusByUserName(StringPool.BLANK);
		}
		else {
			clsChallengeImpl.setStatusByUserName(statusByUserName);
		}

		if (statusDate == Long.MIN_VALUE) {
			clsChallengeImpl.setStatusDate(null);
		}
		else {
			clsChallengeImpl.setStatusDate(new Date(statusDate));
		}

		clsChallengeImpl.setCompanyId(companyId);
		clsChallengeImpl.setGroupId(groupId);
		clsChallengeImpl.setUserId(userId);

		if (dateEnd == Long.MIN_VALUE) {
			clsChallengeImpl.setDateEnd(null);
		}
		else {
			clsChallengeImpl.setDateEnd(new Date(dateEnd));
		}

		if (dateStart == Long.MIN_VALUE) {
			clsChallengeImpl.setDateStart(null);
		}
		else {
			clsChallengeImpl.setDateStart(new Date(dateStart));
		}

		if (language == null) {
			clsChallengeImpl.setLanguage(StringPool.BLANK);
		}
		else {
			clsChallengeImpl.setLanguage(language);
		}

		if (challengeHashTag == null) {
			clsChallengeImpl.setChallengeHashTag(StringPool.BLANK);
		}
		else {
			clsChallengeImpl.setChallengeHashTag(challengeHashTag);
		}

		if (dmFolderName == null) {
			clsChallengeImpl.setDmFolderName(StringPool.BLANK);
		}
		else {
			clsChallengeImpl.setDmFolderName(dmFolderName);
		}

		clsChallengeImpl.setIdFolder(idFolder);

		if (representativeImgUrl == null) {
			clsChallengeImpl.setRepresentativeImgUrl(StringPool.BLANK);
		}
		else {
			clsChallengeImpl.setRepresentativeImgUrl(representativeImgUrl);
		}

		clsChallengeImpl.setCalendarBooking(calendarBooking);

		clsChallengeImpl.resetOriginalValues();

		return clsChallengeImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();
		challengeId = objectInput.readLong();
		challengeTitle = objectInput.readUTF();
		challengeDescription = objectInput.readUTF();
		challengeWithReward = objectInput.readBoolean();
		challengeReward = objectInput.readUTF();
		numberOfSelectableIdeas = objectInput.readInt();
		dateAdded = objectInput.readLong();
		challengeStatus = objectInput.readUTF();
		status = objectInput.readInt();
		statusByUserId = objectInput.readLong();
		statusByUserName = objectInput.readUTF();
		statusDate = objectInput.readLong();
		companyId = objectInput.readLong();
		groupId = objectInput.readLong();
		userId = objectInput.readLong();
		dateEnd = objectInput.readLong();
		dateStart = objectInput.readLong();
		language = objectInput.readUTF();
		challengeHashTag = objectInput.readUTF();
		dmFolderName = objectInput.readUTF();
		idFolder = objectInput.readLong();
		representativeImgUrl = objectInput.readUTF();
		calendarBooking = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(challengeId);

		if (challengeTitle == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(challengeTitle);
		}

		if (challengeDescription == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(challengeDescription);
		}

		objectOutput.writeBoolean(challengeWithReward);

		if (challengeReward == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(challengeReward);
		}

		objectOutput.writeInt(numberOfSelectableIdeas);
		objectOutput.writeLong(dateAdded);

		if (challengeStatus == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(challengeStatus);
		}

		objectOutput.writeInt(status);
		objectOutput.writeLong(statusByUserId);

		if (statusByUserName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(statusByUserName);
		}

		objectOutput.writeLong(statusDate);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(groupId);
		objectOutput.writeLong(userId);
		objectOutput.writeLong(dateEnd);
		objectOutput.writeLong(dateStart);

		if (language == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(language);
		}

		if (challengeHashTag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(challengeHashTag);
		}

		if (dmFolderName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(dmFolderName);
		}

		objectOutput.writeLong(idFolder);

		if (representativeImgUrl == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(representativeImgUrl);
		}

		objectOutput.writeLong(calendarBooking);
	}

	public String uuid;
	public long challengeId;
	public String challengeTitle;
	public String challengeDescription;
	public boolean challengeWithReward;
	public String challengeReward;
	public int numberOfSelectableIdeas;
	public long dateAdded;
	public String challengeStatus;
	public int status;
	public long statusByUserId;
	public String statusByUserName;
	public long statusDate;
	public long companyId;
	public long groupId;
	public long userId;
	public long dateEnd;
	public long dateStart;
	public String language;
	public String challengeHashTag;
	public String dmFolderName;
	public long idFolder;
	public String representativeImgUrl;
	public long calendarBooking;
}