package it.eng.rspa.ideas.utils.listener;

import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.GregorianCalendar;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

public abstract class LoggingBuildingBlockNotificationService {

	private static String loggingBBUrl = IdeaManagementSystemProperties.getProperty("lbbAddress");
	protected static Logger _log = Logger.getLogger(LoggingBuildingBlockNotificationService.class);
	
	private static JSONObject prepareMessage(String actionType, JSONObject message){
		
		JSONObject messageWrapper = JSONFactoryUtil.createJSONObject();
		messageWrapper.put("appId", IdeaManagementSystemProperties.getProperty("oiaAppId4lbb"));
		messageWrapper.put("timestamp", GregorianCalendar.getInstance().getTimeInMillis());
		messageWrapper.put("type", actionType);
		messageWrapper.put("msg", actionType);
		messageWrapper.put("custom_attr", message);
		
		return messageWrapper;
		
	}
	
	private static ClientResponse invoke(String httpMethod, String url, JSONObject body) 
			throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		
		Client client = Client.create();
		client.addFilter(new HTTPBasicAuthFilter(IdeaManagementSystemProperties.getProperty("basicAuthUser"), IdeaManagementSystemProperties.getProperty("basicAuthPwd")));
		WebResource webResource = client.resource(url);
		WebResource.Builder builder = webResource.type(MediaType.APPLICATION_JSON);
		
		Class<?>[] argTypes = new Class[] { Class.class, Object.class };
		Method method = WebResource.Builder.class.getMethod(httpMethod.toLowerCase(), argTypes);
		
		ClientResponse resp = (ClientResponse)method.invoke(builder, ClientResponse.class, body.toString());
		
		return resp;
	}
	
	protected static void sendNotification(String actionType, JSONObject message){
		
		String url = loggingBBUrl+IdeaManagementSystemProperties.getProperty("oiaAppId4lbb");
		
		JSONObject messageWrapper = prepareMessage(actionType, message);
		
		ClientResponse resp = null;
		try { 
			resp = invoke("post", url, messageWrapper);
			if(resp==null) throw new NullPointerException();
		} 
		catch (Exception e) {
			_log.error("Unable to notify the Logging BuildingBlock. "+e.getMessage());
			e.printStackTrace();
			return;
		}

		String respString = resp.getEntity(String.class);
		
		if(resp.getStatus() != 200){
			_log.error("Service "+url+" responded with status "+resp.getStatus());
			System.out.println(respString);
		}
		else{ _log.info("Logging BuildingBlock notified correctly"); }
		return;
	}
	
}
