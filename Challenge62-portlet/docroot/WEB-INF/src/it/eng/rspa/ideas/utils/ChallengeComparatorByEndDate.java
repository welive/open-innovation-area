package it.eng.rspa.ideas.utils;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;

import java.util.Comparator;

public class ChallengeComparatorByEndDate implements Comparator<CLSChallenge> {

	@Override
	public int compare(CLSChallenge gara1, CLSChallenge gara2) {
		
		int ret=gara2.getDateEnd().compareTo(gara1.getDateEnd()); //mette le piu nuove in testa
		
		return ret;
		
	}

}
