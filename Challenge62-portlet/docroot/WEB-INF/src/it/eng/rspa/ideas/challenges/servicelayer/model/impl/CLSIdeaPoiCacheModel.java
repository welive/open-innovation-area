/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing CLSIdeaPoi in entity cache.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSIdeaPoi
 * @generated
 */
public class CLSIdeaPoiCacheModel implements CacheModel<CLSIdeaPoi>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{ideaId=");
		sb.append(ideaId);
		sb.append(", latitude=");
		sb.append(latitude);
		sb.append(", longitude=");
		sb.append(longitude);
		sb.append(", description=");
		sb.append(description);
		sb.append(", poiId=");
		sb.append(poiId);
		sb.append(", title=");
		sb.append(title);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CLSIdeaPoi toEntityModel() {
		CLSIdeaPoiImpl clsIdeaPoiImpl = new CLSIdeaPoiImpl();

		clsIdeaPoiImpl.setIdeaId(ideaId);

		if (latitude == null) {
			clsIdeaPoiImpl.setLatitude(StringPool.BLANK);
		}
		else {
			clsIdeaPoiImpl.setLatitude(latitude);
		}

		if (longitude == null) {
			clsIdeaPoiImpl.setLongitude(StringPool.BLANK);
		}
		else {
			clsIdeaPoiImpl.setLongitude(longitude);
		}

		if (description == null) {
			clsIdeaPoiImpl.setDescription(StringPool.BLANK);
		}
		else {
			clsIdeaPoiImpl.setDescription(description);
		}

		clsIdeaPoiImpl.setPoiId(poiId);

		if (title == null) {
			clsIdeaPoiImpl.setTitle(StringPool.BLANK);
		}
		else {
			clsIdeaPoiImpl.setTitle(title);
		}

		clsIdeaPoiImpl.resetOriginalValues();

		return clsIdeaPoiImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		ideaId = objectInput.readLong();
		latitude = objectInput.readUTF();
		longitude = objectInput.readUTF();
		description = objectInput.readUTF();
		poiId = objectInput.readLong();
		title = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(ideaId);

		if (latitude == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(latitude);
		}

		if (longitude == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(longitude);
		}

		if (description == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(description);
		}

		objectOutput.writeLong(poiId);

		if (title == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(title);
		}
	}

	public long ideaId;
	public String latitude;
	public String longitude;
	public String description;
	public long poiId;
	public String title;
}