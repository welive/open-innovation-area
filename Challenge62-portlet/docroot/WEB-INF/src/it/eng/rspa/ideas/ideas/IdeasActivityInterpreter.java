package it.eng.rspa.ideas.ideas;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;

import java.util.Iterator;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.Layout;
import com.liferay.portal.model.User;
import com.liferay.portal.service.LayoutLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.social.model.BaseSocialActivityInterpreter;
import com.liferay.portlet.social.model.SocialActivity;
import com.liferay.portlet.social.model.SocialActivityFeedEntry;

public class IdeasActivityInterpreter extends BaseSocialActivityInterpreter {

	private static final String[] _CLASS_NAMES = new String[] {
		CLSIdea.class.getName()
	};
	
	@Override
	public String[] getClassNames() {
		return _CLASS_NAMES;
	}

	@Override
	protected SocialActivityFeedEntry doInterpret(SocialActivity activity, ThemeDisplay themeDisplay) throws Exception {

	int type = activity.getType();
	
	CLSIdea idea = null;
	try {
		idea = CLSIdeaLocalServiceUtil.getCLSIdea(activity.getClassPK());
	} catch (PortalException e) {
		e.printStackTrace();
	} catch (SystemException e) { 
		e.printStackTrace();
	}
	
	if(idea!=null){
		User user;
		try {
			user = UserLocalServiceUtil.getUser(activity.getUserId());
			
			String userName = user.getFirstName();
			
			String linkUserProfile = user.getDisplayURL(themeDisplay);
			
			//String key = "activity-product-registration-add-registration";
			
			String key = "";
			switch (type) {
			  case 1: {
				  key = "Ha creato una nuova idea:";
			    break;
			  }

			  case 2: {
				  key = "Ha aggiornato l'idea: ";
			    break;
			  }
			}

			//String linkToChallenge = themeDisplay.getPortalURL() + themeDisplay.getPathMain() + "/blogs/find_entry?entryId=" + activity.getClassPK();
			
			String  linkToIdea = null;//"http://localhost:8080/web/guest/home?p_p_id=Challenges_WAR_Challengesportlet?%20portlet&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=?%20column-2&p_p_col_pos=1&p_p_col_count=2&_challenges_WAR_?%20challengesportlet_jspPage=/html/challenges/view_challenge.jsp?challengeId=" + challenge.getChallengeId();

			
			List<Layout> layouts = LayoutLocalServiceUtil.getLayouts(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			Iterator<Layout> layoutsIt = layouts.iterator();
			while (layoutsIt.hasNext()) {
				Layout layout = (Layout) layoutsIt.next();
			}
			String title = getTitle(activity, key, linkToIdea, idea.getIdeaTitle(), linkUserProfile, userName, themeDisplay);
			String body = StringPool.BLANK;
			
			SocialActivityFeedEntry socialActivityFeedEntry = new SocialActivityFeedEntry(linkUserProfile, title, body);
			
			return socialActivityFeedEntry;
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	return null;
	}

	
	protected String getTitle(SocialActivity activity, String key, String linkToIdea, String idea, String linkUserProfile, String userName, ThemeDisplay themeDisplay) {
		
		idea = HtmlUtil.escape(cleanContent(idea));
		key = HtmlUtil.escape(cleanContent(key));
		userName = HtmlUtil.escape(cleanContent(userName));
		
		String witheSpace = HtmlUtil.escape((" "));
		
		String userNameProfile = "";
//		String challengeLink = "";
		
			
			String text = HtmlUtil.escape(cleanContent(idea));
			
			if (Validator.isNotNull(linkUserProfile)) {
				userNameProfile = wrapLink(linkUserProfile, userName);
			}
			
//			if (Validator.isNotNull(linkToChallenge)) {
//				challengeLink = wrapLink(linkToChallenge, challenge);
//			}
			
//			String groupName = StringPool.BLANK;
//			if (activity.getGroupId() != themeDisplay.getScopeGroupId()) {
//				groupName = getGroupName(activity.getGroupId(),themeDisplay);
//			}
//			
//			String pattern = key;
//			if (Validator.isNotNull(groupName)) {
//				pattern += "-in";
//			}
			
			return userNameProfile + witheSpace + key + witheSpace + idea;//text;//themeDisplay.translate(pattern, new Object[] {userName, text, groupName});
		
//		return key + " " + content;
	}

}
