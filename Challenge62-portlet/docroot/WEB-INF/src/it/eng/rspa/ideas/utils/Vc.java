package it.eng.rspa.ideas.utils;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSVmeProjectsImpl;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCoworkerLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSVmeProjectsLocalServiceUtil;
import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserGroup;
import com.liferay.portal.service.UserGroupLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;

public class Vc {
	
	//Basic Authentication
	static String USER = IdeaManagementSystemProperties.getProperty("basicAuthUser");
	static String PWD = IdeaManagementSystemProperties.getProperty("basicAuthPwd");
	
	
	public static void vcNotification(CLSIdea idea, ArrayList<Long> coworkersUserIdList) {
		
		//coworkersUserIdList != null solo in update
		
		
		long ideaId = idea.getIdeaID();
		
		
		
			String ideaName = idea.getIdeaTitle();
			Long idAuthority = idea.getMunicipalityId();
			User utenteAuth = null;
			try {
				utenteAuth = UserLocalServiceUtil.getUserById(idAuthority);
			} catch (PortalException | SystemException e) {
				
				e.printStackTrace();
			}
			String mailUtenteAuth = utenteAuth.getEmailAddress();
			
			
			String authorityOrCompanyOrganizationName = "";
			
			if (idea.isTakenUp())
				authorityOrCompanyOrganizationName = MyUtils.getOrganizationNameByLeaderId(idAuthority);
			else
				authorityOrCompanyOrganizationName = MyUtils.getOrganizationNameByMunicipalityId(idAuthority);
		
			
			
			
			UserGroup chatGroup = null;
			try {
				chatGroup = UserGroupLocalServiceUtil.fetchUserGroup(idea.getChatGroup());
			} catch (SystemException e) {
				
				e.printStackTrace();
			}
			String wgName = chatGroup.getName();
	
			//Utente autore-proprietario
			long idAutore = idea.getUserId();
			User autore = null;
			try {
				autore = UserLocalServiceUtil.getUserById(idAutore);
			} catch (PortalException | SystemException e) {
				
				e.printStackTrace();
			}
			String mailAutore = autore.getEmailAddress();
			
			JSONObject autoreJson = JSONFactoryUtil.createJSONObject();
			autoreJson.put("username",mailAutore);
			
			JSONArray memberslist = JSONFactoryUtil.createJSONArray();
			memberslist.put(autoreJson);
			
			
			if (coworkersUserIdList != null){
				
				for (Long cwId : coworkersUserIdList){
				
					User utente = null;
					try {
						utente = UserLocalServiceUtil.getUserById(cwId);
					} catch (PortalException | SystemException e) {
						
						e.printStackTrace();
					}
					String mailUtente = utente.getEmailAddress();
					
					JSONObject coworkerJson = JSONFactoryUtil.createJSONObject();
					coworkerJson.put("username",mailUtente);
					memberslist.put(coworkerJson);
				
				}
				
			}else{
			
					List<CLSCoworker> coworkersList = null;
					try {
						coworkersList = CLSCoworkerLocalServiceUtil.getCoworkersByIdeaId(ideaId);
					} catch (SystemException e) {
						
						e.printStackTrace();
					}
					Iterator<CLSCoworker> coworkersListIt = coworkersList.iterator();
					
					
					if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
					
						System.out.println ("Visual Composer Notification  ---- coworkersList --- "+coworkersList.size());
					}
					
					while (coworkersListIt.hasNext()) {
							CLSCoworker coworker = (CLSCoworker) coworkersListIt.next();
							
							long idUtente = coworker.getUserId();
							
//							if (UpdateIdeaUtils.isDeveloperByUserId(idUtente)){
							
							
								User utente = null;
								try {
									utente = UserLocalServiceUtil.getUserById(idUtente);
								} catch (PortalException | SystemException e) {
									
									e.printStackTrace();
								}
								String mailUtente = utente.getEmailAddress();
								
								JSONObject coworkerJson = JSONFactoryUtil.createJSONObject();
								coworkerJson.put("username",mailUtente);
								memberslist.put(coworkerJson);
							
//							}	
							
					}
			}
		
		/*	Struttura del JSON	di invio
			{
				"ideaid": <id dell'idea>,
		    	"ideaname": <nome dell'idea>,
		    	"authority":<nome utente (email)>,
		    	"wgname": <nome del workgroup>,
		  		"members":
		        	[
		            	{ "username": <nome utente (email)> },
		            	...
		         	]
		 	}
		*/		
		JSONObject  jsonCompleto = JSONFactoryUtil.createJSONObject();
		jsonCompleto.put("ideaid", ideaId);
		jsonCompleto.put("ideaname", ideaName);
		jsonCompleto.put("authority", mailUtenteAuth);
		jsonCompleto.put("authorityOrCompanyOrganizationName", authorityOrCompanyOrganizationName);
		jsonCompleto.put("authorityOrCompanyOrganizationId", idea.getMunicipalityOrganizationId());
		jsonCompleto.put("isTakenUp", idea.getTakenUp());
		jsonCompleto.put("wgname", wgName);
		jsonCompleto.put("members", memberslist);
		
		
		String vcEndPoint = IdeaManagementSystemProperties.getProperty("vcWSAddress")+"/ideaworkgroup";
		
		
		if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
			
			System.out.println("json sent to Visual Composer "+jsonCompleto);
		}
		
		
		String risposta = "";
		try {
			risposta = RestUtils.consumePostWs(jsonCompleto, vcEndPoint,USER, PWD );
		} catch (Exception e) {
			
			e.printStackTrace();
		}	
		
		
	    JSONObject rispostaJSON = null;
		try {
			rispostaJSON = JSONFactoryUtil.createJSONObject(risposta);
		} catch (JSONException e) {
			
			
			//e.printStackTrace();
			if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){	
				System.out.println("JSON exception, it is a String (sent by Swagger). I'm going to use the workaround");
			}
			
			
			/*
			"{\"message\":0,\"type\":0,\"workgroupid\":74,\"mashupid\":341,\"mockupid\":228,\"mashupname\":\"IdeaTestoilVC\",\"mockupname\":\"IdeaTestoilVC\"}"
			*/
			risposta = risposta.replaceAll("\\\\", ""); 
			risposta = risposta.substring(1, risposta.length() -1); //Remove the " at the begin and at the end of the string
			
//			risposta = risposta.replaceFirst("\"", "");
//			
//			String revRisp = new StringBuffer(risposta).reverse().toString();
//			revRisp = revRisp.replaceFirst("\"", "");
//			risposta= new StringBuffer(revRisp).reverse().toString();
			
			
			try {
				rispostaJSON = JSONFactoryUtil.createJSONObject(risposta);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			
		}
	    
	    
	    //Struttura JSON risposta
	    //risposta: {"message":0,"type":0,"workgroupid":8,"mashupid":31,"mockupid":17}

	    
	    String message =	rispostaJSON.getString("message");
	    String type =	rispostaJSON.getString("type");
	    //String workgroupid =	rispostaJSON.getString("workgroupid"); //momentaneamente inutile
	    String mashupid =	rispostaJSON.getString("mashupid");
	    String mockupid =	rispostaJSON.getString("mockupid");
	    String mashupname =	rispostaJSON.getString("mashupname");
	    String mockupname =	rispostaJSON.getString("mockupname");
	    

//		(type)
//	    MESSAGE_TYPE_SUCCESS = 0;
//	    MESSAGE_TYPE_ERROR = 1;

//	    (message)
//	   	MESSAGE_CREATED = 0; // workgroup e progetti creati correttamente
//	    MESSAGE_NO_MEMBERS = 1; // lista di membri vuota
//	    MESSAGE_NO_USER = 2; // utente non esistente sul VME
//	    MESSAGE_JSON_PARSING_ERROR = 3; // errore nel parsing del JSON
//	    MESSAGE_IDEAWG_EXISTS = 4; // associazione idea-workgroup gia' esistente
//	    MESSAGE_SERVER_ERROR = 5; // errore del VME
	    
//	    mashupid.equalsIgnoreCase("") in fase di aggiornamento dell'idea
	    
	    //se non ci sono errori salvo i dati sul DB
	    if (message.equals("0") && type.equals("0")  && !mashupid.equalsIgnoreCase("")){	   
	    	
	    	CLSVmeProjects mashupProject = new  CLSVmeProjectsImpl();
	    	try {
				mashupProject.setRecordId(CounterLocalServiceUtil.increment());
			} catch (SystemException e) {
				
				e.printStackTrace();
			}
	    	mashupProject.setVmeProjectId(Long.parseLong(mashupid));
	    	mashupProject.setVmeProjectName(mashupname);
	    	mashupProject.setIsMockup(false);
	    	mashupProject.setIdeaId(ideaId);
	    	try {
				CLSVmeProjectsLocalServiceUtil.addCLSVmeProjects(mashupProject);
			} catch (SystemException e) {
				
				e.printStackTrace();
			}
	    	
	    	CLSVmeProjects mockupProject = new  CLSVmeProjectsImpl();
	    	try {
				mockupProject.setRecordId(CounterLocalServiceUtil.increment());
			} catch (SystemException e) {
				
				e.printStackTrace();
			}
	    	mockupProject.setVmeProjectId(Long.parseLong(mockupid));
	    	mockupProject.setVmeProjectName(mockupname);
	    	mockupProject.setIsMockup(true);
	    	mockupProject.setIdeaId(ideaId);
	    	try {
				CLSVmeProjectsLocalServiceUtil.addCLSVmeProjects(mockupProject);
			} catch (SystemException e) {
				
				e.printStackTrace();
			}
	    	
	    }else{
	    	
	    	System.out.println("Is not possible to create/save the group for VC. Message "+message+" type "+type);
	    	
	    }
	
	}//vcNotification
}//class

