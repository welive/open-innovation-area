package it.eng.rspa.ideas.utils;

import java.util.Comparator;

import com.liferay.portal.model.User;

public class StringComparator implements Comparator<User> {

    public int compare(User u1, User u2) {
          
    	  int ret = 0;

          String name1 = u1.getLastName().toLowerCase() + u1.getFirstName().toLowerCase();
          String name2 = u2.getLastName().toLowerCase() + u2.getFirstName().toLowerCase();
          
          if(name1.compareTo(name2) > 0)
              return 1;
          if(name1.compareTo(name2) < 0)
              return -1;
          
		return ret;
    }  
}