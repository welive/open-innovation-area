package it.eng.rspa.ideas.challenges;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;
import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;

import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portlet.asset.model.BaseAssetRenderer;

public class ChallengeAssetRenderer extends BaseAssetRenderer { 
	
	CLSChallenge challenge = null;
	
	public ChallengeAssetRenderer(CLSChallenge challenge){
		this.challenge = challenge;
	}
	
	// utilizzato dala portlet search per l'url associato alla gara
	@Override
	public String getURLViewInContext(
			LiferayPortletRequest liferayPortletRequest,
			LiferayPortletResponse liferayPortletResponse,
			String noSuchEntryRedirect) throws Exception {
				
		IdeaManagementSystemProperties imsp = new IdeaManagementSystemProperties();
		return imsp.getFriendlyUrlChallenges(challenge.getChallengeId());
		
		//return getURLViewInContext(liferayPortletRequest, noSuchEntryRedirect, "/html/challenges/view_challenge.jsp", "challengeId", challenge.getChallengeId());
	}
	
//	//questo dovrebbe essere quello utilizzato dall'asset publisher per il link nel titolo della risorsa
//	@Override
//	public String getUrlTitle() {		
////		IdeaManagementSystemProperties imsp = new IdeaManagementSystemProperties();
////		try {
////			return imsp.getFriendlyUrlChallenges(challenge.getChallengeId());
////		} catch (PortalException | SystemException | IOException
////				| PortletException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		}
////		return null;
//		return "piripicchio";
//	}
	
	@Override
	public String getClassName() {
		return CLSChallenge.class.getName();
	}

	@Override
	public long getClassPK() {
		return challenge.getPrimaryKey();
	}

	@Override
	public long getGroupId() {
		return challenge.getGroupId();
	}

	@Override
	public String getSummary(Locale locale) {
		String summary = challenge.getChallengeDescription();
		
		String regex ="\\<[^\\>]*\\>";
		String replacement = "";
		summary = summary.replaceAll(regex, replacement);
		
		if(summary!=null){
			summary = StringUtil.shorten(HtmlUtil.stripHtml(summary), 200);
		}else{
			summary="";
		}
		
		return summary;
	}
	
	@Override
	public String getTitle(Locale locale) {
		return challenge.getChallengeTitle();
	}

	@Override
	public long getUserId() {
		return challenge.getUserId();
	}

	@Override
	public String getUserName() {
		return null;
	}

	@Override
	public String getUuid() {
		return null;
	}

	@Override
	public String render(RenderRequest renderRequest,
			RenderResponse renderResponse, String template) throws Exception {
		
		renderRequest.setAttribute("challenge", this.challenge);
		//String page = "/html/challenges/abstract.jsp";
		
		// if (template.equals(TEMPLATE_FULL_CONTENT)) {
		String	 page = "/html/challengesexplorermaterial/challengesdetails/view.jsp";
			 			 
	//	 }
	   	 
		 return page;
	}
	
	@Override
	public String getIconPath(PortletRequest portletRequest) {
		// TODO Auto-generated method stub
		return "/Challenge62-portlet/img/icon/challengesImg.png";///super.getIconPath(portletRequest);
	}

}
