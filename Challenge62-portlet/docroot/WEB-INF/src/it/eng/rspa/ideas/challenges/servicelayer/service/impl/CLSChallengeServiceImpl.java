/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.impl;

import it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSChallengeServiceBaseImpl;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;




/**
 * The implementation of the c l s challenge remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSChallengeServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeServiceUtil
 */
public class CLSChallengeServiceImpl extends CLSChallengeServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeServiceUtil} to access the c l s challenge remote service.
	 */
	
	
	
	/**
	 * @param fileEntryId
	 * 
	 * questo metodo viene utilizzato per eliminare gli allegati con Ajax
	 */
	public void deleteChallengeFile(long fileEntryId)  {

			try {
				DLFileEntryLocalServiceUtil.deleteDLFileEntry(fileEntryId);
			} catch (PortalException | SystemException e) {
				
				e.printStackTrace();
			}


	}
	

}