package it.eng.rspa.ideas.utils;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.util.portlet.PortletProps;

public class DecisionEngine {

	private static final String DE_ADDRESS = IdeaManagementSystemProperties.getProperty("deAddress");
	
	
	/**
	 * @param idea
	 */
	public static void deNewUpdateIdeaNotifier (CLSIdea idea){
		
		
		//non invio notifiche per Need
		if (idea.getIsNeed())
			return;
		
		
		JSONObject jsonInvio =  JSONFactoryUtil.createJSONObject();
		
		//Languages supported: danish, dutch, english, finnish, french, german, hungarian, 
		//italian, norwegian, porter, portuguese, romanian, russian, spanish and swedish.
		
		
		String lingua = MyUtils.getFullLanguageNameByAcronym(idea.getLanguage());
		
		//ripulisco dai tag Html
		//http://stackoverflow.com/questions/4432560/remove-html-tags-from-string-using-java
	//	String textPulito = idea.getIdeaDescription().replaceAll("\\<.*?\\>", "");
		
		//rimangono solo i minore e maggiore &lt; &gt;
	//	String testoUnescaped = StringEscapeUtils.unescapeHtml4(textPulito);
		
		jsonInvio.put("lang",lingua );
		//jsonInvio.put("text", testoUnescaped);
		
//		JSONArray tags = IdeasListUtils.getTagsByIdeaId(idea.getIdeaID());
		
		
		JSONArray tags = IdeasListUtils.getThemeCategoriesTagsByIdeaIdAcronimLanguage(idea.getIdeaID(),idea.getLanguage() );
		
		jsonInvio.put("tags", tags);
		
		if (jsonInvio.length() >0){//potrebbe essere vuoto solo in caso di update
			
			
			if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
			
				System.out.println("DE jsonInvio "+jsonInvio);
			}
			try {
				RestUtils.consumePutWs(jsonInvio, DE_ADDRESS+"idea/"+idea.getIdeaID()); //put /de/idea/{ideaID
			} catch (Exception e) {
				
				e.printStackTrace();
			} 
		}
		
	}
	
	
	/**
	 * @param ideaId
	 */
	public static void deDeleteIdeaNotifier (CLSIdea idea){
		
		if (idea.getIsNeed())
			return;
		
		if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
			System.out.println("deDeleteIdeaNotifier "+idea.getIdeaID());
		}
			try {
				RestUtils.consumeDeleteWs( DE_ADDRESS+"idea/"+idea.getIdeaID());
			} catch (Exception e) {
				
				e.printStackTrace();
			} 
		
		
	}
	
	
	/**
	 * @param idea
	 */
	public static JSONArray deUserReccomenderNotifier (String categAjax, String tagAjax, String lingua ){
		
		
		JSONArray jsonArrayRitorno= JSONFactoryUtil.createJSONArray();
		
		
		JSONArray jsAC = null;
		JSONArray jsAT = null;
		try {
			 jsAC = JSONFactoryUtil.createJSONArray(categAjax);
			 jsAT = JSONFactoryUtil.createJSONArray(tagAjax);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		
		
		JSONObject jsonObjectInvio = JSONFactoryUtil.createJSONObject();
		
		
		String linguaCompletaLowerCase = MyUtils.getFullLanguageNameByAcronym(lingua);
		
		
		jsonObjectInvio.put("lang", linguaCompletaLowerCase);
		jsonObjectInvio.put("category_list", jsAC);
		jsonObjectInvio.put("tag_list", jsAT);
		
		if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
			System.out.println("jsonObjectInvio "+jsonObjectInvio);
		}
		
		
		String wsRes = null;
		try {
			wsRes = RestUtils.consumePostWsJSONFormat(jsonObjectInvio, DE_ADDRESS+"idea/recommend/users_by_skills"); 
			
			
			if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
				System.out.println("Risposta reccomender users "+wsRes);
			}
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return jsonArrayRitorno;
		}
		
		
////	Esempio risposta	
//		[
//		  {
//		    "matching_rate": 1,
//		    "user_id": 123,
//		    "matching_skill_list": [
//		      "liferay",
//		      "java"
//		    ]
//		  },
//		  {
//		    "matching_rate": 1,
//		    "user_id": 1002,
//		    "matching_skill_list": [
//		      "liferay",
//		      "java"
//		    ]
//		  },
//		  {
//		    "matching_rate": 0.5,
//		    "user_id": 89,
//		    "matching_skill_list": [
//		      "java"
//		    ]
//		  }
//		]
//
		// per test
		//wsRes = "	[{\"matching_rate\":1,\"user_id\":1005,\"matching_skill_list\":[\"liferay\",\"java\"]},{\"matching_rate\":2,\"user_id\":1002,\"matching_skill_list\":[\"liferay\",\"java\"]},{\"matching_rate\":0.5,\"user_id\":98765,\"matching_skill_list\":[\"java\"]}]";
		
		
		JSONArray rispostaJA = null;
		try {
			
			rispostaJA  =  JSONFactoryUtil.createJSONArray(wsRes);
			
		} catch (JSONException e) {
			
			e.printStackTrace();
		}
		
		
		
		
		HashMap<String,Double > mappa = new HashMap<String, Double>();
		for (int i =0; i< rispostaJA.length(); i++){
				JSONObject datiUtenti = rispostaJA.getJSONObject(i);
				
				String ccUserIdUtente = datiUtenti.getString("user_id");
				
				Long liferayIdUtente = MyUtils.getUserIdByCcUserId(ccUserIdUtente);
				
				String strLiferayIdUtente = Long.toString(liferayIdUtente);
						
				
				String matchingutente = datiUtenti.getString("matching_rate");
			    double matchingutenteInt = Double.parseDouble(matchingutente);
			    			    
				mappa.put(strLiferayIdUtente,matchingutenteInt );
		}
		
		
		
		HashMap<String,Double > sortedMap =  sortByValues (mappa);
		
		try {
	
			for (String key: sortedMap.keySet()) {
				
				JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
				jsonObject.put("id", key);
				jsonObject.put("matching",sortedMap.get(key));
				
				jsonArrayRitorno.put(key);
				   
			}
		}
		catch (Exception e) { e.printStackTrace(); }
		
		if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
			System.out.println("DE jsonArrayRitorno "+jsonArrayRitorno);
		}
		return jsonArrayRitorno;
 
		
		
	}
	
	
	
/**
 * @param map
 * @return
 */
@SuppressWarnings("unchecked")
private static HashMap<String, Double> sortByValues(HashMap<String, Double> map) { 
	
    List list = new LinkedList(map.entrySet());
    // Defined Custom Comparator here
    Collections.sort(list, new Comparator() {
         public int compare(Object o1, Object o2) {
            return ((Comparable) ((Map.Entry) (o1)).getValue()).compareTo(((Map.Entry) (o2)).getValue());
         }
    });
    
    // Here I am copying the sorted list in HashMap
    // using LinkedHashMap to preserve the insertion order
    HashMap<String, Double> sortedHashMap = new LinkedHashMap<String, Double>();
    for (Iterator it = list.iterator(); it.hasNext();) {
           Map.Entry<String, Double> entry = (Map.Entry<String, Double>) it.next();
           sortedHashMap.put(entry.getKey(), entry.getValue());
    } 
    return sortedHashMap;
}


/**
 * @param ideaId
 * @return
 */
public static List<CLSIdea> deGetIdeasVsIdeasRecommendations(long ideaId){
	
	//Prendo le idee suggerite dal Decision Engine tramite WS
	
	List<CLSIdea> listaRitorno = new ArrayList<CLSIdea>();
	
	if (!IdeaManagementSystemProperties.getEnabledProperty("deEnabled")  ){
		
		return listaRitorno;
		
	}
	
	
	String wsRes = null;
	try {
		
		wsRes = RestUtils.consumeGetWsAuthJsonFormat( DE_ADDRESS+"idea/"+ideaId+"/recommend/ideas"); 
		
		
		if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
			System.out.println("Suggested ideas WS response: "+wsRes);
		}
		
	} catch (Exception e) {
		
		e.printStackTrace();
		return listaRitorno;
	}
	
	
	//wsRes = "[22903, 22902]";
	
	JSONArray ideasJA = null;
	try {
		 ideasJA  =  JSONFactoryUtil.createJSONArray(wsRes);
	} catch (JSONException e) {
		
		e.printStackTrace();
		return listaRitorno;
	}
	
	
	for (int i =0; i< ideasJA.length(); i++){
		
			 try {
				CLSIdea idea = CLSIdeaLocalServiceUtil.getCLSIdea(ideasJA.getLong(i));
				
				listaRitorno.add(idea);
				
			} catch (PortalException | SystemException e) {
				
				System.out.println("Error on DE deGetIdeasVsIdeasRecommendations: "+ e.getMessage());
				
			}
		
	}
	
	return listaRitorno;
	
	
}









	
}
