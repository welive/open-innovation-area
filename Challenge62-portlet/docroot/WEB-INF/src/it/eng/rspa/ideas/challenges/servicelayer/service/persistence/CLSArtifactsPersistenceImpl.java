/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the c l s artifacts service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSArtifactsPersistence
 * @see CLSArtifactsUtil
 * @generated
 */
public class CLSArtifactsPersistenceImpl extends BasePersistenceImpl<CLSArtifacts>
	implements CLSArtifactsPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CLSArtifactsUtil} to access the c l s artifacts persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CLSArtifactsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CLSArtifactsModelImpl.ENTITY_CACHE_ENABLED,
			CLSArtifactsModelImpl.FINDER_CACHE_ENABLED, CLSArtifactsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CLSArtifactsModelImpl.ENTITY_CACHE_ENABLED,
			CLSArtifactsModelImpl.FINDER_CACHE_ENABLED, CLSArtifactsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CLSArtifactsModelImpl.ENTITY_CACHE_ENABLED,
			CLSArtifactsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ARTIFACTSBYIDEAIDEARTIFACTID =
		new FinderPath(CLSArtifactsModelImpl.ENTITY_CACHE_ENABLED,
			CLSArtifactsModelImpl.FINDER_CACHE_ENABLED, CLSArtifactsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByartifactsByIdeaIdEArtifactId",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTSBYIDEAIDEARTIFACTID =
		new FinderPath(CLSArtifactsModelImpl.ENTITY_CACHE_ENABLED,
			CLSArtifactsModelImpl.FINDER_CACHE_ENABLED, CLSArtifactsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByartifactsByIdeaIdEArtifactId",
			new String[] { Long.class.getName(), Long.class.getName() },
			CLSArtifactsModelImpl.IDEAID_COLUMN_BITMASK |
			CLSArtifactsModelImpl.ARTIFACTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ARTIFACTSBYIDEAIDEARTIFACTID =
		new FinderPath(CLSArtifactsModelImpl.ENTITY_CACHE_ENABLED,
			CLSArtifactsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByartifactsByIdeaIdEArtifactId",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns all the c l s artifactses where ideaId = &#63; and artifactId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param artifactId the artifact ID
	 * @return the matching c l s artifactses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSArtifacts> findByartifactsByIdeaIdEArtifactId(long ideaId,
		long artifactId) throws SystemException {
		return findByartifactsByIdeaIdEArtifactId(ideaId, artifactId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s artifactses where ideaId = &#63; and artifactId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param artifactId the artifact ID
	 * @param start the lower bound of the range of c l s artifactses
	 * @param end the upper bound of the range of c l s artifactses (not inclusive)
	 * @return the range of matching c l s artifactses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSArtifacts> findByartifactsByIdeaIdEArtifactId(long ideaId,
		long artifactId, int start, int end) throws SystemException {
		return findByartifactsByIdeaIdEArtifactId(ideaId, artifactId, start,
			end, null);
	}

	/**
	 * Returns an ordered range of all the c l s artifactses where ideaId = &#63; and artifactId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param artifactId the artifact ID
	 * @param start the lower bound of the range of c l s artifactses
	 * @param end the upper bound of the range of c l s artifactses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s artifactses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSArtifacts> findByartifactsByIdeaIdEArtifactId(long ideaId,
		long artifactId, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTSBYIDEAIDEARTIFACTID;
			finderArgs = new Object[] { ideaId, artifactId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ARTIFACTSBYIDEAIDEARTIFACTID;
			finderArgs = new Object[] {
					ideaId, artifactId,
					
					start, end, orderByComparator
				};
		}

		List<CLSArtifacts> list = (List<CLSArtifacts>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSArtifacts clsArtifacts : list) {
				if ((ideaId != clsArtifacts.getIdeaId()) ||
						(artifactId != clsArtifacts.getArtifactId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_CLSARTIFACTS_WHERE);

			query.append(_FINDER_COLUMN_ARTIFACTSBYIDEAIDEARTIFACTID_IDEAID_2);

			query.append(_FINDER_COLUMN_ARTIFACTSBYIDEAIDEARTIFACTID_ARTIFACTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSArtifactsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				qPos.add(artifactId);

				if (!pagination) {
					list = (List<CLSArtifacts>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSArtifacts>(list);
				}
				else {
					list = (List<CLSArtifacts>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s artifacts in the ordered set where ideaId = &#63; and artifactId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param artifactId the artifact ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s artifacts
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a matching c l s artifacts could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSArtifacts findByartifactsByIdeaIdEArtifactId_First(long ideaId,
		long artifactId, OrderByComparator orderByComparator)
		throws NoSuchCLSArtifactsException, SystemException {
		CLSArtifacts clsArtifacts = fetchByartifactsByIdeaIdEArtifactId_First(ideaId,
				artifactId, orderByComparator);

		if (clsArtifacts != null) {
			return clsArtifacts;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(", artifactId=");
		msg.append(artifactId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSArtifactsException(msg.toString());
	}

	/**
	 * Returns the first c l s artifacts in the ordered set where ideaId = &#63; and artifactId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param artifactId the artifact ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s artifacts, or <code>null</code> if a matching c l s artifacts could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSArtifacts fetchByartifactsByIdeaIdEArtifactId_First(long ideaId,
		long artifactId, OrderByComparator orderByComparator)
		throws SystemException {
		List<CLSArtifacts> list = findByartifactsByIdeaIdEArtifactId(ideaId,
				artifactId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s artifacts in the ordered set where ideaId = &#63; and artifactId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param artifactId the artifact ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s artifacts
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a matching c l s artifacts could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSArtifacts findByartifactsByIdeaIdEArtifactId_Last(long ideaId,
		long artifactId, OrderByComparator orderByComparator)
		throws NoSuchCLSArtifactsException, SystemException {
		CLSArtifacts clsArtifacts = fetchByartifactsByIdeaIdEArtifactId_Last(ideaId,
				artifactId, orderByComparator);

		if (clsArtifacts != null) {
			return clsArtifacts;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(", artifactId=");
		msg.append(artifactId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSArtifactsException(msg.toString());
	}

	/**
	 * Returns the last c l s artifacts in the ordered set where ideaId = &#63; and artifactId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param artifactId the artifact ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s artifacts, or <code>null</code> if a matching c l s artifacts could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSArtifacts fetchByartifactsByIdeaIdEArtifactId_Last(long ideaId,
		long artifactId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByartifactsByIdeaIdEArtifactId(ideaId, artifactId);

		if (count == 0) {
			return null;
		}

		List<CLSArtifacts> list = findByartifactsByIdeaIdEArtifactId(ideaId,
				artifactId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s artifactses before and after the current c l s artifacts in the ordered set where ideaId = &#63; and artifactId = &#63;.
	 *
	 * @param clsArtifactsPK the primary key of the current c l s artifacts
	 * @param ideaId the idea ID
	 * @param artifactId the artifact ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s artifacts
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a c l s artifacts with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSArtifacts[] findByartifactsByIdeaIdEArtifactId_PrevAndNext(
		CLSArtifactsPK clsArtifactsPK, long ideaId, long artifactId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSArtifactsException, SystemException {
		CLSArtifacts clsArtifacts = findByPrimaryKey(clsArtifactsPK);

		Session session = null;

		try {
			session = openSession();

			CLSArtifacts[] array = new CLSArtifactsImpl[3];

			array[0] = getByartifactsByIdeaIdEArtifactId_PrevAndNext(session,
					clsArtifacts, ideaId, artifactId, orderByComparator, true);

			array[1] = clsArtifacts;

			array[2] = getByartifactsByIdeaIdEArtifactId_PrevAndNext(session,
					clsArtifacts, ideaId, artifactId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSArtifacts getByartifactsByIdeaIdEArtifactId_PrevAndNext(
		Session session, CLSArtifacts clsArtifacts, long ideaId,
		long artifactId, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSARTIFACTS_WHERE);

		query.append(_FINDER_COLUMN_ARTIFACTSBYIDEAIDEARTIFACTID_IDEAID_2);

		query.append(_FINDER_COLUMN_ARTIFACTSBYIDEAIDEARTIFACTID_ARTIFACTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSArtifactsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(ideaId);

		qPos.add(artifactId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsArtifacts);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSArtifacts> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s artifactses where ideaId = &#63; and artifactId = &#63; from the database.
	 *
	 * @param ideaId the idea ID
	 * @param artifactId the artifact ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByartifactsByIdeaIdEArtifactId(long ideaId,
		long artifactId) throws SystemException {
		for (CLSArtifacts clsArtifacts : findByartifactsByIdeaIdEArtifactId(
				ideaId, artifactId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(clsArtifacts);
		}
	}

	/**
	 * Returns the number of c l s artifactses where ideaId = &#63; and artifactId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param artifactId the artifact ID
	 * @return the number of matching c l s artifactses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByartifactsByIdeaIdEArtifactId(long ideaId, long artifactId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ARTIFACTSBYIDEAIDEARTIFACTID;

		Object[] finderArgs = new Object[] { ideaId, artifactId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CLSARTIFACTS_WHERE);

			query.append(_FINDER_COLUMN_ARTIFACTSBYIDEAIDEARTIFACTID_IDEAID_2);

			query.append(_FINDER_COLUMN_ARTIFACTSBYIDEAIDEARTIFACTID_ARTIFACTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				qPos.add(artifactId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ARTIFACTSBYIDEAIDEARTIFACTID_IDEAID_2 =
		"clsArtifacts.id.ideaId = ? AND ";
	private static final String _FINDER_COLUMN_ARTIFACTSBYIDEAIDEARTIFACTID_ARTIFACTID_2 =
		"clsArtifacts.id.artifactId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ARTIFACTSBYIDEAID =
		new FinderPath(CLSArtifactsModelImpl.ENTITY_CACHE_ENABLED,
			CLSArtifactsModelImpl.FINDER_CACHE_ENABLED, CLSArtifactsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByartifactsByIdeaId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTSBYIDEAID =
		new FinderPath(CLSArtifactsModelImpl.ENTITY_CACHE_ENABLED,
			CLSArtifactsModelImpl.FINDER_CACHE_ENABLED, CLSArtifactsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByartifactsByIdeaId", new String[] { Long.class.getName() },
			CLSArtifactsModelImpl.IDEAID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ARTIFACTSBYIDEAID = new FinderPath(CLSArtifactsModelImpl.ENTITY_CACHE_ENABLED,
			CLSArtifactsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByartifactsByIdeaId", new String[] { Long.class.getName() });

	/**
	 * Returns all the c l s artifactses where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @return the matching c l s artifactses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSArtifacts> findByartifactsByIdeaId(long ideaId)
		throws SystemException {
		return findByartifactsByIdeaId(ideaId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s artifactses where ideaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param start the lower bound of the range of c l s artifactses
	 * @param end the upper bound of the range of c l s artifactses (not inclusive)
	 * @return the range of matching c l s artifactses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSArtifacts> findByartifactsByIdeaId(long ideaId, int start,
		int end) throws SystemException {
		return findByartifactsByIdeaId(ideaId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s artifactses where ideaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param start the lower bound of the range of c l s artifactses
	 * @param end the upper bound of the range of c l s artifactses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s artifactses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSArtifacts> findByartifactsByIdeaId(long ideaId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTSBYIDEAID;
			finderArgs = new Object[] { ideaId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ARTIFACTSBYIDEAID;
			finderArgs = new Object[] { ideaId, start, end, orderByComparator };
		}

		List<CLSArtifacts> list = (List<CLSArtifacts>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSArtifacts clsArtifacts : list) {
				if ((ideaId != clsArtifacts.getIdeaId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSARTIFACTS_WHERE);

			query.append(_FINDER_COLUMN_ARTIFACTSBYIDEAID_IDEAID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSArtifactsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				if (!pagination) {
					list = (List<CLSArtifacts>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSArtifacts>(list);
				}
				else {
					list = (List<CLSArtifacts>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s artifacts in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s artifacts
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a matching c l s artifacts could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSArtifacts findByartifactsByIdeaId_First(long ideaId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSArtifactsException, SystemException {
		CLSArtifacts clsArtifacts = fetchByartifactsByIdeaId_First(ideaId,
				orderByComparator);

		if (clsArtifacts != null) {
			return clsArtifacts;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSArtifactsException(msg.toString());
	}

	/**
	 * Returns the first c l s artifacts in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s artifacts, or <code>null</code> if a matching c l s artifacts could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSArtifacts fetchByartifactsByIdeaId_First(long ideaId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSArtifacts> list = findByartifactsByIdeaId(ideaId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s artifacts in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s artifacts
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a matching c l s artifacts could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSArtifacts findByartifactsByIdeaId_Last(long ideaId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSArtifactsException, SystemException {
		CLSArtifacts clsArtifacts = fetchByartifactsByIdeaId_Last(ideaId,
				orderByComparator);

		if (clsArtifacts != null) {
			return clsArtifacts;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSArtifactsException(msg.toString());
	}

	/**
	 * Returns the last c l s artifacts in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s artifacts, or <code>null</code> if a matching c l s artifacts could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSArtifacts fetchByartifactsByIdeaId_Last(long ideaId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByartifactsByIdeaId(ideaId);

		if (count == 0) {
			return null;
		}

		List<CLSArtifacts> list = findByartifactsByIdeaId(ideaId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s artifactses before and after the current c l s artifacts in the ordered set where ideaId = &#63;.
	 *
	 * @param clsArtifactsPK the primary key of the current c l s artifacts
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s artifacts
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a c l s artifacts with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSArtifacts[] findByartifactsByIdeaId_PrevAndNext(
		CLSArtifactsPK clsArtifactsPK, long ideaId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSArtifactsException, SystemException {
		CLSArtifacts clsArtifacts = findByPrimaryKey(clsArtifactsPK);

		Session session = null;

		try {
			session = openSession();

			CLSArtifacts[] array = new CLSArtifactsImpl[3];

			array[0] = getByartifactsByIdeaId_PrevAndNext(session,
					clsArtifacts, ideaId, orderByComparator, true);

			array[1] = clsArtifacts;

			array[2] = getByartifactsByIdeaId_PrevAndNext(session,
					clsArtifacts, ideaId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSArtifacts getByartifactsByIdeaId_PrevAndNext(Session session,
		CLSArtifacts clsArtifacts, long ideaId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSARTIFACTS_WHERE);

		query.append(_FINDER_COLUMN_ARTIFACTSBYIDEAID_IDEAID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSArtifactsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(ideaId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsArtifacts);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSArtifacts> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s artifactses where ideaId = &#63; from the database.
	 *
	 * @param ideaId the idea ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByartifactsByIdeaId(long ideaId)
		throws SystemException {
		for (CLSArtifacts clsArtifacts : findByartifactsByIdeaId(ideaId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(clsArtifacts);
		}
	}

	/**
	 * Returns the number of c l s artifactses where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @return the number of matching c l s artifactses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByartifactsByIdeaId(long ideaId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ARTIFACTSBYIDEAID;

		Object[] finderArgs = new Object[] { ideaId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSARTIFACTS_WHERE);

			query.append(_FINDER_COLUMN_ARTIFACTSBYIDEAID_IDEAID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ARTIFACTSBYIDEAID_IDEAID_2 = "clsArtifacts.id.ideaId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ARTIFACTSBYARTIFACTID =
		new FinderPath(CLSArtifactsModelImpl.ENTITY_CACHE_ENABLED,
			CLSArtifactsModelImpl.FINDER_CACHE_ENABLED, CLSArtifactsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByartifactsByArtifactId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTSBYARTIFACTID =
		new FinderPath(CLSArtifactsModelImpl.ENTITY_CACHE_ENABLED,
			CLSArtifactsModelImpl.FINDER_CACHE_ENABLED, CLSArtifactsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByartifactsByArtifactId",
			new String[] { Long.class.getName() },
			CLSArtifactsModelImpl.ARTIFACTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ARTIFACTSBYARTIFACTID = new FinderPath(CLSArtifactsModelImpl.ENTITY_CACHE_ENABLED,
			CLSArtifactsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByartifactsByArtifactId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the c l s artifactses where artifactId = &#63;.
	 *
	 * @param artifactId the artifact ID
	 * @return the matching c l s artifactses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSArtifacts> findByartifactsByArtifactId(long artifactId)
		throws SystemException {
		return findByartifactsByArtifactId(artifactId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s artifactses where artifactId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param artifactId the artifact ID
	 * @param start the lower bound of the range of c l s artifactses
	 * @param end the upper bound of the range of c l s artifactses (not inclusive)
	 * @return the range of matching c l s artifactses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSArtifacts> findByartifactsByArtifactId(long artifactId,
		int start, int end) throws SystemException {
		return findByartifactsByArtifactId(artifactId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s artifactses where artifactId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param artifactId the artifact ID
	 * @param start the lower bound of the range of c l s artifactses
	 * @param end the upper bound of the range of c l s artifactses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s artifactses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSArtifacts> findByartifactsByArtifactId(long artifactId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTSBYARTIFACTID;
			finderArgs = new Object[] { artifactId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ARTIFACTSBYARTIFACTID;
			finderArgs = new Object[] { artifactId, start, end, orderByComparator };
		}

		List<CLSArtifacts> list = (List<CLSArtifacts>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSArtifacts clsArtifacts : list) {
				if ((artifactId != clsArtifacts.getArtifactId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSARTIFACTS_WHERE);

			query.append(_FINDER_COLUMN_ARTIFACTSBYARTIFACTID_ARTIFACTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSArtifactsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(artifactId);

				if (!pagination) {
					list = (List<CLSArtifacts>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSArtifacts>(list);
				}
				else {
					list = (List<CLSArtifacts>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s artifacts in the ordered set where artifactId = &#63;.
	 *
	 * @param artifactId the artifact ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s artifacts
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a matching c l s artifacts could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSArtifacts findByartifactsByArtifactId_First(long artifactId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSArtifactsException, SystemException {
		CLSArtifacts clsArtifacts = fetchByartifactsByArtifactId_First(artifactId,
				orderByComparator);

		if (clsArtifacts != null) {
			return clsArtifacts;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("artifactId=");
		msg.append(artifactId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSArtifactsException(msg.toString());
	}

	/**
	 * Returns the first c l s artifacts in the ordered set where artifactId = &#63;.
	 *
	 * @param artifactId the artifact ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s artifacts, or <code>null</code> if a matching c l s artifacts could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSArtifacts fetchByartifactsByArtifactId_First(long artifactId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSArtifacts> list = findByartifactsByArtifactId(artifactId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s artifacts in the ordered set where artifactId = &#63;.
	 *
	 * @param artifactId the artifact ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s artifacts
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a matching c l s artifacts could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSArtifacts findByartifactsByArtifactId_Last(long artifactId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSArtifactsException, SystemException {
		CLSArtifacts clsArtifacts = fetchByartifactsByArtifactId_Last(artifactId,
				orderByComparator);

		if (clsArtifacts != null) {
			return clsArtifacts;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("artifactId=");
		msg.append(artifactId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSArtifactsException(msg.toString());
	}

	/**
	 * Returns the last c l s artifacts in the ordered set where artifactId = &#63;.
	 *
	 * @param artifactId the artifact ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s artifacts, or <code>null</code> if a matching c l s artifacts could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSArtifacts fetchByartifactsByArtifactId_Last(long artifactId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByartifactsByArtifactId(artifactId);

		if (count == 0) {
			return null;
		}

		List<CLSArtifacts> list = findByartifactsByArtifactId(artifactId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s artifactses before and after the current c l s artifacts in the ordered set where artifactId = &#63;.
	 *
	 * @param clsArtifactsPK the primary key of the current c l s artifacts
	 * @param artifactId the artifact ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s artifacts
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a c l s artifacts with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSArtifacts[] findByartifactsByArtifactId_PrevAndNext(
		CLSArtifactsPK clsArtifactsPK, long artifactId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSArtifactsException, SystemException {
		CLSArtifacts clsArtifacts = findByPrimaryKey(clsArtifactsPK);

		Session session = null;

		try {
			session = openSession();

			CLSArtifacts[] array = new CLSArtifactsImpl[3];

			array[0] = getByartifactsByArtifactId_PrevAndNext(session,
					clsArtifacts, artifactId, orderByComparator, true);

			array[1] = clsArtifacts;

			array[2] = getByartifactsByArtifactId_PrevAndNext(session,
					clsArtifacts, artifactId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSArtifacts getByartifactsByArtifactId_PrevAndNext(
		Session session, CLSArtifacts clsArtifacts, long artifactId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSARTIFACTS_WHERE);

		query.append(_FINDER_COLUMN_ARTIFACTSBYARTIFACTID_ARTIFACTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSArtifactsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(artifactId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsArtifacts);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSArtifacts> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s artifactses where artifactId = &#63; from the database.
	 *
	 * @param artifactId the artifact ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByartifactsByArtifactId(long artifactId)
		throws SystemException {
		for (CLSArtifacts clsArtifacts : findByartifactsByArtifactId(
				artifactId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(clsArtifacts);
		}
	}

	/**
	 * Returns the number of c l s artifactses where artifactId = &#63;.
	 *
	 * @param artifactId the artifact ID
	 * @return the number of matching c l s artifactses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByartifactsByArtifactId(long artifactId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ARTIFACTSBYARTIFACTID;

		Object[] finderArgs = new Object[] { artifactId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSARTIFACTS_WHERE);

			query.append(_FINDER_COLUMN_ARTIFACTSBYARTIFACTID_ARTIFACTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(artifactId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ARTIFACTSBYARTIFACTID_ARTIFACTID_2 =
		"clsArtifacts.id.artifactId = ?";

	public CLSArtifactsPersistenceImpl() {
		setModelClass(CLSArtifacts.class);
	}

	/**
	 * Caches the c l s artifacts in the entity cache if it is enabled.
	 *
	 * @param clsArtifacts the c l s artifacts
	 */
	@Override
	public void cacheResult(CLSArtifacts clsArtifacts) {
		EntityCacheUtil.putResult(CLSArtifactsModelImpl.ENTITY_CACHE_ENABLED,
			CLSArtifactsImpl.class, clsArtifacts.getPrimaryKey(), clsArtifacts);

		clsArtifacts.resetOriginalValues();
	}

	/**
	 * Caches the c l s artifactses in the entity cache if it is enabled.
	 *
	 * @param clsArtifactses the c l s artifactses
	 */
	@Override
	public void cacheResult(List<CLSArtifacts> clsArtifactses) {
		for (CLSArtifacts clsArtifacts : clsArtifactses) {
			if (EntityCacheUtil.getResult(
						CLSArtifactsModelImpl.ENTITY_CACHE_ENABLED,
						CLSArtifactsImpl.class, clsArtifacts.getPrimaryKey()) == null) {
				cacheResult(clsArtifacts);
			}
			else {
				clsArtifacts.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all c l s artifactses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CLSArtifactsImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CLSArtifactsImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the c l s artifacts.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CLSArtifacts clsArtifacts) {
		EntityCacheUtil.removeResult(CLSArtifactsModelImpl.ENTITY_CACHE_ENABLED,
			CLSArtifactsImpl.class, clsArtifacts.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<CLSArtifacts> clsArtifactses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CLSArtifacts clsArtifacts : clsArtifactses) {
			EntityCacheUtil.removeResult(CLSArtifactsModelImpl.ENTITY_CACHE_ENABLED,
				CLSArtifactsImpl.class, clsArtifacts.getPrimaryKey());
		}
	}

	/**
	 * Creates a new c l s artifacts with the primary key. Does not add the c l s artifacts to the database.
	 *
	 * @param clsArtifactsPK the primary key for the new c l s artifacts
	 * @return the new c l s artifacts
	 */
	@Override
	public CLSArtifacts create(CLSArtifactsPK clsArtifactsPK) {
		CLSArtifacts clsArtifacts = new CLSArtifactsImpl();

		clsArtifacts.setNew(true);
		clsArtifacts.setPrimaryKey(clsArtifactsPK);

		return clsArtifacts;
	}

	/**
	 * Removes the c l s artifacts with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param clsArtifactsPK the primary key of the c l s artifacts
	 * @return the c l s artifacts that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a c l s artifacts with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSArtifacts remove(CLSArtifactsPK clsArtifactsPK)
		throws NoSuchCLSArtifactsException, SystemException {
		return remove((Serializable)clsArtifactsPK);
	}

	/**
	 * Removes the c l s artifacts with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the c l s artifacts
	 * @return the c l s artifacts that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a c l s artifacts with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSArtifacts remove(Serializable primaryKey)
		throws NoSuchCLSArtifactsException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CLSArtifacts clsArtifacts = (CLSArtifacts)session.get(CLSArtifactsImpl.class,
					primaryKey);

			if (clsArtifacts == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCLSArtifactsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(clsArtifacts);
		}
		catch (NoSuchCLSArtifactsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CLSArtifacts removeImpl(CLSArtifacts clsArtifacts)
		throws SystemException {
		clsArtifacts = toUnwrappedModel(clsArtifacts);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(clsArtifacts)) {
				clsArtifacts = (CLSArtifacts)session.get(CLSArtifactsImpl.class,
						clsArtifacts.getPrimaryKeyObj());
			}

			if (clsArtifacts != null) {
				session.delete(clsArtifacts);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (clsArtifacts != null) {
			clearCache(clsArtifacts);
		}

		return clsArtifacts;
	}

	@Override
	public CLSArtifacts updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts clsArtifacts)
		throws SystemException {
		clsArtifacts = toUnwrappedModel(clsArtifacts);

		boolean isNew = clsArtifacts.isNew();

		CLSArtifactsModelImpl clsArtifactsModelImpl = (CLSArtifactsModelImpl)clsArtifacts;

		Session session = null;

		try {
			session = openSession();

			if (clsArtifacts.isNew()) {
				session.save(clsArtifacts);

				clsArtifacts.setNew(false);
			}
			else {
				session.merge(clsArtifacts);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CLSArtifactsModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((clsArtifactsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTSBYIDEAIDEARTIFACTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsArtifactsModelImpl.getOriginalIdeaId(),
						clsArtifactsModelImpl.getOriginalArtifactId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ARTIFACTSBYIDEAIDEARTIFACTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTSBYIDEAIDEARTIFACTID,
					args);

				args = new Object[] {
						clsArtifactsModelImpl.getIdeaId(),
						clsArtifactsModelImpl.getArtifactId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ARTIFACTSBYIDEAIDEARTIFACTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTSBYIDEAIDEARTIFACTID,
					args);
			}

			if ((clsArtifactsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTSBYIDEAID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsArtifactsModelImpl.getOriginalIdeaId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ARTIFACTSBYIDEAID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTSBYIDEAID,
					args);

				args = new Object[] { clsArtifactsModelImpl.getIdeaId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ARTIFACTSBYIDEAID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTSBYIDEAID,
					args);
			}

			if ((clsArtifactsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTSBYARTIFACTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsArtifactsModelImpl.getOriginalArtifactId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ARTIFACTSBYARTIFACTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTSBYARTIFACTID,
					args);

				args = new Object[] { clsArtifactsModelImpl.getArtifactId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ARTIFACTSBYARTIFACTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ARTIFACTSBYARTIFACTID,
					args);
			}
		}

		EntityCacheUtil.putResult(CLSArtifactsModelImpl.ENTITY_CACHE_ENABLED,
			CLSArtifactsImpl.class, clsArtifacts.getPrimaryKey(), clsArtifacts);

		return clsArtifacts;
	}

	protected CLSArtifacts toUnwrappedModel(CLSArtifacts clsArtifacts) {
		if (clsArtifacts instanceof CLSArtifactsImpl) {
			return clsArtifacts;
		}

		CLSArtifactsImpl clsArtifactsImpl = new CLSArtifactsImpl();

		clsArtifactsImpl.setNew(clsArtifacts.isNew());
		clsArtifactsImpl.setPrimaryKey(clsArtifacts.getPrimaryKey());

		clsArtifactsImpl.setIdeaId(clsArtifacts.getIdeaId());
		clsArtifactsImpl.setArtifactId(clsArtifacts.getArtifactId());

		return clsArtifactsImpl;
	}

	/**
	 * Returns the c l s artifacts with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s artifacts
	 * @return the c l s artifacts
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a c l s artifacts with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSArtifacts findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCLSArtifactsException, SystemException {
		CLSArtifacts clsArtifacts = fetchByPrimaryKey(primaryKey);

		if (clsArtifacts == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCLSArtifactsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return clsArtifacts;
	}

	/**
	 * Returns the c l s artifacts with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException} if it could not be found.
	 *
	 * @param clsArtifactsPK the primary key of the c l s artifacts
	 * @return the c l s artifacts
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a c l s artifacts with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSArtifacts findByPrimaryKey(CLSArtifactsPK clsArtifactsPK)
		throws NoSuchCLSArtifactsException, SystemException {
		return findByPrimaryKey((Serializable)clsArtifactsPK);
	}

	/**
	 * Returns the c l s artifacts with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s artifacts
	 * @return the c l s artifacts, or <code>null</code> if a c l s artifacts with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSArtifacts fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		CLSArtifacts clsArtifacts = (CLSArtifacts)EntityCacheUtil.getResult(CLSArtifactsModelImpl.ENTITY_CACHE_ENABLED,
				CLSArtifactsImpl.class, primaryKey);

		if (clsArtifacts == _nullCLSArtifacts) {
			return null;
		}

		if (clsArtifacts == null) {
			Session session = null;

			try {
				session = openSession();

				clsArtifacts = (CLSArtifacts)session.get(CLSArtifactsImpl.class,
						primaryKey);

				if (clsArtifacts != null) {
					cacheResult(clsArtifacts);
				}
				else {
					EntityCacheUtil.putResult(CLSArtifactsModelImpl.ENTITY_CACHE_ENABLED,
						CLSArtifactsImpl.class, primaryKey, _nullCLSArtifacts);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CLSArtifactsModelImpl.ENTITY_CACHE_ENABLED,
					CLSArtifactsImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return clsArtifacts;
	}

	/**
	 * Returns the c l s artifacts with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param clsArtifactsPK the primary key of the c l s artifacts
	 * @return the c l s artifacts, or <code>null</code> if a c l s artifacts with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSArtifacts fetchByPrimaryKey(CLSArtifactsPK clsArtifactsPK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)clsArtifactsPK);
	}

	/**
	 * Returns all the c l s artifactses.
	 *
	 * @return the c l s artifactses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSArtifacts> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s artifactses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s artifactses
	 * @param end the upper bound of the range of c l s artifactses (not inclusive)
	 * @return the range of c l s artifactses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSArtifacts> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s artifactses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s artifactses
	 * @param end the upper bound of the range of c l s artifactses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of c l s artifactses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSArtifacts> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CLSArtifacts> list = (List<CLSArtifacts>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CLSARTIFACTS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CLSARTIFACTS;

				if (pagination) {
					sql = sql.concat(CLSArtifactsModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CLSArtifacts>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSArtifacts>(list);
				}
				else {
					list = (List<CLSArtifacts>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the c l s artifactses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (CLSArtifacts clsArtifacts : findAll()) {
			remove(clsArtifacts);
		}
	}

	/**
	 * Returns the number of c l s artifactses.
	 *
	 * @return the number of c l s artifactses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CLSARTIFACTS);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the c l s artifacts persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CLSArtifacts>> listenersList = new ArrayList<ModelListener<CLSArtifacts>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CLSArtifacts>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CLSArtifactsImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CLSARTIFACTS = "SELECT clsArtifacts FROM CLSArtifacts clsArtifacts";
	private static final String _SQL_SELECT_CLSARTIFACTS_WHERE = "SELECT clsArtifacts FROM CLSArtifacts clsArtifacts WHERE ";
	private static final String _SQL_COUNT_CLSARTIFACTS = "SELECT COUNT(clsArtifacts) FROM CLSArtifacts clsArtifacts";
	private static final String _SQL_COUNT_CLSARTIFACTS_WHERE = "SELECT COUNT(clsArtifacts) FROM CLSArtifacts clsArtifacts WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "clsArtifacts.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CLSArtifacts exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CLSArtifacts exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CLSArtifactsPersistenceImpl.class);
	private static CLSArtifacts _nullCLSArtifacts = new CLSArtifactsImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CLSArtifacts> toCacheModel() {
				return _nullCLSArtifactsCacheModel;
			}
		};

	private static CacheModel<CLSArtifacts> _nullCLSArtifactsCacheModel = new CacheModel<CLSArtifacts>() {
			@Override
			public CLSArtifacts toEntityModel() {
				return _nullCLSArtifacts;
			}
		};
}