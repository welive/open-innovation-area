package it.eng.rspa.ideas.utils;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;

import java.util.Comparator;

public class ChallengeComparator implements Comparator<CLSChallenge> {

	@Override
	public int compare(CLSChallenge gara1, CLSChallenge gara2) {

		int ret=gara2.getDateAdded().compareTo(gara1.getDateAdded()); //mette le piu nuove in testa
		
		return ret;
		
	}

}
