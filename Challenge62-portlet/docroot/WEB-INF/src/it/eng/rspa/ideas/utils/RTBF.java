package it.eng.rspa.ideas.utils;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCoworkerLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteChallengesLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteIdeasLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSRequisitiLocalServiceUtil;
import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;
import it.eng.rspa.ideas.ideas.Ideas;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.portlet.ActionRequest;
import javax.portlet.PortletException;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserGroupLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.tasks.model.TasksEntry;
import com.liferay.tasks.service.TasksEntryLocalServiceUtil;

public class RTBF {
	
	
	/** Implement the Right to be forgotten on OIA
	 * @param liferayUserId
	 * @param deleteAllContent
	 * @param actionRequest
	 * @return
	 */
	public static JSONObject doRightToBeForgottenIMS(long liferayUserId, boolean deleteAllContent, ActionRequest actionRequest){
		
		long defaultDeletedUserId = getDefaultDeleteUserIdByUserIdToDelete(liferayUserId);
		long newUserId =defaultDeletedUserId;
		
		boolean allOk = true;
		JSONArray errors = JSONFactoryUtil.createJSONArray();
		
		
		
		//
		//	REMOVE ALL COLLABORATIONS
		//
		if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
					System.out.println("REMOVE ALL COLLABORATIONS");
		}
		
		List<CLSCoworker> allcowks = new ArrayList<CLSCoworker>();
		try {
			 allcowks = CLSCoworkerLocalServiceUtil.getCoworkersByUserId(liferayUserId);
		} catch (SystemException e1) {
			
			e1.printStackTrace();
			allOk = false;
			JSONObject erMsg = JSONFactoryUtil.createJSONObject();
			erMsg.put("message", e1.getMessage());
			errors.put(erMsg);
		}
		
		
		for (CLSCoworker allcowk:allcowks){
			
			//get idea of this coworker
			CLSIdea ideaCwk=null;
			try {
				ideaCwk = CLSIdeaLocalServiceUtil.getCLSIdea(allcowk.getIdeaID());
			} catch (PortalException | SystemException e1) {
				
				e1.printStackTrace();
				allOk = false;
				JSONObject erMsg = JSONFactoryUtil.createJSONObject();
				erMsg.put("message", e1.getMessage());
				errors.put(erMsg);
			}
			
			//	Remove the Coworker from the chatgroup
			try {
				UserGroupLocalServiceUtil.deleteUserUserGroup(liferayUserId, ideaCwk.getChatGroup());
			} catch (SystemException e1) {
				
				e1.printStackTrace();
				allOk = false;
				JSONObject erMsg = JSONFactoryUtil.createJSONObject();
				erMsg.put("message", e1.getMessage());
				errors.put(erMsg);
			}
			
			
			//	Delete the Coworker 
			try {
				CLSCoworkerLocalServiceUtil.deleteCLSCoworker(allcowk);
			} catch (SystemException e) {
				
				e.printStackTrace();
				allOk = false;
				JSONObject erMsg = JSONFactoryUtil.createJSONObject();
				erMsg.put("message", e.getMessage());
				errors.put(erMsg);
			}
			
		}
		
		
		if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
			System.out.println("TASKs assignee to the user");
		}
		
		//TASKs assignee to the user
		List<TasksEntry> tasks = new ArrayList<TasksEntry>();
		try {
			tasks = TasksEntryLocalServiceUtil.getAssigneeTasksEntries(liferayUserId, 0, TasksEntryLocalServiceUtil.getAssigneeTasksEntriesCount(liferayUserId));
		} catch (SystemException e2) {
			
			e2.printStackTrace();
			allOk = false;
			JSONObject erMsg = JSONFactoryUtil.createJSONObject();
			erMsg.put("message", e2.getMessage());
			errors.put(erMsg);
		}
		

		for (TasksEntry task:tasks){
			
			if (task.getResolverUserId() <1){//it is unresolved
				
				//delete unresolved tasks assig
				try {
					TasksEntryLocalServiceUtil.deleteTasksEntry(task);
				} catch (PortalException | SystemException e) {
					
					e.printStackTrace();
					allOk = false;
					JSONObject erMsg = JSONFactoryUtil.createJSONObject();
					erMsg.put("message", e.getMessage());
					errors.put(erMsg);
				}
			}else{//Resolved tasks assigned to defaultDeleteUser
				
				task.setAssigneeUserId(defaultDeletedUserId);
				task.setResolverUserId(defaultDeletedUserId);
				
				try {
					TasksEntryLocalServiceUtil.updateTasksEntry(task);
				} catch (SystemException e) {
					e.printStackTrace();
					allOk = false;
					JSONObject erMsg = JSONFactoryUtil.createJSONObject();
					erMsg.put("message", e.getMessage());
					errors.put(erMsg);
				}
			}
		}

		if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
			System.out.println("TASKs created by the user");
		}
		
		//TASKs created by the user
		List<TasksEntry> tasksUser = new ArrayList<TasksEntry>();
		try {
			tasksUser = TasksEntryLocalServiceUtil.getUserTasksEntries(liferayUserId, 0, TasksEntryLocalServiceUtil.getUserTasksEntriesCount(liferayUserId));
		} catch (SystemException e2) {
			
			e2.printStackTrace();
			allOk = false;
			JSONObject erMsg = JSONFactoryUtil.createJSONObject();
			erMsg.put("message", e2.getMessage());
			errors.put(erMsg);
		}
		
		for (TasksEntry taskUser:tasksUser){
			
			if (deleteAllContent  ||  (taskUser.getResolverUserId()==0 )){//delete all created tasks and unresolved
				
				try {
					TasksEntryLocalServiceUtil.deleteTasksEntry(taskUser);
				} catch (PortalException | SystemException e) {
					e.printStackTrace();
					allOk = false;
					JSONObject erMsg = JSONFactoryUtil.createJSONObject();
					erMsg.put("message", e.getMessage());
					errors.put(erMsg);
				}
			}else{//assign to deleteUser
				
				taskUser.setUserId(defaultDeletedUserId);
				taskUser.setResolverUserId(defaultDeletedUserId);
				
				try {
					TasksEntryLocalServiceUtil.updateTasksEntry(taskUser);
				} catch (SystemException e) {
					
					e.printStackTrace();
					allOk = false;
					JSONObject erMsg = JSONFactoryUtil.createJSONObject();
					erMsg.put("message", e.getMessage());
					errors.put(erMsg);
				}
			}
		}
		
		
		if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
			System.out.println("REMOVE ALL FAVOURITEs");
		}
		
		//
		//	REMOVE ALL FAVOURITEs
		//
		//favourite ideas
		List<CLSFavouriteIdeas> iFavs = new ArrayList<CLSFavouriteIdeas>();
		try {
			iFavs = CLSFavouriteIdeasLocalServiceUtil.getFavouriteIdeasEntriesByUserId(liferayUserId);
		} catch (SystemException e1) {
			e1.printStackTrace();
			allOk = false;
			JSONObject erMsg = JSONFactoryUtil.createJSONObject();
			erMsg.put("message", e1.getMessage());
			errors.put(erMsg);
		}
		
		for (CLSFavouriteIdeas iFav:iFavs){
			try {
				CLSFavouriteIdeasLocalServiceUtil.deleteCLSFavouriteIdeas(iFav);
			} catch (SystemException e) {
				
				e.printStackTrace();
				allOk = false;
				JSONObject erMsg = JSONFactoryUtil.createJSONObject();
				erMsg.put("message", e.getMessage());
				errors.put(erMsg);
			}
			
		}
		
		List<CLSFavouriteChallenges> cFavs = new ArrayList<CLSFavouriteChallenges>();
		//favourite challenges
		try {
			CLSFavouriteChallengesLocalServiceUtil.getFavouriteChallengesEntriesByUserId(liferayUserId);
		} catch (SystemException e1) {
			
			e1.printStackTrace();
			allOk = false;
			JSONObject erMsg = JSONFactoryUtil.createJSONObject();
			erMsg.put("message", e1.getMessage());
			errors.put(erMsg);
		}

		for (CLSFavouriteChallenges cFav:cFavs){
			try {
				CLSFavouriteChallengesLocalServiceUtil.deleteCLSFavouriteChallenges(cFav);
			} catch (SystemException e) {
				
				e.printStackTrace();
				allOk = false;
				JSONObject erMsg = JSONFactoryUtil.createJSONObject();
				erMsg.put("message", e.getMessage());
				errors.put(erMsg);
			}
			
		}
		
		if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
			System.out.println("UPDATE or DELETE all IDEAs");
		}
		
		
		//
		//	UPDATE or DELETE all IDEAs
		//
		
		List<CLSIdea> ideas = new ArrayList<CLSIdea>();
		try {
			ideas = CLSIdeaLocalServiceUtil.getIdeasByUserId(liferayUserId);
		} catch (SystemException e) {
			
			e.printStackTrace();
			allOk = false;
			JSONObject erMsg = JSONFactoryUtil.createJSONObject();
			erMsg.put("message", e.getMessage());
			errors.put(erMsg);
		}
	
		
		for (CLSIdea idea : ideas ){
			
			if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
				System.out.println("retrieve the chatgroup and remove the userRTBF");
			}
			
			//retrieve the chatgroup and remove the userRTBF
			try {
				UserGroupLocalServiceUtil.deleteUserUserGroup(liferayUserId, idea.getChatGroup());
			} catch (SystemException e) {
				e.printStackTrace();
				allOk = false;
				JSONObject erMsg = JSONFactoryUtil.createJSONObject();
				erMsg.put("message", e.getMessage());
				errors.put(erMsg);
			}
			
			if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
				System.out.println("Coworker of this idea");
			}
			
			//Coworker of this idea
			List<CLSCoworker> cowks = new ArrayList<CLSCoworker>();
			try {
				cowks = CLSCoworkerLocalServiceUtil.getCoworkersByIdeaId(idea.getIdeaID());
			} catch (SystemException e) {
				e.printStackTrace();
				allOk = false;
				JSONObject erMsg = JSONFactoryUtil.createJSONObject();
				erMsg.put("message", e.getMessage());
				errors.put(erMsg);
			}
				
				 
				
				if (cowks.size() > 0){ //assign to the oldest coworker
					
					CLSCoworker cowk = cowks.get(0);
					newUserId = cowk.getUserId();
					idea.setUserId(cowk.getUserId());
					
					//Delete as Coworker 
					try {
						CLSCoworkerLocalServiceUtil.deleteCLSCoworker(cowk);
					} catch (SystemException e) {
						
						e.printStackTrace();
						allOk = false;
						JSONObject erMsg = JSONFactoryUtil.createJSONObject();
						erMsg.put("message", e.getMessage());
						errors.put(erMsg);
					}
					
					try {
						CLSIdeaLocalServiceUtil.updateCLSIdea(idea);
					} catch (SystemException e) {
						e.printStackTrace();
						allOk = false;
						JSONObject erMsg = JSONFactoryUtil.createJSONObject();
						erMsg.put("message", e.getMessage());
						errors.put(erMsg);
					}
					
					
					//////Coworker Notification
					ResourceBundle res = MyUtils.getResBundleViaPilotByAction(actionRequest);
					
					String promoteToAuthor = "The author of an idea which cooperate deleted his/her account from the platform, so you have been promoted as author of the idea";
					try {
						promoteToAuthor = new String(res.getString("ims.promoted-to-author") .getBytes("ISO-8859-1"), "UTF-8");
					} catch (UnsupportedEncodingException e) {
						
						e.printStackTrace();
						allOk = false;
						JSONObject erMsg = JSONFactoryUtil.createJSONObject();
						erMsg.put("message", e.getMessage());
						errors.put(erMsg);
					}
					
					String textMessage = promoteToAuthor +" '"+ idea.getIdeaTitle()+"' ";
					String senderName = IdeaManagementSystemProperties.getProperty("senderNotificheMailIdeario");
					
					try {
						CLSIdeaLocalServiceUtil.sendNotification(res,textMessage, newUserId, senderName, idea.getIdeaID(), actionRequest,  "promoteToAuthor");
					} catch (PortalException | SystemException e) {
						
						e.printStackTrace();
						allOk = false;
						JSONObject erMsg = JSONFactoryUtil.createJSONObject();
						erMsg.put("message", e.getMessage());
						errors.put(erMsg);
					}
					
					
					
				}else{//idea without any coworkers 
					
					
					if (deleteAllContent){//if the user chose to remove all his contents
						
						
						//delete the idea
						Ideas ideaObj = new Ideas();
						try {
							ideaObj.deleteIdea(actionRequest,idea.getIdeaID() );
						} catch (IOException | PortletException e) {
							
							e.printStackTrace();
							allOk = false;
							JSONObject erMsg = JSONFactoryUtil.createJSONObject();
							erMsg.put("message", e.getMessage());
							errors.put(erMsg);
						}
						
						
						
					}else{//assign to deleteUser
						
						idea.setUserId(defaultDeletedUserId);
						
						try {
							CLSIdeaLocalServiceUtil.updateCLSIdea(idea);
						} catch (SystemException e) {
							e.printStackTrace();
							allOk = false;
							JSONObject erMsg = JSONFactoryUtil.createJSONObject();
							erMsg.put("message", e.getMessage());
							errors.put(erMsg);
						}
						
						
					}//if (deleteAllContent
					
				}//if (cowks.size() > 0)
				
				
				
				if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
					System.out.println("REQUIREMENTs");
				}
				//REQUIREMENTs				
				List<CLSRequisiti> reqs= new ArrayList<CLSRequisiti>();
				try {
					reqs = CLSRequisitiLocalServiceUtil.getRequirementByUserIdAndIdeaId(liferayUserId, idea.getIdeaID());
				} catch (SystemException e) {
					e.printStackTrace();
					allOk = false;
					JSONObject erMsg = JSONFactoryUtil.createJSONObject();
					erMsg.put("message", e.getMessage());
					errors.put(erMsg);
				}
				
				for (CLSRequisiti req:reqs){
					
					req.setAuthorUser(newUserId);//assign to coworker or defaultDeletedUser
					
					try {
						CLSRequisitiLocalServiceUtil.updateCLSRequisiti(req);
					} catch (SystemException e) {
						e.printStackTrace();
						allOk = false;
						JSONObject erMsg = JSONFactoryUtil.createJSONObject();
						erMsg.put("message", e.getMessage());
						errors.put(erMsg);
					}
				}
				
		}
				
		JSONObject oiaResp = JSONFactoryUtil.createJSONObject();
		oiaResp.put("success", allOk);
		oiaResp.put("errors", errors);
		
		
		return oiaResp;
	}
	
	
	/**
	 * @param liferayUserId
	 * @return
	 */
	private static long getDefaultDeleteUserIdByUserIdToDelete (long liferayUserId){
		
		
		String pilotUserToDelete = MyUtils.getPilotByUserId(liferayUserId);
		
		
		long companyId = MyUtils.getDefaultCompanyId();
		
		
		
	    Role role=null;
		try {
			role = RoleLocalServiceUtil.getRole(companyId, MyConstants.ROLE_DELETED_USER);
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
		}
		
		//an user foreach pilot
		List<User> users = new ArrayList<User>();
		try {
			 users = UserLocalServiceUtil.getRoleUsers(role.getRoleId());
		} catch (SystemException e) {
			
			e.printStackTrace();
		}
		
		//search the user for the current Pilot
		for (User user:users){
			
			String userPilot = MyUtils.getPilotByUserId(liferayUserId);
			
			if (userPilot.equalsIgnoreCase(pilotUserToDelete) ){
				
				return user.getUserId();
			}
		}
		
		return 0;//default- It should never happen
		
	}
	

}
