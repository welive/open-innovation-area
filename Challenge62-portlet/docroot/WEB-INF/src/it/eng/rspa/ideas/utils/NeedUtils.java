package it.eng.rspa.ideas.utils;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.NeedLinkedChallengeLocalServiceUtil;
import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portlet.asset.model.AssetVocabulary;
import com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil;

public class NeedUtils {

	
	/**
	 * @param needId
	 * @return
	 */
	public static boolean isNeedLinkedToChallenge (long needId){
		
		boolean isLinked = false;
		
		List<CLSChallenge> gare = new ArrayList<CLSChallenge>();
		
		try {
			gare = NeedLinkedChallengeLocalServiceUtil.getChallengeByNeedId(needId);
			
		} catch (SystemException e) {
			
			e.printStackTrace();
			return isLinked;
		}
		
		if (gare.size() > 0)
			isLinked= true;
		
		
		return isLinked;
		
	}
	
	
	/**
	 * @param needId
	 * @return
	 */
	public static boolean isNeedLinkedToIdea (long needId){
		
		boolean isLinked = false;
		
		List<CLSIdea> idee = new ArrayList<CLSIdea>();
		
		try {
			idee = CLSIdeaLocalServiceUtil.getIdeasByNeedId(needId);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return isLinked;
		}
		

		
		if (idee.size() > 0)
			isLinked= true;
		
		
		return isLinked;
		
	}
	
	
	
	/**
	 * @param needId
	 * @return
	 */
	public static boolean isNeedAssigned(long needId){
		
		
		boolean isNeedLinkedToChallenge = isNeedLinkedToChallenge(needId);
		
		
		return isNeedLinkedToChallenge;
		
		
		
	}
	
	
	
	/**
	 * @param ideaId
	 * @param locale
	 * @return
	 */
	public static String getVocabularyNameByNeedId(long ideaId, String locale){
		
		String nomeVocabolario = "";
		
		long vocId = IdeasListUtils.getVocabularyIdByIdeaId(ideaId);

		
		if (vocId>-1){
		
			AssetVocabulary voc = null; 
			
			try {
				 voc = AssetVocabularyLocalServiceUtil.getVocabulary(vocId);
			} catch (PortalException | SystemException e) {
				
				e.printStackTrace();
				return null;
			}
			
			nomeVocabolario = voc.getTitle(locale);
		}
		
	
	return nomeVocabolario;

}
	
	
	
	/**
	 * @param currentUser
	 * @return
	 */
	public static long getMunicipalityOrganizationIdByUser(User currentUser){
	
		long municipalityOrganizationId = 0;
		
		if (Validator.isNull(currentUser))
			return municipalityOrganizationId;
		
		
		List<Organization> municipalitaI = UpdateIdeaUtils.getAllOrganizationMunicipalities();
		List<Organization> municipalita = MyUtils.getListaOrganizationsconPilotUgualeByUser(currentUser, municipalitaI); //filtro su pilo
		
		if (municipalita.size() >0){
			
			municipalityOrganizationId = municipalita.get(0).getOrganizationId(); //TODO not only the first
			
		}
		
		return municipalityOrganizationId;
		
	}
	
		
	
	
	/**
	 * I can delete a need only if it is linked only with Ideas with challenge
	 * @param needId
	 * @return
	 */
	public static boolean isNeedWhichCanDelete (long needId){
		
		boolean blocked = false;
		
		
		List<CLSIdea> ideasForThisNeed = new ArrayList<CLSIdea>();
		
		try {
			ideasForThisNeed = CLSIdeaLocalServiceUtil.getIdeasByNeedId(needId);
		} catch (SystemException e) {
			
			e.printStackTrace();
		}
		
		for (CLSIdea idea : ideasForThisNeed){
			
			if (idea.getChallengeId() <=0){
				blocked = true;
				break;
			}
		}
		
		return !blocked;
		
	}
	
	
	
	/**
	 * @param needId
	 * @return
	 */
	public static boolean isNeedWhichCanProposeIdeas(long needId, User currentUser){
		
		
		if (!MyUtils.isIMSSimpleUser(currentUser))
			return false;
		
		
		return true;
		
		
		
	}
	
	
	/**
	 * Check if the user has need, so he/she can create ideas.
	 * 
	 * @param user
	 * @return
	 */
	public static boolean areThereNeedByPilot(RenderRequest renderRequest){
		
		List<CLSIdea> allNeeds= new ArrayList<CLSIdea>();
		try {
			allNeeds = CLSIdeaLocalServiceUtil.getNeeds();
		} catch (SystemException e) {
			
			e.printStackTrace();
		}
		String pilot = IdeasListUtils.getPilot(renderRequest);
		
		List<CLSIdea> filteredNeeds = IdeasListUtils.getIdeasFilteredByPilot(allNeeds, pilot);
		
		
		if (filteredNeeds.size() > 0)
			return true;
		
		
		return false;
	}
	
	
	
	/**
	 * redirect to the rigth page
	 * @param actionRequest
	 * @param actionResponse
	 * @param ideaId
	 */
	public static void redirectToNeed(ActionRequest actionRequest,	ActionResponse actionResponse, long ideaId){

		 String totalPath = IdeaManagementSystemProperties.getFriendlyUrlNeeds(ideaId);
		 
		try {
			actionResponse.sendRedirect(totalPath);
		} catch (IOException e) {
			e.printStackTrace();
		} //go to the page that you put as second parameter
	}
	
	

	/**
	 * @param pilot
	 * @param isNeed
	 * @param maxNumber
	 * @param language (en,es, it, sr, fi)
	 * @return
	 */
	public static JSONArray getTopRatedIdeasOrNeedsByPilotOrLanguage (String pilot, boolean isNeed, int maxNumber, String language, Locale locale){
		
		JSONArray needsOrIdeasJ =  JSONFactoryUtil.createJSONArray();
		
		List<CLSIdea> needsOrIdeas = new ArrayList<CLSIdea>();
		
		
		if (isNeed){
			
			try {
				needsOrIdeas = CLSIdeaLocalServiceUtil.getNeeds();
			} catch (SystemException e) {
				
				e.printStackTrace();
				return needsOrIdeasJ;
			}
		}else{
			
			try {
				needsOrIdeas = CLSIdeaLocalServiceUtil.getOnlyIdeas();
			} catch (SystemException e) {
				
				e.printStackTrace();
				return needsOrIdeasJ;
			}
			
		}
		
		if (!pilot.equalsIgnoreCase("")){
			//Filter by Pilot
			needsOrIdeas=IdeasListUtils.getIdeasListFilterByPilot(needsOrIdeas, pilot);
		}else{
			
			needsOrIdeas=IdeasListUtils.getIdeasListFilterByLanguage(needsOrIdeas, language);
			
		}
		
		
		
		if (needsOrIdeas.size() >0){
		
			Collections.reverse (needsOrIdeas);//before the newest
				
			//I show before the top rated
			IdeaComparatorByRating ic = new IdeaComparatorByRating();
			needsOrIdeas=ListUtil.sort(needsOrIdeas, ic);
		
		}
		
		
		
		for (int i=0; i< needsOrIdeas.size(); i++ ){
			
			if (i >= maxNumber )
				break;
			
			CLSIdea needOrIdea = needsOrIdeas.get(i);
			
			JSONObject needOrIdeaJ = jsonifyIdeaOrNeed(needOrIdea,  locale);
			
			
			needsOrIdeasJ.put(needOrIdeaJ);
		
		}
		
		return needsOrIdeasJ;
	}
	
	
	/**
	 * @param userid
	 * @return
	 */
	public static List<CLSIdea> getNeedsOfAuthorityIdAndNotAssignedByUserId(long userid) {
		
		long orgId = MyUtils.getMunicipalityOrganizationIdByUserId(userid);
		
		List<CLSIdea> needs = new ArrayList<CLSIdea>();
		List<CLSIdea> needsF = new ArrayList<CLSIdea>();
		
		try {
			needs = CLSIdeaLocalServiceUtil.getNeedsByMunicipalityOrganizationId(orgId);
			
			for (CLSIdea need: needs){
				
				if ( 
					(need.getMunicipalityId() == userid) ||
						(need.getMunicipalityId() == 0)
				   )
					needsF.add(need);
			}
			
			
		} catch (SystemException e) {
			
			e.printStackTrace();
			return needs;
		}
		
		
		
		return needs;
	}
	
	
	
	
	/**
	 * @param liferayUserId
	 * @param isNeed
	 * @param maxNumber
	 * @return
	 */
	public static JSONArray getIdeasOrNeedsByUserId (long liferayUserId, boolean isNeed, int maxNumber, Locale locale){
		
		JSONArray needsOrIdeasJ =  JSONFactoryUtil.createJSONArray();
		
		List<CLSIdea> needsOrIdeas = new ArrayList<CLSIdea>();
		
		boolean isAuthorityOrCompanyLeader = MyUtils.isAuthorityOrCompanyLeaderByUserId(liferayUserId);
		
		
		if (isAuthorityOrCompanyLeader){ 
			
			if (MyUtils.isAuthorityByUserId(liferayUserId)){ //MUNICIPALITY
				
				if (isNeed){
					needsOrIdeas=getNeedsOfAuthorityIdAndNotAssignedByUserId(liferayUserId);
				}else{
					needsOrIdeas=IdeasListUtils.getIdeasOfAuthorityIdAndNotAssignedByUserId(liferayUserId);
				}
				
				
			}else if (MyUtils.isCompanyLeaderByUserId(liferayUserId)){ //COMPANY LEADER
				
				if (isNeed){
					
					try {
						needsOrIdeas=CLSIdeaLocalServiceUtil.getNeedsByMunicipalityId(liferayUserId);
					} catch (SystemException e) {
						
						e.printStackTrace();
					}
					
				}else{
					
					try {
						needsOrIdeas=CLSIdeaLocalServiceUtil.getOnlyIdeasByMunicipalityId(liferayUserId);
					} catch (SystemException e) {
						
						e.printStackTrace();
					}
				}
			}
			
			
			
		}else{ //  !isAuthorityOrCompanyLeader
			if (isNeed){
				
				try {
					needsOrIdeas = CLSIdeaLocalServiceUtil.getNeedsByUserId(liferayUserId);
				} catch (SystemException e) {
					
					e.printStackTrace();
					return needsOrIdeasJ;
				}
			}else{
				needsOrIdeas = IdeasListUtils.getOnlyMyIdeasPlusCollaborationByUserId(liferayUserId);
			}
		}
		Collections.reverse (needsOrIdeas);//before the newest
		
		
		
		
		//make JSON
		for (int i=0; i< needsOrIdeas.size(); i++ ){
			
			if (i >= maxNumber )
				break;
			
			CLSIdea needOrIdea = needsOrIdeas.get(i);
			
			JSONObject needOrIdeaJ = jsonifyIdeaOrNeed(needOrIdea, locale);
			needsOrIdeasJ.put(needOrIdeaJ);
		}

		
		//If the user has less than maxNumber ideas, then I concat getTopRatedIdeasOrNeedsByPilotOrLanguage
		
		int elemSize =needsOrIdeas.size();
		
		if (elemSize <maxNumber){
			
			String userPilot = MyUtils.getPilotByUserId(liferayUserId);
			JSONArray arrTopRated = getTopRatedIdeasOrNeedsByPilotOrLanguage (userPilot, isNeed,maxNumber, "", locale);
			
			for ( int i = 0; i< arrTopRated.length(); i++){
				
				if (elemSize <maxNumber){
					
					JSONObject row = arrTopRated.getJSONObject(i);
					
					
					boolean exist = existJsonObjectInJsonArray(row,needsOrIdeasJ );//to avoid duplicate elements
					
					if (!exist){
						needsOrIdeasJ.put(row);
						elemSize++;
					}
					
				}
				
			}
			
		}
		
		
		return needsOrIdeasJ;
		
	}
	
	
	
	
	
	/**
	 * @param needOrIdea
	 * @return
	 */
	private static JSONObject jsonifyIdeaOrNeed (CLSIdea needOrIdea, Locale locale){
		
		boolean isNeed = needOrIdea.getIsNeed();
		String typeJ = "Idea";
		
		
		JSONObject needOrIdeaJ = JSONFactoryUtil.createJSONObject();
		
		needOrIdeaJ.put("tit", needOrIdea.getIdeaTitle());
		needOrIdeaJ.put("text",  IdeasListUtils.getSocialSummaryByIdea(needOrIdea));
		
		
		if (isNeed){
			typeJ="Need";
			needOrIdeaJ.put("url", IdeaManagementSystemProperties.getFriendlyUrlNeeds(needOrIdea.getIdeaID()));
			needOrIdeaJ.put("numberOfLinkedChallenge", getNumberChallengesLinkedToNeed(needOrIdea));
			needOrIdeaJ.put("numberOfProposedIdeas", getNumberIdeasProposedToNeed(needOrIdea) );
			
		}else{
			needOrIdeaJ.put("url", IdeaManagementSystemProperties.getFriendlyUrlIdeas(needOrIdea.getIdeaID()));
			needOrIdeaJ.put("isTakenUp",  needOrIdea.isTakenUp()   ) ;
			
			needOrIdeaJ.put("status", needOrIdea.getIdeaStatus());
			needOrIdeaJ.put("statusi18n", MyUtils.getIdeaStatusI18n(needOrIdea.getIdeaStatus(),  locale) );
			
			
			if ( needOrIdea.isTakenUp() )
				needOrIdeaJ.put("referredToIcon",  "business_center" ) ;
			else
				needOrIdeaJ.put("referredToIcon",  "location_city" ) ;
			
			if ( needOrIdea.getChallengeId() > -1 ){
				needOrIdeaJ.put("associatedToChallenge",  true );
				needOrIdeaJ.put("associatedChallengeName",   IdeasListUtils.getChallengeNameByIdeaId(needOrIdea.getIdeaID()) );
				needOrIdeaJ.put("associatedChallengeURL", IdeasListUtils.getUrlChallengeByIdeaId(needOrIdea.getIdeaID())  );
				
			}else{
				needOrIdeaJ.put("associatedToChallenge",  false );
			}
			
			
			if ( needOrIdea.getNeedId() > 0 ){
				needOrIdeaJ.put("associatedToNeed",true); 
				needOrIdeaJ.put("associatedNeedName",IdeasListUtils.getNeedNameByIdea(needOrIdea));
				needOrIdeaJ.put("associatedNeedURL", IdeasListUtils.getUrlNeedByIdea(needOrIdea));
				
			}else{
				needOrIdeaJ.put("associatedToNeed",false);
			}
			
		}
		needOrIdeaJ.put("type", typeJ);
		needOrIdeaJ.put("image", IdeaManagementSystemProperties.getRootUrl() + IdeasListUtils.getRepresentativeImgUrlByIdeaId(needOrIdea.getIdeaID()));
		needOrIdeaJ.put("rating", IdeasListUtils.getAverageDoubleRatingsByIdeaOrNeed(needOrIdea));
		needOrIdeaJ.put("vocabularyName", IdeasListUtils.getVocabularyNameByIdeaId(needOrIdea.getIdeaID(), locale));
		needOrIdeaJ.put("vocabularyIcon", IdeasListUtils.getIconForVocabularyByIdea(needOrIdea));
		needOrIdeaJ.put("vocabularyColor", IdeasListUtils.getColorForVocabularyByIdea(needOrIdea));
		needOrIdeaJ.put("authorName", IdeasListUtils.getAuthorNameByIdeaId(needOrIdea.getIdeaID()));
		needOrIdeaJ.put("themes", IdeasListUtils.getCategoriesVirgoleByIdeaId(needOrIdea.getIdeaID(), locale)  );
		needOrIdeaJ.put("creationDate",  IdeasListUtils.getDateDDMMYYYYByIdeaId(needOrIdea.getIdeaID())) ;
		needOrIdeaJ.put("referredTo",  IdeasListUtils.getMunicipalityOrganizationNameByIdea(needOrIdea)   ) ;
		

		
		return needOrIdeaJ;
	}
	
	
	/**
	 * @param jo
	 * @param ja
	 * @return
	 */
	private static boolean existJsonObjectInJsonArray (JSONObject jo, JSONArray ja ){
		
		String urlJo = jo.getString("url");
		
		for ( int i = 0; i< ja.length(); i++){
			
			JSONObject row = ja.getJSONObject(i);
			
			String urlJa = row.getString("url");
			
			if (urlJo.equalsIgnoreCase(urlJa))
				return true;
		}
		
		return false;
	}
	
	/**
	 * @param need
	 * @return
	 */
	public static int getNumberChallengesLinkedToNeed(CLSIdea need){
		
		int numChallengeNeed=0;
		
		try {
			
			numChallengeNeed = NeedLinkedChallengeLocalServiceUtil.getChallengeByNeedId(need.getIdeaID()).size();
			
			
		} catch (SystemException e) {
			
			e.printStackTrace();
		}
		
		return numChallengeNeed;
	}
	
	/**
	 * @param need
	 * @return
	 */
	public static int getNumberIdeasProposedToNeed(CLSIdea need){
	
		int numIdeeNeed=0;
		
		try {
			numIdeeNeed = CLSIdeaLocalServiceUtil.getIdeasByNeedId(need.getIdeaID()).size();
		} catch (SystemException e) {
			
			e.printStackTrace();
		}
		return numIdeeNeed;
	}
}
