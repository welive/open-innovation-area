package it.eng.rspa.ideas.challenges;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria;
import it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation;
import it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetForChallengeImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengeImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengePoiImpl;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCategoriesSetForChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengePoiLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteChallengesLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.EvaluationCriteriaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.IdeaEvaluationLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.NeedLinkedChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.NeedLinkedChallengePK;
import it.eng.rspa.ideas.controlpanel.CategoryKeys;
import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;
import it.eng.rspa.ideas.utils.ChallengesUtils;
import it.eng.rspa.ideas.utils.IdeasListUtils;
import it.eng.rspa.ideas.utils.MyConstants;
import it.eng.rspa.ideas.utils.MyUtils;
import it.eng.rspa.ideas.utils.Tweeting;
import it.eng.rspa.ideas.utils.listener.ChallengeLoggingNotifier;
import it.eng.rspa.ideas.utils.listener.iChallengeListener;
import it.eng.rspa.jms.Producer;
import it.eng.rspa.jms.model.IdeaCompetitionDataWrapper;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.activation.MimetypesFileTypeMap;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import com.liferay.calendar.model.CalendarBooking;
import com.liferay.calendar.service.CalendarBookingLocalServiceUtil;
import com.liferay.calendar.service.CalendarLocalServiceUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.ResourceConstants;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.RoleConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.service.ResourceLocalServiceUtil;
import com.liferay.portal.service.ResourcePermissionServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.service.SubscriptionLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.asset.model.AssetEntry;
import com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil;
import com.liferay.portlet.social.service.SocialActivityLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;


/**
 * Portlet implementation class Challenges
 */
/**
 * @author UTENTE
 *
 */
public class Challenges extends MVCPortlet {
	User user;
	Log logger = LogFactoryUtil.getLog(this.getClass());
	
	public static Set<iChallengeListener> challengeListeners = new HashSet<iChallengeListener>();
	
	static{
		if(IdeaManagementSystemProperties.getEnabledProperty("lbbEnabled")){
			challengeListeners.add(new ChallengeLoggingNotifier());
		}
	}
	
	
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////		
/////////////////////////////////		  	deleteChallenge 	///////////////////////////////////////////	
///////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public void deleteChallenge(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		
		
		user = (User) actionRequest.getAttribute(WebKeys.USER);
		
		ResourceBundle res = MyUtils.getResBundleViaPilotByAction(actionRequest);
		final String challengeS = new String(res.getString("ims.challenge") .getBytes("ISO-8859-1"), "UTF-8");
		final String deletedS = new String(res.getString("ims.deleted") .getBytes("ISO-8859-1"), "UTF-8");
		
		long challengeId = ParamUtil.getLong(actionRequest, "challengeId");
		
		if (challengeId > 0) {//only the author can delete the Challenge - Security issue
			
			boolean isAuthor = ChallengesUtils.isAuthorByChallengeId(challengeId, user);
			boolean isOmniadmin = PortalUtil.isOmniadmin(user.getUserId());
			boolean isEnabled =  isAuthor || isOmniadmin;
			
			
			if (!isEnabled){//add error
				SessionErrors.add(actionRequest, "error");
				return;
			}
		}


			CLSChallenge challenge = null;
			try {
				challenge = CLSChallengeLocalServiceUtil.fetchCLSChallenge(challengeId);
			} catch (SystemException e1) {
				
				e1.printStackTrace();
			}
			
			//elimino il calendar booking
			long calendarBookingId = challenge.getCalendarBooking();
			if(calendarBookingId>0){
				try {
					CalendarBookingLocalServiceUtil.deleteCalendarBooking(calendarBookingId);
				} catch (PortalException | SystemException e) {
					
					e.printStackTrace();
				}
			}
			
			//rimuovo l'associzione con le idee 
			try {
				ServiceContext serviceContext = ServiceContextFactory.getInstance(actionRequest);
			} catch (PortalException | SystemException e1) {
				
				e1.printStackTrace();
			}
			
			
			
			List<CLSIdea> ideas = new ArrayList<CLSIdea>();
			try {
				ideas = CLSIdeaLocalServiceUtil.getIdeasByChallengeId(challengeId);
			} catch (SystemException e1) {
				
				e1.printStackTrace();
			}

			for (CLSIdea idea:ideas){
			
				idea.setChallengeId(-1);
				
				try {
					CLSIdeaLocalServiceUtil.updateCLSIdea(idea);
				} catch (SystemException e) {
					
					e.printStackTrace();
				}
				
				
			}
			
			//elmino i file associati e la folder della gara
			Long groupId = ParamUtil.getLong(actionRequest, "groupid");
			Long folderId = challenge.getIdFolder();
			Integer count = 0;
			try {
				count = DLFileEntryLocalServiceUtil.getFileEntriesCount(groupId, folderId);
			} catch (SystemException e1) {
				
				e1.printStackTrace();
			}
			OrderByComparator obc = OrderByComparatorFactoryUtil.create("DLFileEntry", "title", true);
			List<DLFileEntry> files = new ArrayList<DLFileEntry>();
			try {
				files = DLFileEntryLocalServiceUtil.getFileEntries(groupId.longValue(), folderId.longValue(), 0, count, obc);
			} catch (SystemException e1) {
				
				e1.printStackTrace();
			}

			
			for (DLFileEntry file:files){
			
				try {
					DLFileEntryLocalServiceUtil.deleteDLFileEntry(file);
				} catch (SystemException e) {
					
					e.printStackTrace();
				}
			}
			
			
			
			//rimuovo la social activity della folder
			try {
				SocialActivityLocalServiceUtil.deleteActivities(DLFolder.class.getName(), folderId);
			} catch (PortalException | SystemException e1) {
				
				e1.printStackTrace();
			}
			
			////////////////////////rimuovo la cartella come asset
			try{ AssetEntryLocalServiceUtil.deleteEntry(DLFolder.class.getName(),folderId);
			}catch(Exception e){ System.out.println(e.getMessage()); }
			
			
			try {
				DLFolderLocalServiceUtil.deleteDLFolder(folderId);
			} catch (PortalException | SystemException e1) {
				
				e1.printStackTrace();
			}

			
			/////////////////remove the association with the need.
			ChallengesUtils.deleteNeedLinkedChallengeByChallenge(challengeId);
			
			
			
			//invio la notifica della cancellazione della gara a tutti gli utenti che la hanno segnata come preferita
			List<CLSFavouriteChallenges> favouriteChallengesEntries = new ArrayList<CLSFavouriteChallenges>();
			try {
				favouriteChallengesEntries = CLSFavouriteChallengesLocalServiceUtil.getFavouriteChallengesEntriesByChallengeId(challengeId);
			} catch (SystemException e1) {
				
				e1.printStackTrace();
			}

				
				
			for (CLSFavouriteChallenges favCh:favouriteChallengesEntries){
				
				String textMessage = challengeS + " " + challenge.getChallengeTitle() + " " + deletedS;
				long selectedUserId = favCh.getUserId();
				String senderName = user.getFullName();
				
				try {
					CLSChallengeLocalServiceUtil.sendNotification(textMessage, selectedUserId, senderName, challengeId, actionRequest,"delete");
				} catch (PortalException | SystemException e) {
					
					e.printStackTrace();
				}
			}
			
			
			//rimuovo la gara come preferita per tutti gli utenti che la avevano segnata come tale ed invio loro una notifica
			try {
				CLSFavouriteChallengesLocalServiceUtil.removeFavouriteChallengesOnChallengeDelete(challengeId);
			} catch (NoSuchCLSFavouriteChallengesException | SystemException e1) {
				
				e1.printStackTrace();
			}
			
			//rimuovo i poi della gara
			List<CLSChallengePoi> challengePois = new ArrayList<CLSChallengePoi>();
			try {
				challengePois = CLSChallengePoiLocalServiceUtil.getChallengePOIByChallengeId(challengeId);
			} catch (SystemException e1) {
				
				e1.printStackTrace();
			}
				
				
			for (CLSChallengePoi challengePoi:challengePois)	{
				
				try {
					CLSChallengePoiLocalServiceUtil.deleteCLSChallengePoi(challengePoi);
				} catch (SystemException e) {
					
					e.printStackTrace();
				}
			}
			
			//rimuovo l'associazione tra la categoria, che poi sarebbe il vocabolario, e questa gara (che ha una tabella a parte)
			try {
				List<CLSCategoriesSetForChallenge> recordSet = CLSCategoriesSetForChallengeLocalServiceUtil.getCategoriesSetForChallenge(challengeId);
				
				for (CLSCategoriesSetForChallenge recSet : recordSet){
					CLSCategoriesSetForChallengeLocalServiceUtil.deleteCLSCategoriesSetForChallenge(recSet);
				}
				
			} catch (SystemException e2) {
				
				e2.printStackTrace();
			}
			
			
			
			//Delete all notification on the challenge 
			try {
				SubscriptionLocalServiceUtil.deleteSubscriptions(challenge.getCompanyId(), CLSChallenge.class.getName(), challenge.getChallengeId() );
			} catch (PortalException | SystemException e2) {

				e2.printStackTrace();
			}
			
			
			
			
			//rimuovo la gara come asset
			try {
				AssetEntryLocalServiceUtil.deleteEntry(CLSChallenge.class.getName(), challenge.getPrimaryKey());
			} catch (PortalException | SystemException e1) {
				
				e1.printStackTrace();
			}
			
			//rimuovo le social activities
			try {
				SocialActivityLocalServiceUtil.deleteActivities(CLSChallenge.class.getName(), challengeId);
			} catch (PortalException | SystemException e1) {
				
				e1.printStackTrace();
			}
			
			//rimuovo la gara dalle resource
			try {
				ResourceLocalServiceUtil.deleteResource(challenge.getCompanyId(),CLSChallenge.class.getName(),ResourceConstants.SCOPE_INDIVIDUAL, challenge.getPrimaryKey());
			} catch (PortalException | SystemException e1) {
				
				e1.printStackTrace();
			}
			
			/** Fire all listeners for the event DELETE **/
			for(iChallengeListener listener : challengeListeners){
				listener.onChallengeDelete(challenge);
			}
			
			//cancello la gara
			try { CLSChallengeLocalServiceUtil.deleteCLSChallenge(challengeId); } 
			catch (PortalException | SystemException e1) { e1.printStackTrace(); }
			
			
			/////////////////////////////BONIFICO I TAG ////////////////////////////////
			MyUtils.bonificaTags();
			/////////////////////////////BONIFICO I TAG ////////////////////////////////
			
			
			//////////////////////////////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////    JMS - inizio
			//////////////////////////////////////////////////////////////////////////////////////////////////////
			
			//Invio la notifica JMS al Sentiment Analyzer
			if(IdeaManagementSystemProperties.getEnabledProperty("jmsEnabled")){
				try {
					Producer sentimentProducer = new Producer(IdeaManagementSystemProperties.getProperty("jmsTopic"));
					sentimentProducer.sendChallengeRemoved(challengeId);
				} 
				catch (Exception e) {
					System.out.println("In deleteChallenge - JMS");
					e.printStackTrace();
				}
			}
			
			//////////////////////////////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////    JMS - fine
			//////////////////////////////////////////////////////////////////////////////////////////////////////

	}

	
///////////////////////////////////////////////////////////////////////////////////////////////////////////		
/////////////////////////////////		  	updateChallenges 	///////////////////////////////////////////	
///////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public long updateChallenges(ActionRequest actionRequest,ActionResponse actionResponse)   {

		
		ThemeDisplay themeD = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		user = (User) actionRequest.getAttribute(WebKeys.USER);
		
		long challengeId = ParamUtil.getLong(actionRequest, "challengeId");
		
		boolean isAuthor = false;
		if (challengeId > 0) {//only the author can edit the Challenge - Security issue
			
			isAuthor = ChallengesUtils.isAuthorByChallengeId(challengeId, user);
			if (!isAuthor)//add error
				return-1;
		}
		
		
		String allegatoAllaGara = "attached to challenge";
		String challengeS = "challenge";
		String modifiedS = "modified";
		String challEndDate = "challenge end date";
		String documentiGara = "challenge documents";
		
		ResourceBundle res = MyUtils.getResBundleViaPilotByAction(actionRequest);
		
		try{
			 allegatoAllaGara = new String(res.getString("ims.attached-to-challenge") .getBytes("ISO-8859-1"), "UTF-8");
			 challengeS = new String(res.getString("ims.challenge") .getBytes("ISO-8859-1"), "UTF-8");
			 modifiedS = new String(res.getString("ims.modified") .getBytes("ISO-8859-1"), "UTF-8");
			 challEndDate = new String(res.getString("ims.challenge-end-date") .getBytes("ISO-8859-1"), "UTF-8");
			 documentiGara = new String(res.getString("ims.challenge-documents") .getBytes("ISO-8859-1"), "UTF-8");
		}catch(Exception e){
			System.out.println(e.getMessage()); 
		}
		

		Map<String, String[]> paramMap = actionRequest.getParameterMap();
		Set<String> set = paramMap.keySet();
		Iterator<String> setIt = set.iterator();

		ArrayList<Long> categoriesUserIdList = new ArrayList<Long>();
		
		String[] actionIdWiew =  {ActionKeys.VIEW};
		String[] actionIdRoot =  {ActionKeys.VIEW,ActionKeys.ACCESS, ActionKeys.ADD_DOCUMENT,ActionKeys.ADD_SHORTCUT,ActionKeys.ADD_SUBFOLDER,ActionKeys.UPDATE};
		// imposto i permessi per il gruppo user
		
		Role userRols = null;
		try {
			userRols = RoleLocalServiceUtil.getRole(user.getCompanyId(), RoleConstants.USER);
		} catch (PortalException | SystemException e2) {
			
			e2.printStackTrace();
		}	
		
		
		List<Role> allRoles = new ArrayList<Role>();
		try {
			allRoles = RoleLocalServiceUtil.getRoles(user.getCompanyId());
		} catch (SystemException e2) {
			
			e2.printStackTrace();
		}
		List<Role> roles = new ArrayList<Role>(); // se voglio tutti i ruoli --> RoleLocalServiceUtil.getRoles(user.getCompanyId());				
		roles.add(userRols);		
		
		//contiene i POI della gara
		HashMap<String, CLSChallengePoi> pois = new HashMap<>();
		
		//contiene i set di categorie di rifeimento della gara
		ArrayList<CLSCategoriesSetForChallenge> categoriesSet = new ArrayList<CLSCategoriesSetForChallenge>();
		
		long[] catsIdsLongArray = null;
		String[] tagsArrgay = null;
		long vocIdChallenge = 0;
		
		// cerco le categorie associate [vecchio codice che utilizza gli asset
		// di liferay]
		while (setIt.hasNext()) {
			String key = (String) setIt.next();


			//Gestione dei POI
			if(key.startsWith("newPoiInput")){
				String poiIdentifier = key.substring(key.indexOf("#") + 1);
				String poiAttribute = key.substring("newPoiInput".length(),key.indexOf("_#"));
				if(pois.containsKey(poiIdentifier)){
					CLSChallengePoi poi = pois.get(poiIdentifier);
					if(poiAttribute.equals("Description")){
						poi.setDescription(paramMap.get(key)[0]);
					}else if(poiAttribute.equals("Title")){
						poi.setTitle(paramMap.get(key)[0]);
					}else if(poiAttribute.equals("Longitude")){
						poi.setLongitude(paramMap.get(key)[0]);
					}else if(poiAttribute.equals("Latidute")){
						poi.setLatitude(paramMap.get(key)[0]);
					}
				}else{
					CLSChallengePoi poi = new CLSChallengePoiImpl();
					
					 
					try {
						long newPoiId = CounterLocalServiceUtil.increment(CLSChallengePoi.class.getName());
						poi.setPoiId(newPoiId);
					} catch (SystemException e) {
						
						e.printStackTrace();
					}
										
					
					if(poiAttribute.equals("Description")){
						poi.setDescription(paramMap.get(key)[0]);
					}else if(poiAttribute.equals("Title")){
						poi.setTitle(paramMap.get(key)[0]);
					}else if(poiAttribute.equals("Longitude")){
						poi.setLongitude(paramMap.get(key)[0]);
					}else if(poiAttribute.equals("Latidute")){
						poi.setLatitude(paramMap.get(key)[0]);
					}
					pois.put(poiIdentifier, poi);
				}
			}
			//Gestione dei POI - Fine
			
			
			
			//Gestione delle categorie
			if (key.equalsIgnoreCase("radio_vocabulary")) {
				
				String vocabularyParameterValue = actionRequest.getParameter(key);
				vocIdChallenge = Long.parseLong(vocabularyParameterValue);
					//creo un nuovo oggetto per le categorie

					CLSCategoriesSetForChallenge categoriesSetForChallenge = new CLSCategoriesSetForChallengeImpl();
										
					categoriesSetForChallenge.setCategoriesSetID(vocIdChallenge);
					
//					if( Integer.parseInt(inputTypeValue) == CategoryKeys.SINGLE_CHOICE ){
//						categoriesSetForChallenge.setType(CategoryKeys.SINGLE_CHOICE);
//					}else{
						categoriesSetForChallenge.setType(CategoryKeys.MULTI_CHOICE);
//					}
					
					categoriesSet.add(categoriesSetForChallenge);
			}
			
			//Gestione delle categorie - fine
		}

		// converto categoriesUserIdList (ArrayList<Long>) in catsIdsLongArray
		// (long[])
		if (!categoriesUserIdList.isEmpty()) {
			catsIdsLongArray = new long[categoriesUserIdList.size()];
			for (int count = 0; count < categoriesUserIdList.size(); count++) {
				catsIdsLongArray[count] = categoriesUserIdList.get(count).longValue();
			}
		}

		//recupero l'immagine rappresentativa
		String representativeImgUrl = ParamUtil.getString(actionRequest,"representativeImgUrl");
				
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		// recupero la data di inizio del challenge
		int startDateMonth = ParamUtil.getInteger(actionRequest,"startDateMonth");
		startDateMonth++;
		int startDateDay = ParamUtil.getInteger(actionRequest, "startDateDay");
		int startDateYear = ParamUtil.getInteger(actionRequest,"startDateYear");
		int startDateHour = ParamUtil.getInteger(actionRequest,"startDateHour");
		int startDateMinute = ParamUtil.getInteger(actionRequest,"startDateMinute");
		int startDateAmPm = ParamUtil.getInteger(actionRequest,"startDateAmPm");

		if (startDateAmPm == Calendar.PM) {
			startDateHour += 12;
		}

		String dateStartString = startDateDay + "/" + startDateMonth + "/"
				+ startDateYear + " " + startDateHour + ":"
				+ startDateMinute;

		Date dateStart = null;
		try {
			dateStart = formatter.parse(dateStartString);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		// FINE recupero data inizio gara
		
		// recupero la data di fine del challenge
		int endDateMonth = ParamUtil.getInteger(actionRequest,"endDateMonth");
		endDateMonth++;
		int endDateDay = ParamUtil.getInteger(actionRequest, "endDateDay");
		int endDateYear = ParamUtil.getInteger(actionRequest,"endDateYear");
		int endDateHour = ParamUtil.getInteger(actionRequest,"endDateHour");
		int endDateMinute = ParamUtil.getInteger(actionRequest,"endDateMinute");
		int endDateAmPm = ParamUtil.getInteger(actionRequest,"endDateAmPm");

		if (endDateAmPm == Calendar.PM) {
			endDateHour += 12;
		}

		String dateEndString = endDateDay + "/" + endDateMonth + "/"
				+ endDateYear + " " + endDateHour + ":"
				+ endDateMinute;

		Date dateEnd = null;
		
		try {
			dateEnd = formatter.parse(dateEndString);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		// FINE recupero data fine gara

		String challengeTitle = ParamUtil.getString(actionRequest, "challengeTitle");
		String challengeDescription = ParamUtil.getString(actionRequest,"challengeDescription");
		String tags = ParamUtil.getString(actionRequest, "assetTagNames");
		
		boolean challengeWithReward = ParamUtil.getBoolean(actionRequest, "challengeWithReward");
		String challengeReward = ParamUtil.getString(actionRequest,	"challengeReward");
		int numberOfSelectableIdeas = ParamUtil.getInteger (actionRequest,	"numberOfSelectableIdeas");
		String challengeLanguage = ParamUtil.getString(actionRequest,"languageChosen");
		String needs = ParamUtil.getString(actionRequest,	"needSelection");
		
		String[] needsArray = null;
		
		// convert needs in needsArray
		if (needs.length() > 0) {
			needsArray = needs.split(",");
		}
		
		
		String challengeHashTag = ParamUtil.getString(actionRequest, "challengeHashTag");
		
		
		// converto tags in tagsArray
		if (tags.length() > 0) {
			tagsArrgay = tags.split(",");
		}

		CLSChallenge challenge = new CLSChallengeImpl();
		// set primary key
		
		boolean isNew = false;
		try {
			if (challengeId < 0) {
				isNew = true;
				challengeId = CounterLocalServiceUtil.increment(this.getClass().getName());
				challenge.setChallengeId(challengeId);
				challenge.setLanguage(challengeLanguage);
				challenge.setChallengeStatus(MyConstants.CHALLENGE_STATE_OPEN);//default state
				//qui
			}else{
				// allora recupero la challenge dal db
				challenge = CLSChallengeLocalServiceUtil.fetchCLSChallenge(challengeId);
			}
			
			

			challenge.setChallengeId(challengeId);
			challenge.setRepresentativeImgUrl(representativeImgUrl);
			challenge.setChallengeTitle(challengeTitle);
			challenge.setChallengeHashTag(challengeHashTag);
			challenge.setChallengeDescription(challengeDescription);
			challenge.setChallengeWithReward(challengeWithReward);
			challenge.setChallengeReward(challengeReward);
			challenge.setNumberOfSelectableIdeas(numberOfSelectableIdeas);
			challenge.setDateAdded(new Date());
			
			//gestione delle data di inizio e fine della gara - inizio
			challenge.setDateStart(dateStart);
			challenge.setDateEnd(dateEnd);
			//gestione delle data di inizio e fine della gara - fine
			
			challenge.setUserId(user.getUserId());

			challenge.setGroupId(ParamUtil.getLong(actionRequest, "groupid"));
			challenge.setCompanyId(user.getCompanyId());

			ServiceContext serviceContext = ServiceContextFactory.getInstance(actionRequest);
			Long groupId = ParamUtil.getLong(actionRequest, "groupid");

			// recupero la cartella che contiene tutte le sottocartelle ciascuna
			// per ogni idea partendo dalla root

			DLFolder ideaManagementFolderRoot;
			try{
				ideaManagementFolderRoot = DLFolderLocalServiceUtil.getFolder(groupId, 0, MyConstants.DL_FOLDER_ROOT);
			} catch (Exception e) {
				//questa folder non esiste la creo
				ideaManagementFolderRoot=DLFolderLocalServiceUtil.addFolder(user.getUserId(),// userId,
						ParamUtil.getLong(actionRequest, "groupid"),// groupId
						ParamUtil.getLong(actionRequest, "groupid"),// repositoryId,
						false,// mountPoint,
						0,// parentFolderId,
																// 0 e' la root.
																// In questo
																// caso 20406 e'
																// la folder
																// ideaManagementRoot
																// che contiene
																// le
																// sottocartelle
																// per ciascuna
																// idea.
						MyConstants.DL_FOLDER_ROOT,// name,
						"",// description,
						false,
						serviceContext);// serviceContext)

					    setPermission(actionRequest, Long.toString(ideaManagementFolderRoot.getFolderId()), DLFolder.class.getName(),actionIdRoot,roles);
						
			}
			
			
			// dichiaro la folder di questa idea
			DLFolder folder;
			
			//File folderUpload = new File(getInitParameter("uploadFolder")
			File dummyFile = File.createTempFile("dummy-file", ".dummy");
			String tempdir = dummyFile.getParent();

			dummyFile.delete();
			File folderUpload = new File(tempdir
					+ File.separator
					+ ParamUtil.getString(actionRequest, "token"));
			
			if (isNew) {
				
				IdeaManagementSystemProperties imsp = new IdeaManagementSystemProperties();
				
				try{
				
					long referenceCalendarId = imsp.getChallengesReferenceCalendarId();
					if(referenceCalendarId > -1){
					
						com.liferay.calendar.model.Calendar referenceCalendar = CalendarLocalServiceUtil.fetchCalendar(referenceCalendarId);
						
						CalendarBooking calendarBooking = CalendarBookingLocalServiceUtil.createCalendarBooking(CounterLocalServiceUtil.increment());
						
						calendarBooking.setAllDay(true);
						calendarBooking.setCalendarId(referenceCalendar.getCalendarId());
						calendarBooking.setCalendarResourceId(referenceCalendar.getCalendarResourceId());
						calendarBooking.setCompanyId(user.getCompanyId());
	
						
						
						
						calendarBooking.setTitle(challEndDate + challengeTitle); 
						calendarBooking.setDescription(challEndDate + challengeTitle);
						
						calendarBooking.setCreateDate(new Date());
						calendarBooking.setModifiedDate(new Date());
						calendarBooking.setStartTime(dateEnd.getTime());
						calendarBooking.setEndTime(dateEnd.getTime());
						
						calendarBooking.setFirstReminder(0);
						calendarBooking.setFirstReminderType("email");
						calendarBooking.setGroupId(ParamUtil.getLong(actionRequest, "groupid"));
						calendarBooking.setLocation("");
						
						calendarBooking.setParentCalendarBookingId(calendarBooking.getCalendarBookingId());
						calendarBooking.setRecurrence("");
						calendarBooking.setSecondReminder(0);
						calendarBooking.setSecondReminderType("email");
						
						calendarBooking.setStatus(0);
						calendarBooking.setStatusByUserId(0);
						calendarBooking.setStatusByUserName("");
						calendarBooking.setStatusDate(new Date());
						
						calendarBooking.setUserId(user.getUserId());
						calendarBooking.setUserName(user.getFullName());
										
						CalendarBookingLocalServiceUtil.addCalendarBooking(calendarBooking);
						
						AssetEntryLocalServiceUtil.updateEntry(user.getUserId(),
								ParamUtil.getLong(actionRequest, "groupid"), CalendarBooking.class.getName(),
								calendarBooking.getPrimaryKey(), catsIdsLongArray, tagsArrgay);
						
						challenge.setCalendarBooking(calendarBooking.getCalendarBookingId());
					}
				
				}catch(Exception e){ 
					System.out.println("updateChallenges exception: "+ e.getMessage()); 
				}
				
				
				// l'idea e nuova creo la folder di questa idea in ideaManagementFolderRoot
				
				
				String folderDescription = documentiGara + challengeTitle; 
				long idFolder = 0;
			try{
				
				
				// imposto il nome della folder
				String dmFolderName = challengeId
						+ "_challenge_"
						+ ParamUtil.getString(actionRequest, "challengeTitle")
								.replaceAll("[^\\dA-Za-z ]", "")
								.replaceAll("\\s+", "_");
				
				if(dmFolderName.length()>100){
					dmFolderName = dmFolderName.substring(0, 99);
				}
				
				folder = DLFolderLocalServiceUtil.addFolder(user.getUserId(),// userId,
						ParamUtil.getLong(actionRequest, "groupid"),// groupId
						ParamUtil.getLong(actionRequest, "groupid"),// repositoryId,
						false,// mountPoint,
						ideaManagementFolderRoot.getFolderId(),// parentFolderId,
																// 0 e' la root.
																// In questo
																// caso 20406 e'
																// la folder
																// ideaManagementRoot
																// che contiene
																// le
																// sottocartelle
																// per ciascuna
																// idea.

						dmFolderName,// name,
						folderDescription,// description,
						false,
						serviceContext);// serviceContext)
				
				setPermission(actionRequest, Long.toString(folder.getFolderId()), DLFolder.class.getName(),actionIdWiew,roles);
			
				
				challenge.setDmFolderName(dmFolderName);
				challenge.setIdFolder(folder.getFolderId());
				
				idFolder = folder.getFolderId();
				
			}catch(Exception e){ 
				System.out.println("updateChallenges folder exception: "+ e.getMessage()); 
			}
				
			// dopo aver creato la folder di questa idea, faccio l'upload dei files
			
			try{
				
				File[] listOfFiles = folderUpload.listFiles();
				if (listOfFiles!=null){
						for (int i = 0; i < listOfFiles.length; i++) {		
							
							
						  try{	
							
							if (listOfFiles[i].isFile()) {
								File f = listOfFiles[i];
								String mimeType = URLConnection.guessContentTypeFromName(f.getName());
								
								if(mimeType==null){
									mimeType = new MimetypesFileTypeMap().getContentType(f);
								}
								
								
								
							    
								DLFileEntry fe = DLFileEntryLocalServiceUtil.addFileEntry(
										user.getUserId(), // userId,
										ParamUtil.getLong(actionRequest, "groupid"),// groupId
										ParamUtil.getLong(actionRequest, "groupid"),// repositoryId,
										idFolder,// folderId
										f.getName(), // sourceFileName
										mimeType, // mimeType
										f.getName(), // title
										allegatoAllaGara + challengeTitle,//challengeDescription,// description
										"upload",// changeLog
										0,// entryTypeId
										null,
										f,// file
										new FileInputStream(f), f.length(),
										serviceContext);
								
								DLFileEntryLocalServiceUtil.updateFileEntry(user.getUserId(), // userId
			                                fe.getFileEntryId(), //fileEntryId,
			                                f.getName(), // sourceFileName
			                                mimeType, // mimeType
			                                f.getName(), // title
			                                allegatoAllaGara + challengeTitle,//challengeDescription,// description
			                                "update status to publish", // changeLog,
			                                true, //majorVersion,
			                                fe.getFileEntryTypeId(), // fileEntryTypeId,
			                                null, //  fieldsMap,
			                                f, // file
			                                new FileInputStream(f), // is,
			                                f.length(), // size,
			                                serviceContext);
								
								
							 // imposto i permessi per il gruppo user					    
							setPermission(actionRequest, Long.toString(fe.getFileEntryId()), DLFileEntry.class.getName(),actionIdWiew, allRoles); //roles);
							   
							}
							
						  }catch(Exception e){ 
								System.out.println("updateIdeas folder exception: "+ e.getMessage()); 
							}
						}
				
				}
				

				
			}catch(Exception e){ 
				System.out.println("updateChallenges file exception: "+ e.getMessage()); 
			}
				
				SocialActivityLocalServiceUtil.addActivity(user.getUserId(), ParamUtil.getLong(actionRequest, "groupid"),
						CLSChallenge.class.getName(), challengeId,
						ChallengesActivityKeys.ADD_CHALLENGE, StringPool.BLANK,
						0);

				CLSChallengeLocalServiceUtil.addCLSChallenge(challenge);
			
			
			
			
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				/////////////////////////////////////Tweeting Challenge//////////////////////////////////////////////////////////////////
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
				
				if (IdeaManagementSystemProperties.getEnabledProperty("tweetingEnabled")){
					
					String tipo="Challenge";
					
					Tweeting.twitta(themeD, tipo, challengeTitle, challengeId, challengeHashTag, tagsArrgay, null , categoriesSet  );
				}
			
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				/////////////////////////////////////Tweeting Challenge//////////////////////////////////////////////////////////////////
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
				
				
				
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				/////////////////////////////////////	Notification 	/////////////////////////////////////////////////////////////////
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
				//subscribe the author to challenge comment
				ChallengesUtils.subscribeUserToChallengeComment(challenge, user.getUserId());
				
								
				if (Validator.isNotNull(needsArray)  ){
					
					
					for (int i = 0; i < needsArray.length; i++ ){
						
						long needReport = Long.valueOf(needsArray[i]);
					
				        NeedLinkedChallenge nlc = NeedLinkedChallengeLocalServiceUtil.createNeedLinkedChallenge( new NeedLinkedChallengePK(needReport, challengeId));
				        nlc.setDate(new Date());
				        NeedLinkedChallengeLocalServiceUtil.addNeedLinkedChallenge(nlc);
						
						//I need to bring the challengeId in the Notification class
						actionRequest.setAttribute("challengeForNeed", challengeId);
						CLSIdeaLocalServiceUtil.sendNotificationForTransition(res,needReport, "challengeFromNeed", actionRequest);
						
						
						List<CLSIdea> ideeLinked  = new ArrayList<CLSIdea>();
						try {
							ideeLinked =CLSIdeaLocalServiceUtil.getIdeasByNeedId(needReport);
						} catch (SystemException e) {
							e.printStackTrace();
						}
						
						for (CLSIdea ide:ideeLinked){
							
							if (ide.getChallengeId() <= 0){
								
								long vocIdIdea = IdeasListUtils.getVocabularyIdByIdeaId(ide.getIdeaID());
								
								if (vocIdIdea == vocIdChallenge )//only if the main category is equals
									CLSIdeaLocalServiceUtil.sendNotificationForTransition(res,ide.getIdeaID(), "linkIdeaToChallengeFromNeed", actionRequest);
							}
						}
					}
				}
			
			} else { //aggiornamento gara
				
				
				
				//Manage linked Needs
				List<CLSIdea> oldNeeds = NeedLinkedChallengeLocalServiceUtil.getNeedByChallengeId(challengeId);
				
				if (Validator.isNull(needsArray)  ){
					//delete all
					for (CLSIdea oldNeed:oldNeeds){
						NeedLinkedChallengeLocalServiceUtil.deleteNeedLinkedChallenge(new NeedLinkedChallengePK(oldNeed.getIdeaID(), challengeId));
					}
				}else{
					
					//add the new, added in modify
					for (int i = 0; i < needsArray.length; i++ ){
						
						long needReport = Long.valueOf(needsArray[i]);
						
						NeedLinkedChallenge nlcOld =null;
						try {
							 nlcOld = NeedLinkedChallengeLocalServiceUtil.fetchNeedLinkedChallenge(new NeedLinkedChallengePK(needReport, challengeId));
						} catch (SystemException e1) {
							e1.printStackTrace();
						}
						
						
						if (Validator.isNull(nlcOld)){//isNew need
							
							NeedLinkedChallenge nlc = NeedLinkedChallengeLocalServiceUtil.createNeedLinkedChallenge( new NeedLinkedChallengePK(needReport, challengeId));
					        nlc.setDate(new Date());
					        NeedLinkedChallengeLocalServiceUtil.addNeedLinkedChallenge(nlc);
							
							//I need to bring the challengeId in the Notification class
							actionRequest.setAttribute("challengeForNeed", challengeId);
							CLSIdeaLocalServiceUtil.sendNotificationForTransition(res,needReport, "challengeFromNeed", actionRequest);
							
							
							List<CLSIdea> ideeLinked  = new ArrayList<CLSIdea>();
							try {
								ideeLinked =CLSIdeaLocalServiceUtil.getIdeasByNeedId(needReport);
							} catch (SystemException e) {
								e.printStackTrace();
							}
							
							for (CLSIdea ide:ideeLinked){
								
								if (ide.getChallengeId() <= 0){
									
									long vocIdIdea = IdeasListUtils.getVocabularyIdByIdeaId(ide.getIdeaID());
									
									if (vocIdIdea == vocIdChallenge )//only if the main category is equals
										CLSIdeaLocalServiceUtil.sendNotificationForTransition(res,ide.getIdeaID(), "linkIdeaToChallengeFromNeed", actionRequest);
								}
							}
						}
					}
					
					List<CLSIdea> updatedNeeds = NeedLinkedChallengeLocalServiceUtil.getNeedByChallengeId(challengeId);
					
					//delete the old that has been removed
					for (CLSIdea oldNeed:updatedNeeds){
						boolean found = false;	
						for (int i = 0; i < needsArray.length; i++ ){
							long needReport = Long.valueOf(needsArray[i]);
							
							if (oldNeed.getIdeaID() == needReport ){
								found=true;
								break;
							}
						}
						
						if (!found){
							NeedLinkedChallengeLocalServiceUtil.deleteNeedLinkedChallenge(new NeedLinkedChallengePK(oldNeed.getIdeaID(), challengeId));
						}
					}
				}
				//END - Manage linked Needs
				
				
				//update of calendar booking
				IdeaManagementSystemProperties imsp = new IdeaManagementSystemProperties();
				long referenceCalendarId = imsp.getChallengesReferenceCalendarId();
				if(referenceCalendarId > -1){
					long calendarBookingId = challenge.getCalendarBooking();
					if(calendarBookingId>0){
						CalendarBooking calendarBooking = CalendarBookingLocalServiceUtil.getCalendarBooking(calendarBookingId);
						calendarBooking.setModifiedDate(new Date());
						calendarBooking.setStartTime(dateEnd.getTime());
						calendarBooking.setEndTime(dateEnd.getTime());
						CalendarBookingLocalServiceUtil.updateCalendarBooking(calendarBooking);
					}
				}
				
				// challenge non nuova la folder gia esiste
				// la recupero partendo dalla root
				folder = DLFolderLocalServiceUtil.getFolder(
						ParamUtil.getLong(actionRequest, "groupid"),// groupId
						ideaManagementFolderRoot.getFolderId(),// parent folderId,
						challenge.getDmFolderName());// idea name

				File[] listOfFiles = folderUpload.listFiles();
				if (listOfFiles!=null){
					for (int i = 0; i < listOfFiles.length; i++) {
						
						
						try{
							
							if (listOfFiles[i].isFile()) {
								File f = listOfFiles[i];
								String mimeType = URLConnection.guessContentTypeFromName(f.getName());
								
								if(mimeType==null){
									mimeType = new MimetypesFileTypeMap().getContentType(f);
								}
								
								
								DLFileEntry fe = DLFileEntryLocalServiceUtil.addFileEntry(
										user.getUserId(), // userId,
										ParamUtil.getLong(actionRequest, "groupid"),// groupId
										ParamUtil.getLong(actionRequest, "groupid"),// repositoryId,
										folder.getFolderId(),// folderId
										f.getName(), // sourceFileName
										mimeType, // mimeType
										f.getName(), // title
										allegatoAllaGara + challengeTitle,//challengeDescription,// description
										"upload",// changeLog
										0,// entryTypeId
										null,
										f,// file
										new FileInputStream(f), f.length(),
										serviceContext);
								
								
								DLFileEntryLocalServiceUtil.updateFileEntry(user.getUserId(), // userId
		                                fe.getFileEntryId(), //fileEntryId,
		                                f.getName(), // sourceFileName
		                                mimeType, // mimeType
		                                f.getName(), // title
		                                allegatoAllaGara + challengeTitle,//challengeDescription,// description
		                                "update status to publish", // changeLog,
		                                true, //majorVersion,
		                                fe.getFileEntryTypeId(), // fileEntryTypeId,
		                                null, //  fieldsMap,
		                                f, // file
		                                new FileInputStream(f), // is,
		                                f.length(), // size,
		                                serviceContext);							
								
							setPermission(actionRequest, Long.toString(fe.getFileEntryId()), DLFileEntry.class.getName(),actionIdWiew, allRoles); //roles);
						    
							}
							
						}catch(Exception e){ 
							System.out.println("updateChallenges folder exception: "+ e.getMessage()); 
						}	
					}
				}
				
				//challenge.setDmFolderName(challenge.getDmFolderName());
				challenge.setIdFolder(folder.getFolderId());
				CLSChallengeLocalServiceUtil.updateCLSChallenge(challenge);
				SocialActivityLocalServiceUtil.addActivity(user.getUserId(), ParamUtil.getLong(actionRequest, "groupid"),
						CLSChallenge.class.getName(), challengeId,
						ChallengesActivityKeys.UPDATE_CHALLENGE,
						StringPool.BLANK, 0);
				
				//invio la notifica della modifica della gara a tutti gli utenti che la hanno segnata come preferita
				List<CLSFavouriteChallenges> FavouriteChallengesEntries = CLSFavouriteChallengesLocalServiceUtil.getFavouriteChallengesEntriesByChallengeId(challengeId);
				Iterator<CLSFavouriteChallenges> favouriteChallengesEntriesIt = FavouriteChallengesEntries.iterator();
				while(favouriteChallengesEntriesIt.hasNext()){
					CLSFavouriteChallenges favouriteChallengesEntry = favouriteChallengesEntriesIt.next();					
					
					String textMessage = challengeS + " " + challenge.getChallengeTitle() + " " + modifiedS;
					long selectedUserId = favouriteChallengesEntry.getUserId();
					String senderName = user.getFullName();
					
					CLSChallengeLocalServiceUtil.sendNotification(textMessage, selectedUserId, senderName, challengeId, actionRequest,"modificata");
				}
			}
			//folderUpload.delete();
			
			deleteDirectory(folderUpload);
			
			//Scrivo su db i poi della gara
			//prima di scriverli cancello tutti gia' associati se la gara non e' nuova
			if (!isNew) {
				List<CLSChallengePoi> oldPois = CLSChallengePoiLocalServiceUtil.getChallengePOIByChallengeId(challengeId);
				Iterator<CLSChallengePoi> oldPoisIt = oldPois.iterator();
				while (oldPoisIt.hasNext()) {
					CLSChallengePoi clsChallengePoi = (CLSChallengePoi) oldPoisIt.next();
					CLSChallengePoiLocalServiceUtil.deleteCLSChallengePoi(clsChallengePoi);
				}
			}
			Collection<CLSChallengePoi> poisCollection = pois.values();
			Iterator<CLSChallengePoi> poisCollectionIt = poisCollection.iterator();
			while (poisCollectionIt.hasNext()) {
				CLSChallengePoi clsChallengePoi = (CLSChallengePoi) poisCollectionIt.next();
				clsChallengePoi.setChallengeId(challengeId);
				CLSChallengePoiLocalServiceUtil.addCLSChallengePoi(clsChallengePoi);
			}
			
			//Scrivo su db i set di categorie di riferimento della gara
			//prima di scriverli cancello tutti gia associati se la gara non e' nuova
			if (!isNew) {
				List<CLSCategoriesSetForChallenge> oldCategories = CLSCategoriesSetForChallengeLocalServiceUtil.getCategoriesSetForChallenge(challengeId);
				Iterator<CLSCategoriesSetForChallenge> oldCategoriesIt = oldCategories.iterator();
				while(oldCategoriesIt.hasNext()){
					CLSCategoriesSetForChallenge category = oldCategoriesIt.next();
					CLSCategoriesSetForChallengeLocalServiceUtil.deleteCLSCategoriesSetForChallenge(category);
				}
			}
			Iterator<CLSCategoriesSetForChallenge> categoriesSetIt = categoriesSet.iterator();
			while(categoriesSetIt.hasNext()){
				CLSCategoriesSetForChallenge category = categoriesSetIt.next();
				category.setChallengeId(challengeId);
				CLSCategoriesSetForChallengeLocalServiceUtil.addCLSCategoriesSetForChallenge(category);
			}
			
			if(tagsArrgay==null){
				tagsArrgay = new String[0];
			}
			AssetEntry asset = AssetEntryLocalServiceUtil.updateEntry(user.getUserId(),
					ParamUtil.getLong(actionRequest, "groupid"), CLSChallenge.class.getName(), challenge.getChallengeId(), catsIdsLongArray, tagsArrgay);
			asset.setTitle(challengeTitle);
			asset.setDescription(challengeTitle);
			asset.setSummary(challengeTitle);
			asset.setPublishDate(dateStart);
			
			AssetEntryLocalServiceUtil.updateAssetEntry(asset);
			if (isNew){
				ResourceLocalServiceUtil.addResources(challenge.getCompanyId(),
													  challenge.getGroupId(), 
													  challenge.getUserId(), 
													  CLSChallenge.class.getName(), 
													  challenge.getChallengeId(), 
													  false, 
													  true, 
													  true);
			
				///////////////////////////////////////////////////////////////////////////////////////////
				/////////////////////////////        JMS inizio
				///////////////////////////////////////////////////////////////////////////////////////////
				if(IdeaManagementSystemProperties.getEnabledProperty("jmsEnabled")){
					try{
						Producer producer = new Producer(IdeaManagementSystemProperties.getProperty("jmsTopic"));
						producer.sendChallenge(new IdeaCompetitionDataWrapper(challengeId));
					}
					catch(Exception e){
						System.out.println("In updateChallenge for New Challenge");
						e.printStackTrace();
					}
				}
				
				///////////////////////////////////////////////////////////////////////////////////////////
				/////////////////////////////        JMS fine
				///////////////////////////////////////////////////////////////////////////////////////////				
				
				
				
			}
			
			for(iChallengeListener listener : challengeListeners){
				if(isNew){ listener.onChallengePublish(challenge); }
				else{ listener.onChallengeUpdate(challenge); }
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return challengeId;

	}
	
	
	 /**
	 * Creation and management of the criteria for selection of Ideas on the Challenge
	 * @param actionRequest
	 * @param actionResponse
	 */
	 public void manageCriteria (ActionRequest actionRequest, ActionResponse actionResponse){
		 
		 long challengeId = ParamUtil.getLong(actionRequest, "challengeId");
		 
		 User user = (User) actionRequest.getAttribute(WebKeys.USER);
		 boolean isAuthor = ChallengesUtils.isAuthorByChallengeId(challengeId, user);
			if (!isAuthor){//Security issue
				SessionErrors.add(actionRequest, "error");
				return;
		}
		 
		int numBarrierCriteria = ParamUtil.getInteger(actionRequest, "numBarriera");	
		int numQuantitativeCriteria = ParamUtil.getInteger(actionRequest, "numQuantitative");		 
		 
		//barrier criteria 
		saveCriteria(actionRequest,  true, numBarrierCriteria, challengeId);
		
		//Quantitative criteria 
		saveCriteria(actionRequest,  false, numQuantitativeCriteria, challengeId);
		 
		 
		 ChallengesUtils.redirectToChallenge(actionRequest, actionResponse, challengeId);
		 
	 }
	
	
	 
/**
 * It is used by manageCriteria to both Barriera and Quantitative Criteria
 * @param actionRequest
 * @param isBarriera
 * @param numCriteria
 * @param challengeId
 */
private void saveCriteria (ActionRequest actionRequest, boolean isBarriera, int numCriteria,  long challengeId){
	
	User user = (User) actionRequest.getAttribute(WebKeys.USER);
	
	boolean isAuthor = ChallengesUtils.isAuthorByChallengeId(challengeId, user);
		if (!isAuthor){//Security issue
			SessionErrors.add(actionRequest, "error");
			return;
	}
	
	
	
	
	
	String bq = "q";
	if (isBarriera)
		bq = "b";
	
		 
	for (int i =0; i<= numCriteria;i++){
			 
			 boolean isChecked = ParamUtil.getBoolean(actionRequest, bq+"c"+i);
			 
			 String barrierTextCriteria = ParamUtil.getString(actionRequest, bq+"t"+i);
			 boolean isCustom = ParamUtil.getBoolean(actionRequest, bq+"ic"+i);
			 boolean toDelete = ParamUtil.getBoolean(actionRequest, bq+"di"+i);
			 
			 double weight = ParamUtil.getDouble(actionRequest,"w"+i);
			 int threshold = ParamUtil.getInteger(actionRequest, "t"+i);
			 
			 if (isBarriera){
				 
				 weight=0;
				 threshold=0;
			 }
			 
				 
			 if(!barrierTextCriteria.isEmpty()){
					
				//check if it is an update
				Long barrierCriteriaId = ParamUtil.getLong(actionRequest, bq+"cId"+i);
				 
				List<EvaluationCriteria> critWithThisInputId = new ArrayList<EvaluationCriteria>();
				
				if (!isCustom){
					 critWithThisInputId = EvaluationCriteriaLocalServiceUtil.getCriteriaByChallengeIdAndInputId(challengeId, bq+"cId"+i);
				}else{
						 
					 EvaluationCriteria ecExists =null;
					try {
							ecExists = EvaluationCriteriaLocalServiceUtil.fetchEvaluationCriteria(new EvaluationCriteriaPK( barrierCriteriaId, challengeId));
					} catch (SystemException e) {
								
						e.printStackTrace();
					}
									 
					if 	(Validator.isNotNull(ecExists))	 
						critWithThisInputId.add( ecExists);

				}	
				if (critWithThisInputId.size() > 0 ){ //it could be an update or a deletion
					 
					EvaluationCriteria currentEC = critWithThisInputId.get(0);
					if (toDelete){ //if before there was, now it is update only for deletion
						
						deleteCriteriaAndlinkedEvaluation(currentEC);
							
					}else{//it could be an update
						
						currentEC.setAuthorId(user.getUserId());
						currentEC.setChallengeId(challengeId);
						currentEC.setEnabled(isChecked);
						currentEC.setDate(new Date());
						currentEC.setDescription(barrierTextCriteria);
						currentEC.setInputId(bq+"cId"+i);
						currentEC.setIsBarriera(isBarriera);
						currentEC.setIsCustom(isCustom);
						currentEC.setWeight(weight);
						currentEC.setThreshold(threshold);
						currentEC.setLanguage(ChallengesUtils.getLanguageByChallengeId(challengeId));
						try {
							EvaluationCriteriaLocalServiceUtil.updateEvaluationCriteria(currentEC);
						} catch (SystemException e) {
							e.printStackTrace();
						}
						
						
					}
					
						
				}else{//it is a new Criteria
					
					
					if (!toDelete  && (isCustom || isChecked) ){//if 
					
						try {
								long criId= CounterLocalServiceUtil.increment(EvaluationCriteria.class.getName());
								EvaluationCriteria nec = EvaluationCriteriaLocalServiceUtil.createEvaluationCriteria(new EvaluationCriteriaPK( criId, challengeId));
									nec.setAuthorId(user.getUserId());
									nec.setChallengeId(challengeId);
									nec.setEnabled(isChecked);
									nec.setDate(new Date());
									nec.setDescription(barrierTextCriteria);
									nec.setInputId(bq+"cId"+i);
									nec.setIsBarriera(isBarriera);
									nec.setIsCustom(isCustom);
									nec.setWeight(weight);
									nec.setThreshold(threshold);
									nec.setLanguage(ChallengesUtils.getLanguageByChallengeId(challengeId));
								EvaluationCriteriaLocalServiceUtil.addEvaluationCriteria(nec);
								
						} catch (SystemException e) {
								e.printStackTrace();
						}
					}	 
				}
					
					 
		  }//if(!barrierTextCriteria.isEmpty()){
				 
			 
	}
		 
		 
 }//method



/**
 * @param evalCriteria
 */
private static void deleteCriteriaAndlinkedEvaluation(EvaluationCriteria evalCriteria){


	//Retrieve and delette all evaluations linked with this criteria
	List<IdeaEvaluation> iEvals = IdeaEvaluationLocalServiceUtil.getIdeaEvaluationsByCriteriaId(evalCriteria.getCriteriaId());
	
	for (IdeaEvaluation iEval:iEvals){
		
		try {
			IdeaEvaluationLocalServiceUtil.deleteIdeaEvaluation(iEval);
		} catch (SystemException e) {
			
			e.printStackTrace();
		}
	}
	
	
	//Delete the criteria
	try {
		EvaluationCriteriaLocalServiceUtil.deleteEvaluationCriteria(evalCriteria);
	} catch (SystemException e) {
		
		e.printStackTrace();
	}

	
}


/**
*  Evaluation of the Ideas on the Challenge
* @param actionRequest
* @param actionResponse
*/
public void evalIdea (ActionRequest actionRequest, ActionResponse actionResponse){
	
	User user = (User) actionRequest.getAttribute(WebKeys.USER);
	
	long ideaId  = ParamUtil.getLong(actionRequest,"evaluationIdeaId");
	
	boolean isEntityReference = IdeasListUtils.isEntityReferencebyIdeaId(ideaId, user);
	if (!isEntityReference){//Security issue
		SessionErrors.add(actionRequest, "error");
		return;
	}
	
	
	String finalMotivation  = ParamUtil.getString(actionRequest,"finalMotivation");
	boolean evaluationPassed  = ParamUtil.getBoolean(actionRequest,"accRej");
	
	
	List<EvaluationCriteria> bECs = IdeasListUtils.getBarrierEvaluationCriteriaByIdeaId(ideaId);
	List<EvaluationCriteria> qECs = IdeasListUtils.getQuantitativeEvaluationCriteriaByIdeaId(ideaId);
	
	
	//barrier criteria
	saveEvaluation (actionRequest,ideaId, true,bECs.size(), user.getUserId() );
	
	//quantitative criteria
	saveEvaluation (actionRequest,ideaId, false,qECs.size(), user.getUserId() );
	
	
	//update the data of the idea
	try {
		CLSIdea idea = CLSIdeaLocalServiceUtil.getIdeasByIdeaId(ideaId);
		idea.setFinalMotivation(finalMotivation);
		idea.setEvaluationPassed(evaluationPassed);
		CLSIdeaLocalServiceUtil.updateCLSIdea(idea);
		
		
	} catch (SystemException e) {
		
		e.printStackTrace();
	}
	
	
	
	 
}
	

/** Used by evalIdea in order to do the Evaluation of the Ideas on the Challenge
 * @param actionRequest
 * @param ideaId
 * @param isBarriera
 * @param numEC
 * @param isUpdate
 * @param authorId
 */
private void saveEvaluation (ActionRequest actionRequest, long ideaId , boolean isBarriera, int numEC, long authorId){
	
	String bq = "q";
	if (isBarriera)
		bq = "b";
	
	for (int i = 0; i<numEC; i++){
		 String bMotivation = ParamUtil.getString(actionRequest, bq+"t"+i); 
		 long criteriaId = ParamUtil.getLong(actionRequest, bq+"cId"+i); 
		 
		 boolean isPassed = ParamUtil.getBoolean(actionRequest, bq+"c"+i);
		 double score = ParamUtil.getDouble(actionRequest, bq+"s"+i);

		 
		 IdeaEvaluation existentEC = null;
		try {
			existentEC = IdeaEvaluationLocalServiceUtil.fetchIdeaEvaluation(new IdeaEvaluationPK( criteriaId, ideaId));
		} catch (SystemException e1) {
			
			e1.printStackTrace();
		}
		 
		
		 if (Validator.isNotNull(existentEC)){ //isUpdate = true;
			 
			 IdeaEvaluation ideaEval = IdeaEvaluationLocalServiceUtil.getIdeaEvaluationByCriteriaIdAndIdeaId(criteriaId, ideaId);
			 ideaEval.setMotivation(bMotivation);
			 ideaEval.setPassed(isPassed);
			 ideaEval.setScore(score);
			 try {
				IdeaEvaluationLocalServiceUtil.updateIdeaEvaluation(ideaEval);
			} catch (SystemException e) {
				
				e.printStackTrace();
			}
		 }else{
			 
			 try {
					 IdeaEvaluation nie = IdeaEvaluationLocalServiceUtil.createIdeaEvaluation(new IdeaEvaluationPK( criteriaId, ideaId));
					 nie.setMotivation(bMotivation);
					 nie.setPassed(isPassed);
					 nie.setScore(score);
					 nie.setAuthorId(authorId);
					 nie.setDate(new Date());
			
					 IdeaEvaluationLocalServiceUtil.addIdeaEvaluation(nie);
					 
			} catch (SystemException e) {
				e.printStackTrace();
			}
		 }
	}
}


/**
 * Close the openEvaluation process and send all notifications. 
 * @param actionRequest
 * @param actionResponse
 */
public void openEvaluation (ActionRequest actionRequest, ActionResponse actionResponse){
	
	 long challengeId = ParamUtil.getLong(actionRequest, "challengeId");
	 
	 User user = (User) actionRequest.getAttribute(WebKeys.USER);
	 boolean isAuthor = ChallengesUtils.isAuthorByChallengeId(challengeId, user);
		if (!isAuthor){//Security issue
			SessionErrors.add(actionRequest, "error");
			return;
	}
	 
	 ResourceBundle res = MyUtils.getResBundleViaPilotByAction(actionRequest);

	
		List<CLSIdea> ideas = new ArrayList<CLSIdea>();
		try {
			 ideas = CLSIdeaLocalServiceUtil.getIdeasByChallengeId(challengeId);
		} catch (SystemException e) {
			
			e.printStackTrace();
		}
		
		
		//update idea with evaluation state
		for (CLSIdea idea:ideas ){
			
			//update idea model
			updateIdeaStatus(idea, MyConstants.IDEA_STATE_EVALUATION);
			
			//send notification
			CLSIdeaLocalServiceUtil.sendNotificationForTransition(res,idea.getIdeaID(), MyConstants.IDEA_STATE_EVALUATION, actionRequest);
		}
		
		updateChallengeStatus(challengeId, MyConstants.CHALLENGE_STATE_EVALUATION);
		
		ChallengesUtils.redirectToChallenge(actionRequest, actionResponse, challengeId);
		
	
}




/**
 * Close the evaluation process and send all notifications. 
 * @param actionRequest
 * @param actionResponse
 */
public void closeEvaluation (ActionRequest actionRequest, ActionResponse actionResponse){
	
	 long challengeId = ParamUtil.getLong(actionRequest, "challengeId");
	 
	 User currentUser = (User) actionRequest.getAttribute(WebKeys.USER);
	 boolean isAuthor = ChallengesUtils.isAuthorByChallengeId(challengeId, currentUser);
	 if (!isAuthor){//Security issue
		SessionErrors.add(actionRequest, "error");
		return;
	 }
	 
	 
	 ResourceBundle res = MyUtils.getResBundleViaPilotByAction(actionRequest);

	
		List<CLSIdea> ideas = new ArrayList<CLSIdea>();
		try {
			 ideas = CLSIdeaLocalServiceUtil.getIdeasByChallengeId(challengeId);
		} catch (SystemException e) {
			
			e.printStackTrace();
		}
		
		for (CLSIdea idea:ideas ){
			
			if (idea.getEvaluationPassed()){
				updateIdeaStatus(idea, MyConstants.IDEA_STATE_SELECTED);
				CLSIdeaLocalServiceUtil.sendNotificationForTransition(res,idea.getIdeaID(), MyConstants.IDEA_STATE_SELECTED, actionRequest);
			}else{
				updateIdeaStatus(idea, MyConstants.IDEA_STATE_REJECT);
				CLSIdeaLocalServiceUtil.sendNotificationForTransition(res,idea.getIdeaID(), MyConstants.IDEA_STATE_REJECT, actionRequest);
			
			}
			
			
		}
	
		updateChallengeStatus(challengeId, MyConstants.CHALLENGE_STATE_EVALUATED);
		
		
		ChallengesUtils.redirectToChallenge(actionRequest, actionResponse, challengeId);
}


/**
 * @param challengeId
 * @param status
 */
private void updateIdeaStatus (CLSIdea idea, String status){
	
	idea.setIdeaStatus(status);
	
	try {
		CLSIdeaLocalServiceUtil.updateCLSIdea(idea);
	} catch (SystemException e) {
		
		e.printStackTrace();
	}
}



/**
 * @param challengeId
 * @param status
 */
private void updateChallengeStatus (long challengeId, String status){
	
	CLSChallenge chall = null;
	try {
		 chall = CLSChallengeLocalServiceUtil.getCLSChallenge(challengeId);
	} catch (PortalException | SystemException e) {
		e.printStackTrace();
	}
	
	chall.setChallengeStatus(status);
	
	try {
		CLSChallengeLocalServiceUtil.updateCLSChallenge(chall);
	} catch (SystemException e) {
		
		e.printStackTrace();
	}
}


	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)  throws IOException, PortletException {  
//		
//		try{
//			OutputStream outStream;	   
//			String jsonRet = "";
//			
			// rimuove un'idea dai preferiti
//			if (resourceRequest.getResourceID().equals("removeFavouriteChallenge")) {	
//				user = (User) resourceRequest.getAttribute(WebKeys.USER);
//				Long favChallengeId = ParamUtil.getLong(resourceRequest,"favChallengeId");
//				// inserisco all'elenco delle idee preferite 
//				jsonRet = String.valueOf( removeFavouriteChallenge(favChallengeId,user.getUserId())) ;
//				
//				outStream = resourceResponse.getPortletOutputStream();
//	            outStream.write(jsonRet.getBytes(Charset.forName("UTF-8")));
//			}
//			
//			// aggiunge un'idea ai preferiti
//			if (resourceRequest.getResourceID().equals("addFavouriteChallenge")) {	
//				user = (User) resourceRequest.getAttribute(WebKeys.USER);
//				Long favChallengeId = ParamUtil.getLong(resourceRequest,"favChallengeId");
//				// inserisco all'elenco delle idee preferite 
//				jsonRet = String.valueOf( addFavouriteChallenge(favChallengeId, user.getUserId())) ;
//				
//				outStream = resourceResponse.getPortletOutputStream();
//	            outStream.write(jsonRet.getBytes(Charset.forName("UTF-8")));
//			}
//			
//			// ottengo l'elenco delle idee preferite
//			if (resourceRequest.getResourceID().equals("getFavouriteChallenge")) {
//					user = (User) resourceRequest.getAttribute(WebKeys.USER);
//					Long favChallengeId = ParamUtil.getLong(resourceRequest,"favChallengeId");
//
//					// ricavo l'elenco delle idee preferite 
//					jsonRet = getFavouriteChallenges(favChallengeId, user.getUserId());
//					
//					outStream = resourceResponse.getPortletOutputStream();
//		            outStream.write(jsonRet.getBytes(Charset.forName("UTF-8")));
//			}
//			
//		
//				
//		}catch (Exception e) {
//			e.printStackTrace();
//			
//		}
	}
	
	public void deleteChallengeFile(ActionRequest actionRequest, ActionResponse actionRresponse) throws PortletException, IOException {
		
		Map<String, String[]> paramMap = actionRequest.getParameterMap();
		Set<String> set = paramMap.keySet();
		Iterator<String> setIt = set.iterator();
		while (setIt.hasNext()) {
			String key = (String) setIt.next();
			String[] value = paramMap.get(key);
			for (int i = 0; i < value.length; i++) {
			}
		}
		
		
		Long fileEntryId = ParamUtil.getLong(actionRequest, "fileEntryId");
		try {
			DLFileEntryLocalServiceUtil.deleteDLFileEntry(fileEntryId);
		} catch (PortalException e) {
			
			e.printStackTrace();
		} catch (com.liferay.portal.kernel.exception.SystemException e) {
			
			e.printStackTrace();
		}

	}

	
	/**
	 * Delets a dir recursively deleting anything inside it.
	 * @param dir The dir to delete
	 * @return true if the dir was successfully deleted
	 */
	private boolean deleteDirectory(File dir) {
	    if(! dir.exists() || !dir.isDirectory())    {
	        return false;
	    }

	    String[] files = dir.list();
	    for(int i = 0, len = files.length; i < len; i++)    {
	        File f = new File(dir, files[i]);
	        if(f.isDirectory()) {
	            deleteDirectory(f);
	        }else   {	
	        	System.gc();//Added this part  
	            f.delete();	        	
	        }
	    }
	    return dir.delete();
	}
	
	
	private void setPermission(ActionRequest actionRequest, String resourceId, String resourceType,String[] actionIdWiew, java.util.List<Role> roles) throws com.liferay.portal.kernel.exception.SystemException, PortalException {
		//String[] actionIdsOwner =  {ActionKeys.VIEW, ActionKeys.};
		String[] actionIdAllFile =  {ActionKeys.VIEW,ActionKeys.ADD_DISCUSSION,ActionKeys.DELETE,ActionKeys.DELETE_DISCUSSION,ActionKeys.PERMISSIONS,ActionKeys.UPDATE,ActionKeys.UPDATE_DISCUSSION};
		String[] actionIdAllFolder =  {ActionKeys.VIEW,
				ActionKeys.ACCESS,
				ActionKeys.ADD_DOCUMENT,
				ActionKeys.ADD_SHORTCUT,
				ActionKeys.ADD_SUBFOLDER,
				ActionKeys.DELETE,
				ActionKeys.PERMISSIONS,
				ActionKeys.UPDATE};
		
		//List<Role> roles = RoleLocalServiceUtil.getRoles(com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS, com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS);
		Iterator<Role> rolesIt = roles.iterator();
		while(rolesIt.hasNext()){
			Role role = rolesIt.next();
			//ResourcePermissionLocalServiceUtil.setResourcePermissions(companyId, name, scope, primKey, roleId, actionIds)
			
			if(!role.getName().equals("Owner")){
				ResourcePermissionServiceUtil.setIndividualResourcePermissions(ParamUtil.getLong(actionRequest, "groupid"), //groupId
						   user.getCompanyId(), //companyId
						   resourceType, //name
						   resourceId, //primKey
						   role.getPrimaryKey(), //roleId
						   actionIdWiew);
			}
			
			
			final Role userRolsOwner = RoleLocalServiceUtil.getRole(user.getCompanyId(), RoleConstants.OWNER);
			
			if (resourceType.equals("com.liferay.portlet.documentlibrary.model.DLFileEntry")){
				ResourcePermissionServiceUtil.setIndividualResourcePermissions(ParamUtil.getLong(actionRequest, "groupid"), //groupId
						   user.getCompanyId(), //companyId
						   resourceType, //name
						   resourceId, //primKey
						   userRolsOwner.getPrimaryKey(), //roleId
						   actionIdAllFile);
			}else if (resourceType.equals("com.liferay.portlet.documentlibrary.model.DLFolder")){
				ResourcePermissionServiceUtil.setIndividualResourcePermissions(ParamUtil.getLong(actionRequest, "groupid"), //groupId
						   user.getCompanyId(), //companyId
						   resourceType, //name
						   resourceId, //primKey
						   userRolsOwner.getPrimaryKey(), //roleId
						   actionIdAllFolder);
			}
		}
		
		
	}
	
//	public void addFavouriteChallenge(ActionRequest actionRequest,
//			ActionResponse actionResponse) throws IOException, PortletException {
//
//		user = (User) actionRequest.getAttribute(WebKeys.USER);
//		Long challengeId = ParamUtil.getLong(actionRequest, "favChallengeId");// groupId	
//		
//		try {
//			
//			CLSFavouriteChallenges favChallenge = new CLSFavouriteChallengesImpl();
//			if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
//				logger.info(favChallenge.getClass().getName());
//			}
//			favChallenge.setChallengeId(challengeId);
//			favChallenge.setUserId(user.getUserId());			
//			CLSFavouriteChallengesLocalServiceUtil.addCLSFavouriteChallenges(favChallenge);
//		} catch (com.liferay.portal.kernel.exception.SystemException e) {
//			e.printStackTrace();
//			throw new PortletException(e.getMessage());
//		}
//		if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
//			logger.info("add favourite challenge");
//		}		
//	}
	
	/*
	private boolean removeFavouriteChallenge(Long favChallengeId, Long userId) throws  PortalException {
		boolean retVal = true;
		try {
			CLSFavouriteChallengesPK clsFavouriteChallengesPK = new CLSFavouriteChallengesPK(favChallengeId, userId);
			CLSFavouriteChallengesLocalServiceUtil.deleteCLSFavouriteChallenges(clsFavouriteChallengesPK);
			
		} catch (com.liferay.portal.kernel.exception.SystemException e) {
			retVal = false;
			e.printStackTrace();
		}
		if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
			logger.info("removed favourite idea --> "+retVal);
		}
		return retVal;
	}
	
	
	
	private boolean addFavouriteChallenge(Long favChallengeId, Long UserId) throws PortletException {
		boolean retVal = true;
		try {
			
			CLSFavouriteChallenges favChallenge = new CLSFavouriteChallengesImpl();
					
			favChallenge.setChallengeId(favChallengeId);
			favChallenge.setUserId(user.getUserId());			
			
			CLSFavouriteChallengesLocalServiceUtil.addCLSFavouriteChallenges(favChallenge);
			
			
			
		} catch (com.liferay.portal.kernel.exception.SystemException e) {
			retVal = false;
			e.printStackTrace();
			throw new PortletException(e.getMessage());
		}
		if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
			logger.info("added favourite idea --> "+retVal);
		}
		return retVal;
	}
	
	
	 private String getFavouriteChallenges(Long favChallengeId, Long userId) throws PortalException {		 	   
			
			if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
				logger.info("getFavouriteIdeas ideaId ==> " +favChallengeId);
			}	
			List favChallenges = new ArrayList();
			
			String retVal = "";
			try {			
				// verifico se l'idea e' tra i miei preferiti
				Criterion c_favIdea1 = RestrictionsFactoryUtil.eq("primaryKey.challengeId", favChallengeId);
				Criterion c_favIdea2 = RestrictionsFactoryUtil.eq("primaryKey.userId",userId);
				Criterion c_favIdea3 = RestrictionsFactoryUtil.and(c_favIdea1,c_favIdea2);
				
				DynamicQuery dqFavChallenge = DynamicQueryFactoryUtil.forClass(CLSFavouriteChallenges.class).add(c_favIdea3);
				
				
				
				favChallenges = CLSFavouriteChallengesLocalServiceUtil.dynamicQuery(dqFavChallenge);
				String json = new Gson().toJson(favChallenges );
				
				//CLSFavouriteIdeas fi = CLSFavouriteIdeasLocalServiceUtil.getCLSFavouriteIdeas(1);				
				//String json = new Gson().toJson(fi);
				if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
					System.out.println("JSONArray :: "+json);
				}
				retVal = json;
				
			} catch (com.liferay.portal.kernel.exception.SystemException e) {
				e.printStackTrace();				
			}
			
			if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
				logger.info("lst_favIdea "+favChallenges);
			}	
			
			return retVal;
	    }*/
	
}
