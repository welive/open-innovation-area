/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.impl;


import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSVmeProjectsImpl;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSVmeProjectsLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSIdeaServiceBaseImpl;
import it.eng.rspa.ideas.utils.MyConstants;
import it.eng.rspa.ideas.utils.MyUtils;

import java.util.GregorianCalendar;

import javax.xml.ws.WebServiceContext;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portlet.documentlibrary.NoSuchFolderException;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;

/**
 * The implementation of the c l s idea remote service. (servizi pubblici)
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSIdeaServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaServiceUtil
 */
public class CLSIdeaServiceImpl extends CLSIdeaServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaServiceUtil} to access the c l s idea remote service.
	 */
	
	@javax.annotation.Resource
    WebServiceContext ctx;
	
public JSONObject publishScreenShot(String body	){
	
		JSONObject jsonIn=null;
		try {
			jsonIn = JSONFactoryUtil.createJSONObject(body);
		} catch (JSONException e2) {
			e2.printStackTrace();
		}
		String idea_id = jsonIn.getString("idea_id");
		String encodedScreenShot =jsonIn.getString("encodedScreenShot");
		String description =jsonIn.getString("description");
		String imgType =jsonIn.getString("imgMimeType");
		String username =jsonIn.getString("username");
		
		JSONObject jsonResp = JSONFactoryUtil.createJSONObject();
		
		boolean utenteAbilitato=false;
		
		//check utente corrente
		try {
			
			utenteAbilitato = MyUtils.checkWsCredentials(this.getUser().getEmailAddress());
			
		} catch (PortalException | SystemException e1) {
			jsonResp.put("error",true);
            jsonResp.put("message","KO - ScreenShot NOT saved over Liferay. The problem is :"+e1.getMessage());
            e1.printStackTrace();
			  return jsonResp;
			
		}
		
		if (!utenteAbilitato){
			
			jsonResp.put("error",true);
            jsonResp.put("message","KO - ScreenShot NOT saved over Liferay. Unauthorized user ");
            return jsonResp;
			
		}
		
		
		
		
		
		try {
			//Recupero l'utente che ha fatto la richiesta
			ServiceContext sc = new ServiceContext();
			User u = UserLocalServiceUtil.getUserByEmailAddress(this.getUser().getCompanyId(), username); 
			
			//Decodifico l'immagine
			com.sun.org.apache.xml.internal.security.Init.init();
			byte[] data = com.sun.org.apache.xml.internal.security.utils.Base64.decode(encodedScreenShot);
			
			//Recupero la root Folder associata all'idea correlata allo screenshot corrente
			CLSIdea idea = CLSIdeaLocalServiceUtil.getCLSIdea(Long.parseLong(idea_id));
			Long folderId = idea.getIdFolder();
			DLFolder folder = DLFolderLocalServiceUtil.fetchDLFolder(folderId);
			
			//Recupero la sub-folder degli screenshot
			DLFolder screenshotsFolder;
			try { 
				screenshotsFolder = DLFolderLocalServiceUtil.getFolder(idea.getGroupId(), 
																		folder.getFolderId(), 
																		PortletProps.get("screenshot.foldername"));
			}
			catch(NoSuchFolderException e) {
				//Se la sub-folder degli screenshot non esiste, la creo
				screenshotsFolder = DLFolderLocalServiceUtil.addFolder(u.getUserId(), 
																		idea.getGroupId(), 
																		folder.getRepositoryId(), 
																		false, 
																		folder.getFolderId(),
																		PortletProps.get("screenshot.foldername"), 
																		"", 
																		false, 
																		sc);
			}
			//Salvo lo screenshot
			String filename = "Screenshot_"+new GregorianCalendar().getTimeInMillis()+"."+imgType.replace("image/", "");
			DLAppLocalServiceUtil.addFileEntry(u.getUserId(),
												screenshotsFolder.getRepositoryId(), 
												screenshotsFolder.getFolderId(),
												filename, 
												imgType, 
												filename,
												description, 
												"",
												data,
												sc);
			
			//Recupero la sub-sub-folder che contiene i thumbanils
			DLFolder thumbsFolder;
			try{ 
				thumbsFolder = DLFolderLocalServiceUtil.getFolder(idea.getGroupId(), 
																	screenshotsFolder.getFolderId(), 
																	PortletProps.get("screenshot.thumbnail.foldername")); 
			}
			catch(NoSuchFolderException e) {
				//Se la sub-sub-folder dei thubnails non esiste, la creo
				thumbsFolder = DLFolderLocalServiceUtil.addFolder(u.getUserId(), 
																	idea.getGroupId(), 
																	screenshotsFolder.getRepositoryId(), 
																	false, 
																	screenshotsFolder.getFolderId(),
																	PortletProps.get("screenshot.thumbnail.foldername"), 
																	"", 
																	false, 
																	sc);
			}
			//Creo il thumbnail
			data = MyUtils.createThumbnail(data, 
											MyConstants.SCREENSHOT_THUMB_SIZE,
											imgType);
			
			//Salvo il thumbnail dello screenshot
			DLAppLocalServiceUtil.addFileEntry(u.getUserId(),
												thumbsFolder.getRepositoryId(), 
												thumbsFolder.getFolderId(),
												filename, 
												imgType, 
												filename,
												description, 
												"",
												data,
												sc);
			
			
			
			jsonResp.put("error",false);
            jsonResp.put("message","OK - ScreenShot  saved over Liferay.");
			
		} catch (Exception e) {
			e.printStackTrace();
			
			jsonResp.put("error",true);
            jsonResp.put("message","KO - ScreenShot NOT saved over Liferay. The problem is :"+e.getMessage());
            e.printStackTrace();
			return jsonResp;
			
		}
		
		//Ritorno la risposta all'invoker
		return jsonResp;
		
	}

	
	@JSONWebService(value = "vc-project-update", method = "POST")
	public JSONObject vcProjectUpdate( String vmeprojectid, String vmeprojectname, boolean ismockup, String ideaid ) {
		
		System.out.println("IN vcProjectUpdate");
			
		JSONObject jsonResp = JSONFactoryUtil.createJSONObject();
		
		
		boolean utenteAbilitato=false;
		
		//check utente corrente
		try {
			utenteAbilitato = MyUtils.checkWsCredentials(this.getUser().getEmailAddress());
			
			
		} catch (PortalException | SystemException e1) {
			jsonResp.put("error",true);
            jsonResp.put("message","KO - VC Project NOT saved over Liferay. The problem is :"+e1.getMessage());
            e1.printStackTrace();
			  return jsonResp;
			
		}
		
		if (!utenteAbilitato){
			
			jsonResp.put("error",true);
            jsonResp.put("message","KO - VC Project NOT saved over Liferay. Unauthorized user ");
            return jsonResp;
			
		}
		
		
		String msgReturn =  new String(" Project updated ");
		boolean valid = false;
		//formato del JSON
		//{"VmeProjectId":"34", "isMockup": "false", "ideaId": "2801"}
		
//		 String VmeProjectId =	inputJSON.getString("VmeProjectId");
//		 String VmeProjectName =	inputJSON.getString("VmeProjectName");
//		 String isMockupS =	inputJSON.getString("isMockup");
//		 boolean isMockup = Boolean.parseBoolean(isMockupS);
//		 String ideaId =	inputJSON.getString("ideaId");
		 
		try {
			
			 //validazione parametri
			 if (Validator.isNull(vmeprojectid)){
				 msgReturn="VmeProjectId mandatory.";
				 valid = false;
				 
			 }else if(Validator.isNull(vmeprojectname)){
				 msgReturn="VmeProjectName mandatory.";
				 valid = false;
				 
				 
			 }else if(Validator.isNull(ismockup)){
				 msgReturn="isMockup mandatory.";
				 valid = false;
				 
			 }else if(Validator.isNull(ideaid)){
				 msgReturn="ideaId mandatory.";
				 valid = false;
			 }else{
				 valid = true;
			 }
			
			
			
		 } catch (Exception e) {
			 jsonResp.put("error",true);
             jsonResp.put("message","KO - VC Project NOT saved over Liferay. The problem is :"+e.getMessage());
             e.printStackTrace();
			  return jsonResp;
		}
		
		
		if(valid == true){
		 
			 CLSVmeProjects vmeProject = new  CLSVmeProjectsImpl();
			 
			 try {
				vmeProject.setRecordId(CounterLocalServiceUtil.increment());
			} catch (SystemException e) {
				
				jsonResp.put("error",true);
	             jsonResp.put("message","KO - VC Project NOT saved over Liferay. The problem is :"+e.getMessage());
	             e.printStackTrace();
				  return jsonResp;
			}
			 vmeProject.setVmeProjectId(Long.parseLong(vmeprojectid));
			 vmeProject.setVmeProjectName(vmeprojectname);
			 vmeProject.setIsMockup(ismockup);
			 vmeProject.setIdeaId(Long.parseLong(ideaid));
			 try {
				CLSVmeProjectsLocalServiceUtil.addCLSVmeProjects(vmeProject);
			} catch (SystemException e) {
				
				jsonResp.put("error",true);
	             jsonResp.put("message","KO - VC Project NOT saved over Liferay. The problem is :"+e.getMessage());
	             e.printStackTrace();
				  return jsonResp;
			}
			
			 jsonResp.put("error",false);
       	  	jsonResp.put("message","OK - VC Project saved on Liferay");
			 
			 
		}else{
			 
			 jsonResp.put("error",true);
            jsonResp.put("message","KO - VC Project NOT saved over Liferay. The problem is: "+msgReturn);
			 
		 }
				
		return jsonResp;
	}
	
	
	
	
	
	/**
	 * @param fileEntryId
	 * 
	 * questo metodo viene utilizzato per eliminare gli allegati con Ajax
	 */
	public void deleteIdeaFile(long fileEntryId)  {
		
			try {
				DLFileEntryLocalServiceUtil.deleteDLFileEntry(fileEntryId);
			} catch (PortalException | SystemException e) {
				
				e.printStackTrace();
			}
		
		
	}
	
	
	
}