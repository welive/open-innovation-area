/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing CLSCategoriesSetForChallenge in entity cache.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSCategoriesSetForChallenge
 * @generated
 */
public class CLSCategoriesSetForChallengeCacheModel implements CacheModel<CLSCategoriesSetForChallenge>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{categoriesSetID=");
		sb.append(categoriesSetID);
		sb.append(", challengeId=");
		sb.append(challengeId);
		sb.append(", type=");
		sb.append(type);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CLSCategoriesSetForChallenge toEntityModel() {
		CLSCategoriesSetForChallengeImpl clsCategoriesSetForChallengeImpl = new CLSCategoriesSetForChallengeImpl();

		clsCategoriesSetForChallengeImpl.setCategoriesSetID(categoriesSetID);
		clsCategoriesSetForChallengeImpl.setChallengeId(challengeId);
		clsCategoriesSetForChallengeImpl.setType(type);

		clsCategoriesSetForChallengeImpl.resetOriginalValues();

		return clsCategoriesSetForChallengeImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		categoriesSetID = objectInput.readLong();
		challengeId = objectInput.readLong();
		type = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(categoriesSetID);
		objectOutput.writeLong(challengeId);
		objectOutput.writeInt(type);
	}

	public long categoriesSetID;
	public long challengeId;
	public int type;
}