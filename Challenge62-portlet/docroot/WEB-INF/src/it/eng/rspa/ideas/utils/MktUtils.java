package it.eng.rspa.ideas.utils;

import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.service.CategoriaLocalServiceUtil;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.util.portlet.PortletProps;


public class MktUtils {
	
	
 public static String getDefaultMktIconByCategoryName (String catname){
	
	
	   String imgUrl = "/img/marketplace/";
	   
	   if(catname.equalsIgnoreCase(MyConstants.MKT_CAT_REST))
	    imgUrl += "icone_servizi_rest-01.png";
	   else if(catname.equalsIgnoreCase(MyConstants.MKT_CAT_SOAP))
	    imgUrl += "icone_servizi_soap-01.png";
	   else if(catname.equalsIgnoreCase(MyConstants.MKT_CAT_DATASET))
	    imgUrl += "icone_dataset-01.png";
	   else if(catname.equalsIgnoreCase(MyConstants.MKT_CAT_APPMOBILE))
	    imgUrl += "icone_app_android-01.png";
	   else if(catname.equalsIgnoreCase(MyConstants.MKT_CAT_APPWEB))
	    imgUrl += "icone_app_web-01.png";
	   else
	    imgUrl += "defaultIcon.png";
	   
	   
	   return imgUrl;
	   
 
	  }

 /**
 * @param artefatto
 * @return
 */
public static String getCategoryNameByArtefact (Artefatto artefatto){
 
 
	//Recupero la categoria dell'artefatto
	 String catname = "Other";
	 try {
		catname = CategoriaLocalServiceUtil.getCategoria(artefatto.getCategoriamkpId()).getNomeCategoria();
	} catch (PortalException | SystemException e) {
				
		System.out.println("Exception on  getCategoryNameByArtefact "+ e.getMessage());
		return catname;
	} 
	
	 return catname;
	 
 }
 

/**
 * @param artefatto
 * @return
 */
public static String getDefaultMktIconByArtefact(Artefatto artefatto){
	
	String categName = getCategoryNameByArtefact(artefatto);
	String mkticon = getDefaultMktIconByCategoryName(categName);
	
	return mkticon;
}
 




}
