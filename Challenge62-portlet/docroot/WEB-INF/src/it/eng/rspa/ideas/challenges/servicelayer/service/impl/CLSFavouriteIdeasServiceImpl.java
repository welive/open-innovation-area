/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.impl;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteIdeasLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSFavouriteIdeasServiceBaseImpl;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;


/**
 * The implementation of the c l s favourite ideas remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteIdeasService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSFavouriteIdeasServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteIdeasServiceUtil
 */
public class CLSFavouriteIdeasServiceImpl
	extends CLSFavouriteIdeasServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteIdeasServiceUtil} to access the c l s favourite ideas remote service.
	 */
	
	
	 
	 public List<CLSFavouriteIdeas> getFavouriteIdeas(Long ideaId, Long userId) throws SystemException {		 	   
		 	return CLSFavouriteIdeasLocalServiceUtil.getFavouriteIdeas(ideaId, userId);
	 }
	 
	 public boolean addFavouriteIdea(Long ideaId, Long userId) throws SystemException {
		 return CLSFavouriteIdeasLocalServiceUtil.addFavouriteIdea(ideaId, userId);
	 
	 }
	 
//	 public boolean removeFavouriteIdea(Long favideaId) throws  SystemException {
//		 return CLSFavouriteIdeasLocalServiceUtil.removeFavouriteIdea(favideaId);
//	 }
//	 
	 public boolean removeFavouriteIdea(Long favIdeaId, Long userId) throws  SystemException {
		 return CLSFavouriteIdeasLocalServiceUtil.removeFavouriteIdea(favIdeaId, userId);
	 } 
}