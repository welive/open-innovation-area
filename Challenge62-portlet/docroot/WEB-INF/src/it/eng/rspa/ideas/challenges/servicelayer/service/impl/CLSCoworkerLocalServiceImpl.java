/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.impl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker;
import it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSCoworkerLocalServiceBaseImpl;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSCoworkerUtil;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the c l s coworker local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.ideas.challenges.servicelayer.service.CLSCoworkerLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSCoworkerLocalServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.CLSCoworkerLocalServiceUtil
 */
public class CLSCoworkerLocalServiceImpl extends CLSCoworkerLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.ideas.challenges.servicelayer.service.CLSCoworkerLocalServiceUtil} to access the c l s coworker local service.
	 */
	
	public void removeCoworkerOnIdeaDelete(long ideaId){
		try{
			List<CLSCoworker> coworkersIdea = CLSCoworkerUtil.findByIdeaID(ideaId);
			for(CLSCoworker coworker : coworkersIdea) {
				try{ CLSCoworkerUtil.remove(coworker.getCoworkerId()); }
				catch(Exception e){ System.out.println(e.getMessage()); }
			}
		}
		catch(Exception e){ System.out.println(e.getMessage()); }
	}
	
	public CLSCoworker getCoworkerByIdeaIdAndUserId(long ideaId, long userId) throws SystemException{
		try {
			return CLSCoworkerUtil.findByIdeaIDAndUserID(ideaId, userId);
		} catch (NoSuchCLSCoworkerException e) {
			//e.printStackTrace();
			return null;
		}
	}
	
	public List<CLSCoworker> getCoworkersByIdeaId(long ideaId) throws SystemException{
		return CLSCoworkerUtil.findByIdeaID(ideaId);
	}
	
	public List<CLSCoworker> getCoworkersByUserId(long userId) throws SystemException{
		return CLSCoworkerUtil.findByUserID(userId);
	}
}