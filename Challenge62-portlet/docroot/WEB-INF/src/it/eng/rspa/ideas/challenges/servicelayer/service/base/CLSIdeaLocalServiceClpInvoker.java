/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.base;

import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;

import java.util.Arrays;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 * @generated
 */
public class CLSIdeaLocalServiceClpInvoker {
	public CLSIdeaLocalServiceClpInvoker() {
		_methodName0 = "addCLSIdea";

		_methodParameterTypes0 = new String[] {
				"it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea"
			};

		_methodName1 = "createCLSIdea";

		_methodParameterTypes1 = new String[] { "long" };

		_methodName2 = "deleteCLSIdea";

		_methodParameterTypes2 = new String[] { "long" };

		_methodName3 = "deleteCLSIdea";

		_methodParameterTypes3 = new String[] {
				"it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea"
			};

		_methodName4 = "dynamicQuery";

		_methodParameterTypes4 = new String[] {  };

		_methodName5 = "dynamicQuery";

		_methodParameterTypes5 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
			};

		_methodName6 = "dynamicQuery";

		_methodParameterTypes6 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
			};

		_methodName7 = "dynamicQuery";

		_methodParameterTypes7 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
				"com.liferay.portal.kernel.util.OrderByComparator"
			};

		_methodName8 = "dynamicQueryCount";

		_methodParameterTypes8 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
			};

		_methodName9 = "dynamicQueryCount";

		_methodParameterTypes9 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery",
				"com.liferay.portal.kernel.dao.orm.Projection"
			};

		_methodName10 = "fetchCLSIdea";

		_methodParameterTypes10 = new String[] { "long" };

		_methodName11 = "fetchCLSIdeaByUuidAndCompanyId";

		_methodParameterTypes11 = new String[] { "java.lang.String", "long" };

		_methodName12 = "fetchCLSIdeaByUuidAndGroupId";

		_methodParameterTypes12 = new String[] { "java.lang.String", "long" };

		_methodName13 = "getCLSIdea";

		_methodParameterTypes13 = new String[] { "long" };

		_methodName14 = "getPersistedModel";

		_methodParameterTypes14 = new String[] { "java.io.Serializable" };

		_methodName15 = "getCLSIdeaByUuidAndCompanyId";

		_methodParameterTypes15 = new String[] { "java.lang.String", "long" };

		_methodName16 = "getCLSIdeaByUuidAndGroupId";

		_methodParameterTypes16 = new String[] { "java.lang.String", "long" };

		_methodName17 = "getCLSIdeas";

		_methodParameterTypes17 = new String[] { "int", "int" };

		_methodName18 = "getCLSIdeasCount";

		_methodParameterTypes18 = new String[] {  };

		_methodName19 = "updateCLSIdea";

		_methodParameterTypes19 = new String[] {
				"it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea"
			};

		_methodName160 = "getBeanIdentifier";

		_methodParameterTypes160 = new String[] {  };

		_methodName161 = "setBeanIdentifier";

		_methodParameterTypes161 = new String[] { "java.lang.String" };

		_methodName166 = "sendNotificationForTransition";

		_methodParameterTypes166 = new String[] {
				"java.util.ResourceBundle", "long", "java.lang.String",
				"javax.portlet.ActionRequest"
			};

		_methodName167 = "sendNotification";

		_methodParameterTypes167 = new String[] {
				"java.util.ResourceBundle", "java.lang.String", "long",
				"java.lang.String", "long", "javax.portlet.ActionRequest",
				"java.lang.String"
			};

		_methodName168 = "getIdeasByUserId";

		_methodParameterTypes168 = new String[] { "long" };

		_methodName169 = "getIdeasByIdeaId";

		_methodParameterTypes169 = new String[] { "long" };

		_methodName170 = "getIdeasByChallengeId";

		_methodParameterTypes170 = new String[] { "long" };

		_methodName171 = "getIdeasByMunicipalityId";

		_methodParameterTypes171 = new String[] { "long" };

		_methodName172 = "getIdeasByMunicipalityOrganizationId";

		_methodParameterTypes172 = new String[] { "long" };

		_methodName173 = "getIdeasByNeedId";

		_methodParameterTypes173 = new String[] { "long" };

		_methodName174 = "getNeeds";

		_methodParameterTypes174 = new String[] {  };

		_methodName175 = "getNeedsByMunicipalityOrganizationId";

		_methodParameterTypes175 = new String[] { "long" };

		_methodName176 = "getOnlyIdeasByMunicipalityOrganizationId";

		_methodParameterTypes176 = new String[] { "long" };

		_methodName177 = "getNeedsByMunicipalityId";

		_methodParameterTypes177 = new String[] { "long" };

		_methodName178 = "getOnlyIdeasByMunicipalityId";

		_methodParameterTypes178 = new String[] { "long" };

		_methodName179 = "getNeedsByUserId";

		_methodParameterTypes179 = new String[] { "long" };

		_methodName180 = "getOnlyIdeas";

		_methodParameterTypes180 = new String[] {  };

		_methodName181 = "getOnlyIdeasByUserId";

		_methodParameterTypes181 = new String[] { "long" };

		_methodName182 = "addCoworker";

		_methodParameterTypes182 = new String[] {
				"long", "com.liferay.portal.model.User"
			};

		_methodName183 = "doRightToBeForgottenIMS";

		_methodParameterTypes183 = new String[] {
				"long", "boolean", "javax.portlet.ActionRequest"
			};

		_methodName184 = "getTopRatedIdeasOrNeedsByPilot";

		_methodParameterTypes184 = new String[] {
				"java.lang.String", "boolean", "int", "java.util.Locale"
			};

		_methodName185 = "getTopRatedIdeasOrNeedsByLanguage";

		_methodParameterTypes185 = new String[] {
				"java.lang.String", "boolean", "int", "java.util.Locale"
			};

		_methodName186 = "getIdeasOrNeedsByUserId";

		_methodParameterTypes186 = new String[] {
				"long", "boolean", "int", "java.util.Locale"
			};

		_methodName187 = "updateStatus";

		_methodParameterTypes187 = new String[] {
				"long", "long", "int",
				"com.liferay.portal.service.ServiceContext"
			};

		_methodName188 = "getIMSProperties";

		_methodParameterTypes188 = new String[] { "java.lang.String" };
	}

	public Object invokeMethod(String name, String[] parameterTypes,
		Object[] arguments) throws Throwable {
		if (_methodName0.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.addCLSIdea((it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea)arguments[0]);
		}

		if (_methodName1.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.createCLSIdea(((Long)arguments[0]).longValue());
		}

		if (_methodName2.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.deleteCLSIdea(((Long)arguments[0]).longValue());
		}

		if (_methodName3.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.deleteCLSIdea((it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea)arguments[0]);
		}

		if (_methodName4.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.dynamicQuery();
		}

		if (_methodName5.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0]);
		}

		if (_methodName6.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue());
		}

		if (_methodName7.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue(),
				(com.liferay.portal.kernel.util.OrderByComparator)arguments[3]);
		}

		if (_methodName8.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0]);
		}

		if (_methodName9.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				(com.liferay.portal.kernel.dao.orm.Projection)arguments[1]);
		}

		if (_methodName10.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.fetchCLSIdea(((Long)arguments[0]).longValue());
		}

		if (_methodName11.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.fetchCLSIdeaByUuidAndCompanyId((java.lang.String)arguments[0],
				((Long)arguments[1]).longValue());
		}

		if (_methodName12.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.fetchCLSIdeaByUuidAndGroupId((java.lang.String)arguments[0],
				((Long)arguments[1]).longValue());
		}

		if (_methodName13.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getCLSIdea(((Long)arguments[0]).longValue());
		}

		if (_methodName14.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getPersistedModel((java.io.Serializable)arguments[0]);
		}

		if (_methodName15.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getCLSIdeaByUuidAndCompanyId((java.lang.String)arguments[0],
				((Long)arguments[1]).longValue());
		}

		if (_methodName16.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes16, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getCLSIdeaByUuidAndGroupId((java.lang.String)arguments[0],
				((Long)arguments[1]).longValue());
		}

		if (_methodName17.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes17, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getCLSIdeas(((Integer)arguments[0]).intValue(),
				((Integer)arguments[1]).intValue());
		}

		if (_methodName18.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes18, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getCLSIdeasCount();
		}

		if (_methodName19.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes19, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.updateCLSIdea((it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea)arguments[0]);
		}

		if (_methodName160.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes160, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getBeanIdentifier();
		}

		if (_methodName161.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes161, parameterTypes)) {
			CLSIdeaLocalServiceUtil.setBeanIdentifier((java.lang.String)arguments[0]);

			return null;
		}

		if (_methodName166.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes166, parameterTypes)) {
			CLSIdeaLocalServiceUtil.sendNotificationForTransition((java.util.ResourceBundle)arguments[0],
				((Long)arguments[1]).longValue(),
				(java.lang.String)arguments[2],
				(javax.portlet.ActionRequest)arguments[3]);

			return null;
		}

		if (_methodName167.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes167, parameterTypes)) {
			CLSIdeaLocalServiceUtil.sendNotification((java.util.ResourceBundle)arguments[0],
				(java.lang.String)arguments[1],
				((Long)arguments[2]).longValue(),
				(java.lang.String)arguments[3],
				((Long)arguments[4]).longValue(),
				(javax.portlet.ActionRequest)arguments[5],
				(java.lang.String)arguments[6]);

			return null;
		}

		if (_methodName168.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes168, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getIdeasByUserId(((Long)arguments[0]).longValue());
		}

		if (_methodName169.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes169, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getIdeasByIdeaId(((Long)arguments[0]).longValue());
		}

		if (_methodName170.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes170, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getIdeasByChallengeId(((Long)arguments[0]).longValue());
		}

		if (_methodName171.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes171, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getIdeasByMunicipalityId(((Long)arguments[0]).longValue());
		}

		if (_methodName172.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes172, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getIdeasByMunicipalityOrganizationId(((Long)arguments[0]).longValue());
		}

		if (_methodName173.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes173, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getIdeasByNeedId(((Long)arguments[0]).longValue());
		}

		if (_methodName174.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes174, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getNeeds();
		}

		if (_methodName175.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes175, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getNeedsByMunicipalityOrganizationId(((Long)arguments[0]).longValue());
		}

		if (_methodName176.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes176, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getOnlyIdeasByMunicipalityOrganizationId(((Long)arguments[0]).longValue());
		}

		if (_methodName177.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes177, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getNeedsByMunicipalityId(((Long)arguments[0]).longValue());
		}

		if (_methodName178.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes178, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getOnlyIdeasByMunicipalityId(((Long)arguments[0]).longValue());
		}

		if (_methodName179.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes179, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getNeedsByUserId(((Long)arguments[0]).longValue());
		}

		if (_methodName180.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes180, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getOnlyIdeas();
		}

		if (_methodName181.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes181, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getOnlyIdeasByUserId(((Long)arguments[0]).longValue());
		}

		if (_methodName182.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes182, parameterTypes)) {
			CLSIdeaLocalServiceUtil.addCoworker(((Long)arguments[0]).longValue(),
				(com.liferay.portal.model.User)arguments[1]);

			return null;
		}

		if (_methodName183.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes183, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.doRightToBeForgottenIMS(((Long)arguments[0]).longValue(),
				((Boolean)arguments[1]).booleanValue(),
				(javax.portlet.ActionRequest)arguments[2]);
		}

		if (_methodName184.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes184, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getTopRatedIdeasOrNeedsByPilot((java.lang.String)arguments[0],
				((Boolean)arguments[1]).booleanValue(),
				((Integer)arguments[2]).intValue(),
				(java.util.Locale)arguments[3]);
		}

		if (_methodName185.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes185, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getTopRatedIdeasOrNeedsByLanguage((java.lang.String)arguments[0],
				((Boolean)arguments[1]).booleanValue(),
				((Integer)arguments[2]).intValue(),
				(java.util.Locale)arguments[3]);
		}

		if (_methodName186.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes186, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getIdeasOrNeedsByUserId(((Long)arguments[0]).longValue(),
				((Boolean)arguments[1]).booleanValue(),
				((Integer)arguments[2]).intValue(),
				(java.util.Locale)arguments[3]);
		}

		if (_methodName187.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes187, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.updateStatus(((Long)arguments[0]).longValue(),
				((Long)arguments[1]).longValue(),
				((Integer)arguments[2]).intValue(),
				(com.liferay.portal.service.ServiceContext)arguments[3]);
		}

		if (_methodName188.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes188, parameterTypes)) {
			return CLSIdeaLocalServiceUtil.getIMSProperties((java.lang.String)arguments[0]);
		}

		throw new UnsupportedOperationException();
	}

	private String _methodName0;
	private String[] _methodParameterTypes0;
	private String _methodName1;
	private String[] _methodParameterTypes1;
	private String _methodName2;
	private String[] _methodParameterTypes2;
	private String _methodName3;
	private String[] _methodParameterTypes3;
	private String _methodName4;
	private String[] _methodParameterTypes4;
	private String _methodName5;
	private String[] _methodParameterTypes5;
	private String _methodName6;
	private String[] _methodParameterTypes6;
	private String _methodName7;
	private String[] _methodParameterTypes7;
	private String _methodName8;
	private String[] _methodParameterTypes8;
	private String _methodName9;
	private String[] _methodParameterTypes9;
	private String _methodName10;
	private String[] _methodParameterTypes10;
	private String _methodName11;
	private String[] _methodParameterTypes11;
	private String _methodName12;
	private String[] _methodParameterTypes12;
	private String _methodName13;
	private String[] _methodParameterTypes13;
	private String _methodName14;
	private String[] _methodParameterTypes14;
	private String _methodName15;
	private String[] _methodParameterTypes15;
	private String _methodName16;
	private String[] _methodParameterTypes16;
	private String _methodName17;
	private String[] _methodParameterTypes17;
	private String _methodName18;
	private String[] _methodParameterTypes18;
	private String _methodName19;
	private String[] _methodParameterTypes19;
	private String _methodName160;
	private String[] _methodParameterTypes160;
	private String _methodName161;
	private String[] _methodParameterTypes161;
	private String _methodName166;
	private String[] _methodParameterTypes166;
	private String _methodName167;
	private String[] _methodParameterTypes167;
	private String _methodName168;
	private String[] _methodParameterTypes168;
	private String _methodName169;
	private String[] _methodParameterTypes169;
	private String _methodName170;
	private String[] _methodParameterTypes170;
	private String _methodName171;
	private String[] _methodParameterTypes171;
	private String _methodName172;
	private String[] _methodParameterTypes172;
	private String _methodName173;
	private String[] _methodParameterTypes173;
	private String _methodName174;
	private String[] _methodParameterTypes174;
	private String _methodName175;
	private String[] _methodParameterTypes175;
	private String _methodName176;
	private String[] _methodParameterTypes176;
	private String _methodName177;
	private String[] _methodParameterTypes177;
	private String _methodName178;
	private String[] _methodParameterTypes178;
	private String _methodName179;
	private String[] _methodParameterTypes179;
	private String _methodName180;
	private String[] _methodParameterTypes180;
	private String _methodName181;
	private String[] _methodParameterTypes181;
	private String _methodName182;
	private String[] _methodParameterTypes182;
	private String _methodName183;
	private String[] _methodParameterTypes183;
	private String _methodName184;
	private String[] _methodParameterTypes184;
	private String _methodName185;
	private String[] _methodParameterTypes185;
	private String _methodName186;
	private String[] _methodParameterTypes186;
	private String _methodName187;
	private String[] _methodParameterTypes187;
	private String _methodName188;
	private String[] _methodParameterTypes188;
}