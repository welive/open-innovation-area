/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetException;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the c l s categories set service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSCategoriesSetPersistence
 * @see CLSCategoriesSetUtil
 * @generated
 */
public class CLSCategoriesSetPersistenceImpl extends BasePersistenceImpl<CLSCategoriesSet>
	implements CLSCategoriesSetPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CLSCategoriesSetUtil} to access the c l s categories set persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CLSCategoriesSetImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CLSCategoriesSetModelImpl.ENTITY_CACHE_ENABLED,
			CLSCategoriesSetModelImpl.FINDER_CACHE_ENABLED,
			CLSCategoriesSetImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CLSCategoriesSetModelImpl.ENTITY_CACHE_ENABLED,
			CLSCategoriesSetModelImpl.FINDER_CACHE_ENABLED,
			CLSCategoriesSetImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CLSCategoriesSetModelImpl.ENTITY_CACHE_ENABLED,
			CLSCategoriesSetModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public CLSCategoriesSetPersistenceImpl() {
		setModelClass(CLSCategoriesSet.class);
	}

	/**
	 * Caches the c l s categories set in the entity cache if it is enabled.
	 *
	 * @param clsCategoriesSet the c l s categories set
	 */
	@Override
	public void cacheResult(CLSCategoriesSet clsCategoriesSet) {
		EntityCacheUtil.putResult(CLSCategoriesSetModelImpl.ENTITY_CACHE_ENABLED,
			CLSCategoriesSetImpl.class, clsCategoriesSet.getPrimaryKey(),
			clsCategoriesSet);

		clsCategoriesSet.resetOriginalValues();
	}

	/**
	 * Caches the c l s categories sets in the entity cache if it is enabled.
	 *
	 * @param clsCategoriesSets the c l s categories sets
	 */
	@Override
	public void cacheResult(List<CLSCategoriesSet> clsCategoriesSets) {
		for (CLSCategoriesSet clsCategoriesSet : clsCategoriesSets) {
			if (EntityCacheUtil.getResult(
						CLSCategoriesSetModelImpl.ENTITY_CACHE_ENABLED,
						CLSCategoriesSetImpl.class,
						clsCategoriesSet.getPrimaryKey()) == null) {
				cacheResult(clsCategoriesSet);
			}
			else {
				clsCategoriesSet.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all c l s categories sets.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CLSCategoriesSetImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CLSCategoriesSetImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the c l s categories set.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CLSCategoriesSet clsCategoriesSet) {
		EntityCacheUtil.removeResult(CLSCategoriesSetModelImpl.ENTITY_CACHE_ENABLED,
			CLSCategoriesSetImpl.class, clsCategoriesSet.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<CLSCategoriesSet> clsCategoriesSets) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CLSCategoriesSet clsCategoriesSet : clsCategoriesSets) {
			EntityCacheUtil.removeResult(CLSCategoriesSetModelImpl.ENTITY_CACHE_ENABLED,
				CLSCategoriesSetImpl.class, clsCategoriesSet.getPrimaryKey());
		}
	}

	/**
	 * Creates a new c l s categories set with the primary key. Does not add the c l s categories set to the database.
	 *
	 * @param categoriesSetID the primary key for the new c l s categories set
	 * @return the new c l s categories set
	 */
	@Override
	public CLSCategoriesSet create(long categoriesSetID) {
		CLSCategoriesSet clsCategoriesSet = new CLSCategoriesSetImpl();

		clsCategoriesSet.setNew(true);
		clsCategoriesSet.setPrimaryKey(categoriesSetID);

		return clsCategoriesSet;
	}

	/**
	 * Removes the c l s categories set with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param categoriesSetID the primary key of the c l s categories set
	 * @return the c l s categories set that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetException if a c l s categories set with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCategoriesSet remove(long categoriesSetID)
		throws NoSuchCLSCategoriesSetException, SystemException {
		return remove((Serializable)categoriesSetID);
	}

	/**
	 * Removes the c l s categories set with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the c l s categories set
	 * @return the c l s categories set that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetException if a c l s categories set with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCategoriesSet remove(Serializable primaryKey)
		throws NoSuchCLSCategoriesSetException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CLSCategoriesSet clsCategoriesSet = (CLSCategoriesSet)session.get(CLSCategoriesSetImpl.class,
					primaryKey);

			if (clsCategoriesSet == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCLSCategoriesSetException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(clsCategoriesSet);
		}
		catch (NoSuchCLSCategoriesSetException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CLSCategoriesSet removeImpl(CLSCategoriesSet clsCategoriesSet)
		throws SystemException {
		clsCategoriesSet = toUnwrappedModel(clsCategoriesSet);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(clsCategoriesSet)) {
				clsCategoriesSet = (CLSCategoriesSet)session.get(CLSCategoriesSetImpl.class,
						clsCategoriesSet.getPrimaryKeyObj());
			}

			if (clsCategoriesSet != null) {
				session.delete(clsCategoriesSet);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (clsCategoriesSet != null) {
			clearCache(clsCategoriesSet);
		}

		return clsCategoriesSet;
	}

	@Override
	public CLSCategoriesSet updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet clsCategoriesSet)
		throws SystemException {
		clsCategoriesSet = toUnwrappedModel(clsCategoriesSet);

		boolean isNew = clsCategoriesSet.isNew();

		Session session = null;

		try {
			session = openSession();

			if (clsCategoriesSet.isNew()) {
				session.save(clsCategoriesSet);

				clsCategoriesSet.setNew(false);
			}
			else {
				session.merge(clsCategoriesSet);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(CLSCategoriesSetModelImpl.ENTITY_CACHE_ENABLED,
			CLSCategoriesSetImpl.class, clsCategoriesSet.getPrimaryKey(),
			clsCategoriesSet);

		return clsCategoriesSet;
	}

	protected CLSCategoriesSet toUnwrappedModel(
		CLSCategoriesSet clsCategoriesSet) {
		if (clsCategoriesSet instanceof CLSCategoriesSetImpl) {
			return clsCategoriesSet;
		}

		CLSCategoriesSetImpl clsCategoriesSetImpl = new CLSCategoriesSetImpl();

		clsCategoriesSetImpl.setNew(clsCategoriesSet.isNew());
		clsCategoriesSetImpl.setPrimaryKey(clsCategoriesSet.getPrimaryKey());

		clsCategoriesSetImpl.setCategoriesSetID(clsCategoriesSet.getCategoriesSetID());

		return clsCategoriesSetImpl;
	}

	/**
	 * Returns the c l s categories set with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s categories set
	 * @return the c l s categories set
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetException if a c l s categories set with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCategoriesSet findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCLSCategoriesSetException, SystemException {
		CLSCategoriesSet clsCategoriesSet = fetchByPrimaryKey(primaryKey);

		if (clsCategoriesSet == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCLSCategoriesSetException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return clsCategoriesSet;
	}

	/**
	 * Returns the c l s categories set with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetException} if it could not be found.
	 *
	 * @param categoriesSetID the primary key of the c l s categories set
	 * @return the c l s categories set
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetException if a c l s categories set with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCategoriesSet findByPrimaryKey(long categoriesSetID)
		throws NoSuchCLSCategoriesSetException, SystemException {
		return findByPrimaryKey((Serializable)categoriesSetID);
	}

	/**
	 * Returns the c l s categories set with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s categories set
	 * @return the c l s categories set, or <code>null</code> if a c l s categories set with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCategoriesSet fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		CLSCategoriesSet clsCategoriesSet = (CLSCategoriesSet)EntityCacheUtil.getResult(CLSCategoriesSetModelImpl.ENTITY_CACHE_ENABLED,
				CLSCategoriesSetImpl.class, primaryKey);

		if (clsCategoriesSet == _nullCLSCategoriesSet) {
			return null;
		}

		if (clsCategoriesSet == null) {
			Session session = null;

			try {
				session = openSession();

				clsCategoriesSet = (CLSCategoriesSet)session.get(CLSCategoriesSetImpl.class,
						primaryKey);

				if (clsCategoriesSet != null) {
					cacheResult(clsCategoriesSet);
				}
				else {
					EntityCacheUtil.putResult(CLSCategoriesSetModelImpl.ENTITY_CACHE_ENABLED,
						CLSCategoriesSetImpl.class, primaryKey,
						_nullCLSCategoriesSet);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CLSCategoriesSetModelImpl.ENTITY_CACHE_ENABLED,
					CLSCategoriesSetImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return clsCategoriesSet;
	}

	/**
	 * Returns the c l s categories set with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param categoriesSetID the primary key of the c l s categories set
	 * @return the c l s categories set, or <code>null</code> if a c l s categories set with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCategoriesSet fetchByPrimaryKey(long categoriesSetID)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)categoriesSetID);
	}

	/**
	 * Returns all the c l s categories sets.
	 *
	 * @return the c l s categories sets
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSCategoriesSet> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s categories sets.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s categories sets
	 * @param end the upper bound of the range of c l s categories sets (not inclusive)
	 * @return the range of c l s categories sets
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSCategoriesSet> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s categories sets.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s categories sets
	 * @param end the upper bound of the range of c l s categories sets (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of c l s categories sets
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSCategoriesSet> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CLSCategoriesSet> list = (List<CLSCategoriesSet>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CLSCATEGORIESSET);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CLSCATEGORIESSET;

				if (pagination) {
					sql = sql.concat(CLSCategoriesSetModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CLSCategoriesSet>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSCategoriesSet>(list);
				}
				else {
					list = (List<CLSCategoriesSet>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the c l s categories sets from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (CLSCategoriesSet clsCategoriesSet : findAll()) {
			remove(clsCategoriesSet);
		}
	}

	/**
	 * Returns the number of c l s categories sets.
	 *
	 * @return the number of c l s categories sets
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CLSCATEGORIESSET);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the c l s categories set persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CLSCategoriesSet>> listenersList = new ArrayList<ModelListener<CLSCategoriesSet>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CLSCategoriesSet>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CLSCategoriesSetImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CLSCATEGORIESSET = "SELECT clsCategoriesSet FROM CLSCategoriesSet clsCategoriesSet";
	private static final String _SQL_COUNT_CLSCATEGORIESSET = "SELECT COUNT(clsCategoriesSet) FROM CLSCategoriesSet clsCategoriesSet";
	private static final String _ORDER_BY_ENTITY_ALIAS = "clsCategoriesSet.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CLSCategoriesSet exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CLSCategoriesSetPersistenceImpl.class);
	private static CLSCategoriesSet _nullCLSCategoriesSet = new CLSCategoriesSetImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CLSCategoriesSet> toCacheModel() {
				return _nullCLSCategoriesSetCacheModel;
			}
		};

	private static CacheModel<CLSCategoriesSet> _nullCLSCategoriesSetCacheModel = new CacheModel<CLSCategoriesSet>() {
			@Override
			public CLSCategoriesSet toEntityModel() {
				return _nullCLSCategoriesSet;
			}
		};
}