/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.impl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteChallengesImpl;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteChallengesLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSFavouriteChallengesLocalServiceBaseImpl;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSChallengeUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesUtil;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the c l s favourite challenges local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteChallengesLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSFavouriteChallengesLocalServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteChallengesLocalServiceUtil
 */
public class CLSFavouriteChallengesLocalServiceImpl
	extends CLSFavouriteChallengesLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteChallengesLocalServiceUtil} to access the c l s favourite challenges local service.
	 */
	
	public void removeFavouriteChallengesOnChallengeDelete(long challengeId) throws SystemException, NoSuchCLSFavouriteChallengesException{
		List<CLSFavouriteChallenges> favouriteChallenges = CLSFavouriteChallengesUtil.findByChallengeId(challengeId);
		
		for(CLSFavouriteChallenges favouriteChallenge : favouriteChallenges) {
			CLSFavouriteChallengesPK favouriteChallengesPK = new CLSFavouriteChallengesPK(favouriteChallenge.getChallengeId(), favouriteChallenge.getUserId());
			CLSFavouriteChallengesUtil.remove(favouriteChallengesPK);
		}
	}
	
	public List<CLSFavouriteChallenges> getFavouriteChallengesEntriesByChallengeId(long challengeId) throws SystemException{
		List<CLSFavouriteChallenges> favouriteChallenges = CLSFavouriteChallengesUtil.findByChallengeId(challengeId);
		return favouriteChallenges;
	}
	
	public List<CLSChallenge> getFavouriteChallengesByChallengeId(long challengeId) throws SystemException{
		List<CLSFavouriteChallenges> favouriteChallenges = CLSFavouriteChallengesUtil.findByChallengeId(challengeId);
		List<CLSChallenge> challenges = new ArrayList<CLSChallenge>();
		
		for(CLSFavouriteChallenges favouriteChallenge : favouriteChallenges) {
			challenges.add( CLSChallengeUtil.fetchByPrimaryKey(favouriteChallenge.getChallengeId()));
		}
		return challenges;
	}
	
	public List<CLSChallenge> getFavouriteChallengesByUserId(long userId) throws SystemException{
		List<CLSFavouriteChallenges> favouriteChallenges = CLSFavouriteChallengesUtil.findByUserID(userId);
		List<CLSChallenge> challenges = new ArrayList<CLSChallenge>();
		
		for(CLSFavouriteChallenges favouriteChallenge : favouriteChallenges) {
			challenges.add( CLSChallengeUtil.fetchByPrimaryKey(favouriteChallenge.getChallengeId()));
		}
		return challenges;
	}
	
	public boolean addFavouriteChallenge(Long favChallengeId, Long userId) throws  SystemException{
		boolean retVal = true;
		try {
			
			CLSFavouriteChallenges favChallenge = new CLSFavouriteChallengesImpl();
					
			favChallenge.setChallengeId(favChallengeId);
			favChallenge.setUserId(userId);			
			
			CLSFavouriteChallengesLocalServiceUtil.addCLSFavouriteChallenges(favChallenge);

		} catch (com.liferay.portal.kernel.exception.SystemException e) {
			
			retVal = false;
		
			throw new SystemException(e.getMessage());
		}
		
		return retVal;
	}
 
 public boolean removeFavouriteChallenge(Long favChallengeId, Long userId) throws  SystemException {
		boolean retVal = true;
		try {
			CLSFavouriteChallengesPK clsFavouriteChallengesPK = new CLSFavouriteChallengesPK(favChallengeId, userId);
			CLSFavouriteChallengesLocalServiceUtil.deleteCLSFavouriteChallenges(clsFavouriteChallengesPK);
			
		} catch (com.liferay.portal.kernel.exception.PortalException e) {
			
			retVal = false;
			throw new SystemException(e.getMessage());
		}
				
		return retVal;
	}
 
 public List<CLSFavouriteChallenges> getFavouriteChallenges(Long favChallengeId, Long userId) throws SystemException {		 	   
		
	
		List favChallenges = new ArrayList();
		
					
		// verifico se l'idea sia tra i miei preferiti
		Criterion c_favIdea1 = RestrictionsFactoryUtil.eq("primaryKey.challengeId", favChallengeId);
		Criterion c_favIdea2 = RestrictionsFactoryUtil.eq("primaryKey.userId",userId);
		Criterion c_favIdea3 = RestrictionsFactoryUtil.and(c_favIdea1,c_favIdea2);
		
		DynamicQuery dqFavChallenge = DynamicQueryFactoryUtil.forClass(CLSFavouriteChallenges.class,this.getClassLoader()).add(c_favIdea3);
		favChallenges = CLSFavouriteChallengesLocalServiceUtil.dynamicQuery(dqFavChallenge);
		
		return favChallenges;
    }
 
 
 public List<CLSFavouriteChallenges> getFavouriteChallengesEntriesByUserId(long userId) throws SystemException{
		return CLSFavouriteChallengesUtil.findByUserID(userId);
	 }
 
 
 
}