package it.eng.rspa.ideas.utils;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.sun.jersey.api.client.ClientResponse;



public class ShortUrl {

	public static String shortens(String longUrl){

	String endpoint ="https://hec.su/api?url="+longUrl;
	                  
		//endpoint = "http://apps.morelab.deusto.es/welive/api/examples/hello"; //endpoint di test
	
		ClientResponse risposta;
		String laRisposta="";
		String titoloShort="";
		try {
			risposta =RestUtils.invokeRestAPI(endpoint, "get", null);
			
			laRisposta = risposta.getEntity(String.class);
			
			JSONObject rispostaJSON =  JSONFactoryUtil.createJSONObject(laRisposta);
			//Struttura JSON risposta
			//{"long":"http:\/\/www.i-b.com\/","short":"https:\/\/hec.su\/cmzE"}
			
			  titoloShort =	rispostaJSON.getString("short");
			
		} catch (Exception e) {
			e.printStackTrace();
			
			titoloShort="https://hec.su/cnpN";
		}
	
	return titoloShort;


	}

}	
		
		