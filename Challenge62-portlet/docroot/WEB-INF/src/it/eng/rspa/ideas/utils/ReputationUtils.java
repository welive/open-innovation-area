package it.eng.rspa.ideas.utils;

import javax.portlet.RenderRequest;

import com.liferay.portal.model.User;

public class ReputationUtils {
	
	
	
	/**
	 * @param user  
	 * @param renderRequest
	 * 
	 * Controllo se utente da mostrare nella Reputation
	 * 
	 * 
	 * @return
	 */
	public static boolean isUserToShow(User user, RenderRequest renderRequest){
		
		//escludo gli enti
		if (!MyUtils.isIMSSimpleUser(user))//utente su cui itero
			return false;
		
		
		//filtro per pilot
		String currentPilot = IdeasListUtils.getPilot(renderRequest);
		String userPilot = MyUtils.getPilotByUser(user); //utente su cui itero
		
		if (currentPilot.equalsIgnoreCase(userPilot))
			return true;
		
		
		return false;
	}
	

}
