package it.eng.rspa.ideas.ideas;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;

import java.util.Locale;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portlet.asset.model.AssetRenderer;
import com.liferay.portlet.asset.model.BaseAssetRendererFactory;

public class IdeaRenderFactory extends BaseAssetRendererFactory {

	@Override
	public AssetRenderer getAssetRenderer(long classPK, int type)
			throws PortalException, SystemException {
		CLSIdea idea = CLSIdeaLocalServiceUtil.getCLSIdea(classPK);
		return new IdeaAssetRenderer(idea);
	}

	@Override
	public String getClassName() {
		return CLSIdea.class.getName();
	}

	@Override
	public String getType() {
		return "CLSIdea";
	}

	@Override
	public String getTypeName(Locale locale, boolean hasSubtypes) {
		
		return LanguageUtil.get(LocaleUtil.getDefault(), CLSIdea.class.getName());
	}
	
//	@Override
//	public boolean hasPermission(PermissionChecker permissionChecker,
//			long classPK, String actionId) throws Exception {
//		return true;
//	}
	
//	@Override
//	public PortletURL getURLView(LiferayPortletResponse liferayPortletResponse,
//			WindowState windowState) throws PortalException, SystemException {
//
//		
//		PortletURL portletURL = super.getURLView(liferayPortletResponse, windowState);
//		portletURL.setParameter(
//		            "struts_action", "/message_boards/view_message");
//		        portletURL.setParameter(
//		            "messageId", String.valueOf(_message.getMessageId()));
//		        portletURL.setWindowState(windowState);
//		return portletURL;
//	}
}
