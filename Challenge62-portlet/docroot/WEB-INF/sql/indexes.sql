create index IX_C368F781 on CSL_CLSArtifacts (artifactId);
create index IX_38A8E726 on CSL_CLSArtifacts (ideaId);
create index IX_74AA83E7 on CSL_CLSArtifacts (ideaId, artifactId);

create index IX_2AA4D88 on CSL_CLSCategoriesChallenge (challengeId);

create index IX_9752B692 on CSL_CLSCategoriesSetForChallenge (categoriesSetID);
create index IX_8366136F on CSL_CLSCategoriesSetForChallenge (challengeId);

create index IX_634BAC8F on CSL_CLSChallenge (challengeDescription);
create index IX_93E5C0AB on CSL_CLSChallenge (challengeTitle);
create index IX_4609CF20 on CSL_CLSChallenge (needReport);
create index IX_2115D6FC on CSL_CLSChallenge (userId);
create index IX_4EFFCAB2 on CSL_CLSChallenge (uuid_);
create index IX_F1CF636 on CSL_CLSChallenge (uuid_, companyId);
create unique index IX_90B99238 on CSL_CLSChallenge (uuid_, groupId);

create index IX_A93D91A on CSL_CLSChallengePoi (challengeId);

create index IX_820969FD on CSL_CLSCoworker (ideaID);
create unique index IX_2BEF7537 on CSL_CLSCoworker (ideaID, userId);
create index IX_7B0504F1 on CSL_CLSCoworker (userId);

create index IX_BDCDCED6 on CSL_CLSFavouriteChallenges (challengeID);
create unique index IX_CD11F110 on CSL_CLSFavouriteChallenges (challengeID, userId);
create index IX_BDCE46F6 on CSL_CLSFavouriteChallenges (challengeId);
create unique index IX_69A84930 on CSL_CLSFavouriteChallenges (challengeId, userId);
create index IX_512689D2 on CSL_CLSFavouriteChallenges (userId);

create index IX_41E4D3C6 on CSL_CLSFavouriteIdeas (ideaID);
create unique index IX_5C0D0600 on CSL_CLSFavouriteIdeas (ideaID, userId);
create index IX_3AE06EBA on CSL_CLSFavouriteIdeas (userId);

create index IX_98209E69 on CSL_CLSIdea (benefiForPA);
create index IX_9A51B8AA on CSL_CLSIdea (benefitForPP);
create index IX_5757C1D5 on CSL_CLSIdea (benefitForUser);
create index IX_CBCC200A on CSL_CLSIdea (challengeId);
create unique index IX_4E46B94A on CSL_CLSIdea (ideaID);
create index IX_A174CE8D on CSL_CLSIdea (ideaTitle);
create index IX_3CCE466F on CSL_CLSIdea (municipalityId);
create index IX_861B9342 on CSL_CLSIdea (municipalityOrganizationId);
create index IX_8ABD9389 on CSL_CLSIdea (needId);
create index IX_4742543E on CSL_CLSIdea (userId);
create index IX_9A8D9D30 on CSL_CLSIdea (uuid_);
create index IX_7C8748F8 on CSL_CLSIdea (uuid_, companyId);
create unique index IX_6EBDAD7A on CSL_CLSIdea (uuid_, groupId);

create index IX_C9F871D8 on CSL_CLSIdeaPoi (ideaId);

create index IX_31F5A630 on CSL_CLSIdeaProblemRelevant (uuid_);

create index IX_A92C15F on CSL_CLSIdeaSolutionRealistic (uuid_);

create index IX_2D6748B0 on CSL_CLSIdeaSolveProblem (uuid_);

create index IX_CDCD463F on CSL_CLSOpLogIdea (ideaId);

create index IX_6B181AC6 on CSL_CLSRequisiti (authorUser);
create index IX_3BB74D42 on CSL_CLSRequisiti (ideaId);
create index IX_DA5DCB2C on CSL_CLSRequisiti (ideaId, authorUser);
create index IX_2C224CD0 on CSL_CLSRequisiti (taskId);

create index IX_8E286A6D on CSL_CLSVmeProjects (ideaId);
create index IX_20F800D0 on CSL_CLSVmeProjects (ideaId, isMockup);

create index IX_6B91E1AA on CSL_EvaluationCriteria (challengeId);
create index IX_A2094BC7 on CSL_EvaluationCriteria (challengeId, enabled);
create index IX_2D68960B on CSL_EvaluationCriteria (challengeId, inputId);
create index IX_60C67AF2 on CSL_EvaluationCriteria (challengeId, isBarriera);
create index IX_E098A37F on CSL_EvaluationCriteria (challengeId, isBarriera, enabled);
create index IX_B67629E1 on CSL_EvaluationCriteria (challengeId, isBarriera, isCustom);
create unique index IX_1A025532 on CSL_EvaluationCriteria (criteriaId);
create index IX_C5CDEF5F on CSL_EvaluationCriteria (isCustom, language);

create index IX_583CF8A on CSL_IdeaEvaluation (criteriaId);
create unique index IX_45EADAF0 on CSL_IdeaEvaluation (criteriaId, ideaId);
create index IX_F0635222 on CSL_IdeaEvaluation (ideaId);

create index IX_1FD18397 on CSL_NeedLinkedChallenge (challengeId);
create index IX_4D016BDC on CSL_NeedLinkedChallenge (needId);