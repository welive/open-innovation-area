<%@page import="it.eng.rspa.ideas.utils.UpdateIdeaUtils"%>
<%@page import="it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties"%>
<%@page import="javax.portlet.WindowState"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="it.eng.rspa.ideas.utils.MyUtils"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ include file="/html/challenges/init.jsp" %>
<%@page import="com.liferay.portal.service.PortletLocalServiceUtil"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<portlet:defineObjects />
<liferay-theme:defineObjects/>

<%@ include file="sections/checkPublicUser.jspf" %>


<%
User utenteCorrente = PortalUtil.getUser(request);
boolean isIMSSimpleUser =  MyUtils.isIMSSimpleUser(utenteCorrente);
boolean isEnte = MyUtils.isAuthority(utenteCorrente);
boolean areThereOpenChallenge= UpdateIdeaUtils.areThereOpenChallengeByUser( renderRequest);
boolean areThereNeedByPilot= NeedUtils.areThereNeedByPilot(renderRequest);

String urlcreateNeedPage = IdeaManagementSystemProperties.getFriendyUrlCreateNeed();
String urlNeedsExplorerPage = IdeaManagementSystemProperties.getUrlNeedsExplorerPage();
String urlcreateChallengePage = IdeaManagementSystemProperties.getFriendyUrlCreateChallenge();
String urlChallengesExplorerPage = IdeaManagementSystemProperties.getUrlChallengesExplorerPage();
String urlcreateIdeaPage = IdeaManagementSystemProperties.getFriendyUrlCreateIdea();
String urlIdeasExplorerPage = IdeaManagementSystemProperties.getUrlIdeasExplorerPage();
String pilot = IdeasListUtils.getPilot(renderRequest);
String currentUserFirstname = MyUtils.getFirstNameByUser(utenteCorrente);
ThemeDisplay td = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
String uuid = UUID.randomUUID().toString();


//Get available challenges
List<CLSChallenge> challengesI = CLSChallengeLocalServiceUtil.getActiveChallenges();
List<CLSChallenge> challenges = ChallengesUtils.getListaGareFiltrataPerPilot(challengesI, pilot);//filtro

//Get available needs
List <CLSIdea> needsI = 	CLSIdeaLocalServiceUtil.getNeeds();
List<CLSIdea> filteredNeeds =IdeasListUtils.getIdeasListFilterByPilot(needsI, pilot);

%>

<portlet:resourceURL var="resourceURL" />

<portlet:actionURL var="updateIdeasURL">
	<portlet:param name="<%=ActionRequest.ACTION_NAME%>" value="updateIdeas" />
</portlet:actionURL>

<portlet:renderURL var="modifyIdea">
	<portlet:param name="jspPage" value="/html/ideas/update.jsp"/>
</portlet:renderURL>

<portlet:renderURL var="modifyNeed">
	<portlet:param name="jspPage" value="/html/ideas/updateNeed.jsp"/>
</portlet:renderURL>

<script>
	/* GLOBAL JS variable*/
	var contextPath = '<%=request.getContextPath()%>';
	var portletNamespace = '<portlet:namespace/>';
	var alertCancel = '<liferay-ui:message key="ims.are-you-sure-you-want-to-leave-this-wizard"/>' +'<liferay-ui:message key="ims.all-content-will-be-lost"/>';
	var alertInsertDescription = '<liferay-ui:message key="ims.insert-description"/>';
	var alertInsertTitle = '<liferay-ui:message key="ims.insert-title"/>';
	var alertExistingTitle = '<liferay-ui:message key="ims.existing-title"/>'
	var alertSelectACategory = '<liferay-ui:message key="ims.please-select-at-least-one-subcategory"/>'
	
</script>

<div class="materialize">

	<div class="row">

	    <div class="col m12 hide-on-small-only"> 
	      <ul class="tabs">
	        <li class="tab col m3"><a id="oia1tab" href="#oia1" ><span><i class="fa fa-lightbulb-o fa-lg fa-fw" aria-hidden="true"></i><liferay-ui:message key="ims.open-innovation-area"/></span></a></li>
	        <li class="tab col m3"><a id="oia2tab" href="#oia2"><span><i class="fa fa-users fa-lg fa-fw" aria-hidden="true"></i><liferay-ui:message key="ims.most-active-users"/></span></a></li> 
	        
	  <% if (isEnte || isIMSSimpleUser){ %>      
	        <li class="tab col m3"><a id="oia3tab" href="#oia3" ><span><i class="fa fa-pie-chart fa-lg fa-fw" aria-hidden="true"></i><liferay-ui:message key="statistics"/></span></a></li>
	        
	        
	   <% }
 	  if (false){//TODO isEnte da ripristinare appena la Topic Detection e' pronta %>
	        <li class="tab col m3"><a id="oia4tab" href="#oia4"><span><i class="fa fa-line-chart fa-lg fa-fw" aria-hidden="true"></i><liferay-ui:message key="ims.topic-detection"/></span></a></li> 
	        
	   <% } %>
	      </ul>
	    </div>
	    	
	    <div class="col s12 hide-on-med-and-up"> 
	      <ul class="tabs">
	        <li class="tab col s1"><a id="oia1tab" href="#oia1"><i class="fa fa-lightbulb-o fa-1x fa-fw" aria-hidden="true"></i></a></li>
	        <li class="tab col s1"><a id="oia2tab" href="#oia2"><i class="fa fa-users fa-1x fa-fw" aria-hidden="true"></i></a></li> 
	        
	  <% if (isEnte || isIMSSimpleUser){ %>      
	        <li class="tab col s1"><a id="oia3tab" href="#oia3" ><i class="fa fa-pie-chart fa-1x fa-fw" aria-hidden="true"></i></a></li>
	   <% }
	  
	  	if (false){//TODO isEnte da ripristinare appena la Topic Detection e' pronta %>
	        <li class="tab col s1"><a id="oia4tab" href="#oia4"><i class="fa fa-line-chart fa-1x fa-fw" aria-hidden="true"></i></a></li> 
	        
	   <% } %>
	      </ul>
	    </div>

	
	<!-- Includes -->
	<div id="idea_page"><%-- Idea Dialogs --%>
		<%//I create this variable in order to disambiguate the included pages on both idea and need
			String nw = ""; 
		%>
	 	<%@ include file="/html/home/fullscrollpage/view_idea.jspf" %>
	</div>
	
	<%	 nw = "nw"; %>
	
	<div id="need_page"><%-- Need Dialogs --%>
		<%@ include file="/html/home/fullscrollpage/view_need.jspf" %>
	</div>
	<!-- Zoom Image Flows in Home Modals -->
	<%@ include file="fullscrollpage/zoom-panel.jspf" %>
	<!-- Select Idea/Need Modals -->
	<%@ include file="fullscrollpage/choose-need-panel.jspf" %>
	<%@ include file="fullscrollpage/choose-challenge-panel.jspf" %>
		
	<!-- Conclusion Modal -->
	<%@ include file="fullscrollpage/conclusion_modal.jspf" %>
	<!-- Conclusion Modal -->
	<%@ include file="fullscrollpage/need_conclusion_modal.jspf" %>
	
	
	<!-- IDEA -->
	
	<!-- Open Innovation Area -->
	<div id="oia1" class="col s12">
		<%@ include file="pages/loader.jspf" %>
		
		<div id="scroller">	
			
			<div id="scroll-body" class="">
				
				<div id="fullpage">
				
		<!-- 	Home Idea o Need -->
					<div class="section-fsp	animated fadeIn" id="section1">
						<div class="slide" data-anchor="slide1">
							<%@ include file="fullscrollpage/sectionOne_leftSlide.jspf" %>
						</div>
						
						<div class="slide active" data-anchor="slide2">
							<%@ include file="fullscrollpage/sectionOne_MainSlide.jspf" %>
						</div>
						 
		        		<div class="slide" data-anchor="slide3">
							<%@ include file="fullscrollpage/sectionOne_rightSlide.jspf" %>
						</div>
					</div>
					
		<!-- 	Descrizione	 -->
					<div class="section-fsp animated fadeIn" id="section2">
						 	<div id="subSection2a" class="subSection">
							</div>
					</div>
		<!-- 	Titolo -->
					<div class="section-fsp animated fadeIn" id="section3">
					  	 	<div id="subSection3a" class="subSection">
							</div>
							<div id="subSection3b" class="subSection">
							</div>
					</div>
		<!-- 	Categoria	 -->
					<div class="section-fsp animated fadeIn" id="section4">
							<div id="subSection4a" class="subSection">
							</div>
							<div id="subSection4b" class="subSection">
							</div>
					</div>
		<!-- 	Tags Parole Chiave -->
					<div class="section-fsp animated fadeIn" id="section5">
							<div id="subSection5a" class="subSection">
							</div>
							<div id="subSection5b" class="subSection">
							</div>
					</div>
		<!-- 	Creazione -->
					<div class="section-fsp animated fadeIn" id="section6">
						
							<div id="subSection6a" class="subSection">
							</div>
					</div>
					
				</div><!-- end Fullpage -->		
				
				<!-- end Fullpage -->
		
		<!-- 	Prev Button -->
		<!-- 
				<div id="scrollBtnToolPrev" class="scrollBtn scrollBtnTool-width">
					<div id="prevStep">
			    		<a id="btnPrevArrow" class="transparent"><i class="material-icons medium animated bounce infinite">expand_less</i></a>
			    	</div>
				</div>
		 -->

		<!-- 	Next Button -->
				<div id="scrollBtnToolNext" class=" scrollBtn scrollBtnTool-width animated fadeIn">
					<div id="nextStep" class="col s12 m8 offset-m2">
<!-- 			    		<a id="btnNextArrow" class="transparent"><i class="fa fa-angle-down fa-3x animated bounce infinite" aria-hidden="true"></i></a> -->
						<div class="">
						    <div class="col s4 m4 right-align">
						      <a id="deleteBtn" class="hide-on-med-and-down btn align-center core-color color-3 z-depth-2 tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.cancel-wizard"/>">
						      	<i class="material-icons left">delete</i> <liferay-ui:message key="ims.cancel"/>
						      </a>
						      <a id="deleteBtn" class="hide-on-large-only btn-floating btn-large align-center core-color color-3 z-depth-2 tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.cancel"/>">
						      	<i class="material-icons left">delete</i>
						      </a>
						    </div>
							<div class="col s4 m4 center-align">
						      <a id="btnPrevArrow" class="hide-on-med-and-down btn align-center core-color color-2 z-depth-2 tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.go-to-the-previous-step-of-the-wizard"/>">
						      	<i class="material-icons right">arrow_upward</i><liferay-ui:message key="previous"/>
						      </a>
						      <a id="btnPrevArrow" class="hide-on-large-only btn-floating btn-large align-center core-color color-2 z-depth-2 tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="previous"/>">
						      	<i class="material-icons right">arrow_upward</i>
						      </a>
						    </div>
						    <div class="col s4 m4 left-align">
						      <a id="btnNextArrow" class="hide-on-med-and-down btn align-center core-color color-2 z-depth-2 tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.go-to-the-next-step-of-the-wizard"/>">
						      	<i class="material-icons right">arrow_downward</i><liferay-ui:message key="ims.next"/>
						      </a>
						      <a id="btnNextArrow" class="hide-on-large-only btn-floating btn-large align-center core-color color-2 z-depth-2 tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.next"/>">
						      	<i class="material-icons right">arrow_downward</i>
						      </a>
						    </div>
						</div>
			    	</div>
				</div>
			</div>
		
		
		</div>
	
	</div>
	
	    <div id="oia2" class="col s12" style="display:none;">
	    
			<div class="row section top-margin">
					<div class="col s12 m12 l12">
						<%@ include file="sections/reputation.jspf" %>
					</div>
			</div>
			
		</div>
		
		
		<% if (isEnte || isIMSSimpleUser){ %>
	    <div id="oia3" class="col s12" style="display:none;">
	    
			<div class="row section top-margin">
					<div class="col s12 m12 l12">
						
					<% if (isIMSSimpleUser){ %>
						
						<%@ include file="/html/statistiche/view_expanded.jspf" %>					
					<% }else if (isEnte){ %>
						
						<%@ include file="/html/statisticheente/view_expanded.jspf" %>			
					<% } %>
					</div>
			</div>
			
		</div>
		
		 <% }
		
		if (isEnte){//WIP hidden %>
		
	    <div id="oia4" class="col s12" style="display:none;">
	    
			<div class="row section top-margin">
					<div class="col s12 m12 l12">
						topic detection
					</div>
			</div>
			
		</div>
		
		 <% } %>
		
	</div><!-- Fine Row -->
	
</div><!-- Fine Materialize -->


<script type="text/javascript" >
/* label used inside fullpage_custom.js */

var imsTopLevel = '<liferay-ui:message key="top-level"/>';
var imsDescription = '<liferay-ui:message key="ims.description"/>';
var imsTitle = '<liferay-ui:message key="ims.title"/>';
var imsCategories = '<liferay-ui:message key="ims.categories"/>';
var imsKeywords = '<liferay-ui:message key="keywords"/>';
var imsEnding = '<liferay-ui:message key="ims.ending"/>';

</script>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/fullscrollpage/jquery.fullpage.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/fullscrollpage/fullpage_custom.js"></script>

<script>

var border_select_card = 5; //this is the selection border dimension (in px)

function inizializeChooseModal(item){
	var $item = item;
	
	    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
	    $('#modal-carousel-'+$item+' .carousel.carousel-slider').carousel({ full_width: true, indicators: false }); 
	    
	    //This inizialize the modal for card selection
	    $('.trigger-carousel-'+$item+'').leanModal({
	    	starting_top: '5%', // Starting top style attribute
		    ending_top: '5%', // Ending top style attribute
	    	//This function calculate card dimensions with the inside image dinamically for a responsive result.
	        ready: function() { 
	        	initModalCarousel($item);
	        }, // Callback for Modal open
	        complete: function() { checkSelectedCard(); } // Callback for Modal close
	      });
	    
	    
	    //Apply the selection border to card if you click on its image
	    $('#modal-carousel-'+$item+' .carousel.carousel-slider .carousel-item .card-image').on('click', function(){
	    	
	    	
	    	$('.tooltipped').tooltip('remove'); /*Tooltip update - step 1 */
	    	
	    	//remove all selected and tip
	    	$(".card-image").attr('data-tooltip', $(this).data('tooltip1')); /*Tooltip update - step 2 */
    		$(".linked").removeClass("linked");
	    	
	    	//Select or Deselect Card
			 if($(".carousel-item .linked").length<=1) {
	    		selectCard($(this));
	    		$(this).attr('data-tooltip',  $(this).data('tooltip2')); /*Tooltip update - step 2 */
	    	}
	    	 
	    	setTimeout(function(){ $('.tooltipped').tooltip({delay: 50}); }, 500);  /*Tooltip update - step 3 */
	    	 
	    	
	    	
	    	//Enable or Disable Link Button
			if($(".carousel-item .linked").length==1) {
				$('#modal-action-link-'+$item).removeClass('disabled');
			}else {
				$('#modal-action-link-'+$item).addClass('disabled');
			}
	    })
	    
		$('#modal-action-link-'+$item).on('click', function(){
			var $this = $(this);
			if(!$this.hasClass( "disabled" )){
				/* $item can be "need" or "challenge" */
				var idSelected = $(".carousel-item .linked").attr("id");
				
				var titleSelected = $(".linked .detailTitleSelected").text() ;
				
				if ($item == "need" ){
					var idOnlyLong = idSelected.replace('need', '');
					setOnChosenChallengeOrNeed("need", idOnlyLong, titleSelected);
				}else{
					var idOnlyLong = idSelected.replace('challenge', '');
					setOnChosenChallengeOrNeed("challenge", idOnlyLong, titleSelected);
				}	
				
				$('#modal-carousel-'+$item+'').closeModal();
				checkSelectedCard();
			}
		});
	    
		$('#modal-action-cancel-'+$item).on('click', function(){
			var $this = $(this);
			$('#modal-action-link-'+$item).addClass( "disabled" );
			$(".linked").removeClass("linked");
		});

		$('#bindBtn-change').on('click', function(){
			$('.viewIfSelected').css("display","none");
			$('.hideIfSelected').css("display","block");
			
		});
		
}


function initModalCarousel(elem){
	
	var modale = $(window).height()*(100/100); //calculate a percentual of the Window
	
	if($(window).width() < 600){//if vertical layout
		var caruHeight = modale*(45/100); //calculate a percentual of the Modal
		var imgHeight = modale*25/100; //calculate a percentual of the card
	}else{//if orizontal layout
		var caruHeight = modale*(62/100); //calculate a percentual of the Modal
		var imgHeight = modale*45/100; //calculate a percentual of the card
	}
	var caruWidth = $('#modal-carousel-'+elem+' .carousel.carousel-slider .carousel-item .card').width(); //width of the Card
	
	$('#modal-carousel-'+elem).css("max-height","100%");
    $('#modal-carousel-'+elem+' .carousel.carousel-slider').height(caruHeight);
//		$('#modal-carousel-'+elem+' .carousel.carousel-slider .carousel-item .card').height("auto");
	$('#modal-carousel-'+elem+' .carousel.carousel-slider .carousel-item .card').height(caruHeight*(93/100));
	$('#modal-carousel-'+elem+' .carousel.carousel-slider .card-image').css("min-height", imgHeight);
	$('#modal-carousel-'+elem+' .carousel.carousel-slider .card-image').css("min-width", caruWidth-2*border_select_card);
	$('#modal-carousel-'+elem+' .carousel.carousel-slider .card-image img').css("max-height", imgHeight);
	$('#modal-carousel-'+elem+' .carousel.carousel-slider .card-image img').css("max-width", caruWidth-2*border_select_card);
}


/*Operation after the choice of Challenge or Need */
function setOnChosenChallengeOrNeed (needOrChallenge, chosenid, titleSelected ){
	
	if (needOrChallenge == "need"){
		
		document.getElementById("<portlet:namespace/>challengeSelection").value ="" ;
		document.getElementById("<portlet:namespace/>needSelection").value=chosenid ;
		updateCategories("true");
		$("#languageTipForNeed").css('display','inline'); 
		$("#languageTipForChallenge").css('display','none');
		
		$(".needSelected").css('display','inline-block'); 
		$(".challengeSelected").css('display','none');
		
		
		$(".tipNeedTitle").attr("data-tooltip",'<liferay-ui:message key="ims.linked-need"/>: '+titleSelected); 
		$(".tipNeedTitle").tooltip({delay: 50});

		
	}else if  (needOrChallenge == "challenge"){
		
		document.getElementById("<portlet:namespace/>challengeSelection").value =chosenid ;
		document.getElementById("<portlet:namespace/>needSelection").value="" ;
		updateCategories("false");
		$("#languageTipForNeed").css('display','none'); 
		$("#languageTipForChallenge").css('display','inline');
		
		$(".needSelected").css('display','none'); 
		$(".challengeSelected").css('display','inline-block');
		
		$(".tipChallengeTitle").attr("data-tooltip",'<liferay-ui:message key="ims.linked-challenge"/>: '+titleSelected); 
		$(".tipChallengeTitle").tooltip({delay: 50});

	}
	
	$(".showTitleSelected").text(titleSelected);
}

/*Select a Card (challenge/need) to link*/
function selectCard(elem){
	$(".linked").removeClass("linked");
	elem.parent().toggleClass("linked");
	$(".linked").css("border-width",border_select_card+"px");
}

//Check if there is almost a card selected and shows the "Start Button" on the slide
function checkSelectedCard(){
	
// 	$('#rememberAssociatedItem').css("display","none");
	
	if($(".linked").length==0){
		$('.slideRight .btnNext_start').parent().css("display","none");
		$('.viewIfSelected').css("display","none");
		$('.hideIfSelected').css("display","block");
// 		$('#rememberAssociatedItem').css("display","none");
	}else{
		$('.slideRight .btnNext_start').parent().css("display","inline-block");
		$('.viewIfSelected').css("display","block");
		$('.hideIfSelected').css("display","none");
// 		$('#rememberAssociatedItem').css("display","block");
	}
// 	console.log("Selezionati: "+$(".linked").length)
}



function inizializeConclusionModal(){
	//This inizialize the Conclusion modal.
	$('.conclusion-modal').leanModal({
		dismissible: false,
		starting_top: '5%', // Starting top style attribute
	    ending_top: '5%', // Ending top style attribute
   		ready: function() { 
   			$(".itemCreated").each(function( index ) { $(this).html(elemToCreate) });
   		}, // Callback for Modal open
   		complete: function() { //On closing
   			//console.log("Conclusion Closed.");
   		} // Callback for Modal close
	 });
}


$(document).ready(function(){
	
	<% if (challenges.size () > 0){	%>
		inizializeChooseModal('challenge');
	<%}
	
	if (filteredNeeds.size () > 0){ %>
		inizializeChooseModal('need');
	<%}%>	


	/*check if it is an idea started by a challenge detail */
	var startingChallenge = queryString('c');
	var preStarted = false;
	
	if (startingChallenge != ""){
		preStarted = true;
		$("#challenge"+startingChallenge).addClass("linked");
		var titleSelectedLinked = $(".linked .detailTitleSelected").text() ;
		
		setOnChosenChallengeOrNeed("challenge",startingChallenge,titleSelectedLinked );
	}else{
	
		/*check if it is an idea started by a need detail */
		var startingNeed = queryString('n');
		
		if (startingNeed != ""){
			preStarted = true;
			$("#need"+startingNeed).addClass("linked");
			var titleSelectedLinked = $(".linked .detailTitleSelected").text() ;
			
			setOnChosenChallengeOrNeed("need",startingNeed,titleSelectedLinked );
		}
	}
	
	checkSelectedCard();
	
	if (preStarted == true){
		elemToCreate = "idea";
		insertTipo();
		$.fn.fullpage.moveTo("sectionTwo");
	}
	
});

/*Get the input URL parameter value*/
function queryString(param){
	
    var queryString = window.location.href;
    var varArray = queryString.split("="); /*eg. index.html?msg=pippo */
    var parameter =	 varArray[0].split("?")[1]; /* msg */
    var paramValue = varArray[1]; /* pippo */
    
    if (parameter == param)
    	return paramValue;
    else 
    	return "";
}



</script>



<!-- REPUTATION TAB -->
<script>

function redrawPieChart(){
	setTimeout(function() {
		drawChart(); 
	}, 250);
}
function redrawBarChart(){
	setTimeout(function() {
		drawVisualization();
	}, 250);
}


function ResizeChart(){

	if(<%=isEnte%>) {
		redrawPieChart();
	}else {
		redrawPieChart();
		redrawBarChart();
	};
};

var resizePageTimer;
$(window).on('resize', function(e) {
  clearTimeout(resizePageTimer);
	resizePageTimer = setTimeout(function() {
		ResizeChart();
  }, 250);

});


$( document ).ready( function(){
		
	$("a#oia3tab").click(function(){
		ResizeChart();
	});
	
	//Inizialize Tabs HomeOIA and enable HTML, Body tags scroll in other tabs
	$('ul.tabs').tabs({
		onShow: function(t){
			$this = t;
			if($this.attr('id') != "oia1") { $('html, body').css("overflow","scroll")}
			else{ $('html, body').css("overflow","hidden")};
		}
	});
	

});


/* Ajax SYNC call to see if a title exist */
function checkTitleExist(ajaxTitle){
	
	var ret = true;
	
	var input = {};
	input['<portlet:namespace />param'] = 'titoloIdea';
	input['<portlet:namespace />titleChoosen'] = ajaxTitle;
	input['<portlet:namespace />modifica'] = '-1';
	
	$.ajax('<%=resourceURL.toString()%>', {
		'type': 'POST',
		'data':input,
       	'dataType': 'json',
       	async:false
	})
	.success(function(data){
		var ajRitornoTitleExist = data.titleExist;
  	 	ret = (ajRitornoTitleExist == 'yes')
	})
	.error(function(){
		console.log("error on checkTitleExist backend!");
	});
	
	return ret;
	
} /* checkTitleExist */






$("#sendBtn-idea").click(function(){
	
	/* Ajax ASYNC call to see if a title exist */
	 AUI().use('aui-io-request', function(A){
	    	
		 var ideaTitle = document.getElementById('<portlet:namespace/>ideaTitle').value;
		 var ideaDescription= document.getElementById('<portlet:namespace/>ideaDescription').value;
	 
	        A.io.request('<%=resourceURL.toString()%>', {
	               method: 'post',
	               data: {
	            	   <portlet:namespace />param: 'titoloIdea',
	            	   <portlet:namespace />titleChoosen: ideaTitle,
	            	   <portlet:namespace />modifica: '-1'
	               },
	               dataType: 'json',
	               on: {
	                   success: function() {
	                    
	                	   var ajRitornoTitleExist = this.get('responseData').titleExist;
		                    
		                   var ckCat = checkCategories();
		                    
		                   if(ideaDescription.trim() == ""){ 
								alert(alertInsertDescription);
								  $.fn.fullpage.moveTo(2);
						   }else if (ideaTitle.trim() == "") {
								alert(alertInsertTitle);
								  $.fn.fullpage.moveTo(3);
		                   }else if(ajRitornoTitleExist == 'yes'){
	                			alert(alertExistingTitle);
	                			  $.fn.fullpage.moveTo(3);
		                   }else if( ckCat == false ){
			                    alert(alertSelectACategory);	
			                    $.fn.fullpage.moveTo(4);
		                   }else{
		                	 	//display overlay on submit
		                	 	$('#wait-overlay').html('<i id="wait-overlay-spin"class="fa fa-cog fa-5x fa-spin " aria-hidden="true"></i>');
			               		$('#wait-overlay').css({'display':'block', 'opacity':'.5','z-index':'999999'});
		                	 	//disable scrolling page
			               		$.fn.fullpage.setAllowScrolling(false);
								submittoForm("<portlet:namespace/>formIdea", false);
							}//if else					                  	                   	                	
	                   }//success
	              }//on					               
	        });//A.io.request					 
	    });//AUI().use					
});
//$("#submitForm").click




$("#sendBtn-need").click(function(){
	
	/* Ajax ASYNC call to see if a title exist */
	 AUI().use('aui-io-request', function(A){
	    	
		 var needTitle = document.getElementById('<portlet:namespace/>needTitle').value;
		 var needDescription= document.getElementById('<portlet:namespace/>needDescription').value;
	 
	        A.io.request('<%=resourceURL.toString()%>', {
	               method: 'post',
	               data: {
	            	   <portlet:namespace />param: 'titoloIdea',
	            	   <portlet:namespace />titleChoosen: needTitle,
	            	   <portlet:namespace />modifica: '-1'
	               },
	               dataType: 'json',
	               on: {
	                   success: function() {
	                    
		                   var ajRitornoTitleExist = this.get('responseData').titleExist;
		                    
		                   var ckCat = checkMaterialCategories();

		                    
		                   if(needDescription.trim() == ""){ 
								alert(alertInsertDescription);
								  $.fn.fullpage.moveTo(2);
						   }else if (needTitle.trim() == "") {
								alert(alertInsertTitle);
								  $.fn.fullpage.moveTo(3);
		                   }else if(ajRitornoTitleExist == 'yes'){
	                			alert(alertExistingTitle);
	                			  $.fn.fullpage.moveTo(3);
		                   }else if( ckCat ){
			                    alert(alertSelectACategory);	
			                    $.fn.fullpage.moveTo(4);
		                   }else{
		                	 	//display overlay on submit
		                	 	$('#wait-overlay').html('<i id="wait-overlay-spin"class="fa fa-cog fa-5x fa-spin " aria-hidden="true"></i>');
			               		$('#wait-overlay').css({'display':'block', 'opacity':'.5','z-index':'999999'});
			               		//disable scrolling page
			               		$.fn.fullpage.setAllowScrolling(false);
								submittoForm("<portlet:namespace/>formNeed", true);
							}//if else
		               		
	                   }//success
	              }//on					               
	        });//A.io.request					 
	    });//AUI().use					
});//$("#...").click


function submittoForm(formId, isNeed){
    AUI().use('aui-io-request', function(A){
    	A.io.request('<%=updateIdeasURL.toString()%>', {
            method: 'post',
            form: {
                id: formId
            },
            on: {
                 success: function() {
                	 
                		 if (isNeed == true){
                			 
                			 $('#needModalConclusion').openModal({
                							dismissible: false,
                							starting_top: '5%', // Starting top style attribute
                						    ending_top: '5%', // Ending top style attribute
                					   		ready: function() { 
                					   			$(".itemCreated").each(function( index ) { $(this).html(elemToCreate) });
                					   		} // Callback for Modal open

                						 }
                			 );
                			 getNeedDetail(true);
                			 
                		 }else{

                			 $('#modalConclusion').openModal({
		     							dismissible: false,
		    							starting_top: '5%', // Starting top style attribute
		    						    ending_top: '5%', // Ending top style attribute
		    					   		ready: function() { 
		    					   			$(".itemCreated").each(function( index ) { $(this).html(elemToCreate) });
		    					   		} // Callback for Modal open
		    	
		    						 });
                			 getIdeaDetail(true);
                		 } 
                		 //remove overlay on submit
                		 $('#wait-overlay').remove();
                 	 	
                 }
            }
         });
 });
}//function submittoForm



//controllo che ci sia almeno una categoria selezionata
function checkCategories(){
	
	var catTrovata = false;
	var ns=$( ".category-item.mdl-checkbox__input" );//tutti i checkbox per classe
	
	$.each(ns, function(i){
		
		if ($(this).is(':checked')){
			catTrovata = true;
		}
	})
	
	return catTrovata;
}

//controllo che ci sia almeno una categoria selezionata
function checkMaterialCategories(){
	
	var catFound = false;
	var selectedValues=$( "#<%=nw%>selectCategories" ).val();
	
	
	if (selectedValues != null){//it is null if neither the Vocabulary was chosen
	
		if (selectedValues.length > 0)
			catFound = true;	
	}
	
	
	return !catFound;
}




function  updateCategoriesAjax(){
	
	/*get the select value */
	var selectedVoc = $( "#<%=nw%>selectVocabulary" ).val();
		

	AUI().use('aui-io-request', function(A){
	  A.io.request('<%=resourceURL.toString()%>', {
          method: 'post',
          data: {
       			   <portlet:namespace />param: 'categoriesByVocabolaryId',
       	 		   <portlet:namespace />vocId: selectedVoc,
       	 		   <portlet:namespace />localePerAssetVoc: '<%=locale%>',
       	 		   
          },
          dataType: 'json',
          on: {
              success: function() {
               
            	  var jsonVocArray = this.get('responseData');
            	  
            	  var conteinerSelect = $('#<%=nw%>selectCategories');
            	  

            	  
            	  $(conteinerSelect).empty();
            	  
            	  
            	  $(conteinerSelect).append( "<option value='' disabled selected><liferay-ui:message key='ims.choose-one-or-more-subcategories'/></option>");
            	  
            	  
            	  jQuery.each(jsonVocArray, function(index, voc) {
            		  
            		$(conteinerSelect).append( "<option value='"+voc.categId+"' >"+voc.categName+"</option>");
            		  
            		  
            	  });/* jQuery.each */
                  
            	  $(conteinerSelect).append("<optgroup label=''></optgroup>");
            	  $(conteinerSelect).material_select();
            	  
                   
	              }/* success */
	         }/*on	*/				               
	   });/*A.io.request */					 
	});/*AUI().use*/
	

}/*updateCategoriesAjax */





function  updateCategories(isNeed){
	
	RemoveAllCategories();/* Remove the already selected categories  */
	
	
	var idSelected = $(".carousel-item .linked").attr("id");
	var idgara = idSelected.replace('challenge', '');
	var idneed = idSelected.replace('need', '');
	
	//to get with Ajax, in the ideas, the categories enabled in the challenge or need
	 AUI().use('aui-io-request', function(A){
	    	
			
	 
	        A.io.request('<%=resourceURL.toString()%>', {
	               method: 'post',
	               data: {
	            	   <portlet:namespace />param: 'updateCategories',
	            	   <portlet:namespace />challengeChoosen: idgara,
	            	   <portlet:namespace />localePerAssetVoc: '<%=locale%>',
	            	   <portlet:namespace />needChoosen: idneed,
	            	   <portlet:namespace />isNeed: isNeed
	            	   
	               },
	               dataType: 'json',
	               on: {
	                   success: function() {
	                    
		                    ajRitornoAggiorno = this.get('responseData').vocabolari;
		                    				         
		                    var splita = ajRitornoAggiorno.split(',');

		                    if (splita.length == 1){ //se non ci sono categorie, 1 perch� una virgola c'e' sempre
		                    	
		                    	$( ".contentCategory" ).hide();//cancello tutto il tab delle categorie				                    
		                    	document.getElementById("messaggioNoCategory").style.display="inline";//mostro msg no category

		                    
		                    }else{
		                    	//if there are categories
		                    	
		                    	document.getElementById("messaggioNoCategory").style.display="none";//nasconto msg no category
		                    	$( " .contentCategory" ).show();//mostro  il tab delle categorie	
		                    	
								//loop on selected categories
			                    $( "#categorySection .nav-tabs > li" ).each(function( index ) {
			                    	  
			                    	  trovato=false;
			                    	  for ( i=0; i <splita.length-1; i++){
					                    					                    		  					                    		  
			                    		  if ($( this ).text().trim() == splita[i].trim()){
			                    			  trovato=true;				                    			  
			                    		  }
			                    		 
					                    }
			                    	  
			                    	  if (trovato == false){
			                    		
			                    		  $( this ).hide();//nascondo tab
			                    		  
			                    	  }else{
			                    		  
			                    		  $( this ).show(); //mostro tab, se non lo metto li cancella tutti				                    	
			                    		  this.firstElementChild.click();//per prendere il focus e aggiornare sotto
			                    	  }				                    	  
			                    	});	
		                    
		                    }
		                    
	                   }//success
	              }//on					               
	        });//A.io.request					 
	    });//AUI().use	
	
	
}



//devo poter selezionare solo da un tab,quindi appena clickko
//deseleziono tutti i checkbox sotto gli altri tab		
function RemoveAllCategories(){
	
	var ns=$( ".category-item.mdl-checkbox__input" );//tutti i checkbox per classe
	
	$.each(ns, function(i){
		
		$(this).closest('label').removeClass('is-checked'); //parte grafica
		$(this).prop('checked', false); //parte logica
		
	})
}


function getIdeaDetail(isModify){
	
	//chiamata Ajax per vedere se i titoli gia' esistono
	 AUI().use('aui-io-request', function(A){
	    	
		 var ideaTitle = document.getElementById('<portlet:namespace/>ideaTitle').value;
	 
	        A.io.request('<%=resourceURL.toString()%>', {
	               method: 'post',
	               data: {
	            	   <portlet:namespace />param: 'titoloIdea',
	            	   <portlet:namespace />titleChoosen: ideaTitle,
	            	   <portlet:namespace />modifica: '-1'
	               },
	               dataType: 'json',
	               on: {
	                   success: function(){
	                    
	                	   var ideaId = this.get('responseData').ideaId;	
	                	   
	                	   $('#<portlet:namespace/>fm_modify #<portlet:namespace/>ideaId').attr("value",ideaId)  ;
	                	   
	                	   
	                	   if (isModify == false){	    
	                		   
	                		   var ideaDetailsPage = this.get('responseData').ideaDetailsPage;	                	   
		                    	window.location.href = ideaDetailsPage;
		                    }
	                	   
	                   }//success
	              }//on					               
	        });//A.io.request					 
	    });//AUI().use	
};//function


function getNeedDetail(isModify){
	
	//chiamata Ajax per vedere se i titoli gia' esistono
	 AUI().use('aui-io-request', function(A){
	    	
		 var needTitle = document.getElementById('<portlet:namespace/>needTitle').value;
	 
	        A.io.request('<%=resourceURL.toString()%>', {
	               method: 'post',
	               data: {
	            	   <portlet:namespace />param: 'titoloIdea',
	            	   <portlet:namespace />titleChoosen: needTitle,
	            	   <portlet:namespace />modifica: '-1'
	               },
	               dataType: 'json',
	               on: {
	                   success: function(){
	                    
	                	   var ideaId = this.get('responseData').ideaId;	
	                	   
	                	   $('#<portlet:namespace/>fm_modifyNeed #<portlet:namespace/>ideaId').attr("value",ideaId);
	                	   
	                	   
	                	   if (isModify == false){	    
	                		   
	                		   var ideaDetailsPage = this.get('responseData').ideaDetailsPage;	   
		                    	window.location.href = ideaDetailsPage;
		                    }
	                	   
	                   }//success
	              }//on					               
	        });//A.io.request					 
	    });//AUI().use	
};//function


//Only one main category (Tab) must be choosen
function checkOnlyOneMainCategory(){
	
	var ns=$( "#nwcategorySection .category-item.mdl-checkbox__input" );//all the checkboxes per class
	
	
	var catSelezionate = [];
	
	$.each(ns, function(i){
		
		if ($(this).is(':checked')){
			
			var cBoxId = $(this).attr('id').replace("checkbox-", ""); //was checkbox-1-2, now 1-2
			var cBoxMainCatId= cBoxId.substr(0, cBoxId.indexOf('-')); 
			catSelezionate.push (cBoxMainCatId);
			
		}
		
	})
	
	if (allTheSame(catSelezionate)){
		return false;
	}
		
	//else
	return true;
}

//check if all elements of an array are equal
function allTheSame(array) {
    var first = array[0];
    return array.every(function(element) {
        return element === first;
    });
}




</script>