<%@page import="it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties"%>
<%@ include file="/html/challenges/init.jsp" %>

<%

String errorMessageKeyI18n = request.getParameter("errorMessageKeyI18n");
String urlHomeIms = IdeaManagementSystemProperties.getUrlHomeIMS(); 
%>


<div class="materialize">

	  		
	  		
	<div class="col s4 valign center ">
			<div class="portlet-msg-error">
				<liferay-ui:message key="warning"/>!
			  <br/><br/>
			
			
					 <liferay-ui:message key="an-unexpected-error-occurred" />
					 <br/><br/> 
			
			
					 <liferay-ui:message key="<%=errorMessageKeyI18n %>"/>
			 <br/>
</div>
				<p>
					 <a class="waves-effect btn-flat tooltipped" onClick="history.go(-1);" href="#" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.go-back"/>">
	  				<i class="icon-arrow-left left">&nbsp;</i><liferay-ui:message key="back"/>
	  			</a>
				</p>
				
				<p>
				<a class="waves-effect btn-flat tooltipped" href="<%=urlHomeIms %>" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.go-to-open-innovation-area-home"/>">
					<i class="icon-arrow-left left">&nbsp;</i><liferay-ui:message key="ims.open-innovation-area"/>
				</a>
			</p>
	</div>



</div>




