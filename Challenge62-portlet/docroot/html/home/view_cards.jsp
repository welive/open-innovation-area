<%@page import="it.eng.rspa.ideas.utils.UpdateIdeaUtils"%>
<%@page import="it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties"%>
<%@page import="javax.portlet.WindowState"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="it.eng.rspa.ideas.utils.MyUtils"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<portlet:defineObjects />

<%@ include file="sections/checkPublicUser.jspf" %>


<%
User utenteCorrente = PortalUtil.getUser(request);
boolean isIMSSimpleUser =  MyUtils.isIMSSimpleUser(utenteCorrente);
boolean isEnte = MyUtils.isAuthority(utenteCorrente);
boolean areThereOpenChallenge= UpdateIdeaUtils.areThereOpenChallengeByUser( renderRequest);

String urlcreateNeedPage = IdeaManagementSystemProperties.getFriendyUrlCreateNeed();
String urlNeedsExplorerPage = IdeaManagementSystemProperties.getUrlNeedsExplorerPage();
String urlcreateChallengePage = IdeaManagementSystemProperties.getFriendyUrlCreateChallenge();
String urlChallengesExplorerPage = IdeaManagementSystemProperties.getUrlChallengesExplorerPage();
String urlcreateIdeaPage = IdeaManagementSystemProperties.getFriendyUrlCreateIdea();
String urlIdeasExplorerPage = IdeaManagementSystemProperties.getUrlIdeasExplorerPage();

%>


<div class="materialize">
	

	<div class="row">
	
	    <div class="col m12 hide-on-small-only"> 
	      <ul class="tabs">
	        <li class="tab col m3"><a id="oia1tab" href="#oia1" ><liferay-ui:message key="ims.open-innovation-area"/></a></li>
	        <li class="tab col m3"><a id="oia2tab" href="#oia2"><liferay-ui:message key="ims.most-active-users"/></a></li> 
	        
	  <% if (isEnte || isIMSSimpleUser){ %>      
	        <li class="tab col m3"><a id="oia3tab" href="#oia3" ><liferay-ui:message key="statistics"/></a></li>
	        
	        
	   <% }
	  if (false){//TODO isEnte da ripristinare appena la Topic Detection e' pronta %>
	        <li class="tab col m3"><a id="oia4tab" href="#oia4"><liferay-ui:message key="ims.topic-detection"/></a></li> 
	        
	   <% } %>
	      </ul>
	    </div>
	    	
	    <div class="col s12 hide-on-med-and-up"> 
	      <ul class="tabs">
	        <li class="tab col s1"><a id="oia1tab" href="#oia1"><i class="fa fa-lightbulb-o" aria-hidden="true"></i></a></li>
	        <li class="tab col s1"><a id="oia2tab" href="#oia2"><i class="fa fa-users" aria-hidden="true"></i></a></li> 
	        
	  <% if (isEnte || isIMSSimpleUser){ %>      
	        <li class="tab col s1"><a id="oia3tab" href="#oia3" ><i class="fa fa-pie-chart" aria-hidden="true"></i></a></li>
	   <% }
	  
	  	if (false){//TODO isEnte da ripristinare appena la Topic Detection e' pronta %>
	        <li class="tab col s1"><a id="oia4tab" href="#oia4"><i class="fa fa-line-chart" aria-hidden="true"></i></a></li> 
	        
	   <% } %>
	      </ul>
	    </div>
	    
	    
	    
	    <div id="oia1" class="col s12">
				<!-- Open Innovation Area -->
				<%@ include file="sections/cards.jspf" %>
		</div>
		
	    <div id="oia2" class="col s12" style="display:none;">
	    
			<div class="row section top-margin">
					<div class="col s12 m12 l12">
						<%@ include file="sections/reputation.jspf" %>
					</div>
			</div>
			
		</div>
		
		
		<% if (isEnte || isIMSSimpleUser){ %>
	    <div id="oia3" class="col s12" style="display:none;">
	    
			<div class="row section top-margin">
					<div class="col s12 m12 l12">
						
					<% if (isIMSSimpleUser){ %>
						
						<%@ include file="/html/statistiche/view_expanded.jspf" %>					
					<% }else if (isEnte){ %>
						
						<%@ include file="/html/statisticheente/view_expanded.jspf" %>			
					<% } %>
					</div>
			</div>
			
		</div>
		
		 <% }
		
		if (isEnte){ %>
		
	    <div id="oia4" class="col s12" style="display:none;">
	    
			<div class="row section top-margin">
					<div class="col s12 m12 l12">
						topic
					</div>
			</div>
			
		</div>
		
		 <% } %>
		
  </div>
	

</div>



<script>


function redrawPieChart(){
	setTimeout(function() {
		drawChart(); 
	}, 250);
}
function redrawBarChart(){
	setTimeout(function() {
		drawVisualization();
	}, 250);
}


function ResizeChart(){

	if(<%=isEnte%>) {
		redrawPieChart();
	}else {
		redrawPieChart();
		redrawBarChart();
	};
};

function ResizeCard(){
 	var h = 0, ch = 0;
 	$(".badge-card p").css("height","auto");
 	
	$(".badge-card").each(function(i) {
		var currentElement = $(this);
	  	if(h < currentElement.height()) h=currentElement.height();
// 	  	console.log("H: "+h)
	});
	$(".badge-card p").css("height",h);
	
// 		console.log("resize card: "+h)
}; // end of document ready


var resizePageTimer;
$(window).on('resize', function(e) {
  clearTimeout(resizePageTimer);
	resizePageTimer = setTimeout(function() {

		ResizeCard();
		ResizeChart();
  }, 250);

});


$( document ).ready( function(){
		
	ResizeCard();

	$("a#oia3tab").click(function(){
		ResizeChart();
	});

});


</script>