<%@page import="it.eng.rspa.ideas.utils.MyConstants"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<portlet:defineObjects />

<!-- I contenuti di questa pagina non vengono mai mostrati direttamente
la struttura viene clonata e riempita con Ajax
 -->
	
<!-- PRIMA CARD		 -->
        <div class="col s12 m6 l4 schedaGara" style="display:none">
     	
			<!-- CARD -->
			<div class="card cardlink">
          		
				<!-- Card Header -->
				<%@ include file="header-card.jsp" %>
				
				<!-- Card Content -->
				<div class="card-content">
				
						<!-- Titolo e autore -->
				    	<div class="bottom-margin">
	   						<p class="card-multi-title">
	   							Titiolo della Gara
	   						</p><!-- 60 char -->
	   						<a href="#" class="titoloGara tooltipped viewChallenge" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="ims.view-challenge"/>'>
	   							<i class='material-icons fa-fw'>find_in_page</i>
	   						</a>
	  						<p class="grey-text text-darken-2">
	  						  	<label class="grey-text text-darken-2"><liferay-ui:message key="ims.submitted-by"/></label>
					         	<span  class="autoreGara">Arnaldo Fedrigotti</span>
					      	</p>
						</div>
						<div class="divider"></div>
						

						
						<!-- Start/End Date -->
					    <div class="row">
					    		<div  class="statusChallengeOpen col s12 center green white-text">
					    			<liferay-ui:message key="active"/>
					    		</div>
					    		<div  class="statusChallengeClose col s12 center red white-text">
					    			<liferay-ui:message key="expired"/>
					    		</div>
					    	
					      	<div class="col s6 center ">
					      		<h6 class="weight-5"><liferay-ui:message key="start-date"/></h6>
					      		<span class="dataInizioGara">20/01/2016</span>
					      	</div>
					      	<div class="col s6 center">
					      		<h6 class="weight-5"><liferay-ui:message key="end-date"/></h6>
					      		<span class="dataFineGara">18/03/2016</span>
					      	</div>

					     </div>
					     <!-- Rating -->
					    <div class="row">
					     	<div class="col s12 center">
					      		<span class="votoMedio">
									<i class="icon-star"></i>
									<i class="icon-star"></i>
									<i class="icon-star"></i>
									<i class="icon-star"></i>
									<i class="icon-star-empty"></i>
								</span>
					      	</div>
					    </div>
				     	<div class="divider"></div>
				     	
						<!-- Immagine edescrizione -->
						<div class="section row">
						   	<div class="col s8 offset-s2 m4 l4 center-align image-card">   
						   		<img src="" alt="" class="responsive-img imgRappresentativa top-margin">
						   		<div class="sampleText flow-text weight-5"></div>
						   	</div>
						   	<div class="col s12 m8 l8">
								<div class="multi-truncate descrizioneGara  top-margin">
									<p class="descrLink">
									<!-- 
										<a href="" class="moreDescr tooltipped viewChallenge" data-position="top" data-delay="50" data-tooltip='<liferay-ui:message key="ims.view-challenge"/>'>
										descrizione gara 
										</a>
									-->
									</p>
									<!--
									<a href="" class="readmore tooltipped viewChallenge" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="ims.view-challenge"/>'>
									&nbsp;<i class="material-icons fa-fw fa-lg">more</i></a>
									-->
								</div>
							</div>
						</div>
						<div class="divider"></div>
						
						<!-- Associazione Gara -->
						<div class="section row-last">
							<div class="col-fake">
								<i class="material-icons icon-lightbulb small">&nbsp;</i>
								<span class="weight-3">
										
										<liferay-ui:message key="ims.ideas-proposed" /> : <span class="numIdeas">7</span>
										
								</span>
							</div>
						</div>
				</div><!-- fine Card-content -->
			
			
				<!-- Card Footer -->
			   <div class="footer-card center-align grey darken-2 white-text">
			   
				<!-- open -->
	              	  <span class="material-icons  iconaStato <%=MyConstants.CHALLENGE_STATE_OPEN%> grey-text tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.open"/>">lock_open</span>
<%-- 					  <span class="statoChallenge <%=MyConstants.CHALLENGE_STATE_OPEN%> grey-text"></span> --%>
					  &nbsp;
					  
					  	<span class="small iconaStato icon-angle-right"></span>&nbsp;
				<!-- evaluation -->
					  <span class="material-icons  iconaStato <%=MyConstants.CHALLENGE_STATE_EVALUATION%> grey-text tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.in-evaluation"/>">assessment</span>
<%-- 					  <span class="statoChallenge <%=MyConstants.CHALLENGE_STATE_EVALUATION%> grey-text"></span> --%>
					  &nbsp;
					  
					  	<span class="small iconaStato icon-angle-right"></span>&nbsp;
				<!-- evaluated -->
					  <span class="material-icons  iconaStato <%=MyConstants.CHALLENGE_STATE_EVALUATED%> grey-text tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.evaluated"/>">assignment</span>
<%-- 					  <span class="statoChallenge <%=MyConstants.CHALLENGE_STATE_EVALUATED%> grey-text"></span> --%>
					  &nbsp;
					  
			    </div><!-- fine Card Footer -->
			
			    
				<!-- Card Back side -->
			    <div class="card-reveal">
			      <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
			      <div class="divider"></div>
			      <p>Here is some more information about this product that is only revealed once clicked on.</p>
			    </div><!-- fine Card Back side -->
			    
			</div><!-- Fine CARD -->
			
		</div><!-- Fine Col Idea -->
		
<!-- 	Fine PRIMA CARD -->

