<!-- Idea Evaluation, evaluation process & results -->

<%@page import="it.eng.rspa.ideas.utils.MyConstants"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation"%>
<%@page import="it.eng.rspa.ideas.utils.IdeasListUtils"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.EvaluationCriteriaLocalServiceUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria"%>
<%@page import="it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="it.eng.rspa.ideas.utils.ChallengesUtils"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea"%>
<%@page import="java.util.List"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ include file="/html/challenges/init.jsp" %>

<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<liferay-theme:defineObjects />


<%
User currentUser = PortalUtil.getUser(request);
long challengeId = Long.parseLong(request.getParameter("challengeId"));
String challURL = IdeaManagementSystemProperties.getFriendlyUrlChallenges(challengeId);

List<CLSIdea> ideas = IdeasListUtils.getIdeasByChallengeIdOrderedByEvaluationScore(challengeId);

boolean isChallengeAuthor = ChallengesUtils.isAuthorByChallengeId(challengeId, currentUser);
String challengeName = ChallengesUtils.getChallengeNameByChallengeId(challengeId);
boolean isChallengeInEvaluation = ChallengesUtils.isChallengeInEvaluation(challengeId);
boolean isChallengeEvaluated = ChallengesUtils.isChallengeEvaluated(challengeId);

List<EvaluationCriteria> barrierCriteria = EvaluationCriteriaLocalServiceUtil.getEnabledCriteriaByChallengeIdAndIsBarriera(challengeId, true);
List<EvaluationCriteria> quantitativeCriteria = EvaluationCriteriaLocalServiceUtil.getEnabledCriteriaByChallengeIdAndIsBarriera(challengeId, false);

boolean isEvaluationClosable = ChallengesUtils.isEvaluationClosableByChallengeId (challengeId);


%>


<portlet:defineObjects />

<div class="materialize">

 	
	<%@ include file="sections/idea_evaluation_result.jspf" %>
	<%@ include file="sections/evaluationReport_edit.jspf" %>

	<div class="row  valign-wrapper">
		<div class="col s2 left-align">
		 	<a class="waves-effect btn-flat tooltipped" href="<%=challURL %>"  data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.go-back"/>">
	  			<i class="icon-arrow-left left">&nbsp;</i><liferay-ui:message key="back"/>
	  		</a>
		</div>

		<div id="parentRef" class="col s8 center">
					<h4><i class="material-icons icon-trophy small tooltipped" data-position="top" data-delay="40" data-tooltip="<liferay-ui:message key="ims.challenge"/>">&nbsp;</i>
					<a href="<%=challURL%>" target="_blank">  
						<%=challengeName %>
					 </a>
					</h4>
		</div>

		<div class="col s2 right-align">
<%-- 		 	<a class="waves-effect btn-flat tooltipped gobackChall" href="#"  data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.go-back"/>"> --%>
<%-- 		  		<i class="icon-arrow-right right">&nbsp;</i><liferay-ui:message key="back"/> --%>
<!-- 		  	</a> -->
		</div>
		
	</div>

	<div class="container">
		<div class="row">
		
		<!-- TABS -->
			<div class="col s12">
			  <ul class="tabs">
			    <li class="tab col s3"><a class="active" href="#ideascore"><liferay-ui:message key="ims.ideas-scoreboard"/></a></li>
			    <li class="tab col s3"><a class="" href="#evalcriteria"><liferay-ui:message key="ims.evaluation-criteria"/></a></li>
			    <li class="tab col s3"><a class="" href="#evaldoc"><liferay-ui:message key="ims.rules-of-the-game"/></a></li>
			  </ul>
			</div>
		  
		<!-- 	SCOREBOARD & EVALUATON REPORT TAB  -->
			<div id="ideascore" class="col s12 top-margin">
			
				<div class="row">
					<div class="col s12">
						<blockquote>
					      
					      <%if (isChallengeInEvaluation){  %>
					      
					      <liferay-ui:message key="ims.tip-evaluation-click-insert"/>
					     					      
					       <%}else{  %>
					      
		 				<liferay-ui:message key="ims.tip-evaluation-click-view"/>
					      					       
					       <%}  %>
					       
					    </blockquote> 
          
						<table class="highlight centered responsive-table bordered" >
						  <thead>
						    <tr class="core-color color-2 white-text">
						        <th  class="weight-7" data-field="id"><liferay-ui:message key="ranking"/></th>
						        <th  class="weight-7" data-field="name"><liferay-ui:message key="ims.idea-title"/></th>
						        <th  class="weight-7" data-field="score"><liferay-ui:message key="score"/></th>
						        <th  class="weight-7" data-field="result"><liferay-ui:message key="ims.selection-result"/></th>
						        
						        <%if (isChallengeAuthor &&  isChallengeEvaluated){  %>
						        
						        <th  class="weight-7" data-field="state"><liferay-ui:message key="ims.refinement"/></th>
						        
						        <%}  %>
						    </tr>
						  </thead>
						
						  <tbody>
						  
<%if (ideas.size() == 0){  %>

						    <tr class="" >
						    	<td>-</td>
							    <td><i><liferay-ui:message key="ims.no-idea"/></i></td>
							    <td>-</td>
							    <td>-</td> 
							    <td>-</td>
							</tr>
						  
<%
}else{
	for (int i =0; i<ideas.size(); i++){ 
		CLSIdea idea = ideas.get(i);
		
		boolean isEvaluation = IdeasListUtils.isIdeaToEvaluate(idea.getIdeaID()) && isChallengeAuthor;
		List<IdeaEvaluation> bEvals = IdeasListUtils.getBarrierIdeaEvaluationByIdeaId( idea.getIdeaID());
		List<IdeaEvaluation> qEvals = IdeasListUtils.getQuantitativeIdeaEvaluationByIdeaId( idea.getIdeaID());
		double totalScoreIdea = IdeasListUtils.getTotalScoreIdeaByIdeaId(idea.getIdeaID());
		String frUrlIdea = IdeaManagementSystemProperties.getFriendlyUrlIdeas(idea.getIdeaID());
		boolean isIdeaOnRefinementOrImplementationOrMonitoring = IdeasListUtils.isIdeaOnRefinementOrImplementationOrMonitoring(idea.getIdeaID());
%>						  
						    <tr 
							    data-ididea="<%=idea.getIdeaID()%>" 
							    data-ideatitle="<%=idea.getIdeaTitle()%>" 
							    data-ideaurl="<%=frUrlIdea%>" 
							    
							    data-averageratings="<%= IdeasListUtils.getAverageDoubleRatingsByIdeaOrNeedId(idea.getIdeaID())%>" 
							    data-nvote="<%=IdeasListUtils.getNumberOfRatingsByIdeaOrNeedId(idea.getIdeaID())%>" 
							    data-ideafinalmotivation="<%=idea.getFinalMotivation()%>" 
							    data-evaluationpassed="<%=idea.getEvaluationPassed()%>" 
							    data-totalscoreidea="<%=totalScoreIdea%>" 
							    
							 <% for(int x =0; x<qEvals.size(); x++){   %>
							 	 data-scoreq<%=x%>="<%=qEvals.get(x).getScore()%>"
							 	 data-motivationq<%=x%>="<%=qEvals.get(x).getMotivation()%>"
							 
							 <% }  
							    
							   for(int z =0; z<bEvals.size(); z++){   %>
							 	 data-ispassedb<%=z%>="<%=bEvals.get(z).getPassed()%>"
							 	 data-motivationb<%=z%>="<%=bEvals.get(z).getMotivation()%>"
							 
							 <% }  %>    
							  >
						     
						      <td><%=i+1 %></td>
						      <td class="openIdeaDetails tooltipped" data-position="top" data-delay="40"  data-tooltip="<liferay-ui:message key="ims.click-for-idea-details"/>" >
						      
						      
						      <%=idea.getIdeaTitle() %>
						      
						      
						      
						      </td>
						      <td class=
						      
							     <% if(isEvaluation){   %>
								    "openDetailsEval tooltipped" data-tooltip="<liferay-ui:message key="ims.click-to-evaluate-the-idea"/>"
								    
								  <% }else{  %>
								  	"openDetails tooltipped" data-tooltip="<liferay-ui:message key="ims.click-for-the-idea-evaluation-details"/>"
								  	
								  <% }  %>  data-position="top" data-delay="40"   >
								  <%=totalScoreIdea %> <liferay-ui:message key="ims.points"/>
							  </td>
							  
						      <td class=
						      
							     <% if(isEvaluation){   %>
								    "openDetailsEval tooltipped" data-tooltip="<liferay-ui:message key="ims.click-to-evaluate-the-idea"/>"
								    
								  <% }else{  %>
								  	"openDetails tooltipped" data-tooltip="<liferay-ui:message key="ims.click-for-the-idea-evaluation-details"/>"
								  	
								  <% }  %>  
								  
						      data-position="top" data-delay="40" >
						      
						      
						       <% if (idea.getFinalMotivation().isEmpty()){ %>
						      		<i class="material-icons ">thumb_up</i><i class="material-icons text-accent-3">thumb_down</i>
						      		<label><liferay-ui:message key="ims.to-evaluate"/></label>
						      <% }else if (idea.getEvaluationPassed()){ %>
						      		<i class="material-icons light-green-text">thumb_up</i>
						      		<label><liferay-ui:message key="ims.accepted"/></label> 
						      	<%}else{ %>
						      		<i class="material-icons deep-orange-text text-accent-3">thumb_down</i>
						      		<label><liferay-ui:message key="ims.rejected"/></label> 
						      	<%} %>
						      		
						      </td> 
						      
						    <%if (isChallengeAuthor && isChallengeEvaluated){  %>
						      
						      <td>
						      
						      <%if (!idea.getEvaluationPassed()){%>
						      -
						      
						      <% } else if (!isIdeaOnRefinementOrImplementationOrMonitoring){  
						    	  PortletURL goToRefinement = renderResponse.createRenderURL();
						    	  	 goToRefinement.setParameter("companyId", Long.toString(currentUser.getCompanyId()));
						    	 	 goToRefinement.setParameter("userId", Long.toString(currentUser.getUserId()));
						    	 	 goToRefinement.setParameter("transictionName", MyConstants.IDEA_STATE_REFINEMENT);
						    	 	 goToRefinement.setParameter("ideaId", Long.toString(idea.getIdeaID()));
						    		 goToRefinement.setParameter("jspPage", "/html/ideasexplorermaterial/ideasdetails/sections/requirements/pages/requirements.jsp");
						      %>
						      
								    <aui:form cssClass="btnAction" method="POST" action="<%= goToRefinement.toString() %>" name="fm_goToRefinement">
										<aui:input name="challengeId" type="hidden" value="<%=challengeId%>"></aui:input>
									</aui:form>
						
						
									<a class="waves-effect waves-light btn-flat tooltipped" onclick="goToRefinementConfirm()"  data-position="top" data-delay="40"  data-tooltip="<liferay-ui:message key="ims.tip-go-to-refinement"/>">
						      			<i class="material-icons icon-retweet left"></i><liferay-ui:message key="ims.select"/>
						      		</a>
						      
						      <%}else{ %> 
						      	 
						      	 <span class="tooltipped" data-position="top" data-delay="40"  data-tooltip="<liferay-ui:message key="ims.tip-idea-in-refinement-ok"/>"><liferay-ui:message key="done"/></span>
						      	 
						      <%} %> 
						      	
						      	
						      	
						      </td>
						      
						     <%} %> 
						      
						    </tr>
<%	}
}%>
						    

						  </tbody>
						</table>
					</div>
				</div>
				
<%if(isEvaluationClosable && isChallengeAuthor ){

	PortletURL closeEvaluationURL = renderResponse.createActionURL();
	closeEvaluationURL.setParameter(ActionRequest.ACTION_NAME, "closeEvaluation");
%>				
				<div class="row">
					<div class="col s12">
						<aui:form cssClass="btnAction" method="POST" action="<%= closeEvaluationURL.toString() %>" name="fm_closeEvaluation">
							<aui:input name="challengeId" type="hidden" value="<%=challengeId%>"></aui:input>
							<button type="button" class="waves-effect waves-light btn  core-color color-1 tooltipped" 
								onclick="closeEvaluationConfirm()" data-position="top" data-delay="40"  data-tooltip="<liferay-ui:message key="ims.close-evaluation"/>"> 
								<i class="material-icons small left">lock</i><liferay-ui:message key="ims.close-evaluation"/>
							</button>
						</aui:form>
					</div>
				</div>
<%}%>
		
			</div><!-- fine ideaScore tab -->
			
			<!-- 	CRITERIA TAB  -->
		  	<div id="evalcriteria" class="col s12">
				<%@ include file="sections/evaluationCriteria.jspf" %>
			</div>
			
			<!-- 	DOCUMENTATION TAB  -->
		  	<div id="evaldoc" class="col s12">
				<%@ include file="sections/evaluationDocumentation.jspf" %>
			</div>
		</div><!-- fine row TABS -->

	</div><!-- fine Container -->
	

</div>



 <script>
 	$(document).ready(function(){
	    /* the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered */
	    $('.modal-trigger-evaluation').leanModal();
	 });
 	
 	var numberB = <%=barrierCriteria.size() %>;
 	var numberQ = <%=quantitativeCriteria.size() %>;
 	
 	/*Evaluation process */
 	$(document).on('click', ".openDetailsEval", function(el){
 		
 		var wd=$(window).width(), hd=$(window).height();
 		console.log(wd+" "+hd);
 		
 		if(wd-hd < 0){
 			Materialize.toast("<liferay-ui:message key='ims.eval-change-orientation'/>", 5000);
 		}else{
 		
	 <%if (!isChallengeInEvaluation){ %>		
	 		return ;
	 <%} %>
	 	
	 		/*Set the idea title on the popup*/
	 		document.getElementById('ideaTitlePopup').innerHTML="'"+$(this).parent().data('ideatitle')+"'"; 
	 		$('#ideaTitlePopup').attr('href',$(this).parent().data('ideaurl'));
	 		
	 		document.getElementById('averageScorePopup').innerHTML=$(this).parent().data('averageratings');
	 		document.getElementById('nVotePopup').innerHTML=$(this).parent().data('nvote');
	 		$('#evalIdeaId').val($(this).parent().data('ididea'));
	 		
	 		//fill the fields in update case
	 		for (i = 0; i< numberB; i++ ){
	 			
	 			var currentMotb = $(this).parent().data('motivationb'+i);
	 			
	 			if ( typeof  currentMotb != 'undefined' ){			
	 				document.getElementById('bt'+i).innerHTML=currentMotb;
	 			} else{
	 				
	 				document.getElementById('bt'+i).innerHTML=""; /*Solve click on row evaluated and later in a row to eavaluate*/
	 				
	 			}
	 			var isCriterioPassed = $(this).parent().data('ispassedb'+i);
	 			
	 			if ( isCriterioPassed == true)
	 				document.getElementById('bc'+i).checked = true;
	 		}
	 		
	 		for (i = 0; i< numberQ; i++ ){
	 			
	 			var currentScoreq = $(this).parent().data('scoreq'+i);
	 			var currentMotq = $(this).parent().data('motivationq'+i);
	 			
	 			console.log ("i "+i +" currentMotq "+currentMotq);
	 			
	 			if ( typeof  currentScoreq != 'undefined' ) 
	 				document.getElementById('qs'+i).value=currentScoreq;
	 			
	 			if ( typeof  currentMotq != 'undefined' ) {
	 				document.getElementById('qt'+i).innerHTML=currentMotq;
	 			}else{
	 				document.getElementById('qt'+i).innerHTML="";
	 			}
	 		}
	 		
	 		
	 		document.getElementById('finalMotivationIdea').innerHTML=$(this).parent().data('ideafinalmotivation');
	 		document.getElementById('totalScoreCalc').innerHTML=$(this).parent().data('totalscoreidea');
	 		
	 		var isEvaluationPassed =$(this).parent().data('evaluationpassed');
	 		if (isEvaluationPassed == true)
	 			document.getElementById('totalResultOK').checked=true;
	 		else
	 			document.getElementById('totalResultKO').checked=true;
	 		
	 		
	 		
	 		$('#modalEvaluationEdit').openModal({
					dismissible: false, 
	  				starting_top: '5%', /* Starting top style attribute*/
				    ending_top: '5%' /* Ending top style attribute*/
				 });
	 		
	 		
 		}//END IF-ELSE
 	});
 	
 	
 	
 	
 	/*Evaluation result */
 	$(document).on('click', ".openDetails", function(el){
 		
 		if ($(this).parent().data('ideafinalmotivation') == "")
 			return ;
 		
 		document.getElementById('ideaTitlePopupResult').innerHTML="'"+$(this).parent().data('ideatitle')+"'";
 		$('#ideaTitlePopupResult').attr('href',$(this).parent().data('ideaurl'));
 		
 		document.getElementById('averageScorePopup').innerHTML=$(this).parent().data('averageratings');
 		document.getElementById('nVotePopup').innerHTML=$(this).parent().data('nvote');
 		

 		//fill the fields in update case
 		for (i = 0; i< numberB; i++ ){
 			
 			var currentMotb = $(this).parent().data('motivationb'+i);
 			
 			if ( typeof  currentMotb != 'undefined' ) 			
 				document.getElementById('rbt'+i).innerHTML=currentMotb;
 			
 			var isCriterioPassed = $(this).parent().data('ispassedb'+i);
 			
 			if ( isCriterioPassed == true)
 				document.getElementById('rbc'+i).innerHTML='<liferay-ui:message key="yes"/>';
 			else
 				document.getElementById('rbc'+i).innerHTML='<liferay-ui:message key="no"/>';
 		}
 		
 		//fill the fields in update case
 		for (i = 0; i< numberQ; i++ ){
 			
 			var currentMotq = $(this).parent().data('motivationq'+i);
 			
 			if ( typeof  currentMotb != 'undefined' ) 			
 				document.getElementById('rqt'+i).innerHTML=currentMotq;
 			
 			if ( typeof  currentMotb != 'undefined' ) 	
 				document.getElementById('rqs'+i).innerHTML= $(this).parent().data('scoreq'+i);;
 		}
 		
 		
		document.getElementById('totalScoreCalcR').innerHTML=$(this).parent().data('totalscoreidea');
 		
 		var isEvaluationPassed =$(this).parent().data('evaluationpassed');
 		if (isEvaluationPassed == true)
 			document.getElementById('totalResult').innerHTML='<i class="material-icons light-green-text">thumb_up</i>&nbsp; <liferay-ui:message key="ims.accepted"/>';
 		else
 			document.getElementById('totalResult').innerHTML='<i class="material-icons deep-orange-text text-accent-3">thumb_down</i>&nbsp; <liferay-ui:message key="ims.rejected"/>';
 		
 		document.getElementById('finalMotivationPopupResult').innerHTML=$(this).parent().data('ideafinalmotivation');
 		
 		
  		$('#modalReport').openModal({
  				starting_top: '5%', /* Starting top style attribute*/
			    ending_top: '5%' /* Ending top style attribute*/
		 });
	
 		
 	});
 	
 	
 	/*Evaluation result */
 	$(document).on('click', ".openIdeaDetails", function(el){
		
 		/*Idea Details page in a new tab */
 		window.open($(this).parent().data('ideaurl'));
 		
 	});
 		
 	
 	function goToRefinementConfirm(){
 		
 		var msg = "<liferay-ui:message key="ims.alert-go-to-refinement"/>";
 		 var r=confirm(msg);

 	    if (r==true) {
 	        $( '#<portlet:namespace/>fm_goToRefinement' ).submit();
 	    }

 	}
 	
 	function closeEvaluationConfirm(){
 	    
 		var msg ="<liferay-ui:message key="ims.alert-close-evaluation"/>";
 		
 		 var r=confirm(msg);

 	    if (r==true) {
 	        $( '#<portlet:namespace/>fm_closeEvaluation' ).submit();
 	    }
 	}
 	
 </script>         