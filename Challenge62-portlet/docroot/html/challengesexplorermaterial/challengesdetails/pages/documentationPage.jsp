<%@page import="it.eng.rspa.ideas.utils.ChallengesUtils"%>
<%@page import="it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.EvaluationCriteriaLocalServiceUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria"%>

<%@ include file="/html/challenges/init.jsp" %>

<%
User currentUser = PortalUtil.getUser(request);
long challengeId = Long.parseLong(request.getParameter("challengeId"));

String challURL = IdeaManagementSystemProperties.getFriendlyUrlChallenges(challengeId);
String challengeName = ChallengesUtils.getChallengeNameByChallengeId(challengeId);
boolean isEvaluationOpenable = ChallengesUtils.isEvaluationOpenable (challengeId);
boolean isAuthor = ChallengesUtils.isAuthorByChallengeId(challengeId, currentUser);

%>

<div class="materialize">


	<div class="row  valign-wrapper">
		<div class="col s2 left-align">
		 	<a class="waves-effect btn-flat tooltipped" href="<%=challURL %>"  data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.go-back"/>">
	  			<i class="icon-arrow-left left">&nbsp;</i><liferay-ui:message key="back"/>
	  		</a>
		</div>

		<div id="parentRef" class="col s8 center">
					<h4><i class="material-icons icon-trophy small tooltipped" data-position="top" data-delay="40" data-tooltip="<liferay-ui:message key="ims.challenge"/>">&nbsp;</i>
					<a href="<%=challURL%>" target="_blank">  
						<%=challengeName %>
					 </a>
					</h4>
		</div>

		<div class="col s2 right-align">

		</div>
		
	</div>


<div class="container">
<blockquote>
<liferay-ui:message key="ims.tip-evaluation-details"/>
</blockquote>

<%if(isEvaluationOpenable && isAuthor ){%>


				<%
					PortletURL openEvaluationURL = renderResponse.createActionURL();
					openEvaluationURL.setParameter(ActionRequest.ACTION_NAME, "openEvaluation");
				%>
						
						
					
						<aui:form cssClass="btnAction" method="POST" action="<%= openEvaluationURL.toString() %>" name="fm_openEvaluation">
							<aui:input name="challengeId" type="hidden" value="<%=challengeId%>"></aui:input>
							<button type="button" class="btn-flat" onClick='openEvaluationConfirm()'>
								<i class="material-icons small left">lock_open</i>
								<liferay-ui:message key="ims.open-evaluation"/>
							</button>
						</aui:form>
				 	


<%}%>

<div class="row">


</div>
		<div class="row">
		
		<!-- TABS -->
			<div class="col s12">
			  <ul class="tabs">
			    <li class="tab col s3"><a class="" href="#evalcriteria"><liferay-ui:message key="ims.evaluation-criteria"/></a></li>
			    <li class="tab col s3"><a class="" href="#evaldoc"><liferay-ui:message key="ims.rules-of-the-game"/></a></li>
			  </ul>
			</div>

			
			<!-- 	CRITERIA TAB  -->
		  	<div id="evalcriteria" class="col s12">
				<%@ include file="sections/evaluationCriteria.jspf" %>
			</div>
			
			<!-- 	DOCUMENTATION TAB  -->
		  	<div id="evaldoc" class="col s12">
				<%@ include file="sections/evaluationDocumentation.jspf" %>
			</div>
		</div><!-- fine row TABS -->

	</div><!-- fine Container -->


</div>


<script>
function openEvaluationConfirm(){
    var r=confirm('<liferay-ui:message key="ims.alert-open-evaluation"/>');

    if (r==true) {
        $( '#<portlet:namespace/>fm_openEvaluation' ).submit();
    }
}
</script>


