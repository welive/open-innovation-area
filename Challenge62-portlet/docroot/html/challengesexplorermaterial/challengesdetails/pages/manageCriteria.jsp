<%@page import="it.eng.rspa.ideas.utils.MyConstants"%>
<%@page import="it.eng.rspa.ideas.utils.MyUtils"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.EvaluationCriteriaLocalServiceUtil"%>
<%@page import="it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties"%>
<%@page import="it.eng.rspa.ideas.utils.ChallengesUtils"%>
<%@ include file="/html/challenges/init.jsp" %>

<%
User currentUser = PortalUtil.getUser(request);

long challengeId = Long.parseLong(request.getParameter("challengeId"));
String challURL = IdeaManagementSystemProperties.getFriendlyUrlChallenges(challengeId);
String challengeName = ChallengesUtils.getChallengeNameByChallengeId(challengeId);


PortletURL saveCriteria = renderResponse.createActionURL();
	saveCriteria.setParameter(ActionRequest.ACTION_NAME,"manageCriteria"); 
	saveCriteria.setParameter("challengeId", request.getParameter("challengeId"));
	
 List<EvaluationCriteria> customBarrierCriteria = EvaluationCriteriaLocalServiceUtil.getCriteriaByChallengeIdAndIsBarrieraAndIsCustom(challengeId, true, true);
 List<EvaluationCriteria> customQuantitativeCriteria = EvaluationCriteriaLocalServiceUtil.getCriteriaByChallengeIdAndIsBarrieraAndIsCustom(challengeId, false, true);	

 %>


<!-- EDIT CRITERIA TABLE -->
<div class="materialize">

	<div class="row  valign-wrapper">
		<div class="col s2 left-align">
		 	<a class="waves-effect btn-flat tooltipped gobackChall" href="#"  data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.go-back"/>">
		  		<i class="icon-arrow-left left">&nbsp;</i><liferay-ui:message key="back"/>
		  	</a>
		</div>
	
		<div id="parentRef" class="col s8 center">
					<h4><i class="material-icons icon-trophy small tooltipped" data-position="top" data-delay="40" data-tooltip="<liferay-ui:message key="ims.challenge"/>">&nbsp;</i>
					<a href="<%=challURL %>" target="_blank">  
						<%=challengeName %>
					 </a>
					</h4>
		</div>
		
		<div class="col s2 right-align">

		</div>
		
	</div>
	
<div class="container">
	<div class="row">
		
		<!-- TABS -->
			<div class="col s12">
			  <ul class="tabs">
			    <li class="tab col s6"><a class="active" href="#ideaCriteria"><liferay-ui:message key="ims.ideas-scoreboard"/></a></li>
			    <li class="tab col s6"><a class="" href="#evaldoc"><liferay-ui:message key="ims.rules-of-the-game"/></a></li>
			  </ul>
			</div>
	
	<!-- 	Idea/Challenge Criteria TAB  -->
		<div id="ideaCriteria" class="col s12 top-margin">
			<form method="POST" name="fmCriteria" id="fmCriteria" action="<%= saveCriteria.toString() %>">
		
			<div class="row">
				<div class="col s12">
					<h5 class="flow-text truncate">
						<b>&nbsp;<i class="fa fa-pencil-square-o" aria-hidden="true">&nbsp;</i><liferay-ui:message key="ims.creation-and-management-of-the-criteria-for-selection-of-ideas"/></b>
					</h5>
			
			
			<!-- BARRIER CRITERIA -->	
					
					<table class="detailsTable highlight responsive-table bordered" >
					  
					  <thead class="">
					    <tr class="core-color color-1">
					    	<th id="" class="num_edit weight-7 center-align" data-field="num"><liferay-ui:message key="selected"/></th> 
					        <th id="" class="criterio_edit weight-7" data-field="criterio"><liferay-ui:message key="ims.barrier-criteria"/></th>
					        <th id="" class="criterio_edit weight-7"><liferay-ui:message key="delete"/></th>
					    </tr>
					  </thead>
					  
					  
					
						<tbody class="containerRowBarrier">
						
						  <tr id="fake_br3" class="rowCustom" style="display:none"  >
						    <td class="center-align">
						    	<div class="switch">
								    <label><liferay-ui:message key="no"/><input class="rowInputCheckbox" name="<portlet:namespace/>fake_bc3" type="checkbox" checked><span class="lever"></span><liferay-ui:message key="yes"/></label>
								</div>					
							</td>
						    <td class=""><textarea  name="<portlet:namespace/>fake_bt3" placeholder="<liferay-ui:message key="ims.write-a-criteria"/>" class="materialize-textarea rowTextCriteria"></textarea></td>
						    <input hidden="true" class="rowInputIsCustom" name="<portlet:namespace/>fake_bic3" value="true">
						    <input hidden="true" class="rowInputCriteriaId" name="<portlet:namespace/>fake_bcId3" value="">
						    <td class="center-align">
									<a class="rowDeleteCriteria waves-effect waves-light  btn-flat btn-large" id="fake_bd3" onclick="deleteCriteriaRow('b',3)" > <i class="material-icons">cancel</i> </a>
									<input hidden="true" class="rowInputToDelete" id="fake_bdi3" name="<portlet:namespace/>fake_bdi3" value="false">				
							</td>
						  </tr>  
						  
						  
						
						  <tr class="">
						    <td class="center-align">  
						    	<div class="switch">
								    <label><liferay-ui:message key="no"/><input name="<portlet:namespace/>bc0" type="checkbox" <%=ChallengesUtils.isEnabledCriteria(challengeId, "bcId0") ? "checked":"" %>  ><span class="lever"></span><liferay-ui:message key="yes"/></label>
								</div>
							</td>
						    <td class=""><liferay-ui:message key="ims.is-the-proposed-idea-pertinent-to-the-scope-of-the-challenge"/></td>
						    <input hidden="true"  name="<portlet:namespace/>bt0" value="<liferay-ui:message key="ims.is-the-proposed-idea-pertinent-to-the-scope-of-the-challenge"/>">
						    <input hidden="true"  name="<portlet:namespace/>bic0" value="false">
						    <td class="center-align">
					   				<a class="btn-flat disabled"><i class="material-icons">cancel</i></a> 
							</td>
						  </tr>
						  
						  <tr class="">
						    <td class="center-align">
						    	<div class="switch">
								    <label><liferay-ui:message key="no"/><input name="<portlet:namespace/>bc1" type="checkbox" <%=ChallengesUtils.isEnabledCriteria(challengeId, "bcId1") ? "checked":"" %>  ><span class="lever"></span><liferay-ui:message key="yes"/></label>
								</div>
							</td>
						    <td class=""><liferay-ui:message key="ims.can-the-municipality-contribute-in-the-refinement-of-the-proposed-idea"/></td>
						    <input hidden="true"  name="<portlet:namespace/>bt1" value="<liferay-ui:message key="ims.can-the-municipality-contribute-in-the-refinement-of-the-proposed-idea"/>">
						    <input hidden="true"  name="<portlet:namespace/>bic1" value="false">
						    <td class="center-align">
					   				<a class="btn-flat disabled"><i class="material-icons">cancel</i></a> 
							</td>
						  </tr>
						  
						  <tr class="">
						    <td class="center-align">
						    	<div class="switch">
								    <label><liferay-ui:message key="no"/><input name="<portlet:namespace/>bc2" type="checkbox"  <%=ChallengesUtils.isEnabledCriteria(challengeId, "bcId2") ? "checked":"" %> ><span class="lever"></span><liferay-ui:message key="yes"/></label>
								</div>					
							</td>
						    <td class=""><liferay-ui:message key="ims.can-the-proposed-idea-be-addressed-through-the-creation-and-publication-of-a-new-public-service"/></td>
						     <input hidden="true"  name="<portlet:namespace/>bt2" value="<liferay-ui:message key="ims.can-the-proposed-idea-be-addressed-through-the-creation-and-publication-of-a-new-public-service"/>">
						     <input hidden="true"  name="<portlet:namespace/>bic2" value="false">
						     <td class="center-align">
					   				<a class="btn-flat disabled"><i class="material-icons">cancel</i></a> 
							</td>
						  </tr>
						  
					
					<% 
					int indexRow = MyConstants.N_OF_STATIC_BARRIER_CRITERIA;
					if (customBarrierCriteria.size()==0){ %>
					
						 <tr id="br3"  class="">
						    <td class="center-align">
						    	<div class="switch">
								    <label><liferay-ui:message key="no"/><input name="<portlet:namespace/>bc3" type="checkbox" checked><span class="lever"></span><liferay-ui:message key="yes"/></label>
								</div>					
							</td>
						    <td class=""><textarea  name="<portlet:namespace/>bt3" placeholder="<liferay-ui:message key="ims.write-a-criteria"/>" class="materialize-textarea"></textarea></td>
						    <input hidden="true"  name="<portlet:namespace/>bic3" value="true">
						    <input hidden="true"  name="<portlet:namespace/>bcId3" value="">
						    <td class="center-align">
					   				<a class="rowDeleteCriteria waves-effect waves-light  btn-flat btn-large" id="bd3" onclick="deleteCriteriaRow('b',3)"> <i class="material-icons">cancel</i> </a>
							</td>
							<input hidden="true" class="rowInputToDelete" id="bdi3" name="<portlet:namespace/>bdi3" value="false">				
						  </tr>  
						  
					<% }		  
					for (int i =0 ; i<customBarrierCriteria.size(); i ++ ){ 
							indexRow = i+MyConstants.N_OF_STATIC_BARRIER_CRITERIA;
					%>
						  <tr id="br<%=indexRow %>"  class="" >
						    <td class="center-align">
						    	<div class="switch">
								    <label><liferay-ui:message key="no"/><input class="rowInputCheckbox" name="<portlet:namespace/>bc<%=indexRow %>" type="checkbox" <%=customBarrierCriteria.get(i).getEnabled() ? "checked":"" %> ><span class="lever"></span><liferay-ui:message key="yes"/></label>
								</div>					
							</td>
						    <td class=""><textarea  name="<portlet:namespace/>bt<%=indexRow %>" placeholder="<liferay-ui:message key="ims.write-a-criteria"/>" class="materialize-textarea rowTextCriteria"><%=customBarrierCriteria.get(i).getDescription() %></textarea></td>
						    <input hidden="true" class="rowInputIsCustom" name="<portlet:namespace/>bic<%=indexRow %>" value="true">
						    <input hidden="true" class="rowInputCriteriaId" name="<portlet:namespace/>bcId<%=indexRow %>" value="<%=customBarrierCriteria.get(i).getCriteriaId() %>">
							<td class="center-align">
					   				<a class="rowDeleteCriteria waves-effect waves-light  btn-flat btn-large" id="bd<%=indexRow %>" onclick="deleteCriteriaRow('b',<%=indexRow %>)"> <i class="material-icons">cancel</i> </a>
							</td>
								<input hidden="true" class="rowInputToDelete" id="bdi<%=indexRow %>" name="<portlet:namespace/>bdi<%=indexRow %>" value="false">				
						  </tr>
						  
				  <% }%>	
				  
				  <input hidden="true" id="numBarriera" name="<portlet:namespace/>numBarriera" value="<%=indexRow%>"/>
				  
						</tbody>
						
					</table>
								  
				    <div class=" section bottom-margin center-align">
						    <a class="waves-effect waves-light btn" id="addBarrierCriteria" ><i class="material-icons left">add</i><liferay-ui:message key="ims.add-criteria"/></a>
					</div>
		
		
		
		
					
					
					
			<!-- QUANTITATIVE CRITERIA -->		
					
					<table class="detailsTable highlight responsive-table bordered" >
					  
					  
				<thead class="">
				    <tr class="core-color color-1">
				    	<th id="" class="num_edit weight-7 center-align" data-field="num"><liferay-ui:message key="selected"/></th>
				    	<th id="" class="num_edit weight-7 center-align" data-field="weight"><liferay-ui:message key="ims.weight"/></th>
				    	<th id="" class="num_edit weight-7 center-align" data-field="threshold"><liferay-ui:message key="ims.threshold"/></th> 
				        <th class="criterio_edit weight-7" data-field="criterio"><liferay-ui:message key="ims.quantitative-criteria"/></th>
				        <th class="criterio_edit weight-7" ><liferay-ui:message key="delete"/></th>
				    </tr>
				  </thead>
					  
					
						<tbody class="containerRowQuantitative">
						
							<tr class="rowCustomQ" id="fake_qr3" style="display:none"  >
							    <td class="center-align">
									<input class="rowInputCheckbox" name="<portlet:namespace/>fake_qc3" id="fake_qc3" type="checkbox" checked >
			      					<label class="forRowInputCheckbox" for="fake_qc3"></label>
								</td>
							<td>
					       		<input class="rowInputWeigth" placeholder="<liferay-ui:message key="ims.tip-value-weight"/>" name="<portlet:namespace/>fake_w0" type="text" class="validate">
				        	</td>
				        	<td>
					       		<input class="rowInputThreshold" placeholder="<liferay-ui:message key="ims.tip-value-threshold"/>" name="<portlet:namespace/>fake_t0" type="text" class="validate">
				        	</td>
							    <td class=""><textarea  name="<portlet:namespace/>fake_qt3" placeholder="<liferay-ui:message key="ims.write-a-criteria"/>" class="materialize-textarea rowTextCriteria"></textarea></td>
							    <input hidden="true" class="rowInputIsCustom" name="<portlet:namespace/>fake_qic3" value="true">
							    <input hidden="true" class="rowInputCriteriaId" name="<portlet:namespace/>fake_qcId3" value="">
							    <td class="center-align">
									<a class="rowDeleteCriteria waves-effect waves-light  btn-flat btn-large" id="fake_qd3" onclick="deleteCriteriaRow('q',3)"> <i class="material-icons">cancel</i> </a>
								</td>
								 <input hidden="true" class="rowInputToDelete" id="fake_qdi3" name="<portlet:namespace/>fake_qdi3" value="false">
						  </tr> 
						
						
						<tr class=" ">
						  
						  	<td class="center-align">
								<input name="<portlet:namespace/>qc0" id="qc0" type="checkbox"  checked="checked" onclick="return false" onkeydown="return false" >
		      					<label for="qc0"></label>
							</td>
							<td>
					       		<input placeholder="<liferay-ui:message key="ims.tip-value-weight"/>" id="w0" value="<%=ChallengesUtils.getCriteriaDetailsByChallengeInputType(challengeId, "qcId0", "w") %>" name="<portlet:namespace/>w0" type="text" class="validate">
				        	</td>
				        	<td class="center">
					       		-
				        	</td>
					    	<td class=""><liferay-ui:message key="ims.citizens-vote"/></td> 
						    	<input hidden="true"  name="<portlet:namespace/>qt0" value="<liferay-ui:message key="ims.citizens-vote"/>">
						     	<input hidden="true"  name="<portlet:namespace/>qic0" value="false">
						  	<td class="center-align">
					   				<a class="btn-flat disabled"><i class="material-icons">cancel</i></a> 
							</td>
						  </tr>
						
						
						  <tr class=" ">
						  
						  	<td class="center-align">
								<input name="<portlet:namespace/>qc1" id="qc1" type="checkbox" <%=ChallengesUtils.isEnabledCriteria(challengeId, "qcId1") ? "checked":"" %> >
		      					<label for="qc1"></label>
							</td>
							<td>
					       		<input placeholder="<liferay-ui:message key="ims.tip-value-weight"/>" id="w1" value="<%=ChallengesUtils.getCriteriaDetailsByChallengeInputType(challengeId, "qcId1", "w") %>" name="<portlet:namespace/>w1" type="text" class="validate">
				        	</td>
				        	<td>
					       		<input placeholder="<liferay-ui:message key="ims.tip-value-threshold"/>" id="t1" value="<%=ChallengesUtils.getCriteriaDetailsByChallengeInputType(challengeId, "qcId1", "t") %>" name="<portlet:namespace/>t1" type="text" class="validate">
				        	</td>
					    	<td class=""><liferay-ui:message key="ims.how-much-the-proposed-idea-is-feasible-in-socioeconomic-terms"/></td> 
						    	<input hidden="true"  name="<portlet:namespace/>qt1" value="<liferay-ui:message key="ims.how-much-the-proposed-idea-is-feasible-in-socioeconomic-terms"/>">
						     	<input hidden="true"  name="<portlet:namespace/>qic1" value="false">
						  	<td class="center-align">
					   				<a class="btn-flat disabled"><i class="material-icons">cancel</i></a> 
							</td>
						  </tr>
						  
						  <tr class="">
							  <td class="center-align">
									<input name="<portlet:namespace/>qc2" id="qc2" type="checkbox" <%=ChallengesUtils.isEnabledCriteria(challengeId, "qcId2") ? "checked":"" %> >
			      					<label for="qc2"></label>
								</td>
								<td>
						       		<input placeholder="<liferay-ui:message key="ims.tip-value-weight"/>" id="w2" value="<%=ChallengesUtils.getCriteriaDetailsByChallengeInputType(challengeId, "qcId2", "w") %>" name="<portlet:namespace/>w2" type="text" class="validate">
					        	</td>
					        	<td>
						       		<input placeholder="<liferay-ui:message key="ims.tip-value-threshold"/>" id="t2" value="<%=ChallengesUtils.getCriteriaDetailsByChallengeInputType(challengeId, "qcId2", "t") %>" name="<portlet:namespace/>t2" type="text" class="validate">
					        	</td>
						    	<td class=""><liferay-ui:message key="ims.how-much-the-proposed-idea-is-feasible-from-a-technical-point-of-view"/></td> 
							    	<input hidden="true"  name="<portlet:namespace/>qt2" value="<liferay-ui:message key="ims.how-much-the-proposed-idea-is-feasible-from-a-technical-point-of-view"/>">
							     	<input hidden="true"  name="<portlet:namespace/>qic2" value="false">
							    <td class="center-align">
					   				<a class="btn-flat disabled"><i class="material-icons">cancel</i></a> 
								</td>
						  </tr>
						  
						  <tr class="">
						  		<td class="center-align">
									<input name="<portlet:namespace/>qc3" id="qc3" type="checkbox" <%=ChallengesUtils.isEnabledCriteria(challengeId, "qcId3") ? "checked":"" %> >
			      					<label for="qc3"></label>
								</td>
								<td>
						       		<input placeholder="<liferay-ui:message key="ims.tip-value-weight"/>" id="w3" value="<%=ChallengesUtils.getCriteriaDetailsByChallengeInputType(challengeId, "qcId3", "w") %>" name="<portlet:namespace/>w3" type="text" class="validate">
					        	</td>
					        	<td>
						       		<input placeholder="<liferay-ui:message key="ims.tip-value-threshold"/>" id="t3" value="<%=ChallengesUtils.getCriteriaDetailsByChallengeInputType(challengeId, "qcId3", "t") %>" name="<portlet:namespace/>t3" type="text" class="validate">
					        	</td>
						    	<td class=""><liferay-ui:message key="ims.how-well-does-the-proposed-idea-solve-the-needs-expressed-in-the-challenge"/></td> 
							    	<input hidden="true"  name="<portlet:namespace/>qt3" value="<liferay-ui:message key="ims.how-well-does-the-proposed-idea-solve-the-needs-expressed-in-the-challenge"/>">
							     	<input hidden="true"  name="<portlet:namespace/>qic3" value="false">
							    <td class="center-align">
					   				<a class="btn-flat disabled"><i class="material-icons">cancel</i></a> 
								</td>
						  </tr>
						  
						  <tr class="">
						  		<td class="center-align">
									<input name="<portlet:namespace/>qc4" id="qc4" type="checkbox" <%=ChallengesUtils.isEnabledCriteria(challengeId, "qcId4") ? "checked":"" %> >
			      					<label for="qc4"></label>
								</td>
								<td>
						       		<input placeholder="<liferay-ui:message key="ims.tip-value-weight"/>" id="w4" value="<%=ChallengesUtils.getCriteriaDetailsByChallengeInputType(challengeId, "qcId4", "w") %>" name="<portlet:namespace/>w4" type="text" class="validate">
					        	</td>
					        	<td>
						       		<input placeholder="<liferay-ui:message key="ims.tip-value-threshold"/>" id="t4" value="<%=ChallengesUtils.getCriteriaDetailsByChallengeInputType(challengeId, "qcId4", "t") %>" name="<portlet:namespace/>t4" type="text" class="validate">
					        	</td>
						    	<td class=""><liferay-ui:message key="ims.how-much-the-idea-can-be-implemented-reusing-available-resources"/></td> 
							    	<input hidden="true"  name="<portlet:namespace/>qt4" value="<liferay-ui:message key="ims.how-much-the-idea-can-be-implemented-reusing-available-resources"/>">
							     	<input hidden="true"  name="<portlet:namespace/>qic4" value="false">
							     <td class="center-align">
					   				<a class="btn-flat disabled"><i class="material-icons">cancel</i></a> 
								</td>
						  </tr>
						  
						  <tr class="">
						  		<td class="center-align">
									<input name="<portlet:namespace/>qc5" id="qc5" type="checkbox" <%=ChallengesUtils.isEnabledCriteria(challengeId, "qcId5") ? "checked":"" %> >
			      					<label for="qc5"></label>
								</td>
								<td>
						       		<input placeholder="<liferay-ui:message key="ims.tip-value-weight"/>" id="w5" value="<%=ChallengesUtils.getCriteriaDetailsByChallengeInputType(challengeId, "qcId5", "w") %>" name="<portlet:namespace/>w5" type="text" class="validate">
					        	</td>
					        	<td>
						       		<input placeholder="<liferay-ui:message key="ims.tip-value-threshold"/>" id="t5" value="<%=ChallengesUtils.getCriteriaDetailsByChallengeInputType(challengeId, "qcId5", "t") %>" name="<portlet:namespace/>t5" type="text" class="validate">
					        	</td>
						    	<td class=""><liferay-ui:message key="ims.how-interesting-is-this-idea-according-to-citizens-opinion"/></td> 
							    	<input hidden="true"  name="<portlet:namespace/>qt5" value="<liferay-ui:message key="ims.how-interesting-is-this-idea-according-to-citizens-opinion"/>">
							     	<input hidden="true"  name="<portlet:namespace/>qic5" value="false">
							    <td class="center-align">
					   				<a class="btn-flat disabled"><i class="material-icons">cancel</i></a> 
								</td>
						  </tr>
						  
						  <tr class="">
						  		<td class="center-align">
									<input name="<portlet:namespace/>qc6" id="qc6" type="checkbox" <%=ChallengesUtils.isEnabledCriteria(challengeId, "qcId6") ? "checked":"" %> >
			      					<label for="qc6"></label>
								</td>
								<td>
						       		<input placeholder="<liferay-ui:message key="ims.tip-value-weight"/>" id="w6" value="<%=ChallengesUtils.getCriteriaDetailsByChallengeInputType(challengeId, "qcId6", "w") %>" name="<portlet:namespace/>w6" type="text" class="validate">
					        	</td>
					        	<td>
						       		<input placeholder="<liferay-ui:message key="ims.tip-value-threshold"/>" id="t6" value="<%=ChallengesUtils.getCriteriaDetailsByChallengeInputType(challengeId, "qcId6", "t") %>" name="<portlet:namespace/>t6" type="text" class="validate">
					        	</td>
						    	<td class=""><liferay-ui:message key="ims.how-much-the-proposed-idea-has-some-business-potential"/></td> 
							    	<input hidden="true"  name="<portlet:namespace/>qt6" value="<liferay-ui:message key="ims.how-much-the-proposed-idea-has-some-business-potential"/>">
							     	<input hidden="true"  name="<portlet:namespace/>qic6" value="false">
							     <td class="center-align">
					   				<a class="btn-flat disabled"><i class="material-icons">cancel</i></a> 
								</td>
						  </tr>
						  
						  <tr class="">
						  		<td class="center-align">
									<input name="<portlet:namespace/>qc7" id="qc7" type="checkbox" <%=ChallengesUtils.isEnabledCriteria(challengeId, "qcId7") ? "checked":"" %> >
			      					<label for="qc7"></label>
								</td>
								<td>
						       		<input placeholder="<liferay-ui:message key="ims.tip-value-weight"/>" id="w7" name="<portlet:namespace/>w7" value="<%=ChallengesUtils.getCriteriaDetailsByChallengeInputType(challengeId, "qcId7", "w") %>" type="text" class="validate">
					        	</td>
					        	<td>
						       		<input placeholder="<liferay-ui:message key="ims.tip-value-threshold"/>" id="t7" name="<portlet:namespace/>t7" value="<%=ChallengesUtils.getCriteriaDetailsByChallengeInputType(challengeId, "qcId7", "t") %>" type="text" class="validate">
					        	</td>
						    	<td class=""><liferay-ui:message key="ims.how-much-the-proposed-idea-could-bring-social-impact"/></td> 
							    	<input hidden="true"  name="<portlet:namespace/>qt7" value="<liferay-ui:message key="ims.how-much-the-proposed-idea-could-bring-social-impact"/>">
							     	<input hidden="true"  name="<portlet:namespace/>qic7" value="false">
							     <td class="center-align">
					   				<a class="btn-flat disabled"><i class="material-icons">cancel</i></a> 
								</td>
						  </tr>
						  
						  <tr class="">
						  		<td class="center-align">
									<input name="<portlet:namespace/>qc8" id="qc8" type="checkbox" <%=ChallengesUtils.isEnabledCriteria(challengeId, "qcId8") ? "checked":"" %> >
			      					<label for="qc8"></label>
								</td>
								<td>
						       		<input placeholder="<liferay-ui:message key="ims.tip-value-weight"/>" id="w8" value="<%=ChallengesUtils.getCriteriaDetailsByChallengeInputType(challengeId, "qcId8", "w") %>" name="<portlet:namespace/>w8" type="text" class="validate">
					        	</td>
					        	<td>
						       		<input placeholder="<liferay-ui:message key="ims.tip-value-threshold"/>" id="t8" value="<%=ChallengesUtils.getCriteriaDetailsByChallengeInputType(challengeId, "qcId8", "t") %>" name="<portlet:namespace/>t8" type="text" class="validate">
					        	</td>
						    	<td class=""><liferay-ui:message key="ims.how-likely-does-this-idea-survive-and-be-implemented-even-if-rejected-by-the-municipality"/></td> 
							    	<input hidden="true"  name="<portlet:namespace/>qt8" value="<liferay-ui:message key="ims.how-likely-does-this-idea-survive-and-be-implemented-even-if-rejected-by-the-municipality"/>">
							     	<input hidden="true"  name="<portlet:namespace/>qic8" value="false">
							    <td class="center-align">
					   				<a class="btn-flat disabled"><i class="material-icons">cancel</i></a> 
								</td>
						  </tr>
						  
						  <tr class="">
						  		<td class="center-align">
									<input name="<portlet:namespace/>qc9" id="qc9" type="checkbox" <%=ChallengesUtils.isEnabledCriteria(challengeId, "qcId9") ? "checked":"" %> >
			      					<label for="qc9"></label>
								</td>
								<td>
						       		<input placeholder="<liferay-ui:message key="ims.tip-value-weight"/>" id="w9" value="<%=ChallengesUtils.getCriteriaDetailsByChallengeInputType(challengeId, "qcId9", "w") %>" name="<portlet:namespace/>w9" type="text" class="validate">
					        	</td>
					        	<td>
						       		<input placeholder="<liferay-ui:message key="ims.tip-value-threshold"/>" id="t9" value="<%=ChallengesUtils.getCriteriaDetailsByChallengeInputType(challengeId, "qcId9", "t") %>" name="<portlet:namespace/>t9" type="text" class="validate">
					        	</td>
						    	<td class=""><liferay-ui:message key="ims.how-innovative-is-the-proposed-idea-does-it-have-a-competitive-advantage"/></td> 
							    	<input hidden="true"  name="<portlet:namespace/>qt9" value="<liferay-ui:message key="ims.how-innovative-is-the-proposed-idea-does-it-have-a-competitive-advantage"/>">
							     	<input hidden="true"  name="<portlet:namespace/>qic9" value="false">
							    <td class="center-align">
					   				<a class="btn-flat disabled"><i class="material-icons">cancel</i></a> 
								</td>
						  </tr>
						  
						  
					<% 	int indexRowQ = MyConstants.N_OF_STATIC_QUANTITATIVE_CRITERIA;
					if (customQuantitativeCriteria.size()==0){ %>
						  
						  <tr class="" id="qr10">
							<td class="center-align">
								<input type="checkbox" name="<portlet:namespace/>qc10" id="qc10" type="checkbox"  />
			      				<label for="qc10"></label>
							</td>
							<td>
						       <input placeholder="<liferay-ui:message key="ims.tip-value-weight"/>" id="w10" value="" name="<portlet:namespace/>w10" type="text" class="validate">
					        </td>
					        <td>
						       <input placeholder="<liferay-ui:message key="ims.tip-value-threshold"/>" id="t10" name="<portlet:namespace/>t10" type="text" class="validate">
					        </td>
						    <td class=""><textarea name="<portlet:namespace/>qt10" placeholder="<liferay-ui:message key="ims.write-a-criteria"/>" class="materialize-textarea"></textarea></td>
						    <input hidden="true"  name="<portlet:namespace/>qic10" value="true">
						    <input hidden="true"  name="<portlet:namespace/>qcId10" value="">
						   	<td class="center-align">
									<a class="rowDeleteCriteria waves-effect waves-light  btn-flat btn-large" id="qd10" onclick="deleteCriteriaRow('q',10)"> <i class="material-icons">cancel</i> </a>
							</td>
						   	 <input hidden="true" class="rowInputToDelete" id="qdi10" name="<portlet:namespace/>qd10" value="false">
						  </tr>		
						  
					<% }
					for (int y =0 ; y<customQuantitativeCriteria.size(); y ++ ){ 
						indexRowQ = y+MyConstants.N_OF_STATIC_QUANTITATIVE_CRITERIA;
						%>	
						<tr class="" id="qr<%=indexRowQ %>">
						
						
							<td class="center-align">
								<input type="checkbox" class="rowInputCheckbox" name="<portlet:namespace/>qc<%=indexRowQ %>" id="qc<%=indexRowQ %>" type="checkbox" <%=customQuantitativeCriteria.get(y).getEnabled() ? "checked":"" %> />
			      				<label for="qc<%=indexRowQ %>"></label>
							</td>
							<td>
						       <input placeholder="<liferay-ui:message key="ims.tip-value-weight"/>" id="w<%=indexRowQ %>" name="<portlet:namespace/>w<%=indexRowQ %>" type="text" class="validate" value="<%=customQuantitativeCriteria.get(y).getWeight() %>">
					        </td>
					        <td>
						       <input placeholder="<liferay-ui:message key="ims.tip-value-threshold"/>" id="t<%=indexRowQ %>" name="<portlet:namespace/>t<%=indexRowQ %>" type="text" class="validate" value="<%=customQuantitativeCriteria.get(y).getThreshold() %>">
					        </td>
						    <td class=""><textarea name="<portlet:namespace/>qt<%=indexRowQ %>" placeholder="<liferay-ui:message key="ims.write-a-criteria"/>" class="materialize-textarea rowTextCriteria"><%=customQuantitativeCriteria.get(y).getDescription() %></textarea></td>
						    <input hidden="true"  name="<portlet:namespace/>qic<%=indexRowQ %>" value="true">
						    <input hidden="true"  name="<portlet:namespace/>qcId<%=indexRowQ %>" value="<%=customQuantitativeCriteria.get(y).getCriteriaId() %>">
							<td class="center-align">
			      					<a class="rowDeleteCriteria waves-effect waves-light  btn-flat btn-large" id="qd<%=indexRowQ %>"  onclick="deleteCriteriaRow('q',<%=indexRowQ %>)"> <i class="material-icons">cancel</i> </a>
							</td>
								<input hidden="true" class="rowInputToDelete" id="qdi<%=indexRowQ %>" name="<portlet:namespace/>qdi<%=indexRowQ %>" value="false">
						  </tr>
						
				<% 	}
					%>
						 <input hidden="true" id="numQuantitative" name="<portlet:namespace/>numQuantitative" value="<%=indexRowQ%>"/>    
				  
						</tbody>
						
					</table>
					<div class=" section bottom-margin center-align">
						    <a id="addQuantitativeCriteria" class="waves-effect waves-light btn" ><i class="material-icons left">add</i><liferay-ui:message key="ims.add-criteria"/></a>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col s12">
					<div class=" section bottom-margin right-align">
					    <a id="cancelForm" class="waves-effect waves-light btn core-color color-1 gobackChall"><i class="material-icons right">delete</i><liferay-ui:message key="cancel"/></a>
					    <a id="sendCriteriaForm" class="waves-effect waves-light btn core-color color-1"><i class="material-icons right">send</i><liferay-ui:message key="save"/></a>
					</div>
				</div>
			</div>
			
		</form>
	
	</div><!-- fine Tab IdeaCriteria -->
	

	<!-- 	DOCUMENTATION TAB  -->
  	<div id="evaldoc" class="col s12">
		<%@ include file="sections/evaluationDocumentation.jspf" %>
	</div>
	
	</div><!-- fine row Tab -->
	</div><!-- fine Container -->
	
</div>



<script type="text/javascript">

(function($){


	$("#addBarrierCriteria").click(function(){

		addBarrierCriteria();
	});
	
	
	function addBarrierCriteria(){
	   		
			 var counter=document.getElementById("numBarriera").getAttribute("value");
			 counter++;
			 
			 var container = jQuery('.containerRowBarrier');
			 var templateP = jQuery('.rowCustom').first();
			 var template = templateP.clone();
			 template.attr("id","br"+counter);
			 
			 template.css('display', '');
			 template.find('.rowInputCheckbox').attr("name","<portlet:namespace/>bc"+counter);
			 
			 template.find('.rowTextCriteria').attr("name","<portlet:namespace/>bt"+counter);
			 template.find('.rowTextCriteria').val('');//clean the textarea
			 template.find('.rowDeleteCriteria').attr("id","bd"+counter);
			 
			 template.find('.rowDeleteCriteria').attr("onclick","deleteCriteriaRow('b',"+counter+")");
			 
			 
			 
			 //the hiddens inputs go out the node
			 var inputHidden1P = jQuery('.rowInputIsCustom').first();
			 var inputHidden1 = inputHidden1P.clone();
			 inputHidden1.attr("name","<portlet:namespace/>bic"+counter);
			 
			 var inputHidden2P = jQuery('.rowInputCriteriaId').first();
			 var inputHidden2 = inputHidden2P.clone();
			 inputHidden2.attr("name","<portlet:namespace/>bcId"+counter);
			 
			 var inputHidden3P = jQuery('.rowInputToDelete').first();
			 var inputHidden3 = inputHidden3P.clone();
			 inputHidden3.attr("name","<portlet:namespace/>bdi"+counter);
			 inputHidden3.attr("id","bdi"+counter);
			 
			
			 jQuery(container).append(inputHidden1);
			 jQuery(container).append(inputHidden2);
			 jQuery(container).append(inputHidden3);
			 jQuery(container).append(template);
			 
			 document.getElementById("numBarriera").setAttribute("value", counter);
			 
};

})(jQuery);// end of jQuery name space

</script>

	
<script type="text/javascript">

(function($){


	$("#addQuantitativeCriteria").click(function(){

		addQuantitativeCriteria();
	});
	
	
	function addQuantitativeCriteria(){
	   		
			 var counter=document.getElementById("numQuantitative").getAttribute("value");
			 counter++;
			 
			 var container = jQuery('.containerRowQuantitative');
			 var templateP = jQuery('.rowCustomQ').first();
			 var template = templateP.clone();
			 template.attr("id","qr"+counter);
			 
			 template.css('display', '');
			 template.find('.rowInputCheckbox').attr("name","<portlet:namespace/>qc"+counter);
			 template.find('.rowInputCheckbox').attr("id","qc"+counter);
			 template.find('.forRowInputCheckbox').attr("for","qc"+counter);
			 
			 template.find('.rowTextCriteria').attr("name","<portlet:namespace/>qt"+counter);
			 template.find('.rowTextCriteria').val('');//clean the textarea
			 
			 template.find('.rowInputWeigth').attr("name","<portlet:namespace/>w"+counter);
			 template.find('.rowInputWeigth').attr("id","w"+counter);
			 template.find('.rowInputThreshold').attr("name","<portlet:namespace/>t"+counter);
			 template.find('.rowInputThreshold').attr("id","t"+counter);
			 
			 template.find('.rowDeleteCriteria').attr("id","qd"+counter);
			 template.find('.rowDeleteCriteria').attr("onclick","deleteCriteriaRow('q',"+counter+")");
			 
			 //the hidden inputs go out of the node
			 var inputHidden1P = jQuery('.rowInputIsCustom').first();
			 var inputHidden1 = inputHidden1P.clone();
			 inputHidden1.attr("name","<portlet:namespace/>qic"+counter);
			 
			 var inputHidden2P = jQuery('.rowInputCriteriaId').first();
			 var inputHidden2 = inputHidden2P.clone();
			 inputHidden2.attr("name","<portlet:namespace/>qcId"+counter);
			 
  			 var inputHidden3P = jQuery('.rowInputToDelete').first();
			 var inputHidden3 = inputHidden3P.clone();
			 inputHidden3.attr("name","<portlet:namespace/>qdi"+counter);
			 inputHidden3.attr("id","qdi"+counter);
			 
			
			 jQuery(container).append(inputHidden1);
			 jQuery(container).append(inputHidden2);
			 jQuery(container).append(inputHidden3);
			 jQuery(container).append(template);
			 
			 document.getElementById("numQuantitative").setAttribute("value", counter);
			 
};

})(jQuery);// end of jQuery name space

</script>


<script>

function deleteCriteriaRow(varType, id){

	if (confirm('<liferay-ui:message key="are-you-sure-you-want-to-delete-this-entry"/> ')) { 
		
		$("#"+varType+"r"+id).css('display', 'none');
		
		/*console.log ("varType "+varType + " id "+id);*/
		document.getElementById(varType+"di"+id).setAttribute("value", "true");
		
		/* uncheck so it is not checked by function */
		if (varType == 'q'){
			$("#qc"+id).prop('checked', false);
		}
		
		
	}
}

$(".gobackChall").click(function(){

	if (confirm('<liferay-ui:message key="ims.are-you-sure-you-want-to-leave-this-page"/> <liferay-ui:message key="ims.all-the-unsaved-content-will-be-lost"/>')) { 
		window.location.href = '<%=challURL%>';
	}
});

$("#sendCriteriaForm").click(function(){

	var haveError = errorInInputValue();
	
	if (haveError == "errorWeight" )
		alert('<liferay-ui:message key="ims.error-weight"/>');
	else if (haveError == "errorTreshold" )
		alert('<liferay-ui:message key="ims.error-threshold"/>');
	else	
		$("#fmCriteria").submit();
});


function errorInInputValue(){
	
	var num = document.getElementById("numQuantitative").value;
	
	for ( i = 0; i <=num; i++ ){
		
		if($("#qc"+i).is(':checked')){
			var weight = $("#w"+i).val();
			
			if (weight.match(",")){
				weight=weight.replace(",",".");
				 $("#w"+i).val(weight);
			}
			
			
			if (weight == ""  || weight > 1 || weight <0  || isNaN(weight)  ){
				return "errorWeight";
			}
			
			if (i !=0){ //0 is citizenVote that dosen't have treshold
				
				var treshold = $("#t"+i).val();
			
				if (treshold.match(",")){
					treshold=treshold.replace(",",".");
					 $("#t"+i).val(treshold);
				}
				
				if (treshold == ""  || treshold > 5 || treshold <0 || isNaN(treshold)  ){
					return "errorTreshold";
				}	
			}
		}
	}

	return "noError";
}


</script>