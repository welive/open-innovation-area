

  <!-- Sorting Modal Structure -->
<div id="modal-sorting" class="modal bottom-sheet" style="z-index: 1003; display: block; opacity: 0; bottom: -100%;">
   <div class="modal-content row">
        
       	<div class="col s12 m4 l4">  
          	<form action="#3" id="form1">
			    <p>
			      <input type="checkbox" id="startDate" class="activeSort" checked="checked"/>
			      <label for="startDate">start</label>
			    </p>
			    
			    <div class="switch">
				    <label for="sortDateStart">
				      <liferay-ui:message key="ims.descending"/>
				      <input type="checkbox" id="sortDateStart">
				      <span class="lever"></span>
				      <liferay-ui:message key="ims.ascending"/>
				    </label>
				</div>
				
			</form>
		</div>
		<div class="col s12 m4 l4">  

			<form action="#2" id="form2">
			    <p>
			      <input type="checkbox" id="endDate" class="activeSort"/>
			      <label for="endDate">end</label>
			    </p>
			    
			    <div class="switch">
				    <label for="sortDateEnd">
				      <liferay-ui:message key="ims.descending"/>
				      <input type="checkbox" id="sortDateEnd">
				      <span class="lever"></span>
				      <liferay-ui:message key="ims.ascending"/>
				    </label>
				</div>
				
			</form>
		</div>
		<div class="col s12 m4 l4">	
			<form action="#3" id="form3">
			    <p>
			      <input type="checkbox" id="ratingSort" class="activeSort"/>
			      <label for="ratingSort"><liferay-ui:message key="ratings"/></label> 
			    </p>
			    
				<div class="switch">
				    <label for="rateSort">
				      	<liferay-ui:message key="ims.higher"/>
				      <input type="checkbox" id="rateSort">
				      <span class="lever"></span>
				      	<liferay-ui:message key="ims.lower"/>
				    </label>
				</div>
				
			</form>
  		</div>
  		
  		
  </div>
  
</div><!-- Fine Modal -->


<script>

$( "#startDate" ).change(function() {
	  var $input = $( this );
	  var $lever = $input.parent().parent().find( ".switch input" );
	  
	  if($input.is( ":checked" )) {
		  		$lever.prop( "disabled", false );
				$('#form2 #endDate').prop('checked', false);
				$('#form2 #endDate').parent().parent().find( ".switch input" ).prop( "disabled", true );
				$('#form3 #ratingSort').prop('checked', false);
				$('#form3 #ratingSort').parent().parent().find( ".switch input" ).prop( "disabled", true );
	}
	  
}).change();


$( "#endDate" ).change(function() {
	  var $input = $( this );
	  var $lever = $input.parent().parent().find( ".switch input" );
	  
	  if($input.is( ":checked" )) {
		  		$lever.prop( "disabled", false );
				$('#form1 #startDate').prop('checked', false);
				$('#form1 #startDate').parent().parent().find( ".switch input" ).prop( "disabled", true );
				$('#form3 #ratingSort').prop('checked', false);
				$('#form3 #ratingSort').parent().parent().find( ".switch input" ).prop( "disabled", true );
	}
					  
}).change();
				
				
$( "#ratingSort" ).change(function() {
	  var $input = $( this );
	  var $lever = $input.parent().parent().find( ".switch input" );
	  
	  if($input.is( ":checked" )) {
		 		 $lever.prop( "disabled", false );
				$('#form1 #startDate').prop('checked', false);
				$('#form1 #startDate').parent().parent().find( ".switch input" ).prop( "disabled", true );
				$('#form2 #endDate').prop('checked', false);
				$('#form2 #endDate').parent().parent().find( ".switch input" ).prop( "disabled", true );
	}
}).change();

</script>