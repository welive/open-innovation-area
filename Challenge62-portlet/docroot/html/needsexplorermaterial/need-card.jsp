<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<portlet:defineObjects />

<!-- I contenuti di questa pagina non vengono mai mostrati direttamente
la struttura viene clonata e riempita con Ajax
 -->
	
<!-- PRIMA CARD		 -->
        <div class="col s12 m6 l4 schedaIdea" style="display:none">
        	
			<!-- CARD -->
			<div class="card  cardlink">
          		
				<!-- Card Header -->
				<%@ include file="header-card.jsp" %>
				
				<!-- Card Content -->
				<div class="card-content">
				
						<!-- Titolo e autore -->
				    	<div class="bottom-margin">
	 						<p class="card-multi-title">
	 							Titolo del Need
	 						</p><!-- 60 char -->
   							<a class="titoloIdea tooltipped viewNeed" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="ims.view-need"/>' href="#" >
 								<i class='material-icons fa-fw'>find_in_page</i>
 							</a>
	  						<p class="grey-text text-darken-2 ">
	  						  	<label class="grey-text text-darken-2"><liferay-ui:message key="ims.submitted-by"/></label>
					         	<span  class="autoreIdea">Massimiliano Ugonotti</span>
					      	</p>
						</div>
						
						<div class="divider"></div>
						
						<!-- SubCategories -->
						<div class="section">
							<span class="paddingAll truncate">
								<i class="material-icons icon-asterisk tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="subcategories"/>"></i> 
								<span class="categorieIdea">
									<small>Parks</small>,&nbsp;<small>Pollution</small>,&nbsp; <small>Public green</small>,&nbsp; 
									<small>Public orange</small>,&nbsp; <small>Secret green</small>,&nbsp; <small>Governement violet</small>
								</span>
							</span>
						</div>
						
						<div class="divider bottom-margin"></div>
						
						<!-- Date e Rating -->
					    <div class="row">
					      	<div class="col s6 center">
					      		<h6 class="weight-5"><liferay-ui:message key="ims.creation-date"/></h6>
					      		<span class="dataCreazioneIdea">20/01/2016</span>
					      	</div>
					      	<div class="col s6 center">
					      		<h6 class="weight-5"><liferay-ui:message key="ratings"/></h6>
					      		
					      		<span class="votoMedio">
									<i class="icon-star"></i>
									<i class="icon-star"></i>
									<i class="icon-star"></i>
									<i class="icon-star"></i>
									<i class="icon-star-empty"></i>
								</span>
					      	</div>
						    <div class="col s12 top-margin">
								<h6 class="valign-wrapper">
									<i class="material-icons small tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.authority"/>">location_city</i>
									&nbsp; <span class="enteIdea">Trento</span>
								</h6>
				            </div>
					     </div>
				     	<div class="divider"></div>
				     	
						<!-- Immagine edescrizione -->
						<div class="section row">
						   	<div class="col s8 offset-s2 m4 l4 center-align image-card">  
						   		<img src="" alt="" class="responsive-img imgRappresentativa top-margin">
						   		<div class="sampleText flow-text weight-5"></div>
						   	</div>
						   	<div class="col s12 m8 l8">
								<div class="multi-truncate descrizioneIdea top-margin">
									<p class="descrLink"> 
									<!--
 										<a href="" class="moreDescr tooltipped viewNeed" data-position="top" data-delay="50" data-tooltip='<liferay-ui:message key="ims.view-need"/>'> 
										descrizione 
										</a> 
									-->
									</p>
									<!-- 
									<a href="" class="readmore tooltipped viewNeed" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="ims.view-need"/>'>
									&nbsp;<i class="material-icons fa-fw fa-lg">more</i></a>
									-->
								</div>
							</div>
						</div>
 						<div class="divider"></div> 
						
						
						<div class="section row">
							<div class="col-fake">
								<i class="material-icons icon-trophy small">&nbsp;</i>
								<span class="weight-3">
										
										<liferay-ui:message key="ims.number-of-linked-challenges" /> : <span class="numGare">7</span>
										
								</span>
							</div>
						</div>
						<div class="divider"></div> 
						
						<!-- Associazione Gara -->
						<div class="section row-last">
							
							
							<div class="col-fake">
								<i class="material-icons icon-lightbulb small">&nbsp;</i>
								<span class="weight-3">
										
										<liferay-ui:message key="ims.ideas-proposed" /> : <span class="numIdeas">7</span>
										
								</span>
							</div>
						</div>

				</div><!-- fine Card-content -->
				
			   <!-- Card Footer -->
			   <div class="footer-card center-align grey darken-2 white-text">

			    </div><!-- fine Card Footer -->
			    
				<!-- Card Back side -->
			    <div class="card-reveal">
			      <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
			      <div class="divider"></div>
			      <p>Here is some more information about this product that is only revealed once clicked on.</p>
			    </div><!-- fine Card Back side -->
			    
			</div><!-- Fine CARD -->
			
		</div><!-- Fine Col Need -->
		
<!-- 	Fine PRIMA CARD -->
