

  <!-- Sorting Modal Structure -->
<div id="modal-sorting" class="modal bottom-sheet" style="z-index: 1003; display: block; opacity: 0; bottom: -100%;">
   <div class="modal-content row">
        
       	<div class="col s12 m6 l4">  
          	<form action="#1" id="form1">
			    <p>
			      <input type="checkbox" id="creationDate" class="activeSort" checked/>
			      <label for="creationDate"><liferay-ui:message key="ims.creation-date"/></label>
			    </p>
			    
			    <div class="switch">
				    <label for="sortDate">
				      <liferay-ui:message key="ims.descending"/>
				      <input type="checkbox" id="sortDate">
				      <span class="lever"></span>
				      <liferay-ui:message key="ims.ascending"/>
				    </label>
				</div>
				
			</form>
		</div>
		
		<div class="col s12 m6 l4">	
			<form action="#2" id="form2">
			    <p>
			      <input type="checkbox" id="ratingSort" class="activeSort" />
			      <label for="ratingSort"><liferay-ui:message key="ratings"/></label> 
			    </p>
			    
				<div class="switch">
				    <label for="rateSort">
				      	<liferay-ui:message key="ims.higher"/>
				      <input disabled type="checkbox" id="rateSort">
				      <span class="lever"></span>
				      	<liferay-ui:message key="ims.lower"/>
				    </label>
				</div>
				
			</form>
  		</div>
  		
  		
  </div>
  
</div><!-- Fine Modal -->


<script>

$( "#creationDate" ).change(function() {
					  var $input = $( this );
					  var $lever = $input.parent().parent().find( ".switch input" );
					  
					  if($input.is( ":checked" )) {
						  		$lever.prop( "disabled", false );
								$('#form2 #ratingSort').prop('checked', false);
								$('#form2 #ratingSort').parent().parent().find( ".switch input" ).prop( "disabled", true );
					}
					  
				}).change();
				
				
$( "#ratingSort" ).change(function() {
	  var $input = $( this );
	  var $lever = $input.parent().parent().find( ".switch input" );
	  
	  if($input.is( ":checked" )) {
		 		 $lever.prop( "disabled", false );
				$('#form1 #creationDate').prop('checked', false);
				$('#form1 #creationDate').parent().parent().find( ".switch input" ).prop( "disabled", true );
				
	}
}).change();

</script>