<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<!-- exter wrap -->

<div class="materialize">	
		
			<div class="section row valign-wrapper">
			
		        <div class="col s12">
		        	<div class="row"> 
		        		<div class="col s12 m12 l12">
				          <div class="card">
				            <div class="card-content">
				            	<span class="card-title"><liferay-ui:message key="statistics"/></span>
				            	<div class="row">
				            		<div class="col s12 m6 offset-m3">
				            			<%@ include file="content.jsp" %>
				            		</div>
				            	</div>
				            </div>
				            <div class="card-action">
				              <a id="button2" href="<portlet:renderURL windowState="<%=WindowState.MAXIMIZED.toString() %>">
									<portlet:param name="jspPage" value="/html/statisticheente/view_expanded.jsp" />
								 </portlet:renderURL>" class="btn-flat core-color-text text-color-4" ><liferay-ui:message key="view"/></a>
				            </div>
				          </div>
				        </div>
					</div>
				</div>
				
			</div>

</div>