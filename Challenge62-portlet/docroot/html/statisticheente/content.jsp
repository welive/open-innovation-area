<%@page import="it.eng.rspa.ideas.utils.MyConstants"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.liferay.portal.kernel.workflow.WorkflowTaskManager"%>
<%@page	import="com.liferay.portal.service.WorkflowDefinitionLinkLocalService"%>
<%@page import="com.liferay.portal.kernel.workflow.WorkflowDefinition"%>
<%@page	import="com.liferay.portal.kernel.workflow.WorkflowInstanceManagerUtil"%>
<%@page	import="com.liferay.portal.service.WorkflowInstanceLinkLocalService"%>
<%@page import="com.liferay.portal.kernel.workflow.WorkflowInstance"%>
<%@page import="com.liferay.portal.model.WorkflowInstanceLink"%>
<%@page	import="com.liferay.portal.service.WorkflowInstanceLinkLocalServiceUtil"%>
<%@page	import="com.liferay.portal.service.persistence.WorkflowInstanceLinkUtil"%>
<%@page import="com.liferay.portal.model.WorkflowDefinitionLink"%>
<%@page	import="com.liferay.portal.service.persistence.WorkflowDefinitionLinkUtil"%>
<%@page	import="com.liferay.portal.kernel.workflow.WorkflowDefinitionManagerUtil"%>
<%@page	import="com.liferay.portal.service.WorkflowDefinitionLinkLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.workflow.WorkflowTask"%>
<%@page	import="com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.OrderByComparator"%>
<%@page import="com.liferay.portal.model.RoleConstants"%>
<%@page import="com.liferay.portal.service.RoleLocalServiceUtil"%>
<%@page import="com.liferay.portal.model.Role"%>
<%@page	import="com.liferay.portal.kernel.workflow.WorkflowTaskManagerUtil"%>
<%@page	import="com.liferay.portal.kernel.workflow.WorkflowHandlerRegistryUtil"%>
<%@page	import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteChallengesLocalServiceUtil"%>
<%@page	import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeServiceUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.Challenges"%>
<%@page	import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges"%>
<%@page	import="it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesUtil"%>
<%@page import="com.ibm.icu.text.DecimalFormat"%>
<%@page import="com.liferay.portlet.messageboards.service.MBMessageLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.PortalClassLoaderUtil"%>
<%@page import="com.liferay.portal.kernel.dao.orm.DynamicQuery"%>
<%@page import="com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil"%>
<%@page import="com.liferay.portlet.messageboards.model.MBMessage"%>
<%@ include file="/html/challenges/init.jsp" %>

<%-- include jquery by theme --%>
<script src="<%=request.getContextPath()%>/js/D3/d3.v3.min.js"></script>

			
				<!-- CONTENUTO PORTLET -->
<%
PortletURL createIdeaURL = renderResponse.createRenderURL();
createIdeaURL.setParameter("jspPage", "/html/ideas/update.jsp");
createIdeaURL.setWindowState(WindowState.MAXIMIZED);
%>

<script type="text/javascript">
jQuery(document).ready(function() {
  jQuery(".content").show();
  //toggle the componenet with class msg_body
  jQuery(".heading").click(function()
  {
    jQuery(this).next(".content").slideToggle(500);
  });
});
</script>

<portlet:defineObjects />


	<%
		User currentUser = PortalUtil.getUser(request); 
		String fullname = currentUser.getFullName();
		String email = currentUser.getDisplayEmailAddress();
		
		long portraitId = currentUser.getPortraitId();
		Image image = ImageLocalServiceUtil.getImage(portraitId);
		
		Locale locale = (Locale)request.getSession().getAttribute("org.apache.struts.action.LOCALE");
		String language = locale.getLanguage();
		String country = locale.getCountry();
		ResourceBundle res = ResourceBundle.getBundle("Language", new Locale(language, country));
		String espandi= res.getString("ims.expand");
		espandi = new String(espandi.getBytes("ISO-8859-1"), "UTF-8");
		
		ThemeDisplay themeDisplay = 
		(ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
		Calendar cal = CalendarFactoryUtil.getCalendar();
	%>
<!-- 
<div id="riconoscimenti">
	<aui:fieldset label="I miei riconoscimenti">
	</aui:fieldset>
</div>
 -->
<div id="stats">
<%
User challengeUserStats = PortalUtil.getUser(request); 
List<CLSChallenge> challengesStats = CLSChallengeLocalServiceUtil.getChallengesByUserId(challengeUserStats.getPrimaryKey());
Iterator<CLSChallenge> challengesStatsIt = challengesStats.iterator();
int totalIdeas = 0;
while(challengesStatsIt.hasNext()){
	CLSChallenge challenge = challengesStatsIt.next();
	List<CLSIdea> ideas = CLSIdeaLocalServiceUtil.getIdeasByChallengeId(challenge.getChallengeId());
	totalIdeas = totalIdeas + ideas.size();
}

int monitoringIdeas = 0;
int implementationIdeas = 0;
int refinementIdeas = 0;
int selectedIdeas = 0;
int evaluationIdeas = 0;
int selectionIdeas = 0;
int rejectedIdeas = 0;

int monitoringIdeasPercent  = 0;
int implementationIdeasPercent = 0;
int refinementIdeasPercent = 0;
int selectedIdeasPercent = 0;
int evaluationIdeasPercent = 0;
int selectionIdeasPercent = 0;
int rejectedIdeasPercent = 0;

double totChallengeScore = 0;
double totChallengeComments = 0;

// Recupero le idee che sono in statao di monitoraggio
Long userIdStats = challengeUserStats.getUserId();
Long companyIdStats = challengeUserStats.getCompanyId();
Long groupIdStats = themeDisplay.getCompanyGroupId();


String  ruolo = PortletProps.get("role.authority");

Role roleEnteStats = RoleLocalServiceUtil.getRole(companyIdStats, ruolo);

// int countStats = WorkflowTaskManagerUtil.getWorkflowTaskCountByRole(companyIdStats, roleEnteStats.getRoleId(), false);
// OrderByComparator orderByComparatorStats = OrderByComparatorFactoryUtil.create("kaleotask", "kaleotaskid", true);
// List<WorkflowTask> tasksStats = WorkflowTaskManagerUtil.getWorkflowTasksByRole(companyIdStats, roleEnteStats.getRoleId(), false, 0, countStats, orderByComparatorStats);
// countStats = WorkflowTaskManagerUtil.getWorkflowTaskCountByUser(companyIdStats, userIdStats, false);
// tasksStats.addAll(WorkflowTaskManagerUtil.getWorkflowTasksByUser(companyIdStats, userIdStats, false, 0, countStats, orderByComparatorStats));


for (CLSChallenge chl : challengesStats) {
	List<CLSIdea> ideas = CLSIdeaLocalServiceUtil.getIdeasByChallengeId(chl.getChallengeId());
	for (CLSIdea idea : ideas) {		
		
		
		if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_MONITORING))) {
			monitoringIdeas++;
		} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_IMPLEMENTATION))) {
			implementationIdeas++;
		} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_REFINEMENT))) {
			refinementIdeas++;
		} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_SELECTED))) {
			selectedIdeas++;
		} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_EVALUATION))) {
			evaluationIdeas++;
		} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_ELABORATION))) {
			selectionIdeas++;
		} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_REJECT))) {
			rejectedIdeas++;
		}
		
		
	}
	
	
	List<RatingsEntry> lstRE = RatingsEntryLocalServiceUtil.getEntries(CLSChallenge.class.getName(), chl.getPrimaryKey());
	for(RatingsEntry RE:lstRE) {
		totChallengeScore += RE.getScore();
	}
	
	//ottengo i commenti
	DynamicQuery mbMessageQuery = DynamicQueryFactoryUtil.forClass(MBMessage.class, PortalClassLoaderUtil.getClassLoader());
	mbMessageQuery.add(RestrictionsFactoryUtil.eq("classNameId", Long.valueOf(PortalUtil.getClassNameId(CLSChallenge.class.getName()))));
	mbMessageQuery.add(PropertyFactoryUtil.forName("parentMessageId").ne(Long.valueOf(0)));
	mbMessageQuery.add(PropertyFactoryUtil.forName("classNameId").eq(Long.valueOf(PortalUtil.getClassNameId(CLSChallenge.class.getName()))));
	mbMessageQuery.add(PropertyFactoryUtil.forName("classPK").eq(Long.valueOf(chl.getPrimaryKey())));
	mbMessageQuery.add(PropertyFactoryUtil.forName("categoryId").eq(Long.valueOf(-1)));
	List comments = MBMessageLocalServiceUtil.dynamicQuery(mbMessageQuery);
	totChallengeComments += comments.size();
}

int totalActiveIdeas = monitoringIdeas + implementationIdeas + refinementIdeas + selectedIdeas+ evaluationIdeas+ selectionIdeas;

if(totalActiveIdeas>0){
	monitoringIdeasPercent = (monitoringIdeas * 100) / totalActiveIdeas;
	implementationIdeasPercent = (implementationIdeas * 100) / totalActiveIdeas;
	refinementIdeasPercent = (refinementIdeas * 100) / totalActiveIdeas;
	selectedIdeasPercent = (selectedIdeas * 100) / totalActiveIdeas;
	evaluationIdeasPercent = (evaluationIdeas * 100) / totalActiveIdeas;
	selectionIdeasPercent = (selectionIdeas * 100) / totalActiveIdeas;
	rejectedIdeasPercent = (rejectedIdeas * 100) / totalIdeas;
	

}

double averageChallengeScore = 0;
double averageIdeaComments = 0;
double averageideas = 0;

if(challengesStats.size() > 0 ){
	averageChallengeScore = (double)totChallengeScore / (double)challengesStats.size();
	averageIdeaComments = (double)totChallengeComments / (double)challengesStats.size();
	averageideas = (double)totalIdeas / (double)challengesStats.size();
}

DecimalFormat decimalFIdeaStats = new DecimalFormat("#.##");

%>

	<div id="statsGraph">
		<div id="chart-ente">
		</div>
	</div>
</div>


<style type="text/css">
 .slice text {
     font-size: 16pt;
     font-family: Arial;
 }   
</style>

<%
if(totalIdeas>0){
%>
<script type="text/javascript">

var w = 260,
h = 260,
r = 120,
inner = 70,
legendW = 150,
legendH = 120,
color = gradient;

data = [{"label":'<liferay-ui:message key="ims.processing"/>', "value":<%=selectionIdeas %>, "percent":<%=selectionIdeasPercent %>}, 
        {"label":'<liferay-ui:message key="ims.evaluation"/>', "value":<%=evaluationIdeas %>, "percent":<%=evaluationIdeasPercent %>}, 
        {"label":'<liferay-ui:message key="ims.selected"/>', "value":<%=selectedIdeas %>, "percent":<%=selectedIdeasPercent %>}, 
        {"label":'<liferay-ui:message key="ims.refinement"/>', "value":<%=refinementIdeas %>, "percent":<%=refinementIdeasPercent %>}, 
        {"label":'<liferay-ui:message key="ims.implementation"/>', "value":<%=implementationIdeas %>, "percent":<%=implementationIdeasPercent %>},
        {"label":'<liferay-ui:message key="ims.monitoring"/>', "value":<%=monitoringIdeas %>, "percent":<%=monitoringIdeasPercent %>}
        {"label":'<liferay-ui:message key="ims.reject"/>', "value":<%=rejectedIdeas %>, "percent":<%=refinementIdeasPercent %>}
        ];

var total = <%=totalIdeas%>; 

var vis = d3.select("#chart-ente")
.append("svg:svg")
.data([data])
    .attr("width", w)
    .attr("height", h)
.append("svg:g")
    .attr("transform", "translate(" + w/2 + "," + w/2 + ")");

var textTop = vis.append("text")
.attr("dy", ".35em")
.style("text-anchor", "middle")
.attr("class", "textTop")
.text( '<liferay-ui:message key="ims.active-ideas-total"></liferay-ui:message>' )
.attr("y", -10),
textBottom = vis.append("text")
.attr("dy", ".35em")
.style("text-anchor", "middle")
.attr("class", "textBottom")
.text(total.toFixed(0))
.attr("y", 10);

var arc = d3.svg.arc()
.innerRadius(inner)
.outerRadius(r);

var arcOver = d3.svg.arc()
.innerRadius(inner + 5)
.outerRadius(r + 5);

var pie = d3.layout.pie()
.value(function(d) { return d.value; });

var arcs = vis.selectAll("g.slice")
.data(pie)
.enter()
    .append("svg:g")
        .attr("class", "slice")
        .on("mouseover", function(d) {
            d3.select(this).select("path").transition()
                .duration(200)
                .attr("d", arcOver)
            
            textTop.text(d3.select(this).datum().data.label)
                .attr("y", -10);
            textBottom.text(d3.select(this).datum().data.value.toFixed(0))
                .attr("y", 10);
        })
        .on("mouseout", function(d) {
            d3.select(this).select("path").transition()
                .duration(100)
                .attr("d", arc);
            
            textTop.text( '<liferay-ui:message key="ims.active-ideas-total"/>' )
                .attr("y", -10);
            textBottom.text(total.toFixed(0));
        });

arcs.append("svg:path")
.attr("fill", function(d, i) { return color[i]; } )
.attr("d", arc);


</script>
<%
}

if(totalIdeas==0){

%>	
	<p class='infoText bottomSpace'><liferay-ui:message key="ims.no-data"/></p>
<%	
}
%>
 