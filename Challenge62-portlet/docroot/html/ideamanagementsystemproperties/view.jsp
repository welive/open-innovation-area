<%@page import="com.liferay.calendar.model.Calendar"%>
<%@page import="com.liferay.calendar.service.CalendarLocalServiceUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet"%>
<%@page import="it.eng.rspa.ideas.controlpanel.CategoryKeys"%>
<%@page import="it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties"%>
<%@page import="com.liferay.portlet.PortletPreferencesFactoryUtil"%>
<%@page import="javax.portlet.PortletPreferences"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="javax.portlet.ActionRequest"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil"%>
<%@page import="com.liferay.portlet.asset.model.AssetVocabulary"%>
<%@page import="com.liferay.portlet.asset.model.AssetCategory"%>
<%@page import="com.liferay.util.portlet.PortletProps"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ include file="/html/challenges/init.jsp" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<%
IdeaManagementSystemProperties imsp = new IdeaManagementSystemProperties();

String rootUrl = PortalUtil.getHomeURL(request);

%>

<%
PortletURL savePreferenciesURL = renderResponse.createActionURL();
savePreferenciesURL.setParameter(ActionRequest.ACTION_NAME, "savePreferencies");
%>

<aui:form name="fm" method="POST" action="<%= savePreferenciesURL.toString() %>" >



<b><liferay-ui:message key="categories"/></b>
 
<br/>
<%
ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
Long scopeGroupId = themeDisplay.getScopeGroupId();
List<AssetCategory> categoryList = null;
List<AssetVocabulary> vocabularies = AssetVocabularyLocalServiceUtil.getAssetVocabularies(com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS, com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS);

List<Long> mapCategoriesSet = imsp.getMapCategoriesSetIMS();

Iterator vocabulariesIt = vocabularies.iterator();
while(vocabulariesIt.hasNext()){
	AssetVocabulary vocabulary = (AssetVocabulary)vocabulariesIt.next();
//	if(vocabulary.getGroupId() == scopeGroupId){
	
	if(!   (vocabulary.getName().equals ("Topic") || vocabulary.getName().equals ("Topic (2)") )){
		
		String inputSingleName = "choiceTypeForVocabulary_" + vocabulary.getVocabularyId();
		String inputMultiName = "choiceTypeForVocabulary_" + vocabulary.getVocabularyId();
		
		String checkboxVocabularyName = "checkbox_vocabulary_#"+vocabulary.getVocabularyId()+"#Checkbox";
		
		//List<CLSCategoriesSet> sets = imsp.getCategoriesSetIMS();
		
		
		String value = "false";
		boolean inputSingleChecked = false;
		boolean inputMultiChecked = true;
		if(mapCategoriesSet.contains(vocabulary.getVocabularyId()) ){
			value = "true";
			/* if(mapCategoriesSet.get(vocabulary.getVocabularyId()) == CategoryKeys.SINGLE_CHOICE){
				inputSingleChecked = true;
				inputMultiChecked = false;
			} */
		}
		
		%>
			<aui:input inlineField="true" type="checkbox" label="<%=vocabulary.getName()%>" name="<%=checkboxVocabularyName %>" value="<%=value%>"/>
			<%--
			<aui:input inlineField="true" inlineLabel="right" name="<%=inputSingleName%>" type="radio" value="<%=CategoryKeys.SINGLE_CHOICE%>" label="Scelta singola"  checked="<%= inputSingleChecked %>"/>
			<aui:input inlineField="true" inlineLabel="right" name="<%=inputMultiName%>"  type="radio" value="<%=CategoryKeys.MULTI_CHOICE%>"  label="Scelta multipla" checked="<%= inputMultiChecked %>" />
			--%>
			<br/>
		<%
	}
	//}
}

// ottengo i suffissi url
String suffixImsHome = IdeaManagementSystemProperties.getUrlSuffixImsHome();
String suffixIdeas = IdeaManagementSystemProperties.getFriendlyUrlSuffixIdeas();
String suffixNeeds = IdeaManagementSystemProperties.getFriendlyUrlSuffixNeeds();
String suffixChallenges = IdeaManagementSystemProperties.getFriendlyUrlSuffixChallenges();

%>
	<br/>
	<b><liferay-ui:message key="suffix"/></b>
	 
	 <aui:input name="sfxHomeIms" label="ims.imsHome-suffix" value="<%=suffixImsHome%>" helpMessage="e.g. innovation-area">
				  	 <aui:validator name="required"  errorMessage="ims.enter-imsHome-suffix"  />
	 </aui:input>
	 
	<aui:input name="sfxIdeas" label="ims.ideas-suffix" value="<%=suffixIdeas%>" helpMessage="e.g. ideas_explorer">
				  	 <aui:validator name="required"  errorMessage="ims.enter-ideas-suffix"  />
	 </aui:input>
	              
	 <aui:input name="sfxNeeds" label="ims.needs-suffix" value="<%=suffixNeeds%>" helpMessage="e.g. needs_explorer">
				  	 <aui:validator name="required"  errorMessage="ims.enter-needs-suffix"  />
	 </aui:input>
	
	<aui:input name="sfxChallenges" label="ims.challenges-suffix" value="<%=suffixChallenges%>" helpMessage="e.g. challenges_explorer">
				  	 <aui:validator name="required"  errorMessage="ims.enter-challenges-suffix"  />
	</aui:input>
	

	
	<br/>
	<br/>
	<br/>
	<%
	String senderNotificheMailIdeario = IdeaManagementSystemProperties.getProperty("senderNotificheMailIdeario");
	String oggettoNotificheMailIdeario = IdeaManagementSystemProperties.getProperty("oggettoNotificheMailIdeario");
	String firmaNotificheMailIdeario = IdeaManagementSystemProperties.getProperty("firmaNotificheMailIdeario");
	String utenzaMail = IdeaManagementSystemProperties.getProperty("utenzaMail");
	
	 %>
	<b><liferay-ui:message key="default-email-notification"/></b>
	
	<aui:input name="senderNotificheMailIdeario" label="sender" value="<%=senderNotificheMailIdeario%>" helpMessage="e.g. WeLive">
				  	 <aui:validator name="required"   />
	</aui:input>
	<aui:input name="oggettoNotificheMailIdeario" label="ims.email-object" value="<%=oggettoNotificheMailIdeario%>" helpMessage="e.g. Open Innovation Area">
				  	 <aui:validator name="required"   />
	</aui:input>
	<aui:input name="firmaNotificheMailIdeario" label="signature" value="<%=firmaNotificheMailIdeario%>" helpMessage="e.g. admin">
				  	 <aui:validator name="required"   />
	</aui:input>
	<aui:input name="utenzaMail" label="suffix" value="<%=utenzaMail%>" helpMessage="e.g. openinnovationarea">
				  	 <aui:validator name="required"   />
	</aui:input>
	
	
	<br/>
	<br/>
	<br/>
	<b> <liferay-ui:message key="ims.map-parameters"/></b> 
	 
	<%
	String latitude = imsp.getMapCenterLatitude();
	String longitude = imsp.getMapCenterLongitude();
	%>
	<aui:input name="Latitude"  id="Latitude"  label="ims.latitude"  value="<%=latitude%>" type="text" helpMessage="e.g. 40.353237">
				  	 <aui:validator name="required"  errorMessage="Enter the latitude"  />
	              </aui:input>
	<aui:input name="Longitude" id="Longitude" label="ims.longitude" value="<%=longitude%>" type="text" helpMessage="e.g. 18.172545">
				  	 <aui:validator name="required"  errorMessage="Enter the longitude"  />
	              </aui:input>

	<br/>
	<br/>
	<%	
	List<Calendar> calendars = CalendarLocalServiceUtil.getCalendars(com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS, com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS);
	long calendarId = imsp.getChallengesReferenceCalendarId();
	 %>
	
	<b><liferay-ui:message key="ims.challenges-calendar"/></b>
	 <br/>
	<aui:select label="" name="challengeCalendar" id="challengeCalendar" >
		<aui:option selected="<%=calendarId == -1 ? true : false %>" value="-1"><liferay-ui:message key="ims.nothing" /></aui:option>
		<%
		for (Calendar calendar : calendars) {
		%>
						<aui:option selected="<%=calendarId == calendar.getCalendarId() ? true : false %>" value="<%=calendar.getCalendarId()%>"><liferay-ui:message key="<%= calendar.getName() %>" /></aui:option>

		<%
		
		}
		%>
	</aui:select>
	<br/><br/>
	<%boolean verboseEnabled = IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled");
	%>

	<b>Verbosity</b>
	<br/>
	<aui:input inlineField="true" type="checkbox" label="Catalina.out verbosity Enabled" name="verboseEnabled" value="<%=verboseEnabled%>"/><br/>
	
	
		<br/>
	<br/>
	
<h2><b><liferay-ui:message key="external-services"/></b></h2>


	<%
	boolean cdvEnabled = IdeaManagementSystemProperties.getEnabledProperty("cdvEnabled");
	String cdvAddress = IdeaManagementSystemProperties.getProperty("cdvAddress");
	
	boolean mktEnabled = IdeaManagementSystemProperties.getEnabledProperty("mktEnabled");
	
	boolean vcEnabled = IdeaManagementSystemProperties.getEnabledProperty("vcEnabled");
	String vcAddress = IdeaManagementSystemProperties.getProperty("vcAddress");
	String vcWSAddress = IdeaManagementSystemProperties.getProperty("vcWSAddress");
	boolean deEnabled = IdeaManagementSystemProperties.getEnabledProperty("deEnabled");
	String deAddress = IdeaManagementSystemProperties.getProperty("deAddress");
	boolean lbbEnabled = IdeaManagementSystemProperties.getEnabledProperty("lbbEnabled");
	String lbbAddress = IdeaManagementSystemProperties.getProperty("lbbAddress");
	String oiaAppId4lbb = IdeaManagementSystemProperties.getProperty("oiaAppId4lbb");
	boolean tweetingEnabled = IdeaManagementSystemProperties.getEnabledProperty("tweetingEnabled");
	String basicAuthUser = IdeaManagementSystemProperties.getProperty("basicAuthUser");
	String basicAuthPwd = IdeaManagementSystemProperties.getProperty("basicAuthPwd");
	
	boolean emailNotificationsEnabled = IdeaManagementSystemProperties.getEnabledProperty("emailNotificationsEnabled");
	boolean dockbarNotificationsEnabled = IdeaManagementSystemProperties.getEnabledProperty("dockbarNotificationsEnabled");
	
	boolean jmsEnabled = IdeaManagementSystemProperties.getEnabledProperty("jmsEnabled");
	String brokerJMSusername = IdeaManagementSystemProperties.getProperty("brokerJMSusername");
	String brokerJMSpassword = IdeaManagementSystemProperties.getProperty("brokerJMSpassword");
	String brokerJMSurl = IdeaManagementSystemProperties.getProperty("brokerJMSurl");
	String jmsTopic = IdeaManagementSystemProperties.getProperty("jmsTopic");
	
	
	 %>
	 

	<aui:input name="basicAuthUser" id="basicAuthUser" label="Basic Authentication User" value="<%=basicAuthUser%>" type="text" helpMessage="e.g. opsi@opsi.eu"/><br/>
	<aui:input name="basicAuthPwd" id="basicAuthPwd" label="Basic Authentication Password" value="<%=basicAuthPwd%>" type="text" helpMessage="e.g. 01h2h2h3o3"/><br/>
<br/><br/>
	<aui:input inlineField="true" type="checkbox" label="Marketplace Enabled" name="mktEnabled" value="<%=mktEnabled%>"/><br/>	 
	
<br/><br/>
	<aui:input inlineField="true" type="checkbox" label="Tweeting Enabled" name="tweetingEnabled" value="<%=tweetingEnabled%>"/><br/>
<br/><br/>
	<aui:input inlineField="true" type="checkbox" label="Citizen Data Vault Enabled" name="cdvEnabled" value="<%=cdvEnabled%>"/><br/>
	<aui:input name="cdvAddress" id="cdvAddress" label="Citizen Data Vault Address" value="<%=cdvAddress%>" type="text" helpMessage="e.g. https://dev.welive.eu/dev/api/cdv"/><br/>
<br/><br/>				  	
	<aui:input inlineField="true" type="checkbox" label="Visual Composer Enabled" name="vcEnabled" value="<%=vcEnabled%>"/><br/>
	<aui:input name="vcAddress" id="vcAddress" label="Visual Composer Address" value="<%=vcAddress%>" type="text" helpMessage="e.g. https://dev.welive.eu/visualcomposer/"/><br/>
	<aui:input name="vcWSAddress" id="vcWSAddress" label="Visual Composer WS Address" value="<%=vcWSAddress%>" type="text" helpMessage="e.g. https://test.welive.eu/dev/api/vc"/><br/>
<br/><br/>	
	<aui:input inlineField="true" type="checkbox" label="Decision Engine Enabled" name="deEnabled" value="<%=deEnabled%>"/><br/>
	<aui:input name="deAddress" id="deAddress" label="Decision Engine Address" value="<%=deAddress%>" type="text" helpMessage="e.g. https://dev.welive.eu/dev/api/de/"/><br/>
<br/><br/>
	<aui:input inlineField="true" type="checkbox" label="Logging BB Enabled" name="lbbEnabled" value="<%=lbbEnabled%>"/><br/>
	<aui:input name="lbbAddress" id="lbbAddress" label="Logging BB Address" value="<%=lbbAddress%>" type="text" helpMessage="e.g. https://dev.welive.eu/dev/api/log/"/><br/>
	<aui:input name="oiaAppId4lbb" id="oiaAppId4lbb" label="OIA appId Logging BB" value="<%=oiaAppId4lbb%>" type="text" helpMessage="oia"/><br/>
	
	<br/><br/>
	<aui:input inlineField="true" type="checkbox" label="Email notifications Enabled" name="emailNotificationsEnabled" value="<%=emailNotificationsEnabled%>"/><br/><br/>	
	<aui:input inlineField="true" type="checkbox" label="Dockbar Notifications Enabled" name="dockbarNotificationsEnabled" value="<%=dockbarNotificationsEnabled%>"/><br/>	
	<br/><br/><br/>
	
	
	<aui:input inlineField="true" type="checkbox" label="JMS Enabled" name="jmsEnabled" value="<%=jmsEnabled%>"/><br/>
	<aui:input name="brokerJMSusername" id="brokerJMSusername" label="broker JMS username" value="<%=brokerJMSusername%>" type="text" helpMessage="e.g. tesb"/><br/>
	<aui:input name="brokerJMSpassword" id="brokerJMSpassword" label="broker JMS password" value="<%=brokerJMSpassword%>" type="text" helpMessage="e.g. tesb"/><br/>
	<aui:input name="brokerJMSurl" id="brokerJMSurl" label="broker JMS URL" value="<%=brokerJMSurl%>" type="text" helpMessage="e.g. tcp://oia.eng.it:8080"/><br/>
	<aui:input name="jmsTopic" id="jmsTopic" label="JMS Topic" value="<%=jmsTopic%>" type="text" helpMessage="e.g. IdeaManagement"/><br/>
	<br/><br/>
<aui:button type="submit" value="save"/>


</aui:form>

