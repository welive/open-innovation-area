<%@page import="it.eng.rspa.ideas.utils.MyConstants"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.liferay.portal.kernel.workflow.WorkflowTaskManager"%>
<%@page	import="com.liferay.portal.service.WorkflowDefinitionLinkLocalService"%>
<%@page import="com.liferay.portal.kernel.workflow.WorkflowDefinition"%>
<%@page	import="com.liferay.portal.kernel.workflow.WorkflowInstanceManagerUtil"%>
<%@page	import="com.liferay.portal.service.WorkflowInstanceLinkLocalService"%>
<%@page import="com.liferay.portal.kernel.workflow.WorkflowInstance"%>
<%@page import="com.liferay.portal.model.WorkflowInstanceLink"%>
<%@page	import="com.liferay.portal.service.WorkflowInstanceLinkLocalServiceUtil"%>
<%@page	import="com.liferay.portal.service.persistence.WorkflowInstanceLinkUtil"%>
<%@page import="com.liferay.portal.model.WorkflowDefinitionLink"%>
<%@page	import="com.liferay.portal.service.persistence.WorkflowDefinitionLinkUtil"%>
<%@page	import="com.liferay.portal.kernel.workflow.WorkflowDefinitionManagerUtil"%>
<%@page	import="com.liferay.portal.service.WorkflowDefinitionLinkLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.workflow.WorkflowTask"%>
<%@page	import="com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.OrderByComparator"%>
<%@page import="com.liferay.portal.model.RoleConstants"%>
<%@page import="com.liferay.portal.service.RoleLocalServiceUtil"%>
<%@page import="com.liferay.portal.model.Role"%>
<%@page	import="com.liferay.portal.kernel.workflow.WorkflowTaskManagerUtil"%>
<%@page	import="com.liferay.portal.kernel.workflow.WorkflowHandlerRegistryUtil"%>
<%@page	import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteChallengesLocalServiceUtil"%>
<%@page	import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeServiceUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.Challenges"%>
<%@page	import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges"%>
<%@page	import="it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesUtil"%>
<%@page import="com.ibm.icu.text.DecimalFormat"%>
<%@page import="com.liferay.portlet.messageboards.service.MBMessageLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.PortalClassLoaderUtil"%>
<%@page import="com.liferay.portal.kernel.dao.orm.DynamicQuery"%>
<%@page import="com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil"%>
<%@page import="com.liferay.portlet.messageboards.model.MBMessage"%>
<%@ include file="/html/challenges/init.jsp" %>

<%-- include jquery by theme --%>
<script src="<%=request.getContextPath()%>/js/D3/d3.v3.min.js"></script>

			
				<!-- CONTENUTO PORTLET -->
<%
PortletURL createIdeaURL = renderResponse.createRenderURL();
createIdeaURL.setParameter("jspPage", "/html/ideas/update.jsp");
createIdeaURL.setWindowState(WindowState.MAXIMIZED);
%>

<script type="text/javascript">
jQuery(document).ready(function() {
  jQuery(".content").show();
  //toggle the componenet with class msg_body
  jQuery(".heading").click(function()
  {
    jQuery(this).next(".content").slideToggle(500);
  });
});
</script>

<portlet:defineObjects />


	<%
		User currentUser = PortalUtil.getUser(request); 
		String fullname = currentUser.getFullName();
		String email = currentUser.getDisplayEmailAddress();

		
		long portraitId = currentUser.getPortraitId();
		Image image = ImageLocalServiceUtil.getImage(portraitId);
		
		Locale locale = (Locale)request.getSession().getAttribute("org.apache.struts.action.LOCALE");
		String language = locale.getLanguage();
		String country = locale.getCountry();
		ResourceBundle res = ResourceBundle.getBundle("Language", new Locale(language, country));
		String espandi= res.getString("ims.expand");
		espandi = new String(espandi.getBytes("ISO-8859-1"), "UTF-8");
		
		ThemeDisplay themeDisplay = 
		(ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
		Calendar cal = CalendarFactoryUtil.getCalendar();
	%>
<!-- 
<div id="riconoscimenti">
	<aui:fieldset label="I miei riconoscimenti">
	</aui:fieldset>
</div>
 -->
<div id="stats">
		<%
		User ideaUserStats = PortalUtil.getUser(request); 
		int monitoringIdeas = 0;
		int implementationIdeas = 0;
		int refinementIdeas = 0;
		int selectedIdeas = 0;
		int evaluationIdeas = 0;
		int selectionIdeas = 0;
		int monitoringIdeasPercent  = 0;
		int implementationIdeasPercent = 0;
		int refinementIdeasPercent = 0;
		int selectedIdeasPercent = 0;
		int evalutationIdeasPercent = 0;
		int selectionIdeasPercent = 0;
		
		double totIdeaScore = 0;
		double totIdeaComments = 0;
		
		// Recupero le idee che sono in statao di monitoraggio
		Long userIdStats = ideaUserStats.getUserId();
		Long companyIdStats = ideaUserStats.getCompanyId();
		Long groupIdStats = themeDisplay.getCompanyGroupId();

		
		List<CLSIdea> ideasStats = CLSIdeaLocalServiceUtil.getIdeasByUserId(userIdStats);
		for (CLSIdea idea : ideasStats) {
			
			if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_MONITORING))) {
				monitoringIdeas++;
			} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_IMPLEMENTATION))) {
				implementationIdeas++;
			} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_REFINEMENT))) {
				refinementIdeas++;
			} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_SELECTED))) {
				selectedIdeas++;
			} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_EVALUATION))) {
				evaluationIdeas++;
			} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_ELABORATION))) {
				selectionIdeas++;
			}
			
			List<RatingsEntry> lstRE = RatingsEntryLocalServiceUtil.getEntries(CLSIdea.class.getName(), idea.getIdeaID());
			for(RatingsEntry RE:lstRE) {
				totIdeaScore += RE.getScore();
		    }
			
			//ottengo i commenti
			DynamicQuery mbMessageQuery = DynamicQueryFactoryUtil.forClass(MBMessage.class, PortalClassLoaderUtil.getClassLoader());
			AssetEntry entryIdea = AssetEntryLocalServiceUtil.getEntry(CLSIdea.class.getName(), Long.valueOf(idea.getIdeaID()));
			mbMessageQuery.add(RestrictionsFactoryUtil.eq("classNameId", Long.valueOf(entryIdea.getClassNameId())));
			mbMessageQuery.add(PropertyFactoryUtil.forName("parentMessageId").ne(Long.valueOf(0)));
			mbMessageQuery.add(PropertyFactoryUtil.forName("classNameId").eq(Long.valueOf(entryIdea.getClassNameId())));
			mbMessageQuery.add(PropertyFactoryUtil.forName("classPK").eq(Long.valueOf(idea.getIdeaID())));
			mbMessageQuery.add(PropertyFactoryUtil.forName("categoryId").eq(Long.valueOf(-1)));
			List comments = MBMessageLocalServiceUtil.dynamicQuery(mbMessageQuery);
			totIdeaComments += comments.size();
		}
		
		int totalIdeas = monitoringIdeas + evaluationIdeas+implementationIdeas + refinementIdeas + selectionIdeas; 
		
		if(totalIdeas>0){
			monitoringIdeasPercent = (monitoringIdeas * 100) / totalIdeas;
			implementationIdeasPercent = (implementationIdeas * 100) / totalIdeas;
			refinementIdeasPercent = (refinementIdeas * 100) / totalIdeas;
			selectedIdeasPercent = (selectedIdeas * 100) / totalIdeas;
			evalutationIdeasPercent = (evaluationIdeas * 100) / totalIdeas;
			selectionIdeasPercent = (selectionIdeas * 100) / totalIdeas;
		}
		
		double averageIdeaScore = 0;
		double averageIdeaComments = 0 ;
		
		if(ideasStats.size()>0){
			averageIdeaScore = (double)totIdeaScore / (double)ideasStats.size();
			averageIdeaComments = (double)totIdeaComments / (double)ideasStats.size();
		}
		
		DecimalFormat decimalF = new DecimalFormat("#.##");
		
		%>

	<div id="statsGraph">
		<div id="chart-default">
		</div>
	</div>	
	
</div>

<%

	if(totalIdeas>0){
%>

<script type="text/javascript">

	var w = 260,
	h = 260,
	r = 120,
	inner = 70,
	legendW = 150,
	legendH = 120,
	color = gradient;
	
	data = [{"label":'<liferay-ui:message key="ims.processing"/>', "value":<%=selectionIdeas %>, "percent":<%=selectionIdeasPercent %>}, 
	        {"label":'<liferay-ui:message key="ims.evaluation"/>', "value":<%=evaluationIdeas %>, "percent":<%=evalutationIdeasPercent %>}, 
	        {"label":'<liferay-ui:message key="ims.selected"/>', "value":<%=selectedIdeas %>, "percent":<%=selectedIdeasPercent %>}, 
	        {"label":'<liferay-ui:message key="ims.refinement"/>', "value":<%=refinementIdeas %>, "percent":<%=refinementIdeasPercent %>}, 
	        {"label":'<liferay-ui:message key="ims.implementation"/>', "value":<%=implementationIdeas %>, "percent":<%=implementationIdeasPercent %>},
	        {"label":'<liferay-ui:message key="ims.monitoring"/>', "value":<%=monitoringIdeas %>, "percent":<%=monitoringIdeasPercent %>}
	        ];
	
	var total = <%=totalIdeas%>; 
	
	var vis = d3.select("#chart-default")
	.append("svg:svg")
	.data([data])
	    .attr("width", w)
	    .attr("height", h)
	.append("svg:g")
	    .attr("transform", "translate(" + w/2 + "," + w/2 + ")");
	
	var textTop = vis.append("text")
	.attr("dy", ".35em")
	.style("text-anchor", "middle")
	.attr("class", "textTop")
	.text( '<liferay-ui:message key="ims.active-ideas-total"/>' )
	.attr("y", -10),
	textBottom = vis.append("text")
	.attr("dy", ".35em")
	.style("text-anchor", "middle")
	.attr("class", "textBottom")
	.text(total.toFixed(0))
	.attr("y", 10);
	
	var arc = d3.svg.arc()
	.innerRadius(inner)
	.outerRadius(r);
	
	var arcOver = d3.svg.arc()
	.innerRadius(inner + 5)
	.outerRadius(r + 5);
	
	var pie = d3.layout.pie()
	.value(function(d) { return d.value; });
	
	var arcs = vis.selectAll("g.slice")
	.data(pie)
	.enter()
	    .append("svg:g")
	        .attr("class", "slice")
	        .on("mouseover", function(d) {
	            d3.select(this).select("path").transition()
	                .duration(200)
	                .attr("d", arcOver)
	            
	            textTop.text(d3.select(this).datum().data.label)
	                .attr("y", -10);
	            textBottom.text(d3.select(this).datum().data.value.toFixed(0))
	                .attr("y", 10);
	        })
	        .on("mouseout", function(d) {
	            d3.select(this).select("path").transition()
	                .duration(100)
	                .attr("d", arc);
	            
	            textTop.text( '<liferay-ui:message key="ims.active-ideas-total"/>' )
	                .attr("y", -10);
	            textBottom.text(total.toFixed(0));
	        });
	
	arcs.append("svg:path")
	.attr("fill", function(d, i) { return color[i]; } )
	.attr("d", arc);

</script>
<%
}


if(totalIdeas==0){

%>	
	<p class='infoText bottomSpace'><liferay-ui:message key="ims.no-data"></liferay-ui:message></p>
<%	
}
%>

 