<%@page import="it.eng.rspa.ideas.utils.IdeasListUtils"%>
<%@page import="com.liferay.portal.model.Organization"%>
<%@page import="it.eng.rspa.ideas.utils.UpdateIdeaUtils"%>
<%@page import="it.eng.rspa.ideas.utils.MyUtils"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSCategoriesSetForChallengeLocalServiceUtil"%>
<%@page import="com.liferay.portal.model.Role"%>
<%@page import="com.liferay.portal.service.RoleLocalServiceUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaPoiLocalServiceUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi"%>
<%@page import="org.apache.commons.lang3.ArrayUtils"%>
<%@page import="it.eng.rspa.ideas.controlpanel.CategoryKeys"%>
<%@page import="com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil"%>
<%@page import="com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil"%>
<%@page import="com.liferay.portlet.asset.model.AssetVocabulary"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet"%>
<%@page import="it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="com.liferay.portal.util.PortletKeys"%>
<%@page import="javax.portlet.PortletRequest"%>
<%@page import="com.liferay.portlet.PortletURLFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletURL"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFileEntry"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.OrderByComparator"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil"%>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFolder"%>
<%@page import="javax.portlet.PortletPreferences"%>
<%@page import="com.liferay.util.PwdGenerator"%>
<%@page import="java.io.File"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSCoworkerUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSCoworkerLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.dao.orm.QueryUtil"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="com.liferay.portal.service.RoleLocalServiceUtil"%>
<%@page import="com.liferay.portlet.asset.model.AssetTag"%>
<%@page import="com.liferay.portlet.asset.service.AssetTagServiceUtil"%>
<%@page import="com.liferay.portlet.asset.model.AssetCategory"%>
<%@page import="com.liferay.portlet.asset.service.AssetCategoryServiceUtil"%>
<%@page import="it.eng.rspa.ideas.utils.StringComparator"%>
<%@page import="com.liferay.portal.kernel.util.KeyValuePair"%>
<%@ include file="/html/challenges/init.jsp" %>

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script src="<%=request.getContextPath()%>/js/upload/jquery.uploadfile.js"></script>

		<script src="<%=request.getContextPath()%>/js/customckeditor/ckeditor.js"></script>
		
		<link href="<%=request.getContextPath()%>/css/upload/uploadfile.css" rel="stylesheet"/>
		<link href="<%=request.getContextPath()%>/css/creation.css" rel="stylesheet"/>
		
		<portlet:renderURL var="portletURL" windowState="normal">
			<portlet:param name="struts_action" value="/ext/reports/view_reports"/>
		</portlet:renderURL>
		
		<portlet:actionURL var="editCaseURL" name="uploadCase">
		    <portlet:param name="jspPage" value="/edit.jsp" />
		</portlet:actionURL>
		
		<portlet:resourceURL var="resourceURL" />
		
		<portlet:actionURL var="deleteFileURL" >
		    <portlet:param name="<%=ActionRequest.ACTION_NAME%>" value="deleteFile" />
		</portlet:actionURL>
		
		<portlet:actionURL var="updateIdeasURL">
			<portlet:param name="<%=ActionRequest.ACTION_NAME%>" value="updateIdeas" />
		</portlet:actionURL>
		
		
		<%@ include file="checkUser.jspf" %>
		
		<% 
		
		Locale locale = (Locale)request.getSession().getAttribute("org.apache.struts.action.LOCALE");
		
		String uuid = UUID.randomUUID().toString();
		
		User currentUser = PortalUtil.getUser(request);
		
		boolean needReport=true;
		String pathDefaultimg = "/img/needsImg.png";
		String urlHomeIms = IdeaManagementSystemProperties.getUrlHomeIMS();   
		
	    //per la gestione delle municipalita
    
	  //List<User> municipalitaI = UpdateIdeaUtils.getAllMunicipalities();
	    List<Organization> municipalitaI = UpdateIdeaUtils.getAllOrganizationMunicipalities();
	    
	   // List<User> municipalita = MyUtils.getListaUtenticonPilotUguali(currentUser, municipalitaI);//filtro su pilot
		 List<Organization> municipalita = MyUtils.getListaOrganizationsconPilotUgualeByUser(currentUser, municipalitaI); //filtro su pilot
	    
		 String pilota = IdeasListUtils.getPilot(renderRequest);
		 String [] coordinateMap = 	MyUtils.getMapLatLongCenterbyPilot(pilota);
		 
		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
		
		Long groupId = themeDisplay.getCompanyGroupId();
		pageContext.setAttribute("themeDisplay", themeDisplay);
		
		//recupero i dati dell'idea se sono in modifica
		long id = -1;
		long challengeId = -1;
		long municipalityId = 0;
		String authorId = "-1";
		String ideaId = "-1";
		String ideaTitle = "";
		String ideaHashTag="";
		String ideaeDescription = "";
		String tags = "";
		String linguaMod = "";
		long modifyVocabularyId = 0;
		String modifyCategs = "";

				
		//Gestione immagine rappresentativa
		String ideaRepresentativeImgUrl = "";
		//Gestione immagine rappresentativa
		
		Iterator<DLFileEntry> iter =null;
		ArrayList<Long> coworkesUsersIds = new ArrayList<Long>();
		Map<String, String[]> paramMap = request.getParameterMap();
		String[] paramIdeaId = paramMap.get("ideaId");
		
		boolean isModifica = false;	
		boolean areDescriptionAndTagsEditable = true;
		boolean areIdeaFieldsEditable = true;
		
		
		
		if(paramIdeaId!=null){
			if(paramIdeaId.length>0){
				String ideaIdText = paramMap.get("ideaId")[0];
				if(ideaIdText!=null){
					try{
						id = Long.parseLong(ideaIdText);
						//recupero i dati del challenge
						if(id>0){ //sono in modifica
							
							isModifica = true;
							
							CLSIdea idea = CLSIdeaLocalServiceUtil.getCLSIdea(id);
							ideaId = Long.toString(id);
							
							areDescriptionAndTagsEditable = UpdateIdeaUtils.isDescriptionIdeaEditable(idea);
							areIdeaFieldsEditable = UpdateIdeaUtils.areIdeaFieldsEditable(idea);
							tags =IdeasListUtils.getTagsVirgoleByIdeaId(id);
							
							modifyVocabularyId = IdeasListUtils.getVocabularyIdByIdeaId(id);
							modifyCategs = IdeasListUtils.getCategoriesIdCommasByIdeaId(id);
							
							
							
							authorId = Long.toString(idea.getUserId());
							ideaeDescription = idea.getIdeaDescription();
							ideaTitle = idea.getIdeaTitle();
							challengeId = idea.getChallengeId();
							municipalityId = idea.getMunicipalityId();
							linguaMod = idea.getLanguage();
							
							//recupero dei documenti associati
							DLFolder folder = DLFolderLocalServiceUtil.getDLFolder(idea.getIdFolder());
							Long folderId = idea.getIdFolder();
							Integer count = DLFileEntryLocalServiceUtil.getFileEntriesCount(groupId, folderId);
							OrderByComparator obc = OrderByComparatorFactoryUtil.create("DLFileEntry", "title", true);
							List<DLFileEntry> files = DLFileEntryLocalServiceUtil.getFileEntries(groupId.longValue(), folderId.longValue(), 0, count,obc);
							iter = files.iterator();
							
							
							// Gestione immagine rappresentativa
							if(idea.getRepresentativeImgUrl() != null){
								ideaRepresentativeImgUrl = idea.getRepresentativeImgUrl();
							}
							
							
						}//if(id>0){
					}catch(Exception e){
						
					}
				}
			}
		}
		
	
		%>
<div class="materialize">
	<div class="row">
		<div class="col s12">
			<a class="waves-effect btn-flat tooltipped" href="<%=urlHomeIms %>" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.go-back"/>">
				<i class="icon-arrow-left left">&nbsp;</i><liferay-ui:message key="back"/>
			</a>
		</div>
	</div>


<div class="css-class-wrapper-update-fake">
		
		
		<aui:form name="fm" method="POST" action="<%= updateIdeasURL.toString() %>" >
		<!-- enctype="multipart/form-data" > -->
			
			<!-- Gestione immagine rappresentativa campi -->
				<aui:model-context  model="<%=CLSIdea.class%>"></aui:model-context>
				<aui:input name="ideaId" type="hidden" value="<%=ideaId %>"></aui:input>
				<aui:input name="authorId" type="hidden" value="<%=authorId %>"></aui:input>
				<aui:input name="groupid" type="hidden" value="<%=groupId.longValue()%>"/> <!-- questo campo deve essere nascosto -->
				<aui:input name="token" type="hidden" value="<%=uuid%>"/><!-- questo campo deve essere nascosto -->												
				<aui:input name="assetTagNames" type="hidden" value="<%=IdeasListUtils.getTagsCommasByIdeaId(Long.valueOf(ideaId)) %>"/>								
				
											
<!-- INFORMAZIONI GENERALI -->
		<%
		String panelTitle;
		if(ideaId.equals("-1")){ panelTitle = "ims.create.need";}
		else{panelTitle="ims.edit.need";}%>
		
		<div id="introduction" class="section">
	    	<div class="row">
	    		<div class="col s12">
	    			<div class="row">
   						<div class="col s12 flow-text weight-4 center"><liferay-ui:message key="<%=panelTitle %>"/></div>
   						<br>
   						<div class="bottom-margin"></div>
   						<div class="divider"></div>
		
						<blockquote>
						  	*&nbsp;<liferay-ui:message key="ims.starred-are-mandatory"/>
						</blockquote>
				<!-- Selezione  Ente -->				

												
					<!-- Inserisco  municipalita -->
					<div class="ideaToPropose">
						
						<span class="challengeMunicType"style="display:none">
							
							  <input type="radio" id="radioChallengeMunicType2" name="<portlet:namespace/>challengeMunicType" value="challengeMunicTypeMunic" class="with-gap" checked="true" onChange="gestioneSelettori()"/>
							  <label for="radioChallengeMunicType2" ><liferay-ui:message key="ims.select-municipality"/>

						</span>						
						
						<div class="divSelectsPropose">	
									
							<!-- Seleziono l'autority  -->			
							  <div id="municipalityIdSpan" style="display:none">
							  <%if(municipalita.size()==1){%>
							  		<input type="hidden" name="<portlet:namespace/>municipalityOrganizationId" value="<%=municipalita.get(0).getOrganizationId()%>"/>
							  		<div class="center" style="display: inline-block;">
										<i class="material-icons left">location_city</i>
										<span class=""><%=municipalita.get(0).getName()%></span>
									</div>
							  	<%}
							  	else{%>
							  	<div class="row">
							  		<div class="col s12">
									    <div class="input-field">
									    
									    <c:choose>
										    <c:when test="<%= areIdeaFieldsEditable %>">
										    	 <select  name="<portlet:namespace/>municipalityOrganizationId" id="<portlet:namespace/>municipalityOrganizationId">
										    </c:when>
										     <c:otherwise>
										      	<select disabled name="<portlet:namespace/>municipalityOrganizationId" id="<portlet:namespace/>municipalityOrganizationId">
										     </c:otherwise>
									     </c:choose>
									    
										   
										      <option value="" disabled selected><liferay-ui:message key="ims.choose-your-option"/></option>
										      <% 
												for(int i=0; i<municipalita.size(); i++){						
												
													if (i ==0){
												%>
														<option value="<%=municipalita.get(i).getOrganizationId() %>" selected><%=municipalita.get(i).getName() %></option>
													<%}else{%>
										     	 		<option value="<%=municipalita.get(i).getOrganizationId() %>"><%=municipalita.get(i).getName() %></option>
										      <%	}
												}%>
										    </select>
										    <label><liferay-ui:message key="ims.select-municipality"/>&nbsp;*</label>
									  	</div>
								  	</div>
							  	</div>
								  <%} %>
							  </div>
							
						</div>	
																								
					</div>

				<!-- fine	Need to Propose Ente -->		
				
				<!-- Inizio	Scelta lingua -->	
					<%@ include file="choiceOfLanguage.jspf" %>
				
				<!-- fine	Scelta lingua-->
				
<!-- 		Sezione Immagine e Descrizione Need -->
					<div class="section row">
						<%@ include file="generalSectionUpdate.jspf" %>
					</div>
<!-- 	fine Sezione Immagine e Descrizione Need -->
				
					<div class="block next-button-wrapper">
							<button class="right next-button btn-flat" id="next-general" data-nextid="cat-tag-panel">
							  <span class="icon-arrow-down"></span>
							</button>
					</div>
				</div>
			</div>
		</div>
		
				
<!-- Categorie e Tags -->
				<div class="section" id="cat-tag-panel">
					<h5 class="header"><liferay-ui:message key="ims.categories-and-keywords"/></h5> 
					<div class="divider bottom-margin "></div>	
					<%@ include file="needUpdate/categoriesUpdateNeed.jspf" %>
					<%@ include file="tagsUpdate.jspf" %>
				</div>
				
<!-- fine Categorie e Tags --> 


<!-- attachments -->
				<div class="section" id="attach-panel" >
					<h5 class="header"><liferay-ui:message key="attachments"/><liferay-ui:icon-help message="ims.tip-attachments"/></h5> 
					<div class="divider bottom-margin "></div>		
					<%@ include file="attachmentsUpdate.jspf" %>
				</div>
<!-- fine attachments -->
			
		
		
			
			
			<!-- Pulsanti Salva e Cancella della pagina -->
			<div id="mainFormButtons" class="material-design-restyle btn--raised btn--colored single-btn-space" >
				<aui:button-row>
					<aui:button cssClass="waves-effect waves-light btn core-color color-1" type="button" value="publish" id="submitForm"  />
					<aui:button cssClass="waves-effect waves-light btn core-color color-1" type="cancel" value="cancel" onClick="history.go(-1);"/>			
				</aui:button-row>
			</div>
		
		</aui:form>
			<div id="overlay">
				<aui:form name="newPOIForm" id="newPOIForm">
					<span id="POIDiplayCoordinates"></span>
					<aui:input name="POITitle" id="POITitle" label="title" value="" type="text" />
					<aui:input name="POIDescription" id="POIDescription" label="description" value="" type="textarea" />
					<aui:input name="POILatitude"  id="POILatitude"   value="" type="hidden" /> 
					<aui:input name="POILongitude" id="POILongitude"  value="" type="hidden" /> 
					<aui:button type="button" value="add" id="addPOI"/>
					<aui:button type="reset" value="cancel"  id="cancelAddPOI"/>
			  	</aui:form>
			</div>

		
		<form style="display: none;" method="POST" id="fm2" name="fm2" enctype="multipart/form-data">
		     <input type="file" name="myFile" id="myFile" onchange="imgsubmit()"/>
		     <input type="submit" id="btnsubmit" value="Save"/>
		</form>		

</div><!-- fine css-class-wrapper-update-fake -->


</div><!-- end Materialize -->
		
		
<script>
		
		var editor;
		var markers = {};
		
		function markerHash(marker){
			  var position = marker.getPosition();
			  return position.lat() + "_" + position.lng();
		};
		
		function changeEnter() {
			// If we already have an editor, let's destroy it first.
			if ( editor )
				editor.destroy( true );
			
			var token = Liferay.authToken;
			var listUrl = "<%=themeDisplay.getPortalURL()%>/api/jsonws/Challenge62-portlet.clsidea/get-idea-images/idea-id/<%=ideaId%>?p_auth=";
			listUrl = listUrl+token;
		
			// Create the editor again, with the appropriate settings.
			editor = CKEDITOR.replace( 'ideaDesc');
			CKEDITOR.config.allowedContent = true; /*questa chiamata permette di caricare i contenuti di youtube a runtime */
			
		}
		
		window.onload = changeEnter;
		
		function submitFormData() {
			var editorText = CKEDITOR.instances.ideaDesc.getData();
			document.getElementById('<portlet:namespace/>ideaDescription').setAttribute('value', editorText);
			
			
			$("#<portlet:namespace />fm").submit();
		}
		
		function deletePOI(markerHashIdentifier){
			var marker = markers[markerHashIdentifier];
			marker.setMap(null);
			markers[markerHashIdentifier] = null;
			
			var poisList = document.getElementById("POIsList");
			poisList.removeChild(document.getElementById("<portlet:namespace/>newPoiInputTitle_#"+markerHashIdentifier));
			poisList.removeChild(document.getElementById("<portlet:namespace/>newPoiInputDescription_#"+markerHashIdentifier));
			poisList.removeChild(document.getElementById("<portlet:namespace/>newPoiInputLatidute_#"+markerHashIdentifier));
			poisList.removeChild(document.getElementById("<portlet:namespace/>newPoiInputLongitude_#"+markerHashIdentifier));
		}
		
		
		
		function gestioneSelettori(){
			
			if ($("#radioChallengeMunicType1").is(":checked")){
												
				$("#municipalityIdSpan").css('display','none');
				$("#challengeSelectionIdSpan").css('display','block');
				$("#mapContainer").css('display','none');
				$("#mapContainer").parent().removeClass('l7');
				$(".contentUpload").parent().removeClass('l5');
				
																				
			}else{
				$("#mapContainer").css('display','block');
				$("#mapContainer").parent().addClass('l7');
				$(".contentUpload").parent().addClass('l5');
				$("#municipalityIdSpan").css('display','block');
				$("#challengeSelectionIdSpan").css('display','none');	
			}														
		}
		
		
		function  updateCategoriesAjax(){
			
			/*get the select value */
			var selectedVoc = $( "#selectVocabulary" ).val();
				

			AUI().use('aui-io-request', function(A){
			  A.io.request('<%=resourceURL.toString()%>', {
		          method: 'post',
		          data: {
		       			   <portlet:namespace />param: 'categoriesByVocabolaryId',
		       	 		   <portlet:namespace />vocId: selectedVoc,
		       	 		   <portlet:namespace />localePerAssetVoc: '<%=locale%>',
		       	 		   
		          },
		          dataType: 'json',
		          on: {
		              success: function() {
		            	  
		            	  var modCat= [<%=modifyCategs%>]
		            	  
		            	  
		            	  var jsonVocArray = this.get('responseData');
		            	  var conteinerSelect = $('#selectCategories');
		            	  
		            	  $(conteinerSelect).empty();
		            	  $(conteinerSelect).append( "<option value='' disabled selected><liferay-ui:message key='ims.choose-one-or-more-subcategories'/></option>");
		            	  
		            	  
		            	  jQuery.each(jsonVocArray, function(index, voc) {
		            		  
		            		  if (modCat.indexOf(voc.categId) >-1)
		            		     $(conteinerSelect).append( "<option value='"+voc.categId+"' selected>"+voc.categName+"</option>");
		            		  else
		            			  $(conteinerSelect).append( "<option value='"+voc.categId+"' >"+voc.categName+"</option>"); 
		            		  
		            	  });/* jQuery.each */
		                  
		            	  
		            	  $(conteinerSelect).material_select();
		            	  
		                   
			              }/* success */
			         }/*on	*/				               
			   });/*A.io.request */					 
			});/*AUI().use*/
			
		}/*updateCategoriesAjax */


		//controllo che ci sia almeno una categoria selezionata
		function checkMaterialCategories(){
			
			var catFound = false;
			var selectedValues=$( "#selectCategories" ).val();
			
			
			if (selectedValues != null){//it is null if neither the Vocabulary was chosen
			
				if (selectedValues.length > 0)
					catFound = true;	
			}
			
			
			return !catFound;
		}
		
		
$(document).ready(function(){
	
			//Iserisce una classe di comodo nella section 
			//ad insicare che si tratta di una nuova idea e non di una modifica
			<%if(ideaId.equals("-1")){%>
				$(".section").addClass("newCreation");
			<%}else{%>
			
			updateCategoriesAjax();//Categories on select, basing on Vocabulary
			
			<% 
			}
			if(challengeId == -1){ %>
			//caricamento selettori alternativi gare/enti
			gestioneSelettori();
			<%}%>

			
			
			
			var uploadObj =$("#advancedUpload").uploadFile({
				formData: { token: '<%=uuid%>'},
				url:"<%= editCaseURL %>",
				autoSubmit:false,
				fileName:"myfile",
				showProgress:true,
				dragDropStr : '<em><liferay-ui:message key="ims.drag-e-drop-files"/></em>',
				uploadStr:'<liferay-ui:message key="ims.add-document"/>',
	            cancelStr: '<liferay-ui:message key="cancel"/>',
	            deletelStr: '<liferay-ui:message key="delete"/>',
	            extErrorStr: '<liferay-ui:message key="ims.is-not-allowed-allowed-extensions"/>',
	            duplicateErrorStr: '<liferay-ui:message key="ims.is-not-allowed-allowed-file-already-exists"/>',
	            sizeErrorStr: '<liferay-ui:message key="ims.is-not-allowed-allowed-max-size"/> ',
	            dragdropWidth: 'auto',
	            statusBarWidth: 'auto',
	            allowedTypes: "<%=PortletProps.get("upload-allowed-extension") %>",
	            maxFileSize:<%=PortletProps.get("upload-max-bytes-size") %>, /*20MB*/
				showStatusAfterSuccess:false,
				afterUploadAll:function(obj){
					submitFormData();
				}
				});

			
				$("#submitForm").click(function(){
					
					//I use Ajax to check the entries
					 AUI().use('aui-io-request', function(A){
					    	
						 var ideaTitle = document.getElementById('<portlet:namespace/>ideaTitle').value;
					 
					        A.io.request('<%=resourceURL.toString()%>', {
					               method: 'post',
					               data: {
					            	   <portlet:namespace />param: 'titoloIdea',
					            	   <portlet:namespace />titleChoosen: ideaTitle,
					            	   <portlet:namespace />modifica: '<%=ideaId%>'
					               },
					               dataType: 'json',
					               on: {
					                   success: function() {
					                    
					                	    var ajRitornoTitleExist = this.get('responseData').titleExist;
						                    var ckCat = checkMaterialCategories();

	
						                    if( (ideaTitle.trim() == "") ){
												alert('<liferay-ui:message key="ims.insert-title"/>');
						                    }else if(ajRitornoTitleExist == 'yes'){
					                			alert('<liferay-ui:message key="ims.existing-title"/>');	
						                    }else if( ckCat ){
							                    alert('<liferay-ui:message key="please-select-a-category"/>');	
											}else if(CKEDITOR.instances.ideaDesc.getData().trim() == ""){
												alert('<liferay-ui:message key="ims.insert-description"/>');
											}else{
												if(uploadObj.getFileCount()>0){		 
													uploadObj.startUpload();
												 }else{
													 submitFormData();
												 }
											}//if else					                  	                   	                	
					                   }//success
					              }//on					               
					        });//A.io.request					 
					    });//AUI().use					
				});//$("#submitForm").click
				
				$("#cancelAddPOI").click(function(){
					
					//il contenuto dei campi � cancellato dal pulsante annulla che � di tipo reset
					document.getElementById('map_canvas').style.display='block';
					document.getElementById('mainFormButtons').style.display='block';
			     	document.getElementById('overlay').style.display='none';
				});
				
				$("#addPOI").click(function(){
					
					var poiTitle       = document.getElementById('<portlet:namespace/>POITitle').value; //getAttribute('value');
					var poiDescription = document.getElementById('<portlet:namespace/>POIDescription').value; //getAttribute('value');
					
					poiTitle = poiTitle.split('\"').join('\'');
					poiDescription = poiDescription.split('\"').join('\'');
					
					var poiLatitude    = document.getElementById('<portlet:namespace/>POILatitude').getAttribute('value');
					var poiLongitude   = document.getElementById('<portlet:namespace/>POILongitude').getAttribute('value');
			  	
					if((poiTitle.trim() == "") || (poiDescription.trim() == "")){
						alert('<liferay-ui:message key="ims.insert-poi-title-description"></liferay-ui:message>');
					}else{
						// poiLatitude+"_"+poiLongitude � uguale al markerHashIdentifier utilizzato per la cancellazione in function deletePOI(markerHashIdentifier)
						
					  	var newPoiInputTitle = document.createElement("input");
				  		newPoiInputTitle.setAttribute("type", "hidden");
				  		newPoiInputTitle.setAttribute("value", poiTitle);
				  		newPoiInputTitle.setAttribute("name", "<portlet:namespace/>newPoiInputTitle_#"+poiLatitude+"_"+poiLongitude);
				  		newPoiInputTitle.setAttribute("id", "<portlet:namespace/>newPoiInputTitle_#"+poiLatitude+"_"+poiLongitude);
				  		newPoiInputTitle.setAttribute("class", "field");
					  		
					  	var newPoiInputDescription = document.createElement("input");
					  	newPoiInputDescription.setAttribute("type", "hidden");
					  	newPoiInputDescription.setAttribute("value", poiDescription);
					  	newPoiInputDescription.setAttribute("name", "<portlet:namespace/>newPoiInputDescription_#"+poiLatitude+"_"+poiLongitude);
					  	newPoiInputDescription.setAttribute("id", "<portlet:namespace/>newPoiInputDescription_#"+poiLatitude+"_"+poiLongitude);
					  	newPoiInputDescription.setAttribute("class", "field");
							
					  	var newPoiInputLatidute = document.createElement("input");
					  	newPoiInputLatidute.setAttribute("type", "hidden");
					  	newPoiInputLatidute.setAttribute("value", poiLatitude);
					  	newPoiInputLatidute.setAttribute("name", "<portlet:namespace/>newPoiInputLatidute_#"+poiLatitude+"_"+poiLongitude);
					  	newPoiInputLatidute.setAttribute("id", "<portlet:namespace/>newPoiInputLatidute_#"+poiLatitude+"_"+poiLongitude);
					  	newPoiInputLatidute.setAttribute("class", "field");
							
					  	var newPoiInputLongitude = document.createElement("input");
					  	newPoiInputLongitude.setAttribute("type", "hidden");
					  	newPoiInputLongitude.setAttribute("value", poiLongitude);
					  	newPoiInputLongitude.setAttribute("name", "<portlet:namespace/>newPoiInputLongitude_#"+poiLatitude+"_"+poiLongitude);
					  	newPoiInputLongitude.setAttribute("id", "<portlet:namespace/>newPoiInputLongitude_#"+poiLatitude+"_"+poiLongitude);
					  	newPoiInputLongitude.setAttribute("class", "field");
						
						var poisList = document.getElementById("POIsList");
						poisList.appendChild(newPoiInputTitle);
						poisList.appendChild(newPoiInputDescription);
						poisList.appendChild(newPoiInputLatidute);
						poisList.appendChild(newPoiInputLongitude);
						
						document.getElementById('<portlet:namespace/>POITitle').value = "";
						document.getElementById('<portlet:namespace/>POIDescription').value = "";
						document.getElementById('<portlet:namespace/>POILatitude').setAttribute('value', "");
						document.getElementById('<portlet:namespace/>POILongitude').setAttribute('value', "");
						
						
						var latlng = new google.maps.LatLng(poiLatitude, poiLongitude);
						
				    	var marker = new google.maps.Marker({
							position: latlng,
							map: map, 
				            title: poiTitle,
				            html: ""
				        });
		
						var markerHashIdentifier = markerHash(marker);
		
						var infoWindowContent = '<b><liferay-ui:message key="ims.title"></liferay-ui:message></b><br/>'+
												poiTitle+
												'<br/><b><liferay-ui:message key="ims.description"></liferay-ui:message></b><br/>'+
												poiDescription+
												'<br/><button type=\"button\" onclick=\"deletePOI(\''+markerHashIdentifier+'\');\" class=\"btn\"><liferay-ui:message key="ims.delete"></liferay-ui:message></button>';
												
						marker.html = infoWindowContent;
						
						markers[markerHashIdentifier] = marker;
		
						google.maps.event.addListener(marker, 'click', function() {
							infowindow.setContent(this.html);
							infowindow.open(map,this);
						});
						
						document.getElementById('map_canvas').style.display='block';
						document.getElementById('mainFormButtons').style.display='block';
				     	document.getElementById('overlay').style.display='none';
					}
					
				});
				
				
				$('.next-button').click(function(e){
					e.preventDefault();
					var gotoId = $(this).data('nextid');
					scrollPage(gotoId);
				});
				
				/* Manage mouse hover sample image */
				/*
				$('#imgDiv').mouseenter(function(){
					$(this).find('span').stop().animate({opacity: '100'}, 'slow');
				}).mouseleave(function(){
					$(this).find('span').stop().animate({opacity: '0'}, "slow");
				})
				*/
				
		});
		
		function scrollPage(elementId){
			var position = $('#'+elementId).offset().top;
			$("html, body").animate({scrollTop:(position-45)}, 'slow');
			return;
		}
		
		function deleteFile(fileId) {
			if (confirm('<liferay-ui:message key="ims.delete-document-message"></liferay-ui:message>')) { 
				var url = "<%=deleteFileURL.toString()%>";
				
				Liferay.Service(
							'/Challenge62-portlet.clsidea/delete-idea-file',
						  {
						    fileEntryId: fileId
						  },
						  function(obj) {
// 						    console.log(obj);
						    $( "#div_"+fileId).remove();
						  }
						);
			}
		}
		</script>
		
		
		
		
		<!-- Gestione immagine rappresentativa script -->
		<script charset="utf-8" type="text/javascript">
		function <portlet:namespace/>selectRepresentativeImg(imgURL){
// 			console.log(imgURL);
			document.getElementById('<portlet:namespace/>representativeImgUrl').setAttribute('value', imgURL);
			var pic1 = document.getElementById("<portlet:namespace/>article-image");
			pic1.src = imgURL;
		}
		</script>
		
		<script>
		function imgsubmit() {
			 $("#btnsubmit").trigger("click");
		}
		
		function flb() {
			 $("#myFile").trigger("click");
		}
		</script>
		
		
		<!-- Gestione immagine rappresentativa tramite Ajax -->
		<aui:script use="aui-io-request,aui-node" >
		
		YUI().use(
		  'aui-node',
		  'aui-io-request',
		  function(Y) {
		 
		  var node = Y.one('#btnsubmit');
		var img=Y.one('#<portlet:namespace/>representativeImgUrl');
		    
		    node.on(
		      'click',
		      function(){
		      
		      var A = AUI();
		  A.io.request('<%=resourceURL.toString()%>',{
		  method: 'POST',
		  form: { id: 'fm2', upload: true },
		  data: {
					  <portlet:namespace/>param:'caricamento',
						
					},
		  
		  on: {
		  complete: function(){
		   	
		  	
		  	display();
		  	
		   
		       }
		     }
		    });
		      }
		    );
		    
		    
		    function display(){
		   
		    
		       Y.io.request('<%=resourceURL.toString()%>',
		 			   
			            {
				  data: {
					  //qua mandiamo il parametro vista
					  
					  <portlet:namespace/>param:'view',
						
					},
			              dataType: 'json',
			              on: {
			                success: function() {
			                	
			                	var data = this.get('responseData');
			                	
			                	img.set("value", data.value);
		  						var pic1 = document.getElementById("<portlet:namespace/>article-image");
		  						
		  						pic1.src =data.value;
			                	
			                }
			              }
			            }
			          );
			  
		    }
		  }
		);
		</aui:script >
		
		


		
