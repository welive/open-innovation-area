
<!-- Card Header -->
<%@page import="javax.portlet.WindowState"%>
<%@page import="javax.portlet.PortletURL"%>
<div class="header-card valign-wrapper coloreVocabolario lighten-1 white-text">

		<div class="owner_label hide"> 
			<i class="material-icons fa-fw author hide tooltipped" data-position="left" data-delay="50" data-tooltip="ims.you-are-collaborator">account_circle</i> 
			<i class="material-icons fa-fw coworker hide tooltipped" data-position="left" data-delay="50" data-tooltip="ims.you-are-author">build</i>
		</div>
		
        <div class="col s2 center">
			<i class="material-icons iconaVocabolario circle small"></i>
        </div>
        <div class="col s8 tooltipped" data-position="bottom" data-delay="50" data-tooltip="Category">
          <span class="truncate titoloVocabolario">
            
          </span>
        </div>
        
        <div class="col s2 center ">
          <!-- Dropdown Button -->
 		  <a class='dropdown-button white-text dropdown-buttonMore' href='#' data-activates='dropdown-00' data-constrainwidth="false" data-alignment='right'><i class="material-icons">more_vert</i></a>
 		  
 		  <!-- Dropdown Structure -->
		  <ul id='dropdown-00' class='dropdown-content numIdeaDropdown'>
			<!-- 		  
		    <li><a class="activator grey-text text-darken-4" href="#!"><liferay-ui:message key="ims.more-info"/></a></li>
		    <li class="divider"></li>
		     -->
		    <li>
		    	<a class="grey-text text-darken-4 ideaLink" href="">
		    		<liferay-ui:message key="ims.view-idea"/>
		    	</a>
		    </li>
		  </ul>
       </div>
</div>



