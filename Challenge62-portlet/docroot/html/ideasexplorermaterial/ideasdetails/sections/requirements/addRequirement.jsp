<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSRequisitiLocalServiceUtil"%>
<%@ include file="/html/tasks/init.jsp" %>
<%@ include file="/html/challenges/init.jsp" %>
<portlet:resourceURL var="resourceURL" />

<link href="<%=request.getContextPath()%>/css/requirements-popup.css" rel="stylesheet">

<% 

long ideaId=ParamUtil.getLong(request, "ideaID");
String tipoRequisito=ParamUtil.getString(request, "tipoRequisito");
User currentUser = PortalUtil.getUser(request);
%>

<div class="addRequirementContent">


	<div class="reqTypeTitle">
		<b><liferay-ui:message key="ims.description"/></b>
		<textarea id="requisito" class="requirementDescription" cols="200" name="<portlet:namespace/>requisito" rows="4" ></textarea>
	</div>
	
		
	<button class="mdl-button mdl-button--raised btn_add_requirement" onclick="addReq()"><liferay-ui:message key="save"/></button>




	<A id="hbtn" hidden="true" href="<portlet:renderURL windowState="<%=WindowState.MAXIMIZED.toString()%>">
								<portlet:param name="jspPage" value="/html/ideasexplorermaterial/ideasdetails/view.jsp" />
			    				<portlet:param name="ideaId" value="<%=String.valueOf(ideaId)%>"/>
			 				 </portlet:renderURL>"><liferay-ui:message key="ims.return-to-idea"/></A>

</div>

 
 
 <script>
 function addReq(){
		YUI().use(
		  'aui-node',
		  'aui-io-request',
		  function(Y) {
		  
			  var requisito=Y.one("#requisito");
			   
		       Y.io.request('<%=resourceURL.toString()%>',
		 		   
			            {
				  data: {
				
					  
					  <portlet:namespace/>param:'addReq',
					  <portlet:namespace/>ideaId:'<%= ideaId%>',
					  <portlet:namespace/>authorUser:'<%= currentUser.getUserId() %>',
					  <portlet:namespace/>descReq:requisito.get("value"),
					  <portlet:namespace/>tipoRequisito:'<%= tipoRequisito%>',
					},
			              dataType: 'json',
			              on: {
			                success: function() {
			                	
			               
			                	 document.getElementById("hbtn").click();
			                }
			              }
			            }
			          );
		    
		    
		  }
		  
		);
}
</script >	