<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSRequisitiLocalServiceUtil"%>
<%@ include file="/html/tasks/init.jsp" %>
<%@ include file="/html/challenges/init.jsp" %>
<portlet:resourceURL var="resourceURL" />
<% 
long reqID=ParamUtil.getLong(request, "reqID");
long ideaId=ParamUtil.getLong(request, "ideaID");
CLSRequisiti requisito = CLSRequisitiLocalServiceUtil.getCLSRequisiti(reqID);

User currentUser = PortalUtil.getUser(request);

PortletURL modifyRequirementURL = renderResponse.createActionURL();
modifyRequirementURL.setParameter(ActionRequest.ACTION_NAME, "modifyRequirement");
%>



	<textarea cols="200" id="requisito"
		name="<portlet:namespace/>requisito" rows="4"
		style="margin: 30px; width: 90%;"><%=requisito.getDescrizione()%></textarea>
		
		<input name="<portlet:namespace/>reqId" type="hidden" value="<%=reqID%>"></input>
	<button class="btn" onclick="modifyReq()"><liferay-ui:message key="save"/></button>




<A id="hbtn" hidden="true" href="<portlet:renderURL windowState="<%=WindowState.MAXIMIZED.toString()%>">
								<portlet:param name="jspPage" value="/html/ideasexplorermaterial/ideasdetails/view.jsp" />
			    				<portlet:param name="ideaId" value="<%=String.valueOf(ideaId)%>"/>
			 				 </portlet:renderURL>"> <liferay-ui:message key="ims.return-to-idea"/></A>



 
 
 <script>
 function modifyReq(){
		YUI().use(
		  'aui-node',
		  'aui-io-request',
		  function(Y) {
		  
			  var requisito=Y.one("#requisito");
			   
		       Y.io.request('<%=resourceURL.toString()%>',
		 		   
			            {
				  data: {
				
					  
					  <portlet:namespace/>param:'modifyReq',
					  <portlet:namespace/>reqId:<%=reqID %>,
					  <portlet:namespace/>descReq:requisito.get("value"),
					  
					},
			              dataType: 'json',
			              on: {
			                success: function() {
			                	
			               
			                	 document.getElementById("hbtn").click();
			                }
			              }
			            }
			          );
		    
		    
		  }
		  
		);
}
</script >	
 
 