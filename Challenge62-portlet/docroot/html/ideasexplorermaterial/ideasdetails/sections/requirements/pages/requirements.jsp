<%@page import="it.eng.rspa.ideas.utils.IdeasListUtils"%>
<%@ include file="/html/challenges/init.jsp" %>
<%@page import="java.util.Locale" %>
<%String ideaeDescription = ""; %>

<link href="<%=request.getContextPath()%>/css/requirements-popup.css" rel="stylesheet">

				
<%
	PortletURL seleionaIdea = renderResponse.createActionURL();
	seleionaIdea.setParameter(ActionRequest.ACTION_NAME,"completeTask"); 
				
	seleionaIdea.setParameter("companyId",request.getParameter("companyId") );
	seleionaIdea.setParameter("userId",request.getParameter("userId") );
	seleionaIdea.setParameter("transictionName", request.getParameter("transictionName"));
	seleionaIdea.setParameter("ideaId", request.getParameter("ideaId"));
	seleionaIdea.setParameter("takenUp", "false" );

	boolean isTakenUp = Boolean.valueOf(request.getParameter("takenUp"));
	
	
	PortletURL takeChargeIdea = renderResponse.createActionURL();
	takeChargeIdea.setParameter("userId", request.getParameter("userId") );
	takeChargeIdea.setParameter("transictionName", "takeCharge");
	takeChargeIdea.setParameter("ideaId", request.getParameter("ideaId"));
	takeChargeIdea.setParameter("takenUp", "true");
	takeChargeIdea.setParameter(ActionRequest.ACTION_NAME,"takeCharge");
	
	
	CLSIdea idea =  CLSIdeaLocalServiceUtil.getCLSIdea( Long.valueOf(request.getParameter("ideaId")));
	String titoloGara = IdeasListUtils.getChallengeNameByIdeaId(idea.getIdeaID());
	
	String actionToSend = seleionaIdea.toString();
		if (isTakenUp)
			actionToSend = takeChargeIdea.toString();
						
%>

<div class="materialize">
	<div class="row">
		<div class="col s12">
			<a class="waves-effect btn-flat tooltipped" href="#" onClick="history.go(-1);" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.go-back"/>">
				<i class="icon-arrow-left left">&nbsp;</i><liferay-ui:message key="back"/>
			</a>
			<br/>
		</div>
		
		
		<div class="col s12">
		
				<% if (! (titoloGara.equals("") || isTakenUp)   ){ %>
				
				<h6 class="valign-wrapper valign-content-center">
				<i class="material-icons icon-trophy small tooltipped" data-position="top" data-delay="40" data-tooltip="<liferay-ui:message key="ims.challenge"/>">&nbsp;</i>
					<%=titoloGara %>
				</h6>
				<br>
				
				<% } %>
				<h6 class="valign-wrapper valign-content-center">
				<i class="material-icons icon-lightbulb small tooltipped" data-position="top" data-delay="40" data-tooltip="<liferay-ui:message key="ims.idea"/>">&nbsp;</i>
				<%=idea.getIdeaTitle() %>
				</h6>
				<br>
			
				<div id="refinement-tip">
					<blockquote>
				      	<liferay-ui:message key="ims.tip-refinement-request-section"/>
				   </blockquote>
				</div>
		</div>
		
	</div>
</div>

<div class="addRequirementContent">



		<form method="POST" action="<%= actionToSend %>">
		
			<div  id="requisitifunzionali" class="reqTypeTitle">
			    
				<b><liferay-ui:message key="ims.refinement-request"/></b> 
				<input hidden="true" id="numFunzionali" name="<portlet:namespace/>numFunzionali" value="0"></input>
				&nbsp;
			</div>
			<button class="mdl-button mdl-button--raised btn_add_requirement" type="button" value="Add" onClick="addInputFunctional('requisitifunzionali');"><liferay-ui:message key="add"/></button>
			
		
			<button type="submit" class="btnSave mdl-button mdl-button--raised btn_add_requirement" ><liferay-ui:message key="ims.save"/></button>
		
		</form>

</div>

		
<script type="text/javascript">

	$(document).ready(function(){
    
		addInputFunctional('requisitifunzionali');
  
 });

function addInputFunctional(divName){
   		
		 var counter=document.getElementById("numFunzionali").getAttribute("value");
		 counter++;
         var newdiv = document.createElement('div');
         newdiv.innerHTML = "<span>"+counter+"</span> <textarea id=\"testorequisitifunzionali"+(counter)+"\" name=\"<portlet:namespace/>testorequisitifunzionali"+(counter)+"\" ></textarea>" ;
         document.getElementById(divName).appendChild(newdiv);
        
         document.getElementById("numFunzionali").setAttribute("value", counter);  
}



</script>			
