<!-- ATTENZIONE! Questa pagina � legata alla pagina /html/ideasexplorermaterial/ideasdetails/pages/sections/ajax_comment.jsp -->
<!-- ATTENZIONE! Questa pagina � legata alla pagina /html/ideasexplorermaterial/ideasdetails/pages/sections/ajax_rating.jsp -->

<%@page import="com.liferay.portlet.PortletURLFactoryUtil"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFileEntry"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil"%>
<%@page import="com.liferay.util.portlet.PortletProps"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea"%>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFolder"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil" %>

<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ include file="/html/challenges/init.jsp" %>
<portlet:defineObjects />
<liferay-theme:defineObjects />

<portlet:resourceURL var="resURL"/>

<div class="materialize">

		<% PortletURL viewIdea =  renderResponse.createRenderURL();
				viewIdea.setParameter("ideaId", request.getParameter("ideaId"));
				viewIdea.setParameter("jspPage", "/html/ideasexplorermaterial/ideasdetails/view.jsp");
				viewIdea.setWindowState(WindowState.MAXIMIZED); %>
				
  	<!-- back row -->
   	<div class="row">
		<div class="col s12 m2 l2">
  			<a class="waves-effect btn-flat tooltipped" href="<%=viewIdea.toString()%>" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.go-back"/>">
  				<i class="material-icons icon-arrow-left small">&nbsp;</i><liferay-ui:message key="back"/>
  			</a>
  		</div>
	</div>	

	<div class="row row-background bottom-margin ">

	  	<div class="col s12 m12 l8 offset-l2">	
		

	<%
	String screenName = null;
	DLFileEntry screen = null;
	if(request.getParameter("screenId")!=null){
		long screenId = Long.parseLong(request.getParameter("screenId"));
		
		try{ 
			screen = DLFileEntryLocalServiceUtil.getDLFileEntry(screenId);
			screenName = screen.getTitle();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	try{
		CLSIdea idea = CLSIdeaLocalServiceUtil.getCLSIdea(Long.parseLong(request.getParameter("ideaId")));
		
		DLFolder screenshotsFolder = DLFolderLocalServiceUtil.getFolder(idea.getGroupId(), idea.getIdFolder(), PortletProps.get("screenshot.foldername"));
		DLFolder thumbsFolder = DLFolderLocalServiceUtil.getFolder(idea.getGroupId(), screenshotsFolder.getFolderId(), PortletProps.get("screenshot.thumbnail.foldername"));
		
		List<DLFileEntry> screenshots = DLFileEntryLocalServiceUtil.getFileEntries(idea.getGroupId(), thumbsFolder.getFolderId());
		Iterator<DLFileEntry> it = screenshots.iterator();
		
		String pathDocumentLibrary = themeDisplay.getPortalURL()
									+ themeDisplay.getPathContext() + "/documents/"
									+ idea.getGroupId() + StringPool.SLASH;
		%>
		
		<div class="card-multi-title flow-text"><liferay-ui:message key="screenshots"/></div>
					
		<div class="grey-text text-darken-2">
		  	<label class=""><liferay-ui:message key="ims.idea"/></label>
       		<%= idea.getIdeaTitle()%><!-- 60 char -->
      	</div>
		
		<blockquote  class="valign-wrapper">
			<label><liferay-ui:message key="ims.engagement-phrase-voting"/></label>
		</blockquote>

		
		<div class="screenshotContent">	
			<div class="carousel">
				<%while(it.hasNext()){	DLFileEntry dlf = it.next();%> 
		
						<a  class="carousel-item tooltipped" data-position="top" data-delay="50" data-tooltip='<liferay-ui:message key="ims.select-to-view-details"/>' href="#!">
				    		<span class="thumbs-list" data-filename="<%=dlf.getTitle() %>" 	data-entryid="<%=dlf.getFileEntryId() %>">
				    			<img src='<%=pathDocumentLibrary+dlf.getUuid()%>'/>
				    		</span>
				    	</a>
				<%} %>
			</div>
		</div>
		<div class="section" id="sezcommenti">
			<h5 class="header"><liferay-ui:message key="details"/></h5>
			<div class="divider bottom-margin"></div>	
			
			<div id="preview">
				<div class="row" id="img-and-star-container">
					<div class="col s12">
						<div id="img-and-star-container">
							<liferay-ui:message key="ims.select-an-image"/>
						
							<%if(screenName!=null && !screenName.equals("")){
								String uri = "";
								if(screen.getFolder().getName().equals(PortletProps.get("screenshot.thumbnail.foldername"))){
									//Se l'id appartiene a un thumbnail recupera l'immagine fullsize
									uri = pathDocumentLibrary+DLFileEntryLocalServiceUtil.fetchFileEntry(idea.getGroupId(), screenshotsFolder.getFolderId(), screenName).getUuid();
								}
								else if(screen.getFolder().getName().equals(PortletProps.get("screenshot.foldername"))){
									//Se l'id appartiene a un'immagine fullsize la mostro direttamente
									uri = pathDocumentLibrary+screen.getUuid();
								}%>
								<img class="previewImgs" src="<%=uri%>"/>
							<%} %>
						</div>
					</div>
				</div>
				<div class="row">	
					<div class="col s12 m6" id="info-box">
							<p id="uploaded-by" class='<%=(screenName!=null && !screenName.equals(""))?"showed":"hidden" %>'>
								<label><liferay-ui:message key="ims.uploaded-by"/></label><i><%=(screenName!=null && !screenName.equals(""))? UserLocalServiceUtil.getUser(screen.getUserId()).getFullName() : "no info" %></i>
							</p>
							<p id="creation-date" class='<%=(screenName!=null && !screenName.equals(""))?"showed":"hidden" %>'>
								<label><liferay-ui:message key="ims.creation-date"/></label><i><%=(screenName!=null && !screenName.equals(""))? screen.getCreateDate() : "" %></i>
							</p>
					</div>
					<div class="col s12 m6" id="star-box">		
							<div id="stars" class='<%=(screenName!=null && !screenName.equals(""))?"showed":"hidden" %>'>
							<%if(screenName!=null && !screenName.equals("")){ %>
								<liferay-ui:ratings className="<%= DLFileEntry.class.getName() %>" classPK="<%=screen.getFileEntryId() %>"/>
							<%} %>
							</div>
					</div>
					<div class="col s12" id="description">
						<p class="descriptionContent">
						<%if(screenName!=null && !screenName.equals("")){
								out.print(screen.getDescription());
						} %>
						</p>
					</div>
				</div>
			</div>
		</div>
			
			
			<div id="comment-box">
					<%if(screenName!=null && !screenName.equals("")){ %>
					
						<liferay-ui:panel-container extended="true" id="ideaCommentsPanelContainer" persistState="true">
							<liferay-ui:panel collapsible="false" 
											  extended="false"
											  id="ideaCommentsPanel"
											  persistState="<%= true %>"
										      title="">
												
							
									<portlet:actionURL name="invokeTaglibDiscussion" var="discussionURL" />
									<%String currentURL = PortalUtil.getCurrentURL(request);
									  currentURL = currentURL + "&screenId="+screen.getFileEntryId();%>
									<liferay-ui:discussion  className='<%= DLFileEntry.class.getName() %>'
											classPK="<%= screen.getFileEntryId() %>"
									 		formAction='<%= discussionURL %>'
									 		formName="fm2"
											ratingsEnabled="false" 
											redirect="<%=currentURL %>"
											subject="screenshot_<%=screen.getFileEntryId() %>"
											userId="<%= themeDisplay.getUserId() %>" />
							</liferay-ui:panel>
						</liferay-ui:panel-container>
					<%} %>
			</div>
		
		<portlet:renderURL var="rating" windowState="<%= LiferayWindowState.EXCLUSIVE.toString() %>">
			<portlet:param name="jspPage" value="/html/ideasexplorermaterial/ideasdetails/pages/sections/ajax_rating.jsp"/>
			<portlet:param name="classname" value="<%=DLFileEntry.class.getName() %>"/>
		</portlet:renderURL>
		
		<portlet:renderURL var="comment" windowState="<%= LiferayWindowState.EXCLUSIVE.toString() %>">
			<portlet:param name="jspPage" value="/html/ideasexplorermaterial/ideasdetails/pages/sections/ajax_comment.jsp"/>
			<portlet:param name="classname" value="<%=DLFileEntry.class.getName() %>"/>
			<portlet:param name="ideaId" value="<%=String.valueOf(idea.getIdeaID()) %>"/>
		</portlet:renderURL>
		
		
		</div>
		
		<script>
			$(document).ready(function(){
				$(".thumbs-list").click(function(e){
	// 				e.preventDefault();
	// 				e.stopPropagation();
					
					$('.current-thumb').toggleClass('current-thumb');
					$(this).toggleClass('current-thumb');
					
					var filename = $(this).data('filename');
					var primKey = $(this).data('entryid'); 
					
					$.post('${resURL}',
							{'<portlet:namespace/>param': 'showScreenShot',
							 '<portlet:namespace/>ideaId': '${idea.getIdeaID()}',
							 '<portlet:namespace/>filename': filename})
						.done(function(r){
							var response = $.parseJSON(r);
// 							console.log(response);
							
							var user = response.userScreenName;
							var date = response.uploadDateTime;
							
							if($('#img-and-star-container').has('img').length){ 
								//Se la preview mostra gi� un'immagine
								//Cambia solamente la sorgente dell'immagine
								$('#img-and-star-container > img').attr('src', response.screenshotUrl);
							}
							else{
								//Se la preview non mostra ancora alcuna immagine
								//Inserisci il nodo img e definisci la sorgente
								var imgNode = $('<img>');
								imgNode.attr('src', response.screenshotUrl);
								imgNode.addClass('previewImgs');
								$('#img-and-star-container').html(imgNode);
							}
							$('#description').html('<label><liferay-ui:message key="description"/></label>'+response.screenshotDescription);
							
							//Carica in maniera asincrona la taglib per il rating
							var ratingUrl = '${rating}';
								ratingUrl += '&<portlet:namespace/>classPK=' + primKey;
								
							var commentUrl = '${comment}';
								commentUrl += '&<portlet:namespace/>classPK=' + primKey;
								
							$.post(ratingUrl)
								.done(function(e){
									$('#stars').html(e).addClass("showed").removeClass("hidden");
									$('#creation-date i').html(date);
									$('#uploaded-by i').html(user);
									
									$('#uploaded-by').addClass("showed").removeClass("hidden");
									$('#creation-date').addClass("showed").removeClass("hidden");
									$('#stars').addClass("showed").removeClass("hidden");
									
								});
							
							$.post(commentUrl)
								.done(function(e){
									$('#comment-box').html(e);
								});
			   			})
					
				})
			})
			
			    $(document).ready(function(){
			      $('.carousel').carousel({dist:-100});
			    });
		</script>
	<%}
	catch(Exception e){
		%>	
		<liferay-ui:message key="ims.no-screenshot-available"/>
	<%	
		System.out.println(e.getMessage());
	}%>
	
	
		
	</div>

</div>	