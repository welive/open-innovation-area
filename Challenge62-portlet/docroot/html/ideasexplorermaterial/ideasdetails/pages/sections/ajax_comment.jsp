<!-- ATTENZIONE! Questa pagina � legata alla pagina /html/ideasexplorermaterial/ideasdetails/pages/screenshots.jsp -->


<%@ include file="/html/challenges/init.jsp" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ include file="/html/challenges/init.jsp" %>
<portlet:defineObjects />
<liferay-theme:defineObjects />
<%

String className = request.getParameter("classname");
long classPK = Long.parseLong(request.getParameter("classPK"));
%>
<div class="section" id="sezcommenti">
	<h5 class="header"><liferay-ui:message key="comments"/></h5>
	<div class="divider"></div>

	<liferay-ui:panel-container extended="true" id="ideaCommentsPanelContainer" persistState="true">
		<liferay-ui:panel collapsible="false" 
								  extended="false"
								  id="ideaCommentsPanel"
								  persistState="<%= true %>"
							      title="">
							      

					<!--  title='<%= LanguageUtil.get(pageContext, "comments") %>'> -->

			<portlet:actionURL name="invokeTaglibDiscussion" var="discussionURL" />
			<liferay-ui:discussion  className='<%= className %>'
					classPK="<%= classPK %>"
			 		formAction='<%= discussionURL %>'
			 		formName="fm2"
					ratingsEnabled="false"
					subject="screenshot_<%=classPK %>"
					userId="<%= themeDisplay.getUserId() %>" />
			
	
		</liferay-ui:panel>
	</liferay-ui:panel-container>

</div>