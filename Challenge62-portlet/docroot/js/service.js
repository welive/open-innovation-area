Liferay.Service.register("Liferay.Service.CSL", "it.eng.rspa.ideas.challenges.servicelayer.service", "ChallengeGA3-portlet");

Liferay.Service.registerClass(
	Liferay.Service.CSL, "CLSChallenge",
	{
		getChallengesCount: true,
		getChallengesByUserId: true
	}
);

Liferay.Service.registerClass(
	Liferay.Service.CSL, "CLSIdea",
	{
		getIdeasByChallengeId: true,
		getIdeasByUserId: true,
		searchIdeaByTileAndDescription: true,
		getRDFIdea: true
	}
);