/*Puglia@Service gradient*/
//var gradient = [ "#009FDE", "#007DB8", "#0F73B2", "#0B5480"];
//var gradient = [ "#80CFEE", "#66B1D4", "#0F73B2", "#0B5480"];

/*MyOpenGov gradient*/
//var gradient = [ "#38ADC7", "#008AA9", "#00677C" , "#005060"];

/*EDISON*/
//var gradient = ["#77B7D5","#3897C1","#448CB6","#3B7B9F"];

/*WeLive gradient*/
	var gradient = [ "#DDDDDD", "#F7323F", "#67B2E8", "#B6BD00"];

$( document ).ready(function() {
	
	dimensionEllipsis();
});

$( window ).resize(function() {
	
	dimensionEllipsis();
});

function dimensionEllipsis(){
	
	if($(".lineDiv").length > 0) {
		$(".lineDiv").each(function() {
			var w1 = $( this ).width();
			var w2 = $( this ).css("padding-left").replace("px", "");
			var w3 = $( this ).css("margin-left").replace("px", "");
			if($( this ).find("img").length > 0)
				var wi = $( this ).find("img").outerWidth(true);
			else wi = 0;
			
			if(w2+w3 < 10) {w2=5; w3=5};
//			console.log("LINK "+$( this ).find(".divFeatures").find("a").find("span").html())
			$( this ).find(".divFeatures").find("a").css("width", w1-wi-w2-w3);
			
//			console.log("Misure "+w1+" "+wi+" "+w2+" "+w3)
		});

	}
	
}
//function dimensionEllipsis(){
//	if($(".lineDiv").length > 0) {
//		$.each($(".lineDiv"), function() {			
//			var w1 = $(".lineDiv").width();
//			var w2 = $(".lineDiv").css("padding-left").replace("px", "");
//			var w3 = $(".lineDiv").css("margin-left").replace("px", "");
//			if($(".lineDiv img").length > 0)
//				var wi = $(".lineDiv img").outerWidth(true);
//			if(w2+w3 < 10) {w2=5; w3=5};
//			$(".lineDiv .divFeatures a").css("width", w1-wi-w2-w3);
////			console.log(w1+" "+wi+" "+w2+" "+w3)
//		});
//	}
//}



/*task*/
AUI().use(
		'aui-base',
		'aui-io-plugin-deprecated',
		'liferay-util-window',
		function(A) {
			Liferay.namespace('Tasks');

			Liferay.Tasks = {
				init: function(param) {
					var instance = this;

					instance._setupFilter();
					instance._setupTagsPopup();
					instance._setupProgressBar();

					instance._currentTab = param.currentTab;
					instance._namespace = param.namespace;
					instance._taskListURL = param.taskListURL;
				},

				initUpcomingTasks: function(param) {
					var instance = this;

					instance._upcomingTasksListURL = param.upcomingTasksListURL;
				},

				clearFilters: function() {
					var instance = this;

					A.all('.tasks-portlet .asset-tag-filter .asset-tag.selected').toggle('selected');

					var groupFilter = A.one('.tasks-portlet .group-filter select');

					if (groupFilter) {
						groupFilter.set('value', 0);
					}

					var showAll = A.one('.tasks-portlet input[name="all-tasks"]').get('checked');

					instance.updateTaskList(null, showAll);
				},

				closePopup: function() {
					var instance = this;

					instance.getPopup().hide();
				},

				displayPopup: function(url, title) {
					var instance = this;

					var viewportRegion = A.getBody().get('viewportRegion');

					var popup = instance.getPopup();

					popup.show();

					popup.titleNode.html(title);

					popup.io.set('uri', url);
					popup.io.start();
				},

				getPopup: function() {
					var instance = this;

					if (!instance._popup) {
						instance._popup = Liferay.Util.Window.getWindow(
							{
								dialog: {
									align: {
										node: null,
										points: ['tc', 'tc']
									},
									constrain2view: true,
									cssClass: 'tasks-dialog',
									modal: true,
									resizable: false,
									width: 600
								}
							}
						).plug(
							A.Plugin.IO,
							{autoLoad: false}
						).render();
					}

					instance._popup.io.set('form', null);
					instance._popup.io.set('uri', null);

					return instance._popup;
				},

				openTask: function(href) {
					this.displayPopup(href, "Tasks");
				},

				toggleCommentForm: function() {
					var comment = A.one('.tasks-dialog .add-comment');

					var control = comment.one('.control');
					var form = comment.one('.form');

					form.toggle();
					control.toggle();
				},

				toggleTasksFilter: function() {
					A.one('.tasks-portlet .filter-wrapper').toggle();
				},

				updateTaskList: function(url, showAll) {
					var instance = this;

					instance._taskList = A.one('.tasks-portlet .list-wrapper');

					if (!instance._taskList) {
						instance._taskList = A.one('.upcoming-tasks-portlet .tasks-entries-container');

						if (!url) {
							url = instance._upcomingTasksListURL;
						}
					}

					if (!instance._taskList.io) {
						instance._taskList.plug(
							A.Plugin.IO,
							{autoLoad: false}
						);
					}

					if (!url) {
						url = instance._taskListURL;

						var data = {};

						data[instance._namespace + 'assetTagIds'] = instance._getAssetTagIds();
						data[instance._namespace + 'groupId'] = instance._getGroupId();
						data[instance._namespace + 'tabs1'] = instance._currentTab;
						data[instance._namespace + 'tabs2'] = showAll ? 'all' : 'open';

						instance._taskList.io.set('data', data);
					}

					instance._taskList.io.set('uri', url);

					instance._taskList.io.start();
				},

				_getAssetTagIds: function() {
					var assetTagIds = [];

					A.all('.tasks-portlet .asset-tag-filter .asset-tag.selected').each(
						function(assetTag, index, collection) {
							assetTagIds.push(assetTag.attr('data-assetTagId'));
						}
					);

					return assetTagIds.join(',');
				},

				_getGroupId: function() {
					var groupSelect = A.one('.tasks-portlet .group-filter select');

					if (!groupSelect) {
						return 0;
					}

					return groupSelect.get('value');
				},

				_setupFilter: function() {
					var instance = this;

					A.one('.tasks-portlet .asset-tag-filter').delegate(
						'click',
						function(event) {
							var assetTag = event.currentTarget;

							assetTag.toggleClass('selected');

							var showAll = A.one('.tasks-portlet input[name="all-tasks"]').get('checked');

							instance.updateTaskList(null, showAll);
						},
						'.asset-tag'
					);

					A.all('.tasks-portlet .group-filter select').on(
						'change',
						function(event) {
							var showAll = A.one('.tasks-portlet input[name="all-tasks"]').get('checked');

							instance.updateTaskList(null, showAll);
						}
					);
				},

				_setupTagsPopup: function() {
					var container = A.one('.tasks-portlet');

					container.delegate(
						'mouseover',
						function(event) {
							event.currentTarget.one('.tags').show();
						},
						'.tags-wrapper'
					);

					container.delegate(
						'mouseout',
						function(event) {
							event.currentTarget.one('.tags').hide();
						},
						'.tags-wrapper'
					);
				},

				_setupProgressBar: function() {
					var instance = this;

					var portlet = A.one('.tasks-portlet .list-wrapper');

					portlet.delegate(
						'mouseover',
						function(event) {
							event = event.currentTarget;

							event.one('.current').hide();
							event.one('.progress-picker').show();
						},
						'.progress-wrapper'
					);

					portlet.delegate(
						'mouseout',
						function(event) {
							event = event.currentTarget;

							event.one('.current').show();
							event.one('.progress-picker').hide();
						},
						'.progress-wrapper'
					);

					portlet.delegate(
						'mouseover',
						function(event) {
							event = event.currentTarget;

							var str = event.getAttribute('class');
							var pos = str.substring(str.indexOf('progress-') + 9);

							var container = event.ancestor('.progress-wrapper');

							container.one('.new-progress').setStyle('width', pos + '%');
							container.one('.progress-indicator').set('text', pos + '% Complete');
						},
						'.progress-selector a'
					);

					portlet.delegate(
						'click',
						function(event) {
							event.halt();

							var href = event.currentTarget.getAttribute('href');

							instance.updateTaskList(href);
						},
						'.progress-selector a'
					);
				}
			}
		}
	);


// Remove every Tooltip if window size is less than 600px (Mobile)
$( window ).load(function() {
	  // Run code
	console.log("OIA Started");
	if($( window ).width() < 600){
		$('*').tooltip('remove');
		console.log("Tooltip Disabled");
	}
});