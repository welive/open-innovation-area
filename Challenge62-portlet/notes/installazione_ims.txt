# Una volta installato Liferay, mantenere la lingua inglese ecambiarla in taliano solo dopo aver completato i passi seguenti

#modificare il nome di defualt del site
1) andare in admin -> control panel -> configuration -> portal settings -> Name (required)

# Configurazone Ruoli
1) Andare in pannello di controllo -> ruoli
2) Creare i ruoli "Ente" "Cittadino" "Board", per i quali definire i seguenti permessi:
	  Site Administration
	    Applications
	       Calendar
	          Resource Permissions
	             Calendar
	                View Event Details
	  	Content
	  		Documents and Media
	  		  General Permissions
		     	Permissions
			  Resource Permissions
			  	Documents
			  		Add Document
		          	Add Folder 
		          	Update
		          	View
		        Document
		        	Permissions
3) Fornire al ruolo guest la possibilit� di vedere i documenti
   Site Administration
      Content
         Documents and Media
            Documents Folder
               View

#Installare
 1) Social Office
 2) chat-portlet-6.2.0.3.war
 3) notifications-portlet-6.2.0.1.war
 4) Challenge62-portlet-6.2.0.1.war
 5) PugliaAtServiceCustonThumbsRatingDisplay-hook-6.2.0.1.war
 6) PugliaAtServiceFlagAs-hook-6.2.0.1.war
 7) PugliaAtServicePortalLanguageExtension-hook-6.2.0.1.war
 8) search-url-users-redirectToSOprofile-hook-6.2.0.1.war
 9) assetsearch-hook-6.2.0.1.war
10) Reputazione-utente-portlet-6.2.0.1.war
11) Colors-theme-6.2.0.1.war
12) contacts-portlet-6.2.0.10.war
13) endorser-service-portlet-6.2.0.1.war
14) pats-social-theme-6.2.0.1.war

# Impostare il tema principale
1) Dopo aver installato "Colors-theme-6.2.0.1.war", sostituire il file "favicon.con" presente in "liferay-portal-6.2-ce-ga2\tomcat-7.0.42\webapps\Colors-theme\images" con quello presente in "liferay-portal-6.2-ce-ga2\tomcat-7.0.42\webapps\Colors-theme\images\color_schemes\azzurro"
2) Nella pagina di amministrazione del site, dal men� Admin -> site administration, selezionare il tema "Color" e scegliere la combinazione di colori "Azzurro Mediterraneo"
3) Spostarsi nella sezione "Logo" e scegliere il file "puglia@service_style4_v3a-02.png".
4) Inserire "Puglia@Service" come "footer-text" 

# Impostare il tema di Social Office
1) Dopo aver installato "pats-social-theme-6.2.0.1.war", andare nella pagine di amministrazione dei sete templates, Admin-> Site Templates, e per i site:
   Social Office User Home
   Default Social Office Site
   Social Office User Profile
   Impostare il tema "PatS Social" e successivamente inserire "Puglia@Service" come "footer-text" 

# Configurazone workflow
Creazione
1) da pannello di controllo di liferay entrare nella sezione "Workflow" (Flusso di lavoro) sotto Configuration e cliccare su "Upload Definition"
2) selezionare il file xml che descrive il workflow (in questo caso "ideaWorkflow.xml")
3) inserire il nome del workflow e clicare "Save"
4) selezionare il tab "Default Configuration" (Configurazione workflow)
5) associare all'asset "idea" (model.resource.it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea) il workflow appena creato

P.S.
Per il corretto funzionamento del workflow occorre che sul portale Liferay esistano i ruoli Ente, Cittadino e Board.

Modifica
1) da pannello di controllo di liferay entrare nella sezione "Workflow" del menu "Portal"
2) selezionare "Edit" dal men� "Action" e rifare l'upload del nuovo file di descrizione

# Configurazione pagine pubbliche e Friendly URL

1) Creare la pagina [Tutte le idee]
2) Aggiungere la portlet Ideas Explorer alla pagina [Tutte le idee]
3) Nelle impostazioni della pagina [Tutte le idee] (Gestisci -> Pagine) inserire come parte finale del Friendly URL "/ideas_explorer"
4) Il Friendly URL di una idea risulter� essere (no considerare i caratteri "[", "]", "{", e "}" )
   [http://liferay.ideario.it:8181]/web/guest/[ideas_explorer]/-/ideas_explorer_contest/{id dell'idea}/view
5) Aggiungere lan portlet "Esplora le Idee"

6) Creare la pagina [Tutte le gare]
7) Aggiungere la portlet Challenges Explorer alla pagina [Tutte le gare]
8) Nelle impostazioni della pagina [Tutte le gare] (Gestisci -> Pagine) inserire come parte finale del Friendly URL "/challenges_explorer"
9) Il Friendly URL di una idea risulter� essere (no considerare i caratteri "[", "]", "{", e "}" )
   [http://liferay.ideario.it:8181]/web/guest/[challenges_explorer]/-/challenges_explorer_contest/{id della gara}/view
10)Aggiungere lan portlet "Esplora le Gare" 

N.B.
I nomi tra [] sono provvisori
I nomi tra {} sono sostituiti da valori

# Configarazione delle pagine per gli utenti
1) Creare la pagina "Ente" con layout a 1 colonna e aggiungere la portlet "Cruscotto Ente"; fornire i permessi di visualizzazione al ruolo "Ente" e toglierli al ruolo "Guest" 
2) Creare la pagina "Cittadino" con layout a 1 colonna e aggiungere la portlet "Cruscotto Cittadino"; fornire i permessi di visualizzazione al ruolo "Cittadino" e toglierli al ruolo "Guest"
3) Creare la pagina "Board" con layout a 1 colonna e aggiungere la portlet "Cruscotto Board"; fornire i permessi di visualizzazione al ruolo "Board" e toglierli al ruolo "Guest"

# Configarazione della pagina del profilo di social office
1) andare in pannello di controllo -> site templates
2) Social Office User Profile -> Action -> View Pages
3) nella colonna di destra posizionare la portlet "Reputazione Utente"
4) spostare la portlet "Profile" nella colonna di sinistra
5) nele preferenze della portlet "Profile" tolgiere la visualizzazione dei siti
6) togliere la porlet Actvities
7) aggiungere la portlet Social -> Activities


# Configurazione Liferay
Nel file webapps\ROOT\WEB-INF\classes\portal-ext.properties (se non esiste crearlo) aggiungere i seguenti parametri:
* per la gestione dei voti

    ratings.max.score[it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge]=1000
    ratings.min.score[it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge]=-1000
    ratings.max.score[it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea]=1000
    ratings.min.score[it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea]=-1000


# Se fosse necessatio settare il DB
Creare su PostGres il DB lportal
Nello stesso file inserire i parametri per il db

jdbc.default.driverClassName=org.postgresql.Driver
jdbc.default.url=jdbc:postgresql://localhost:5432/lportal
jdbc.default.username=sa
jdbc.default.password=

#Configurazione portlet search di liferay
Aprire la configurazione della portlet "search"
Passare alla confifurazione avanzata
In Search Configuration aggiungere a "values" i valori: "it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge" e "it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea"

#Asset publisher
Se si usa l'asset publisher, disabilitare i social bookmark

#Togliere (e/o assicurarsi che siano tolti) agli utenti Power User, user e guest il permesso di aggiungere le portlet 
"Cruscotto Ente"
"Cruscotto Cittadino"
"Cruscotto Board" 
"Esplora le idee"
"Esplora le gare" 
"Idea Management System configurazione" 
"Le idee pi� votate"
"Guarda tutte le idee su"
"Idee Condivise"

Es. [Ruolo]> Amministrazione Sito > Applicazioni > Cruscotto Ente>Permessi Applicazione>Aggiungi alla pagina --> no check


#Configurare la pagina principale:
1) Impostare il layout della pagina su due colonne
2) Inserire all�interno della colonna di sinistra la portlet
    "Classifica Utenti"
    "La Mia Reputazione" (togliere i permessi di visualizzazione al ruolo Guest e fornirli al Cittadino)
    "Scrivi la tua Idea" (togliere i permessi di visualizzazione al ruolo Guest e fornirli al Cittadino)
    "Idee condivise"
3) Inserie all�interno della colonna di destra nell�ordine la portlet �Le idee pi� votate� e la portlet �Guarda tutte le idee su�.

Utenti:
Ente: pa@liferay.com
Cittadino: guidorossi@liferay.com
Cittadino: elenarusso@liferay.com
Cittadino: marcobianchi@liferay.com
Board: livinglab@liferay.com

password alla creazione: pwd
nuova password: test

# Associare gli utenti a Social Office
Andare in admin -> control panel -> Social Office Configurations e selzionare gli utenti abilitati all'utilizzo di Social Office

# Creare i seguenti vocabolari e categorie:
Luoghi
	Bari
	Lecce
	Salento
Ambiente
	Inquinamento
	Parchi
	Verde pubblico
Economia
	Lavoro
	Retribuzioni
Societ�
	Comunit�
	Immigrazione
	Integrazione
Sviluppo
	Beni
	Imprenditoria
	Servizi
	Incentivi
	Piccole Aziende
Territorio
	Ferrovie
	Strade
	Viabilit�
Turismo
	Alberghi
	Guide
	Itinerari
	Operatori Turistici

# Configurazone IMS
1) Andare nel men� Admin -> content
2) Inserire in "Suffisso Idee" il valore: ideas_explorer
3) Inserire in "Suffisso Gare" il valore: challenges_explorer
4) In "Categorie per Gare di Idee e Idee" selezionare i valori desiderati
5) In "Parametri Mappa", se si vuole, modificare i valori di latitudine e longitudine
6) In "Calendario delle Gare di Idee" selezionare il calendario del site principale.